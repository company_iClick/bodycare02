$(document).ready(function() {
    load_scroll();
    toogle_scroll();  
    pagination_ajax();
    pagination_ajax_cagegory(); 
    pagination_ajax_cagegory_child();
    change_layout();
    filter_mobile();
});


function load_scroll(){
        
    $("a[rel='load-content']").click(function(e){
        e.preventDefault();
        var url=$(this).attr("href");
        $.get(url,function(data){
            $(".list_th_scroll_1 .mCSB_container").append(data); //load new content inside .mCSB_container
            //scroll-to appended content 
            $(".list_th_scroll_1").mCustomScrollbar("scrollTo","h2:last");
        });
    });
    
    $(".list_th_scroll_1").delegate("a[href='top']","click",function(e){
        e.preventDefault();
        $(".list_th_scroll_1").mCustomScrollbar("scrollTo",$(this).attr("href"));
    });
        
   

}

function toogle_scroll(){
    $('.click_show_left').click(function(){
        $('.list_th_scroll_1').toggle();

        if($(this).children('i').hasClass('a_arrow_123')){
            $(this).children('i').removeClass('a_arrow_123'); 
        }else{
            $(this).children('i').addClass('a_arrow_123');
        }
    })
}

function pagination_ajax(){
    $('#pagination_ajax li a').click(function(){
        var page = $(this).data('page');
        var page_current = $('input[name=page]').val();
        if(page == page_current) return false;

        $('input[name=page]').val(page);
        $('#pagination_ajax li').removeClass('active');
        $(this).parent('li').addClass('active');
        product_filter(true);
    })
}

function pagination_ajax_cagegory(){
    $('.paging_ajax_category li a').click(function(){
        var mod = $('input[name=mod]').val();
        var mod_id = $('input[name=mod_id]').val();
        var cate_id = $(this).data('cate');
        var page = $(this).data('page');
        var page_current = $('#load_product_list_' + cate_id).find('.paging_ajax_category li.active a').data('page');
        if(page == page_current) return false;
        var page_co = $(this).data('item');
        var total_ct = $('input[name=total_ct_'+ cate_id +']').val();
        var layout = $('input[name=layout]').val();
        var keyword = $('input[name=keyword]').val();

        var ajax_loadding = $('.ajax_loadding');
        var header_h = $('#header1').height();
        var list_product_h = $('#load_product_list_' + cate_id).offset().top;

        //get price value
        var input_price = [];
        $('input[name="input_price[]"]:checked').each(function(i){
            input_price[i] = $(this).val();
        });

        //get brand value
        var input_brand = [];
        $('input[name="input_brand[]"]:checked').each(function(i){
            input_brand[i] = $(this).val();
        });

        //get origin value
        var input_origin = [];
        if(mod != 'origin'){  
            $('input[name="input_origin[]"]:checked').each(function(i){
                input_origin[i] = $(this).val();
            });
            if($('#input_origin:checked').length > 0){
                input_origin = $('#input_origin:checked').val();
            }
        }else{
            mod = 'category';
            input_origin = $('input[name=method_id_'+ cate_id +']').val();
        }

        //get product_type value
        var input_product_type = [];
        if(mod != 'product_type'){
            $('input[name="input_product_type[]"]:checked').each(function(i){
                $('#input_product_type').prop('checked', false); 
                input_product_type[i] = $(this).val();
            });
            if($('#input_product_type:checked').length > 0){
                input_product_type = $('#input_product_type:checked').val();
            }
        }else{
            mod = 'category';
            input_product_type = $('input[name=method_id_'+ cate_id +']').val();
        }

        //get skin_type value
        var input_skin_type = [];
        if(mod != 'skin_type'){
            $('input[name="input_skin_type[]"]:checked').each(function(i){
                $('#input_skin_type').prop('checked', false); 
                input_skin_type[i] = $(this).val();
            });
            if($('#input_skin_type:checked').length > 0){
                input_skin_type = $('#input_skin_type:checked').val();
            }
        }else{
            mod = 'category';
            input_skin_type = $('input[name=method_id_'+ cate_id +']').val();
        }

        //get hair_type value
        var input_hair_type = [];
        if(mod != 'hair_type'){
            $('input[name="input_hair_type[]"]:checked').each(function(i){
                $('#input_hair_type').prop('checked', false); 
                input_hair_type[i] = $(this).val();
            });
            if($('#input_hair_type:checked').length > 0){
                input_hair_type = $('#input_hair_type:checked').val();
            }
        }else{
            mod = 'category';
            input_hair_type = $('input[name=method_id_'+ cate_id +']').val();
        }
      

        //get action value
        var input_action = [];
        if(mod != 'action'){      
            $('input[name="input_action[]"]:checked').each(function(i){
                $('#input_action').prop('checked', false); 
                input_action[i] = $(this).val();
            });
            if($('#input_action:checked').length > 0){
                input_action = $('#input_action:checked').val();
            }
        }else{
            mod = 'category';
            input_action = $('input[name=method_id_'+ cate_id +']').val();
        }
       
        //get capacity value
        var input_capacity = [];
        if(mod != 'capacity'){  
            $('input[name="input_capacity[]"]:checked').each(function(i){
                $('#input_capacity').prop('checked', false); 
                input_capacity[i] = $(this).val();
            });
            if($('#input_capacity:checked').length > 0){
                input_capacity = $('#input_capacity:checked').val();
            }
        }else{
            mod = 'category';
            input_capacity = $('input[name=method_id_'+ cate_id +']').val();
        }
       
        //get weigh value
        var input_weigh = [];
        if(mod != 'weigh'){ 
            $('input[name="input_weigh[]"]:checked').each(function(i){
                $('#input_weigh').prop('checked', false); 
                input_weigh[i] = $(this).val();
            });
            if($('#input_weigh:checked').length > 0){
                input_weigh = $('#input_weigh:checked').val();
            }
        }else{
            mod = 'category';
            input_weigh = $('input[name=method_id_'+ cate_id +']').val();
        }
        

        //get pill_number value
        var input_pill_number = [];
        if(mod != 'pill_number'){
            $('input[name="input_pill_number[]"]:checked').each(function(i){
                $('#input_pill_number').prop('checked', false); 
                input_pill_number[i] = $(this).val();
            });
            if($('#input_pill_number:checked').length > 0){
                input_pill_number = $('#input_pill_number:checked').val(); 
            }
        }else{
            mod = 'category';
            input_pill_number = $('input[name=method_id_'+ cate_id +']').val();
        }
        
        $.ajax({
            type: 'POST',
            url: 'category-page',
            data: {'mod': mod, 'mod_id': mod_id, 'cate_id': cate_id, 'page_no': page, 'page_co': page_co, 'input_brand': input_brand, 'input_origin': input_origin, 'input_product_type': input_product_type, 'input_skin_type': input_skin_type, 'input_hair_type': input_hair_type, 'input_action': input_action, 'input_capacity': input_capacity, 'input_weigh': input_weigh, 'input_pill_number': input_pill_number, 'input_price': input_price, 'total_ct': total_ct, 'layout': layout, 'keyword': keyword},
            beforeSend: function(){
                $('#load_product_list_' + cate_id).find('.ajax_loadding').show();
            },
            success:function(result){
                $('#load_product_list_' + cate_id).html(result);
                $('#page_cate_' + cate_id).find('.paging_ajax_category li').removeClass('active');
                $('#page_cate_' + cate_id).find('.pajaxcate_' + page).addClass('active');
                $('#load_product_list_' + cate_id).find('.ajax_loadding').hide();
                $('html, body').animate({scrollTop:list_product_h - header_h - 63},500);
                pagination_ajax_cagegory();
            }
        })
    })
}

function pagination_ajax_cagegory_child(){
    $('.paging_ajax_category_child li a').click(function(){

        var mod = $('input[name=mod]').val();
        var layout = $('input[name=layout]').val();

        var cate_id = $(this).data('cate');
        var page = $(this).data('page');
        var page_current = $('#load_product_list').find('.paging_ajax_category_child li.active a').data('page');
        if(page == page_current) return false;
        var page_co = $(this).data('item');
        var total_ct = $('input[name=total_ct]').val();
        var layout = $('input[name=layout]').val();
        var sort = $('select[name=sort]').val();

        var ajax_loadding = $('.ajax_loadding');
        var header_h = $('#header1').height();
        var list_product_h = $('#load_product_list').offset().top;
        var route = $('input[name=route]').val();


        //get price value
        var input_price = [];
        $('input[name="input_price[]"]:checked').each(function(i){
            input_price[i] = $(this).val();
        });
        var url_push = '';
        if(input_price.length > 0) url_push = '&price=' + input_price.join();


        //get brand value
        var input_brand = [];
        $('input[name="input_brand[]"]:checked').each(function(i){
            input_brand[i] = $(this).val();
        });
        if(input_brand.length > 0) url_push += '&brand=' + input_brand.join();

        //get origin value
        var input_origin = [];
        $('input[name="input_origin[]"]:checked').each(function(i){
            input_origin[i] = $(this).val();
        });
        if($('#input_origin:checked').length > 0){
            input_origin = $('#input_origin:checked').val();
        } 
        if(input_origin.length > 0){
            if(Array.isArray(input_origin))
                url_push += '&origin=' + input_origin.join();
            else
                url_push += '&origin=' + input_origin;
        }

        //get product_type value
        var input_product_type = [];
        $('input[name="input_product_type[]"]:checked').each(function(i){
            $('#input_product_type').prop('checked', false); 
            input_product_type[i] = $(this).val();
        });
        if($('#input_product_type:checked').length > 0){
            input_product_type = $('#input_product_type:checked').val();
        }
        if(input_product_type.length > 0){
            if(Array.isArray(input_product_type))
                url_push += '&product_type=' + input_product_type.join();
            else
                url_push += '&product_type=' + input_product_type;
        } 

        //get skin_type value
        var input_skin_type = [];
        $('input[name="input_skin_type[]"]:checked').each(function(i){
            $('#input_skin_type').prop('checked', false); 
        });
        if($('#input_skin_type:checked').length > 0){
            input_skin_type = $('#input_skin_type:checked').val();
        }
        if(input_skin_type.length > 0){
            if(Array.isArray(input_skin_type))
                url_push += '&skin_type=' + input_skin_type.join();
            else
                url_push += '&skin_type=' + input_skin_type;
        }

        //get hair_type value
        var input_hair_type = [];
        $('input[name="input_hair_type[]"]:checked').each(function(i){
            $('#input_hair_type').prop('checked', false); 
            input_hair_type[i] = $(this).val();
        });
        if($('#input_hair_type:checked').length > 0){
            input_hair_type = $('#input_hair_type:checked').val();
        }
        if(input_hair_type.length > 0){
            if(Array.isArray(input_hair_type))
                url_push += '&hair_type=' + input_hair_type.join();
            else
                url_push += '&hair_type=' + input_hair_type;
        }

        //get action value
        var input_action = [];
        $('input[name="input_action[]"]:checked').each(function(i){
            $('#input_action').prop('checked', false); 
            input_action[i] = $(this).val();
        });
        if($('#input_action:checked').length > 0){
            input_action = $('#input_action:checked').val();
        }
        if(input_action.length > 0){
            if(Array.isArray(input_action))
                url_push += '&function=' + input_action.join();
            else
                url_push += '&function=' + input_action;
        }

        //get capacity value
        var input_capacity = [];
        $('input[name="input_capacity[]"]:checked').each(function(i){
            $('#input_capacity').prop('checked', false); 
            input_capacity[i] = $(this).val();
        });
        if($('#input_capacity:checked').length > 0){
            input_capacity = $('#input_capacity:checked').val();
        }
        if(input_capacity.length > 0){
            if(Array.isArray(input_capacity))
                url_push += '&capacity=' + input_capacity.join();
            else
                url_push += '&capacity=' + input_capacity;
        }

        //get weigh value
        var input_weigh = [];
        $('input[name="input_weigh[]"]:checked').each(function(i){
            $('#input_weigh').prop('checked', false); 
            input_weigh[i] = $(this).val();
        });
        if($('#input_weigh:checked').length > 0){
            input_weigh = $('#input_weigh:checked').val();
        }
        if(input_weigh.length > 0){
            if(Array.isArray(input_weigh))
                url_push += '&weigh=' + input_weigh.join();
            else
                url_push += '&weigh=' + input_weigh;
        }

        //get pill_number value
        var input_pill_number = [];
        $('input[name="input_pill_number[]"]:checked').each(function(i){
            $('#input_pill_number').prop('checked', false); 
            input_pill_number[i] = $(this).val();
        });
        if($('#input_pill_number:checked').length > 0){
            input_pill_number = $('#input_pill_number:checked').val();
        }
        if(input_pill_number.length > 0){
            if(Array.isArray(input_pill_number))
                url_push += '&pill_number=' + input_pill_number.join();
            else
                url_push += '&pill_number=' + input_pill_number;
        }
        if(sort != ''){
            url_push += '&sort=' + sort;
        }
        if(layout == 'list'){
            url_push += '&layout=' + layout;
        }
        if(page > 1){
            url_push += '&page=' + page;
        }

        var url_redirect = route;
        if(url_push != ''){
            var url_redirect = route + '?' + url_push.slice(1);
        }
  
        $.ajax({
            type: 'POST',
            url: 'category-page',
            data: {'level': 2, 'mod': mod, 'cate_id': cate_id, 'page_no': page, 'page_co': page_co, 'input_brand': input_brand, 'input_origin': input_origin, 'input_product_type': input_product_type, 'input_skin_type': input_skin_type, 'input_hair_type': input_hair_type, 'input_action': input_action, 'input_capacity': input_capacity, 'input_weigh': input_weigh, 'input_pill_number': input_pill_number, 'input_price': input_price, 'total_ct': total_ct, 'layout': layout},
            beforeSend: function(){
                ajax_loadding.show();
            },
            success:function(result){
                $('#load_product_list').html(result);
                $('#page_cate').find('.paging_ajax_category_child li').removeClass('active');
                $('#page_cate').find('.pajaxcate_' + page).addClass('active');
                $('input[name=page]').val(page);
                ajax_loadding.hide();
                $('html, body').animate({scrollTop:list_product_h - header_h - 63},500);
                pagination_ajax_cagegory_child();
                window.history.pushState({path:url_redirect},'',url_redirect);
            }
        })
    })
}

function product_filter(flag = false){

    if(flag == false){
        $('input[name=page]').val(1);
    }

    var ajax_loadding = $('.ajax_loadding');
    
    var mod = $('input[name=mod]').val();
    var mod_id = $('input[name=mod_id]').val();;
    var sort = $('select[name=sort]').val();
    var page = $('input[name=page]').val();
    var cate_id_search = $('input[name=cate_id_search]').val();
    var keyword = $('input[name=keyword]').val();
    var route = $('input[name=route]').val();
    var layout = $('input[name=layout]').val();

    var header_h = $('#header1').height();
    var list_product_h = $('#product_filter_select').offset().top;
    
    //get price value
    var input_price = [];
    var price_select = [];
    $('input[name="input_price[]"]:checked').each(function(i){
        input_price[i] = $(this).val();

        var priceObj = {};
        priceObj["id"] = $(this).val();
        priceObj["name"] = $(this).data('name');
        price_select[i] = JSON.stringify(priceObj);
    });
    var url_push = '';
    if(input_price.length > 0) url_push = '&price=' + input_price.join();


    //get brand value
    var input_brand = [];
    var brand_select = [];
    $('input[name="input_brand[]"]:checked').each(function(i){
        input_brand[i] = $(this).val();

        var brandObj = {};
        brandObj["id"] = $(this).val();
        brandObj["name"] = $(this).data('name');
        brand_select[i] = JSON.stringify(brandObj);
    });
    if(input_brand.length > 0) url_push += '&brand=' + input_brand.join();

    //get origin value
    var input_origin = [];
    var origin_select = [];
    $('input[name="input_origin[]"]:checked').each(function(i){
        input_origin[i] = $(this).val();

        var originObj = {};
        originObj["id"] = $(this).val();
        originObj["name"] = $(this).data('name');
        origin_select[i] = JSON.stringify(originObj);
    });
    if($('#input_origin:checked').length > 0){
        input_origin = $('#input_origin:checked').val();
        
        var originObj = {};
        originObj["id"] = 0;
        originObj["name"] = $('#input_origin:checked').data('name');
        origin_select = JSON.stringify(originObj);
        origin_select = new Array(origin_select);
    } 
    if(input_origin.length > 0){
        if(Array.isArray(input_origin))
            url_push += '&origin=' + input_origin.join();
        else
            url_push += '&origin=' + input_origin;
    }

    //get product_type value
    var input_product_type = [];
    var product_type_select = [];
    $('input[name="input_product_type[]"]:checked').each(function(i){
        $('#input_product_type').prop('checked', false); 
        input_product_type[i] = $(this).val();

        var productTypeObj = {};
        productTypeObj["id"] = $(this).val();
        productTypeObj["name"] = $(this).data('name');
        product_type_select[i] = JSON.stringify(productTypeObj);
    });
    if($('#input_product_type:checked').length > 0){
        input_product_type = $('#input_product_type:checked').val();
        
        var productTypeObj = {};
        productTypeObj["id"] = 0;
        productTypeObj["name"] = $('#input_product_type:checked').data('name');
        product_type_select = JSON.stringify(productTypeObj);
        product_type_select = new Array(product_type_select);
    }
    if(input_product_type.length > 0){
        if(Array.isArray(input_product_type))
            url_push += '&product_type=' + input_product_type.join();
        else
            url_push += '&product_type=' + input_product_type;
    } 

    //get skin_type value
    var input_skin_type = [];
    var skin_type_select = [];
    $('input[name="input_skin_type[]"]:checked').each(function(i){
        $('#input_skin_type').prop('checked', false); 
        input_skin_type[i] = $(this).val();

        var skinTypeObj = {};
        skinTypeObj["id"] = $(this).val();
        skinTypeObj["name"] = $(this).data('name');
        skin_type_select[i] = JSON.stringify(skinTypeObj);
    });
    if($('#input_skin_type:checked').length > 0){
        input_skin_type = $('#input_skin_type:checked').val();
        
        var skinTypeObj = {};
        skinTypeObj["id"] = 0;
        skinTypeObj["name"] = $('#input_skin_type:checked').data('name');
        skin_type_select = JSON.stringify(skinTypeObj);
        skin_type_select = new Array(skin_type_select);
    }
    if(input_skin_type.length > 0){
        if(Array.isArray(input_skin_type))
            url_push += '&skin_type=' + input_skin_type.join();
        else
            url_push += '&skin_type=' + input_skin_type;
    }

    //get hair_type value
    var input_hair_type = [];
    var hair_type_select = [];
    $('input[name="input_hair_type[]"]:checked').each(function(i){
        $('#input_hair_type').prop('checked', false); 
        input_hair_type[i] = $(this).val();

        var hairTypeObj = {};
        hairTypeObj["id"] = $(this).val();
        hairTypeObj["name"] = $(this).data('name');
        hair_type_select[i] = JSON.stringify(hairTypeObj);
    });
    if($('#input_hair_type:checked').length > 0){
        input_hair_type = $('#input_hair_type:checked').val();
        
        var hairTypeObj = {};
        hairTypeObj["id"] = 0;
        hairTypeObj["name"] = $('#input_hair_type:checked').data('name');
        hair_type_select = JSON.stringify(hairTypeObj);
        hair_type_select = new Array(hair_type_select);
    }
    if(input_hair_type.length > 0){
        if(Array.isArray(input_hair_type))
            url_push += '&hair_type=' + input_hair_type.join();
        else
            url_push += '&hair_type=' + input_hair_type;
    }

    //get action value
    var input_action = [];
    var action_select = [];
    $('input[name="input_action[]"]:checked').each(function(i){
        $('#input_action').prop('checked', false); 
        input_action[i] = $(this).val();

        var actionObj = {};
        actionObj["id"] = $(this).val();
        actionObj["name"] = $(this).data('name');
        action_select[i] = JSON.stringify(actionObj);
    });
    if($('#input_action:checked').length > 0){
        input_action = $('#input_action:checked').val();
        
        var actionObj = {};
        actionObj["id"] = 0;
        actionObj["name"] = $('#input_action:checked').data('name');
        action_select = JSON.stringify(actionObj);
        action_select = new Array(action_select);
    }
    if(input_action.length > 0){
        if(Array.isArray(input_action))
            url_push += '&function=' + input_action.join();
        else
            url_push += '&function=' + input_action;
    }

    //get capacity value
    var input_capacity = [];
    var capacity_select = [];
    $('input[name="input_capacity[]"]:checked').each(function(i){
        $('#input_capacity').prop('checked', false); 
        input_capacity[i] = $(this).val();

        var capacityTypeObj = {};
        capacityTypeObj["id"] = $(this).val();
        capacityTypeObj["name"] = $(this).data('name');
        capacity_select[i] = JSON.stringify(capacityTypeObj);
    });
    if($('#input_capacity:checked').length > 0){
        input_capacity = $('#input_capacity:checked').val();
        
        var capacityObj = {};
        capacityObj["id"] = 0;
        capacityObj["name"] = $('#input_capacity:checked').data('name');
        capacity_select = JSON.stringify(capacityObj);
        capacity_select = new Array(capacity_select);
    }
    if(input_capacity.length > 0){
        if(Array.isArray(input_capacity))
            url_push += '&capacity=' + input_capacity.join();
        else
            url_push += '&capacity=' + input_capacity;
    }

    //get weigh value
    var input_weigh = [];
    var weigh_select = [];
    $('input[name="input_weigh[]"]:checked').each(function(i){
        $('#input_weigh').prop('checked', false); 
        input_weigh[i] = $(this).val();

        var weighObj = {};
        weighObj["id"] = $(this).val();
        weighObj["name"] = $(this).data('name');
        weigh_select[i] = JSON.stringify(weighObj);
    });
    if($('#input_weigh:checked').length > 0){
        input_weigh = $('#input_weigh:checked').val();
        
        var weighObj = {};
        weighObj["id"] = 0;
        weighObj["name"] = $('#input_weigh:checked').data('name');
        weigh_select = JSON.stringify(weighObj);
        weigh_select = new Array(weigh_select);
    }
    if(input_weigh.length > 0){
        if(Array.isArray(input_weigh))
            url_push += '&weigh=' + input_weigh.join();
        else
            url_push += '&weigh=' + input_weigh;
    }

    //get pill_number value
    var input_pill_number = [];
    var pill_number_select = [];
    $('input[name="input_pill_number[]"]:checked').each(function(i){
        $('#input_pill_number').prop('checked', false); 
        input_pill_number[i] = $(this).val();

        var pillNumberTypeObj = {};
        pillNumberTypeObj["id"] = $(this).val();
        pillNumberTypeObj["name"] = $(this).data('name');
        pill_number_select[i] = JSON.stringify(pillNumberTypeObj);
    });
    if($('#input_pill_number:checked').length > 0){
        input_pill_number = $('#input_pill_number:checked').val();
        
        var pillNumberTypeObj = {};
        pillNumberTypeObj["id"] = 0;
        pillNumberTypeObj["name"] = $('#input_pill_number:checked').data('name');
        pill_number_select = JSON.stringify(pillNumberTypeObj);
        pill_number_select = new Array(pill_number_select);
    }
    if(input_pill_number.length > 0){
        if(Array.isArray(input_pill_number))
            url_push += '&pill_number=' + input_pill_number.join();
        else
            url_push += '&pill_number=' + input_pill_number;
    }
    if(sort != ''){
        url_push += '&sort=' + sort;
    }
    if(layout == 'list'){
        url_push += '&layout=' + layout;
    }
    if(page > 1){
        url_push += '&page=' + page;
    }

    var url_redirect = route;
    if(url_push != ''){
        var url_redirect = route + '?' + url_push.slice(1);
    }

    var data_filter = {'mod': mod, 'mod_id': mod_id, 'page': page, 'sort':sort, 'input_brand': input_brand, 'input_origin': input_origin, 'input_product_type': input_product_type, 'input_skin_type': input_skin_type, 'input_hair_type': input_hair_type, 'input_action': input_action, 'input_capacity': input_capacity, 'input_weigh': input_weigh, 'input_pill_number': input_pill_number, 'input_price': input_price, 'cate_id_search': cate_id_search, 'keyword': keyword, 'layout': layout, 'url_push': url_push.slice(1) };

    var data_filter_select = {'price_select': price_select, 'brand_select': brand_select, 'product_type_select': product_type_select, 'skin_type_select': skin_type_select, 'hair_type_select': hair_type_select, 'action_select': action_select, 'origin_select': origin_select, 'capacity_select': capacity_select, 'weigh_select': weigh_select, 'pill_number_select': pill_number_select};

    $.ajax({
        type: 'POST',
        url: 'product-filter',
        data: data_filter,
        beforeSend: function(){
            ajax_loadding.show();
        },
        success:function(res){
            $('#load_product_list').html(res);

            $.ajax({
                type: 'POST',
                url: 'product-filter-select',
                data: data_filter_select,
                success:function(result){
                    $('#product_filter_select').html(result);
                }
            });

            $.ajax({
                type: 'POST',
                url: 'product-filter-element',
                data: data_filter,
                success:function(result){
                    $('#product_filter_element').html(result);
                    toogle_scroll();
                    $(".list_th_scroll_1").mCustomScrollbar();
                }
            });

            $('html, body').animate({scrollTop:list_product_h - header_h - 15},500);
            ajax_loadding.hide();
            pagination_ajax();
            pagination_ajax_cagegory(); 
            window.history.pushState({path:url_redirect},'',url_redirect);
        }, 
        error: function(jqXHR, exception) {
            if (jqXHR.status === 0) {
                console.log('Not connect.\n Verify Network.');
            } else if (jqXHR.status == 404) {
                console.log('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                console.log('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                console.log('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                console.log('Time out error.');
            } else if (exception === 'abort') {
                console.log('Ajax request aborted.');
            } else {
                console.log('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    })
}

function product_filter_all_product_type(){
    $('input[name="input_product_type[]"]').prop('checked', false); 
    product_filter();      
}

function product_filter_all_skin_type(){
    $('input[name="input_skin_type[]"]').prop('checked', false); 
    product_filter();      
}

function product_filter_all_hair_type(){
    $('input[name="input_hair_type[]"]').prop('checked', false); 
    product_filter();      
}

function product_filter_all_origin(){
    $('input[name="input_origin[]"]').prop('checked', false); 
    product_filter();      
}

function product_filter_all_action(){
    $('input[name="input_action[]"]').prop('checked', false); 
    product_filter();      
}

function product_filter_all_capacity(){
    $('input[name="input_capacity[]"]').prop('checked', false); 
    product_filter();      
}

function product_filter_all_weigh(){
    $('input[name="input_weigh[]"]').prop('checked', false); 
    product_filter();      
}

function product_filter_all_pill_number(){
    $('input[name="input_pill_number[]"]').prop('checked', false); 
    product_filter();      
}

function remove_fiter_select(type , id){
    switch (type)
    {
        case 'price' : {
            $('#input_price_' + id).prop('checked', false);
            $("#panelPrice").find('.fa-check-square').each(function( index ) {
                var label = $(this).parent('a').data('label');
                var value = $(this).parent('a').data('value');
                var tagId = $(this).parent('a').data('id');
                if(value == id){
                    $(this).removeClass('fa-check-square');
                    $(this).addClass('fa-square-o');
                    
                    label_select.price.splice(label_select.price.indexOf(label), 1);
                    value_select.price.splice(label_select.price.indexOf(value), 1);

                    label_select_item(type, tagId);
                }
                
            });
            break;
        }

        case 'brand' : {
            $('#input_brand_' + id).prop('checked', false);
            $("#panelBrand").find('.fa-check-square').each(function( index ) {
                var label = $(this).parent('a').data('label');
                var value = $(this).parent('a').data('value');
                var tagId = $(this).parent('a').data('id');
                if(value == id){
                    $(this).removeClass('fa-check-square');
                    $(this).addClass('fa-square-o');
                    
                    label_select.price.splice(label_select.price.indexOf(label), 1);
                    value_select.price.splice(label_select.price.indexOf(value), 1);

                    label_select_item(type, tagId);
                } 
            });
            break;
        }

        case 'product_type' : {
            if(id == 0){
                $('#input_product_type').prop('checked', false);
            }else{
                $('#input_product_type_' + id).prop('checked', false);
                $("#panelProductType").find('.fa-check-square').each(function( index ) {
                    var label = $(this).parent('a').data('label');
                    var value = $(this).parent('a').data('value');
                    var tagId = $(this).parent('a').data('id');
                    if(value == id){
                        $(this).removeClass('fa-check-square');
                        $(this).addClass('fa-square-o');
                        
                        label_select.product_type.splice(label_select.product_type.indexOf(label), 1);
                        value_select.product_type.splice(label_select.product_type.indexOf(value), 1);

                        label_select_item(type, tagId);
                    } 
                });
            }
            break;
        }

        case 'skin_type' : {
            if(id == 0){
                $('#input_skin_type').prop('checked', false);
            }else{
                $('#input_skin_type_' + id).prop('checked', false);
                $("#panelSkinType").find('.fa-check-square').each(function( index ) {
                    var label = $(this).parent('a').data('label');
                    var value = $(this).parent('a').data('value');
                    var tagId = $(this).parent('a').data('id');
                    if(value == id){
                        $(this).removeClass('fa-check-square');
                        $(this).addClass('fa-square-o');
                        
                        label_select.skin_type.splice(label_select.skin_type.indexOf(label), 1);
                        value_select.skin_type.splice(label_select.skin_type.indexOf(value), 1);

                        label_select_item(type, tagId);
                    } 
                });
            }
            break;
        }

        case 'hair_type' : {
            if(id == 0){
                $('#input_hair_type').prop('checked', false);
            }else{
                $('#input_hair_type_' + id).prop('checked', false);
                $("#panelHairType").find('.fa-check-square').each(function( index ) {
                    var label = $(this).parent('a').data('label');
                    var value = $(this).parent('a').data('value');
                    var tagId = $(this).parent('a').data('id');

                    if(value == id){
                        $(this).removeClass('fa-check-square');
                        $(this).addClass('fa-square-o');
                        
                        label_select.hair_type.splice(label_select.hair_type.indexOf(label), 1);
                        value_select.hair_type.splice(label_select.hair_type.indexOf(value), 1);

                        label_select_item(type, tagId);
                    } 
                });
            }
            break;
        }

        case 'origin' : {
            if(id == 0){
                $('#input_origin').prop('checked', false);
            }else{
                $('#input_origin_' + id).prop('checked', false);
                $("#panelOrigin").find('.fa-check-square').each(function( index ) {
                    var label = $(this).parent('a').data('label');
                    var value = $(this).parent('a').data('value');
                    var tagId = $(this).parent('a').data('id');
                    if(value == id){
                        $(this).removeClass('fa-check-square');
                        $(this).addClass('fa-square-o');
                        
                        label_select.origin.splice(label_select.origin.indexOf(label), 1);
                        value_select.origin.splice(label_select.origin.indexOf(value), 1);

                        label_select_item(type, tagId);
                    } 
                });
            }
            break;
        }

        case 'action' : {
            if(id == 0){
                $('#input_action').prop('checked', false);
            }else{
                $('#input_action_' + id).prop('checked', false);
                $("#panelAction").find('.fa-check-square').each(function( index ) {
                    var label = $(this).parent('a').data('label');
                    var value = $(this).parent('a').data('value');
                    var tagId = $(this).parent('a').data('id');

                    if(value == id){
                        $(this).removeClass('fa-check-square');
                        $(this).addClass('fa-square-o');
                        
                        label_select.action.splice(label_select.action.indexOf(label), 1);
                        value_select.action.splice(label_select.action.indexOf(value), 1);

                        label_select_item(type, tagId);
                    } 
                });
            }
            break;
        }

        case 'capacity' : {
            if(id == 0){
                $('#input_capacity').prop('checked', false);
            }else{
                $('#input_capacity_' + id).prop('checked', false);
                $("#panelCapacity").find('.fa-check-square').each(function( index ) {
                    var label = $(this).parent('a').data('label');
                    var value = $(this).parent('a').data('value');
                    var tagId = $(this).parent('a').data('id');
                    if(value == id){
                        $(this).removeClass('fa-check-square');
                        $(this).addClass('fa-square-o');
                        
                        label_select.capacity.splice(label_select.capacity.indexOf(label), 1);
                        value_select.capacity.splice(label_select.capacity.indexOf(value), 1);

                        label_select_item(type, tagId);
                    } 
                });
            }
            break;
        }

        case 'weigh' : {
            if(id == 0){
                $('#input_weigh').prop('checked', false);
            }else{
                $('#input_weigh_' + id).prop('checked', false);
                $("#panelWeigh").find('.fa-check-square').each(function( index ) {
                    var label = $(this).parent('a').data('label');
                    var value = $(this).parent('a').data('value');
                    var tagId = $(this).parent('a').data('id');
                    if(value == id){
                        $(this).removeClass('fa-check-square');
                        $(this).addClass('fa-square-o');
                        
                        label_select.weigh.splice(label_select.weigh.indexOf(label), 1);
                        value_select.weigh.splice(label_select.weigh.indexOf(value), 1);

                        label_select_item(type, tagId);
                    } 
                });
            }
            break;
        }

        case 'pill_number' : {
            if(id == 0){
                $('#input_pill_number').prop('checked', false);
            }else{
                $('#input_pill_number_' + id).prop('checked', false);
                $("#panelPillNumber").find('.fa-check-square').each(function( index ) {
                    var label = $(this).parent('a').data('label');
                    var value = $(this).parent('a').data('value');
                    var tagId = $(this).parent('a').data('id');
                    if(value == id){
                        $(this).removeClass('fa-check-square');
                        $(this).addClass('fa-square-o');
                        
                        label_select.pill_number.splice(label_select.pill_number.indexOf(label), 1);
                        value_select.pill_number.splice(label_select.pill_number.indexOf(value), 1);

                        label_select_item(type, tagId);
                    } 
                });
            }
            break;
        }

    }
    product_filter();
}

function remove_fiter_all(){
    $('#product_filter_element input[type=checkbox]').prop('checked', false);
    $( "#filter-menu" ).find('.fa-check-square').each(function( index ) {
        $(this).removeClass('fa-check-square');
        $(this).addClass('fa-square-o');
    });
    $('#filter-menu').find('.filter_select ').removeClass('hasMgTop').html('');
    label_select = [];
    label_select.price = [];
    label_select.action = [];
    label_select.brand = [];
    label_select.origin = [];
    label_select.product_type = [];
    label_select.skin_type = [];
    label_select.hair_type = [];
    label_select.capacity = [];
    label_select.weigh = [];
    label_select.pill_number = [];
    value_select = [];
    value_select.price = [];
    value_select.action = [];
    value_select.brand = [];
    value_select.origin = [];
    value_select.product_type = [];
    value_select.skin_type = [];
    value_select.hair_type = [];
    value_select.capacity = [];
    value_select.weigh = [];
    value_select.pill_number = [];

    product_filter();
}

function change_layout(){
    $('.long_arrangementt').click(function() {
        $('.box_product_v2').addClass('box_product_v2_list');
        $('.square_arrangementt').removeClass('active_arrangementt');
        $('.long_arrangementt').addClass('active_arrangementt');
        $('input[name=layout]').val('list');
        product_filter();
    });
    $('.square_arrangementt').click(function() {
        $('.box_product_v2').removeClass('box_product_v2_list');
        $('.long_arrangementt').removeClass('active_arrangementt');
        $('.square_arrangementt').addClass('active_arrangementt');
        $('input[name=layout]').val('grid');
        product_filter();
    })
}

var label_select = [];
label_select.price = [];
label_select.action = [];
label_select.brand = [];
label_select.origin = [];
label_select.product_type = [];
label_select.skin_type = [];
label_select.hair_type = [];
label_select.capacity = [];
label_select.weigh = [];
label_select.pill_number = [];

var value_select = [];
value_select.price = [];
value_select.action = [];
value_select.brand = [];
value_select.origin = [];
value_select.product_type = [];
value_select.skin_type = [];
value_select.hair_type = [];
value_select.capacity = [];
value_select.weigh = [];
value_select.pill_number = [];


function filter_mobile(){
    $('.filter_mobile a').click(function(){
        $('html, body').scrollTop(0);
        $('html').addClass('state-filter');
    });

    $('#filter-menu .context-back').click(function(e){
        e.preventDefault();
        $('html').removeClass('state-filter');
    });

    $('#filter-menu .filter-clear').click(function(e){
        e.preventDefault();
        $('html').removeClass('state-filter');
    });

    $('.group_toogle').click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        if($(''+href+'').hasClass('in')){
            $(this).attr('aria-expanded', false);
            $(''+href+'').removeClass('in');
            $(''+href+'').slideUp(1000);
        }else{
            $(this).attr('aria-expanded', true);
            $(''+href+'').addClass('in');
            $(''+href+'').slideDown(1000);
        }
        
    });

    
    $( "#panelPrice" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.price.push(label_current); 
        value_select.price.push(value_current);
        label_select_item('price', tagId);
    });

    $( "#panelAction" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.action.push(label_current); 
        value_select.action.push(value_current);
        label_select_item('action', tagId);
    });

    $( "#panelBrand" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.brand.push(label_current); 
        value_select.brand.push(value_current);
        label_select_item('brand', tagId);
    });

    $( "#panelOrigin" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.origin.push(label_current); 
        value_select.origin.push(value_current);
        label_select_item('origin', tagId);
    });

    $( "#panelProductType" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.product_type.push(label_current); 
        value_select.product_type.push(value_current);
        label_select_item('product_type', tagId);
    });

    $( "#panelSkinType" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.skin_type.push(label_current); 
        value_select.skin_type.push(value_current);
        label_select_item('skin_type', tagId);
    });

    $( "#panelHairType" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.hair_type.push(label_current); 
        value_select.hair_type.push(value_current);
        label_select_item('hair_type', tagId);
    });

    $( "#panelCapacity" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.capacity.push(label_current); 
        value_select.capacity.push(value_current);
        label_select_item('capacity', tagId);
    });

    $( "#panelWeigh" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.weigh.push(label_current); 
        value_select.weigh.push(value_current);
        label_select_item('weigh', tagId);
    });

    $( "#panelPillNumber" ).find('.fa-check-square').each(function( index ) {
        var label_current = $(this).parent('a').data('label');
        var value_current = $(this).parent('a').data('value');
        var tagId = $(this).parent('a').data('id');
        label_select.pill_number.push(label_current); 
        value_select.pill_number.push(value_current);
        label_select_item('pill_number', tagId);
    });

    

    $('.select_filter_condition').click(function(e){
        e.preventDefault();

        var value = $(this).data('value');
        var label = $(this).data('label');
        var id = $(this).data('id'); 
        var group = $(this).data('group');          

        if($(this).find('i').hasClass('fa-check-square')){
            $(this).find('i').removeClass('fa-check-square');
            $(this).find('i').addClass('fa-square-o');
            switch (group) {
                case 'price':
                    label_select.price.splice(label_select.price.indexOf(label), 1);
                    value_select.price.splice(label_select.price.indexOf(value), 1);
                    break;

                case 'action':
                    label_select.action.splice(label_select.action.indexOf(label), 1);
                    value_select.action.splice(label_select.action.indexOf(value), 1);
                    break;

                case 'brand':
                    label_select.brand.splice(label_select.brand.indexOf(label), 1);
                    value_select.brand.splice(label_select.brand.indexOf(value), 1);
                    break;

                case 'origin':
                    label_select.origin.splice(label_select.origin.indexOf(label), 1);
                    value_select.origin.splice(label_select.origin.indexOf(value), 1);
                    break;

                case 'product_type':
                    label_select.product_type.splice(label_select.product_type.indexOf(label), 1);
                    value_select.product_type.splice(label_select.product_type.indexOf(value), 1);
                    break;

                case 'skin_type':
                    label_select.skin_type.splice(label_select.skin_type.indexOf(label), 1);
                    value_select.skin_type.splice(label_select.skin_type.indexOf(value), 1);
                    break;

                case 'hair_type':
                    label_select.hair_type.splice(label_select.hair_type.indexOf(label), 1);
                    value_select.hair_type.splice(label_select.hair_type.indexOf(value), 1);
                    break;

                case 'capacity':
                    label_select.capacity.splice(label_select.capacity.indexOf(label), 1);
                    value_select.capacity.splice(label_select.capacity.indexOf(value), 1);
                    break;

                case 'weigh':
                    label_select.weigh.splice(label_select.weigh.indexOf(label), 1);
                    value_select.weigh.splice(label_select.weigh.indexOf(value), 1);
                    break;

                case 'pill_number':
                    label_select.pill_number.splice(label_select.pill_number.indexOf(label), 1);
                    value_select.pill_number.splice(label_select.pill_number.indexOf(value), 1);
                    break;
            }
            
        }else{
            $(this).find('i').removeClass('fa-check-o');
            $(this).find('i').addClass('fa-check-square');
            switch (group) {
                case 'price':
                    label_select.price.push(label); 
                    value_select.price.push(value);
                    break;

                case 'action':
                    label_select.action.push(label); 
                    value_select.action.push(value);
                    break;

                case 'brand':
                    label_select.brand.push(label); 
                    value_select.brand.push(value);
                    break;

                case 'origin':
                    label_select.origin.push(label); 
                    value_select.origin.push(value);
                    break;

                case 'product_type':
                    label_select.product_type.push(label); 
                    value_select.product_type.push(value);
                    break;

                case 'skin_type':
                    label_select.skin_type.push(label); 
                    value_select.skin_type.push(value);
                    break;

                case 'hair_type':
                    label_select.hair_type.push(label); 
                    value_select.hair_type.push(value);
                    break;

                case 'capacity':
                    label_select.capacity.push(label); 
                    value_select.capacity.push(value);
                    break;

                case 'weigh':
                    label_select.weigh.push(label); 
                    value_select.weigh.push(value);
                    break;

                case 'pill_number':
                    label_select.pill_number.push(label); 
                    value_select.pill_number.push(value);
                    break;
            }
 
        }

        label_select_item(group, id);

        
    });


 $('.filter-apply').click(function(e){
        e.preventDefault();
        var url_push = '';
        if(value_select.price.length > 0){
            value_select.price.sort(function(a, b){return a-b});
            url_push += '&price=' + value_select.price.toString();
        } 
        if(value_select.action.length > 0){
            value_select.action.sort(function(a, b){return a-b});
            url_push += '&function=' + value_select.action.toString();
        } 
        if(value_select.brand.length > 0) {
            value_select.brand.sort(function(a, b){return a-b});
            url_push += '&brand=' + value_select.brand.toString();
        }
        if(value_select.origin.length > 0) {
            value_select.origin.sort(function(a, b){return a-b});
            url_push += '&origin=' + value_select.origin.toString();
        }
        if(value_select.product_type.length > 0) {
            value_select.product_type.sort(function(a, b){return a-b});
            url_push += '&product_type=' + value_select.product_type.toString();
        }
        if(value_select.skin_type.length > 0) {
            value_select.skin_type.sort(function(a, b){return a-b});
            url_push += '&skin_type=' + value_select.skin_type.toString();
        }
        if(value_select.hair_type.length > 0) {
            value_select.hair_type.sort(function(a, b){return a-b});
            url_push += '&hair_type=' + value_select.hair_type.toString();
        }
        if(value_select.capacity.length > 0) {
            value_select.capacity.sort(function(a, b){return a-b});
            url_push += '&capacity=' + value_select.capacity.toString();
        }
        if(value_select.weigh.length > 0) {
            value_select.weigh.sort(function(a, b){return a-b});
            url_push += '&weigh=' + value_select.weigh.toString();
        }
        if(value_select.pill_number.length > 0) {
            value_select.pill_number.sort(function(a, b){return a-b});
            url_push += '&pill_number=' + value_select.pill_number.toString();
        }

        var route = $('input[name=route]').val();
        var url_redirect = route;
        if(url_push != ''){
            var url_redirect = route + '?' + url_push.slice(1);
        }
        if(url_push != '') window.location.href = url_redirect;
    });
}


function label_select_item(group, id) {
    var label_select_price_txt = '';
    var label_select_action_txt = '';
    var label_select_brand_txt = '';
    var label_select_origin_txt = '';
    var label_select_product_type_txt = '';
    var label_select_skin_type_txt = '';
    var label_select_hair_type_txt = '';
    var label_select_capacity_txt = '';
    var label_select_weigh_txt = '';
    var label_select_pill_number_txt = '';
    var x;

    switch (group) {
        case 'price':
            label_select_group = label_select.price; 
            break;

        case 'action':
            label_select_group = label_select.action; 
            break;

        case 'brand':
            label_select_group = label_select.brand; 
            break;

        case 'origin':
            label_select_group = label_select.origin; 
            break;

        case 'product_type':
            label_select_group = label_select.product_type; 
            break;

        case 'skin_type':
            label_select_group = label_select.skin_type; 
            break;

        case 'hair_type':
            label_select_group = label_select.hair_type; 
            break;

        case 'capacity':
            label_select_group = label_select.capacity; 
            break;

        case 'weigh':
            label_select_group = label_select.weigh; 
            break;

        case 'pill_number':
            label_select_group = label_select.pill_number; 
            break;
    }

    for (x in label_select_group) {
        switch (group) {
            case 'price':
                if(x > 0) label_select_price_txt += ', ' + label_select_group[x];else label_select_price_txt += label_select_group[x];
                break;

            case 'action':
                if(x > 0) label_select_action_txt += ', ' + label_select_group[x];else label_select_action_txt += label_select_group[x];
                break;

            case 'brand':
                if(x > 0) label_select_brand_txt += ', ' + label_select_group[x];else label_select_brand_txt += label_select_group[x];
                break;

            case 'origin':
                if(x > 0) label_select_origin_txt += ', ' + label_select_group[x];else label_select_origin_txt += label_select_group[x];
                break;

            case 'product_type':
                if(x > 0) label_select_product_type_txt += ', ' + label_select_group[x];else label_select_product_type_txt += label_select_group[x];
                break;

            case 'skin_type':
                if(x > 0) label_select_skin_type_txt += ', ' + label_select_group[x];else label_select_skin_type_txt += label_select_group[x];
                break;

            case 'hair_type':
                if(x > 0) label_select_hair_type_txt += ', ' + label_select_group[x];else label_select_hair_type_txt += label_select_group[x];
                break;

            case 'capacity':
                if(x > 0) label_select_capacity_txt += ', ' + label_select_group[x];else label_select_capacity_txt += label_select_group[x];
                break;

            case 'weigh':
                if(x > 0) label_select_weigh_txt += ', ' + label_select_group[x];else label_select_weigh_txt += label_select_group[x];
                break;

            case 'pill_number':
                if(x > 0) label_select_pill_number_txt += ', ' + label_select_group[x];else label_select_pill_number_txt += label_select_group[x];
                break;
        }
    }
   
    switch (group) {
        case 'price':
            $(''+id+'').addClass('hasMgTop').html(label_select_price_txt);
            if(label_select_price_txt == '') $(''+id+'').removeClass('hasMgTop'); 
            break;

        case 'action':
            $(''+id+'').addClass('hasMgTop').html(label_select_action_txt);
            if(label_select_action_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;

        case 'brand':
            $(''+id+'').addClass('hasMgTop').html(label_select_brand_txt);
            if(label_select_brand_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;

        case 'origin':
            $(''+id+'').addClass('hasMgTop').html(label_select_origin_txt);
            if(label_select_origin_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;

        case 'product_type':
            $(''+id+'').addClass('hasMgTop').html(label_select_product_type_txt);
            if(label_select_product_type_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;

        case 'skin_type':
            $(''+id+'').addClass('hasMgTop').html(label_select_skin_type_txt);
            if(label_select_skin_type_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;

        case 'hair_type':
            $(''+id+'').addClass('hasMgTop').html(label_select_hair_type_txt);
            if(label_select_hair_type_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;

        case 'capacity':
            $(''+id+'').addClass('hasMgTop').html(label_select_capacity_txt);
            if(label_select_capacity_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;

        case 'weigh':
            $(''+id+'').addClass('hasMgTop').html(label_select_weigh_txt);
            if(label_select_weigh_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;

        case 'pill_number':
            $(''+id+'').addClass('hasMgTop').html(label_select_pill_number_txt);
            if(label_select_pill_number_txt == '') $(''+id+'').removeClass('hasMgTop');
            break;
    }
}
