$(document).ready(function() {
    brand_char();
    // send_order();
    call_back();
    advisory_request();
    advisory_request_one();
    toogle_comment();
    forgot_password();
    newsletter();
    show_cart_mobile();

    $(".box_category_home_product .ul_list_recieve_text").mCustomScrollbar();

    $('.img_click_minus').click(function() {
        var num = $('.number_cart_input').val();
        var num1 = parseInt(num);
        var input = $('.number_cart_input');
        if (num1 > 1) {
            input.val(--num1);
        }
    });
    $('.img_click_plus').click(function() {
        var num = $('.number_cart_input').val();
        var num1 = parseInt(num);
        var input = $('.number_cart_input');
        input.val(++num1);
    });

    $('.img_click_minus_one').click(function() {
    	var rowid = $(this).data('rowid');
        var input = $('#qty_' + rowid);
        var num = input.val();
        var qty = parseInt(num);       
        if (qty > 1) {
        	qty = --qty;
            input.val(qty);

            $.ajax({
				type: 'POST',
				url: 'update-cart',
				data: {'rowid': rowid, 'qty': qty},
				success:function(){
					window.location.reload();
				}
			})
        }
    });
    $('.img_click_plus_one').click(function() {   	
        var rowid = $(this).data('rowid');	        
        var input = $('#qty_' + rowid);

        var num = input.val();
        var qty = parseInt(num);       
    	qty = ++qty;
        input.val(qty);

        $.ajax({
			type: 'POST',
			url: 'update-cart',
			data: {'rowid': rowid, 'qty': qty},
			success:function(){
				window.location.reload();
			}
		})
    });

    //$('.img_detail_name_2 > ul').find('div, span, p, b, i, h1, h2, h3, h4, h5, h6').attr('style', '');

    $('.view_more_cate').click(function(){
        if($(this).find('span').hasClass('fa-plus')){
            $(this).find('span').attr('class', 'fa fa-minus');
        }else{
            $(this).find('span').attr('class', 'fa fa-plus');
        }
        $('#view_more_cate').toggle();
    });

    $('#wrapper_tab a.tab_link').hover(function() {
        if ($(this).attr('class') != $('#wrapper_tab').attr('class') ) {
            $('#wrapper_tab').attr('class',$(this).attr('class'));
        }
        return false;
    });

    $('input[name=s_cate_select]').click(function () {
        if($(this).is(':checked')){
            var cate_id = $(this).val();
            $('#frm_search_mobile').find('input[name=s_cate]').val(cate_id);
        }else{
            $('#frm_search_mobile').find('input[name=s_cate]').val(0);
        }
    })

});

$(window).scroll(function() {
    show_cart_mobile();
});

(function() {
    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function(el) {
        new SelectFx(el);
    });
})();

var ajax_loadding = $('.ajax_loadding');

function add_to_cart(id){
    var qty = 1;
	$.ajax({
		type: 'POST',
		url: 'add-to-cart',
		data: {'id': id, 'qty': 1},
		success:function(){
			document.location = 'gio-hang.html';
		}
	})
}

function add_to_cart_pr_detail(id){
    var province_id = $('select[name="province_id"]').val();
    var district_id = $('select[name="district_id"]').val();
    var param = '';
    if(province_id != '') param = param + '?province_id=' + province_id;
    if(district_id != '') param = param + '&district_id=' + district_id;
    var qty_input = $('input[name="qty"]');
    var qty = 1;
    if(qty_input) qty = qty_input.val();
    $.ajax({
        type: 'POST',
        url: 'add-to-cart',
        data: {'id': id, 'qty': qty},
        success:function(){
            document.location = 'gio-hang.html' + param;
        }
    })
}


function delete_cart(rowid){
	$.ajax({
		type: 'POST',
		url: 'delete-cart',
		data: {'rowid': rowid},
		success:function(){
			window.location.reload();
		}
	})
}

function load_district(){
    var province_id = $('select[name="province_id"]').val();
    $.ajax({
        type: 'POST',
        url: 'load-district',
        data: {'province_id': province_id},
        success:function(html){
            $('select[name="district_id"]').html(html);
        }
    })
}

function brand_char(){
    $('.brand_char').click(function() {
        $('.brand_char').removeClass('active');
        $(this).addClass('active');
        var char = $(this).data('char');
        var category_id = $('select[name="category_brand"]').val();
        $.ajax({
            type: 'POST',
            url: 'brand-character',
            data: {'category_id': category_id, 'char': char},
            beforeSend: function(){
                ajax_loadding.show();
            },
            success:function(res){
                $('.list_all_thuonghieu').html(res);
                ajax_loadding.hide();
            }
        })
    });
}

function brand_char_(){
    $('.brand_char').click(function() {
        $('.brand_char').removeClass('active');
        $(this).addClass('active');
        var char = $(this).data('char');
        var product_type_id = $('select[name="product_type_brand"]').val();
        $.ajax({
            type: 'POST',
            url: 'brand-character',
            data: {'product_type_id': product_type_id, 'char': char},
            beforeSend: function(){
                ajax_loadding.show();
            },
            success:function(res){
                $('.list_all_thuonghieu').html(res);
                ajax_loadding.hide();
            }
        })
    });
}

function load_product_type_brand(){
    var product_type_id = $('select[name="product_type_brand"]').val();
    var char = '';
    if($('.brand_char').hasClass('active'))
        var char = $('.brand_char.active').data('char');

    $.ajax({
        type: 'POST',
        url: 'brand-character',
        data: {'product_type_id': product_type_id, 'char': char},
        success:function(res){
            $('.list_all_thuonghieu').html(res);
        }
    })
    
}

function load_category_brand(){
    var category_id = $('select[name="category_brand"]').val();
    var char = '';
    if($('.brand_char').hasClass('active'))
        var char = $('.brand_char.active').data('char');

    $.ajax({
        type: 'POST',
        url: 'brand-character',
        data: {'category_id': category_id, 'char': char},
        beforeSend: function(){
            ajax_loadding.show();
        },
        success:function(res){
            $('.list_all_thuonghieu').html(res);
            ajax_loadding.hide();
        }
    })
    
}

function enable_edit(input_name) {
    value = $("input[name="+ input_name +"]").attr("readonly");
    if( value == "readonly") {
        $("input[name="+ input_name +"]").attr("readonly", false); 
    } else {
        $("input[name="+ input_name +"]").attr("readonly", true);
    }
}

function validateSearch(){
    var cat = document.getElementById("s_cate").value;
    var keyword = document.getElementById("s_keyword").value;
    var str= keyword.toLowerCase();
    str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
    str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
    str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
    str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
    str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
    str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
    str= str.replace(/đ/g,"d");
    str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
    str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
    str= str.replace(/^\-+|\-+$/g,"");//cắt bỏ ký tự - ở đầu và cuối chuỗi

    if(str==''){
        alert('Bạn chưa nhập từ khóa');
        return false;
    }

    window.location.href="tim-kiem/cat/"+cat+"/q/"+str+"/";

    return false;
}

function toogle_comment(){
    $('.list_commnet').show();
    $('.comment_danhgia').toggle(
        function() {

            $('.list_commnet').slideDown("fast");
            $('.comment_danhgia p').addClass('active_p')
        },
        function() {

            $('.list_commnet').slideUp("fast");
            $('.active_p').removeClass('active_p')
        }
    );
}

function toogle_reaply(comment_id){
    $('#form_send_comment_' + comment_id).slideToggle();
    $('#comment_content_' + comment_id).focus();
}

function goBack() {
    window.history.back();
}

function myFunction() {
    window.print();
}

function send_order(){
    $('#btn_order').click(function(e) {
        e.preventDefault();

        var name = $('input[name=full_name]').val();
        var phone = $('input[name=phone]').val();
        var email = $('input[name=email]').val();
        var address = $('input[name=address]').val();
        var note = $('input[name=note]').val();
        var emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;
        var phone_regex=/^0([0-9]{9,10})$/;

        if(name == ''){
            $('#errorOrderName').html('<div class="show_error">Bạn phải nhập họ tên.</div>');
        }else{
            $('#errorOrderName').html('');
        }

        if(phone == ''){
            $('#errorOrderPhone').html('<div class="show_error">Bạn phải nhập số điện thoại.</div>');
        }else if(!phone_regex.test(phone)){
            $('#errorOrderPhone').html('<div class="show_error">Số điện thoại không hợp lệ.</div>');
        }else{
            $('#errorOrderPhone').html('');
        }

        if(address == ''){
            $('#errorOrderAdress').html('<div class="show_error">Bạn phải nhập địa chỉ.</div>');
        }else{
            $('#errorOrderAdress').html('');
        }

        if(name == '' || phone == '' || !phone_regex.test(phone) || address == '') return false;
        $.ajax({
            url: 'order-checkout',
            type: 'POST',
            dataType: 'json',
            data: $('#frm_order').serialize(),
             beforeSend: function(){
                $('#btn_order').val('Đang gửi...');
            },
            success: function(result) {
                $.ajax({
                    url: 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536',
                    type: 'POST',
                    data: {'mailto': result.mailto, 'mailcc': result.mailcc, 'title': result.title, 'content': result.content}
                });
                window.location.href = 'don-hang/' + result.code;
            }, 
            error: function(jqXHR, exception) {
                if (jqXHR.status === 0) {
                    console.log('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    console.log('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    console.log('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    console.log('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    console.log('Time out error.');
                } else if (exception === 'abort') {
                    console.log('Ajax request aborted.');
                } else {
                    console.log('Uncaught Error.\n' + jqXHR.responseText);
                }
            }           
        });
    });
    
}

function call_back(){
    $('#submit_callback').click(function(e) {
        e.preventDefault();
        var phone_regex=/^0([0-9]{9,10})$/;

        var phone = $('input[name=callback_phone]').val();
        var id = $(this).data('id');

        if(phone == ''){
            $('#error_callback').html('<div class="show_error">Bạn chưa nhập số điện thoại</div>');
        }else if(!phone_regex.test(phone)){
            $('#error_callback').html('<div class="show_error">Số điện thoại không hợp lệ.</div>');
        }else{
            $('#error_callback').html('');
        }

        if(phone != '' && phone_regex.test(phone)){
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "call-back",
                data: {'id':id,'phone':phone,'link':window.location.href},
                success:function(result){
                    if(result.message == true){
                        $('#error_callback').html('<div class="show_error">Cảm ơn bạn. Chúng tôi sẽ gọi lại sau ít phút.</div>');
                    }else{
                        $('#error_callback').html('<div class="show_error">Đã xảy ra lỗi. Vui lòng thử lại.</div>');
                    }
                    $('input[name=callback_phone]').val('');

                    $.ajax({
                        url: 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536',
                        type: 'POST',
                        data: {'mailto': result.mailto, 'mailcc': result.mailcc, 'title': result.title, 'content': result.content}
                    });

                }
            });
        }

    })
}

function advisory_request(){
    $('#btn_adv_request').click(function(e) {
        e.preventDefault();
        var phone_regex=/^0([0-9]{9,10})$/;
        var emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;

        var phone = $('input[name=adv_phone]').val();
        var email = $('input[name=adv_email]').val();
        var id = $(this).data('id');

        if(phone == ''){
            $('#errorAdvPhone').html('<div class="show_error">Bạn chưa nhập số điện thoại</div>');
        }else if(!phone_regex.test(phone)){
            $('#errorAdvPhone').html('<div class="show_error">Số điện thoại không hợp lệ.</div>');
        }else{
            $('#errorAdvPhone').html('');
        }

        if(email !='' && !emailRegExp.test(email)){
            $('#errorAdvEmail').html('<div class="show_error">Định dạng Email không hợp lệ.</div>');
            
        }else{
            $('#errorAdvEmail').html('');
        }

        if(phone == '' || !phone_regex.test(phone) || (email !='' && !emailRegExp.test(email))) return false;
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "advisory-request",
                data: $('#form_support').serialize(),
                beforeSend: function(){
                    $('#btn_adv_request').val('Đang gửi...');
                },
                success:function(result){
                    if(result.message == true){
                        $('#add_success_mgs').html('<span>Cảm ơn bạn. Chúng tôi đã tiếp nhận yêu cầu tư vấn của bạn.</span>');
                     }else{
                        $('#add_success_mgs').html('<span>Đã xảy ra lỗi. Vui lòng thử lại.</span>');
                     }
                     $('#btn_adv_request').val('Gửi');
                     $('#form_support').find("input[type=text], input[type=email], textarea, select").val("");;
                
                     $.ajax({
                        url: 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536',
                        type: 'POST',
                        data: {'mailto': result.mailto, 'mailcc': result.mailcc, 'title': result.title, 'content': result.content}
                    });
                }
            });
       

    })
}

function advisory_request_one(){
    $('#btn_adv_request_one').click(function(e) {
        e.preventDefault();
        var phone_regex=/^0([0-9]{9,10})$/;
        var emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;

        var phone = $('input[name=adv_phone]').val();
        var email = $('input[name=adv_email]').val();
        var id = $(this).data('id');

        if(phone == ''){
            $('#errorAdvPhone').html('<div class="show_error mgrb_10">Bạn chưa nhập số điện thoại</div>');
        }else if(!phone_regex.test(phone)){
            $('#errorAdvPhone').html('<div class="show_error mgrb_10">Số điện thoại không hợp lệ.</div>');
        }else{
            $('#errorAdvPhone').html('');
        }

        if(email !='' && !emailRegExp.test(email)){
            $('#errorAdvEmail').html('<div class="show_error mgrb_10">Định dạng Email không hợp lệ.</div>');
            
        }else{
            $('#errorAdvEmail').html('');
        }

        if(phone == '' || !phone_regex.test(phone) || (email !='' && !emailRegExp.test(email))) return false;
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "advisory-request",
                data: $('#form_tuvan').serialize(),
                beforeSend: function(){
                    $('#btn_adv_request_one').val('Đang gửi...');
                },
                success:function(result){
                    if(result.message == true){
                        $('#add_success_mgs').html('<span>Cảm ơn bạn. Chúng tôi đã tiếp nhận yêu cầu tư vấn của bạn.</span>');
                     }else{
                        $('#add_success_mgs').html('<span>Đã xảy ra lỗi. Vui lòng thử lại.</span>');
                     }
                     $('#btn_adv_request_one').val('Gửi');
                     $('#form_tuvan').find("input[type=text], input[type=email], textarea, select").val("");

                     $.ajax({
                        url: 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536',
                        type: 'POST',
                        data: {'mailto': result.mailto, 'mailcc': result.mailcc, 'title': result.title, 'content': result.content}
                    });
                }
            });
       

    })
}

function send_comment(product_id) {

    var comment_name = $('#form_send_comment input[name=comment_name]').val();
    var comment_content = $('#form_send_comment textarea[name=comment_content]').val();
    if(comment_name == ''){
        $('#errorCommentName').html('<div class="show_error">Bạn phải nhập họ tên.</div>');
        $('#form_send_comment input[name=comment_name]').focus();
    }else{
        $('#errorCommentName').html('');
    }

    if(comment_content == ''){
        $('#errorCommentContent').html('<div class="show_error">Bạn phải nhập nội dung.</div>');
        $('#form_send_comment textarea[name=comment_content]').focus();
    }else{
        $('#errorCommentContent').html('');
    }

    if(comment_name == '' || comment_content == '') return false;
    $.ajax({
        type: 'POST',
        url: 'send-comment',
        dataType: 'html',
        data: {'product_id': product_id, 'name': comment_name, 'content': comment_content},
        success:function(mgs){
            $('#mgs_comment').html('<div class="txt_success">' + mgs + '</div>');
            $('#form_send_comment').find("input[type=text], input[type=email], textarea, select").val("");;
        }
    });
    return false;
}

function answer_comment(comment_id, product_id) {
    var frm_name_id = $('#form_send_comment_' + comment_id);
    var comment_name = frm_name_id.find('input[name=comment_name]').val();
    var comment_content = frm_name_id.find('textarea[name=comment_content]').val();

    if(comment_name == ''){
        $('#errorCommentName' + comment_id).html('<div class="show_error">Bạn phải nhập họ tên.</div>');
        frm_name_id.find('input[name=comment_name]').focus();
    }else{
        $('#errorCommentName' + comment_id).html('');
    }

    if(comment_content == ''){
        $('#errorCommentContent' + comment_id).html('<div class="show_error">Bạn phải nhập nội dung.</div>');
        frm_name_id.find('input[name=comment_name]' + comment_id).focus();
    }else{
        $('#errorCommentContent' + comment_id).html('');
    }

    if(comment_name == '' || comment_content == '') return false;

    $.ajax({
        type: 'POST',
        url: 'answer-comment',
        dataType: 'html',
        data: {'product_id': product_id, 'comment_id': comment_id, 'name': comment_name, 'content': comment_content},
        success:function(mgs){
            $('#mgs_comment_' + comment_id).html('<div class="txt_success">' + mgs + '</div>');
            frm_name_id.find("input[type=text], input[type=email], textarea, select").val("");
        }
    });
    return false;
}

function forgot_password(){
    $('#btn_forgot').click(function (e) {
        e.preventDefault();
        var email = $('input[name=email]').val();

        var emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;

        if(email == ''){
            $('#errorEmailForgot').html('<div class="show_error">Bạn phải nhập Email.</div>');
        }else if(!emailRegExp.test(email)){
            $('#errorEmailForgot').html('<div class="show_error">Email không hợp lệ.</div>');
        }else{
            $('#errorEmailForgot').html('');
        }

        if(email == '' || !emailRegExp.test(email)) return false;

        $.ajax({
            url: 'forgot-password-process',
            type: 'POST',
            dataType: 'json',
            data: $('#frm_forgot_password').serialize(),
            success: function(result) {

                if(result.error != ''){
                    $('#errorEmailForgot').html('<div class="show_error">' + result.error + '</div>');
                    return false;
                }
                $.ajax({
                    url: 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536',
                    type: 'POST',
                    data: {'mailto': result.mailto, 'mailcc': result.mailcc, 'title': result.title, 'content': result.content}
                });
                $('#message_change_pass').html('<div class="txt_success">' + result.message + '</div>');
            }, 
            error: function(jqXHR, exception) {
                if (jqXHR.status === 0) {
                    console.log('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    console.log('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    console.log('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    console.log('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    console.log('Time out error.');
                } else if (exception === 'abort') {
                    console.log('Ajax request aborted.');
                } else {
                    console.log('Uncaught Error.\n' + jqXHR.responseText);
                }
            }           
        });
    })
}

function list_feedback(){
    $('#list_feedback').load('list-feedback');
}

function send_feedback() {

    var comment_content = $('#form_send_comment textarea[name=comment_content]').val();

    if(comment_content == ''){
        $('#errorCommentContent').html('<div class="show_error">Bạn phải nhập nội dung.</div>');
        $('#form_send_comment textarea[name=comment_content]').focus();
    }else{
        $('#errorCommentContent').html('');
    }

    if(comment_content == '') return false;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'send-feedback',
        data: {'content': comment_content},
        success:function(result){
            $('#mgs_comment').html('<div class="txt_success">' + result.message + '</div>');
            $('#form_send_comment').find("input[type=text], input[type=email], textarea, select").val("");
            list_feedback();

            $.ajax({
                url: 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536',
                type: 'POST',
                data: {'mailto': result.mailto, 'mailcc': result.mailcc, 'title': result.title, 'content': result.content}
            });
        }, 
            error: function(jqXHR, exception) {
                if (jqXHR.status === 0) {
                    console.log('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    console.log('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    console.log('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    console.log('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    console.log('Time out error.');
                } else if (exception === 'abort') {
                    console.log('Ajax request aborted.');
                } else {
                    console.log('Uncaught Error.\n' + jqXHR.responseText);
                }
            } 
    });
    return false;
}

function answer_feedback(comment_id) {
    var frm_name_id = $('#form_send_comment_' + comment_id);
    var comment_content = frm_name_id.find('textarea[name=comment_content]').val();

    if(comment_content == ''){
        $('#errorCommentContent' + comment_id).html('<div class="show_error">Bạn phải nhập nội dung.</div>');
        frm_name_id.find('input[name=comment_name]' + comment_id).focus();
    }else{
        $('#errorCommentContent' + comment_id).html('');
    }

    if(comment_content == '') return false;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'answer-feedback',
        data: {'comment_id': comment_id,'content': comment_content},
        success:function(result){
            list_feedback();
            $.ajax({
                url: 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536',
                type: 'POST',
                data: {'mailto': result.mailto, 'mailcc': result.mailcc, 'title': result.title, 'content': result.content}
            });
        }
    });
    return false;
}

function newsletter(){
    $('#btn_email_letter').click(function(e) {
        e.preventDefault();
        var emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;

        var email = $('input[name=email_letter]').val();

        if(email == ''){
            $('#errorNewsletter').html('<div class="show_error_white">Bạn chưa nhập Email</div>');
        }else if(!emailRegExp.test(email)){
            $('#errorNewsletter').html('<div class="show_error_white">Email không hợp lệ.</div>');
        }else{
            $('#errorNewsletter').html('');
        }

        if(email == '' || !emailRegExp.test(email)) return false;

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "newsletter",
            data: {'email':email},
            success:function(mgs){
                $('#errorNewsletter').html(mgs);
                $('input[name=email_letter]').val('');
            }
        });

    })
}
      

function fbLogin() {
    FB.login(function(response) {
        if (response.authResponse) {
         console.log('Welcome!  Fetching your information.... ');
         FB.api('/me', {fields: 'id,name,email,birthday,gender'}, function(info) {
            $.ajax({
                type: 'POST',
                url: 'login-facebook',
                data: {'id': info.id, 'email': info.email, 'name': info.name, 'gender': info.gender, 'birthday': info.birthday},
                success:function(url){
                    window.location.href= url; 
                }
            });
         });
        } else {
         console.log('User cancelled login or did not fully authorize.');
        }
    }, { scope: 'email,user_birthday' });
}

function show_cart_mobile() {
    if($('.box_other_product').length > 0 && $(window).width() < 992){
        var box_other_product_h = $('.box_other_product').offset().top;
        var window_h = $(window).height();
        if($(window).scrollTop() > (box_other_product_h - window_h)) {
            $('#fixed_cart_phone').hide();
        }else{
            $('#fixed_cart_phone').show();
        }
    }
}

