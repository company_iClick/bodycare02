$(document).ready(function(){
    menu_scroll();
   // menu();
    menu_left();
    slider_pr_home();
    remove_menu();
    menu_scroll1();
    slide_comment();
    slide_banner();
    slide_news();
   // click_giohang();
    
})

function click_giohang(){

    $('.giohang').click(function(){
        window.location.href='gio-hang.html';
    });

}

function menu_scroll1(){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 500) {
            $('.box_tuvan_detail').addClass('fixed1');
            // $('#menu_header').addClass('fixed1');
        } else {
            $('.box_tuvan_detail').removeClass('fixed1');
            // $('#menu_header').removeClass('fixed1');
        }
    });
}
function menu_scroll(){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#header1').addClass('fixed');
            // $('#menu_header').addClass('fixed1');
        } else {
            $('#header1').removeClass('fixed');
            // $('#menu_header').removeClass('fixed1');
        }
    });
}
function menu(){
    ddsmoothmenu.init({
        mainmenuid: "smoothmenu1", //menu DIV id
        orientation: 'v', //Horizontal or vertical menu: Set to "h" or "v"
        classname: 'ddsmoothmenu', //class added to menu's outer DIV
        //customtheme: ["#1c5a80", "#18374a"],
        contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
    })
}
function menu_left(){

        $('.left_menu_home .menu_li_left').each(function (index, el) {
            var height_sub = $(this).children('.sub-list-menu').height();
            $(this).children('.sub-list-menu').css({'bottom': -height_sub});
            $(this).hover(function () {
                $(this).children('.sub-list-menu').fadeIn('500');

            }, function () {
                $('.sub-list-menu').fadeOut();

            });
        });
        $('.wrapper_search .form-text').width($('.pager_wrapper').width() - $('.wrapper_search select').width());

        /** Expand menu **/
        $('.left_menu_home .menu_li_left:not(".dropdown")').each(function (index, el) {
            index++;
            if (index >= 11) {
                $(this).hide();

            }
        });
        $('.left_menu_home .dropdown').toggle(function () {
            if ($('.left_menu_home .menu_li_left').show()

               // $('.dropdown a img').removeClass('img_a_1');
            ) {
                $(this).show();
                $('.dropdown a img').addClass('img_a_1');
            }
        }, function () {
            var $count = $('.left_menu_home .menu_li_left').size();

            $('.left_menu_home .menu_li_left:not(".dropdown")').slice(10, $count).hide();
             $('.dropdown a img').removeClass('img_a_1');
        });
}

function slider_pr_home(){
    $('.product_slider_wp').bxSlider({
        auto: true,
        slideWidth: 109,
        minSlides: 4,
        maxSlides: 5,
        moveSlides: 4,
        slideMargin: 12,
        pager: false, controls: true

    });
}


function remove_menu() {
    var windowWidth = $(window).width();
    if (windowWidth < 1170) {
        $(".sub_menu").remove();
    } else {
        $(".menu_ul").remove();
    }
}

function slide_comment(){
    $('#slider_comment').bxSlider({
        auto: true,
        // slideWidth: 1200,
        mode: 'fade',
        minSlides: 1,
        pause: 10000,
        maxSlides: 5,
        moveSlides: 1,
        //slideMargin: 20,
        pager: true, controls: false

    });
}
function slide_banner(){
    $('.right_conent1_1').bxSlider({
        auto: true,
        slideWidth: 603,
        mode: 'fade',
        minSlides: 1,
        pause: 10000,
        maxSlides: 5,
        moveSlides: 1,
        //slideMargin: 20,
        pager: true, controls: false

    });
}
function slide_news(){
    $('.list_banner_news').bxSlider({
        auto: true,
        slideWidth: 853,
        mode: 'fade',
        minSlides: 1,
        pause: 10000,
        maxSlides: 5,
        moveSlides: 1,
        //slideMargin: 20,
        pager: true, controls: false

    });
}
