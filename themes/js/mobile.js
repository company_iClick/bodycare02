$(document).ready(function(){
    $('.menu_mobi_click_add').click(function(){
        $('body').removeClass('body_1');
        $('.left-container').removeClass('open');
        $('.main-container').removeClass('open');
        $("#c-header-mobile__menu").attr("data",1);
    });

    $('.mobile__menu__1').click(function() {
        $('body').removeClass('body_1');
    });
    $('.main-container').click(function() {
        $("#c-header-mobile__menu").attr("data", 1);
        $('body').removeClass('body_1');
        $('#c-header-mobile__menu').removeClass('mobile__menu__1');
    });

    $('#c-header-mobile__menu').click(function() {
        $('.c-header-mobile__menu-icon').addClass('mobile__menu__1');
    });

    toogle_menu_mb();
    toogle_menu_page_mb();
    sort_mobile();
});

function hide_menu(){
    var data = $("#c-header-mobile__menu").attr("data");       
    if(data==1){
        $('body').addClass('body_1');
        $("#c-header-mobile__menu").attr("data",2);

    }else{
       $('body').removeClass('body_1');
       $("#c-header-mobile__menu").attr("data",1);
    }
}

function toogle_menu_mb(){
    $('.toogle_cate').click(function(){
        var id = $(this).data('id');
        if($('#cate_' + id).hasClass('cate_open')){
            $('#cate_' + id).fadeOut(500);
            $('#cate_' + id).removeClass('cate_open');  
            $(this).removeClass('fa-minus');
            $(this).addClass('fa-plus');  
        }else{
            $('#cate_' + id).fadeIn(500);
            $('#cate_' + id).addClass('cate_open');
            $(this).removeClass('fa-plus');
            $(this).addClass('fa-minus');
        }
        
    });

    $('.toogle_cc_cate').click(function(){
        var id = $(this).data('id');
        if($('#cc_cate_' + id).hasClass('cate_open')){
            $('#cc_cate_' + id).fadeOut(500);
            $('#cc_cate_' + id).removeClass('cate_open');  
            $(this).removeClass('fa-minus');
            $(this).addClass('fa-plus');  
        }else{
            $('#cc_cate_' + id).fadeIn(500);
            $('#cc_cate_' + id).addClass('cate_open');
            $(this).removeClass('fa-plus');
            $(this).addClass('fa-minus');
        }
        
    });
}

function toogle_menu_page_mb(){
    $('.toogle_cate_page').click(function(){
        var id = $(this).data('id');
        if($('#cate_page_' + id).hasClass('cate_open')){
            $('#cate_page_' + id).fadeOut(500);
            $('#cate_page_' + id).removeClass('cate_open');  
            $(this).removeClass('fa-minus');
            $(this).addClass('fa-plus');  
        }else{
            $('#cate_page_' + id).fadeIn(500);
            $('#cate_page_' + id).addClass('cate_open');
            $(this).removeClass('fa-plus');
            $(this).addClass('fa-minus');
        }
        
    });

    $('.toogle_cc_cate_page').click(function(){
        var id = $(this).data('id');
        if($('#cc_cate_page_' + id).hasClass('cate_open')){
            $('#cc_cate_page_' + id).fadeOut(500);
            $('#cc_cate_page_' + id).removeClass('cate_open');  
            $(this).removeClass('fa-minus');
            $(this).addClass('fa-plus');  
        }else{
            $('#cc_cate_page_' + id).fadeIn(500);
            $('#cc_cate_page_' + id).addClass('cate_open');
            $(this).removeClass('fa-plus');
            $(this).addClass('fa-minus');
        }
        
    });
}

function sort_mobile(){
    $('.sort_mobile a').click(function(){
        $('html, body').scrollTop(0);
        $('html').addClass('state-sort');
    });

    $('#sort-menu .list-group-item a').click(function () {
        var sort = $(this).data('sort');
        var route = $('input[name=route]').val()
        var url_redirect = route;
        if(sort != '' ) url_redirect = route + '?sort=' + sort;
        window.location.href = url_redirect;
    });

    $('.context-back').click(function(e){
        e.preventDefault();
        $('html').removeClass('state-sort');
    });
}

function sort_mobile(){
    $('.sort_mobile a').click(function(){
        $('html, body').scrollTop(0);
        $('html').addClass('state-sort');
    });

    $('#sort-menu .list-group-item a').click(function () {
        var sort = $(this).data('sort');
        var route = $('input[name=route]').val()
        var url_redirect = route;
        if(sort != '' ) url_redirect = route + '?sort=' + sort;
        window.location.href = url_redirect;
    });

    $('#sort-menu .context-back').click(function(e){
        e.preventDefault();
        $('html').removeClass('state-sort');
    });
}

$(window).scroll(function(){
    if ($(this).scrollTop() > 30) {
        $('.c-header-mobile').addClass('fixed_mb');
        // $('#menu_header').addClass('fixed1');
    } else {
        $('.c-header-mobile').removeClass('fixed_mb');
        // $('#menu_header').removeClass('fixed1');
    }
});