<?php

/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */
/**
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 * */
return array(
    'js' => array(
        '../js/jquery.bxslider.js',
        '../js/default.js',
        '../js/classie.js',
        '../js/selectFx.js',
        '../js/jquery.mCustomScrollbar.concat.min.js',
        '../js/function.js',
        '../js/main.js'
    ),
    'css' => array(
        '../css/reset.css',
        '../css/font-awesome.min.css',
        '../css/col.css',
        '../css/style_fonts.css',
        '../css/jquery.bxslider.css',
        '../css/cs-select.css',
        '../css/jquery.mCustomScrollbar.css',
        '../css/style.css',
        '../css/ddsmoothmenu.css',
        '../css/reponsive.css'
    ),
);
