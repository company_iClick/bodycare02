$(document).ready(function(){
$("#f_sub").validate({
	rules: {
     site: {required:true,
	 remote: {
			url: "web/check_sitename_jquery",
			type: "post",
			data: {
			  sitename: function() {
				return $("#site").val();
			  }
			}
		  }
	 },
	 user: {required:true},
	 tinhthanh: {required:true},
	 hinh:{accept: "jpg|gif|png"},
	 tencty: {required:true,maxlength:255},
	 diachi: {required:true,maxlength:255},
	 dienthoai: {required:true,maxlength:255},
	 linhvuc: {required:true}
	 
   },
   messages: {
				site: {remote:jQuery.format("Site name này đã có. Không thể dùng.")},
  }
});
	

		
$("#f_sua").validate({
	rules: {
     site: {required:true,
	 remote: {
			url: "admin_home/user/check_sitename_sua_jquery",
			type: "post",
			data: {
			  sitename: function() {
				return $("#site").val();
			  },id:function(){return $("#site").attr('alt') }
			}
		  }
	 },
	 user: {required:true},
	 tinhthanh: {required:true},
	 hinh:{accept: "jpg|gif|png"},
	 tencty: {required:true,maxlength:255},
	 diachi: {required:true,maxlength:255},
	 dienthoai: {required:true,maxlength:255},
	 linhvuc: {required:true}
	 
   },
   messages: {
				site: {remote:jQuery.format("Site name này đã có. Không thể dùng.")}
  }
});
	
	
$('.hientin').live('click',function(){
	var id=$(this).attr('id'); 
	$(this).attr('class','antin');
	$(this).children().attr('src','img/icons/icon_approve.png');
	$.post('admin_post/post/vip/site_active/'+id,function(dt){
		if(dt==1)
		{
			alert('Đã cập nhập');
			
		}
		});
	});

$('.antin').live('click',function(){
	var id=$(this).attr('id');
	$(this).attr('class','hientin'); 
	$(this).children().attr('src','img/icons/icon_delete.png');
	$.post('admin_post/post/novip/site_active/'+id,function(dt){
		if(dt==1)
		{
			alert('Đã cập nhập');
		}
		});
	});	
	
	$('.xoa').live('click',function(){
		if(window.confirm("Bạn có muốn xóa"))
		{
			id=$(this).attr('id');
			xoa_3('admin_post/post/xoa/site_name',id);
		}
	});

});
