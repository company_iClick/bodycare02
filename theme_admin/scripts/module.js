$(document).ready(function(){
	
		
	$('.themtin').live('click',function(){
		$('#f_m').slideDown('slow');
		$('#f_m_sua').slideUp('slow');
		return false;
		});
	
	jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "Name active không thể có khoảng cách");

	
$("#f_m").validate({
	rules: {
	 name: {required:true},
	 gia: {required:true,number:true},
     name_active: {required:true,noSpace:true,
			 remote: {
					url: "admin_post/post/check_module",
					type: "post",
					data: {
					  name_active: function() {
						return $("#name_active").val();
					  }
					}
				  }
			 }
	},
   messages: {
				name_active: {remote:jQuery.format("Name active đã có.")}
   },
   submitHandler: function() {
	   muc =($('input[name=muc]:checked').val());
	   $.post('admin_post/post/them/module',{name:$('#name').val(),muc:muc,gia:$('#gia').val(),name_active:$('#name_active').val()},		function(dt){
				if(dt==1)
				{
					$('#load').load('admin_post/post/load/module')
					$('input[type=text]').val('');
					//$('input[name=muc]:checked').attr('checked','');
					alert('Đã thêm');
				}
				else alert('Không thêm được');
			});
	   }

});

$("#f_m_sua").validate({
	rules: {
	 name_s: {required:true},
	 gia_s: {required:true,number:true},
     name_active_s: {required:true,noSpace:true,
			 remote: {
					url: "admin_post/post/check_module_sua",
					type: "post",
					data: {
					  name_active: function() {return $("#name_active_s").val();},
					  id:function(){return $('#sua_s').attr('alt');}
					}
				  }
			 }
	},
   messages: {
				name_active_s: {remote:jQuery.format("Name active đã có.")}
   },
   submitHandler: function() {
	   var id=$('#sua_s').attr('alt');
	   muc_s =($('input[name=muc_s]:checked').val());
	   $.post('admin_post/post/sua/module',{id:id,name:$('#name_s').val(),muc:muc_s,gia:$('#gia_s').val(),name_active:$('#name_active_s').val()},		function(dt){
				if(dt==1)
				{
					$('#load').load('admin_post/post/load/module')
					$('input[type=text]').val('');
					$('input[name=muc]:checked').attr('checked','');
					alert('Đã sửa');
				}
				else alert('Không sửa được');
			});
	   }

});
	
		
	$('.sua').live('click',function(){
		var id=$(this).attr('id');
		var gia=$('#gia'+id).attr('t');
		var muc=$('#muc'+id).attr('t');
		$('#name_s').val($('#name'+id).text());
		$('#name_active_s').val($('#name_active'+id).text());
		$('#gia_s').val(Math.abs(gia));
		$('.muc_s[value='+muc+']').attr('checked','checked');
		$('#sua_s').attr('alt',id);
		$('#f_m').slideUp('slow');
		$('#f_m_sua').slideDown('slow');
		$('#name_s').focus();
		return false;
	});
		
	$('#bo, #bo_s').live('click',function(){
		$('input[type="text"]').val('');
		$('input[name=muc]:checked').attr('checked','');
		$('#f_m').slideUp('slow');
		$('#f_m_sua').slideUp('slow');
		});
		
	/*$('#sua').live('click',function(){
		var id=$(this).attr('alt');
		var muc =($('input[name=muc]:checked').val());
		if($('#name').val()=='')
		{
			alert('Chưa nhập tên');
			$('#name').focus();
		}
		else if($('#gia').val()=='')
		{
			alert('Chưa nhập gia');
			$('#gia').focus();
		}
		else
		{
			$.post('admin_post/post/sua/module',{id:id,name:$('#name').val(),muc:muc,gia:$('#gia').val()},function(dt){
			if(dt==1)
			{
				$('#load').load('admin_post/post/load/module/')
				$('input[type="text"]').val('');
				$('input[name=muc]:checked').attr('checked','');
				alert('Đã sửa');
			}
			else alert('Không sửa được')
			})
		}
		});
	*/	
	$('.xoa').live('click',function(){
		if(window.confirm("Bạn có muốn xóa"))
		{
			id=$(this).attr('id');
			xoa_3('admin_post/post/xoa/module',id);
		}
	});
});