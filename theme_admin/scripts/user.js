$(document).ready(function(){
	
	$( "#ns" ).datepicker({
					inline: true,
					changeMonth: true,
					changeYear: true,
					yearRange: '1950:2011'
		});		
		$( "#ns" ).datepicker( "option",
				$.datepicker.regional['vi']);
		$( "#ns" ).datepicker( "option", "dateFormat","dd-mm-yy" );
	
	$( "#ns_s" ).datepicker({
					inline: true,
					changeMonth: true,
					changeYear: true,
					yearRange: '1950:2011'
		});		
		$( "#ns_s" ).datepicker( "option",
				$.datepicker.regional['vi']);
		$( "#ns_s" ).datepicker( "option", "dateFormat","dd-mm-yy" );
	
	
	
	
	
	$('.themtin').live('click',function(){
		$('#f_sub').slideDown(500);
		$('#f_sua').slideUp(500);
		return false;
		});
	
$("#f_sub").validate({
	rules: {
     email: {required:true,email:true,
	 remote: {
			url: "web/check_email_jquery",
			type: "post",
			data: {
			  mail: function() {
				return $("#email").val();
			  }
			}
		  }
	 },
	 hoten: {required:true,maxlength:255},
	 pass: {required:true,rangelength:[6,20]},
	 r_pass: {required:true,rangelength:[6,20],equalTo: "#pass"},
	 ns: {required:true}
   },
   messages: {
				email: {remote:jQuery.format("Email này đã có. Không thể dùng.")}
  },
	submitHandler: function(){ 
		$.post('admin_post/post/them/user',
			{email:$('#email').val(),hoten:$('#hoten').val(),pass:$('#pass').val(),ns:$('#ns').val(),gt:$('.rd:checked').val()},
			function(dt){
				if(dt==1)
				{
					$('#load').load('admin_post/post/load_data/user');
					$('input[type="text"]').val('');
					alert('Đã thêm');
				}
				else if(dt==2)
				{
					alert('Email đã có không thể thêm');
				}
				else alert('Không thêm');
			});
	}
});
	

		
	$('.sua').live('click',function(){
		var id=$(this).attr('id');
		$.post('admin_post/post/sua_data/user/'+id,
			function(dt){
				data=$.parseJSON(dt);
				var ns=data.ngaysinh.split('-');
				$('#email_s').val(data.email);
				$('#hoten_s').val(data.hoten);
				$('input[value="'+data.gioitinh+'"]').attr('checked','checked');
				$('#ns_s').val(ns[2]+'-'+ns[1]+'-'+ns[0]);
				$('#email_s').attr('alt',data.email);
			});
		
		
		
		var t=$('#name'+id).attr('t');
		$('#sua').attr('alt',id);
		$('#sua').attr('t',t);
		$('#f_sub').slideUp(200);
		$('#f_sua').slideDown(200);
		$('#name').focus();
		
		return false;
	});
		
$('#bo').live('click',function(){
		$('input[type="text"]').val('');
		$('#f_sua').slideUp(200);
		$('#f_sub').slideUp(200);
		});

		
$("#f_sua").validate({
	rules: {	 
	 hoten_s: {required:true,maxlength:255},
	 ns_s: {required:true}
   },
   messages: {
				email: {remote:jQuery.format("Email này đã có. Không thể dùng.")},
  },
	submitHandler: function(){ 
		$.post('admin_post/post/sua/user',
			{hoten:$('#hoten_s').val(),ns:$('#ns_s').val(),gt:$('.rd_s:checked').val(),id:$('#sua').attr('alt')},
			function(dt){
				if(dt==1)
				{
					$('#load').load('admin_post/post/load_data/user/'+$('#sua').attr('t'));
					$('input[type="text"]').val('');
					alert('Đã sửa');
				}
				else alert("Không sửa");
			});
	}
});
	
	$('.xoa').live('click',function(){
		if(window.confirm("Bạn có muốn xóa"))
		{
			id=$(this).attr('id');
			xoa_3('admin_post/post/xoa/user',id);
		}
	});
	
	
	
	$('.hientin').live('click',function(){
		var id=$(this).attr('id');
		var t=$('#name'+id).attr('t');
		$.post('admin_post/post/vip/user/'+id,function(dt){
			if(dt==1)
			{
				$('#load').load('admin_post/post/load_data/user/'+t);
			}
			else 
			alert('Sửa thất bại');
			});
		});
		
	$('.antin').live('click',function(){
		var id=$(this).attr('id');
		var t=$('#name'+id).attr('t');
		$.post('admin_post/post/novip/user/'+id,function(dt){
			if(dt==1)
			{
				$('#load').load('admin_post/post/load_data/user/'+t);
			}
			else 
			alert('Sửa thất bại');
			});
		});

});
