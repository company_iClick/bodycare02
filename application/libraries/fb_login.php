<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fb_login {

    public function __construct() {
        
        $CI =& get_instance();
        $CI->config->load('fb_graph');

        require 'facebook-php-sdk/src/facebook.php';

        $this->fb = new Facebook(array(
          'appId'  => $CI->config->item('appId', 'fb_graph'),
          'secret' => $CI->config->item('secret', 'fb_graph'),
        ));

    }

    public function getFbLoginUrl(){

        $loginUrl = $this->fb->getLoginUrl(array(
            'scope'     => 'email',
            'redirect_uri'  => base_url('login-facebook'),
            'display'=>'popup'
        ));

        return $loginUrl;
    }

    public function getFbUserinfo(){
        $user = $this->fb->getUser();

        if ($user) {
          try {
             $user = $this->fb->api('/me?fields=email,id,name,birthday,gender');
            } catch (FacebookApiException $e) {
              error_log($e);
              $user = null;
            }
        }

        return $user;
    }
}