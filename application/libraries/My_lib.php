<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class My_lib {

    public function stripUnicode($str) {
        if (!$str)
            return false;
        $unicode = array(
            'a' => 'á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ',
            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd' => 'đ',
            'D' => 'Đ',
            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|€',
            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i' => 'í|ì|ỉ|ĩ|ị',
            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
            '-' => '!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~',
            '-' => '-+-',
            '' => '^\-+|\-+$'
        );
        foreach ($unicode as $khongdau => $codau) {
            $arr = explode("|", $codau);
            $str = str_replace($arr, $khongdau, $str);
        }
        return $str;
    }

// Doi tu co dau => khong dauu

    public function changeTitle($str) {
        $str = $this->stripUnicode($str);
        $str = strtolower($str);
        $str = trim($str);
        $str = preg_replace('/[^a-zA-Z0-9\ ]/', '', $str);
        $str = str_replace("  ", " ", $str);
        $str = str_replace(" ", "-", $str);
        return $str;
    }

    public function sw_human_time_diff($date) {

        $langs = array('Giây', 'Phút', 'Giờ', 'Ngày', 'Tuần', 'Tháng', 'Năm');
        $chunks = array(
            array(60 * 60 * 24 * 365, $langs[6], $langs[6]),
            array(60 * 60 * 24 * 30, $langs[5], $langs[5]),
            array(60 * 60 * 24 * 7, $langs[4], $langs[4]),
            array(60 * 60 * 24, $langs[3], $langs[3]),
            array(60 * 60, $langs[2], $langs[2]),
            array(60, $langs[1], $langs[1]),
            array(1, $langs[0], $langs[0])
        );

        $newer_date = time();


        $since = $newer_date - $date;
        if (0 > $since)
            return __('Gần đây', 'swhtd');
        for ($i = 0, $j = count($chunks); $i < $j; $i++) {
            $seconds = $chunks[$i][0];
            if (( $count = floor($since / $seconds) ) != 0)
                break;
        }
        $output = ( 1 == $count ) ? '1 ' . $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
        if (!(int) trim($output)) {
            $output = '0 ' . $langs[0];
        }
        $output .= ' trước';
        return $output;
    }
    
    function replacenumber($str) {
        $html = '';
        for ($i = 0; $i < strlen($str); $i++) {
            $val = substr($str, $i, 1);
            $html .= '<b>' . $val . '</b>';
        }
        return $html;
    }

    function cut_string($str, $limit)
    {
        if (strlen($str) > $limit) {
            $re = substr($str, 0, $limit);
            $re = substr($re, 0, strrpos($re, " "));
            $re .= "...";
            return $re;
        } else {
            return $str;
        }
    }

}

?>