<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:

  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */


require_once( BASEPATH .'database/DB'. EXT ); $db =& DB();
$route['default_controller'] = "home";
$route['404_override'] = 'home/notfound';

/* * ***************admin********************** */
///for paging

$route['admin'] = "back/administrator";
$route['admin/login'] = 'back/administrator/login';
$route['admin/thongke'] = 'back/administrator/thongke';
$route['admin/logout'] = 'back/administrator/logout';
$route['admin/change-my-pass'] = 'moderator/admin/change_my_pass';
$route['admin/change-info'] = 'moderator/admin/ChangeMyInfo';
$route['admin/change-info'] = 'moderator/admin/ChangeMyInfo';
$route['admin/not-permission'] = 'back/administrator/NotPermission';
$route['admin/set-value'] = 'back/administrator/set_value';
$route['admin/set-value-article'] = 'back/administrator/set_value_article';

$route['admin/(any)/(:num)'] = '$1/admin/index/$2';
$route['admin/(any)/(:num)/(:num)'] = '$1/admin/index/$2/$3';
//end
$route['admin/(:any)'] = '$1/admin';
$route['admin/(:any)/(:any)'] = '$1/admin/$2';
$route['admin/(:any)/(:any)/(:num)'] = '$1/admin/$2/$3';
$route['admin/(:any)/(:any)/(:num)/(:num)'] = '$1/admin/$2/$3/$4';
$route['admin/(:any)/(:any)/(:num)/(:num)/(:num)'] = '$1/admin/$2/$3/$4/$5';
$route['admin/(:any)/index/(:num)'] = '$1/admin/index/$2';
$route['admin/(:any)/date/(:any)'] = '$1/admin/date/$2';

    $category=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('category')->result();
    if(count($category)>0){
        foreach ($category as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/category/$cate->slug";
                $route[$cate->slug."/(:num)"] = "product/category/".$cate->slug."/$1";
            } 
        }
    }

    $brand=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('brand')->result();
    if(count($brand)>0){
        foreach ($brand as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/brand_detail/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/brand_detail_category/$cate->slug/$1";
            } 
        }
    }

    $product_type=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('product_type')->result();
    if(count($product_type)>0){
        foreach ($product_type as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/product_type/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/product_type_category/".$cate->slug."/$1";
            } 
        }
    }

    $skin_type=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('skin_type')->result();
    if(count($skin_type)>0){
        foreach ($skin_type as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/skin_type/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/skin_type_category/".$cate->slug."/$1";
            } 
        }
    }

    $hair_type=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('hair_type')->result();
    if(count($hair_type)>0){
        foreach ($hair_type as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/hair_type/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/hair_type_category/".$cate->slug."/$1";
            } 
        }
    }

    $action=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('action')->result();
    if(count($action)>0){
        foreach ($action as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/action/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/action_category/".$cate->slug."/$1";               
            } 
        }
    }

    $capacity=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('capacity')->result();
    if(count($capacity)>0){
        foreach ($capacity as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/capacity/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/capacity_category/".$cate->slug."/$1";
            } 
        }
    }

    $weigh=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('weigh')->result();
    if(count($weigh)>0){
        foreach ($weigh as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/weigh/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/weigh_category/".$cate->slug."/$1";
            } 
        }
    }

    $pill_number=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('pill_number')->result();
    if(count($pill_number)>0){
        foreach ($pill_number as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/pill_number/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/pill_number_category/".$cate->slug."/$1";
            } 
        }
    }

    $origin=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('origin')->result();
    if(count($origin)>0){
        foreach ($origin as $cate){
            if($cate->slug!=''){
                $route[$cate->slug] = "product/origin/$cate->slug";
                $route[$cate->slug."/(:any)"] = "product/origin_category/".$cate->slug."/$1";
            } 
        }
    }
    
    $product=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('product')->result();
    if(count($product)>0){
        foreach ($product as $p){
            if($p->slug!=''){
                $route[$p->slug] = "product/detail/$p->slug";
            } 
        }
    }

    $tags=$db
        ->select("slug")
        ->where(array("status"=>1))
        ->get('tags')->result();
    if(count($tags)>0){
        foreach ($tags as $p){
            if($p->slug!=''){
                $route[$p->slug] = "product/tags/$p->slug";
                $route[$p->slug."/(:any)"] = "product/tag_category/".$p->slug."/$1";
            } 
        }
    }


    $article_1=$db
        ->select("slug")
        ->where(array("status"=>1, "type"=>1))
        ->get('article')->result();
    if(count($article_1)>0){
        foreach ($article_1 as $p){
            if($p->slug!=''){
                $route[$p->slug] = "article/static_page/$p->slug";
            } 
        }
    }

    $article_3=$db
        ->select("slug")
        ->where(array("status"=>1, "type"=>3))
        ->get('article')->result();
    if(count($article_3)>0){
        foreach ($article_3 as $p){
            if($p->slug!=''){
                $route[$p->slug] = "article/detail/$p->slug";
                $route[$p->slug . "/(:num)"] = "article/detail/$p->slug/$1";
            } 
        }
    }

    $article_4=$db
        ->select("slug")
        ->where(array("status"=>1, "type"=>4))
        ->get('article')->result();
    if(count($article_4)>0){
        foreach ($article_4 as $p){
            if($p->slug!=''){
                $route[$p->slug] = "article/customer_support/$p->slug";
            } 
        }
    }
    

$route['(thuong-hieu)'] = 'product/brand';
$route['khuyen-mai'] = 'product/promotion';
$route['khuyen-mai/(:num)'] = 'product/promotion/$1';

$route["khuyen-mai/(price-asc|price-desc|khuyen-mai|qua-tang)"] = "product/promotion";
$route["khuyen-mai/(price-asc|price-desc|khuyen-mai|qua-tang)/(:num)"] = "product/promotion/$2";

$route['danh-cho-nam'] = 'product/male';
$route['danh-cho-nam/(:num)'] = 'product/male/$1';
$route["danh-cho-nam/(price-asc|price-desc|khuyen-mai|qua-tang)"] = "product/male";
$route["danh-cho-nam/(price-asc|price-desc|khuyen-mai|qua-tang)/(:num)"] = "product/male/$2";

$route['danh-cho-nu'] = 'product/female';
$route['danh-cho-nu/(:num)'] = 'product/female/$1';
$route["danh-cho-nu/(price-asc|price-desc|khuyen-mai|qua-tang)"] = "product/female";
$route["danh-cho-nu/(price-asc|price-desc|khuyen-mai|qua-tang)/(:num)"] = "product/female/$2";

$route['search/cat/(:num)/q/(:any)'] = 'product/search/$1/$2';
$route['search/cat/(:num)/q/(:any)/(:num)'] = 'product/search/$1/$2/$3';

$route["search/cat/(:num)/q/(:any)/(price-asc|price-desc|khuyen-mai|qua-tang)"] = 'product/search/$1/$2';
$route["search/cat/(:num)/q/(:any)/(price-asc|price-desc|khuyen-mai|qua-tang)/(:num)"] = "product/search/$1/$2/$4";

$route["s-search"] = "product/s_search";

$route['(product-filter)'] = 'product/product_filter';
$route['(product-filter-select)'] = 'product/product_filter_select';
$route['(product-filter-element)'] = 'product/product_filter_element';
$route['(category-page)'] = 'product/category_page';

$route['(brand-character)'] = 'product/brand_character';
$route['order-checkout'] = 'product/order_checkout';
$route['call-back'] = 'product/call_back';
$route['(add-cart)'] = 'product/addcart';
$route['(add-to-cart)'] = 'product/add_to_cart';
$route['(gio-hang)'] = 'product/cart';
$route['(hoan-tat-don-hang)'] = 'product/checkout';
$route['(thanh-toan-ngan-luong)'] = 'product/nganluongProcess';
$route['(don-hang)/(:any)'] = 'product/checkout_success/$2';
$route['(update-cart)'] = 'product/update_cart';
$route['(delete-cart)'] = 'product/remove_cart';
$route['(delete-all-cart)'] = 'product/remove_all_cart';

$route['dang-ky'] = 'users/register';
$route['dang-nhap'] = 'users/login';
$route['login-facebook'] = 'users/login_facebook';
$route['login-face'] = 'users/face';
$route['login-google'] = 'users/login_google';
$route['thong-tin-tai-khoan'] = 'users/info';
$route['lich-su-mua-hang'] = 'users/lichsu';
$route['dang-xuat'] = 'users/logout';
$route['doi-mat-khau'] = 'users/change_password';
$route['dia-chi-giao-hang'] = 'users/shipping_address';
$route['quen-mat-khau'] = 'users/forgot_password';
$route['forgot-password-process'] = 'users/forgot_password_process';
$route['reset-password'] = 'users/reset_password';
$route['lich-su-don-hang'] = 'users/history_order';
$route['chi-tiet-don-hang/(:num)'] = 'users/history_order_detail/$1';
$route['tin-tuc'] = 'article/index01';
$route['kien-thuc-lam-dep'] = 'article/index';
$route['kien-thuc-lam-dep/(:any)'] = 'article/category/$1';

$route['send-comment'] = 'comment/send_comment';
$route['answer-comment'] = 'comment/answer_comment';

$route['load-district'] = 'product/load_district';
$route['advisory-request'] = 'home/advisory_request';

$route['list-feedback'] = 'feedback/list_feedback';
$route['send-feedback'] = 'feedback/send_feedback';
$route['answer-feedback'] = 'feedback/answer_feedback';

$route['newsletter'] = 'newsletter/index';

$route["video/(:any)"] = "video/detail/$1";

$route["chuc-nang"] = "action/index";
$route["loai-san-pham"] = "product_type/index";
$route["loai-da"] = "skin_type/index";
$route["loai-toc"] = "hair_type/index";
$route["xuat-xu"] = "origin/index";
$route["dung-tich"] = "capacity/index";
$route["trong-luong"] = "weigh/index";
$route["so-luong-vien"] = "pill_number/index";

/* End of file routes.php */
/* Location: ./application/config/routes.php */