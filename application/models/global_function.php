<?php
class Global_function extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function set_event($format, $img, $title, $link, $w, $h)
    {
        $kq = '<a href="' . $link . '" title="' . $title . '" target="_blank" />';
        if ($format == 0) {
            $kq .= '<img src="../themes/slide/' . $img . '" border="0" width="' . $w . '" height="' . $h . '" alt="' . $title . '" title="' . $title . '"/>';
        } else {
            $kq .= '


                <div>


                    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" height="' . $h . 'px" width="' . $w . 'px" title="' . $title . '" >


                      <param name="movie" value="themes/slide/' . $img . '">


                      <param name="quality" value="high">


                      <param NAME="WMode" value="transparent">  


                      <embed src="themes/slide/' . $img . '" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="' . $w . 'px" title="' . $title . '" height="' . $h . 'px" ></embed>


                    </object>


                </div>


            ';
        }
        return $kq . "</a>";
    }
    function unicode($text)
    {
        $trans        = array(
            'à' => 'a',
            'á' => 'a',
            'ả' => 'a',
            'ã' => 'a',
            'ạ' => 'a',
            'â' => 'a',
            'ấ' => 'a',
            'ầ' => 'a',
            'ẫ' => 'a',
            'ẩ' => 'a',
            'ậ' => 'a',
            'ú' => 'a',
            'ù' => 'u',
            'ủ' => 'u',
            'ũ' => 'u',
            'ụ' => 'u',
            'à' => 'a',
            'á' => 'a',
            'ô' => 'o',
            'ố' => 'o',
            'ồ' => 'o',
            'ổ' => 'o',
            'ỗ' => 'o',
            'ộ' => 'o',
            'ó' => 'o',
            'ò' => 'o',
            'ỏ' => 'o',
            'õ' => 'o',
            'ọ' => 'o',
            'ẽ' => 'e',
            'ê' => 'e',
            'è' => 'e',
            'ế' => 'e',
            'ề' => 'e',
            'ể' => 'e',
            'ễ' => 'e',
            'ệ' => 'e',
            'í' => 'i',
            'ì' => 'i',
            'ỉ' => 'i',
            'ĩ' => 'i',
            'ị' => 'i',
            'ơ' => 'o',
            'ớ' => 'o',
            'ý' => 'y',
            'ỳ' => 'y',
            'ỷ' => 'y',
            'ỹ' => 'y',
            'ỵ' => 'y',
            'ờ' => 'o',
            'ở' => 'o',
            'ỡ' => 'o',
            'ợ' => 'o',
            'ư' => 'u',
            'ừ' => 'u',
            'ứ' => 'u',
            'ử' => 'u',
            'ữ' => 'u',
            'ự' => 'u',
            'đ' => 'd',
            'ẹ' => 'e',
            'À' => 'A',
            'Á' => 'A',
            'Ả' => 'A',
            'Ã' => 'A',
            'Ạ' => 'A',
            'Â' => 'A',
            'Ấ' => 'A',
            'À' => 'A',
            'Ẫ' => 'A',
            'Ẩ' => 'A',
            'Ậ' => 'A',
            'Ú' => 'U',
            'Ù' => 'U',
            'Ủ' => 'U',
            'Ũ' => 'U',
            'Ụ' => 'U',
            'Ô' => 'O',
            'Ố' => 'O',
            'Ồ' => 'O',
            'Ổ' => 'O',
            'Ỗ' => 'O',
            'Ộ' => 'O',
            'Ê' => 'E',
            'Ế' => 'E',
            'Ề' => 'E',
            'Ể' => 'E',
            'Ễ' => 'E',
            'Ệ' => 'E',
            'Í' => 'I',
            'Ì' => 'I',
            'Ỉ' => 'I',
            'Ĩ' => 'I',
            'Ị' => 'I',
            'Ơ' => 'O',
            'Ớ' => 'O',
            'Ờ' => 'O',
            'Ở' => 'O',
            'Ỡ' => 'O',
            'Ợ' => 'O',
            'Ư' => 'U',
            'Ừ' => 'U',
            'Ứ' => 'U',
            'Ử' => 'U',
            'Ữ' => 'U',
            'Ự' => 'U',
            'Đ' => 'D',
            'Ý' => 'Y',
            'Ỳ' => 'Y',
            'Ỷ' => 'Y',
            'Ỹ' => 'Y',
            'Ỵ' => 'Y',
            'Ằ' => 'A',
            'Ầ' => 'A',
            'á' => 'a',
            'à' => 'a',
            'ả' => 'a',
            'ã' => 'a',
            'ạ' => 'a',
            'ă' => 'a',
            'ắ' => 'a',
            'ằ' => 'a',
            'ẻ' => 'e',
            'ẳ' => 'a',
            'ẵ' => 'a',
            'ặ' => 'a',
            'â' => 'a',
            'ấ' => 'a',
            'ầ' => 'a',
            'ẩ' => 'a',
            'ẫ' => 'a',
            'ậ' => 'a',
            'ú' => 'u',
            'ù' => 'u',
            'ủ' => 'u',
            'ũ' => 'u',
            'ụ' => 'u',
            'ư' => 'u',
            'ứ' => 'u',
            'ừ' => 'u',
            'ử' => 'u',
            'ữ' => 'u',
            'ự' => 'u',
            'í' => 'i',
            'ì' => 'i',
            'ỉ' => 'i',
            'ĩ' => 'i',
            'ị' => 'i',
            'ó' => 'o',
            'ò' => 'o',
            'ỏ' => 'o',
            'õ' => 'o',
            'ọ' => 'o',
            'ô' => 'o',
            'ố' => 'o',
            'ổ' => 'o',
            'ỗ' => 'o',
            'ộ' => 'o',
            'ơ' => 'o',
            'ớ' => 'o',
            'ờ' => 'o',
            'ở' => 'o',
            'ỡ' => 'o',
            'ợ' => 'o',
            'đ' => 'd',
            'Đ' => 'D',
            'ý' => 'y',
            'ỳ' => 'y',
            'ỷ' => 'y',
            'ỹ' => 'y',
            'ỵ' => 'y',
            'Á' => 'A',
            'À' => 'A',
            'Ả' => 'A',
            'Ã' => 'A',
            'Ạ' => 'A',
            'Ă' => 'A',
            'Ắ' => 'A',
            'Ẳ' => 'A',
            'Ẵ' => 'A',
            'Ặ' => 'A',
            'Â' => 'A',
            'Ấ' => 'A',
            'Ẩ' => 'A',
            'Ẫ' => 'A',
            'Ậ' => 'A',
            'É' => 'E',
            'È' => 'E',
            'Ẻ' => 'E',
            'Ẽ' => 'E',
            'Ẹ' => 'E',
            'Ế' => 'E',
            'Ề' => 'E',
            'Ể' => 'E',
            'Ễ' => 'E',
            'Ệ' => 'E',
            'Ú' => 'U',
            'Ù' => 'U',
            'Ủ' => 'U',
            'Ũ' => 'U',
            'Ụ' => 'U',
            'Ư' => 'U',
            'Ứ' => 'U',
            'Ừ' => 'U',
            'Ử' => 'U',
            'Ữ' => 'U',
            'Ự' => 'U',
            'Í' => 'I',
            'Ì' => 'I',
            'Ỉ' => 'I',
            'Ĩ' => 'I',
            'Ị' => 'I',
            'Ó' => 'O',
            'Ò' => 'O',
            'Ỏ' => 'O',
            'Õ' => 'O',
            'Ọ' => 'O',
            'Ô' => 'O',
            'Ố' => 'O',
            'Ổ' => 'O',
            'Ỗ' => 'O',
            'Ộ' => 'O',
            'Ơ' => 'O',
            'Ớ' => 'O',
            'Ờ' => 'O',
            'Ở' => 'O',
            'Ỡ' => 'O',
            'Ợ' => 'O',
            'Ý' => 'Y',
            'Ỳ' => 'Y',
            'Ỷ' => 'Y',
            'Ỹ' => 'Y',
            'Ỵ' => 'Y',
            ' ' => '-',
            '----' => '-',
            '---' => '-',
            '--' => '-',
            '"' => ''
        );
        $text_convert = strtr($text, $trans);
        $text_convert = str_replace('"', '', $text_convert);
        $text_convert = str_replace('&#39;', '', $text_convert);
        $text_convert = str_replace(',', '', $text_convert);
        $text_convert = str_replace(':', '', $text_convert);
        $text_convert = str_replace('(', '', $text_convert);
        $text_convert = str_replace(')', '', $text_convert);
        $text_convert = str_replace('"', '', $text_convert);
        $text_convert = str_replace('"', '', $text_convert);
        $text_convert = str_replace('/', '-', $text_convert);
        $text_convert = str_replace('.', '', $text_convert);
        $text_convert = str_replace('!', '', $text_convert);
        $text_convert = str_replace('@', '', $text_convert);
        $text_convert = str_replace('#', '', $text_convert);
        $text_convert = str_replace('$', '', $text_convert);
        $text_convert = str_replace('%', '', $text_convert);
        $text_convert = str_replace('^', '', $text_convert);
        $text_convert = str_replace('*', '', $text_convert);
        $text_convert = str_replace('&', '', $text_convert);
        $text_convert = str_replace('{', '', $text_convert);
        $text_convert = str_replace('}', '', $text_convert);
        $text_convert = str_replace('[', '', $text_convert);
        $text_convert = str_replace(']', '', $text_convert);
        $text_convert = str_replace('|', '', $text_convert);
        $text_convert = str_replace('é', 'e', $text_convert);
        $text_convert = str_replace('!', '', $text_convert);
        $text_convert = str_replace('?', '', $text_convert);
        $text_convert = str_replace('%', '', $text_convert);
        $text_convert = str_replace('+', '', $text_convert);
        $text_convert = preg_replace("/('|')/", '', $text_convert);
        $text_convert = str_replace('*', '', $text_convert);
        $text_convert = str_replace('&', '', $text_convert);
        $text_convert = str_replace('^', '', $text_convert);
        $text_convert = str_replace(',', '', $text_convert);
        $text_convert = str_replace('---', '-', $text_convert);
        $text_convert = str_replace('&#39;', '', $text_convert);
        $text_convert = str_replace("'", "", $text_convert);
        $text_convert = str_replace("~", "", $text_convert);
        $text_convert = str_replace("`", "", $text_convert);
        $text_convert = strtolower($text_convert);
        $text_convert = preg_replace("/( |!|#|$|%|')/", '', $text_convert);
        $text_convert = preg_replace('/("|")/', '', $text_convert);
        $text_convert = preg_replace("/(̀|́|̉|$|>)/", '', $text_convert);
        $text_convert = preg_replace("'<[/!]*?[^<>]*?>'si", "", $text_convert);
        $text_convert = str_replace('"', '', $text_convert);
        $text_convert = str_replace("----", "-", $text_convert);
        $text_convert = str_replace("---", "-", $text_convert);
        $text_convert = str_replace("--", "-", $text_convert);
        $text_convert = str_replace("\\", "", $text_convert);
        return $text_convert;
    }
    function share_mxh()
    {
        $url   = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $share = NULL;
        $share .= "<a href='https://www.google.com/bookmarks/mark?op=edit&bkmk=$url'><img src='images/google.png' alt='iclick.vn share Zing me' align='absmiddle'></a>";
        $share .= "<a href='http://www.facebook.com/share.php?u=$url'><img src='images/face.png' alt='iclick.vn share Zing me' align='absmiddle'></a>";
        $share .= "<a href='http://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3D$url'><img src='images/twitter.png' alt='iclick.vn share Zing me' align='absmiddle'></a>";
        $share .= "<a href='http://link.apps.zing.vn/pro/view/conn/share?u=$url'><img src='images/zing.png' alt='iclick.vn share Zing me' align='absmiddle'></a>";
        return $share;
    }
    function link_page($link, $forcus, $total)
    {
        $result = '';
        $forcus = is_numeric($forcus) == true ? $forcus : 1;
        if ($forcus < 2) {
            $result .= '<b>' . $forcus . '</b>';
            $end = $total > 3 ? 3 : $total;
            for ($x = 2; $x <= $end; $x++) {
                $result .= '<a href="' . $link . '/' . $x . '">' . $x . '</a>';
            }
            $result .= '<a href="' . $link . '/' . $total . '">Last</a>';
        } else {
            $result .= '<a href="' . $link . '/1/">First</a>';
            (int) $link_left = $forcus - 3 < 1 ? 1 : $forcus - 3;
            for ($x = $link_left; $x <= $forcus - 1; $x++) {
                $result .= '<a href="' . $link . '/' . $x . '">' . $x . '</a>';
            }
            $result .= '<i>' . $forcus . '</i>';
            (int) $link_right = $forcus + 3 > $total ? $total : $forcus + 3;
            for ($x = $forcus + 1; $x <= $link_right; $x++) {
                $result .= '<a href="' . $link . '/' . $x . '">' . $x . '</a>';
            }
        }
        return $result;
    }
    function change_lang($lang_begin, $lang_end, $link_false)
    {
        $this->load->helper('url');
        $url            = str_replace('.html', '', $this->uri->uri_string());
        $sub_link       = explode('/', $url);
        $count_sub_link = count($sub_link);
        $kq             = '';
        $seccess        = false;
        foreach ($sub_link as $text) {
            if ($text == $lang_begin) {
                $kq .= '/' . $lang_end;
                $seccess = true;
            } else {
                $kq .= '/' . $text;
            }
        }
        if ($seccess == true) {
            return site_url($kq);
        } else {
            return $link_false;
        }
    }
    function get_array_cat($id)
    {
        $this->db->select('id');
        $this->db->where('top_category', $id);
        $query = $this->db->get('category');
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row):
                $return[] = $row;
            endforeach;
            $result = $return;
            $query->free_result();
        } else {
            $result = FALSE;
        }
        return $result;
    }
    function randomPassword($n)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . '0123456789';
        $str   = '';
        $max   = strlen($chars) - 1;
        for ($i = 0; $i < $n; $i++)
            $str .= $chars[rand(0, $max)];
        return $str;
    }
    function ChuoiNgauNhien($sokytu)
    {
        $giatri = '';
        $chuoi  = "ABCDEFGHIJKLMNOPQRSTUVWXYZWabcdefghijklmnopqrstuvwxyzw0123456789";
        for ($i = 0; $i < $sokytu; $i++) {
            $vitri  = mt_rand(0, strlen($chuoi));
            $giatri = $giatri . substr($chuoi, $vitri, 1);
        }
        return $giatri;
    }
    function savesess()
    {
        $r           = $this->session->all_userdata();
        $sessionTime = 30;
        $username    = '';
        if (isset($this->session->userdata('user')->id))
            $username = $this->session->userdata('user')->user_email;
        $ipAddress     = $r['ip_address'];
        $userAgent     = $r['user_agent'];
        $session_start = $r['last_activity'];
        $idSession     = $r['session_id'];
        $kq            = $this->db->query("SELECT id FROM usersession WHERE id='$idSession'");
        $giobd         = substr(strtotime(date('Y-m-d H:I')), 0, 8) . '00';
        settype($giobd, 'int');
        $giokt = $giobd + 3599;
        $kq0   = $this->db->query("SELECT id FROM usersview WHERE idsess='$idSession'");
        if ($kq->num_rows() > 0) {
            $this->lastVisit = time();
            $this->Username  = $username;
            $this->db->update('usersession', $this, array(
                'id' => $idSession
            ));
        } else {
            $this->ipAdd     = $ipAddress;
            $this->userAgent = $userAgent;
            $this->lastVisit = time();
            $this->ses_start = $session_start;
            $this->id        = $idSession;
            $this->Username  = $username;
            $this->db->insert('usersession', $this);
        }
        if ($kq0->num_rows() > 0) {
            $kz = $this->db->query("SELECT id FROM usersview WHERE idsess='$idSession' and sesstart = " . $giobd . " and lastVisit = " . $giokt);
            if ($kz->num_rows() > 0)
                $this->db->update('usersview', array(
                    'Username' => $username
                ), array(
                    'idsess' => $idSession,
                    'lastVisit' => $giokt
                ));
            else
                $this->db->insert('usersview', array(
                    'ipAdd' => $ipAddress,
                    'userAgent' => $userAgent,
                    'sesstart' => $giobd,
                    'lastVisit' => $giokt,
                    'ses_start' => $session_start,
                    'id' => time() . $this->ChuoiNgauNhien(8),
                    'Username' => $username,
                    'idsess' => $idSession
                ));
        } else {
            $this->db->insert('usersview', array(
                'ipAdd' => $ipAddress,
                'userAgent' => $userAgent,
                'sesstart' => $giobd,
                'lastVisit' => $giokt,
                'ses_start' => $session_start,
                'id' => time() . $this->ChuoiNgauNhien(8),
                'Username' => $username,
                'idsess' => $idSession
            ));
        }
        $this->db->query("DELETE FROM usersession WHERE unix_timestamp() - lastVisit >= $sessionTime * 60");
        return $userAgent;
    }
    function delsess($ids)
    {
        $this->db->query("DELETE FROM usersession WHERE id='$ids'");
    }
    function rand_chars($c, $l, $u = FALSE)
    {
        if (!$u)
            for ($s = '', $i = 0, $z = strlen($c) - 1; $i < $l; $x = rand(0, $z), $s .= $c{$x}, $i++);
        else
            for ($i = 0, $z = strlen($c) - 1, $s = $c{rand(0, $z)}, $i = 1; $i != $l; $x = rand(0, $z), $s .= $c{$x}, $s = ($s{$i} == $s{$i - 1} ? substr($s, 0, -1) : $s), $i = strlen($s));
        return $s;
    }
    function paging($page, $total, $url, $id = 1, $get = null)
    {
        $previous_btn = true;
        $next_btn     = true;
        $first_btn    = true;
        $last_btn     = true;
        $count        = $total;
        $tongtrang    = ceil($total / $page);
        $num          = "";
        if ($count != 0) {
            if ($id >= 7) {
                $start_loop = $id - 4;
                if ($tongtrang > $id + 4)
                    $end_loop = $id + 4;
                else if ($id <= $tongtrang && $id > $tongtrang - 6) {
                    $start_loop = $tongtrang - 6;
                    $end_loop   = $tongtrang;
                } else {
                    $end_loop = $tongtrang;
                }
            } else {
                $start_loop = 1;
                if ($tongtrang > 7)
                    $end_loop = 7;
                else
                    $end_loop = $tongtrang;
            }
        }
        $getId = $this->input->get($get);
        if ($first_btn && $id > 1) {
            $dau = "<li  class=''><a href='" . site_url($url) . "'>Đầu</a></li>";
        } else if ($first_btn) {
            $dau = "<li  class='text'>Đầu</li>";
        }
        if ($previous_btn && $id > 1) {
            $tam = $id - 1;
            $lui = "<li class='page_prev'><a href='" . site_url($url . $tam) . "'><span class='icon-muiten1'>
                </span></a></li>";
            if($this->router->fetch_class() == 'article' && $this->router->fetch_method() == 'detail') $lui  = "<li class='page_prev'><a href='" . site_url($url . $tam) . "#product' title=''><span class='icon-muiten1'>
                </span></a></li>";
            if ($get != null)
                $lui = "<li class='page_next'><a href='" . site_url($url . $tam) . "?" . $get . "=" . $getId . "'>Lùi</a></li>";
        } else if ($previous_btn) {
            $lui = "";
        }
        if ($next_btn && $id < $tongtrang) {
            $tam2 = $id + 1;
            $toi  = "<li class='page_next'><a href='" . site_url($url . $tam2) . "'><span class='icon-muiten1'>
                
                </span></a></li>";
            if($this->router->fetch_class() == 'article' && $this->router->fetch_method() == 'detail') $toi  = "<li class='page_next'><a href='" . site_url($url . $tam2) . "#product' title=''><span class='icon-muiten1'></span></a></li>";
            if ($get != null)
                $toi = "<li class='page_next'><a href='" . site_url($url . $tam2) . "?" . $get . "=" . $getId . "'> Tới </a></li>";
        } else if ($next_btn) {
            $toi = "";
        }
        if ($last_btn && $id < $tongtrang) {
            $cuoi = "<li  class=''><a href='" . site_url($url . $tongtrang) . "'> Cuối </a></li>";
        } else if ($last_btn) {
            $cuoi = "<li class='text'>Cuối</li>";
        }
        if ($count > 0) {
            for ($i = $start_loop; $i <= $end_loop; $i++) {
                if ($i == $id)
                    $num .= "<li class='active'><a href='#' title='' onclick='return false'>$i</a></li>";
                else {
                    $pageNum = "<li><a href='" . site_url($url . $i) . "' title=''>$i</a></li>";
                    if($this->router->fetch_class() == 'article' && $this->router->fetch_method() == 'detail') $pageNum  = "<li><a href='" . site_url($url . $i) . "#product' title=''>$i</a></li>";
                    if ($get != null)
                        $pageNum = "<li><a href='" . site_url($url . $i) . "?" . $get . "=" . $getId . "' title=''>$i</a></li>";
                    $num .= $pageNum;
                }
            }
        }

        $pageLast = ($tongtrang > 8 && $id < $tongtrang - 4) ? "<li>...</li><li><a href='" . site_url($url . $tongtrang) . "' title=''>$tongtrang</a></li>" : "";
        if ($count > 0 && $tongtrang > 1)
            $link = "
        <div class='paging'><ul class='pagination'> 
            " . $lui . $num . $pageLast . $toi . "
            
        </ul></div>
            ";
        else
            $link = '';
        return $link;
    }

    function paging_ajax($page, $total, $id = 1)
    {
        $previous_btn = true;
        $next_btn     = true;
        $first_btn    = true;
        $last_btn     = true;
        $count        = $total;
        $tongtrang    = ceil($total / $page);
        $num          = "";
        if ($count != 0) {
            if ($id >= 7) {
                $start_loop = $id - 4;
                if ($tongtrang > $id + 4)
                    $end_loop = $id + 4;
                else if ($id <= $tongtrang && $id > $tongtrang - 6) {
                    $start_loop = $tongtrang - 6;
                    $end_loop   = $tongtrang;
                } else {
                    $end_loop = $tongtrang;
                }
            } else {
                $start_loop = 1;
                if ($tongtrang > 7)
                    $end_loop = 7;
                else
                    $end_loop = $tongtrang;
            }
        }

        if ($first_btn && $id > 1) {
            $dau = "<li class=''><a href='javascript:void(0)' data-page='1'>Đầu</a></li>";
        } else if ($first_btn) {
            $dau = "<li class='text'>Đầu</li>";
        }
        if ($previous_btn && $id > 1) {
            $tam = $id - 1;
            $lui = "<li class='page_prev'><a href='javascript:void(0)' data-page='". $tam ."'><span class='icon-muiten1'>
                
                </span></a></li>";
        } else if ($previous_btn) {
            $lui = "";
        }
        if ($next_btn && $id < $tongtrang) {
            $tam2 = $id + 1;
            $toi  = "<li class='page_next'><a href='javascript:void(0)' data-page='". $tam2 ."'><span class='icon-muiten1'>
                
                </span></a></li>";
        } else if ($next_btn) {
            $toi = "";
        }
        if ($last_btn && $id < $tongtrang) {
            $cuoi = "<li  class=''><a href='javascript:void(0)' data-page='". $tongtrang ."'> Cuối </a></li>";
        } else if ($last_btn) {
            $cuoi = "<li class='text'>Cuối</li>";
        }
        if ($count > 0) {
            for ($i = $start_loop; $i <= $end_loop; $i++) {
                if ($i == $id)
                    $num .= "<li class='active'><a href='javascript:void(0)' title='' data-page='". $i ."'>$i</a></li>";
                else {
                    $pageNum = "<li><a href='javascript:void(0)' title='' data-page='". $i ."'>$i</a></li>";
                    $num .= $pageNum;
                }
            }
        }

        $pageLast = ($tongtrang > 8 && $id < $tongtrang - 4) ? "<li>...</li><li><a href='javascript:void(0)' title='' data-page='". $tongtrang ."'>$tongtrang</a></li>" : "";
        if ($count > 0 && $tongtrang > 1)
            $link = "
        <div class='paging'><ul class='pagination' id='pagination_ajax'> 
            " . $lui . $num . $pageLast . $toi . "
            
        </ul></div>
            ";
        else
            $link = '';
        return $link;
    }

    function paging_ajax_category($cate_id, $page, $total, $id = 1, $options = NULL)
    {
        $previous_btn = true;
        $next_btn     = true;
        $first_btn    = true;
        $last_btn     = true;
        $count        = $total;
        $tongtrang    = ceil($total / $page);
        $num          = "";
        if ($count != 0) {
            if ($id >= 7) {
                $start_loop = $id - 4;
                if ($tongtrang > $id + 4)
                    $end_loop = $id + 4;
                else if ($id <= $tongtrang && $id > $tongtrang - 6) {
                    $start_loop = $tongtrang - 6;
                    $end_loop   = $tongtrang;
                } else {
                    $end_loop = $tongtrang;
                }
            } else {
                $start_loop = 1;
                if ($tongtrang > 7)
                    $end_loop = 7;
                else
                    $end_loop = $tongtrang;
            }
        }

        if ($first_btn && $id > 1) {
            $dau = "<li class=''><a href='javascript:void(0)' data-cate='". $cate_id ."' data-item='". $page ."' data-page='1'>Đầu</a></li>";
        } else if ($first_btn) {
            $dau = "<li class='text'>Đầu</li>";
        }
        if ($previous_btn && $id > 1) {
            $tam = $id - 1;
            $lui = "<li class='page_prev'><a href='javascript:void(0)' data-cate='". $cate_id ."' data-item='". $page ."' data-page='". $tam ."'><span class='icon-muiten1'>
                
                </span></a></li>";
        } else if ($previous_btn) {
            $lui = "";
        }
        if ($next_btn && $id < $tongtrang) {
            $tam2 = $id + 1;
            $toi  = "<li class='page_next'><a href='javascript:void(0)' data-cate='". $cate_id ."' data-item='". $page ."' data-page='". $tam2 ."'><span class='icon-muiten1'>
                
                </span></a></li>";
        } else if ($next_btn) {
            $toi = "";
        }
        if ($last_btn && $id < $tongtrang) {
            $cuoi = "<li  class=''><a href='javascript:void(0)' data-cate='". $cate_id ."' data-item='". $page ."' data-page='". $tongtrang ."'> Cuối </a></li>";
        } else if ($last_btn) {
            $cuoi = "<li class='text'>Cuối</li>";
        }
        if ($count > 0) {
            for ($i = $start_loop; $i <= $end_loop; $i++) {
                if ($i == $id)
                    $num .= "<li class='active pajaxcate_".$i."'><a href='javascript:void(0)' title='' data-item='". $page ."' data-cate='". $cate_id ."' data-page='". $i ."'>$i</a></li>";
                else {
                    $pageNum = "<li class='pajaxcate_".$i."'><a href='javascript:void(0)' title='' data-cate='". $cate_id ."' data-item='". $page ."' data-page='". $i ."'>$i</a></li>";
                    $num .= $pageNum;
                }
            }
        }

        $pageLast = ($tongtrang > 8 && $id < $tongtrang - 4) ? "<li>...</li><li><a href='javascript:void(0)' title='' data-cate='". $cate_id ."' data-item='". $page ."' data-page='". $tongtrang ."'>$tongtrang</a></li>" : "";

        if($options != NULL){
            $class_paging = $options['class'];
        }else{
            $class_paging = 'paging_ajax_category';
        }

        if ($count > 0 && $tongtrang > 1)
            $link = "
        <div class='paging'><span>Nhấn trang tiếp theo để xem thêm</span><ul class='pagination ". $class_paging. "'> 
            " . $lui . $num . $pageLast . $toi . "
            
        </ul></div>
            ";
        else
            $link = '';
        return $link;
    }

    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    function get_tableWhere($where = array(), $table, $select = "*")
    {
        $this->db->select($select);
        $this->db->where($where);
        $this->db->from($table);
        return $this->db->get()->row();
    }
    function show_list_table_where($where = array(), $table, $select = "*", $order = "id", $asc = "DESC", $page = 0)
    {
        $this->db->select($select);
        $this->db->where($where);
        $this->db->order_by($order, $asc);
        $this->db->order_by('id', 'DESC');
        if ($page > 0) {
            $this->db->limit($page);
        }
        $this->db->from($table);
        return $this->db->get()->result();
    }
    function count_tableWhere($where = array(), $table)
    {
        $this->db->where($where);
        $this->db->from($table);
        return $this->db->count_all_results();
    }
    function get_row_link($id)
    {
        $this->db->select("name,slug,id");
        $this->db->where(array(
            "id" => $id,
            "status" => 1
        ));
        $this->db->from("article");
        $data = $this->db->get()->row();
        if (isset($data->id)) {
            return site_url('huong-dan/' . $data->id . "-" . $data->slug);
        } else
            return "#";
    }
    function SendMail($email = '', $text = '', $title)
    {
        $company      = $this->get_tableWhere(array(
            "id" => 1
        ), "company", "*");
        $noidung      = '<div>


        <img src="' . base_url() . 'themes/images/layout/logo_blue.png" class="CToWUd">





        <p></p>


        ' . $text . '


        <p></p>


        <div style="color:#333333;border-top:1px dotted #cccccc;padding-top:12px">


            <strong>' . $company->name . '</strong>





            <br>


            <table style="border-collapse:collapse">


                <tbody><tr>


                    <td>Trụ sở</td>


                    <td>:' . $company->address . '</td>


                </tr>


                <tr>


                    <td>Hotline cho nhà tuyển dụng</td>


                    <td>: (84-4) 7309 2828</td>


                </tr>


                <tr>


                    <td>Hotline cho người tìm việc</td>


                    <td>: (84-8) 7309 2828</td>


                </tr>


                <tr>


                    <td>Website</td>


                    <td>: <a href="' . site_url() . '" target="_blank">' . site_url() . '</a></td>


                </tr>


                <tr>


                    <td>Email</td>


                    <td>: <a href="mailto:' . $company->email . '" target="_blank">' . $company->email . '</a></td>


                </tr>


                <tr>


                    <td>Facebook</td>


                    <td>: <a href="https://www.facebook.com/tuyendungvieclam" target="_blank">https://www.facebook.com/tuyendungvieclam</a></td>


                </tr>


            </tbody></table><div class="yj6qo"></div><div class="adL">


        </div></div><div class="adL">


    </div></div>';
        $email_server = 'test@365show.biz';
        $pass_server  = 'Abc@123456';
        $host_mail    = 'smtp.365show.biz';
        $this->load->library('My_PHPMailer');
        $body = $noidung;
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet    = 'UTF-8';
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = "tls";
        $mail->Host       = $host_mail;
        $mail->Port       = 25;
        $mail->IsHTML(true);
        $mail->Username = $email_server;
        $mail->Password = $pass_server;
        $mail->SetFrom($company->email, $title);
        $mail->AddReplyTo($company->email, $title);
        $mail->AddCC("it.phamphong@gmail.com", "Thanh Phong");
        $mail->Subject = $company->name;
        $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
        $mail->MsgHTML($body);
        $mail->AddAddress($email, $company->name);
        if (!$mail->Send()) {
            return 0;
        } else {
            return 1;
        }
    }
    function upload_img($img, $folder, $w, $h)
    {
        $config['upload_path']   = './uploads/' . $folder;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name']  = TRUE;
        $this->load->library("upload", $config);
        if ($this->upload->do_upload($img)) {
            $check = $this->upload->data();
            $this->load->library("image_lib");
            $config['image_library']  = 'gd2';
            $config['source_image']   = './uploads/' . $folder . "/" . $check['file_name'];
            $config['create_thumb']   = TRUE;
            $config['maintain_ratio'] = FALSE;
            if ($check['image_width'] > $w) {
                $config['width'] = $w;
            }
            if ($check['image_height'] > $h) {
                $config['height'] = $h;
            }
            $this->image_lib->initialize($config);
            $this->image_lib->resize();
            return $check['file_name'];
        } else {
            return $this->upload->display_errors();
        }
    }
    function get_row($table, $where = array())
    {
        $this->db->where($where);
        return $this->db->get($table)->first_row();
    }
    function get_row_order($table, $where = array())
    {
        $this->db->where($where);
        $this->db->order_by("id", "DESC");
        return $this->db->get($table)->first_row();
    }
    function get_row_no_where($table)
    {
        return $this->db->get($table)->first_row();
    }
    function count_get_row($table, $where = array())
    {
        $this->db->where($where);
        return $this->db->get($table)->num_rows();
    }
    function get_row_object($where = array(), $select = "*", $table)
    {
        $this->db->select($select);
        $this->db->where($where);
        $this->db->where('status', 1);
        $this->db->from($table);
        return $this->db->get()->row();
    }
    public function get_array_object($where = array(), $arr = "*", $tpl, $limit = 0, $options = null)
    {
        $this->db->select($arr);
        $this->db->where($where);
        $this->db->where('status', 1);
        if($options != null) $this->db->order_by($options['field'], $options['sort']);
        $this->db->order_by("weight", "ASC");
        $this->db->order_by("id", "DESC");
        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get($tpl);
        $array = $query->result();
        return $array;
    }
    public function get_array_object_in($list_id, $tpl)
    {
        $this->db->select("id, name");
        $this->db->where_in('id', $list_id);
        $this->db->where('status', 1);
        $this->db->order_by("name", "ASC");
        $this->db->order_by("id", "DESC");
        $query = $this->db->get($tpl);
        $array = $query->result();
        return $array;
    }
    public function get_array_object_paging($where = array(), $arr = "*", $tpl, $limit = 0, $offset, $options = null)
    {
        $this->db->select($arr);
        $this->db->where($where);
        $this->db->where('status', 1);
        if($options != null) $this->order_by($options['field'], $options['sort']);
        $this->db->order_by("weight", "ASC");
        $this->db->order_by("id", "DESC");
        if ($limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get($tpl);
        $array = $query->result();
        return $array;
    }
    public function get_array($where = array(), $arr = "*", $tpl, $limit = 0)
    {
        $this->db->select($arr);
        $this->db->where($where);
        $this->db->where('status', 1);
        $this->db->order_by("weight", "ASC");
        $this->db->order_by("id", "DESC");
        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get($tpl);
        $array = $query->result_array();
        return $array;
    }

    public function get_province(){
        return $this->db->select('id, name, type')
                        ->where('status', 1)
                        ->order_by('weight', 'DESC')
                        ->order_by('name', 'ASC')
                        ->get('province')
                        ->result();
    }

    public function get_district($province_id){
        return $this->db->select('id, name, type')
                        ->where('status', 1)
                        ->where('province_id', $province_id)
                        ->order_by('weight', 'DESC')
                        ->order_by('name', 'ASC')
                        ->get('district')
                        ->result();
    }

    public function upload($name, $path, $ext)
    {
        if (isset($_FILES[$name]) && $_FILES[$name]["size"] > 0) {
            $file_name               = $this->my_lib->changeTitle(pathinfo($_FILES[$name]['name'], PATHINFO_FILENAME));
            $config['upload_path']   = $path;
            $config['allowed_types'] = $ext;
            $config['file_name']     = $file_name;
            $config['max_size']      = '100000';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($name)) {
                echo $this->upload->display_errors();
                $this->form_validation->set_message('upimg', $this->upload->display_errors());
                return FALSE;
            } else {
                $upload_data = $this->upload->data();
                return $photo = $upload_data;
            }
        }
    }
    public function create_thumb($path, $file, $w, $h)
    {
        $config['image_library']  = 'gd2';
        $config['source_image']   = $path . $file['file_name'];
        $config['create_thumb']   = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']          = $w;
        $config['height']         = $h;
        if ($file['image_width'] < $w) {
            $config['width'] = $file['image_width'];
        }
        if ($file['image_height'] < $h) {
            $config['height'] = $file['image_height'];
        }
        $this->load->library('image_lib', $config);
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        return $thumb = pathinfo($file['file_name'], PATHINFO_FILENAME) .'_thumb' . $file['file_ext'];
    }

    public function get_current_route(){
        $routes = array_reverse($this->router->routes); 

        foreach ($routes as $key => $val) {
            $route = $key; // Current route being checked.

            // Convert wildcards to RegEx
            $key = str_replace(array(':any', ':num'), array('[^/]+', '[0-9]+'), $key);
            // Does the RegEx match?
            if (preg_match('#^'.$key.'$#', $this->uri->segment(1), $matches)) break;
        }

        if ( ! $route) $route = $routes['default_route']; // If the route is blank, it can only be mathcing the default route.

        return $route;
    }

}
?>