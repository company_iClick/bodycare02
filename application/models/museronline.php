<?php
class Museronline extends CI_Model{

	public function __construct() {
        	parent::__construct();
    	}
	
	
	public function count_online(){

		$time = 60; // 1 phut
		$ssid =  session_id();
		$this->db->where('time <',(time()-$time));
		$this->db->delete('online');
		
		$this->db->select('id, session_id'); 
		$this->db->order_by("id", "desc");
		$query = $this->db->get('online');
		$rows = $query->result_array();
		$result['dangxem'] = $query->num_rows();
		$i = 0;
		while(isset($rows[$i]['session_id']) && ($rows[$i]['session_id'] != $ssid) && $i++ < $result['dangxem']);
		
		if($i<$result['dangxem']){
			$sql['time'] = time();
			$this->db->where('session_id', $ssid);				
			$this->db->update('online', $sql); 
			$result['daxem'] = $rows[0]['id'];
		}
		else{
			$data = array(
				'session_id'=>$ssid,
				'time'=>time()								               
			  );
			if($this->db->insert('online', $data)){
				$result['daxem'] = $this->db->insert_id();
				$result['dangxem']++;
			}
			
		}
	
		return $result; // array('dangxem'=>'', 'daxem'=>'') 
	} 
	

	   

 }

?>