<?php
class General extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function get_row($table, $where = array())
    {
        $this->db->select("*");
        $this->db->where($where);
        $this->db->from($table);
        return $this->db->get()->row();
    }
    function get_max($table, $var)
    {
        $this->db->select_max($var);
        $this->db->from($table);
        return $this->db->get()->row()->$var;
    }
    function paging($page, $total, $url, $id = 1)
    {
        $previous_btn = true;
        $next_btn     = true;
        $first_btn    = true;
        $last_btn     = true;
        $count        = $total;
        $tongtrang    = ceil($total / $page);
        $num          = "";
        if ($count != 0) {
            if ($id >= 7) {
                $start_loop = $id - 4;
                if ($tongtrang > $id + 4)
                    $end_loop = $id + 4;
                else if ($id <= $tongtrang && $id > $tongtrang - 6) {
                    $start_loop = $tongtrang - 6;
                    $end_loop   = $tongtrang;
                } else {
                    $end_loop = $tongtrang;
                }
            } else {
                $start_loop = 1;
                if ($tongtrang > 7)
                    $end_loop = 7;
                else
                    $end_loop = $tongtrang;
            }
        }
        if ($first_btn && $id > 1) {
            $dau = "<li  class=''><a href='" . site_url($url) . "'>Đầu</a></li>";
        } else if ($first_btn) {
            $dau = "<li  class='text'>Đầu</li>";
        }
        if ($previous_btn && $id > 1) {
            $tam = $id - 1;
            $lui = "<li class=''><a href='" . site_url($url . $tam) . "'>Lùi</a></li>";
        } else if ($previous_btn) {
            $lui = "<li class='text'>Lùi</li>";
        }
        if ($next_btn && $id < $tongtrang) {
            $tam2 = $id + 1;
            $toi  = "<li class=''><a href='" . site_url($url . $tam2) . "'> Tới </a></li>";
        } else if ($next_btn) {
            $toi = "<li class='text'>Tới</li>";
        }
        if ($last_btn && $id < $tongtrang) {
            $cuoi = "<li  class=''><a href='" . site_url($url . $tongtrang) . "'> Cuối </a></li>";
        } else if ($last_btn) {
            $cuoi = "<li class='text'>Cuối</li>";
        }
        if ($count > 0) {
            for ($i = $start_loop; $i <= $end_loop; $i++) {
                if ($i == $id)
                    $num .= "<li class='page'><a href='#' title='' onclick='return false'>$i</a></li>";
                else
                    $num .= "<li><a href='" . site_url($url . $i) . "' title=''>$i</a></li>";
            }
        }
        if ($count > 0 && $tongtrang > 1)
            $link = "

        <ul class='pagination'>

            

            " . $dau . $lui . $num . $toi . $cuoi . "

            

        </ul>

            ";
        else
            $link = '';
        return $link;
    }
    function show_list_lang()
    {
        $this->db->where("status", 1);
        return $this->db->get('country')->result();
    }
    function show_company($lang = "vn")
    {
        $this->db->select('company.*');
        $this->db->from('company');
        return $this->db->get()->row();
    }
    function show_list_table($where = array(), $limit, $offset, $table, $page = 0)
    {
        $this->db->select("*");
        $this->db->where($where);
        $this->db->order_by('weight', "DESC");
        if ($page != 0)
            $this->db->limit($limit, $offset);
        $this->db->from($table);
        return $this->db->get()->result();
    }
    function show_list_table_where($where = array(), $table)
    {
        $this->db->select();
        $this->db->where($where);
        $this->db->from($table);
        return $this->db->get()->result();
    }
    function get_tableID($id, $table)
    {
        $this->db->select("*");
        $this->db->where($table . '.id', $id);
        $this->db->from($table);
        return $this->db->get()->row();
    }
    function get_tableWhere($where = array(), $table)
    {
        $this->db->select("*");
        $this->db->where($where);
        $this->db->from($table);
        return $this->db->get()->row();
    }
    function get_list_table($table)
    {
        $this->db->select("*");
        $this->db->from($table);
        return $this->db->get()->result();
    }
    function get_list_table_select($table, $select, $o, $t)
    {
        $this->db->select($select);
        $this->db->where("idtuyendung >", $o);
        $this->db->where("idtuyendung <=", $t);
        $this->db->from($table);
        return $this->db->get()->result();
    }
    function update_tableID($id, $sql = array(), $table)
    {
        $this->db->where('id', $id);
        $this->db->update($table, $sql);
    }
    function count_tableWhere($where = array(), $table)
    {
        $this->db->where($where);
        $this->db->from($table);
        return $this->db->get()->num_rows();
    }
    function count_table_where($where = array(), $table)
    {
        $this->db->where($where);
        $this->db->from($table);
        return $this->db->get()->num_rows();
    }
    function admin_detail($id)
    {
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from('tbl_user');
        return $this->db->get()->row();
    }
    function Checkpermission($name)
    {
        if (!$this->session->userdata('active_log')) {
            redirect(site_url('admin/login'));
        }
        $check = $this->get_row("tbl_user", array(
            "id" => $this->session->userdata('admin_login')->id
        ));
        $array = array();
        $array = explode(",", base64_decode($check->permission));
        foreach ($array as $r) {
            if ($r == $name || $this->session->userdata('admin_login')->type == 2)
                return true;
        }
        return false;
    }
    function Checkpermission_check($id, $name)
    {
        if (!$this->session->userdata('active_log')) {
            redirect(site_url('admin/login'));
        }
        $check = $this->get_tableWhere(array(
            "id" => $id
        ), "tbl_user");
        $array = array();
        $array = explode(",", base64_decode($check->permission));
        foreach ($array as $r) {
            if ($r == $name)
                return 1;
        }
        return 0;
    }
    function get_admin($check, $t, $p)
    {
        $this->db->where('user_loginname', $t);
        $this->db->where('user_password', hash('sha256', $p));
        if ($check == 1)
            return $this->db->get('tbl_user')->first_row();
        else
            return $this->db->get('tbl_user')->num_rows();
    }
    function login($user, $pass)
    {
        $this->db->where('user_loginname', trim($user))->where('user_password', hash('sha256', $pass));
        $this->db->where('user_status', 1, true);
        return $this->db->get('tbl_user');
    }
    function UpdateAll($table)
    {
        foreach ($this->get_list_table($table) as $t) {
            $this->db->where("id", $t->id);
            $this->db->update($table, array(
                "slug" => $this->global_function->unicode($t->name)
            ));
        }
    }
}

