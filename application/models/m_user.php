<?php



class M_user extends CI_Model {



    function __construct() {

        parent::__construct();

    }



    /* check user_loginname đã tồn tại hay chưa 

      ------------------------------------------------------------- */



    function check_user_loginname($user_loginname) {

        return $this->db->where('user_loginname', $user_loginname)->get('tbl_user')->num_rows();

    }



    /* check user_loginname có tồn tại trong CSDL hay không

      ------------------------------------------------------------- */



    function users_forgot($user_loginname) {

        return $this->db->where('user_loginname', $user_loginname)->get('tbl_user');

    }



    /* check login

      ------------------------------------------------------------- */

    function login($check, $t, $p) {

        $this->db->where('user_loginname', $t);

        $this->db->where('user_password', md5($p));

        //$this->db->where('group',0);

        if ($check == 1)

            return $this->db->get('tbl_user')->first_row();

        else

            return $this->db->get('tbl_user')->num_rows();

    }

    function login_mod($code, $p) {

        $this->db->select("tbl_user.user_id");

        $this->db->or_where('user_email', $code);

        $this->db->or_where('user_loginname', $code);

        $this->db->where('user_status', 1);

        $this->db->where('user_password', md5($p));

        return $this->db->get('tbl_user')->row();

    }

    function login_customer($code, $p) {

        $this->db->select("users.id");

        $this->db->where('email', $code);

        $this->db->where('status', 1);

        $this->db->where('buyer_id', 0);

        $this->db->where('password', md5($p));

        return $this->db->get('users')->row();

    }

    /* check cookie

      ------------------------------------------------------------- */

    function cookie($user_loginname, $user_passwordword) {

        $this->db->where('user_loginname', $user_loginname);

        $this->db->where('user_password', $user_passwordword);

        $this->db->where('group', 2);

        return $this->db->get('tbl_user');

    }



    /* check active

      ------------------------------------------------------------- */



    function check_active($iduser, $key) {

        $this->db->select('id , key , status');

        $this->db->where('id', $iduser);

        $this->db->where('key', $key);

        return $this->db->get('users');

    }



    /* lấy thông tin user từ db

      ------------------------------------------------------------- */



    /* check thay doi user_passwordword

      --------------------------------------- */



    function check_user_password($user_password) {

        $this->db->where('user_id', $this->session->userdata('tbl_user')->id);

        $this->db->where('user_password', $user_password);

        return $this->db->count_all_results('tbl_user');

    }



    function get_list_user($where) {

        $this->db->where($where);

        $this->db->from('tbl_customers');

        return $this->db->get()->result();

    }



    function get_list_user_export($where) {

        $this->db->where($where);

        $this->db->from('tbl_customers');

        return $this->db->get();

    }



    // set session

    function set_login_customer($code, $p) {

        $this->db->where('user_email', $code);

        $this->db->where('status', 1);



        $this->db->where('user_password', $p);

        return $this->db->get('users')->row();

    }



    function set_login_customer_update($code, $p) {

        $this->db->select("users.id");

        $this->db->where('email', $code);

        $this->db->where('status', 1);

        $this->db->where('buyer_id', 0);

        $this->db->where('password', $p);

        return $this->db->get('users')->row();

    }



    function set_session_user_update_info($code) {

        $this->db->select("user_email,user_name,user_address,user_phone,avatar,time_work,states_id,agent_id,type_member,users.id,type_account,money,last_login, fid");

        $this->db->where('user_email', $code);

        $this->db->where('status', 1);

        return $this->db->get('users')->row();

    }



}



// end index

?>