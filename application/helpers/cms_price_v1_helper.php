<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('cms_price_v1'))
{
	function cms_price_v1($sale, $price, $unit = 'đ', $option = null){
		
		$priceHtml = null;
		if($sale > 0 && $price > 0){
			$priceFormat = number_format($price, 0, ',', '.') . ' ' . $unit;			
			$saleFormat = number_format($sale, 0, ',', '.') . $unit;
			

			$priceHtml = sprintf('<p class="price_v1_old">%s</p><p class="price_v1">%s</p>', $priceFormat, $saleFormat);
		}

		if($sale == 0 && $price > 0){
			$priceFormat = number_format($price, 0, ',', '.') . $unit;
			$priceHtml = sprintf('<p class="price_v1 mgrt_10">%s</p>', $priceFormat);
		}

		if($price == 0){
			$priceFormat = 'Liên hệ';
			$priceHtml = sprintf('<p class="price_v1 mgrt_10">%s</p>', $priceFormat);
		}
		
		return $priceHtml;
	}
}

