<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('percen_calculate_v1'))
{
	function percen_calculate_v1($x, $divisor, $option = null){
		
		$percentHtml = null;
		if(is_numeric($x) && is_numeric($divisor)){
			if($x > 0 && $x < $divisor){
				$percent = ($x / $divisor) * 100;
				$percent = round($percent, 0, PHP_ROUND_HALF_UP);
				$percent = round($percent, 0);
				$percent = 100 - $percent;
				
				$percentHtml = sprintf('<p class="%s">%s</p>', $option['class'], '-' . $percent . '%');
			}else{
				$percentHtml = null;
			}

		}

		
		return $percentHtml;
	}
}

