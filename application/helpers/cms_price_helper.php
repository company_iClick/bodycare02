<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('cms_price'))
{
	function cms_price($sale, $price, $unit = 'đ', $option = null){
		
		$priceHtml = null;
		if($sale > 0 && $price > 0){			
			$saleFormat = number_format($sale, 0, ',', '.')  . $unit;
			$priceFormat = number_format($price, 0, ',', '.') . $unit;

			$priceHtml = sprintf('<div class="wp_price">
			                        <p class="promotion_price_old">%s</p>
			                        <p class="promotion_price_news">%s</p>
			                    </div>', $priceFormat, $saleFormat);
		}

		if($sale == 0 && $price > 0){
			$priceFormat = number_format($price, 0, ',', '.') . $unit;
			$priceHtml = sprintf('<div class="wp_price">
			                        <p class="promotion_price">%s</p>
			                    </div>', $priceFormat);
		}

		if($price == 0){
			$priceFormat = 'Liên hệ';
			$priceHtml = sprintf('<div class="wp_price">
			                        <p class="promotion_price">%s</p>
			                    </div>', $priceFormat);
		}
		
		return $priceHtml;
	}
}

