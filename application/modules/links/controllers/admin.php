<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array("url"));
        $this->load->model(array("links/m_links", "general", "m_session",));
        $this->template->set_template('admin');        // Set template 
        $this->template->write('mod', "links"); // set mod
    }

    function index($page_no = 1) {
        //echo $page_no;exit;
        if (!($this->general->Checkpermission("view_links")))
            redirect(site_url("admin/not-permission"));
        // tool all
        if (isset($_POST['show']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(site_url('admin/links/'.$page_no) . '?messager=success');
        }
        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                //--------change parent------
                $this->hide_more($a);
            }
            redirect(site_url('admin/links/'.$page_no) . '?messager=success');
        }
        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                //--------change parent------
                $this->delete_more($a);
            }
            redirect(site_url('admin/links/'.$page_no) . '?messager=success');
        }
        //end toll
        $page_co = 20;
        $start = ($page_no - 1) * $page_co;
        $count = $this->general->count_table_where(array('id !=' => 0), 'links');
        //echo $count;
        $data['page_no'] = $page_no;
        $data['item'] = $this->m_links->show_list_links(array('id !=' => 0), $page_co, $start);
        //var_dump($data['item']);exit();
        $data['link'] = $this->general->paging($page_co, $count, 'admin/links/', $page_no);
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();
    }
    function add() {
        if (!($this->general->Checkpermission("add_links")))
            redirect(site_url("admin/not-permission"));
        $data = array();
        $data['breadcrumb'] = '<li>>></li><li><a href="back/links">Loại Bài Viết</a></li><li>>></li><li class="current">Thêm mới</li>';
        if (isset($_POST['ok'])) {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required|max_length[100]');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'slug' => $this->global_function->unicode($this->input->post('name')),
                    'status' => $this->input->post('status'),
                    'links' => $this->input->post('links'),
                );
                $this->db->insert('links', $sql);
                $id_category = $this->db->insert_id();
                redirect(site_url('admin/links') . '?messager=success');
            }
        }
        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }

    function edit($id) {
        if (!($this->general->Checkpermission("edit_links")))
            redirect(site_url("admin/not-permission"));
        if (isset($_POST['ok'])) {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required|max_length[100]');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'slug' => $this->global_function->unicode($this->input->post('name')),
                    'status' => $this->input->post('status'),
                    'links' => $this->input->post('links'),
                );
                $this->db->where('id', $id);
                $this->db->update('links', $sql);
                redirect(site_url('admin/links/edit/' . $id) . '?messager=success');
            }
        }
        $data['item'] = $this->m_links->get_linksID($id);
        $this->template->write_view('content', 'admin/edit', $data, TRUE);
        $this->template->render();
    }


//=========================================== 
    function hide($id,$page_no) {
        //exit($id);
        if (!($this->general->Checkpermission("edit_links")))
            redirect(site_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 0), "links");
        redirect(site_url('admin/links/' . $page_no) . '?messager=success');
    }

//============================================\
    function hide_more($id) {
        //echo $id; exit($id);
        if (!($this->general->Checkpermission("edit_links")))
            redirect(site_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 0), "links");
        return true;
    }

//============================================\
    function show_more($id) {
        //	exit($id);
        if (!($this->general->Checkpermission("edit_links")))
            redirect(site_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 1), "links");
        return true;
    }

//============================================\
    function show($id,$page_no) {
        if (!($this->general->Checkpermission("edit_links")))
            redirect(site_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 1), "links");
        redirect(site_url('admin/links/' . $page_no) . '?messager=success');
    }

// ============================================
    function delete($id,$page_no) {
        //exit($id);
        if (!($this->general->Checkpermission("delete_links")))
            redirect(site_url("admin/not-permission"));
            $this->db->delete('links', array('id' => $id));
            redirect(site_url('admin/links/'.$page_no) . '?messager=success');

    }

    function delete_more($id) {
        if (!($this->general->Checkpermission("delete_links")))
            redirect(site_url("admin/not-permission"));
            $this->db->delete('links', array('id' => $id));
            return true;
    }
}
