<?php
class M_action extends CI_Model
{
    protected $_table = 'action';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_action($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function get_actionID($id)
    {
        $this->db->where('id', $id);
        $this->db->from($this->_table);
        return $this->db->get()->row();
    }    
    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    function count_product($action_id)
    {
         $this->db->select('product.id');
         $this->db->where('tmp_product_action.action_id', $action_id);
         $this->db->group_by('product.id');
         $this->db->from('product');
         $this->db->join('tmp_product_action', 'tmp_product_action.product_id=product.id');
         return $this->db->count_all_results();
    }
}