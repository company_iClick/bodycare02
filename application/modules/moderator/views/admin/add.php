<link type="text/css" href="<?php echo base_url() ?>theme_admin/js/date/jquery-ui.css" rel="stylesheet"/>
<script>
    $(document).ready(function() {
        $('#searchStartDate').datepicker({
            dateFormat: 'dd/mm/yy',
        });
    });
</script>

<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
?>
<div class="contentcontainer med left">

    <div class="headings altheading"><h2>Thêm mới</h2></div>

    <div class="contentbox">

        <form name="them" method="post" id="them" action=""	enctype="multipart/form-data">

            <p>
            <table width="100%">
                <tr>
                    <td><b>Tài khoản đăng nhập</b></td>
                    <td><input type="text" id="textfield_code" class="inputbox"  name="user_loginname" value="<?= set_value("user_loginname") ?>"/>
                        <?php echo form_error('user_loginname'); ?></td>
                </tr>


                <tr>
                    <td><b>Password</b></td>
                    <td>
                        <input type="password" id="textfield" class="inputbox"  name="password" value="<?= set_value("password") ?>"/>
                        <?php echo form_error('password'); ?>
                    </td>
                </tr>
                <tr>
                    <td width="130"><b>Re-Password</b></td>
                    <td><input type="password" id="textfield" class="inputbox"  name="re_password" value="<?= set_value("re_password") ?>"/>
                        <?php echo form_error('re_password'); ?></td>
                </tr>
                <tr>
                    <td width="130"><b>Họ và tên</b></td>
                    <td><input type="text" id="textfield" class="inputbox"  name="user_name" value="<?= set_value("user_name") ?>"/>
                        <?php echo form_error('user_name'); ?></td>
                </tr>
                <tr>
                    <td width="130"><b>Địa chỉ</b></td>
                    <td><input type="text" id="textfield" class="inputbox"  name="user_address" value="<?= set_value("user_address") ?>"/>
                        <?php echo form_error('user_address'); ?></td>
                </tr>

                <tr>
                    <td width="130"><b>Điện thoại</b></td>
                    <td><input type="text" id="textfield" class="inputbox"  name="user_phone" value="<?= set_value("user_phone") ?>"/>
                        <?php echo form_error('user_phone'); ?></td>
                </tr>

                <tr>
                    <td width="130"><b>Email</b></td>
                    <td><input type="text" id="email" class="inputbox"  name="user_email" value="<?= set_value("user_email") ?>"/>
                        <?php echo form_error('user_email'); ?></td>
                </tr> 
                <input type="hidden" name="id" value="0"/>
                <tr>
                    <td><b>Kích hoạt</b></td>
                    <td>
                        <input type="radio" name="user_status" class="anhien" value="0" />
                        Không
                        <input type="radio" name="user_status" class="anhien" value="1" checked="checked" />
                        Có
                    </td>
                </tr>
   
                <tr>
                    <td colspan="2" class="button_add">
                        <input type="submit" value="Thêm mới" class="btn" name="ok" id="ok" />
                        <input type="reset" value="Nhập lại" class="btnalt" />
                    </td>
                </tr>
            </table>
            <input type="hidden" value="0" name="user_id">
        </form>
    </div><!-- end contentbox -->

</div>
