<link type="text/css" href="<?php echo base_url() ?>theme_admin/js/date/jquery-ui.css" rel="stylesheet"/>
<script>
    $(document).ready(function() {
        $('#searchStartDate').datepicker({
            dateFormat: 'dd/mm/yy',
        });
    });
</script>
<style>
    .w200{ float: left; width: 150px}
    fieldset{ border: solid 1px #eee}
    legend{ font-weight: bold}
    .contentbox table tr td{ float: left}

</style>
<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
?>
<div class="contentcontainer med left">

    <div class="headings altheading"><h2>Permission</h2></div>

    <div class="contentbox">

        <form name="them" method="post" id="them" action=""	enctype="multipart/form-data">
            <p>
                <?php
                for ($i = 1; $i <= 4; $i++) {
                    switch ($i) {
                        case 1:$title = "Hướng dẫn mua hàng";
                            break;
                        case 2:$title = "Khuyến mãi";
                            break;
                        case 3:$title = "Kiến thức làm đẹp";
                            break;
                        case 4:$title = "Hỗ trợ khách hàng";
                            break;
                    }
                    ?>
                    <?php if($i != 2) {?>
                    <fieldset>
                        <legend><?php echo $title ?></legend>
                    </fieldset>
                    <?php } ?>
                    <table width="100%">
                        <?php if($i == 3) {?>
                        <tr>

                            <td class="w200"><label>Nhóm</label></td>
                            <td><input type="checkbox" name="level[]" value="view_term_<?php echo $i ?>" <?php if ($this->general->Checkpermission_check($id, "view_term_" . $i) == 1) { ?>  checked="checked"<?php } ?>> View</td>
                            <td><input type="checkbox" name="level[]" value="add_term_<?php echo $i ?>" <?php if ($this->general->Checkpermission_check($id, "add_term_" . $i) == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                            <td><input type="checkbox" name="level[]" value="edit_term_<?php echo $i ?>" <?php if ($this->general->Checkpermission_check($id, "edit_term_" . $i) == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                            <td><input type="checkbox" name="level[]" value="delete__<?php echo $i ?>" <?php if ($this->general->Checkpermission_check($id, "delete_term_" . $i) == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                        </tr>
                        <?php }?>
                        <?php if($i != 2) {?>
                        <tr>

                            <td class="w200"><label>Danh sách</label></td>
                            <td><input type="checkbox" name="level[]" value="view_article_<?php echo $i ?>" <?php if ($this->general->Checkpermission_check($id, "view_article_" . $i) == 1) { ?>  checked="checked"<?php } ?>> View</td>
                            <td><input type="checkbox" name="level[]" value="add_article_<?php echo $i ?>" <?php if ($this->general->Checkpermission_check($id, "add_article_" . $i) == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                            <td><input type="checkbox" name="level[]" value="edit_article_<?php echo $i ?>" <?php if ($this->general->Checkpermission_check($id, "edit_article_" . $i) == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                            <td><input type="checkbox" name="level[]" value="delete_article_<?php echo $i ?>" <?php if ($this->general->Checkpermission_check($id, "delete_article_" . $i) == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                        </tr>
                        <?php } ?>
                    </table>
                </fieldset>
            <?php } ?>
            <fieldset>
                <legend>Sản phẩm</legend>
                <table width="100%">
                    <tr>

                        <td class="w200"><label>Danh mục</label></td>
                        <td><input type="checkbox" name="level[]" value="view_category" <?php if ($this->general->Checkpermission_check($id, "view_category") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_category" <?php if ($this->general->Checkpermission_check($id, "add_category") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_category" <?php if ($this->general->Checkpermission_check($id, "edit_category") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_category" <?php if ($this->general->Checkpermission_check($id, "delete_category") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Thương hiệu</label></td>
                        <td><input type="checkbox" name="level[]" value="view_brand" <?php if ($this->general->Checkpermission_check($id, "view_brand") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_brand" <?php if ($this->general->Checkpermission_check($id, "add_brand") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_brand" <?php if ($this->general->Checkpermission_check($id, "edit_brand") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_brand" <?php if ($this->general->Checkpermission_check($id, "delete_brand") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Loại sản phẩm</label></td>
                        <td><input type="checkbox" name="level[]" value="view_product_type" <?php if ($this->general->Checkpermission_check($id, "view_product_type") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_product_type" <?php if ($this->general->Checkpermission_check($id, "add_product_type") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_product_type" <?php if ($this->general->Checkpermission_check($id, "edit_product_type") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_product_type" <?php if ($this->general->Checkpermission_check($id, "delete_product_type") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Loại da</label></td>
                        <td><input type="checkbox" name="level[]" value="view_skin_type" <?php if ($this->general->Checkpermission_check($id, "view_skin_type") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_skin_type" <?php if ($this->general->Checkpermission_check($id, "add_skin_type") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_skin_type" <?php if ($this->general->Checkpermission_check($id, "edit_skin_type") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_skin_type" <?php if ($this->general->Checkpermission_check($id, "delete_skin_type") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Loại tóc</label></td>
                        <td><input type="checkbox" name="level[]" value="view_hair_type" <?php if ($this->general->Checkpermission_check($id, "view_hair_type") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_hair_type" <?php if ($this->general->Checkpermission_check($id, "add_hair_type") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_hair_type" <?php if ($this->general->Checkpermission_check($id, "edit_hair_type") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_hair_type" <?php if ($this->general->Checkpermission_check($id, "delete_hair_type") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Xuất xứ</label></td>
                        <td><input type="checkbox" name="level[]" value="view_origin" <?php if ($this->general->Checkpermission_check($id, "view_origin") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_origin" <?php if ($this->general->Checkpermission_check($id, "add_origin") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_origin" <?php if ($this->general->Checkpermission_check($id, "edit_origin") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_origin" <?php if ($this->general->Checkpermission_check($id, "delete_origin") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Chức năng</label></td>
                        <td><input type="checkbox" name="level[]" value="view_action" <?php if ($this->general->Checkpermission_check($id, "view_action") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_action" <?php if ($this->general->Checkpermission_check($id, "add_action") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_action" <?php if ($this->general->Checkpermission_check($id, "edit_action") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_action" <?php if ($this->general->Checkpermission_check($id, "delete_action") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Dung tích</label></td>
                        <td><input type="checkbox" name="level[]" value="view_capacity" <?php if ($this->general->Checkpermission_check($id, "view_capacity") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_capacity" <?php if ($this->general->Checkpermission_check($id, "add_capacity") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_capacity" <?php if ($this->general->Checkpermission_check($id, "edit_capacity") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_capacity" <?php if ($this->general->Checkpermission_check($id, "delete_capacity") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Trọng lượng</label></td>
                        <td><input type="checkbox" name="level[]" value="view_weigh" <?php if ($this->general->Checkpermission_check($id, "view_weigh") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_weigh" <?php if ($this->general->Checkpermission_check($id, "add_weigh") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_weigh" <?php if ($this->general->Checkpermission_check($id, "edit_weigh") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_weigh" <?php if ($this->general->Checkpermission_check($id, "delete_weigh") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Số lượng viên</label></td>
                        <td><input type="checkbox" name="level[]" value="view_pill_number" <?php if ($this->general->Checkpermission_check($id, "view_pill_number") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_pill_number" <?php if ($this->general->Checkpermission_check($id, "add_pill_number") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_pill_number" <?php if ($this->general->Checkpermission_check($id, "edit_pill_number") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_pill_number" <?php if ($this->general->Checkpermission_check($id, "delete_pill_number") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Tags</label></td>
                        <td><input type="checkbox" name="level[]" value="view_tags" <?php if ($this->general->Checkpermission_check($id, "view_tags") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_tags" <?php if ($this->general->Checkpermission_check($id, "add_tags") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_tags" <?php if ($this->general->Checkpermission_check($id, "edit_tags") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_tags" <?php if ($this->general->Checkpermission_check($id, "delete_tags") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Khoảng giá tìm kiếm</label></td>
                        <td><input type="checkbox" name="level[]" value="view_price_search" <?php if ($this->general->Checkpermission_check($id, "view_price_search") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_price_search" <?php if ($this->general->Checkpermission_check($id, "add_price_search") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_price_search" <?php if ($this->general->Checkpermission_check($id, "edit_price_search") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_price_search" <?php if ($this->general->Checkpermission_check($id, "delete_price_search") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>

                    <tr>
                        <td class="w200"><label>Sản phẩm</label></td>
                        <td><input type="checkbox" name="level[]" value="view_product" <?php if ($this->general->Checkpermission_check($id, "view_product") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_product" <?php if ($this->general->Checkpermission_check($id, "add_product") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_product" <?php if ($this->general->Checkpermission_check($id, "edit_product") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_product" <?php if ($this->general->Checkpermission_check($id, "delete_product") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <legend>Banner</legend>
                <table width="100%">
                    <tr>

                        <td class="w200"><label>Banner</label></td>
                        <td><input type="checkbox" name="level[]" value="view_banner" <?php if ($this->general->Checkpermission_check($id, "view_banner") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_banner" <?php if ($this->general->Checkpermission_check($id, "add_banner") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_banner" <?php if ($this->general->Checkpermission_check($id, "edit_banner") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_banner" <?php if ($this->general->Checkpermission_check($id, "delete_banner") == 1) { ?>  checked="checked"<?php } ?>> Delete</td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <legend>Video</legend>
                <table width="100%">
                    <tr>

                        <td class="w200"><label>Video</label></td>
                        <td><input type="checkbox" name="level[]" value="view_video" <?php if ($this->general->Checkpermission_check($id, "view_video") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_video" <?php if ($this->general->Checkpermission_check($id, "add_video") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_video" <?php if ($this->general->Checkpermission_check($id, "edit_video") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_video" <?php if ($this->general->Checkpermission_check($id, "delete_video") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <legend>Thông báo</legend>
                <table width="100%">

                    <tr>

                        <td class="w200"><label>Đơn hàng</label></td>
                        <td><input type="checkbox" name="level[]" value="view_order" <?php if ($this->general->Checkpermission_check($id, "view_order") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="edit_order" <?php if ($this->general->Checkpermission_check($id, "edit_order") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>

                    </tr>
                </table>
            </fieldset>
           
            <fieldset>
                <legend>Bình luận</legend>
                <table width="100%">
                    <tr>
                        <td class="w200"><label>Nhân viên hỗ trợ</label></td>
                        <td><input type="checkbox" name="level[]" value="view_support_staff" <?php if ($this->general->Checkpermission_check($id, "view_support_staff") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_support_staff" <?php if ($this->general->Checkpermission_check($id, "add_support_staff") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_support_staff" <?php if ($this->general->Checkpermission_check($id, "edit_support_staff") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_support_staff" <?php if ($this->general->Checkpermission_check($id, "delete_support_staff") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>

                    </tr>

                    <tr>
                        <td class="w200"><label>Danh sách</label></td>
                        <td><input type="checkbox" name="level[]" value="view_comment" <?php if ($this->general->Checkpermission_check($id, "view_comment") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_comment" <?php if ($this->general->Checkpermission_check($id, "add_comment") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_comment" <?php if ($this->general->Checkpermission_check($id, "edit_comment") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_comment" <?php if ($this->general->Checkpermission_check($id, "delete_comment") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>

                    </tr>

                </table>
            </fieldset>

            <fieldset>
                <legend>Yêu cầu tư vấn</legend>
                <table width="100%">
                    <tr>
                        <td class="w200"><label>Yêu cầu tư vấn sản phẩm</label></td>
                        <td><input type="checkbox" name="level[]" value="view_call_back" <?php if ($this->general->Checkpermission_check($id, "view_call_back") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_call_back" <?php if ($this->general->Checkpermission_check($id, "add_call_back") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_call_back" <?php if ($this->general->Checkpermission_check($id, "edit_call_back") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_call_back" <?php if ($this->general->Checkpermission_check($id, "delete_call_back") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>

                    </tr>

                    <tr>
                        <td class="w200"><label>Hỗ trợ tư vấn</label></td>
                        <td><input type="checkbox" name="level[]" value="view_advisory_request" <?php if ($this->general->Checkpermission_check($id, "view_advisory_request") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_advisory_request" <?php if ($this->general->Checkpermission_check($id, "add_advisory_request") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_advisory_request" <?php if ($this->general->Checkpermission_check($id, "edit_advisory_request") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_advisory_request" <?php if ($this->general->Checkpermission_check($id, "delete_advisory_request") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>

                    </tr>

                </table>
            </fieldset>

            <fieldset>
                <legend>Ý kiến khách hàng</legend>
                <table width="100%">
                    <tr>

                        <td class="w200"><label>Danh sách</label></td>
                        <td><input type="checkbox" name="level[]" value="view_customer_reviews" <?php if ($this->general->Checkpermission_check($id, "view_customer_reviews") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_customer_reviews" <?php if ($this->general->Checkpermission_check($id, "add_customer_reviews") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_customer_reviews" <?php if ($this->general->Checkpermission_check($id, "edit_customer_reviews") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_customer_reviews" <?php if ($this->general->Checkpermission_check($id, "delete_customer_reviews") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                    </tr>
                </table>
            </fieldset>

            <fieldset>
                <legend>Email khuyến mãi</legend>
                <table width="100%">
                    <tr>

                        <td class="w200"><label>Danh sách</label></td>
                        <td><input type="checkbox" name="level[]" value="view_newsletter" <?php if ($this->general->Checkpermission_check($id, "view_newsletter") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_newsletter" <?php if ($this->general->Checkpermission_check($id, "add_newsletter") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_newsletter" <?php if ($this->general->Checkpermission_check($id, "edit_newsletter") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_newsletter" <?php if ($this->general->Checkpermission_check($id, "delete_newsletter") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                    </tr>
                </table>
            </fieldset>

            <fieldset>
                <legend>Cấu hình</legend>
                <table width="100%">

                    <tr>

                        <td class="w200"><label>Cấu hình</label></td>
                        <td><input type="checkbox" name="level[]" value="view_company" <?php if ($this->general->Checkpermission_check($id, "view_company") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="edit_company" <?php if ($this->general->Checkpermission_check($id, "edit_company") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>

                    </tr>

                </table>
            </fieldset>
            <fieldset>
                <legend>Thành viên</legend>
                <table width="100%">
                    <tr>

                        <td class="w200"><label>Quản lý</label></td>
                        <td><input type="checkbox" name="level[]" value="view_moderator" <?php if ($this->general->Checkpermission_check($id, "view_moderator") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_moderator" <?php if ($this->general->Checkpermission_check($id, "add_moderator") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_moderator" <?php if ($this->general->Checkpermission_check($id, "edit_moderator") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="per_moderator" <?php if ($this->general->Checkpermission_check($id, "edit_moderator") == 1) { ?>  checked="checked"<?php } ?>> Permission</td>

                    </tr>

                    <tr>

                        <td class="w200"><label>Thành viên</label></td>
                        <td><input type="checkbox" name="level[]" value="view_user" <?php if ($this->general->Checkpermission_check($id, "view_user") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_user" <?php if ($this->general->Checkpermission_check($id, "add_user") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_user" <?php if ($this->general->Checkpermission_check($id, "edit_user") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>

                    </tr>
                    
                    <tr>

                        <td class="w200"><label>Ý kiến phản hồi</label></td>
                        <td><input type="checkbox" name="level[]" value="view_feedback" <?php if ($this->general->Checkpermission_check($id, "view_feedback") == 1) { ?>  checked="checked"<?php } ?>> View</td>
                        <td><input type="checkbox" name="level[]" value="add_feedback" <?php if ($this->general->Checkpermission_check($id, "add_feedback") == 1) { ?>  checked="checked"<?php } ?>> Add</td>
                        <td><input type="checkbox" name="level[]" value="edit_feedback" <?php if ($this->general->Checkpermission_check($id, "edit_feedback") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                        <td><input type="checkbox" name="level[]" value="delete_feedback" <?php if ($this->general->Checkpermission_check($id, "delete_feedback") == 1) { ?>  checked="checked"<?php } ?>> Edit</td>
                    </tr>

                </table>
            </fieldset>
            <table width="100%">



                <tr>
                    <td colspan="2" class="button_add">
                        <input type="submit" value="Cập nhật" class="btn" name="ok" id="ok" />
                    </td>
                </tr>
            </table>
            <input type="hidden" value="0" name="user_id">
        </form>
    </div><!-- end contentbox -->

</div>
