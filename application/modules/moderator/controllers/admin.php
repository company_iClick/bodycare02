<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('session', 'cart'));
        $this->load->library('form_validation');
        $this->load->helper(array("url"));
        $this->load->model(array("moderator/m_moderator", "general", "global_function", "counter"));
        $this->template->set_template('admin');        // Set template 
        $this->template->write('mod', "moderator"); // set mod
    }

    function index($page_no = 1) {
        if ($this->session->userdata('admin_login')->type != 2)
            redirect(site_url("admin/not-permission"));;
        if (!($this->general->Checkpermission("view"))) {
            redirect(site_url("admin/not-permission"));
        } else {
// tool all    
            if (isset($_POST['show']) && $this->input->post('checkall') != "") {
                $array = array_keys($this->input->post('checkall'));
                foreach ($array as $a) {
                    $this->show_more($a);
                }
                redirect(site_url('admin/moderator') . '?messager=success');
            }
            if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
                $array = array_keys($this->input->post('checkall'));
                foreach ($array as $a) {
//--------change parent------
                    $this->hide_more($a);
                }
                redirect(site_url('admin/moderator') . '?messager=success');
            }
            if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
                $array = array_keys($this->input->post('checkall'));
                foreach ($array as $a) {
//--------change parent------
                    $this->delete_more($a);
                }
                redirect(site_url('admin/moderator') . '?messager=success');
            }
//end toll
            $page_co = 20;
            $start = ($page_no - 1) * $page_co;
            $count = $this->counter->count_table_where(array("type" => 1), "tbl_user");
            $data['page_no'] = $page_no;
            $data['list'] = $this->m_moderator->show_list_user_where($page_co, $start);
            $data['link'] = $this->general->paging($page_co, $count, 'admin/user' . "/", $page_no);
            $this->template->write_view('content', 'admin/index', $data, TRUE);
            $this->template->render();
        }
    }

    function add() {
        if (!($this->general->Checkpermission("add"))) {
            redirect(site_url("admin/not-permission"));
        } else {
            $data['ma'] = $this->global_function->randomPassword(5) . (str_replace("-", "", date("y-m-d")));
            $data['breadcrumb'] = '<li>>></li><li class="current">Thành viên</li>';
            if ($this->input->post('ok')) {
                $this->form_validation->set_rules('user_name', 'Full name', 'trim|required|max_length[100]');
                $this->form_validation->set_rules('user_loginname', 'User name', 'trim|required|max_length[100]|callback_checkusername');
                $this->form_validation->set_rules('user_email', 'Email', 'trim|required|max_length[100]|callback_checkemail');
                $this->form_validation->set_message('checkemail', 'Email is already in use, please try another');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[32]|matches[re_password]');
                $this->form_validation->set_rules('user_address', 'Address', 'trim');
                $this->form_validation->set_rules('user_phone', 'Phone', 'trim');
                $this->form_validation->set_rules('re_password', 'Re-Password', 'trim|required|max_length[32]');
                $this->form_validation->set_error_delimiters('<label class="error">', '</label>');

                $this->form_validation->set_message('checkusername', 'username   is already in use, please try another.');

                $this->form_validation->set_error_delimiters('<label class="error">', '</label>');

                if ($this->form_validation->run() == TRUE) {
//------ insert du lieu -------
                    $password = hash('sha256', $this->input->post('password'));
                    $sql = array(
                        'user_loginname' => $this->input->post('user_loginname'),
                        'user_address' => $this->input->post('user_address'),
                        'user_status' => $this->input->post('user_status'),
                        'user_email' => $this->input->post('user_email'),
                        'user_phone' => $this->input->post('user_phone'),
                        'user_name' => $this->input->post('user_name'),
                        'user_password' => $password,
                        'store_id' => $this->input->post('store_id'),
                    );
                    if ($this->db->insert('tbl_user', $sql)) {
                        echo "<meta http-equiv='Content-Type' content='text/html;charset=utf-8' />;
<script>alert('Thêm mới thành công');location.href='" . base_url() . "admin/moderator'</script>";
                    } else {
                        echo "<meta http-equiv='Content-Type' content='text/html;charset=utf-8' /><script>alert('Thất bại');location.href='" . base_url() . "admin/moderator/add'</script>";
                    }
                } else {
                    $this->template->write_view('content', 'admin/add', $data, TRUE);
                    $this->template->render();
                }
            } else {

                $this->template->write_view('content', 'admin/add', $data, TRUE);
                $this->template->render();
            }
        }
    }

    function edit($id) {
        if (!($this->general->Checkpermission("edit")) || $id == 1) {
            redirect(site_url("admin/not-permission"));
        } else {
            $data['ma'] = $this->global_function->randomPassword(5) . (str_replace("-", "", date("y-m-d")));
            $data['breadcrumb'] = '<li>>></li><li class="current">Thành viên</li>';
            $data["user"] = $this->general->get_row("tbl_user", array("id" => $id, "id !=" => 1));
            if ($this->input->post('ok')) {
                $this->form_validation->set_rules('user_name', 'Full name', 'trim|required|max_length[100]');
                $this->form_validation->set_rules('user_loginname', 'User name', 'trim|required|max_length[100]|callback_checkusername');
                $this->form_validation->set_rules('user_email', 'Email', 'trim|required|max_length[100]|callback_checkemail');
                $this->form_validation->set_message('checkemail', 'Email is already in use, please try another');
                $this->form_validation->set_rules('user_address', 'Address', 'trim');
                $this->form_validation->set_rules('user_phone', 'Phone', 'trim');
                $this->form_validation->set_error_delimiters('<label class="error">', '</label>');

                $this->form_validation->set_message('checkusername', 'username   is already in use, please try another.');

                $this->form_validation->set_error_delimiters('<label class="error">', '</label>');

                if ($this->form_validation->run() == TRUE) {
//------ insert du lieu -------
                    $password = hash('sha256', $this->input->post('password'));
                    $sql = array(
                        'user_loginname' => $this->input->post('user_loginname'),
                        'user_address' => $this->input->post('user_address'),
                        'user_status' => $this->input->post('user_status'),
                        'user_email' => $this->input->post('user_email'),
                        'user_phone' => $this->input->post('user_phone'),
                        'user_name' => $this->input->post('user_name'),
                        'store_id' => $this->input->post('store_id'),
                    );
                    $this->db->where("id", $id);
                    $this->db->update("tbl_user", $sql);
                    redirect(site_url('admin/moderator/edit/' . $id) . '?messager=success');
                } else {
                    $this->template->write_view('content', 'admin/edit', $data, TRUE);
                    $this->template->render();
                }
            } else {

                $this->template->write_view('content', 'admin/edit', $data, TRUE);
                $this->template->render();
            }
        }
    }

    function Permission($id) {
        if (isset($_REQUEST['ok'])) {
            $p = "";
            $x = 0;
            $count = count($this->input->post("level"));
            foreach ($this->input->post("level") as $r) {
                if ($x != ($count - 1))
                    $p.=$r . ",";
                else {
                    $p.=$r;
                }
                $x++;
            }
            $this->db->where("id", $id);
            $this->db->update("tbl_user", array("permission" => base64_encode($p)));
            redirect(site_url('admin/moderator/permission/'.$id) . '?messager=success');
        }
        $user = $this->general->get_row("tbl_user", array("id" => $id));
        $array = array();
        $array = explode(",", $user->permission);
        $data["id"] = $id;
        $this->template->write_view('content', 'admin/permission', $data, TRUE);
        $this->template->render();
    }

// ============================================
    function delete($id) {
        if (!($this->general->Checkpermission("delete"))) {
            redirect(site_url("admin/not-permission"));
        } else {
            $this->db->delete('tbl_user', array('id' => $id));
            redirect(site_url('admin/moderator') . '?messager=success');
        }
    }

    function delete_more($id) {
        if (!($this->general->Checkpermission("delete"))) {
            redirect(site_url("admin/not-permission"));
        } else {
            $this->db->delete('tbl_user', array('id' => $id));
            return true;
        }
    }

//=========================================== 
    function hide($id) {
        if (!($this->general->Checkpermission("edit"))) {
            redirect(site_url("admin/not-permission"));
        } else {
            $this->db->where("id", $id);
            $this->db->update("tbl_user", array('user_status' => 0));
            redirect(site_url('admin/moderator') . '?messager=success');
        }
    }

//============================================\
    function hide_more($id) {
        if (!($this->general->Checkpermission("edit"))) {
            redirect(site_url("admin/not-permission"));
        } else {
            $this->db->where("id", $id);
            $this->db->update("tbl_user", array('user_status' => 0));
            return true;
        }
    }

//============================================\
    function show_more($id) {
        if (!($this->general->Checkpermission("edit"))) {
            redirect(site_url("admin/not-permission"));
        } else {
            $this->db->where("id", $id);
            $this->db->update("tbl_user", array('user_status' => 1));
            return true;
        }
    }

//============================================\
    function show($id) {
        if (!($this->general->Checkpermission("edit"))) {
            redirect(site_url("admin/not-permission"));
        } else {
            $this->db->where("id", $id);
            $this->db->update("tbl_user", array('user_status' => 1));
            redirect(site_url('admin/moderator') . '?messager=success');
        }
    }

    public function checkemail() {
        $check = $this->general->get_row("tbl_user", array("user_email" => $this->input->post("user_email"), "id !=" => $this->input->post("id")));
        if (isset($check->id))
            return false;
        else
            return true;
    }

    public function checkusername() {
        $check = $this->general->get_row("tbl_user", array("user_loginname" => $this->input->post("user_loginname"), "id !=" => $this->input->post("id")));
        if (isset($check->id))
            return false;
        else
            return true;
    }

    function Change_pass($id) {
        if (!($this->general->Checkpermission("edit")))
            redirect(site_url("admin/not-permission"));
        $data = array();
        if (isset($_POST['ok'])) {
            //$this->form_validation->set_rules('old_pass', 'Password', 'trim|required|max_length[24]');
            $customers = $this->general->get_row("tbl_user", array("id" => $id));
            /*if ($customers->user_password != MD5($this->input->post('old_pass'))) {
                $this->form_validation->set_rules('old_pass', 'Password', 'callback_check_oldpass');
            }*/
            $this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|max_length[24]');
            $this->form_validation->set_rules('re_new_pass', 'Confirm Password', 'trim|required|max_length[24]|callback_checknewpass');
            $this->form_validation->set_message('check_oldpass', 'Password Error');
            $this->form_validation->set_message('checknewpass', 'Confirm Password is false');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'user_password' => hash('sha256', $this->input->post('new_pass')),
                );
                $this->db->where('id', $id);
                if ($this->db->update('tbl_user', $sql)) {
                    if ($this->session->userdata('admin_login')) {
                        
                    }
                    redirect(site_url('admin/moderator/change_pass/' . $id) . '?messager=success');
                } else {

                    redirect(site_url('admin/moderator/change_pass/' . $id) . '?messager=error');
                }
            }
        }
        $this->template->write_view('content', 'admin/change_pass', $data, TRUE);
        $this->template->render();
    }

    // admin change
    function Change_my_pass() {
        $id = $this->session->userdata('admin_login')->id;
        $data = array();
        if (isset($_POST['ok'])) {

            $customers = $this->general->get_row("tbl_user", array("id" => $id));

            $this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|max_length[24]');
            $this->form_validation->set_rules('re_new_pass', 'Confirm Password', 'trim|required|max_length[24]|callback_checknewpass');
            $this->form_validation->set_message('check_oldpass', 'Password Error');
            $this->form_validation->set_message('checknewpass', 'Confirm Password is false');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'user_password' => hash('sha256', $this->input->post('new_pass')),
                );
                $this->db->where('id', $id);
                if ($this->db->update('tbl_user', $sql)) {
                    if ($this->session->userdata('admin_login')) {
                        
                    }
                    redirect(site_url('admin/company') . '?messager=success');
                } else {

                    redirect(site_url('admin/company') . '?messager=error');
                }
            }
        }
        $this->template->write_view('content', 'admin/change_pass', $data, TRUE);
        $this->template->render();
    }

    // change my info admin
    function ChangeMyInfo() {
        $id = $this->session->userdata('admin_login')->id;
        $data['ma'] = $this->global_function->randomPassword(5) . (str_replace("-", "", date("y-m-d")));
        $data['breadcrumb'] = '<li>>></li><li class="current">Thành viên</li>';
        $data["user"] = $this->general->get_row("tbl_user", array("id" => $id));
        if ($this->input->post('ok')) {
            $this->form_validation->set_rules('user_name', 'Full name', 'trim|required|max_length[100]');
            $this->form_validation->set_rules('user_loginname', 'User name', 'trim|required|max_length[100]|callback_checkusername');
            $this->form_validation->set_rules('user_email', 'Email', 'trim|required|max_length[100]|callback_checkemail');
            $this->form_validation->set_message('checkemail', 'Email is already in use, please try another');
            $this->form_validation->set_rules('user_address', 'Address', 'trim');
            $this->form_validation->set_rules('user_phone', 'Phone', 'trim');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');

            $this->form_validation->set_message('checkusername', 'username   is already in use, please try another.');

            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');

            if ($this->form_validation->run() == TRUE) {
//------ insert du lieu -------
                $password = hash('sha256', $this->input->post('password'));
                $sql = array(
                    'user_loginname' => $this->input->post('user_loginname'),
                    'user_address' => $this->input->post('user_address'),
                    'user_status' => $this->input->post('user_status'),
                    'user_email' => $this->input->post('user_email'),
                    'user_phone' => $this->input->post('user_phone'),
                    'user_name' => $this->input->post('user_name'),
                );
                $this->db->where("id", $id);
                $this->db->update("tbl_user", $sql);
                redirect(site_url('admin/change-info') . '?messager=success');
            } else {
                $this->template->write_view('content', 'admin/edit', $data, TRUE);
                $this->template->render();
            }
        } else {

            $this->template->write_view('content', 'admin/edit', $data, TRUE);
            $this->template->render();
        }
    }

    function checknewpass() {
        if ($_POST['new_pass'] == $_POST['re_new_pass'])
            return true;
        else
            return false;
    }

    function check_oldpass($id) {
        return false;
    }

}
