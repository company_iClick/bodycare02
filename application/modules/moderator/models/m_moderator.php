<?php

class M_moderator extends CI_Model {

    function show_list_user_where($limit, $offset) {
        $this->db->select("*");
        $this->db->order_by('tbl_user.id', "DESC");
        $this->db->where("type",1);
        $this->db->limit($limit, $offset);
        $this->db->from('tbl_user');
        return $this->db->get()->result();
    }

}
