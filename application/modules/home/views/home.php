<?php if(!empty($categories)){?>
<div id="box_category_home">
    <?php foreach($categories as $category){ 
        $c_name = $category->name;
        $c_link = site_url($category->slug);
        $c_color = (!empty($category->color)) ? $category->color : '#7cb342';
        $font_class = (!empty($category->font_class)) ? 'icon-' . $category->font_class : 'icon-chamsoccothe';
        $bannerUrl = base_url(_upload_category . $category->banner);
        $link_banner = (!empty($category->link_banner)) ? $category->link_banner : $c_link;
        $product = $category->product;
        $product_right = (count($product) > 3) ? array_slice($product, 3, count($product) - 1) : '';
        $child = $category->child;
        $brand = $category->brand;
        if(!empty($product)){
    ?>
    <div class="box_category_home">
        <div class="box_category_home_title clearfix" style="border-top: 1px solid <?php echo $c_color; ?>">
            <div class="box_category_home_name">
                <div class="category_home_name" style="background: <?php echo $c_color; ?>">
                    <a href="<?php echo $c_link; ?>" title="<?php echo $c_name; ?>">
                        <p><i class="<?php echo $font_class; ?>"></i></p>
                        <span><?php echo $c_name; ?></span>
                    </a>
                    <b style="border-left: 8px solid <?php echo $c_color; ?>"></b>
                </div>
            </div>
            
            <?php if(!empty($brand)){ ?>
            <div class="box_category_home_brand">
                <ul class="clearfix">
                    <?php
                        foreach($brand as $b){
                            $name = $b->name;
                            $link = site_url($b->slug);
                            $photo = base_url() . _upload_brand . $b->photo;
                            echo sprintf('<li><a href="%s" title="%s"><img src="%s" alt="%s"></a></li>', $link, $name, $photo, $name);
                        }
                    ?>
                    <li>
                        <a href="<?php echo site_url('thuong-hieu'); ?>"><span class="icon-daucong"></span></a>
                    </li>
                </ul>
            </div>
            <?php } ?>

        </div>

        <div class="box_category_home_product clearfix">
            <div class="box_category_cate_name">
                <?php if(!empty($child)){ ?>
                <ul class="ul_list_recieve_text">
                    <?php
                        $i = 0; 
                        foreach($child as $row){
                        $pt_name = $row->name;
                        $pt_link = site_url($row->slug); 
                        $count_product = $row->count_product;
                        $class = 'c_node';
                        if ($row->level == 1) $class = 'c_parent';
                        if ($row->level == 2) $class = 'c_child';

                        if($count_product > 0)
                        echo sprintf('<li class="%s"><a href="%s" title="%s">%s<b>(%s)</b></a></li>', $class, $pt_link, $pt_name, $pt_name, $count_product);

                        $i++;
                    } ?>
                </ul>
                <?php } ?>
                <div class="clear"></div>
                <div class="view_all_cate_parent">
                    <a href="<?php echo $c_link; ?>">Xem tất cả</a>
                </div>
            </div>
            <div class="box_category_cate_banner_pr">
                <div class="box_category_cate_banner_pr_left">
                    <?php if(!empty($category->banner)){ ?>
                    <div class="box_category_cate_banner">
                        <a href="<?php echo $link_banner; ?>" title="<?php echo $c_name; ?>">
                            <img src="<?php echo $bannerUrl; ?>" alt="<?php echo $c_name; ?>">
                        </a>
                    </div>
                    <?php } ?>
                    <?php if(!empty($product)){ ?>
                    <div class="box_category_cate_pr clearfix">
                        <?php 
                        $j = 0;
                        foreach($product as $pr){
                            $id = $pr->id;
                            $name = $pr->name;
                            $pr_name = $this->my_lib->cut_string($pr->name, 70);
                            $link = site_url($pr->slug);
                            $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                            $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                            $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                            $price = cms_price_v1($pr->price_sale, $pr->price);

                            if($j < 3) echo sprintf('<div class="box_product_v1">
                                            <div class="box_product_v1_img">
                                                <a href="%s" title="%s">
                                                    <img src="%s" alt="%s">
                                                </a>
                                            </div>
                                            
                                            <div class="box_product_v1_name">
                                                <a href="%s" title="%s">%s</a>
                                            </div>
                                            <div class="box_product_v1_price clearfix">
                                                <div class="box_product_v1_price_num">%s</div>
                                                <div class="box_product_v1_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                            </div>%s%s
                                        </div>', $link, $name, $photo, $name, $link, $name, $pr_name, $price, $id, $percent, $gift_type);
                        $j++;
                        }
                        ?>
                    </div>
                    <?php } ?>
                    
                </div>
                <div class="box_category_cate_banner_pr_right">
                    <?php if(!empty($product_right)){
                    foreach($product_right as $pr){
                        $id = $pr->id;
                        $name = $pr->name;
                        $pr_name = $this->my_lib->cut_string($pr->name, 60);
                        $link = site_url($pr->slug);
                        $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                        $price = cms_price_v1($pr->price_sale, $pr->price);

                        echo sprintf('<div class="media_pr_right">
                                        <div class="media_pr_right_img">
                                            <a href="%s" title="%s">
                                                <img src="%s" alt="%s">
                                            </a>
                                        </div>
                                        <div class="media_pr_right_name_price">
                                            <div class="media_pr_right_name">
                                                <a href="%s" title="%s">%s</a>
                                            </div>
                                            <div class="media_pr_right_num">
                                                %s
                                            </div>
                                        </div>
                                    </div>', $link, $name, $photo, $name, $link, $name, $pr_name, $price);
                    }}?>
                </div>
            </div>
        </div>
    </div>
    <?php  }} ?>
</div>
<?php } ?>

<?php if(!empty($category_small)){?>
<div id="box_category_home_small">
    <div class="box_category_home_small">
        <?php foreach($category_small as $category){ 
            $c_name = $category->name;
            $c_link = site_url($category->slug);
            $c_color = (!empty($category->color)) ? $category->color : '#7cb342';
            $font_class = (!empty($category->font_class)) ? 'icon-' . $category->font_class : 'icon-chamsoccothe';
            $bannerUrl = base_url(_upload_category . $category->banner);
            $link_banner = (!empty($category->link_banner)) ? $category->link_banner : $c_link;
            $product = $category->product;
            $product_right = (count($product) > 1) ? array_slice($product, 1, count($product) - 1) : '';
            $child = $category->child;
            $brand = $category->brand;
            if(!empty($product)){
        ?>
        <div class="box_category_home_small_half">
            <div class="box_category_home_small_half_title" style="border-top: 1px solid <?php echo $c_color; ?>">
                <div class="box_category_home_small_half_name" style="background: <?php echo $c_color; ?>">
                    <a href="<?php echo $c_link; ?>" title="<?php echo $c_name; ?>">
                        <p><i class="<?php echo $font_class; ?>"></i></p>
                        <span><?php echo $c_name; ?></span>
                    </a>
                    <b style="border-left: 8px solid <?php echo $c_color; ?>"></b>
                </div>
                <?php if(!empty($brand)){ ?>
                <div class="box_category_home_small_half_brand">
                    <ul class="clearfix">
                        <?php
                        foreach($brand as $b){
                            $name = $b->name;
                            $link = site_url($b->slug);
                            $photo = base_url() . _upload_brand . $b->photo;
                            echo sprintf('<li><a href="%s" title="%s"><img src="%s" alt="%s"></a></li>', $link, $name, $photo, $name);
                        }
                        ?>
                        <li>
                            <a href="<?php echo site_url('thuong-hieu'); ?>"><span class="icon-daucong"></span></a>
                        </li>
                    </ul>
                </div>
                <?php } ?>

                <div class="box_category_home_small_half_product clearfix">
                    <?php if(!empty($product)){ ?> 
                    <?php 
                    $j = 0;
                    foreach($product as $pr){
                        $id = $pr->id;
                        $name = $pr->name;
                        $pr_name = $this->my_lib->cut_string($pr->name, 65);
                        $link = site_url($pr->slug);
                        $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                        $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                        $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                        $price = cms_price_v1($pr->price_sale, $pr->price);

                        if($j < 1) echo sprintf('<div class="box_category_home_small_half_product_left">
                                                    <div class="box_category_home_small_half_product_left_name">
                                                        <a href="%s" title="%s">%s</a>
                                                    </div>
                                                    <div class="box_category_home_small_half_product_left_img_pr clearfix">
                                                        <div class="box_category_home_small_half_product_left_pr">
                                                            <div class="box_category_home_small_half_product_price">
                                                                %s
                                                            </div>
                                                            <div class="box_category_home_small_half_product_percent clearfix">
                                                                %s
                                                                %s
                                                            </div>
                                                            <div class="box_category_home_small_half_product_cart">
                                                                <a href="javascript:;" onclick="add_to_cart(%s)">Mua ngay</a>
                                                            </div>
                                                        </div>
                                                        <div class="box_category_home_small_half_product_left_img">
                                                            <a href="%s" title="%s">
                                                                <img src="%s" alt="%s">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>', $link, $name, $pr_name, $price, $percent, $gift_type, $id, $link, $name, $photo, $name);

                        $j++;}
                    ?>
                    <?php } ?>

                    <div class="box_category_home_small_half_product_right">
                        <?php if(!empty($product_right)){
                        foreach($product_right as $pr){
                            $id = $pr->id;
                            $name = $pr->name;
                            $pr_name = $this->my_lib->cut_string($pr->name, 50);
                            $link = site_url($pr->slug);
                            $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                            $price = cms_price_v1($pr->price_sale, $pr->price);

                            echo sprintf('<div class="media_pr_right">
                                            <div class="media_pr_right_img">
                                                <a href="%s" title="%s">
                                                    <img src="%s" alt="%s">
                                                </a>
                                            </div>
                                            <div class="media_pr_right_name_price">
                                                <div class="media_pr_right_name">
                                                    <a href="%s" title="%s">%s</a>
                                                </div>
                                                <div class="media_pr_right_num">
                                                    %s
                                                </div>
                                            </div>
                                        </div>', $link, $name, $photo, $name, $link, $name, $pr_name, $price);
                        }}?>
                    </div>
                </div>
            </div>
        </div>
        <?php  }} ?>
    </div>
</div>
<?php  } ?>

<div id="box_category_home_mobile">
    <?php if(!empty($categories)){?>
    <div class="wrapper_full">
        <div class="wrapper_full_1">
            <?php foreach($categories as $category){ 
                $c_name = $category->name;
                $c_link = site_url($category->slug);
                $c_color = (!empty($category->color)) ? $category->color : '#296525';
                $bannerUrl = base_url(_upload_category . $category->banner);
                $link_banner = (!empty($category->link_banner)) ? $category->link_banner : $c_link;
                $list_product = $category->product;
                $child = $category->child;
                $brand = $category->brand;
                if(!empty($list_product)){
            ?>
            <div class="wrapper_full_2">
                <div class="cate_home_mb"><p><a href="<?php echo $c_link; ?>" title="<?php echo $c_name; ?>"><?php echo $c_name; ?></a></p></div>

                <div class="manufacturer_small clearfix">
                    <?php if(!empty($child)){ ?>
                    <div class="wp_select_manufacturer_small">
                        <select class="select_manufacturer_small cs-skin-border" onchange="window.open(this.value,'_self');">
                            <option value="" disabled selected>Loại sản phẩm</option>
                            <?php foreach($child as $pt){
                                $pt_name = $pt->name;
                                $pt_link = site_url($pt->slug); 
                                echo '<option value="'. $pt_link .'">'. $pt_name .'</option>';
                            } ?>
                        </select>
                    </div>
                    <?php } ?>
                    
                    <?php if(!empty($brand)){
                        foreach($brand as $b){
                            $name = $b->name;
                            $link = site_url($b->slug);
                            $photo = base_url() . _upload_brand . $b->photo;
                            echo sprintf('<a href="%s" title="%s"><img src="%s" alt="%s"></a>', $link, $name, $photo, $name);
                        }
                    ?>
                    <a href="<?php echo site_url('thuong-hieu'); ?>" class="a_plus" style="color: <?php echo $c_color; ?>"><i class="icon_plus"></i></a>
                    <?php } ?>
                    <?php if(!empty($category->banner)){ ?>
                    <div class="box_category_cate_banner">
                        <a href="<?php echo $link_banner; ?>" title="<?php echo $c_name; ?>">
                            <img src="<?php echo $bannerUrl; ?>" alt="<?php echo $c_name; ?>">
                        </a>
                    </div>
                    <?php } ?>
                </div>
                
                <div class="list_product_home_cate">
                    <?php 
                        foreach($list_product as $pr){
                        $id = $pr->id;
                        $name = $pr->name;
                        $pr_name = $this->my_lib->cut_string($pr->name, 55);
                        $link = site_url($pr->slug);
                        $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                        $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                        $class_layout_list = '';
                        $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                        $price = cms_price_v1($pr->price_sale, $pr->price);

                        echo sprintf('<div class="box_product_v2%s">
                                        <div class="box_product_v2_img">
                                            <a href="%s" title="%s">
                                                <img src="%s" alt="%s">
                                            </a>
                                        </div>
                                        <div class="box_product_v2_name_element">
                                            <div class="box_product_v2_name">
                                                <a href="%s" title="%s">%s</a>
                                            </div>
                                            <div class="box_product_v2_price clearfix">
                                                <div class="box_product_v2_price_num">%s</div>
                                                <div class="box_product_v2_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                            </div>
                                        </div>%s%s
                                    </div>', $class_layout_list, $link, $name, $photo, $name, $link, $name, $name, $price, $id, $percent, $gift_type);
                    }?>
                </div>

                <div class="wrapper_full_3">
                    <div class="btn_view_more"><a href="<?php echo $c_link; ?>">Xem tất cả</a></div>
                </div>
            </div>
            <?php }} ?>
        </div>
    </div>
    <?php } ?>

    <?php if(!empty($category_small)){?>
    <div class="wrapper_full">
        <div class="wrapper_full_1">
            <?php foreach($category_small as $category){ 
                $c_name = $category->name;
                $c_link = site_url($category->slug);
                $c_color = (!empty($category->color)) ? $category->color : '#296525';
                $bannerUrl = base_url(_upload_category . $category->banner);
                $link_banner = (!empty($category->link_banner)) ? $category->link_banner : $c_link;
                $list_product = $category->product;
                $child = $category->child;
                $brand = $category->brand;
                if(!empty($list_product)){
            ?>
            <div class="wrapper_full_2">
                <div class="title_name_cate_1">
                    <p><a href="<?php echo $c_link; ?>" title="<?php echo $c_name; ?>" style="color: <?php echo $c_color; ?>"><?php echo $c_name; ?></a><b style="background: <?php echo $c_color; ?>"></b></p>

                    <div class="manufacturer_small">
                        <?php if(!empty($child)){ ?>
                        <div class="wp_select_manufacturer_small">
                            <select class="select_manufacturer_small cs-skin-border" onchange="window.open(this.value,'_self');">
                                <option value="" disabled selected>Loại sản phẩm</option>
                                <?php foreach($child as $pt){
                                    $pt_name = $pt->name;
                                    $pt_link = site_url($pt->slug); 
                                    echo '<option value="'. $pt_link .'">'. $pt_name .'</option>';
                                } ?>
                            </select>
                        </div>
                        <?php } ?>
                        
                        <?php if(!empty($brand)){
                            foreach($brand as $b){
                                $name = $b->name;
                                $link = site_url($b->slug);
                                $photo = base_url() . _upload_brand . $b->photo;
                                echo sprintf('<a href="%s" title="%s"><img src="%s" alt="%s"></a>', $link, $name, $photo, $name);
                            }
                        ?>
                        <a href="<?php echo site_url('thuong-hieu'); ?>" class="a_plus" style="color: <?php echo $c_color; ?>"><i class="icon_plus"></i></a>
                        <?php } ?>
                    </div>
                    <i class="line_1"></i>
                </div>
              
                <div class="list_product_home_cate">
                    <?php 
                        foreach($list_product as $pr){
                        $id = $pr->id;
                        $name = $pr->name;
                        $pr_name = $this->my_lib->cut_string($pr->name, 55);
                        $link = site_url($pr->slug);
                        $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                        $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                        $class_layout_list = '';
                        $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                        $price = cms_price_v1($pr->price_sale, $pr->price);

                        echo sprintf('<div class="box_product_v2%s">
                                        <div class="box_product_v2_img">
                                            <a href="%s" title="%s">
                                                <img src="%s" alt="%s">
                                            </a>
                                        </div>
                                        <div class="box_product_v2_name_element">
                                            <div class="box_product_v2_name">
                                                <a href="%s" title="%s">%s</a>
                                            </div>
                                            <div class="box_product_v2_price clearfix">
                                                <div class="box_product_v2_price_num">%s</div>
                                                <div class="box_product_v2_price_percent">%s%s</div>
                                            </div>
                                        </div>
                                    </div>', $class_layout_list, $link, $name, $photo, $name, $link, $name, $name, $price, $percent, $gift_type);
                    }?>
                </div>

                <div class="wrapper_full_3">
                    <div class="btn_view_more"><a href="<?php echo $c_link; ?>">Xem tất cả</a></div>
                </div>
            </div>
            <?php }} ?>
        </div>
    </div>
    <?php } ?>
</div>

<?php $this->load->view('front/block/search_cate_mb') ?>

<div class="wrapper_full wrapper_full_bg_3">
    <div class="wrapper_full_1">
        <div class="left_support">
            <div class="left_support1">
                <p class="title_support">ý kiến khách hàng</p>
                <?php if(!empty($customer_reviews)): ?>
                <div id="slider_comment">
                    <?php foreach ($customer_reviews as $row) {
                        $photo = base_url(_upload_customer . $row->photo);
                        $imgUrl = base_url('timthumb.php?src='. $photo .'&w=90&h=90&zc=1');
                        $name = $row->name;
                        $content = $row->content;

                        echo sprintf('<div class="slider_comment">
                                        <a href="javascript:void(0)" class="a_thumb_1"><img src="%s" alt="%s"></a>
                                            <span class="wf_comment">
                                                <i class="ngay_1"></i>%s<i class="ngay_2"></i>
                                            </span >
                                       <span class="wp_line_sp"> <i class="line_sp"></i></span>
                                        <a href="javascript:void(0)" class="name_comment_sp">%s</a>
                                    </div>', $imgUrl, $name, $content, $name);
                    } ?>
                </div>
            <?php endif; ?>
            </div>
        </div>
        <div class="right_support">

            <img class="bg_left" src="<?php echo base_url() . _images; ?>bg_left.png">
            <div class="right_support1">
                <p class="title_support">HỖ TRỢ TƯ VẤN</p>
                <p class="tuvan_sp">( Vui lòng gọi <i></i><b><a style="color: #b20505" href="tel:<?php echo $info->phone; ?>"><?php echo $info->phone; ?></a></b> để được tư vấn trực tiếp hoặc đăng ký tư vấn )</p>
                <form id="form_support" method="post">
                    <div class="div_full_sp">
                        <div class="left_div_full_sp">
                            <input class="input_sp" name="adv_name" type="text" placeholder="Họ và Tên">
                        </div>
                        <div class="right_div_full_sp">
                            <input class="input_sp" name="adv_phone" type="text" placeholder="SĐT">
                            <div class="clear"></div>
                            <div id="errorAdvPhone"></div>
                        </div>
                    </div>
                    <div class="div_full_sp">
                        <input type="email" class="input_sp" name="adv_email" placeholder="Email">
                        <div class="clear"></div>
                        <div id="errorAdvEmail"></div>
                    </div>
                    <div class="div_full_sp">
                        <div class="left_div_full_sp">
                        <?php if(!empty($category_parent)): ?>
                            <select name="adv_category">
                            <option value="">Loại sản phẩm</option>
                            <?php foreach ($category_parent as $row) {
                                echo '<option value="'. $row->name .'">'. $row->name .'</option>';
                            } ?>
                          </select>
                        <?php endif; ?>
                        </div>
                        <div class="right_div_full_sp">
                        <?php if(!empty($skin_type)): ?>
                            <select name="adv_skin_type">
                            <option value="">Loại da</option>
                            <?php foreach ($skin_type as $row) {
                                echo '<option value="'. $row->name .'">'. $row->name .'</option>';
                            } ?>
                          </select>
                        <?php endif; ?>
                        </div>
                    </div>
                    <div class="div_full_sp">
                        <textarea placeholder="Nội dung" name="adv_content"></textarea>
                    </div>
                    <div class="clear"></div>
                    <div id="add_success_mgs"></div>
                    <div class="div_full_sp">
                        <input type="submit" value="Gửi" class="sm_sp" id="btn_adv_request">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php if(!empty($article)){ ?>
<div id="news_home">
    <div class="news_home">
        <div class="title_name_cate">
            <p><a href="<?php echo site_url('kien-thuc-lam-dep') ?>">KIẾN THỨC LÀM ĐẸP</a></p>
            <i class="line_1"></i>
        </div>
        <?php
            $i = 0; 
            foreach($article as $a){
            $name = $a->name;
            $a_name = $this->my_lib->cut_string($name, 80);;
            $link = site_url($a->slug);
            $summary = $this->my_lib->cut_string($a->summary, 150);
            $imgUrl = base_url(_upload_article . $a->folder . '/' . $a->photo);
            $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=480&h=336&zc=1');
            $date = date('d', strtotime($a->created_at));
            $month_year = date('m/Y', strtotime($a->created_at));
            $class = (($i + 1) % 2 == 0) ? ' child_news_home_2' : '';
            echo sprintf('<div class="child_news_home">
                            <div class="time_news">
                                <p>%u</p>
                                <p>%s</p>
                            </div>
                            <div class="img_news_home">
                                <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                            </div>
                            <div class="sum_name_news_home">
                                <h3><a href="%s" title="%s">%s</a></h3>
                                <p>%s</p>
                            </div>
                        </div>', $date, $month_year, $link, $name, $imgUrl, $name, $link, $name, $a_name, $summary);
            $i++;
        }?>            
        
    </div>
</div>
<?php } ?>