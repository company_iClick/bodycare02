<?php
class Home extends MX_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->model(array("category/a_category", "product_type/a_product_type", "brand/a_brand", "product/a_product", "article/a_article", "banner/a_banner"));
        $this->load->helper(array('cms_price_helper', 'percen_calculate_helper', 'cms_price_v1_helper', 'percen_calculate_v1_helper'));
	}

	function index()
	{ 

        $data['info'] = $info = $this->global_function->get_tableWhere(array("id"=>1),"company","title, descriptions, logo, phone");

        //banner
        $param_1 = array(
		    "select" => "images.name,images.link,images.title",
		    "where" => array("images.status" => 1, "imagealbum.album_id" => 1),
		    "order" => "images.weight",
		    "asc" => "ASC",
		    "limit" => 10,
		);

		$param_2 = array(
		    "select" => "images.name,images.link,images.title",
		    "where" => array("images.status" => 1, "imagealbum.album_id" => 3),
		    "order" => "images.id",
		    "asc" => "DESC",
		    "limit" => 1,
		);

		$param_3 = array(
		    "select" => "images.name,images.link,images.title",
		    "where" => array("images.status" => 1, "imagealbum.album_id" => 4),
		    "order" => "images.id",
		    "asc" => "DESC",
		    "limit" => 1,
		);

		$param_brand = array(
		    "select" => "images.name,images.link,images.title",
		    "where" => array("images.status" => 1, "imagealbum.album_id" => 5),
		    "order" => "images.id",
		    "asc" => "DESC",
		    "limit" => 1,
		);

		$param_discover = array(
		    "select" => "images.name,images.link,images.title",
		    "where" => array("images.status" => 1, "imagealbum.album_id" => 7),
		    "order" => "images.weight",
		    "asc" => "ASC",
		    "limit" => 11,
		);

		$data['banner_1'] = $this->a_banner->show_list_image_album_where($param_1);
		$data['banner_2'] = $this->a_banner->show_list_image_album_where($param_2);
		$data['banner_3'] = $this->a_banner->show_list_image_album_where($param_3);
		$data['banner_brand'] = $this->a_banner->show_list_image_album_where($param_brand);
		$data['discover_banner'] = $this->a_banner->show_list_image_album_where($param_discover);

		//new_sale product
		$data['new_sale'] = $this->a_product->get_product_new_sale_index(10);

		$data['brand'] = $this->global_function->get_array_object(array('hot' => 1), 'id, name, slug, photo', 'brand', 12);

		//list product category index
		$categories = $this->global_function->get_array_object(array('parent_id' => 0, 'hot' => 1, 'layout' => 0), 'id, name, title_show, color, font_class, banner, link_banner, slug, parent_id', 'category');
		if(!empty($categories)){
			foreach ($categories as $ct) {
				$ct_id = $ct->id;
				$list_cate_id = $this->a_category->get_categroy_tree_id($ct_id);
				$ct->product = $this->a_product->get_product_hot_in_category($list_cate_id, 8);
				$ct->child = $this->a_category->get_category($ct_id);
				$ct->brand = $this->a_brand->get_brand_in_category_index($list_cate_id, 9);
			}
		}

		//list product category index layout one
		$category_small = $this->global_function->get_array_object(array('parent_id' => 0, 'hot' => 1, 'layout' => 1), 'id, name, title_show, color, font_class, banner, link_banner, slug, parent_id', 'category');
		if(!empty($category_small)){
			foreach ($category_small as $ct) {
				$ct_id = $ct->id;
				$list_cate_id = $this->a_category->get_categroy_tree_id($ct_id);
				$ct->product = $this->a_product->get_product_hot_in_category($list_cate_id, 3);
				$ct->child = $this->a_category->get_category($ct_id);
				$ct->brand = $this->a_brand->get_brand_in_category_index($list_cate_id, 3);
			}
		}

		$data['categories'] = $categories;

		$data['category_small'] = $category_small;

		//kien thuc lam dep
		$data['article'] = $this->global_function->get_array_object(array('type' => 3), 'id, name, slug, summary, photo, folder, created_at', 'article', 4);

		$data['good_tips'] = $this->global_function->get_array_object(array('type' => 3, 'home' => 1), 'id, name, slug, summary, photo, folder, created_at', 'article', 8);

		//customer review
		$data['customer_reviews'] = $this->global_function->get_array_object(array(), 'id, name, content, photo', 'customer_reviews', 10);

		$data['category_parent'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name', 'category');
		$data['skin_type'] = $this->global_function->get_array_object(array(), 'id, name', 'skin_type');


        $data['share_face'] = 
		'<meta property="og:type" content="website" />
		<meta property="og:url" content="'.current_url().'"/>
		<meta property="og:title" content="'.$info->title.'"/>
		<meta property="og:description" content="'.$info->descriptions.'" />
		<meta property="og:image" content="'.base_url()._upload_default.$info->logo.'"/>
		<meta property="og:image:type" content="image/png">';

		$this->template->write('mod','home');
        $this->template->write_view('content','home', $data, TRUE);
        $this->template->render();

	}

	function advisory_request() {
		
        $post = $this->input->post();

        $data['name'] = strip_tags($post['adv_name']);
        $data['phone'] = $post['adv_phone'];
        $data['email'] = $post['adv_email'];
        $data['category'] = $post['adv_category'];
        $data['skin_type'] = $post['adv_skin_type'];
        $data['email'] = $post['adv_email'];
        $data['content'] = strip_tags($post['adv_content']);
      
        $data['weight'] = 1;
        $data['status'] = 1;

        $body = '<div style="width:700px;margin:auto"><table>';
        $body .= '
            <tr>
                <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
                <th colspan="2">Yêu cầu tư vấn từ website '. site_url() .'</th>
            </tr>
            <tr>
                <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
                <th>Email :</th><td>'.$data['email'].'</td>
            </tr>
            <tr>
                <th>Họ tên :</th><td>'.$data['name'].'</td>
            </tr>
            <tr>
                <th>Điện thoại:</th><td>'.$data['phone'].'</td>
            </tr>
            <tr>
                <th>Loại sản phẩm :</th><td>'.$data['category'].'</td>
            </tr>
            <tr>
                <th>Loại da :</th><td>'.$data['skin_type'].'</td>
            </tr>
            <tr>
                <th>Nội dung :</th><td>'.$data['content'].'</td>
            </tr>';
        $body .= '</table>';

        $result = array();
        $result['mailto'] = 'dangthinh@iclick.vn';
        $result['mailcc'] = 'hientt@iclick.vn';
        $result['title'] = 'Yêu cầu tư vấn từ website ' . site_url();
        $result['content'] = $body;

        if ($this->db->insert('advisory_request', $data)) {
            $result['message'] = true;
        } else
            $result['message'] = false;

        echo json_encode($result);
    }

                  
    function Notfound(){
        redirect(site_url());
    }
 
}
?>