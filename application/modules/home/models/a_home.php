<?php



class A_home extends CI_Model {



    function __construct() {

        parent::__construct();

        $this->load->database();

    }



   function show_list_type_item_where_new($type,$limit, $lang='vn'){

		$this->db->select("country.unit,itemdetail.name_link,itemdetail.name,item.id,item.date_create,item.date_modify,item.status,itemcategory.id_category,images.thumb");		

		$this->db->where("country.name",$lang);

		$this->db->where("item.status",1);

		$this->db->where("item.new",$type);

		$this->db->limit($limit);

		$this->db->order_by('itemdetail.id_item','DESC');

		$this->db->from('item');

		$this->db->join('itemdetail','itemdetail.id_item=item.id');

		$this->db->join('itemcategory','itemcategory.id_item=item.id');

		$this->db->join('itemimages','itemimages.id_item=item.id');

		$this->db->join('images','itemimages.image_id=images.id');

		$this->db->join('country','itemdetail.country_id=country.id');

		return $this->db->get()->result(); $this->db->get()->free_result();

	}

	//====================================

	function show_list_article_term_page($id,$lang='vn'){

		$this->db->select("articledetail.*,article.id,article.date_create,article.date_modify,article.article_status,articleterm.term_id,images.thumb");

		$this->db->where("country.name",$lang);

		$this->db->where("article.article_status",1);

		$this->db->where("articleterm.term_id",$id);

		$this->db->order_by('article.article_weight',"ASC");

		$this->db->order_by('article.date_create',"DESC");

		$this->db->from('article');

		$this->db->join('articledetail','articledetail.article_id=article.id');

		$this->db->join('articleterm','articleterm.article_id=article.id');

		$this->db->join('articleimages','articleimages.article_id=article.id');

		$this->db->join('images','articleimages.image_id=images.id');

		$this->db->join('country','articledetail.country_id=country.id');

		return $this->db->get()->result();$this->db->get()->free_result();

	}



}

