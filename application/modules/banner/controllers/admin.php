<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    protected $meta_title;
    protected $meta_keywords;
    protected $meta_description;
    protected $project = "Terrace resort";

    function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->helper(array('url', 'text', 'form', 'file'));
        $this->load->library(array('session', 'form_validation', 'ftp'));
        $this->load->database();
        $this->load->model(array('m_banner', "general"));
        $this->load->model('global_function');
        date_default_timezone_set('Asia/Saigon');
        $this->template->set_template('admin');  // Set template 
    }

    function admin() {
        parent::Controller();
    }

    function index($type = 1, $page_no = 1) {
        if (!($this->general->Checkpermission("view_banner")))
            redirect(site_url("admin/not-permission"));

        $data = array();
        $data['name_project'] = $this->project;
        $data['mod'] = 'image';
        $data['breadcrumb'] = '<li>>></li><li class="current">Hình ảnh</li>';

        if (isset($_POST['show'])) {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(site_url('admin/banner/index/' . $type . "/" . $page_no) . '?messager=success');
        }
        if (isset($_POST['hide'])) {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                //--------change parent------
                $this->hide_more($a);
            }
            redirect(site_url('admin/banner/index/' . $type . "/" . $page_no) . '?messager=success');
        }
        if (isset($_POST['delete'])) {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                //--------change parent------

                $this->delete_more($a, $type);
            }
            redirect(site_url('admin/banner/index/' . $type . "/" . $page_no) . '?messager=success');
        }

        $page_co = 12;
        $start = ($page_no - 1) * $page_co;
        $this->template->write('mod', "banner_" . $type); // set mod
        $count = $this->m_banner->count_list_image_album_where($type);
        $data['type'] = $type;
        $data['page_no'] = $page_no;
        $data['item'] = $this->m_banner->show_list_image_page_m_where($type, $page_co, $start);
        $data['link'] = $this->paging($page_co, $count, 'admin/banner/index/' . $type . "/", $page_no);
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();
    }

    //============================================
    function add($type = 1) {
        if (!($this->general->Checkpermission("add_banner")))
            redirect(site_url("admin/not-permission"));
        if (!$this->checkuser())
            redirect(site_url('admin/thongke/') . '?messager=warning');
        $this->template->write('mod', "banner_" . $type); // set mod
        $data['breadcrumb'] = '<li>>></li><li><a href="back/image">Hình ảnh</a></li><li>>></li><li class="current">Thêm mới</li>';
        $data['name_project'] = $this->project;

        if (isset($_POST['ok'])) {

            $check_img = $this->input->post('chose_img');
            //Kiểm tra nếu upload hình từ máy thì gọi hàm upload
            $img = '';
            $img_thumb = '';
            if ($check_img == 'img') {
                //Kiểm tra có thực hiện thao tác cắt hình ko
                if ((int) $_POST['x'] == 0) {
                    $this->form_validation->set_rules('img', 'Hình', 'callback_upimg');
                } else {
                    $valid_exts = array('jpeg', 'jpg', 'png', 'gif');
                    $max_file_size = 2000 * 2024; #200kb
                    $nw = $_POST['w'];
                    $nh = $_POST['h']; # image with # height

                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        if (isset($_FILES['img'])) {
                            if (!$_FILES['img']['error'] && $_FILES['img']['size'] < $max_file_size) {
                                $ext = strtolower(pathinfo($_FILES['img']['name'], PATHINFO_EXTENSION));
                                if (in_array($ext, $valid_exts)) {
                                    //create folder
                                    $date = date("d-m-Y");
                                    if (!file_exists("uploads/quang-cao/" . $date)) {
                                        mkdir("uploads/quang-cao/" . $date, 777);
                                    }
                                    //End
                                    $img = uniqid() . '.' . $ext;
                                    $img_thumb = $img;
                                    $path = 'uploads/quang-cao/' . $date . '/' . $img;
                                    $size = getimagesize($_FILES['img']['tmp_name']);

                                    $x = (int) $_POST['x'];
                                    $y = (int) $_POST['y'];
                                    $w = (int) $_POST['w'] ? $_POST['w'] : $size[0];
                                    $h = (int) $_POST['h'] ? $_POST['h'] : $size[1];

                                    $data = file_get_contents($_FILES['img']['tmp_name']);
                                    $vImg = imagecreatefromstring($data);
                                    $dstImg = imagecreatetruecolor($nw, $nh);
                                    imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
                                    imagejpeg($dstImg, $path);
                                    imagedestroy($dstImg);
                                    $url = base_url();
                                    //echo "<img src='$url$path' />";
                                } else {
                                    echo 'unknown problem!';
                                }
                            } else {
                                echo 'file is too small or large';
                            }
                        } else {
                            echo 'file not set';
                        }
                    } else {
                        echo 'bad request!';
                    }
                }////End Kiểm tra có thực hiện thao tác cắt hình ko
            }
            //End Kiểm tra nếu upload hình từ máy thì gọi hàm upload
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == true) {
                if ($check_img == 'img') {
                    if ((int) $_POST['x'] == 0) {
                        $data_img = $this->upload->data();
                        $config_img['image_library'] = 'gd2';
                        $config_img['source_image'] = $data_img['full_path'];
                        $config_img['create_thumb'] = FALSE;
                        $config_img['maintain_ratio'] = TRUE;
                        $this->load->library('image_lib', $config_img);
                        $this->image_lib->resize();
                        $this->image_lib->clear();
                        $img = $data_img['file_name'];
                        $img_thumb = $data_img['raw_name'] . '_thumb' . $data_img['file_ext'];
                    }
                    $sql = array(
                        'link' => $this->input->post('link'),
                        'title' => $this->input->post('title'),
                        'date_create' => date('Y-m-d H:i:s'),
                        'name' => substr($data_img['full_path'], strpos($data_img['full_path'], "uploads")),
                        'author' => $this->session->userdata('admin_login')->id,
                        'status' => $this->input->post('status'),
                        'weight' => $this->input->post('arrange'),
                    );
                }
                //////////////////////////
                $this->db->insert('images', $sql);
                $id_item = $this->db->insert_id();
                //-------------------------
                $album = $this->input->post('check_album');
                if ($album) {
                    foreach ($album as $key => $value) {
                        if ($this->global_function->count_tableWhere(array('image_id' => $id_item, 'album_id' => $key), "imagealbum") == 0) {
                            $sql = array('image_id' => $id_item, 'album_id' => $key);
                            ///////////////////////////////////
                            $this->db->insert('imagealbum', $sql);
                        }
                    }
                }
                redirect(site_url('admin/banner/index/' . $type) . '?messager=success');
            } else {
                if (isset($this->name_uploaded_file)) {
                    @unlink($this->filedata['file_path'] . $this->name_uploaded_file);
                }
            }
        }
        $data['type'] = $type;
        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }

    //===========================================
    function edit($type, $id) {
        if (!($this->general->Checkpermission("edit_banner")))
            redirect(site_url("admin/not-permission"));
        if (!$this->checkuser())
            redirect(site_url('admin/thongke/') . '?messager=warning');
        $data = array();
        $data['name_project'] = $this->project;
        $data['breadcrumb'] = '<li>>></li><li><a href="back/image">Hình ảnh</a></li><li>>></li><li class="current">Cập nhật</li>';
        $this->template->write('mod', "banner_" . $type); // set mod
        if (isset($_POST['ok'])) {
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if (($_FILES['img']['name'] != '')) {

                //Kiểm tra có thực hiện thao tác cắt hình ko
                $this->form_validation->set_rules('img', 'Hình', 'callback_upimg');
                if ($this->form_validation->run() == true) {
                    if ($this->m_banner->show_detail_image_id($id)->name != NULL) {
                        if (file_exists($this->m_banner->show_detail_image_id($id)->name)) {
                            @unlink($this->m_banner->show_detail_image_id($id)->name);
                        }
                    }
                    $data_img = $this->upload->data();
                    $config_img['image_library'] = 'gd2';
                    $config_img['source_image'] = $data_img['full_path'];
                    $config_img['create_thumb'] = FALSE;
                    $config_img['maintain_ratio'] = TRUE;

                    $this->load->library('image_lib', $config_img);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                    $img = $data_img['file_name'];
                    $img_thumb = $data_img['raw_name'] . '_thumb' . $data_img['file_ext'];
                    //////////////////////////
                    //--------------------------------
                    $isize = getimagesize($data_img['full_path']);
                    $sql = array(
                        'link' => $this->input->post('link'),
                        'title' => $this->input->post('title'),
                        'date_create' => date('Y-m-d H:i:s'),
                        'name' => substr($data_img['full_path'], strpos($data_img['full_path'], "uploads")),
                        'status' => $this->input->post('status'),
                        'weight' => $this->input->post('arrange'),
                    );
                    $this->db->where('id', $id);
                    $this->db->update('images', $sql);
                    $id_item = $id;
                    $del = array('image_id' => $id);
                    $this->db->delete('imagealbum', $del);
                    //-------------------------
                    $album = $this->input->post('check_album');
                    if ($album) {
                        foreach ($album as $key => $value) {
                            if ($this->global_function->count_tableWhere(array('image_id' => $id_item, 'album_id' => $key), "imagealbum") == 0) {
                                $sql = array('image_id' => $id_item, 'album_id' => $key);
                                ///////////////////////////////////
                                $this->db->insert('imagealbum', $sql);
                            }
                        }
                    }
                    ///////////////////////////////////
                    redirect(site_url('admin/banner/edit/' . $type . "/" . $id) . '?messager=success');
                }
            } else {

                $sql = array(
                    'link' => $this->input->post('link'),
                    'title' => $this->input->post('title'),
                    'status' => $this->input->post('status'),
                    'weight' => $this->input->post('arrange'),
                );
                $this->db->where('id', $id);
                $this->db->update('images', $sql);
                ///////////////////////////////////
                $del = array('image_id' => $id);
                $this->db->delete('imagealbum', $del);
                //-------------------------
                $album = $this->input->post('check_album');
                if ($album) {
                    foreach ($album as $key => $value) {
                        if ($this->global_function->count_tableWhere(array('image_id' => $id, 'album_id' => $key), "imagealbum") == 0) {
                            $sql = array('image_id' => $id, 'album_id' => $key);
                            ///////////////////////////////////
                            $this->db->insert('imagealbum', $sql);
                        }
                    }
                }
                redirect(site_url('admin/banner/edit/' . $type . "/" . $id) . '?messager=success');
            }
        }
        $data['type'] = $type;
        $data['id'] = $id;
        $data['image'] = $this->m_banner->show_detail_image_id($id);
        $this->template->write_view('content', 'admin/edit', $data, TRUE);
        $this->template->render();
    }

    //===========================================
    function top_album_add($key, $image_id) {
        if ($this->counter->count_image_album($key, $image_id) == 0) {
            $sql = array(
                'image_id' => $image_id,
                'album_id' => $key
            );
            $this->db->insert('imagealbum', $sql);
            if ($this->m_banner->show_detail_album_id($key)->top_album)
                $this->top_album_add($this->m_banner->show_detail_album_id($key)->top_album, $image_id);
        }
    }

    //===============================================
    function upimg() {

        if (($_FILES['img']['name'] != '')) {
            $date = date("d-m-Y");
            if (!file_exists("uploads/quang-cao/" . $date)) {
                mkdir("uploads/quang-cao/" . $date, 0777);
            }
            $config['upload_path'] = './uploads/quang-cao/' . $date;
            $config['allowed_types'] = 'gif|jpg|jpeg|swf|png';
            $config['image_library'] = 'gd2';
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['max_size'] = '100000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';

            $this->upload->initialize($config);

            if (($_FILES['img']['name'] != '')) {
                if (!$this->upload->do_upload('img')) {

                    $this->form_validation->set_message('upimg', $this->upload->display_errors());
                    return FALSE;
                } else {

                    return TRUE;
                }
            }
        } else {
            $this->form_validation->set_message('upimg', 'Bạn chưa chọn hình đại diện');
            return FALSE;
        }
    }

    //============================================\
    function delete($type, $id, $page_no) {
        if (!($this->general->Checkpermission("delete_banner")))
            redirect(site_url("admin/not-permission"));

        if ($this->m_banner->show_detail_image_id($id)->name != NULL) {
            if (file_exists($this->m_banner->show_detail_image_id($id)->name)) {
                @unlink($this->m_banner->show_detail_image_id($id)->name);
            }
        }
        if ($this->global_function->count_tableWhere(array('image_id' => $id, 'album_id' => $type), "imagealbum") > 0) {
            $where = array('image_id' => $id);
            $this->db->delete('imagealbum', $where);
        }
        $where = array('id' => $id);
        $this->db->delete('images', $where);
        redirect(site_url('admin/banner/index/' . $type . "/" . $page_no) . '?messager=success');
    }

    //============================================\
    function delete_more($id, $type) {
        if (!($this->general->Checkpermission("delete_banner")))
            redirect(site_url("admin/not-permission"));
        if (!($this->general->Checkpermission("delete_banner")))
            redirect(site_url("admin/not-permission"));

        if ($this->m_banner->show_detail_image_id($id)->name != NULL) {
            if (file_exists($this->m_banner->show_detail_image_id($id)->name)) {
                @unlink($this->m_banner->show_detail_image_id($id)->name);
            }
        }
        if ($this->global_function->count_tableWhere(array('image_id' => $id, 'album_id' => $type), "imagealbum") > 0) {
            $where = array('image_id' => $id);
            $this->db->delete('imagealbum', $where);
        }
        $where = array('id' => $id);
        $this->db->delete('images', $where);
        return true;
    }

    //============================================\
    function hide($type, $id, $page_no) {
        if (!($this->general->Checkpermission("edit_banner")))
            redirect(site_url("admin/not-permission"));
        $sql = array('status' => 0);
        $this->db->where('id', $id);
        $this->db->update('images', $sql);

        redirect(site_url('admin/banner/index/' . $type . "/" . $page_no) . '?messager=success');
    }

    //============================================\
    function info($id, $page_no) {
        if (!($this->general->Checkpermission("edit_banner")))
            redirect(site_url("admin/not-permission"));
        $art = $this->m_banner->show_detail_image_id($id);
        redirect(site_url('admin/banner/index/' . $page_no) . '?messager=information&type=image&id=' . $id);
    }

    //============================================\
    function hide_more($id) {
        if (!($this->general->Checkpermission("edit_banner")))
            redirect(site_url("admin/not-permission"));
        $sql = array('status' => 0);
        $this->db->where('id', $id);
        $this->db->update('images', $sql);

        return true;
    }

//============================================\
    function show_more($id) {
        if (!($this->general->Checkpermission("edit_banner")))
            redirect(site_url("admin/not-permission"));
        $sql = array('status' => 1);
        $this->db->where('id', $id);
        $this->db->update('images', $sql);

        return true;
    }

    //============================================\
    function show($type, $id, $page_no) {
        if (!($this->general->Checkpermission("edit_banner")))
            redirect(site_url("admin/not-permission"));
        $sql = array('status' => 1);
        $this->db->where('id', $id);
        $this->db->update('images', $sql);

        redirect(site_url('admin/banner/index/' . $type . "/" . $page_no) . '?messager=success');
    }

    //============================================
    function print_arr($array) {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }

    //============================================
    public function checkuser() {
        if (!$this->session->userdata('admin_login'))
            redirect(site_url('admin/login'));
        $a = $this->session->userdata('admin_login')->per;
        $p = unserialize($a);
        return true;
    }

    //===========================================
    public function paging($page, $total, $url, $id = 1) {

        $previous_btn = true;
        $next_btn = true;
        $first_btn = true;
        $last_btn = true;
        //kiem tra


        $count = $total;
        $tongtrang = ceil($total / $page);
        $num = "";

        if ($count != 0) {
            if ($id >= 7) {
                $start_loop = $id - 4;
                if ($tongtrang > $id + 4)
                    $end_loop = $id + 4;
                else if ($id <= $tongtrang && $id > $tongtrang - 6) {
                    $start_loop = $tongtrang - 6;
                    $end_loop = $tongtrang;
                } else {
                    $end_loop = $tongtrang;
                }
            } else {
                $start_loop = 1;
                if ($tongtrang > 7)
                    $end_loop = 7;
                else
                    $end_loop = $tongtrang;
            }
        }


        // FOR ENABLING THE FIRST BUTTON
        if ($first_btn && $id > 1) {
            $dau = "<li  class=''><a href='" . site_url($url) . "'>Đầu</a></li>";
        } else if ($first_btn) {
            $dau = "<li  class='text'>Đầu</li>";
        }

        // FOR ENABLING THE PREVIOUS BUTTON
        if ($previous_btn && $id > 1) {
            $tam = $id - 1;
            $lui = "<li class=''><a href='" . site_url($url . $tam) . "'>Lùi</a></li>";
        } else if ($previous_btn) {
            $lui = "<li class='text'>Lùi</li>";
        }


        if ($next_btn && $id < $tongtrang) {
            $tam2 = $id + 1;
            $toi = "<li class=''><a href='" . site_url($url . $tam2) . "'> Tới </a></li>";
        } else if ($next_btn) {
            $toi = "<li class='text'>Tới</li>";
        }

        // TO ENABLE THE END BUTTON
        if ($last_btn && $id < $tongtrang) {
            $cuoi = "<li  class=''><a href='" . site_url($url . $tongtrang) . "'> Cuối </a></li>";
        } else if ($last_btn) {
            $cuoi = "<li class='text'>Cuối</li>";
        }
        if ($count > 0) {
            for ($i = $start_loop; $i <= $end_loop; $i++) {
                if ($i == $id)
                    $num.="<li class='page'><a href='#' title='' onclick='return false'>$i</a></li>";
                else
                    $num.="<li><a href='" . site_url($url . $i) . "' title=''>$i</a></li>";
            }
        }
        if ($count > 0 && $tongtrang > 1)
            $link = "
		<ul class='pagination'>
            
			" . $dau . $lui . $num . $toi . $cuoi . "
			
		</ul>
			";
        else
            $link = '';

        return $link;
    }

}
