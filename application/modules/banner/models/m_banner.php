<?php

class M_banner extends CI_Model{

	function __construct()

	{

		parent::__construct();

	}

	//====================================

	function show_image_in_album($id,$id_album){

		$this->db->where("image_id",$id);

		$this->db->where_in("album_id",$id_album);

		return $this->db->get("imagealbum")->num_rows();

	}

    function show_image_in_menu($id,$id_album){

        $this->db->where("image_id",$id);

        $this->db->where_in("menu_id",$id_album);

        return $this->db->get("imagemenu")->num_rows();

    }

	//==================list artilce=============

	function show_list_article_page_m($limit,$offset){

		$this->db->select("articledetail.article_name,articledetail.article_summary,article.id,article.article_weight,article.article_status,images.thumb");

		$this->db->where("country.name",'vn');

		$this->db->order_by('article.date_create',"DESC");

		$this->db->limit($limit,$offset);

		$this->db->from('article');

		$this->db->join('articledetail','articledetail.article_id=article.id');

		$this->db->join('articleimages','articleimages.article_id=article.id');

		$this->db->join('images','articleimages.image_id=images.id');

		$this->db->join('country','articledetail.country_id=country.id');

		return $this->db->get()->result();

	}

	//================== list artilce category==================

	function show_list_term_where($where=array(),$lang='vn'){

		$this->db->select("termdetail.term_name,term.id,term.date_create,term.date_modify,term.status,term.type");

		$this->db->where("country.name",$lang);

                $this->db->where($where);

		$this->db->order_by('term.weight',"ASC");

                $this->db->group_by('term.id');

		$this->db->from('term');

		$this->db->join('termdetail','termdetail.term_id=term.id');

		$this->db->join('country','termdetail.country_id=country.id');

		return $this->db->get()->result();

	}

	//=============== Detail artilce category=====================

	function show_detail_term_id($id,$lang='vn'){

		$this->db->select("termdetail.term_name,term.id,term.date_create,term.author,term.weight,term.edit,term.per,term.date_modify,term.status,term.parent_id");

		$this->db->where("country.name",$lang);

		$this->db->where("term.id",$id);

		$this->db->order_by('term.weight',"ASC");

		$this->db->from('term');

		$this->db->join('termdetail','termdetail.term_id=term.id');

		$this->db->join('country','termdetail.country_id=country.id');

		return $this->db->get()->row();

	}

	//================= detail Article===================

	function show_detail_article_id($id,$lang='vn'){

		$this->db->select("country.unit,article.article_views,article.author,articledetail.article_summary,articledetail.article_description,articledetail.article_name,article.id,article.date_create,article.date_modify,article.article_status,images.thumb,articleimages.image_id,images.name as name_img,images.id as id_img,article.article_weight,article.article_hot");

		$this->db->where("country.name",$lang);

		$this->db->where("article.id",$id);

		$this->db->from('article');

		$this->db->join('articledetail','articledetail.article_id=article.id');

		//$this->db->join('articleterm','articleterm.article_id=article.id');

		$this->db->join('articleimages','articleimages.article_id=article.id');

		$this->db->join('images','articleimages.image_id=images.id');

		$this->db->join('country','articledetail.country_id=country.id');

		return $this->db->get()->row();

	}

        // serach header

	function show_list_article_limit_search($key,$limit,$lang='vn'){

		$this->db->select("articledetail.article_name,articledetail.article_summary,article.id,article.date_create,article.date_modify,article.article_status,images.thumb");

		$this->db->where("country.name",$lang);

		$this->db->like("articledetail.article_name",$key);

		$this->db->order_by('article.article_weight',"DESC");

		$this->db->order_by('article.date_modify',"DESC");

		$this->db->limit($limit);

		$this->db->from('article');

		$this->db->join('articledetail','articledetail.article_id=article.id');

		$this->db->join('articleimages','articleimages.article_id=article.id');

		$this->db->join('images','articleimages.image_id=images.id');

		$this->db->join('country','articledetail.country_id=country.id');

		return $this->db->get()->result();

	}	

	function show_list_term_search($key,$limit,$lang='vn'){

		$this->db->select("termdetail.term_name,term.id,term.date_create,term.date_modify,term.status");

		$this->db->where("country.name",$lang);

		$this->db->like("termdetail.term_name",$key);

		$this->db->order_by('term.weight',"ASC");

		$this->db->limit($limit);

		$this->db->from('term');

		$this->db->join('termdetail','termdetail.term_id=term.id');

		$this->db->join('country','termdetail.country_id=country.id');

		return $this->db->get()->result();

	}

	//====================================

	function count_list_image_album_where($album){

		$this->db->select("images.*");

        $this->db->where("album.id",$album);

		$this->db->order_by('images.id',"DESC");

		$this->db->from('images');

		$this->db->join('imagealbum','imagealbum.image_id=images.id');

		$this->db->join('album','imagealbum.album_id=album.id');

		$this->db->group_by('images.id');

		return $this->db->get()->num_rows();

	}

	function show_list_image_page_m_where($album,$limit,$offset){

		$this->db->select("images.*,imagealbum.album_id");

        $this->db->where("album.id",$album);

		$this->db->limit($limit,$offset);

        $this->db->from('images');

        $this->db->join('imagealbum','imagealbum.image_id=images.id');

        $this->db->join('album','imagealbum.album_id=album.id');

        $this->db->group_by('images.id');

		return $this->db->get()->result();

	}

    function show_detail_image_id($id){

        $this->db->select('images.*');

        $this->db->where("images.id",$id);

        $this->db->from('images');

        return $this->db->get()->row();

    }



	

	

}

