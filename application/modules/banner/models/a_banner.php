<?php

class A_banner extends CI_Model{

	function __construct()

	{

		parent::__construct();

	}

    function show_list_image_album_where($params){

        $this->db->select($params['select']);

        $this->db->where($params['where']);

        $this->db->order_by($params['order'],$params['asc']);
		
		$this->db->order_by('images.id', 'DESC');

        $this->db->group_by('images.id');

        $this->db->from('images');

        if($params['limit']>0){

            $this->db->limit($params['limit']);

        }

        $this->db->join('imagealbum','imagealbum.image_id=images.id');

        $this->db->join('album','imagealbum.album_id=album.id');

        $this->db->group_by('images.id');

        return $this->db->get()->result();

    }

}