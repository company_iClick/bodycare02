<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="contentcontainer med left">
        <div class="headings altheading"><h2>Thêm mới hình ảnh</h2></div>
        <div class="contentbox">
            <p >
                <label for="textfield"><strong>Tiêu đề </strong></label>
                <input name="title" value="<?= $image->title ?>" type="text" id="textfield" class="inputbox">
                <?php echo form_error('title'); ?>
            </p>
            <!-- --------------------------------------------------------------- -->
            <p >
                <label for="textfield"><strong>Link web</strong></label>
                <input name="link" value="<?= $image->link ?>" type="text" id="textfield" class="inputbox">
                <?php echo form_error('link'); ?>
            </p>
            <!-- -------------------Hanh dinh kem------------------------------- -->
            <p style="display: none"><input type="radio" value="img" name="chose_img" id="radio_img" checked>Image</p>
             <span id="div_img">
                <label for="smallbox"><strong>Up hình</strong></label>
                    <div class="wrap">
                        <!-- image preview area-->
                        <img id="uploadPreview" style="display:none;"/>
                        <!-- image uploading form -->
                        <input style="float:left" id="uploadImage" type="file" accept="image/jpeg" name="img"/>
                        <!-- hidden inputs -->
                        <input type="hidden" id="x" name="x"/>
                        <input type="hidden" id="y" name="y"/>
                        <input type="hidden" id="w" name="w"/>
                        <input type="hidden" id="h" name="h"/>
                    </div><!--wrap--><img alt="Loading" src="theme_admin/img/loading.gif">
                                Uploading...
                    <div style="clear: both;height: 10px"></div>
                 <?php echo form_error('img'); ?>
                 <?php  if (file_exists($this->m_banner->show_detail_image_id($id)->name)) { ?>
                     <img src="<?php echo $this->m_banner->show_detail_image_id($id)->name?>" width="70px">
                 <?php }?>
                 <div style="clear: both"></div>
                    <br>
                 <span class="smltxt">(Hình mới sẽ được up sau khi "Thêm mới")</span>
              </span>
             <span id="div_google">
                    <textarea cols="70" id="google_adsense" name="google_adsense" rows="10"><?= $this->input->post('google_adsense') ?></textarea>
             </span>
            <p>
                <input type="radio" value="1" name="status" <?php if($image->status==1){?>  checked <?php }?>>Hiển thị hình
                <input type="radio" value="0" name="status" <?php if($image->status==0){?>  checked <?php }?>>Ẩn hình
            </p>
            <!-- --------------------------------------------------------------- -->
            <p>
                <label for="smallbox"><strong>Thứ Tự: </strong></label>
                <select name="arrange">
                    <?php for ($i = 1; $i <= 20; $i++) { ?>
                        <option <?php if($image->weight == $i) echo 'selected' ?> value="<?= $i ?>"><?= $i ?></option>
                    <?php } ?>
                </select>
                <br>
                <span class="smltxt">(Số thứ tự càng nhỏ, độ ưu tiên càng cao)</span>
            </p>
            <input type="submit" class="btn" value="Cập nhật" name="ok"/>
        </div>
        <!-- end contentbox -->
    </div>
    <div class="contentcontainer sml right">
        <div class="headings altheading">
            <h2 class="left">Sơ đồ danh mục</h2>
        </div>
        <div class="contentbox">
            <ul class="list">
                <?php foreach($this->global_function->show_list_table_where(array("id !="=>0, 'status' => 1),"album", "*", "weight", "asc") as $row){
                    $album=$this->m_banner->show_image_in_album($image->id, $row->id);
                    ?>
                    <li><input <?php if($album >0) echo 'checked';?> class="check_item" type="checkbox" value="check_album_<?php echo $row->id ?>" name="check_album[<?php echo $row->id ?>]" >
                        <?php echo $row->name ?></li>

                <?php } ?>
            </ul>
        </div>
    </div>
</form>
<!-- --------------------------------------------------------------- -->
<!-- <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>-->
<script type="text/javascript">
    $(document).ready(function () {
        $("#div_google").hide();
        $("#radio_google").click(function () {
            $("#div_google").fadeIn("slow");
            $("#div_img").fadeOut("slow");
        });
        $("#radio_img").click(function () {
            $("#div_google").fadeOut("slow");
            $("#div_img").fadeIn("slow");
        });
    });
</script>
