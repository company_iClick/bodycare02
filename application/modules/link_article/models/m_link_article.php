<?php



class m_link_article extends CI_Model {



    function __construct() {

        parent::__construct();

        $this->load->database();

    }

    function show_list_link_article($where = array(), $limit, $offset) {

        $this->db->where($where);

        $this->db->order_by('id', "ASC");

        $this->db->order_by('weight', "ASC");

        $this->db->limit($limit, $offset);

        $this->db->from('link_article');

        return $this->db->get()->result();

        $this->db->free_result();

    }



    // get id

    function get_link_articleID($id) {

        $this->db->where('link_article.id', $id);

        $this->db->from('link_article');

        return $this->db->get()->row();

    }

}

