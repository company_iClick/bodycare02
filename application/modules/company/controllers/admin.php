<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    protected $meta_title;
    protected $meta_keywords;
    protected $meta_description;
    protected $project = "";

    function __construct() {

        parent::__construct();

        // Your own constructor code

        $this->load->helper(array('url', 'text', 'form', 'file'));

        $this->load->library(array('session', 'form_validation', 'ftp'));

        $this->load->database();

        $this->load->model(array('general', "global_function"));

        $this->template->set_template('admin');        // Set template 

        $this->template->write('mod', "company"); // set mod
    }

    function admin() {

        parent::Controller();
    }

    function index() {

        //exit('aa');

        if (!($this->general->Checkpermission("edit"))) {

            redirect(base_url("admin/not-permission"));
        } else {

            $data = array();

            $data['name_project'] = $this->project;
            $data['mod'] = 'company';
            $data['breadcrumb'] = '<li>>></li><li class="current">Thông tin công ty</li>';

            if ($this->input->post() == true) {

                 
                $sql = array(
                    'name' => $this->input->post('name'),
                    'website' => $this->input->post('website'),
                    'address' => $this->input->post('address'),
                    'link' => $this->input->post('link'),
                    'phone' => $this->input->post('phone'),
                    'tel' => $this->input->post('tel'),
                    'facebook' => $this->input->post('facebook'),
                    'google' => $this->input->post('google'),
                    'twitter' => $this->input->post('twitter'),
                    'youtube' => $this->input->post('youtube'),
                    'email' => $this->input->post('email'),
                    'fax' => $this->input->post('fax'),
                    'footer' => $this->input->post('footer'),
                    'slogan' => $this->input->post('slogan'),
                    'title' => $this->input->post('title'),
                    'keywords' => $this->input->post('keywords'),
                    'descriptions' => $this->input->post('descriptions'),
                    'code_meta' => $this->input->post('code_meta'),
                    'gift_summary' => $this->input->post('gift_summary'),
                );

                if (($_FILES['logo']['name'] != '')) {
                    if ($photo = $this->global_function->upload('logo', './uploads/default/', 'gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|PMP')) {
                        $sql['logo'] = $photo['file_name'];                    
                    }
                }

                if (($_FILES['logo_mobile']['name'] != '')) {
                    if ($photo = $this->global_function->upload('logo_mobile', './uploads/default/', 'gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|PMP')) {
                        $sql['logo_mobile'] = $photo['file_name'];                    
                    }
                }

                $this->db->where('id', 1);

                $this->db->update('company', $sql);

                if (isset($_FILES['file']['name'])) {

                    $array_img = count($_FILES['file']['name']);

                    $files = $_FILES;

                    $this->load->library('upload', $this->set_upload_options());

                    $this->load->library('image_lib', $this->set_upload_options());

                    $title = $this->input->post("title");

                    for ($i = 0; $i < $array_img; $i++) {

                        $_FILES['file']['name'] = $files['file']['name'][$i];

                        $_FILES['file']['type'] = $files['file']['type'][$i];

                        $_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$i];

                        $_FILES['file']['error'] = $files['file']['error'][$i];

                        $_FILES['file']['size'] = $files['file']['size'][$i];

                        if (file_exists("./" . $_FILES['file']['name'])) {

                            @unlink("./" . $_FILES['file']['name']);
                        }

                        $this->upload->initialize($this->set_upload_options());

                        if (!$this->upload->do_upload("file")) {

                            echo $this->upload->display_errors();

                            exit;

                            echo "<script>alert('Cập nhật bi lỗi');</script>";
                        } else {
                            
                        }
                    }
                }

                redirect(base_url('admin/company/') . '?messager=success');
            }



            $this->template->write_view('content', 'admin/index', $data, TRUE);

            $this->template->render();
        }
    }

    function set_upload_options() {

        //  upload an image options
        $config = array();
        $config['upload_path'] = "./";
        $config['allowed_types'] = 'xml|txt|html';
        $config['image_library'] = 'gd2';

        $config['max_size'] = '0';
        return $config;
    }

    //===============================================
}
