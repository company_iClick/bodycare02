<?php
class M_pill_number extends CI_Model
{
    protected $_table = 'pill_number';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_pill_number($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function get_pill_numberID($id)
    {
        $this->db->where('id', $id);
        $this->db->from($this->_table);
        return $this->db->get()->row();
    }    
    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    function count_product($pill_number_id)
    {
         $this->db->select('product.id');
         $this->db->where('tmp_product_pill_number.pill_number_id', $pill_number_id);
         $this->db->group_by('product.id');
         $this->db->from('product');
         $this->db->join('tmp_product_pill_number', 'tmp_product_pill_number.product_id=product.id');
         return $this->db->count_all_results();
    }
}

