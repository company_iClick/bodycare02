<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php echo $breadcrumbs; ?> 
            <div class="box_contact">
               <?php echo $info->long_lat; ?>
            </div>
            
            <div class="box_info_form">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div id="info_company_contact">
                            <ul>
                                <li class="c_home"><?php echo $info->name; ?></li>
                                <li class="c_address">Địa chỉ: <?php echo $info->address; ?></li>
                                <li class="c_phone">Điện thoại: <?php echo $info->tel; ?></li>
                                <li class="c_fax">Fax: <?php echo $info->fax; ?></li>
                                <li class="c_website">Email: <?php echo $info->email; ?></li>
                            </ul>
                        </div>
                        <?php if(!empty($group_contact)){ ?>
                        <div id="info_contact_group">
                            <ul>
                            <?php foreach ($group_contact as $gr){ ?>
                                <li class="clearfix">
                                    <label><?php echo $gr->name; ?></label>
                                    <span class="phone"><?php echo $gr->phone; ?></span>
                                    <span class="email"><?php echo $gr->email; ?></span>
                                </li>
                            <?php } ?>
                            </ul>
                        </div>
                        <?php } ?>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-5">
                        <div id="form_contact">
                            <form method="post" name="frm_contact">
                                <div class="form-group clearfix">
                                    <div class="col-xs-12">                                       
                                        <input type="text" name="name" value="<?php echo $name; ?>" class="form-control" placeholder="Họ và tên">
                                        <?php echo form_error("name") ?>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <div class="col-xs-12">
                                        <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" placeholder="Email">
                                        <?php echo form_error("email") ?>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <div class="col-xs-12">                                       
                                        <input type="text" name="phone" value="<?php echo $phone; ?>" class="form-control" placeholder="Số điện thoại">                               
                                        <?php echo form_error("phone") ?>
                                    </div>
                                </div>
                                
                                <div class="form-group clearfix">
                                    <div class="col-xs-12">                  
                                        <textarea class="form-control" rows="5" name="content" placeholder="Nội dung"><?php echo $content; ?></textarea>
                                        <?php echo form_error("content") ?>
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <div class="col-xs-12">
                                        <input type="submit" id="send_contact" class="btn btn-primary" value="Gửi">
                                    </div>
                                </div>
                            </form>
                            <?php if($this->input->get('message') == 'success'){ ?>
                            <div class="alert alert-success">Bạn đã gửi liên hệ thành công</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>