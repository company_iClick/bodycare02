<?php

class Contact extends MX_Controller

{

    function __construct()

    {
        parent::__construct();

        $this->load->database();

        $this->load->helper(array("url"));

        $this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
	

    }
	
	protected $_table = 'contact';

    function index(){
        
        $data['breadcrumb'] = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Liên hệ</li>
                     </ul>';

        $data['active']='contact';

        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                    </li>
                    <li class="category3"><strong>Liên hệ</strong></li>
                 </ul>';
        
        $data['name'] = strip_tags($this->input->post('name'));
        $data['phone'] = strip_tags($this->input->post('phone'));
        $data['email'] = strip_tags($this->input->post('email'));
        $data['content'] = strip_tags($this->input->post('content'));
        
      
        $this->form_validation->set_rules('name', "Họ và tên", 'trim|required|min_length[5]|max_length[75]');
        $this->form_validation->set_rules('email', "Email", 'trim|required|valid_email|s_unique[users.email]');
        $this->form_validation->set_rules('phone', "Điện thoại", 'trim|required|callback__validate_phone');
        $this->form_validation->set_rules('content', "Nội dung", 'trim|required|min_length[5]'); 
        
        $this->form_validation->set_error_delimiters('<p class="show_error">', '</p>');
        if ($this->form_validation->run() == true) {
            $sql=array(
                 "email"=>$data['email'],
                 "name"=>$data['name'],
                 "content"=>$data['content'],
                 "phone"=>$data['phone'],              
                 "weight"=>1,
                 "status"=>1,
                 "created_at"=>date('y-m-d H:i:s', time()),
             );
            if($this->db->insert("contact",$sql)){
                redirect(current_url().'?message=success');
            }
        }
        
        $data['group_contact'] = $this->global_function->get_array_object(array('id !=' => 0), 'id, name, email, phone', 'group_contact');
        
        $data['info'] = $this->global_function->get_tableWhere(array("id" => 1), "company", "*");

        $this->template->write('mod', 'contact');

        $this->template->write('title', 'Liên hệ');

        $this->template->write_view('content', 'public/index', $data, TRUE);

        $this->template->render();

    }

  

    

}