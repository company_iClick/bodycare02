<?php

class Video extends MX_Controller {

    protected $_table = 'video';

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model(array("banner/a_banner"));
    }
    
    function index(){
       redirect(site_url());
    }

    function detail($slug = ''){
        $detail = $data['detail'] = $this->global_function->get_row_object(array('slug' => $slug), '*', 'video');
        $id = $detail->id;

        $views = $detail->views + 1;
        $this->db->where('id', $id);
        $this->db->update('video', array('views' => $views));

        $data['breadcrumb'] = '<a href="'. current_url() .'">Video</a>';

        $data['other_video'] = $this->global_function->get_array_object(array('id !=' => $id), 'id, name, slug, photo, created_at', 'video', 10, array('field' => 'id', 'sort' => 'RANDOM'));

        $this->template->write('mod', 'video_detail');
        $title = (!empty($detail->title)) ? $detail->title : $detail->name;
        $data['share_face'] = 
        '<meta property="og:type" content="website" />
        <meta property="og:url" content="'.current_url().'"/>
        <meta property="og:title" content="'.$title.'"/>
        <meta property="og:description" content="'.$detail->description.'" />
        <meta property="og:image" content="'.base_url()._upload_video.$detail->photo.'"/>';
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/detail', $data, TRUE);
        $this->template->render();
    }
   
}
?>
