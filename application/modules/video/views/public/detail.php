<div class="wrapper_full">
    <div class="wrapper_full_1">

        <div class="left_detail_news"><?php  $this->load->view(BLOCK . 'block_video'); ?></div>


        <div class="right_detail_news">

            <h1 class="name_cate_news_detail"><a href="<?php echo current_url(); ?>" title="<?php echo $detail->name; ?>"><?php echo $detail->name; ?></a></h1>

            <div class="content_news">
                <?php echo $detail->summary; ?>
                <div class="videoWrapper"><iframe width="100%" height="500" src="https://www.youtube.com/embed/<?php echo $detail->code; ?>?wmode=transparent&rel=0&feature=oembed" frameborder="0" allowfullscreen=""></iframe></div>
            </div>

            <div class="print_back" id="product">
                <span class="icon-quaylai" onclick="goBack()"><p>Quay lại trang trước</p></span>
                <span class="icon-intrangnay" onclick="myFunction()"><p>In trang này</p></span>
            </div>

            
            <?php if(!empty($other_video)){
                $i = 0;
            ?>
            <div class="orther_news">
                <div class="orther_title">Video khác</div>
                <?php foreach ($other_video as $row) {
                    $a_name = $row->name;
                    $a_link = site_url('video/' . $row->slug);
                    $imgUrl = base_url(_upload_video . $row->photo);
                    $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=125&h=81&zc=1');
                    $class = (($i+1) % 2 == 0) ? ' child_news_orther1': '';
                    echo sprintf('<div class="child_news_orther%s">
                                    <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                                    <span class="summary_news_orther">
                                        <a href="%s" title="%s">%s</a>
                                    </span>
                                </div>', $class, $a_link, $a_name, $imgUrl, $a_name, $a_link, $a_name, $a_name);
                    $i++;
                } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
</div>
<?php  $this->load->view('front/block/policy') ?>