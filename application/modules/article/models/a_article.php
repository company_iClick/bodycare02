<?php
class A_article extends CI_Model
{
    protected $_table = 'article';
    protected $_tmp_atg = 'tmp_article_tag';
    protected $_tgs = 'tags';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function get_article_in_tags($tag_id, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.photo, $this->_table.thumb, $this->_table.folder, $this->_table.summary");
        $this->db->where($where);
        $this->db->where("$this->_tmp_atg.tag_id", $tag_id);
        $this->db->where(array("$this->_table.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_atg", "$this->_tmp_atg.article_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_list_article_term_parent($term_id, $parent)
    {
        $array = array(
            $term_id
        );
        if ($parent == 0) {
            $list = $this->global_function->show_list_table_where(array(
                "status" => 1,
                "parent_id" => $term_id
            ), "term", "id");
            if (count($list) > 0) {
                foreach ($list as $l) {
                    $array[] = $l->id;
                }
            }
        }
        $this->db->where_in("tmp_term_article.term_id", $array);
        $this->db->group_by('article.id');
        $this->db->from('article');
        $this->db->join("tmp_term_article", "tmp_term_article.article_id=article.id");
        return $this->db->get()->num_rows();
    }
    function show_list_article_term_parent($term_id, $limit = 1, $offset = 1, $page = 0, $parent = 0)
    {
        $array = array(
            $term_id
        );
        if ($parent == 0) {
            $list = $this->global_function->show_list_table_where(array(
                "status" => 1,
                "parent_id" => $term_id
            ), "term", "id");
            if (count($list) > 0) {
                foreach ($list as $l) {
                    $array[] = $l->id;
                }
            }
        }
        $this->db->select("article.id,article.name,article.slug,article.summary,article.photo");
        $this->db->where_in("tmp_term_article.term_id", $array);
        $this->db->order_by('article.weight', "DESC");
        $this->db->order_by('article.id', "DESC");
        $this->db->group_by('article.id');
        if ($page > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->from('article');
        $this->db->join("tmp_term_article", "tmp_term_article.article_id=article.id");
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function show_list_article_term_first($term_id)
    {
        $this->db->select("article.name,article.slug,article.content,article.photo");
        $this->db->where("tmp_term_article.term_id", $term_id);
        $this->db->order_by('article.id', "DESC");
        $this->db->group_by('article.id');
        $this->db->from('article');
        $this->db->join("tmp_term_article", "tmp_term_article.article_id=article.id");
        return $this->db->get()->result();
    }
    function show_list_article_other($id, $limit, $offset, $page = 0)
    {
        $array = array(
            0
        );
        $list  = $this->global_function->show_list_table_where(array(
            "article_id" => $id
        ), "tmp_term_article", "term_id");
        if (count($list) > 0) {
            foreach ($list as $l) {
                $array[] = $l->term_id;
            }
        }
        $this->db->select("article.id,article.name,article.slug,article.photo");
        $this->db->where_in("tmp_term_article.term_id", $array);
        $this->db->where(array(
            "article.id !=" => $id,
            "article.status" => 1
        ));
        $this->db->order_by('article.id', "DESC");
        if ($page > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->from('article');
        $this->db->join("tmp_term_article", "tmp_term_article.article_id=article.id");
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function show_list_article_other_type($id, $type, $limit, $offset, $page = 0)
    {
        $this->db->select("article.id,article.name,article.slug,article.photo");
        $this->db->where(array(
            "article.type" => $type,
            "article.id !=" => $id,
            "article.status" => 1
        ));
        $this->db->order_by('article.id', "DESC");
        if ($page > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->from('article');
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function show_list_term_article_where($article_id)
    {
        $this->db->select("term.name,term.slug,term.id,parent_id");
        $this->db->where_in("tmp_term_article.article_id", $article_id);
        $this->db->order_by('term.id', "DESC");
        $this->db->from('term');
        $this->db->join("tmp_term_article", "tmp_term_article.term_id=term.id");
        return $this->db->get()->row();
    }
    function show_list_article_type($type, $limit, $offset, $page = 0, $where = array())
    {
        $this->db->select("id, name, slug, summary, photo, folder, created_at");
        $this->db->where($where);
        $this->db->where(array(
            "article.type" => $type,
            "article.status" => 1
        ));
        $this->db->order_by('article.id', "DESC");
        if ($page > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->from('article');
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function count_list_article_type($type, $where = array())
    {
        $this->db->where($where);
        $this->db->where(array(
            "article.type" => $type,
            "article.status" => 1
        ));
        $this->db->from('article');
        return $this->db->count_all_results();
    }

    function show_list_tmp_article_product_type_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_product_type');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_skin_type_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_skin_type');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_hair_type_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_hair_type');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_action_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_action');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_capacity_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_capacity');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_weigh_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_weigh');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_pill_number_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_pill_number');
        return $this->db->get()->result();
    }
}

