<?php

class M_article extends CI_Model{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_article($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from('article');
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function get_articleID($id)
    {
        $this->db->where('article.id', $id);
        $this->db->from('article');
        return $this->db->get()->row();
    }
    function show_list_tmp_article_term_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_term_article');
        return $this->db->get()->result();
    }

    function show_list_article_term($list_cate_id)
    {
        $this->db->where_in('tmp_term_article.term_id', $list_cate_id);
        $this->db->order_by('article.weight', "ASC");
        $this->db->order_by('article.id', "article.DESC");
        $this->db->group_by("article.id");
        $this->db->from('article');
        $this->db->join('tmp_term_article', 'tmp_term_article.article_id=article.id');
        return $this->db->get()->result();
        $this->db->free_result();
    }

    function show_list_article_keyword($keyword, $type)
    {
        $this->db->where('type', $type);
        $this->db->like('slug', $keyword);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->from('article');
        return $this->db->get()->result();
    }

    function show_list_article_keyword_category($category_id, $keyword, $type)
    {
        if($category_id != 0) $this->db->where('article.category_id', $category_id);
        $this->db->where('type', $type);
        $this->db->like('slug', $keyword);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->from('article');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_product_type_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_product_type');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_skin_type_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_skin_type');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_hair_type_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_hair_type');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_action_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_action');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_capacity_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_capacity');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_weigh_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_weigh');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_pill_number_id($article_id) {
        $this->db->where('article_id', $article_id);
        $this->db->from('tmp_article_pill_number');
        return $this->db->get()->result();
    }
}

