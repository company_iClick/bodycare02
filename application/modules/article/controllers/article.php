<?php

class Article extends MX_Controller
{
    protected $_table = 'article';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array(
            'session',
            'cart'
        ));

        $this->load->helper(array('cms_price_helper', 'percen_calculate_helper', 'cms_price_v1_helper', 'percen_calculate_v1_helper'));
        $this->load->model(array(
            "article/a_article", "banner/a_banner", "product/a_product", "category/a_category"
        ));
        $this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
    }
    function index()
    {
        $data['breadcrumb'] = '<a href="'. current_url() .'">Kiến thức làm đẹp</a>';
        $data['active']     = 'news';
        $data['type'] = 3;

        $data['news_slide'] = $this->global_function->get_array_object(array('slide' => 1, 'type' => 3), 'id, name, slug, summary, photo, folder, created_at', 'article', 10);
        $data['news_hot'] = $this->global_function->get_array_object(array('hot' => 1, 'type' => 3), 'id, name, slug, photo, folder, created_at', 'article', 2);

        $categories = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug', 'category');
        if(!empty($categories)){
            foreach ($categories as $row) {
                $row->article = $this->global_function->get_array_object(array('category_id' => $row->id), 'id, name, slug, summary, photo, folder, created_at', 'article', 3);
            }
        }

        $data['categories'] = $categories;

        $this->template->write('mod', 'news');
        $this->template->write('title', 'Kiến thức làm đẹp');
        $this->template->write_view('content', 'public/index', $data, TRUE);
        $this->template->render();
    }

    function category($slug = '', $page_no = 1)
    {
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug', 'category');
        if(empty($detail)) redirect(site_url());
        $data['breadcrumb'] = '<a href="'. site_url('kien-thuc-lam-dep') .'" class="fist_menu_cate_a">Kiến thức làm đẹp</a>';
        $data['breadcrumb'] .= '<a href="'. current_url() .'">'. $detail->name .'</a>';
        $data['active']     = 'news';
        $data['type'] = 3;
        $id = $detail->id;
        $data['detail'] = $detail;

        $page_co        = 30;
        $start          = ($page_no - 1) * $page_co;
        $count          = $this->a_article->count_list_article_type(3, array('category_id' => $id));
        $data['list_article'] = $this->a_article->show_list_article_type(3, $page_co, $start, 1, array('category_id' => $id));
        $data['paging']   = $this->global_function->paging($page_co, $count, "/kien-thuc-lam-dep/" . $this->uri->segment(2) . "/", $page_no);

        $this->template->write('mod', 'news');
        $this->template->write('title', $detail->name);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function detail($slug = '', $page_no = 1)
    {
        $this->load->model(array("tags/a_tags"));
        
        $detail = $this->global_function->get_row_object(array('slug' => $slug, 'type' => 3), 'id, category_id, cate_id, brand_id, origin_id, name, slug, content, folder, photo, title, keywords, description, created_at, views', 'article');
           
        $data['detail'] = $detail;
        if(empty($detail)) redirect(site_url());
        $data['breadcrumb'] = '<a href="'. site_url('kien-thuc-lam-dep') .'" class="fist_menu_cate_a">Kiến thức làm đẹp</a>';
        $data['breadcrumb'] .= '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $id = $detail->id;
        $data['detail'] = $detail;
        $data['type'] = 3;

        $layout = $data['layout'] = $this->input->get('layout');

        $data['detail_category'] = $this->global_function->get_row_object(array('id' => $detail->category_id), 'id, name, slug', 'category');
        $data['other_news'] = $this->global_function->get_array_object(array('id !=' => $id, 'category_id' => $detail->category_id, 'type' => 3), 'id, name, slug, summary, photo, folder, created_at', 'article', 6);

        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $cate_param = '';
        $list_cate_id = '';
        if($detail->cate_id > 0){
            $cate_param = 'category';
            $list_cate_id = $this->a_category->get_categroy_tree_id($detail->cate_id);
        }
        $brand_value = $detail->brand_id;
        $origin_value = $detail->origin_id;
        
        $product_type_value = array();
        $list_product_type = $this->a_article->show_list_tmp_article_product_type_id($id);
        if(!empty($list_product_type)){
            foreach ($list_product_type as $row) {
                $product_type_value[] = $row->product_type_id;
            }
        }

        $skin_type_value = array();
        $list_skin_type = $this->a_article->show_list_tmp_article_skin_type_id($id);
        if(!empty($list_skin_type)){
            foreach ($list_skin_type as $row) {
                $skin_type_value[] = $row->skin_type_id;
            }
        }

        $hair_type_value = array();
        $list_hair_type = $this->a_article->show_list_tmp_article_hair_type_id($id);
        if(!empty($list_hair_type)){
            foreach ($list_hair_type as $row) {
                $hair_type_value[] = $row->hair_type_id;
            }
        }

        $action_value = array();
        $list_action = $this->a_article->show_list_tmp_article_action_id($id);
        if(!empty($list_hair_type)){
            foreach ($list_action as $row) {
                $action_value[] = $row->action_id;
            }
        }

        $capacity_value = array();
        $list_capacity = $this->a_article->show_list_tmp_article_capacity_id($id);
        if(!empty($list_capacity)){
            foreach ($list_capacity as $row) {
                $capacity_value[] = $row->capacity_id;
            }
        }

        $weigh_value = array();
        $list_weigh = $this->a_article->show_list_tmp_article_weigh_id($id);
        if(!empty($list_weigh)){
            foreach ($list_weigh as $row) {
                $weigh_value[] = $row->weigh_id;
            }
        }

        $pill_number_value = array();
        $list_pill_number = $this->a_article->show_list_tmp_article_pill_number_id($id);
        if(!empty($list_pill_number)){
            foreach ($list_pill_number as $row) {
                $pill_number_value[] = $row->pill_number_id;
            }
        }

        $price_value = array();

        /*$count = $this->a_product->count_product_in_category($list_cate_id);
        $list_product = $this->a_product->get_product_in_category($list_cate_id, $page_co, $start);*/
        $count = $this->a_product->count_product_filter_param($cate_param, $list_cate_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value);
        $list_product = $this->a_product->product_filter_param($cate_param, $list_cate_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $page_co, $start);

        $data['list_product'] = $list_product;
        $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);

        $sql['views'] = $data['detail']->views + 1;
        $this->db->where('id', $data['detail']->id);
        $this->db->update('article', $sql);

        $data['tags'] = $this->a_tags->show_list_tags_in_article($id);

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;
        $data['share_face'] = 
        '<meta property="og:type" content="website" />
        <meta property="og:url" content="'.current_url().'"/>
        <meta property="og:title" content="'.$title.'"/>
        <meta property="og:description" content="'.$detail->description.'" />
        <meta property="og:image" content="'.base_url()._upload_article.$detail->folder.'/'.$detail->photo.'"/>';

        $this->template->write('mod', 'detail');
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/detail', $data, TRUE);
        $this->template->render();
    }

    function customer_support($slug = '')
    {
        $detail = $this->global_function->get_row_object(array('slug' => $slug, 'type' => 4), 'id, category_id, cate_id, name, slug, content, title, keywords, description, created_at, views', 'article');
        if(empty($detail)) redirect(site_url());
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $id = $detail->id;
        $data['detail'] = $detail;
        $data['type'] = 4;

        $sql['views'] = $data['detail']->views + 1;
        $this->db->where('id', $data['detail']->id);
        $this->db->update('article', $sql);

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'customer_support');
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/customer_support', $data, TRUE);
        $this->template->render();
    }

    function static_page($slug = '')
    {
        $detail = $this->global_function->get_row_object(array('slug' => $slug, 'type' => 1), 'id, category_id, cate_id, name, slug, content, title, keywords, description, created_at, views', 'article');
        if(empty($detail)) redirect(site_url());
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $id = $detail->id;
        $data['detail'] = $detail;
        $data['type'] = 1;

        $sql['views'] = $data['detail']->views + 1;
        $this->db->where('id', $data['detail']->id);
        $this->db->update('article', $sql);

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'detail');
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/customer_support', $data, TRUE);
        $this->template->render();
    }

    function index01()
    {
        $data['breadcrumb'] = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Thông tin tài khoản</li>
                     </ul>';
        $data['active']     = 'promotion_news';
        $this->template->write('mod', 'promotion_news');
        $this->template->write('title', 'promotion_news');
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }
    function detail1()
    {
        $data['breadcrumb'] = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Thông tin tài khoản</li>
                     </ul>';
        $data['active']     = 'promotion_news';
        $this->template->write('mod', 'promotion_news');
        $this->template->write('title', 'promotion_news');
        $this->template->write_view('content', 'public/detail', $data, TRUE);
        $this->template->render();
    }
    function promotion_news($page_no = 1)
    {
        $data['breadcrumb'] = '<div class="category-title">
            <div class="mom_breadcrumb breadcrumb breadcrumbs">
                <div class="breadcrumbs-plus">';
        $data['breadcrumb'] .= ' <span><a itemprop="url" href="' . site_url() . '" class="home"><span itemprop="title">Trang chủ</span></a></span>';
        $data['breadcrumb'] .= '<span class="separator"><i class="sep fa-icon-double-angle-right"></i></span>Tin khuyến mãi';
        $data['breadcrumb'] .= '</div></div></div>';
        $page_co        = 10;
        $start          = ($page_no - 1) * $page_co;
        $count          = $this->a_article->count_list_article_type(6);
        $data['result'] = $this->a_article->show_list_article_type(6, $page_co, $start, 1);
        $data['link']   = $this->global_function->paging($page_co, $count, "/" . $this->uri->segment(1) . "/", $page_no);
        $data['active'] = 'promotion_news';
        $this->template->write('mod', 'promotion_news');
        $this->template->write('title', 'promotion_news');
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }
    function page_detail($slug = '')
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Tuyển dụng</li>
                     </ul>';
        $data['detail']      = $this->global_function->get_row_object(array(
            'type' => 1,
            'slug' => $slug
        ), $this->_table, 'name, title, keywords, description, content');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                    </li>
                    <li>
                        <a href="' . site_url('tuyen-dung') . '" title="Tuyển dụng">Tuyển dụng</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                 </ul>';
        $data['active']      = 'page';
        $this->template->write('mod', 'page');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/about', $data, TRUE);
        $this->template->render();
    }
    function about()
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Giới thiệu</li>
                     </ul>';
        $data['detail']      = $this->global_function->get_row_object(array(
            'type' => 4
        ), $this->_table, 'name, content');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li class="category3"><strong>Giới thiệu</strong></li>
                     </ul>';
        $data['active']      = 'about';
        $this->template->write('mod', 'about');
        $this->template->write('title', 'Giới thiệu');
        $this->template->write_view('content', 'public/about', $data, TRUE);
        $this->template->render();
    }
    function about_detail($slug = '')
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Giới thiệu</li>
                     </ul>';
        $data['about']       = $this->global_function->get_array_object(array(
            'type' => 4
        ), 'id, name, slug', $this->_table);
        $data['detail']      = $this->global_function->get_row_object(array(
            'type' => 4,
            'slug' => $slug
        ), $this->_table, 'name, summary, title, keywords, description, content, picture');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li>
                            <a href="' . site_url('gioi-thieu') . '" title="Giới thiệu">Giới thiệu</a>
                            <span>/</span>
                        </li>
                        <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                     </ul>';
        $data['share_face']  = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="' . $data['detail']->name . '"/>
<meta property="og:description" content="' . $data['detail']->name . '" />
<meta property="og:image" content="' . base_url() . _upload_tintuc . $data['detail']->picture . '"/>
<meta property="og:image:type" content="image/png">';
        $data['active']      = 'about';
        $this->template->write('mod', 'about');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/about', $data, TRUE);
        $this->template->render();
    }
    function report($page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Báo cáo</li>
                     </ul>';
        $data['active']      = 'report';
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li class="category3"><strong>Báo cáo</strong></li>
                 </ul>';
        $list_term           = $this->global_function->get_array_object(array(
            'type' => 5
        ), 'id, name, slug', 'term');
        $term                = $list_term[0];
        $page_co             = 5;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/bao-cao/page/", $page_no);
        $this->template->write('mod', 'report');
        $this->template->write('title', 'Báo cáo');
        $this->template->write_view('content', 'public/report', $data, TRUE);
        $this->template->render();
    }
    function report_term($slug, $page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Báo cáo</li>
                     </ul>';
        $data['active']      = 'report';
        $data['term']        = $term = $this->global_function->get_tableWhere(array(
            "status" => 1,
            "slug" => $slug,
            "type" => 5
        ), "term", "name,id,slug,type");
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('bao-cao') . '" title="Báo cáo">Báo cáo</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['term']->name . '</strong></li>
                 </ul>';
        $page_co             = 5;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/" . $slug . "/page/", $page_no);
        $this->template->write('mod', 'report');
        $this->template->write('title', $data['term']->name);
        $this->template->write_view('content', 'public/report', $data, TRUE);
        $this->template->render();
    }
    function report_detail($slug = '')
    {
        $data['breadcrumb']    = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Báo cáo</li>
                     </ul>';
        $data['detail']        = $this->global_function->get_row_object(array(
            'type' => 5,
            'slug' => $slug
        ), $this->_table, 'id, name, summary, title, keywords, description, content, picture');
        $data['term']          = $this->a_article->show_list_term_article_where($data['detail']->id);
        $data['breadcrumbs']   = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                    </li>
                    <li>
                        <a href="' . site_url('bao-cao') . '" title="Báo cáo">Báo cáo</a>
                        <span>/</span>
                    </li>
                    <li>
                        <a href="' . site_url($data['term']->slug) . '" title="' . $data['term']->name . '">' . $data['term']->name . '</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                 </ul>';
        $data['share_face']    = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="' . $data['detail']->name . '"/>
<meta property="og:description" content="' . $data['detail']->summary . '" />
<meta property="og:image" content="' . base_url() . _upload_tintuc . $data['detail']->picture . '"/>
<meta property="og:image:type" content="image/png">';
        $data['term_id']       = $data['term']->id;
        $data['other_article'] = $this->global_function->get_array_object(array(
            'type' => 5,
            'id !=' => $data['detail']->id
        ), 'id, name, slug', 'article');
        $data['active']        = 'report';
        $this->template->write('mod', 'report');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/about', $data, TRUE);
        $this->template->render();
    }
    function recruitment()
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Tuyển dụng</li>
                     </ul>';
        $data['result']      = $this->global_function->get_array_object(array(
            'type' => 2
        ), 'id, name, slug, quantity, workplace, deadline', 'article');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li class="category3"><strong>Tuyển dụng</strong></li>
                 </ul>';
        $data['active']      = 'recruitment';
        $this->template->write('mod', 'recruitment');
        $this->template->write('title', 'Tuyển dụng');
        $this->template->write_view('content', 'public/recruitment', $data, TRUE);
        $this->template->render();
    }
    function recruitment_detail($slug = '')
    {
        $data['breadcrumb']    = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Báo cáo</li>
                     </ul>';
        $data['detail']        = $this->global_function->get_row_object(array(
            'type' => 2,
            'slug' => $slug
        ), $this->_table, 'id, name, title, keywords, description, content, quantity, workplace, salary, deadline');
        $data['breadcrumbs']   = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                    </li>
                    <li>
                        <a href="' . site_url('tuyen-dung') . '" title="Tuyển dụng">Tuyển dụng</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                 </ul>';
        $data['other_article'] = $this->global_function->get_array_object(array(
            'type' => 2,
            'id !=' => $data['detail']->id
        ), 'id, name, slug', 'article');
        $data['share_face']    = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="' . $data['detail']->name . '"/>
<meta property="og:description" content="' . $data['detail']->name . '" />
<meta property="og:image" content=""/>
<meta property="og:image:type" content="image/png">';
        $data['active']        = 'report';
        $this->template->write('mod', 'report');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/recruitment_detail', $data, TRUE);
        $this->template->render();
    }
    function package_solutions($page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Gói giải pháp năng lượng</li>
                     </ul>';
        $data['active']      = 'package_solutions';
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li class="category3"><strong>Giải pháp năng lượng mặt trời</strong></li>
                     </ul>';
        $list_term           = $this->global_function->get_array_object(array(
            'type' => 7
        ), 'id, name, slug', 'term');
        $term                = $list_term[0];
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/goi-giai-phap-nang-luong/page/", $page_no);
        $this->template->write('mod', 'package_solutions');
        $this->template->write('title', 'Gói giải pháp năng lượng');
        $this->template->write_view('content', 'public/package_solutions', $data, TRUE);
        $this->template->render();
    }
    function package_solutions_term($slug, $page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Gói giải pháp năng lượng</li>
                     </ul>';
        $data['active']      = 'report';
        $data['term']        = $term = $this->global_function->get_tableWhere(array(
            "status" => 1,
            "slug" => $slug,
            "type" => 7
        ), "term", "name,id,slug,type");
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('goi-giai-phap-nang-luong') . '" title="Giải pháp năng lượng mặt trời">Giải pháp năng lượng mặt trời</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['term']->name . '</strong></li>
                 </ul>';
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/" . $slug . "/page/", $page_no);
        $this->template->write('mod', 'package_solutions');
        $this->template->write('title', $data['term']->name);
        $this->template->write_view('content', 'public/package_solutions', $data, TRUE);
        $this->template->render();
    }
    function package_solutions_detail($slug = '')
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Gói giải pháp năng lượng</li>
                     </ul>';
        $data['detail']      = $this->global_function->get_row_object(array(
            'type' => 7,
            'slug' => $slug
        ), $this->_table, 'id, name, title, keywords, description, content, summary, picture');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('goi-giai-phap-nang-luong') . '" title="Giải pháp năng lượng mặt trời">Giải pháp năng lượng mặt trời</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                 </ul>';
        $data['share_face']  = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="' . $data['detail']->name . '"/>
<meta property="og:description" content="' . $data['detail']->summary . '" />
<meta property="og:image" content="' . base_url() . _upload_tintuc . $data['detail']->picture . '"/>
<meta property="og:image:type" content="image/png">';
        $data['term']        = $this->a_article->show_list_term_article_where($data['detail']->id);
        $data['term_id']     = $data['term']->id;
        $data['active']      = 'package_solutions';
        $this->template->write('mod', 'package_solutions');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/package_solutions_detail', $data, TRUE);
        $this->template->render();
    }
    function news($page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Tin tức hàng công nghệ</li>
                     </ul>';
        $data['active']      = 'package_solutions';
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li class="category3"><strong>Tin tức hàng công nghệ</strong></li>
                     </ul>';
        $list_term           = $this->global_function->get_array_object(array(
            'type' => 3
        ), 'id, name, slug', 'term');
        $term                = $list_term[0];
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/tin-tuc-hang-cong-nghe/page/", $page_no);
        $this->template->write('mod', 'news');
        $this->template->write('title', 'Tin tức hàng công nghệ');
        $this->template->write_view('content', 'public/news', $data, TRUE);
        $this->template->render();
    }
    function news_term($slug, $page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Tin tức hàng công nghệ</li>
                     </ul>';
        $data['active']      = 'report';
        $data['term']        = $term = $this->global_function->get_tableWhere(array(
            "status" => 1,
            "slug" => $slug,
            "type" => 3
        ), "term", "name,id,slug,type");
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('tin-tuc-hang-cong-nghe') . '" title="Tin tức hàng công nghệ">Tin tức hàng công nghệ</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['term']->name . '</strong></li>
                 </ul>';
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/" . $slug . "/page/", $page_no);
        $this->template->write('mod', 'news');
        $this->template->write('title', $data['term']->name);
        $this->template->write_view('content', 'public/news', $data, TRUE);
        $this->template->render();
    }
    function news_detail($slug = '')
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Tin tức hàng công nghệ</li>
                     </ul>';
        $data['detail']      = $this->global_function->get_row_object(array(
            'type' => 3,
            'slug' => $slug
        ), $this->_table, 'id, name, title, keywords, description, content, summary, picture');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('tin-tuc-hang-cong-nghe') . '" title="Tin tức hàng công nghệ">Tin tức hàng công nghệ</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                 </ul>';
        $data['share_face']  = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="' . $data['detail']->name . '"/>
<meta property="og:description" content="' . $data['detail']->summary . '" />
<meta property="og:image" content="' . base_url() . _upload_tintuc . $data['detail']->picture . '"/>
<meta property="og:image:type" content="image/png">';
        $data['term']        = $this->a_article->show_list_term_article_where($data['detail']->id);
        $data['term_id']     = $data['term']->id;
        $data['active']      = 'news';
        $this->template->write('mod', 'news');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/news_detail', $data, TRUE);
        $this->template->render();
    }
    function tintuc($page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Tin tức</li>
                     </ul>';
        $data['active']      = 'package_solutions';
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li class="category3"><strong>Tin tức</strong></li>
                     </ul>';
        $list_term           = $this->global_function->get_array_object(array(
            'type' => 9
        ), 'id, name, slug', 'term');
        $term                = $list_term[0];
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/tin-tuc/page/", $page_no);
        $this->template->write('mod', 'tintuc');
        $this->template->write('title', 'Tin tức');
        $this->template->write_view('content', 'public/tintuc', $data, TRUE);
        $this->template->render();
    }
    function tintuc_term($slug, $page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Tin tức</li>
                     </ul>';
        $data['active']      = 'report';
        $data['term']        = $term = $this->global_function->get_tableWhere(array(
            "status" => 1,
            "slug" => $slug,
            "type" => 9
        ), "term", "name,id,slug,type");
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('tin-tuc') . '" title="Tin tức">Tin tức</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['term']->name . '</strong></li>
                 </ul>';
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/" . $slug . "/page/", $page_no);
        $this->template->write('mod', 'tintuc');
        $this->template->write('title', $data['term']->name);
        $this->template->write_view('content', 'public/tintuc', $data, TRUE);
        $this->template->render();
    }
    function tintuc_detail($slug = '')
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Tin tức</li>
                     </ul>';
        $data['detail']      = $this->global_function->get_row_object(array(
            'type' => 9,
            'slug' => $slug
        ), $this->_table, 'id, name, title, keywords, description, content, summary, picture');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('tin-tuc') . '" title="Tin tức">Tin tức</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                 </ul>';
        $data['share_face']  = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="' . $data['detail']->name . '"/>
<meta property="og:description" content="' . $data['detail']->summary . '" />
<meta property="og:image" content="' . base_url() . _upload_tintuc . $data['detail']->picture . '"/>
<meta property="og:image:type" content="image/png">';
        $data['term']        = $this->a_article->show_list_term_article_where($data['detail']->id);
        $data['term_id']     = $data['term']->id;
        $data['active']      = 'tintuc';
        $this->template->write('mod', 'tintuc');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/tintuc_detail', $data, TRUE);
        $this->template->render();
    }
    function lighting_projects($page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Dự án chiếu sáng tiết kiệm điện</li>
                     </ul>';
        $data['active']      = 'package_solutions';
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li class="category3"><strong>Dự án chiếu sáng tiết kiệm điện</strong></li>
                     </ul>';
        $list_term           = $this->global_function->get_array_object(array(
            'type' => 8
        ), 'id, name, slug', 'term');
        $term                = $list_term[0];
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/goi-giai-phap-nang-luong/page/", $page_no);
        $this->template->write('mod', 'package_solutions');
        $this->template->write('title', 'Gói giải pháp năng lượng');
        $this->template->write_view('content', 'public/lighting_projects', $data, TRUE);
        $this->template->render();
    }
    function lighting_projects_term($slug, $page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Dự án chiếu sáng tiết kiệm điện</li>
                     </ul>';
        $data['active']      = 'report';
        $data['term']        = $term = $this->global_function->get_tableWhere(array(
            "status" => 1,
            "slug" => $slug,
            "type" => 8
        ), "term", "name,id,slug,type");
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('du-an-chieu-sang') . '" title="Dự án chiếu sáng tiết kiệm điện">Dự án chiếu sáng tiết kiệm điện</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['term']->name . '</strong></li>
                 </ul>';
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/" . $slug . "/page/", $page_no);
        $this->template->write('mod', 'package_solutions');
        $this->template->write('title', $data['term']->name);
        $this->template->write_view('content', 'public/lighting_projects', $data, TRUE);
        $this->template->render();
    }
    function lighting_projects_detail($slug = '')
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Dự án chiếu sáng tiết kiệm điện</li>
                     </ul>';
        $data['detail']      = $this->global_function->get_row_object(array(
            'type' => 8,
            'slug' => $slug
        ), $this->_table, 'id, name, title, keywords, description, content, summary, picture');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li>
                            <a href="' . site_url('du-an-chieu-sang') . '" title="Dự án chiếu sáng tiết kiệm điện">Dự án chiếu sáng tiết kiệm điện</a>
                            <span>/</span>
                        </li>
                        <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                     </ul>';
        $data['term']        = $this->a_article->show_list_term_article_where($data['detail']->id);
        $data['term_id']     = $data['term']->id;
        $data['share_face']  = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="' . $data['detail']->name . '"/>
<meta property="og:description" content="' . $data['detail']->summary . '" />
<meta property="og:image" content="' . base_url() . _upload_tintuc . $data['detail']->picture . '"/>
<meta property="og:image:type" content="image/png">';
        $data['active']      = 'package_solutions';
        $this->template->write('mod', 'package_solutions');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/lighting_projects_detail', $data, TRUE);
        $this->template->render();
    }
    function tech_projects($page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Dự án hàng công nghệ</li>
                     </ul>';
        $data['active']      = 'package_solutions';
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li class="category3"><strong>Dự án hàng công nghệ</strong></li>
                     </ul>';
        $list_term           = $this->global_function->get_array_object(array(
            'type' => 6
        ), 'id, name, slug', 'term');
        $term                = $list_term[0];
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/du-an-hang-cong-nghe/page/", $page_no);
        $this->template->write('mod', 'package_solutions');
        $this->template->write('title', 'Dự án hàng công nghệ');
        $this->template->write_view('content', 'public/tech_projects', $data, TRUE);
        $this->template->render();
    }
    function tech_projects_term($slug, $page_no = 1)
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Dự án hàng công nghệ</li>
                     </ul>';
        $data['active']      = 'report';
        $data['term']        = $term = $this->global_function->get_tableWhere(array(
            "status" => 1,
            "slug" => $slug,
            "type" => 6
        ), "term", "name,id,slug,type");
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                     </li>
                    <li>
                        <a href="' . site_url('du-an-hang-cong-nghe') . '" title="Dự án hàng công nghệ">Dự án hàng công nghệ</a>
                        <span>/</span>
                    </li>
                    <li class="category3"><strong>' . $data['term']->name . '</strong></li>
                 </ul>';
        $page_co             = 6;
        $start               = ($page_no - 1) * $page_co;
        $count               = $this->a_article->count_list_article_term_parent($term->id, $parent = 0);
        $data['result']      = $this->a_article->show_list_article_term_parent($term->id, $page_co, $start, 1);
        $data['paging']      = $this->global_function->paging($page_co, $count, "/" . $slug . "/page/", $page_no);
        $this->template->write('mod', 'tech_projects');
        $this->template->write('title', $data['term']->name);
        $this->template->write_view('content', 'public/tech_projects', $data, TRUE);
        $this->template->render();
    }
    function tech_projects_detail($slug = '')
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Dự án hàng công nghệ</li>
                     </ul>';
        $data['detail']      = $this->global_function->get_row_object(array(
            'type' => 6,
            'slug' => $slug
        ), $this->_table, 'id, name, title, keywords, description, content, summary, picture');
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                        <li class="home">
                            <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                           <span>/</span>
                         </li>
                        <li>
                            <a href="' . site_url('du-an-chieu-sang') . '" title="Dự án hàng công nghệ">Dự án hàng công nghệ</a>
                            <span>/</span>
                        </li>
                        <li class="category3"><strong>' . $data['detail']->name . '</strong></li>
                     </ul>';
        $data['term']        = $this->a_article->show_list_term_article_where($data['detail']->id);
        $data['term_id']     = $data['term']->id;
        $data['share_face']  = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="' . $data['detail']->name . '"/>
<meta property="og:description" content="' . $data['detail']->summary . '" />
<meta property="og:image" content="' . base_url() . _upload_tintuc . $data['detail']->picture . '"/>
<meta property="og:image:type" content="image/png">';
        $data['active']      = 'tech_projects';
        $this->template->write('mod', 'tech_projects');
        $title = (!empty($data['detail']->title)) ? $data['detail']->title : $data['detail']->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $data['detail']->keywords);
        $this->template->write('description', $data['detail']->description);
        $this->template->write_view('content', 'public/tech_project_detail', $data, TRUE);
        $this->template->render();
    }
    
    function office_service()
    {
        $data['breadcrumb']  = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Dịch vụ cho thuê văn phòng</li>
                     </ul>';
        $data['info']        = $this->global_function->get_tableWhere(array(
            "id" => 1
        ), "company", "*");
        $data['breadcrumbs'] = '<ul class="breadcrumbs">
                    <li class="home">
                        <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                       <span>/</span>
                    </li>
                    <li class="category3"><strong>Dịch vụ cho thuê văn phòng</strong></li>
                 </ul>';
        $data['share_face']  = '<meta property="og:type" content="website" />
<meta property="og:url" content="' . current_url() . '"/>
<meta property="og:title" content="Dịch vụ cho thuê văn phòng"/>
<meta property="og:description" content="Dịch vụ cho thuê văn phòng" />
<meta property="og:image" content="' . base_url() . _images . 'toanhachipsang.png"/>
<meta property="og:image:type" content="image/png">';
        $data['active']      = 'offiece_service';
        $this->template->write('mod', 'office_service');
        $this->template->write('title', 'Dịch vụ cho thuê văn phòng');
        $this->template->write_view('content', 'public/office_service', $data, TRUE);
        $this->template->render();
    }
    function other_service()
    {
        $data['breadcrumb'] = '<ul class="breadcrumb">
                         <li class="home">
                             <a href="' . site_url() . '" title="Trang chủ">Trang chủ</a>
                         </li>
                         <li>Các lĩnh vực khác</li>
                     </ul>';
        $data['active']     = 'other_service';
        $this->template->write('mod', 'other_service');
        $this->template->write('title', 'Các lĩnh vực khác');
        $this->template->write_view('content', 'public/other_service', $data, TRUE);
        $this->template->render();
    }
}