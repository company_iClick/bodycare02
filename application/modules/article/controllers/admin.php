<?php
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array(
            "url"
        ));
        $this->load->model(array(
            "article/m_article", 
            "term/m_term",
            "category/m_category",
            "tags/m_tags",
            "general"
        ));
        $this->template->set_template('admin');
    }
    function index($type = 1, $page_no = 1)
    {
        if (!($this->general->Checkpermission("view_article")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST['show']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(base_url('admin/article/index/' . $type . "/" . $page_no) . '?messager=success');
        }
        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->hide_more($a);
            }
            redirect(base_url('admin/article/index/' . $type . "/" . $page_no) . '?messager=success');
        }
        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->delete_more($a);
            }
            redirect(base_url('admin/article/index/' . $type . "/" . $page_no) . '?messager=success');
        }
        $page_co         = 20;
        $start           = ($page_no - 1) * $page_co;
        $count           = $this->general->count_table_where(array(
            'type' => $type
        ), 'article');
        $data['page_no'] = $page_no;
        $data['type']    = $type;
        $data['item']    = $this->m_article->show_list_article(array(
            'type' => $type
        ), $page_co, $start);

        //term filter
        $term             = $this->input->get('term');
        if (isset($term) && is_numeric($term)) {
            $list_cate_id = $this->m_term->get_categroy_tree_id($term);
            $data['item'] = $this->m_article->show_list_article_term($term);
        }

        //keyword search
        if($this->input->post('keyword') != ''){
            $keyword = $this->my_lib->changeTitle($this->input->post('keyword'));
            $data['item'] = $this->m_article->show_list_article_keyword($keyword, $type);
        }

        $data['term'] = $this->m_term->menu_admin(0);

        $data['link']    = $this->general->paging($page_co, $count, 'admin/article/index/' . $type . "/", $page_no);
        $this->template->write('mod', "article_" . $type);
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();
    }
    function add($type = 1)
    {
        if (!($this->general->Checkpermission("add_article")))
            redirect(base_url("admin/not-permission"));
        $date               = date("d-m-Y");
        $data               = array();
        $data['breadcrumb'] = '<li>>></li><li><a href="back/article">Bài Viết</a></li><li>>></li><li class="current">Thêm mới</li>';
        if (isset($_POST['ok'])) {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required');
            if ($type == 3)
                $this->form_validation->set_rules('photo', 'Hình', 'callback_imageValidation');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'category_id' => $this->input->post('category_id'),
                    'cate_id' => $this->input->post('cate_id'),
                    'brand_id' => $this->input->post('brand_id'),
                    'origin_id' => $this->input->post('origin_id'),
                    'folder' => $date,
                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'slug' => ($this->input->post('slug') != '') ? $this->input->post('slug') : $this->my_lib->changeTitle($this->input->post('name')),
                    'status' => $this->input->post('status'),
                    'summary' => $this->input->post('summary'),
                    'content' => $this->input->post('content'),
                    'type' => $type,
                    'title' => $this->input->post('title'),
                    'keywords' => $this->input->post('keywords'),
                    'description' => $this->input->post('description'),
                    'quantity' => $this->input->post('quantity'),
                    'workplace' => $this->input->post('workplace'),
                    'salary' => $this->input->post('salary'),
                    'deadline' => $this->input->post('deadline'),
                    'user_id' => $this->session->userdata('admin_login')->id
                );
                if (!is_dir("uploads/article/" . $date)) {
                    mkdir("uploads/article/" . $date, 0777);
                }
                if ($photo = $this->global_function->upload('photo', './uploads/article/' . $date . '/', 'gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|PMP')) {
                    $sql['photo'] = $photo['file_name'];
                    $sql['thumb'] = $this->global_function->create_thumb('./uploads/article/' . $date . '/', $photo, 200, 200);
                }
                $this->db->insert('article', $sql);
                $id = $this->db->insert_id();
                if (isset($id)) {

                    if($this->input->post('tags') != '') {
                        $tags = array_unique($this->input->post('tags'));
                        foreach ($tags as $key => $value) {
                            $this->db->insert('tmp_article_tag', array('article_id' => $id, 'tag_id' => $value));
                        }
                    }

                    $term = $this->input->post("term");
                    if (!empty($term)) {
                        foreach ($term as $t) {
                            $check = $this->global_function->count_tableWhere(array(
                                "term_id" => $t,
                                "article_id" => $id
                            ), "tmp_term_article");
                            if ($check == 0) {
                                $this->db->insert("tmp_term_article", array(
                                    "term_id" => $t,
                                    "article_id" => $id
                                ));
                            }
                        }
                    }

                    //insert product_type
                    $postProductType = $this->input->post('product_type');
                    if(!empty($postProductType)){
                        foreach ($postProductType as $key => $value) {
                            $this->db->insert('tmp_article_product_type', array('article_id' => $id, 'product_type_id' => $value));
                        }    
                    }

                    //insert skin_type
                    $postSkinType = $this->input->post('skin_type');
                    if(!empty($postSkinType)){
                        foreach ($postSkinType as $key => $value) {
                            $this->db->insert('tmp_article_skin_type', array('article_id' => $id, 'skin_type_id' => $value));
                        }    
                    }

                    //insert hair_type
                    $postHairType = $this->input->post('hair_type');
                    if(!empty($postHairType)){
                        foreach ($postHairType as $key => $value) {
                            $this->db->insert('tmp_article_hair_type', array('article_id' => $id, 'hair_type_id' => $value));
                        }    
                    }

                    //insert action
                    $postAction = $this->input->post('action');
                    if(!empty($postAction)){
                        foreach ($postAction as $key => $value) {
                            $this->db->insert('tmp_article_action', array('article_id' => $id, 'action_id' => $value));
                        }    
                    }

                    //insert capacity
                    $postCapacity = $this->input->post('capacity');
                    if(!empty($postCapacity)){
                        foreach ($postCapacity as $key => $value) {
                            $this->db->insert('tmp_article_capacity', array('article_id' => $id, 'capacity_id' => $value));
                        }    
                    }

                    //insert weigh
                    $postweigh = $this->input->post('weigh');
                    if(!empty($postweigh)){
                        foreach ($postweigh as $key => $value) {
                            $this->db->insert('tmp_article_weigh', array('article_id' => $id, 'weigh_id' => $value));
                        }    
                    }

                    //insert pill_number
                    $postPillNumber = $this->input->post('pill_number');
                    if(!empty($postPillNumber)){
                        foreach ($postPillNumber as $key => $value) {
                            $this->db->insert('tmp_article_pill_number', array('article_id' => $id, 'pill_number_id' => $value));
                        }    
                    }
                }
                redirect(base_url('admin/article/index/' . $type) . '?messager=success');
            }
        }
        $data['type'] = $type;
        $data['term'] = $this->global_function->show_list_table_where(array(
            "type" => $type,
            "status" => 1,
            "parent_id" => 0
        ), "term", "term.id,term.name");
        $this->template->write('mod', "article_" . $type);
        $data['category'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name', 'category');
        $data['cate'] = $this->m_category->menu_admin(0);
        $tags = $this->m_tags->show_list_tag_array();
        $data['tags'] = array();
        if(!empty($tags)) $data['tags'] = json_encode($tags);

        $data['brand'] = $this->global_function->get_array_object(array(), 'id, name', 'brand', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['product_type'] = $this->global_function->get_array_object(array(), 'id, name', 'product_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['skin_type'] = $this->global_function->get_array_object(array(), 'id, name', 'skin_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['hair_type'] = $this->global_function->get_array_object(array(), 'id, name', 'hair_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['origin'] = $this->global_function->get_array_object(array(), 'id, name', 'origin', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['action'] = $this->global_function->get_array_object(array(), 'id, name', 'action', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['capacity'] = $this->global_function->get_array_object(array(), 'id, name', 'capacity', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['weigh'] = $this->global_function->get_array_object(array(), 'id, name', 'weigh', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['pill_number'] = $this->global_function->get_array_object(array(), 'id, name', 'pill_number', 0, array('field' => 'name', 'sort' => 'ASC'));

        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }
    function edit($type = 1, $id)
    {
        $data['item'] = $this->m_article->get_articleID($id);
        if (!($this->general->Checkpermission("edit_article")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST['ok'])) {
            $folder = $data['item']->folder;
            $this->form_validation->set_rules('name', 'Tên', 'trim|required');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'category_id' => $this->input->post('category_id'),
                    'cate_id' => $this->input->post('cate_id'),
                    'brand_id' => $this->input->post('brand_id'),
                    'origin_id' => $this->input->post('origin_id'),
                    'folder' => $folder,
                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'slug' => ($this->input->post('slug') != '') ? $this->input->post('slug') : $this->my_lib->changeTitle($this->input->post('name')),
                    'status' => $this->input->post('status'),
                    'summary' => $this->input->post('summary'),
                    'content' => $this->input->post('content'),
                    'title' => $this->input->post('title'),
                    'keywords' => $this->input->post('keywords'),
                    'description' => $this->input->post('description'),
                    'quantity' => $this->input->post('quantity'),
                    'workplace' => $this->input->post('workplace'),
                    'salary' => $this->input->post('salary'),
                    'deadline' => $this->input->post('deadline'),
                    'modified_in' => date('Y-m-d H:i:s'),
                    'user_edit_id' => $this->session->userdata('admin_login')->id
                );

                if (!is_dir("uploads/article/" . $folder)) {
                    mkdir("uploads/article/" . $folder, 0777);
                }
                if ($photo = $this->global_function->upload('photo', './uploads/article/' . $folder . '/', 'gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|PMP')) {
                    $sql['photo'] = $photo['file_name'];
                    $sql['thumb'] = $this->global_function->create_thumb('./uploads/article/' . $folder . '/', $photo, 200, 200);
                    if (file_exists('./uploads/article/' . $folder . '/' . $data['item']->photo)) {
                        @unlink('./uploads/article/' . $folder . '/' . $data['item']->photo);
                    }
                    if (file_exists('./uploads/article/' . $folder . '/' . $data['item']->thumb)) {
                        @unlink('./uploads/article/' . $folder . '/' . $data['item']->thumb);
                    }
                }
                $this->db->where('id', $id);
                $this->db->update('article', $sql);
                if (isset($id)) {
                    $this->db->delete("tmp_term_article", array(
                        "article_id" => $id
                    ));
                    $term = $this->input->post("term");
                    if (!empty($term)) {
                        foreach ($term as $t) {
                            $this->db->insert("tmp_term_article", array(
                                "term_id" => $t,
                                "article_id" => $id
                            ));
                        }
                    }

                    //insert article_type   
                    $this->db->delete('tmp_article_product_type', array('article_id' => $id));
                    $postProductType = $this->input->post('product_type');
                    if(!empty($postProductType)){                        
                        foreach ($postProductType as $key => $value) {
                            $this->db->insert('tmp_article_product_type', array('article_id' => $id, 'product_type_id' => $value));
                        }    
                    }

                    //insert skin_type   
                    $this->db->delete('tmp_article_skin_type', array('article_id' => $id));
                    $postSkinType = $this->input->post('skin_type');
                    if(!empty($postSkinType)){
                        foreach ($postSkinType as $key => $value) {
                            $this->db->insert('tmp_article_skin_type', array('article_id' => $id, 'skin_type_id' => $value));
                        }    
                    }

                    //insert hair_type    
                    $this->db->delete('tmp_article_hair_type', array('article_id' => $id)); 
                    $postHairType = $this->input->post('hair_type');
                    if(!empty($postHairType)){
                        foreach ($postHairType as $key => $value) {
                            $this->db->insert('tmp_article_hair_type', array('article_id' => $id, 'hair_type_id' => $value));
                        }    
                    }

                    //insert action
                    $this->db->delete('tmp_article_action', array('article_id' => $id));
                    $postAction = $this->input->post('action');
                    if(!empty($postAction)){
                        foreach ($postAction as $key => $value) {
                            $this->db->insert('tmp_article_action', array('article_id' => $id, 'action_id' => $value));
                        }    
                    }

                    //insert capacity
                    $this->db->delete('tmp_article_capacity', array('article_id' => $id));
                    $postCapacity = $this->input->post('capacity');
                    if(!empty($postCapacity)){
                        foreach ($postCapacity as $key => $value) {
                            $this->db->insert('tmp_article_capacity', array('article_id' => $id, 'capacity_id' => $value));
                        }    
                    }

                    //insert weigh
                    $this->db->delete('tmp_article_weigh', array('article_id' => $id));
                    $postweigh = $this->input->post('weigh');
                    if(!empty($postweigh)){
                        foreach ($postweigh as $key => $value) {
                            $this->db->insert('tmp_article_weigh', array('article_id' => $id, 'weigh_id' => $value));
                        }    
                    }

                    //insert pill_number
                    $this->db->delete('tmp_article_pill_number', array('article_id' => $id));
                    $postPillNumber = $this->input->post('pill_number');
                    if(!empty($postPillNumber)){                       
                        foreach ($postPillNumber as $key => $value) {
                            $this->db->insert('tmp_article_pill_number', array('article_id' => $id, 'pill_number_id' => $value));
                        }    
                    }
                }
                redirect(base_url('admin/article/edit/' . $type . "/" . $id) . '?messager=success');
            }
        }
        $data['type'] = $type;
        $this->template->write('mod', "article_" . $type);
        $data['term'] = $this->global_function->show_list_table_where(array(
            "type" => $type,
            "status" => 1,
            "parent_id" => 0
        ), "term", "term.id,term.name");
        $data['category'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name', 'category');
        $data['cate'] = $this->m_category->menu_admin(0);
        $data['list_article_tag'] = $this->m_tags->show_list_tags_in_article($id);
        

        //get tmp_product_category
        $data["list_tmp_article_term"] = $this->m_article->show_list_tmp_article_term_id($id);

        $array_article_term = array();
        if(!empty($data["list_tmp_article_term"])){
            foreach ($data["list_tmp_article_term"] as $key => $value) {
                $array_article_term[] = $value->term_id;
            }
        } 
        $data['array_article_term'] = $array_article_term;


        $tags = $this->m_tags->show_list_tag_array();
        $data['tags'] = array();
        if(!empty($tags)) $data['tags'] = json_encode($tags);

        $data['brand'] = $this->global_function->get_array_object(array(), 'id, name', 'brand', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['product_type'] = $this->global_function->get_array_object(array(), 'id, name', 'product_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['skin_type'] = $this->global_function->get_array_object(array(), 'id, name', 'skin_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['hair_type'] = $this->global_function->get_array_object(array(), 'id, name', 'hair_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['origin'] = $this->global_function->get_array_object(array(), 'id, name', 'origin', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['action'] = $this->global_function->get_array_object(array(), 'id, name', 'action', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['capacity'] = $this->global_function->get_array_object(array(), 'id, name', 'capacity', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['weigh'] = $this->global_function->get_array_object(array(), 'id, name', 'weigh', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['pill_number'] = $this->global_function->get_array_object(array(), 'id, name', 'pill_number', 0, array('field' => 'name', 'sort' => 'ASC'));

        //get tmp_article_product_type
        $data["list_tmp_article_product_type"] = $this->m_article->show_list_tmp_article_product_type_id($id);
        $array_article_product_type = array();
        if(!empty($data["list_tmp_article_product_type"])){
            foreach ($data["list_tmp_article_product_type"] as $key => $value) {
                $array_article_product_type[] = $value->product_type_id;
            }
        } 
        $data['array_article_product_type'] = $array_article_product_type;

        //get tmp_article_skin_type
        $data["list_tmp_article_skin_type"] = $this->m_article->show_list_tmp_article_skin_type_id($id);
        $array_article_skin_type = array();
        if(!empty($data["list_tmp_article_skin_type"])){
            foreach ($data["list_tmp_article_skin_type"] as $key => $value) {
                $array_article_skin_type[] = $value->skin_type_id;
            }
        } 
        $data['array_article_skin_type'] = $array_article_skin_type;

        //get tmp_article_hair_type
        $data["list_tmp_article_hair_type"] = $this->m_article->show_list_tmp_article_hair_type_id($id);
        $array_article_hair_type = array();
        if(!empty($data["list_tmp_article_hair_type"])){
            foreach ($data["list_tmp_article_hair_type"] as $key => $value) {
                $array_article_hair_type[] = $value->hair_type_id;
            }
        } 
        $data['array_article_hair_type'] = $array_article_hair_type;

        //get tmp_article_action
        $data["list_tmp_article_action"] = $this->m_article->show_list_tmp_article_action_id($id);
        $array_article_action = array();
        if(!empty($data["list_tmp_article_action"])){
            foreach ($data["list_tmp_article_action"] as $key => $value) {
                $array_article_action[] = $value->action_id;
            }
        } 
        $data['array_article_action'] = $array_article_action;

        //get tmp_article_capacity
        $data["list_tmp_article_capacity"] = $this->m_article->show_list_tmp_article_capacity_id($id);
        $array_article_capacity = array();
        if(!empty($data["list_tmp_article_capacity"])){
            foreach ($data["list_tmp_article_capacity"] as $key => $value) {
                $array_article_capacity[] = $value->capacity_id;
            }
        } 
        $data['array_article_capacity'] = $array_article_capacity;

        //get tmp_article_weigh
        $data["list_tmp_article_weigh"] = $this->m_article->show_list_tmp_article_weigh_id($id);
        $array_article_weigh = array();
        if(!empty($data["list_tmp_article_weigh"])){
            foreach ($data["list_tmp_article_weigh"] as $key => $value) {
                $array_article_weigh[] = $value->weigh_id;
            }
        } 
        $data['array_article_weigh'] = $array_article_weigh;

        //get tmp_article_pill_number
        $data["list_tmp_article_pill_number"] = $this->m_article->show_list_tmp_article_pill_number_id($id);
        $array_article_pill_number = array();
        if(!empty($data["list_tmp_article_pill_number"])){
            foreach ($data["list_tmp_article_pill_number"] as $key => $value) {
                $array_article_pill_number[] = $value->pill_number_id;
            }
        } 
        $data['array_article_pill_number'] = $array_article_pill_number;

        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }
    function hide($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_article")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "article");
        redirect(base_url('admin/article/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function hide_more($id)
    {
        if (!($this->general->Checkpermission("edit_article")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "article");
        return true;
    }
    function hide_hot($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_article")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'hot' => 0
        ), "article");
        redirect(base_url('admin/article/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function show_more($id)
    {
        if (!($this->general->Checkpermission("edit_article")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "article");
        return true;
    }
    function show($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_article")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "article");
        redirect(base_url('admin/article/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function show_hot($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_article")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'hot' => 1
        ), "article");
        redirect(base_url('admin/article/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function delete($type, $id, $page_no)
    {
        $item   = $this->m_article->get_articleID($id);
        $folder = $item->folder;
        if (!($this->general->Checkpermission("delete_article")))
            redirect(base_url("admin/not-permission"));
        if ($this->db->delete('article', array(
            'id' => $id
        ))) {
            if (file_exists('./uploads/article/' . $folder . '/' . $item->photo)) {
                @unlink('./uploads/article/' . $folder . '/' . $item->photo);
            }
            if (file_exists('./uploads/article/' . $folder . '/' . $item->thumb)) {
                @unlink('./uploads/article/' . $folder . '/' . $item->thumb);
            }

            $this->db->delete('tmp_term_article', array('article_id' => $id));
        }
        redirect(base_url('admin/article/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function delete_more($id)
    {
        $item   = $this->m_article->get_articleID($id);
        $folder = $item->folder;
        if (!($this->general->Checkpermission("delete_article")))
            redirect(base_url("admin/not-permission"));
        if ($this->db->delete('article', array(
            'id' => $id
        ))) {
            if (file_exists('./uploads/article/' . $folder . '/' . $item->photo)) {
                @unlink('./uploads/article/' . $folder . '/' . $item->photo);
            }
            if (file_exists('./uploads/article/' . $folder . '/' . $item->thumb)) {
                @unlink('./uploads/article/' . $folder . '/' . $item->thumb);
            }

            $this->db->delete('tmp_term_article', array('article_id' => $id));
        }
        return true;
    }
    function imageValidation()
    {
        if (($_FILES['photo']['name'] == '')) {
            $this->form_validation->set_message('imageValidation', 'Bạn chưa chọn hình đại diện');
            return FALSE;
        }
        if (!$this->imageCheck($_FILES['photo']['type'])) {
            $this->form_validation->set_message('imageValidation', 'Định dạng file không đúng. Hãy chọn một hình ảnh.');
            return FALSE;
        }
    }
    private function imageCheck($extension)
    {
        $allowedTypes = array(
            'image/gif',
            'image/jpg',
            'image/png',
            'image/bmp',
            'image/jpeg'
        );
        return in_array($extension, $allowedTypes);
    }
}
