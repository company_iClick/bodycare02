<?php

$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager'), 'id' => $this->input->get('id'), 'type' => $this->input->get('type')));

?>

<!-- Alternative Content Box Start -->

<div class="contentcontainer">

    <div class="headings altheading"><h2>Danh sách</h2></div>
    <div class="contentbox">
        <?php if($type == 3){ ?>
        <?php if(!empty($term)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 200px">
            <label><b>Danh mục</b></label>
            <select name="check_category" id="check_category" style="float: right" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/article/'  . $type) ?>"> Tất cả</option>
                <?php foreach($term as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/article/' . $type).'?term='.$cate->id; ?>" <?php if($this->input->get('term') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <div style="clear: both;height: 10px"></div>
        <div>
            <label><b>Tìm kiếm</b></label>
            <form method="post" action="" name="frm_search" enctype="multipart/form-data">
                <input type="text" name="keyword" class="inputbox" value="<?php echo $this->input->post('keyword'); ?>" placeholder="Từ khóa...">
                <div class="clear" style="height: 10px"></div>
                <input type="button" name="ok" value="OK" class="btn">
            </form>
        </div>
        <div style="clear: both;height: 20px"></div>
        <?php } ?>

        <form method="post" action=""	enctype="multipart/form-data">
            <div class="extrabottom">
                <ul>
                    <?php if($type !=1 ){?>

                    <li>
                        <img src="theme_admin/img/icons/add.png" alt="Add" /> 
                        <a style="text-decoration: none;" href="admin/article/add/<?php echo $type?>">Thêm mới</a>
                    </li>

                    <li> <img src="theme_admin/img/icons/icon_approve.png" alt="Approve" />
                        <input class="a_button_act a_show" name="show" style="cursor: pointer" type="submit"	value="Hiện danh sách đã chọn" />
                    </li>

                    <li> <img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" />
                        <input class="a_button_act a_hide" name="hide" style="cursor: pointer" type="submit"	value="Ẩn danh sách đã chọn" />
                    </li>
                    <li> <img src="theme_admin/img/icons/icon_delete.png" alt="Delete" />
                        <p style="display:none">
                            <input class="a_button_act a_delete" name="delete" style="cursor: pointer" type="submit"	value="Delete" />
                        </p>
                        <a style=" cursor:pointer" class="delete" onclick="Delete()">Delete</a> 
                    </li>
                    <?php }?>
                </ul>            
            </div>

            <table width="100%">
                <thead>
                    <tr>
                        <th width="5%">ID</th>
                        <th width="30%">Name</th>
                        <th>Thứ tự</th>
                        <?php if($type==3){?>
                        <th>Slide</th>
                        <th>Nổi bật</th>
                        <th>Mẹo hay</th>
                        <?php }?>     
                        <th>Action</th>
                        <th><input name="" type="checkbox" value="" id="checkboxall" /></th>

                    </tr>

                </thead>

                <tbody>

                    <?php

                    $x = 0;

                    ?>

                    <?php foreach ($item as $a) { ?>

                        <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>

                            <td style="text-align:center"><?php echo $a->id ?></td>

                            <td><?php echo $a->name ?></td>

                            <td style="text-align:center"><?php echo $a->weight ?></td>
                            
                            <?php if($type==3){?>

                            <td style="text-align:center">
                                <?php if ($a->slide == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value_article('slide', 'article', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0, <?php echo $type; ?>)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value_article('slide', 'article', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1, <?php echo $type; ?>)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <td style="text-align:center">
                                <?php if ($a->hot == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value_article('hot', 'article', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0, <?php echo $type; ?>)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value_article('hot', 'article', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1, <?php echo $type; ?>)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <td style="text-align:center">
                                <?php if ($a->home == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value_article('home', 'article', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0, <?php echo $type; ?>)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value_article('home', 'article', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1, <?php echo $type; ?>)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <?php }?>
                            
                            <td style="text-align:center">

                                <a href="admin/article/edit/<?php echo $type."/".$a->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit"/></a>

                                <?php if ($a->status == 1) { ?>
                                    <a href="admin/article/hide/<?php echo $type."/". $a->id ?>/<?php echo $page_no ?>" title="Hiện"><img src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>

                                <?php } else { ?>

                                    <a href="admin/article/show/<?php echo $type."/".$a->id ?>/<?php echo $page_no ?>" title="Ẩn"><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>

                                <?php } ?>

                                <?php if($type !=1){?>

                                <a onclick="if (!confirm('Xác nhận xóa?'))return false;" href="admin/article/delete/<?php echo $type."/". $a->id ?>/<?php echo $page_no ?>" title="Xóa"><img

                                        src="theme_admin/img/icons/icon_delete.png" alt="Delete"/></a>

                                <?php }?>

                            </td>



                            <td style="text-align:center">

                                <?php if($type !=1){?>

                                <input type="checkbox" value="check_item[<?php echo $a->id ?>]" name="checkall[<?php echo $a->id ?>]" class="checkall"/>

                                <?php }?>

                            </td>



                        </tr>

                        <?php

                        $x++;

                    }

                        ?>

                    </tbody>
                </table>

                <div style="clear: both;"></div>
                <p style="display:none">
                    <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>
                </p>
        </form>

            <div style="clear: both"></div>

            <?php echo $link ?>

    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[name=ok]').click(function(){
            var keyword = $('input[name=keyword]').val();
            if(keyword == ''){
                alert('Bạn chưa nhập từ khóa');
                $('input[name=keyword]').focus();
                return false;
            }

            document.frm_search.submit();
        })
    })
</script>


