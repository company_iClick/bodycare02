<style>
    .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
    }
    * html .ui-autocomplete {
    height: 100px;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {

        function split( val ) {
          return val.split( /,\s*/ );
        }
        function extractLast( term ) {
          return split( term ).pop();
        }

        var tags = <?php echo $tags?>;

        $( "#tags" )
          // don't navigate away from the field on tab when selecting an item
          .on( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
              event.preventDefault();
            }
          })
          .autocomplete({
            minLength: 0,
            source: function( request, response ) {
              // delegate back to autocomplete, but extract the last term
              response( $.ui.autocomplete.filter(
                tags, extractLast( request.term ) ) );
            },
            focus: function() {
              // prevent value inserted on focus
              return false;
            },
            select: function( event, ui ) {
              var terms = split( this.value );
              var html = '<div style="float:left;margin-right:10px;padding:5px 7px;border:1px solid #ccc;position:relative">'+ ui.item.label +'</li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_tag"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="tags[]" value="'+ ui.item.id  +'"></div>';
              $('#append_input_tag').append(html);
              // remove the current input
              terms.pop();
              // add the selected item
              terms.push( ui.item.value );
              // add placeholder to get the comma-and-space at the end
              terms.push( "" );
              this.value = terms.join( ", " );
              

              return false;
            }
        });

    });


    $(document).on('click', '.delete_tag', function() {
        if(!confirm('Bạn muốn xóa tag này?')) return false;

        $(this).parent().remove();
    });


</script>

<?php

$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));

$cate_id = (isset($item)) ? $item->cate_id : $this->input->post('cate_id');
$category_id = (isset($item)) ? $item->category_id : $this->input->post('category_id');
$brand_id = (isset($item)) ? $item->brand_id : $this->input->post('brand_id');
$origin_id = (isset($item)) ? $item->origin_id : $this->input->post('origin_id');
$name = (isset($item)) ? $item->name : $this->input->post('name');
$slug = (isset($item)) ? $item->slug : $this->input->post('slug');
$summary = (isset($item)) ? $item->summary : $this->input->post('summary');
$content = (isset($item)) ? $item->content : $this->input->post('content');
$photo = (isset($item)) ? $item->photo : NULL;

$quantity = (isset($item)) ? $item->quantity : $this->input->post('quantity');
$workplace = (isset($item)) ? $item->workplace : $this->input->post('workplace');
$salary = (isset($item)) ? $item->salary : $this->input->post('salary');
$deadline = (isset($item)) ? $item->deadline : $this->input->post('deadline');

$title = (isset($item)) ? $item->title : $this->input->post('title');
$keywords = (isset($item)) ? $item->keywords : $this->input->post('keywords');
$description = (isset($item)) ? $item->description : $this->input->post('description');
$weight = (isset($item)) ? $item->weight : 1;
$button = (isset($item)) ? 'Cập nhật' : 'Thêm mới';

$array_article_term = isset($array_article_term) ? $array_article_term : $this->input->post('term');
$array_article_product_type = isset($array_article_product_type) ? $array_article_product_type : $this->input->post('product_type');
$array_article_skin_type = isset($array_article_skin_type) ? $array_article_skin_type : $this->input->post('skin_type');
$array_article_hair_type = isset($array_article_hair_type) ? $array_article_hair_type : $this->input->post('hair_type');
$array_article_action = isset($array_article_action) ? $array_article_action : $this->input->post('action');
$array_article_capacity = isset($array_article_capacity) ? $array_article_capacity : $this->input->post('capacity');
$array_article_weigh = isset($array_article_weigh) ? $array_article_weigh : $this->input->post('weigh');
$array_article_pill_number = isset($array_article_pill_number) ? $array_article_pill_number : $this->input->post('pill_number');

?>

<form name="them" method="post" id="them" action=""	enctype="multipart/form-data">
<div class="contentcontainer med left">
    <div class="headings altheading"><h2><?php echo $button; ?></h2></div>

    <div class="contentbox">
        <p>
            <label for="textfield"><strong>Tên</strong></label>
            <input type="text" class="inputbox" name="name" value="<?php echo $name; ?>" OnkeyUp="add_alias_vn(this)" />
        </p>
        <?php echo form_error('name'); ?>

        <p>
            <label for="textfield"><strong>Đường dẫn</strong></label>
            <input type="text" class="inputbox" name="slug" value="<?php echo $slug; ?>" />
        </p>

        <div class="list">

            <p>
                <label for="textfield"><strong>Title</strong></label>
                <input type="text" class="inputbox" name="title" value="<?php echo $title; ?>" />
            </p>

            <p>
                <label for="textfield"><strong>Keywords</strong></label>
                <textarea name="keywords" class="textarea" style="width:325px;border-radius: 4px;padding: 10px;" rows="4"><?php echo $keywords; ?></textarea>
            </p>

            <p>
                <label for="textfield"><strong>Description</strong></label>
                <textarea name="description" class="textarea" style="width:325px;border-radius: 4px;padding: 10px;" rows="4"><?php echo $description; ?></textarea>
            </p>

        </div>  
        
        <?php if($type == 2){ ?>
        <p>
            <label for="textfield"><strong>Số lượng</strong></label>
            <input type="text" class="inputbox" name="quantity" value="<?php echo $quantity; ?>" />
        </p>
        
        <p>
            <label for="textfield"><strong>Nơi làm việc</strong></label>
            <input type="text" class="inputbox" name="workplace" value="<?php echo $workplace; ?>" />
        </p>
        
        <p>
            <label for="textfield"><strong>Mức lương</strong></label>
            <input type="text" class="inputbox" name="salary" value="<?php echo $salary; ?>" />
        </p>
        
        <p>
            <label for="textfield"><strong>Ngày hết hạn</strong></label>
            <input type="text" id="datepicker" class="inputbox" name="deadline" value="<?php echo $deadline; ?>" />
        </p>
        <?php } ?>
        
        <?php if($type != 4 && $type != 2 && $type != 1){ ?>
        <p>
            <label for="textfield"><strong>Mô tả</strong></label>
            <textarea name="summary" class="textarea inputbox" rows="5"><?php echo $summary; ?></textarea>
        </p>
        <?php } ?>
        
        <p>

        <label for="textfield"><strong>Nội dung</strong></label>

        <textarea name="content" class="textarea inputbox"><?php echo $content; ?></textarea>

        </p>
        
        <?php if($type != 4 && $type != 2 && $type != 1){ ?>
        <p>

            <label for="smallbox"><strong>Hình đại diện</strong></label>
            <?php if (isset($item) && file_exists('uploads/article/' . $item->folder . '/' . $item->thumb)) {?>
                <img src="uploads/article/<?php echo $item->folder . '/' . $item->thumb; ?>" width="100">
            <?php }?>
            <div id="local">
                <input id="uploader" type="file" name="photo" >
                <img alt="Loading" src="theme_admin/img/loading.gif">
                Uploading...
            </div>
        </p>
        <?php echo form_error('photo'); ?>
        <?php } ?>

        <p>

            <label for="smallbox"><strong>Thứ Tự: </strong></label>
            <input type="text" name="weight" value="<?php echo $weight; ?>" style="width: 30px; text-align: center" class="inputbox">
        </p>

        <p>
            <input type="radio" value="1" name="status" <?php if(isset($item) && $item->status==1){?> checked <?php }else echo 'checked';?>>Hiển thị
            <input type="radio" value="0" name="status" <?php if(isset($item) && $item->status==0){?> checked <?php }?>>Ẩn bài
        </p>

        <input type="submit" value="<?php echo $button; ?>" name="ok" class="btn" />
        <a class="btn" style="color: #fff" href="admin/article/<?php echo $type; ?>">Thoát</a>
    </div><!-- end contentbox -->



</div>

<div class="contentcontainer sml right">

    <div class="headings altheading">
        <h2 class="left">Thuộc tính</h2>
    </div>

    <div class="contentbox">
        
        <?php if(!empty($term)){ ?>
        <div class="list">   
            <?php foreach ($term as $row) { ?>
                <div class="i-li">
                    <label>
                        <input class="check_item" type="checkbox" value="<?php echo $row->id ?>" name="term[]"<?php if(!empty($array_article_term) && in_array($row->id, $array_article_term)) echo ' checked'; ?>>
                    <?php echo $row->name ?>
                    </label>
                        <?php foreach ($this->global_function->show_list_table_where(array("type" => $type, "status" => 1, "parent_id" => $row->id), "term", "term.id,term.name") as $c) { ?>
                            <p>
                                <input class="check_item" type="checkbox" value="<?php echo $c->id ?>" name="term[]"<?php if(!empty($array_article_term) && in_array($c->id, $array_article_term)) echo ' checked'; ?>>
                                <?php echo "|--" . $c->name ?>
                            </p>
                        <?php } ?>
                </div>
            <?php } ?>            
        </div>
        <?php } ?>
        
        
        <?php if($type == 3): ?>
        <div class="list">
            <hr />
            <?php if(!empty($category)){ ?>
            <div class="list">
                <label for="textfield"><strong>Danh mục sản phẩm</strong></label>
                <select name="category_id">
                    <option value="">Chọn danh mục sản phẩm</option>
                    <?php foreach($category as $val){
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <option value="<?php echo $id; ?>"<?php if($category_id == $id) echo ' selected'; ?>><?php echo $name; ?></option>
                    <?php } ?>
                </select>
                <?php echo form_error('category_id'); ?>
            </div>
            <?php } ?>
            <div style="clear:both; height:10px"></div>
        </div>
        
        <div class="list">
            <hr />
            <?php if(!empty($cate)){ ?>
            <div class="list">
                <label for="textfield"><strong>Danh mục show sản phẩm</strong></label>
                <select name="cate_id">
                    <option value="0">Chọn danh mục sản phẩm</option>
                    <?php foreach($cate as $val){
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <option value="<?php echo $id; ?>"<?php if($cate_id == $id) echo ' selected'; ?>><?php echo $name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php } ?>
            <div style="clear:both; height:10px"></div>
        </div>

        <?php if(!empty($product_type)){ ?>
        <div class="list">
            <label for="textfield"><strong>Loại sản phẩm</strong></label>
            <div style="max-height: 250px;overflow: auto;">
                <?php foreach ($product_type as $val) { 
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <div><input type="checkbox" name="product_type[]" value="<?php echo $id; ?>"<?php if(!empty($array_article_product_type) && in_array($id, $array_article_product_type)) echo ' checked'; ?>><?php echo $name; ?></div>
                <?php } ?>
            </div>           
        </div>
        <?php } ?>

        <?php if(!empty($skin_type)){ ?>
        <div class="list">
            <label for="textfield"><strong>Loại da</strong></label>
            <div style="max-height: 250px;overflow: auto;">
                <?php foreach ($skin_type as $val) { 
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <div><input type="checkbox" name="skin_type[]" value="<?php echo $id; ?>"<?php if(!empty($array_article_skin_type) && in_array($id, $array_article_skin_type)) echo ' checked'; ?>><?php echo $name; ?></div>
                <?php } ?>
            </div>           
        </div>
        <?php } ?>

        <?php if(!empty($hair_type)){ ?>
        <div class="list">
            <label for="textfield"><strong>Loại tóc</strong></label>
            <div style="max-height: 250px;overflow: auto;">
                <?php foreach ($hair_type as $val) { 
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <div><input type="checkbox" name="hair_type[]" value="<?php echo $id; ?>"<?php if(!empty($array_article_hair_type) && in_array($id, $array_article_hair_type)) echo ' checked'; ?>><?php echo $name; ?></div>
                <?php } ?>
            </div>           
        </div>
        <?php } ?>

        <?php if(!empty($action)){ ?>
        <div class="list">
            <label for="textfield"><strong>Chức năng</strong></label>
            <div style="max-height: 250px;overflow: auto;">
                <?php foreach ($action as $val) { 
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <div><input type="checkbox" name="action[]" value="<?php echo $id; ?>"<?php if(!empty($array_article_action) && in_array($id, $array_article_action)) echo ' checked'; ?>><?php echo $name; ?></div>
                <?php } ?>
            </div>           
        </div>
        <?php } ?>

        <?php if(!empty($capacity)){ ?>
        <div class="list">
            <label for="textfield"><strong>Dung tích</strong></label>
            <div style="max-height: 250px;overflow: auto;">
                <?php foreach ($capacity as $val) { 
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <div><input type="checkbox" name="capacity[]" value="<?php echo $id; ?>"<?php if(!empty($array_article_capacity) && in_array($id, $array_article_capacity)) echo ' checked'; ?>><?php echo $name; ?></div>
                <?php } ?>
            </div>           
        </div>
        <?php } ?>

        <?php if(!empty($weigh)){ ?>
        <div class="list">
            <label for="textfield"><strong>Trọng lượng</strong></label>
            <div style="max-height: 250px;overflow: auto;">
                <?php foreach ($weigh as $val) { 
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <div><input type="checkbox" name="weigh[]" value="<?php echo $id; ?>"<?php if(!empty($array_article_weigh) && in_array($id, $array_article_weigh)) echo ' checked'; ?>><?php echo $name; ?></div>
                <?php } ?>
            </div>           
        </div>
        <?php } ?>

        <?php if(!empty($pill_number)){ ?>
        <div class="list">
            <label for="textfield"><strong>Số lượng viên</strong></label>
            <div style="max-height: 250px;overflow: auto;">
                <?php foreach ($pill_number as $val) { 
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <div><input type="checkbox" name="pill_number[]" value="<?php echo $id; ?>"<?php if(!empty($array_article_pill_number) && in_array($id, $array_article_pill_number)) echo ' checked'; ?>><?php echo $name; ?></div>
                <?php } ?>
            </div>           
        </div>
        <?php } ?>

        <?php if(!empty($origin)){ ?>
        <div class="list">
            <label for="textfield"><strong>Xuất xứ</strong></label>
            <select name="origin_id">
                <option value="0">Chọn xuất xứ</option>
                <?php foreach($origin as $val){
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <option value="<?php echo $id; ?>"<?php if($origin_id == $id) echo ' selected'; ?>><?php echo $name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        
        <?php if(!empty($brand)){ ?>
        <div class="list">
            <label for="textfield"><strong>Thương hiệu</strong></label>
            <select name="brand_id">
                <option value="">Chọn thương hiệu</option>
                <?php foreach($brand as $val){
                    $id = $val->id;
                    $name = $val->name;
                ?>
                <option value="<?php echo $id; ?>"<?php if($brand_id == $id) echo ' selected'; ?>><?php echo $name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>

        <div class="list">
            <hr />
            <div class="ui-widget">
              <label for="tags"><b>Chọn Tags:</b> </label>
              <input type="text" id="tags" class="inputbox" placeholder="Gõ tên Tag..." style="width:325px">
            </div>
            <div id="append_input_tag" style="padding:10px 0">
                <label for="tags">Tag đã chọn: </label>
                <?php if(isset($list_article_tag) && !empty($list_article_tag)){
                    foreach ($list_article_tag as $key => $value) {
                        $id = $value->id;
                        $name = $value->name;
                ?>
                <div style="float:left;margin-right:10px;padding:5px 7px;border:1px solid #ccc;position:relative"><?php echo $name; ?></li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_tag"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="tags[]" value="<?php echo $id; ?>"></div>
                <?php }} ?>
            </div>   
            <div style="clear:both; height:10px"></div>
        </div>
        <?php endif; ?>
    </div>

</div>
</form>

<script type="text/javascript">

    CKEDITOR.replace('content', {height: 350, width: "98%", resize_enabled: true,
        filebrowserBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Images',
    });

    function up() {
        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();
    }

</script>