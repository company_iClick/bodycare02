<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php echo $breadcrumbs; ?> 
            <div class="box_article_content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <?php echo $this->load->view('front/block/left_tintuc'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9">
                        <div class="title"><h1><?php echo $detail->name; ?></h1></div>
                        <div class="list_article_detail">
                            <?php echo $detail->content; ?>
                            <?php $this->load->view("front/block/social")?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>