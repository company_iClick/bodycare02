<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php echo $breadcrumbs; ?> 
            <div class="box_article_content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <?php echo $this->load->view('front/block/left_article'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="list_article_detail">
                            <?php
                            if (!empty($result)) {
                                foreach ($result as $value) {
                                    $a_name = $value->name;
                                    $a_link = site_url($value->slug);
                                    $a_summary = $this->global_function->catchuoi($value->summary, 200);
                                    $a_picture = base_url('uploads/tin-tuc/' . $value->picture);
                                    $a_picture = base_url('timthumb.php?src=' . $a_picture . '&w=220&h=133&zc=1')
                                    ?>
                            <article class="news_post clearfix">
                                <a href="<?php echo $a_link; ?>">
                                    <img src="<?php echo $a_picture; ?>" alt="<?php echo $a_name; ?>">            </a>
                                <h3><a href="<?php echo $a_link; ?>" title="<?php echo $a_name; ?>"><?php echo $a_name; ?></a></h3>
                                <div class="news_summary"><?php echo $a_summary; ?></div>
                                <a class="view_detail" href="<?php echo $a_link; ?>"><span>Chi tiết</span></a>
                            </article>
                            <?php }}else echo '...nội dung đang cập nhật'; ?>
                            <div class="clear"></div>
                            <?php echo $paging; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>