<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php echo $breadcrumbs; ?> 
            <div class="box_article_content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <?php echo $this->load->view('front/block/left_article'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="list_article_detail">
                            <div class="title"><h1>Vị trí tuyển dụng: <?php echo $detail->name; ?></h1></div>
                            <ul class="recruitment_detail">
                                <li>Nơi làm việc: <span><?php echo $detail->workplace; ?></span></li>
                                <li>Số lượng: <span><?php echo $detail->quantity; ?></span></li>
                                <li>Mức lương: <span><?php echo $detail->salary; ?></span></li>
                                <li>Ngày hết hạn: <span><?php echo date('d/m/Y', strtotime($detail->deadline)); ?></span></li>
                            </ul>
                            <h4>Yêu cầu: </h4>
                            <?php echo $detail->content; ?>
                            <?php $this->load->view("front/block/social")?>
                        </div>
                        <div class="list_article_detail">
                            <?php if(!empty($other_article)){ ?>
                                <div class="title"><h2>Các vị trí tuyển dụng khác</h2></div>
                                <ul class="list-news">
                                <?php foreach ($other_article as $article){ ?>
                                    <li><a href="<?php echo site_url($article->slug); ?>" title="<?php echo $article->name; ?>"><?php echo $article->name; ?></a></li>
                                <?php } ?>
                                </ul>    
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>