<div class="wrapper_full">
    <div class="wrapper_full_1">

        <div id="list_banner_news">
            <div class="list_banner_news">
                <?php if(!empty($news_slide)){ 
                    foreach ($news_slide as $row) {
                        $name = $row->name;
                        $link = site_url($row->slug);
                        $summary = $this->my_lib->cut_string($row->summary, 150);
                        $imgUrl = base_url(_upload_article . $row->folder . '/' . $row->photo);
                        $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=498&h=338&zc=1');
                        $date = date('d', strtotime($row->created_at));
                        $month = date('m', strtotime($row->created_at));
                        echo sprintf('<div class="list_banner_news_left1">
                                        <div class="list_banner_news_left">
                                            <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                                        </div>
                                        <div class="list_banner_news_right">
                                            <div class="wp_title_news1">
                                                <span class="times_news">
                                                    <p>%s</p>
                                                    <p>%s</p>
                                                </span>
                                                <a href="%s" title="%s" class="a_href_name1">%s</a>
                                            </div>
                                            <span class="summary_banner_news">%s</span>
                                            <a href="%s" class="detail_a_1">Chi tiết >></a>
                                        </div>
                                    </div>', $link, $name, $imgUrl, $name, $date, $month, $link, $name, $name, $summary, $link);
                    }}
                ?>
            </div>

        </div>

        <div class="list_banner_news_right_2">            
            <?php if(!empty($news_hot)){ 
                foreach ($news_hot as $row) {
                    $name = $row->name;
                    $link = site_url($row->slug);
                    $imgUrl = base_url(_upload_article . $row->folder . '/' . $row->photo);
                    $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=299&h=161&zc=1');
                    echo sprintf('<div class="child_news_hot">
                                <a href="%s" title=%s>
                                    <img src="%s" alt="%s">
                                </a>
                                <a href="%s" title="%s" class="name_news_a1">%s</a>
                            </div>', $link, $name, $imgUrl, $name, $link, $name, $name);
                }}
            ?>
        </div>


        <div class="list_news_home_cate">


            <div class="list_news_home_cate_left">
                <?php if(!empty($categories)){
                    foreach ($categories as $row) {
                        $name = $row->name;
                        $link = site_url('kien-thuc-lam-dep/' . $row->slug);
                        $article = $row->article;
                        if(count($article) > 0){
                ?>
                    
                <div class="cate_news_1">
                    <div class="title_name_cate_3 color_5">
                        <p><a href="<?php echo $link; ?>"><?php echo $name; ?></a></p>
                        <i class="line_1"></i>
                    </div>
                    <?php
                        $i = 0; 
                        foreach ($article as $row) {
                        $a_name = $row->name;
                        $a_link = site_url($row->slug);
                        $summary = $this->my_lib->cut_string($row->summary, 100);
                        $imgUrl = base_url(_upload_article . $row->folder . '/' . $row->photo);
                        $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=270&h=170&zc=1');
                        $class = (($i + 1) % 3 == 0) ? ' last' : '';
                        echo sprintf('<div class="child_news1%s">
                                        <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                                        <span class="summary_news">
                                            <a href="%s" title="%s">%s</a>
                                            <p>%s</p>
                                             <a href="%s" class="fa fa-long-arrow-right" aria-hidden="true"></a>
                                        </span>
                                    </div>', $class, $a_link, $a_name, $imgUrl, $a_name, $a_link, $a_name, $a_name, $summary, $a_link);
                        $i++;
                    } ?>
                </div>
                <?php }}} ?>
            </div>


            <div class="list_news_home_cate_right"><?php  $this->load->view(BLOCK . 'block_news'); ?></div>
        </div>
    </div>
</div>


<?php  $this->load->view(BLOCK . 'policy'); ?>