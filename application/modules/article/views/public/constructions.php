<?php $this->load->view('front/block/list_energy'); ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="box_article_content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <?php echo $this->load->view('front/block/left_construction'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9">
                        <div class="title"><h1><?php echo $detail->name; ?></h1></div>
                        <div class="list_article_detail">                            
                            <?php echo $detail->content; ?>
                        </div>
                        <?php if(!empty($other_article)){ ?>
                        <div class="title"><h2>Công trình khác</h2></div>
                        <ul class="list-news">
                        <?php foreach ($other_article as $article){ ?>
                            <li><a href="<?php echo site_url($article->slug); ?>" title="<?php echo $article->name; ?>"><?php echo $article->name; ?></a></li>
                        <?php } ?>
                        </ul>    
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>