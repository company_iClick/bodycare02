<div class="wrapper_full">
    <div class="wrapper_full_1">

        <div class="left_detail_news"><?php  $this->load->view(BLOCK . 'block_news'); ?></div>


        <div class="right_detail_news">
            <?php if(!empty($detail->category)){ ?>
            <h2 class="name_cate_news_detail"><a href="<?php echo site_url('kien-thuc-lam-dep/' . $detail_category->slug) ?>" title="<?php echo $detail_category->name; ?>"><?php echo $detail_category->name; ?></a></h2>
            <?php } ?>

            <div class="content_news">
                <h1 class="h1_news_detail"><?php echo $detail->name; ?></h1>
                <?php echo $detail->content; ?>
                <?php if(!empty($tags)){?>
                <div class="tags-node">
                    <label>Tags: </label>
                    <div class="list-tag">
                    <?php $i=0; foreach($tags as $t){
                        $name = $t->name;
                        $link= site_url($t->slug);
                        $join = ($i < count($tags) - 1) ? ',' : '';
                        echo sprintf('<span><a href="%s" title="%s">%s</a>%s</span>', $link, $name, $name, $join)
                    ?>
                    <?php $i++;} ?>
                    </div>
                </div>
                <?php } ?>
            </div>

            <div class="print_back" id="product">
                <span class="icon-quaylai" onclick="goBack()"><p>Quay lại trang trước</p></span>
                <span class="icon-intrangnay" onclick="myFunction()"><p>In trang này</p></span>
            </div>
            <div class="clear"></div>
            <?php if(!empty($list_product)){ ?>
            <div class="cate_arrangement_pr_list clearfix">
                <div class="cate_arrangement_pr_list_text">
                    <p>Sản phẩm đang bán</p>
                    <b></b>
                </div>
                <div class="cate_arrangement_pr_list_select">  
                    <span class="arrangementt_icon square_arrangementt<?php if($layout == '' || $layout == 'grid') echo ' active_arrangementt'; ?>">
                            <i></i>
                    </span>
                    <span class="arrangementt_icon long_arrangementt<?php if($layout == 'list') echo ' active_arrangementt'; ?>">
                        <i></i>
                    </span>
                </div>
            </div>

            <div class="list_pr_ds" id="load_product_list">                 
                <?php 
                foreach($list_product as $pr){
                    $id = $pr->id;
                    $name = $pr->name;
                    $pr_name = $this->my_lib->cut_string($pr->name, 55);
                    $link = site_url($pr->slug);
                    $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                    $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                    $summary = $pr->summary;
                    $class_layout_list = ($layout == 'list') ? ' box_product_v2_list' : '';
                    $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                    $price = cms_price_v1($pr->price_sale, $pr->price);

                    echo sprintf('<div class="box_product_v2%s">
                                    <div class="box_product_v2_img">
                                        <a href="%s" title="%s">
                                            <img src="%s" alt="%s">
                                        </a>
                                    </div>
                                    <div class="box_product_v2_name_element">
                                        <div class="box_product_v2_name">
                                            <a href="%s" title="%s">%s</a>
                                            <a href="%s" title="%s" class="name_layout_list">%s</a>
                                        </div>
                                        <div class="box_product_v2_sum">%s</div>
                                        <div class="box_product_v2_price clearfix">
                                            <div class="box_product_v2_price_num">%s</div>
                                            <div class="box_product_v2_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                        </div>
                                    </div>%s%s
                                </div>', $class_layout_list, $link, $name, $photo, $name, $link, $name, $name, $link, $name, $name, $summary, $price, $id, $percent, $gift_type);
                } 
                ?>

                <div class="clear"></div>
                <?php echo $paging; ?>
                <div class="ajax_loadding"></div>
            </div>
            <?php } ?>
            
            <?php if(!empty($other_news)){
                $i = 0;
            ?>
            <div class="orther_news">
                <div class="orther_title">Các tin khác </div>
                <?php foreach ($other_news as $row) {
                    $a_name = $row->name;
                    $short_name = $this->my_lib->cut_string($row->name, 80);
                    $a_link = site_url($row->slug);
                    $summary = $this->my_lib->cut_string($row->summary, 140);
                    $imgUrl = base_url(_upload_article . $row->folder . '/' . $row->photo);
                    $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=400&h=260&zc=1');
                    $class = (($i+1) % 2 == 0) ? ' child_news_orther1': '';
                    echo sprintf('<div class="child_news_orther%s">
                                    <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                                    <span class="summary_news_orther">
                                        <a href="%s" title="%s">%s</a>
                                        <p>%s</p>
                                    </span>
                                </div>', $class, $a_link, $a_name, $imgUrl, $a_name, $a_link, $a_name, $short_name, $summary);
                    $i++;
                } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
</div>
<?php  $this->load->view('front/block/policy') ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.long_arrangementt').click(function() {
            $('.box_product_v2').addClass('box_product_v2_list');
            $('.square_arrangementt').removeClass('active_arrangementt');
            $('.long_arrangementt').addClass('active_arrangementt');
            $('input[name=layout]').val('list');
        });
        $('.square_arrangementt').click(function() {
            $('.box_product_v2').removeClass('box_product_v2_list');
            $('.long_arrangementt').removeClass('active_arrangementt');
            $('.square_arrangementt').addClass('active_arrangementt');
            $('input[name=layout]').val('grid');
        })
    })
</script>