<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php echo $breadcrumbs; ?> 
            <div class="box_article_content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <?php echo $this->load->view('front/block/left_article'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="list_article_detail">
                            <table class="table table-op">
                                <thead><tr><th>STT</th>
                                        <th width="45%">Vị trí tuyển dụng</th>
                                        <th>Nơi làm việc</th>
                                        <th>Số lượng</th>
                                        <th>Ngày hết hạn</th>
                                    </tr></thead>
                                <tbody>
                                <?php
                                if (!empty($result)) {
                                    $i = 0;
                                    foreach ($result as $value) {
                                        $a_name = $value->name;
                                        $a_link = site_url($value->slug);
                                        $quantity = $value->quantity;
                                        $workplace = $value->workplace;
                                        $deadline = date('d/m/Y', strtotime($value->deadline));                                        
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $i + 1; ?></td>
                                        <td><a style="color:#555" href="<?php echo $a_link; ?>"><?php echo $a_name; ?></a></td>
                                        <td class="text-center"><?php echo $workplace; ?></td>
                                        <td class="text-center"><?php echo $quantity; ?></td>
                                        <td class="text-center"><?php echo $deadline; ?></td>
                                    </tr>
                                <?php $i++;}} ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>