<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php echo $breadcrumbs; ?> 
            <div class="box_office_service clearfix">
                <h1 class="title_office_service">Dịch vụ cho thuê văn phòng</h1>
                <div class="office_about">
                    <?php echo $info->office_service; ?>
                </div>
                <div class="box_info_office clearfix">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-8">
                            <img src="<?php echo _images . 'toanhachipsang.png'; ?>" alt="Dịch vụ cho thuê văn phòng" />
                            <span class="toanhachipsang">Tòa nhà chíp sáng</span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="contact">
                                <h2>Thông tin liên hệ</h2>
                                <ul>
                                    <li>Địa chỉ: <?php echo $info->address; ?></li>
                                    <li>Điện thoại: <?php echo $info->tel; ?></li>
                                    <li>Fax: <?php echo $info->fax; ?></li>
                                    <li>Email: <a href="mailto:<?php echo $info->email; ?>"><?php echo $info->email; ?></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12" style="margin-top:10px"><?php $this->load->view("front/block/social")?></div>
                    </div>      
                </div>
            </div>
        </div>
    </div>
</div>