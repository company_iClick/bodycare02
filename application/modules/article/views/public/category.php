<div class="wrapper_full">
    <div class="wrapper_full_1">
        <div class="list_news_home_cate">
            <div class="list_news_home_cate_left">
                <div class="cate_news_1">
                    <div class="title_name_cate_3 color_5">
                        <p>Kiến thức làm đẹp về <?php echo $detail->name; ?></p>
                        <i class="line_1"></i>
                    </div>
                    <?php
                        $i = 0; 
                        if(!empty($list_article)){
                        foreach ($list_article as $row) {
                        $a_name = $row->name;
                        $a_link = site_url($row->slug);
                        $summary = $this->my_lib->cut_string($row->summary, 100);
                        $imgUrl = base_url(_upload_article . $row->folder . '/' . $row->photo);
                        $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=270&h=170&zc=1');
                        $class = (($i + 1) % 3 == 0) ? ' last' : '';
                        echo sprintf('<div class="child_news1%s">
                                        <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                                        <span class="summary_news">
                                            <a href="%s" title="%s">%s</a>
                                            <p>%s</p>
                                             <a href="%s" class="fa fa-long-arrow-right" aria-hidden="true"></a>
                                        </span>
                                    </div>', $class, $a_link, $a_name, $imgUrl, $a_name, $a_link, $a_name, $a_name, $summary, $a_link);
                        $i++;
                    }} ?>
                    <div class="clear"></div>
                    <?php echo $paging; ?>
                </div>
            </div>

            <div class="list_news_home_cate_right"><?php  $this->load->view(BLOCK . 'block_news'); ?></div>
        </div>
    </div>
</div>

<?php  $this->load->view(BLOCK . 'policy'); ?>