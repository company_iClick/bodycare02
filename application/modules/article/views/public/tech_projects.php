<?php $this->load->view('front/block/list_technology'); ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?php echo $breadcrumbs; ?> 
            <div class="box_article_content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <?php echo $this->load->view('front/block/left_tech_projects'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9">
                        <div class="box_list_news clearfix">                        
                            <div class="row">
                            <?php
                            if (!empty($result)) {
                                foreach ($result as $value) {
                                    $a_name = $value->name;
                                    $a_link = site_url($value->slug);
                                    $a_summary = $this->global_function->catchuoi($value->summary, 200);
                                    $a_picture = base_url('uploads/tin-tuc/' . $value->picture);
                                    $a_picture = base_url('timthumb.php?src=' . $a_picture . '&w=230&h=145&zc=1')
                                    ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="box_news_carousel">
                                        <div class="box_news_carousel_img">
                                            <a href="<?php echo $a_link; ?>">
                                                <img src="<?php echo $a_picture; ?>" alt="<?php echo $a_name; ?>">
                                            </a>
                                        </div>
                                        <div class="box_news_carousel_info clearfix">
                                            <h3><a href="<?php echo $a_link; ?>" title="<?php echo $a_name; ?>"><?php echo $a_name; ?></a></h3>
                                            <p><?php echo $a_summary; ?></p>
                                            <a href="<?php echo $a_link; ?>" class="view_detail_news">Chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            <?php }} ?>
                             <div class="clear"></div>
                            <?php echo $paging; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>