<?php

class m_order_vip extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //================ check itemdetail is db====================
    function show_list_order_vip($limit, $offset) {
        $this->db->select('*');
        $this->db->from('order_vip');
        $this->db->limit($limit, $offset);
        $this->db->order_by("id","DESC");
        return $this->db->get()->result();
    }
    function show_list_order_vip_where($where, $limit, $offset) {
        $this->db->select('*');
        $this->db->from('order_vip');
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by("id","DESC");
        return $this->db->get()->result();
    }
    function count_list_order_vip() {
        $this->db->select('*');
        $this->db->from('order_vip');
        $this->db->where($where);
        return $this->db->get()->num_rows();
    }
    function count_list_order_vip_where($where) {
        $this->db->select('*');
        $this->db->from('order_vip');
        return $this->db->get()->num_rows();
    }
    function Detail($id) {
        $this->db->select('*');
        $this->db->where("id",$id);
        $this->db->from('order_vip');
        return $this->db->get()->row();
    }

    

}
