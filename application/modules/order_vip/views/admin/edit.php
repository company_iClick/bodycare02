<style>
    body{background: none !important}
    .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
    }
    * html .ui-autocomplete {
    height: 100px;
    }
</style>

<link href="<?php echo base_url() ?>theme_admin/styles/layout.css" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url() ?>theme_admin/themes/red/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="<?php echo base_url(); ?>theme_admin/js/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        var items = <?php echo $products?>;

        $( "#item_product" ).autocomplete({
          minLength: 0,
          source: items,
          focus: function( event, ui ) {
            $( "#item_product" ).val( ui.item.label );
            return false;
          },
          select: function( event, ui ) {
            var html = '<label for="tags"><b>Thông tin sản phẩm:</b></label><input type="hidden" name="item_id" value="'+ ui.item.id  +'"><input type="hidden" name="item_link" value="'+ ui.item.link  +'"><input type="hidden" name="item_thumb" value="'+ ui.item.thumb  +'"><label for="tags">Tên sản phẩm: </label><input type="text" class="inputbox" style="width:400px" name="item_name" value="'+ ui.item.value  +'"><label for="tags">Giá: </label><input type="text" class="inputbox" style="width:400px" name="item_price" value="'+ ui.item.price  +'"><label for="tags">Số lượng: </label><input type="text" class="inputbox" style="width:50px" name="item_qty" value="1"><div style="clear:both; height:10px"></div><div style="margin-top:10px;background:green;border:none !important" class="btn" id="add_to_order">Thêm vào đơn hàng</div>';
           $('#append_input').html(html); 
           add_item();
            return false;
          }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li>" )
            .append( "<div>" + item.label + "</div>" )
            .appendTo( ul );
        };
    });

    var list_item_arr  = new Array();
    var list_gift_arr  = new Array();

    <?php if(!empty($order_info)){ 
        foreach($order_info as $i_item){
            $item_info = json_decode($i_item);
    ?>
    list_item_arr.push(<?php echo $item_info->id; ?>);
    <?php }} ?>
    <?php if(!empty($gift_info)){ 
        foreach($gift_info as $i_item){
            $item_info = json_decode($i_item);
    ?>
    list_gift_arr.push(<?php echo $item_info->id; ?>);
    <?php }} ?>
    console.log(list_item_arr);
    console.log(list_gift_arr);

      function add_item(){
       
        $('#add_to_order').click(function(e) {
            var item_id = $('input[name="item_id"]').val();
            var item_name = $('input[name="item_name"]').val();
            var item_link = $('input[name="item_link"]').val();
            var item_price = $('input[name="item_price"]').val();
            var item_thumb = $('input[name="item_thumb"]').val();
            var item_qty = $('input[name="item_qty"]').val();
            var item_money = item_price * item_qty;
        
            var itemObj = {};
            //console.log(jQuery.inArray(item_id, list_item_arr));
            if(jQuery.inArray(parseInt(item_id), list_item_arr) == -1){
                itemObj["id"] = item_id;
                itemObj["product"] = item_name;
                itemObj["link"] = item_link;
                itemObj["price"] = item_price;
                itemObj["images"] = item_thumb;
                itemObj["qty"] = item_qty;
                itemObj["subtotal"] = item_money;

                list_item_arr.push(parseInt(item_id));
                
            } else{

                var item_qty_curent = $('#item_qty_'+ item_id).attr('rel');
                var item_qty = parseInt(item_qty) + parseInt(item_qty_curent);
                var item_money = item_price * item_qty;

                itemObj["id"] = item_id;
                itemObj["product"] = item_name;
                itemObj["link"] = item_link;
                itemObj["price"] = item_price;
                itemObj["images"] = item_thumb;
                itemObj["qty"] = item_qty;
                itemObj["subtotal"] = item_money;

                var item_money_current = $('#item_money_'+ item_id).attr('rel');
                total_order_minus(item_money_current);
                $('#item_'+ item_id).remove();
                $('#item_info_'+ item_id).remove();
            }

            
            var jsonItem = JSON.stringify(itemObj);
            $('#list_item_json').append("<div id='item_"+ item_id +"'><input type='hidden' name='order_info[]' value='"+ jsonItem  +"'></div>");
            var html = '<tr id="item_info_'+ item_id +'"><td align="center"><a href="'+ item_link +'" target="_blank">'+ item_name +'</a></td><td align="center">'+ item_price +'</td><td align="center" id="item_qty_'+ item_id +'" rel="'+ item_qty +'">'+ item_qty +'</td><td align="center" id="item_money_'+ item_id +'" rel="'+ item_money +'">'+ item_money +'</td><td align="center"><a style="color:#f00" href="javascript:void(0)" onclick="delete_item('+ item_id +')">X</a></td></tr>';
            if($('#tpl_list_item tbody').append(html)){
                $('#append_input').html(''); 
                $('#item_product').val('');
                total_order_plus(item_money);
            }
            
        });
}

   

    function delete_item(item_id){
        if(!confirm('Chắc chắn xóa?')) return false;
        var item_money = $('#item_money_'+ item_id).attr('rel');
        total_order_minus(item_money);
        $('#item_'+ item_id).remove();
        $('#item_info_'+ item_id).remove();
    }


    function total_order_plus(item_money){
        var total = $('#total_order input').val();
        var total_calculate = parseInt(total) + parseInt(item_money);
        $('#total_order input').val(total_calculate);
    }

    function total_order_minus(item_money){
        var total = $('#total_order input').val();
        var total_calculate = parseInt(total) - parseInt(item_money);
        $('#total_order input').val(total_calculate);
    }

    function add_new_item(){
        var item_product = $('#item_product').val();
        if(item_product == ''){
            alert('Bạn chưa nhập sản phẩm');
            $('#item_product').focus();
            return false;
        }

        var html = '<label for="tags"><b>Thông tin sản phẩm:</b></label><input type="hidden" name="item_id" value=""><input type="hidden" name="item_link" value=""><input type="hidden" name="item_thumb" value=""><label for="tags">Tên sản phẩm: </label><input type="text" class="inputbox" style="width:400px" name="item_name" value="'+ item_product  +'"><label for="tags">Giá: </label><input type="text" class="inputbox" style="width:400px" name="item_price" value="0"><label for="tags">Số lượng: </label><input type="text" class="inputbox" style="width:50px" name="item_qty" value="1"><div style="clear:both; height:10px"></div><div style="margin-top:10px;background:green;border:none !important" class="btn" id="add_to_order">Thêm vào đơn hàng</div>';
       $('#append_input').html(html); 
       add_item();
    }

    $(document).ready(function($) {
        $('#btn_add_order').click(function() {
            var order_info = $('input[name="order_info"]').val();
            if(order_info == ''){
                alert('Chưa có sản phẩm nào được nhập');
                return false;
            }

            $.ajax({
                url: '<?=base_url()?>admin/order_vip/edit_order',
                type: 'POST',
                data: $('#frm_order_vip').serialize(),
                 beforeSend: function(){
                    $('#btn_add_order').html('Đang gửi dữ liệu...');
                },
                success: function(res) {
                    if(res == 1){
                        alert('Cập nhật đơn hàng thành công.');
                        document.location.reload();
                    }else{
                        alert('Đã xảy ra lỗi.')
                    }
                }            
            });
            return false; 
        });
          
    });

  </script>
<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
?>
<div class="contentcontainer">
    <div class="headings altheading"><h2 style="font-size: 18px">Sửa đơn hàng thành viên <?php echo $user->email ?></h2></div>
    <div class="contentbox">
        <?php echo validation_errors(); ?>
        <form name="them" method="post" id="frm_order_vip" action="" enctype="multipart/form-data">
            <input type="hidden" name="member_id" value="<?php echo $user->id; ?>">
            <input type="hidden" name="product_id" value="<?php echo $item->id; ?>">
        	<div class="ui-widget">
              <label for="tags">Chọn sản phẩm: </label>

              <input type="text" id="item_product" class="inputbox" style="width: 480px">
              <button type="button" style="height: 36px;background: #f00;border: none;outline: none; color: #fff;font-weight: bold;padding: 0 8px;cursor: pointer;" id="btn_select" onclick="add_new_item()">Thêm</button>
            </div>
            <div style="clear:both; height:10px"></div>
            <div id="append_input"></div>  
            <div style="clear:both; height:10px"></div> 
            <div id="list_item">
                <label for="tags"><b>Sản phẩm:</b></label>
                <div id="list_item_json">
                    <?php if(!empty($order_info)){ 
                        foreach($order_info as $i_item){
                            $item_info = json_decode($i_item);
                    ?>
                    <div id="item_<?php echo $item_info->id; ?>"><input type='hidden' name='order_info[]' value='<?php echo $i_item; ?>'></div>
                    <?php }} ?>
                </div>
                <table width="550" cellpadding="0" cellspacing="0" border="1" id="tpl_list_item">
                    <thead>
                        <th width="50%">Tên SP</th>
                        <th>Giá(đ)</th>
                        <th width="7%">Số lượng</th>
                        <th width="15%">Thành tiền(đ)</th>
                        <th>Xóa</th>
                    </thead>
                    <tbody>
                        <?php if(!empty($order_info)){ 
                            foreach($order_info as $i_item){
                                $item_info = json_decode($i_item);
                        ?>
                        <tr id="item_info_<?php echo $item_info->id; ?>">
                            <td align="center">
                                <a href="<?php echo $item_info->link; ?>" target="_blank"><?php echo $item_info->product; ?></a></td>
                            <td align="center"><?php echo $item_info->price; ?></td>
                            <td align="center" id="item_qty_<?php echo $item_info->id; ?>" rel="<?php echo $item_info->qty; ?>"><?php echo $item_info->qty; ?></td>
                            <td align="center" id="item_money_<?php echo $item_info->id; ?>" rel="<?php echo $item_info->subtotal; ?>"><?php echo $item_info->subtotal; ?></td><td align="center"><a style="color:#f00" href="javascript:void(0)" onclick="delete_item(<?php echo $item_info->id; ?>)">X</a></td>
                        </tr>
                        <?php }} ?>
                    </tbody>
                </table>
                <div style="clear:both; height:10px"></div>
                
                <div id="total_order">
                    <label for="tags"><b>Tổng tiền(đ):</b></label>
                    <input type="text" class="inputbox" name="total_order" readonly="true" value="<?php echo $item->total; ?>" style="width:100px">
                </div>
                <div>
                    <label for="tags"><b>Điểm tích lũy:</b></label>
                    <input type="text" name="score" class="inputbox" value="<?php echo $item->score; ?>" style="width:100px">
                </div>
                <div>
                    <label for="tags"><b>Ghi chú:</b></label>
                    <textarea name="note" class="inputbox" rows="5"><?php echo $item->note; ?></textarea>
                </div>
                
            </div>
            <div style="clear:both; height:10px"></div>
            <div style="margin-top:10px;background:red;border:none !important" class="btn" id="btn_add_order">Cập nhật</div>
        </form>
    </div><!-- end contentbox -->
</div>