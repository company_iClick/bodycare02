<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        // Your own constructor code

        $this->load->database();
        $this->load->model(array('m_order_vip', 'general',"product/m_product"));
        $this->template->set_template('admin');

    }
    
   
    public function add($member_id){
        $user = $this->global_function->get_tableWhere(array('id' => $member_id, 'status' => 1), "users", "*");
        if(empty($user)) redirect(base_url('admin'));
        $data['user'] = $user;
        $product_select = $this->m_product->show_list_product_all_select();
        $product_array = array();
        if(!empty($product_select)){
            foreach ($product_select as $key => $value) {

                if ($value->price > 0) {
                    $price = $value->price;
                } else {
                    $price = $value->cost;
                }

                $product_array[] = array(
                        'id' => $value->id,
                        'link' => base_url($value->slug),
                        'label' => $value->label,
                        'value' => $value->value,
                        'thumb' => base_url($value->thumb),
                        'price' => $price
                    );
            }
            $data['products'] = json_encode($product_array);
        }
      

        $this->load->view('admin/add', $data);
    }

    public function add_order(){
        $data = $this->input->post();        
        $user = $this->global_function->get_tableWhere(array('id' => $data['member_id'], 'status' => 1), "users", "*");
        if(empty($user)) redirect(base_url('admin'));
        $order_info = json_encode($data['order_info']);
        $sql['member_id'] = $data['member_id'];
        $sql['code'] = random_string('numeric', 10);
        $sql['order_info'] = $order_info;
        $sql['total'] = $data['total_order'];
        $sql['score'] = $data['score'];
        $sql['note'] = $data['note'];
        $sql['order_status_id'] = 4;
        $sql['status'] = 1;
        $sql['vip'] = 1;
        $sql['user_id'] = $this->session->userdata('admin_login')->id;
        if($this->db->insert('tpl_order', $sql)){
            $score = $user->score + $sql['score'];
            $this->db->where('id', $sql['member_id']);
            $this->db->update('users', array('score' => $score));
            echo 1;
        }else echo 0;
    }

    public function edit($member_id, $id){
        $user = $this->global_function->get_tableWhere(array('id' => $member_id, 'status' => 1), "users", "*");
        $product = $this->global_function->get_tableWhere(array('id' => $id), "tpl_order", "*");
        if(empty($user) || empty($product)) redirect(base_url('admin'));
        $data['user'] = $user;
        $data['item'] = $product;
        $data['order_info'] = json_decode($product->order_info);
        $data['gift_info'] = (!empty($product->gift_info)) ? json_decode($product->gift_info) : '';
            //echo '<pre>'; print_r($data['gift_info']); echo '</pre>';die;
        $product_select = $this->m_product->show_list_product_all_select();
        $product_array = array();
        if(!empty($product_select)){
            foreach ($product_select as $key => $value) {

                if ($value->price > 0) {
                    $price = $value->price;
                } else {
                    $price = $value->cost;
                }

                $product_array[] = array(
                        'id' => $value->id,
                        'link' => base_url($value->slug),
                        'label' => $value->label,
                        'value' => $value->value,
                        'thumb' => base_url($value->thumb),
                        'price' => $price
                    );
            }
            $data['products'] = json_encode($product_array);
        }

        $this->load->view('admin/edit', $data);
    }

    public function edit_order(){
        $data = $this->input->post();        
        $user = $this->global_function->get_tableWhere(array('id' => $data['member_id'], 'status' => 1), "users", "*");
        $product = $this->global_function->get_tableWhere(array('id' => $data['product_id']), "tpl_order", "*");
        if(empty($user)) redirect(base_url('admin'));
        $order_info = json_encode($data['order_info']);
        $sql['member_id'] = $data['member_id'];
        $sql['order_info'] = $order_info;
        $sql['total'] = $data['total_order'];
        $sql['score'] = $data['score'];
        $sql['note'] = $data['note'];
        $sql['order_status_id'] = 4;
        $sql['status'] = 1;
        $sql['vip'] = 1;
        $sql['user_edit_id'] = $this->session->userdata('admin_login')->id;
    
        $this->db->where('id', $product->id);
        if($this->db->update('tpl_order', $sql)){
            $score = $user->score + $sql['score'] - $product->score;                
            $this->db->where('id', $user->id);
            $this->db->update('users', array('score' => $score));
            echo 1;
        }else echo 0;
    }

   
    function delete($member_id, $id, $type = 1){
        $user = $this->global_function->get_tableWhere(array('id' => $member_id, 'status' => 1), "users", "*");
        $product = $this->global_function->get_tableWhere(array('id' => $id), "tpl_order", "*");
        if(empty($user) || empty($product)) redirect(base_url('admin'));
        $this->db->where(array("id"=>$id));
        if($this->db->delete("order")){
            $score = $user->score - $product->score;
            $this->db->where('id', $member_id);
            $this->db->update('users', array('score' => $score));
            if($type == 1){
                redirect(base_url('admin/users/view/' . $member_id . '?messager=success'));
            }else redirect(base_url('admin/users/view/' . $member_id . '?messager=success'));
        }     
    }

}