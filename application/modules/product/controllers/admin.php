<?php
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array(
            "url"
        ));
        $this->load->model(array(
            "product/m_product",
            "category/m_category",
            "comment/m_comment",
            "tags/m_tags",
            "general"
        ));
        $this->template->set_template('admin');
    }
    function index($page_no = 1)
    {

        if (!($this->general->Checkpermission("view_product")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST['show']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(base_url('admin/product/index/' . $page_no) . '?messager=success');
        }
        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->hide_more($a);
            }
            redirect(base_url('admin/product/index/' . $page_no) . '?messager=success');
        }
        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->delete_more($a);
            }
            redirect(base_url('admin/product/index/' . $page_no) . '?messager=success');
        }
        $page_co         = 20;
        $start           = ($page_no - 1) * $page_co;
        $count           = $this->general->count_table_where(array(), 'product');
        $data['page_no'] = $page_no;
        $data['item']    = $this->m_product->show_list_product(array(), $page_co, $start);
        
        //category filter
        $cat             = $this->input->get('cat');
        if (isset($cat) && is_numeric($cat)) {
            $list_cate_id = $this->m_category->get_categroy_tree_id($cat);
            $data['item'] = $this->m_product->show_list_product_category($list_cate_id);
        }

        //webiste filter
        $website             = $this->input->get('website');
        if (isset($website) && is_numeric($website)) {
            $data['item'] = $this->m_product->show_list_product_website($website);
        }

        //product type filter
        $product_type             = $this->input->get('product_type');
        if (isset($product_type) && is_numeric($product_type)) {
            $data['item'] = $this->m_product->show_list_product_product_type($product_type);
        }

        //skin type filter
        $skin_type             = $this->input->get('skin_type');
        if (isset($skin_type) && is_numeric($skin_type)) {
            $data['item'] = $this->m_product->show_list_product_skin_type($skin_type);
        }

        //hair type filter
        $hair_type             = $this->input->get('hair_type');
        if (isset($hair_type) && is_numeric($hair_type)) {
            $data['item'] = $this->m_product->show_list_product_hair_type($hair_type);
        }

        //brand filter
        $brand             = $this->input->get('brand');
        if (isset($brand) && is_numeric($brand)) {
            $data['item'] = $this->m_product->show_list_product_brand($brand);
        }

        //origin filter
        $origin             = $this->input->get('origin');
        if (isset($origin) && is_numeric($origin)) {
            $data['item'] = $this->m_product->show_list_product_origin($origin);
        }

        //action filter
        $action             = $this->input->get('action');
        if (isset($action) && is_numeric($action)) {
            $data['item'] = $this->m_product->show_list_product_action($action);
        }

        //capacity filter
        $capacity             = $this->input->get('capacity');
        if (isset($capacity) && is_numeric($capacity)) {
            $data['item'] = $this->m_product->show_list_product_capacity($capacity);
        }

        //weigh filter
        $weigh             = $this->input->get('weigh');
        if (isset($weigh) && is_numeric($weigh)) {
            $data['item'] = $this->m_product->show_list_product_weigh($weigh);
        }

        //pill_number filter
        $pill_number             = $this->input->get('pill_number');
        if (isset($pill_number) && is_numeric($pill_number)) {
            $data['item'] = $this->m_product->show_list_product_pill_number($pill_number);
        }

        //keyword search
        if($this->input->post('keyword') != ''){
            $keyword = $this->my_lib->changeTitle($this->input->post('keyword'));
            $data['item'] = $this->m_product->show_list_product_keyword($keyword);
            // $data['item'] = $this->m_product->show_list_product_keyword_fulltext($this->input->post('keyword'));
        }

        // status
        $status             = $this->input->get('status');
        if($status != ''){
            $data['item'] = $this->m_product->show_list_product_status($status);
        }



        if (!empty($data['item'])) {
            foreach ($data['item'] as $key => $value) {
                $value->count_comment = $this->m_comment->count_comment_read(array(
                    'product_id' => $value->id
                ));
            }
        }
        $data['link']     = $this->general->paging($page_co, $count, 'admin/product/index/', $page_no); 
        $data['website'] = $this->global_function->get_array_object(array(), 'id, name', 'website_get', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['category'] = $this->m_category->menu_admin(0);
        $data['brand'] = $this->global_function->get_array_object(array(), 'id, name', 'brand', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['product_type'] = $this->global_function->get_array_object(array(), 'id, name', 'product_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['skin_type'] = $this->global_function->get_array_object(array(), 'id, name', 'skin_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['hair_type'] = $this->global_function->get_array_object(array(), 'id, name', 'hair_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['origin'] = $this->global_function->get_array_object(array(), 'id, name', 'origin', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['action'] = $this->global_function->get_array_object(array(), 'id, name', 'action', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['capacity'] = $this->global_function->get_array_object(array(), 'id, name', 'capacity', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['weigh'] = $this->global_function->get_array_object(array(), 'id, name', 'weigh', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['pill_number'] = $this->global_function->get_array_object(array(), 'id, name', 'pill_number', 0, array('field' => 'name', 'sort' => 'ASC'));

        $this->template->write('mod', "product");
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();
    }
    function add()
    {
        $date = date("d-m-Y");
        if (!($this->general->Checkpermission("add_product")))
            redirect(base_url("admin/not-permission"));
        $data               = array();
        $data['breadcrumb'] = '<li>>></li><li><a href="back/product">Danh sách sản phẩm</a></li><li>>></li><li class="current">Thêm mới</li>';
        if ($this->input->post('ok') != '') {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required');
            $this->form_validation->set_rules('slug', 'Đường dẫn', 'is_unique[product.slug]');
            $this->form_validation->set_rules('category[]', "Danh mục", 'required');
            $this->form_validation->set_rules('brand_id', "Thương hiệu", 'required');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql['website_id']    = $this->input->post('website_id');
                $sql['brand_id']    = $this->input->post('brand_id');
                $sql['origin_id']    = $this->input->post('origin_id');
                $sql['weight']      = $this->input->post('weight');
                $sql['name']        = $this->input->post('name');
                $sql['slug']        = ($this->input->post('slug') != '') ? $this->input->post('slug') : $this->my_lib->changeTitle($this->input->post('name'));
                $sql['price']       = $this->input->post('price');
                $sql['price_sale']       = $this->input->post('price_sale');
                $sql['model']       = $this->input->post('model');
                $sql['specifications']       = $this->input->post('specifications');
                $sql['summary']     = $this->input->post('summary');
                $sql['content']     = $this->input->post('content');
                $sql['guide']     = $this->input->post('guide');
                $sql['element']     = $this->input->post('element');
                if($this->input->post('gift_type') == 1 && $this->input->post('product_gift') != ''){
                    $sql['gift_type']     = $this->input->post('gift_type');
                } 
                elseif($this->input->post('gift_type') == 2 && $this->input->post('gift_summary') != ''){
                    $sql['gift_type']     = $this->input->post('gift_type');
                    $sql['gift_summary']     = $this->input->post('gift_summary');
                }else{
                    $sql['gift_type']     = 0;
                }
                    
                $sql['youtube_code']     = $this->input->post('youtube_code');
                $sql['title']       = $this->input->post('title');
                $sql['keywords']    = $this->input->post('keywords');
                $sql['description'] = $this->input->post('description');
                $sql['male']      = ($this->input->post('male') != '') ? $this->input->post('male') : '';;
                $sql['female']      = ($this->input->post('female') != '') ? $this->input->post('female') : '';
                $sql['status']      = $this->input->post('status');
                $sql['folder']      = $date;
                $sql['user_id']      = $this->session->userdata('admin_login')->id;
                if (!is_dir("uploads/product/" . $date)) {
                    mkdir("uploads/product/" . $date, 0777);
                }
            

                $img_select_upload = $this->input->post('img_select_upload');

                $path_parts = pathinfo($img_select_upload);
                $extension = $path_parts['extension'];

                $item_link = $this->my_lib->changeTitle($this->input->post('name'));

                $file = 'multyupload/files/' . $img_select_upload;
                $file_thumb = 'multyupload/files/thumbnail/' . $img_select_upload;
                copy($file, 'uploads/product/'  . $date . '/' . $item_link . '.' . $extension);
                copy($file_thumb, 'uploads/product/'  . $date . '/' . $item_link . '_thumb' . '.' . $extension);
                $sql['photo'] = $item_link . '.' . $extension;
                $sql['thumb'] = $item_link . '_thumb' . '.' . $extension;

                if($this->db->insert('product', $sql)){
                    $id    = $this->db->insert_id();

                    //insert category
                    $postCategory = $this->input->post('category');
                    if(!empty($postCategory)){
                        foreach ($postCategory as $key => $value) {
                            $this->db->insert('tmp_product_category', array('product_id' => $id, 'category_id' => $value));
                        }    
                    }

                    //insert product_type
                    $postProductType = $this->input->post('product_type');
                    if(!empty($postProductType)){
                        foreach ($postProductType as $key => $value) {
                            $this->db->insert('tmp_product_product_type', array('product_id' => $id, 'product_type_id' => $value));
                        }    
                    }

                    //insert skin_type
                    $postSkinType = $this->input->post('skin_type');
                    if(!empty($postSkinType)){
                        foreach ($postSkinType as $key => $value) {
                            $this->db->insert('tmp_product_skin_type', array('product_id' => $id, 'skin_type_id' => $value));
                        }    
                    }

                    //insert hair_type
                    $postHairType = $this->input->post('hair_type');
                    if(!empty($postHairType)){
                        foreach ($postHairType as $key => $value) {
                            $this->db->insert('tmp_product_hair_type', array('product_id' => $id, 'hair_type_id' => $value));
                        }    
                    }

                    //insert action
                    $postAction = $this->input->post('action');
                    if(!empty($postAction)){
                        foreach ($postAction as $key => $value) {
                            $this->db->insert('tmp_product_action', array('product_id' => $id, 'action_id' => $value));
                        }    
                    }

                    //insert capacity
                    $postCapacity = $this->input->post('capacity');
                    if(!empty($postCapacity)){
                        foreach ($postCapacity as $key => $value) {
                            $this->db->insert('tmp_product_capacity', array('product_id' => $id, 'capacity_id' => $value));
                        }    
                    }

                    //insert weigh
                    $postweigh = $this->input->post('weigh');
                    if(!empty($postweigh)){
                        foreach ($postweigh as $key => $value) {
                            $this->db->insert('tmp_product_weigh', array('product_id' => $id, 'weigh_id' => $value));
                        }    
                    }

                    //insert pill_number
                    $postPillNumber = $this->input->post('pill_number');
                    if(!empty($postPillNumber)){
                        foreach ($postPillNumber as $key => $value) {
                            $this->db->insert('tmp_product_pill_number', array('product_id' => $id, 'pill_number_id' => $value));
                        }    
                    }

                    //insert tags
                    if($this->input->post('tags') != '') {
                        $tags = array_unique($this->input->post('tags'));
                        foreach ($tags as $key => $value) {
                            $this->db->insert('tmp_product_tag', array('product_id' => $id, 'tag_id' => $value));
                        }
                    }

                    //insert product_gift
                    if($sql['gift_type'] == 1 && $this->input->post('product_gift') != '') {
                        $product_gift = array_unique($this->input->post('product_gift'));
                        foreach ($product_gift as $key => $value) {
                            $this->db->insert('tmp_product_product_gift', array('product_id' => $id, 'product_gift_id' => $value));
                        }
                    }

                    if($this->input->post('img_other_upload') != ''){
                        $img_other_upload = explode(',', $this->input->post('img_other_upload'));
                        foreach ($img_other_upload as $row) {
                            $path_parts = pathinfo($row);
                            $extension = $path_parts['extension'];
                            $basename = basename($row,'.'.$path_parts['extension']);
                            $file = 'multyupload/files/' . $row;
                            $file_thumb = 'multyupload/files/thumbnail/' . $row;
                            copy($file, 'uploads/product-photo/'  . $row);
                            copy($file_thumb, 'uploads/product-photo/' . $this->my_lib->changeTitle($basename) . '_thumb' . '.' . $extension);
                            $sql_other_img['product_id'] = $id;
                            $sql_other_img['photo'] = $row;
                            $sql_other_img['thumb'] = $this->my_lib->changeTitle($basename) . '_thumb' . '.' . $extension;
                            $this->db->insert('product_photo', $sql_other_img);
                        }

                    }

                    $files = glob('multyupload/files/*', GLOB_BRACE);; // get all file names
                    foreach($files as $file){ // iterate files
                      if(is_file($file))
                        unlink($file); // delete file
                    }

                    $files_thumbnail = glob('multyupload/files/thumbnail/*', GLOB_BRACE);; // get all file names
                    foreach($files_thumbnail as $file){ // iterate files
                      if(is_file($file))
                        unlink($file); // delete file
                    }

                
                }
                
                redirect(base_url('admin/product/index') . '?messager=success');
            }
        }

        $data['category'] = $this->m_category->menu_admin(0);
        $data['website'] = $this->global_function->get_array_object(array(), 'id, name', 'website_get', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['category'] = $this->m_category->menu_admin(0);
        $data['brand'] = $this->global_function->get_array_object(array(), 'id, name', 'brand', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['product_type'] = $this->global_function->get_array_object(array(), 'id, name', 'product_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['skin_type'] = $this->global_function->get_array_object(array(), 'id, name', 'skin_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['hair_type'] = $this->global_function->get_array_object(array(), 'id, name', 'hair_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['origin'] = $this->global_function->get_array_object(array(), 'id, name', 'origin', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['action'] = $this->global_function->get_array_object(array(), 'id, name', 'action', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['capacity'] = $this->global_function->get_array_object(array(), 'id, name', 'capacity', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['weigh'] = $this->global_function->get_array_object(array(), 'id, name', 'weigh', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['pill_number'] = $this->global_function->get_array_object(array(), 'id, name', 'pill_number', 0, array('field' => 'name', 'sort' => 'ASC'));

        $list_product = $this->m_product->show_list_product_array();
        $data['list_product'] = array();
        if(!empty($list_product)) $data['list_product'] = json_encode($list_product);

        $tags = $this->m_tags->show_list_tag_array();
        $data['tags'] = array();
        if(!empty($tags)) $data['tags'] = json_encode($tags);

        $this->template->write('mod', "product");
        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }
    function edit($id)
    {
        $data['item']          = $item = $this->m_product->get_productID($id);
        $data['product_photo'] = $this->global_function->show_list_table_where(array(
            'product_id' => $id,
            'status' => 1
        ), 'product_photo');
        if (!($this->general->Checkpermission("edit_product")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST['ok'])) {
            $folder = $data['item']->folder;
            $this->form_validation->set_rules('name', 'Tên', 'trim|required');
            $this->form_validation->set_rules('slug', 'Đường dẫn', 'callback_slugValidation');
            $this->form_validation->set_rules('category[]', "Danh mục", 'required');
            $this->form_validation->set_rules('brand_id', "Thương hiệu", 'required');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql['website_id']    = $this->input->post('website_id');
                $sql['brand_id']    = $this->input->post('brand_id');
                $sql['origin_id'] = $this->input->post('origin_id');
                $sql['weight']      = $this->input->post('weight');
                $sql['name']        = $this->input->post('name');
                $sql['slug']        = ($this->input->post('slug') != '') ? $this->input->post('slug') : $this->my_lib->changeTitle($this->input->post('name'));
                $sql['price']       = $this->input->post('price');
                $sql['price_sale']       = $this->input->post('price_sale');
                $sql['model']       = $this->input->post('model');
                $sql['specifications']       = $this->input->post('specifications');
                $sql['summary']     = $this->input->post('summary');
                $sql['content']     = $this->input->post('content');
                $sql['guide']     = $this->input->post('guide');
                $sql['element']     = $this->input->post('element');
                if($this->input->post('gift_type') == 1 && $this->input->post('product_gift') != ''){
                    $sql['gift_type']     = $this->input->post('gift_type');
                } 
                elseif($this->input->post('gift_type') == 2 && $this->input->post('gift_summary') != ''){
                    $sql['gift_type']     = $this->input->post('gift_type');
                    $sql['gift_summary']     = $this->input->post('gift_summary');
                }else{
                    $sql['gift_type']     = 0;
                }
                $sql['youtube_code']     = $this->input->post('youtube_code');
                $sql['title']       = $this->input->post('title');
                $sql['keywords']    = $this->input->post('keywords');
                $sql['description'] = $this->input->post('description');
                $sql['male']      = $this->input->post('male');
                $sql['female']      = $this->input->post('female');
                $sql['status']      = $this->input->post('status');
                $sql['folder']      = $folder;
                $sql['user_edit_id']      = $this->session->userdata('admin_login')->id;
                $sql['modified_in'] = date('Y-m-d H:i:s', time());
                if (!is_dir("uploads/product/" . $folder)) {
                    mkdir("uploads/product/" . $folder, 0777);
                }

                $img_select_upload = $this->input->post('img_select_upload');
                
                if($img_select_upload != ''){
                    $path_parts = pathinfo($img_select_upload);
                    $extension = $path_parts['extension'];

                    $item_link = $this->my_lib->changeTitle($this->input->post('name'));

                    $file = 'multyupload/files/' . $img_select_upload;
                    $file_thumb = 'multyupload/files/thumbnail/' . $img_select_upload;

                    rename('uploads/product/'  . $folder . '/' . $item->photo, 'uploads/product-photo/' . $item->photo);

                    rename('uploads/product/'  . $folder . '/' . $item->thumb, 'uploads/product-photo/' . $item->thumb);

                    copy($file, 'uploads/product/'  . $folder . '/' . $item_link . '.' . $extension);
                    copy($file_thumb, 'uploads/product/'  . $folder . '/' . $item_link . '_thumb' . '.' . $extension);

                    $sql_other_img['product_id'] = $id;
                    $sql_other_img['photo'] = $item->photo;
                    $sql_other_img['thumb'] = $item->thumb;
                    $this->db->insert('product_photo', $sql_other_img);
                    
                    $sql['photo'] = $item_link . '.' . $extension;
                    $sql['thumb'] = $item_link . '_thumb' . '.' . $extension;

                    $files = glob('multyupload/files/*', GLOB_BRACE);; // get all file names
                    foreach($files as $file){ // iterate files
                      if(is_file($file))
                        unlink($file); // delete file
                    }

                    $files_thumbnail = glob('multyupload/files/thumbnail/*', GLOB_BRACE);; // get all file names
                    foreach($files_thumbnail as $file){ // iterate files
                      if(is_file($file))
                        unlink($file); // delete file
                    }
                }

                if($this->input->post('img_other_upload') != ''){
                    $img_other_upload = explode(',', $this->input->post('img_other_upload'));
                    foreach ($img_other_upload as $row) {
                        $path_parts = pathinfo($row);
                        $extension = $path_parts['extension'];
                        $basename = basename($row,'.'.$path_parts['extension']);
                        $file = 'multyupload/files/' . $row;
                        $file_thumb = 'multyupload/files/thumbnail/' . $row;
                        copy($file, 'uploads/product-photo/'  . $row);
                        copy($file_thumb, 'uploads/product-photo/' . $this->my_lib->changeTitle($basename) . '_thumb' . '.' . $extension);
                        $sql_other_img['product_id'] = $id;
                        $sql_other_img['photo'] = $row;
                        $sql_other_img['thumb'] = $this->my_lib->changeTitle($basename) . '_thumb' . '.' . $extension;
                        $this->db->insert('product_photo', $sql_other_img);
                    }

                }

                $img_current = $data['item']->photo;
                $select_img_main = $this->input->post('select_img_main');
                if($img_select_upload == '' && $select_img_main != $img_current){
                    $other_img_item = $this->global_function->get_tableWhere(array('photo' => $select_img_main), 'product_photo');

                    $basename_thumb = basename($item->thumb).PHP_EOL;
                    $path_parts = pathinfo($item->thumb);
                    $extension = $path_parts['extension'];
                    $basename = basename($item->thumb,'.'.$path_parts['extension']);

                    rename('uploads/product/'  . $folder . '/' . $item->photo, 'uploads/product-photo/' . $item->photo);

                    rename('uploads/product/'  . $folder . '/' . $item->thumb, 'uploads/product-photo/' . $basename . '.' . $extension);

                    rename('uploads/product-photo/' . $select_img_main, 'uploads/product/'  . $folder . '/' . $select_img_main);

                    rename('uploads/product-photo/' . $other_img_item->thumb, 'uploads/product/'  . $folder . '/' . $other_img_item->thumb);
                    
                    
                    $sql['photo'] = $other_img_item->photo;
                    $sql['thumb'] = $other_img_item->thumb;

                    $sql_other_img['photo'] = $img_current;
                    $sql_other_img['thumb'] = $basename . '.' . $extension;

                    $this->db->where(array('photo' => $select_img_main, 'product_id' => $id));
                    $this->db->update('product_photo', $sql_other_img);
                }

                $this->db->where('id', $id);
                if($this->db->update('product', $sql)){

                    //insert category     
                    $this->db->delete('tmp_product_category', array('product_id' => $id));              
                    $postCategory = $this->input->post('category');
                    if(!empty($postCategory)){  
                        foreach ($postCategory as $key => $value) {
                            $this->db->insert('tmp_product_category', array('product_id' => $id, 'category_id' => $value));
                        }    
                    }

                    //insert product_type   
                    $this->db->delete('tmp_product_product_type', array('product_id' => $id));
                    $postProductType = $this->input->post('product_type');
                    if(!empty($postProductType)){                        
                        foreach ($postProductType as $key => $value) {
                            $this->db->insert('tmp_product_product_type', array('product_id' => $id, 'product_type_id' => $value));
                        }    
                    }

                    //insert skin_type   
                    $this->db->delete('tmp_product_skin_type', array('product_id' => $id));
                    $postSkinType = $this->input->post('skin_type');
                    if(!empty($postSkinType)){
                        foreach ($postSkinType as $key => $value) {
                            $this->db->insert('tmp_product_skin_type', array('product_id' => $id, 'skin_type_id' => $value));
                        }    
                    }

                    //insert hair_type    
                    $this->db->delete('tmp_product_hair_type', array('product_id' => $id)); 
                    $postHairType = $this->input->post('hair_type');
                    if(!empty($postHairType)){
                        foreach ($postHairType as $key => $value) {
                            $this->db->insert('tmp_product_hair_type', array('product_id' => $id, 'hair_type_id' => $value));
                        }    
                    }

                    //insert action
                    $this->db->delete('tmp_product_action', array('product_id' => $id));
                    $postAction = $this->input->post('action');
                    if(!empty($postAction)){
                        foreach ($postAction as $key => $value) {
                            $this->db->insert('tmp_product_action', array('product_id' => $id, 'action_id' => $value));
                        }    
                    }

                    //insert capacity
                    $this->db->delete('tmp_product_capacity', array('product_id' => $id));
                    $postCapacity = $this->input->post('capacity');
                    if(!empty($postCapacity)){
                        foreach ($postCapacity as $key => $value) {
                            $this->db->insert('tmp_product_capacity', array('product_id' => $id, 'capacity_id' => $value));
                        }    
                    }

                    //insert weigh
                    $this->db->delete('tmp_product_weigh', array('product_id' => $id));
                    $postweigh = $this->input->post('weigh');
                    if(!empty($postweigh)){
                        foreach ($postweigh as $key => $value) {
                            $this->db->insert('tmp_product_weigh', array('product_id' => $id, 'weigh_id' => $value));
                        }    
                    }

                    //insert pill_number
                    $this->db->delete('tmp_product_pill_number', array('product_id' => $id));
                    $postPillNumber = $this->input->post('pill_number');
                    if(!empty($postPillNumber)){                       
                        foreach ($postPillNumber as $key => $value) {
                            $this->db->insert('tmp_product_pill_number', array('product_id' => $id, 'pill_number_id' => $value));
                        }    
                    }

                    //insert tags
                    if($this->input->post('tags') != '') {
                        $this->db->delete('tmp_product_tag', array('product_id' => $id));
                        $tags = array_unique($this->input->post('tags'));
                        foreach ($tags as $key => $value) {
                            $this->db->insert('tmp_product_tag', array('product_id' => $id, 'tag_id' => $value));
                        }
                    }

                    //insert product_gift
                    if($sql['gift_type'] == 1) {
                        $this->db->delete('tmp_product_product_gift', array('product_id' => $id));
                        if($this->input->post('product_gift') != ''){
                            $product_gift = array_unique($this->input->post('product_gift'));
                            foreach ($product_gift as $key => $value) {
                                $this->db->insert('tmp_product_product_gift', array('product_id' => $id, 'product_gift_id' => $value));
                            }
                        }                        
                    }

                }
        
                redirect(base_url('admin/product/edit/' . $id) . '?messager=success');
            }
        }

        $this->template->write('mod', "product");
        $data['category'] = $this->m_category->menu_admin(0);
        $data['website'] = $this->global_function->get_array_object(array(), 'id, name', 'website_get', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['category'] = $this->m_category->menu_admin(0);
        $data['brand'] = $this->global_function->get_array_object(array(), 'id, name', 'brand', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['product_type'] = $this->global_function->get_array_object(array(), 'id, name', 'product_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['skin_type'] = $this->global_function->get_array_object(array(), 'id, name', 'skin_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['hair_type'] = $this->global_function->get_array_object(array(), 'id, name', 'hair_type', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['origin'] = $this->global_function->get_array_object(array(), 'id, name', 'origin', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['action'] = $this->global_function->get_array_object(array(), 'id, name', 'action', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['capacity'] = $this->global_function->get_array_object(array(), 'id, name', 'capacity', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['weigh'] = $this->global_function->get_array_object(array(), 'id, name', 'weigh', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['pill_number'] = $this->global_function->get_array_object(array(), 'id, name', 'pill_number', 0, array('field' => 'name', 'sort' => 'ASC'));

        //get tmp_product_category
        $data["list_tmp_product_category"] = $this->m_product->show_list_tmp_product_category_id($id);
        $array_product_category = array();
        if(!empty($data["list_tmp_product_category"])){
            foreach ($data["list_tmp_product_category"] as $key => $value) {
                $array_product_category[] = $value->category_id;
            }
        } 
        $data['array_product_category'] = $array_product_category;

        //get tmp_product_product_type
        $data["list_tmp_product_product_type"] = $this->m_product->show_list_tmp_product_product_type_id($id);
        $array_product_product_type = array();
        if(!empty($data["list_tmp_product_product_type"])){
            foreach ($data["list_tmp_product_product_type"] as $key => $value) {
                $array_product_product_type[] = $value->product_type_id;
            }
        } 
        $data['array_product_product_type'] = $array_product_product_type;

        //get tmp_product_skin_type
        $data["list_tmp_product_skin_type"] = $this->m_product->show_list_tmp_product_skin_type_id($id);
        $array_product_skin_type = array();
        if(!empty($data["list_tmp_product_skin_type"])){
            foreach ($data["list_tmp_product_skin_type"] as $key => $value) {
                $array_product_skin_type[] = $value->skin_type_id;
            }
        } 
        $data['array_product_skin_type'] = $array_product_skin_type;

        //get tmp_product_hair_type
        $data["list_tmp_product_hair_type"] = $this->m_product->show_list_tmp_product_hair_type_id($id);
        $array_product_hair_type = array();
        if(!empty($data["list_tmp_product_hair_type"])){
            foreach ($data["list_tmp_product_hair_type"] as $key => $value) {
                $array_product_hair_type[] = $value->hair_type_id;
            }
        } 
        $data['array_product_hair_type'] = $array_product_hair_type;

        //get tmp_product_action
        $data["list_tmp_product_action"] = $this->m_product->show_list_tmp_product_action_id($id);
        $array_product_action = array();
        if(!empty($data["list_tmp_product_action"])){
            foreach ($data["list_tmp_product_action"] as $key => $value) {
                $array_product_action[] = $value->action_id;
            }
        } 
        $data['array_product_action'] = $array_product_action;

        //get tmp_product_capacity
        $data["list_tmp_product_capacity"] = $this->m_product->show_list_tmp_product_capacity_id($id);
        $array_product_capacity = array();
        if(!empty($data["list_tmp_product_capacity"])){
            foreach ($data["list_tmp_product_capacity"] as $key => $value) {
                $array_product_capacity[] = $value->capacity_id;
            }
        } 
        $data['array_product_capacity'] = $array_product_capacity;

        //get tmp_product_weigh
        $data["list_tmp_product_weigh"] = $this->m_product->show_list_tmp_product_weigh_id($id);
        $array_product_weigh = array();
        if(!empty($data["list_tmp_product_weigh"])){
            foreach ($data["list_tmp_product_weigh"] as $key => $value) {
                $array_product_weigh[] = $value->weigh_id;
            }
        } 
        $data['array_product_weigh'] = $array_product_weigh;

        //get tmp_product_pill_number
        $data["list_tmp_product_pill_number"] = $this->m_product->show_list_tmp_product_pill_number_id($id);
        $array_product_pill_number = array();
        if(!empty($data["list_tmp_product_pill_number"])){
            foreach ($data["list_tmp_product_pill_number"] as $key => $value) {
                $array_product_pill_number[] = $value->pill_number_id;
            }
        } 
        $data['array_product_pill_number'] = $array_product_pill_number;

        $data['list_product_tag'] = $this->m_tags->show_list_tags_in_product($id);
        $tags = $this->m_tags->show_list_tag_array();
        $data['tags'] = array();
        if(!empty($tags)) $data['tags'] = json_encode($tags);

        $list_product = $this->m_product->show_list_product_array();
        $data['list_product'] = array();
        if(!empty($list_product)) $data['list_product'] = json_encode($list_product);

        //get list gift in product
        $data['list_product_gift'] = $this->m_product->show_list_gift_in_product($id);

        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }
    function hide($id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_product")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "product");
        redirect(base_url('admin/product/index/' . $page_no) . '?messager=success');
    }
    function hide_more($id)
    {
        if (!($this->general->Checkpermission("edit_product")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "product");
        return true;
    }
    function show_more($id)
    {
        if (!($this->general->Checkpermission("edit_product")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "product");
        return true;
    }
    function show($id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_product")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "product");
        redirect(base_url('admin/product/index/' . $page_no) . '?messager=success');
    }
    
    function delete($id, $page_no)
    {
        $item   = $this->m_product->get_productID($id);
        $folder = $item->folder;
        if (!($this->general->Checkpermission("delete_product")))
            redirect(base_url("admin/not-permission"));
        if (file_exists('./uploads/product/' . $folder . '/' . $item->photo)) {
            @unlink('./uploads/product/' . $folder . '/' . $item->photo);
        }
        if (file_exists('./uploads/product/' . $folder . '/' . $item->thumb)) {
            @unlink('./uploads/product/' . $folder . '/' . $item->thumb);
        }
        if ($this->db->delete('product', array(
            'id' => $id
        ))) {
            $product_photos = $this->global_function->get_array_object(array(
                'product_id' => $id
            ), 'id, product_id, photo, thumb', 'product_photo');
            if (!empty($product_photos)) {
                foreach ($product_photos as $product_photo) {
                    if (file_exists('uploads/product-photo/' . $product_photo->photo)) {
                        unlink('uploads/product-photo/' . $product_photo->photo);
                    }
                    if (file_exists('uploads/product-photo/' . $product_photo->thumb)) {
                        unlink('uploads/product-photo/' . $product_photo->thumb);
                    }
                    $where = array(
                        'id' => $product_photo->id
                    );
                    $this->db->delete('product_photo', $where);
                }
            }

            $this->db->delete('tmp_product_category', array('product_id' => $id));
            $this->db->delete('tmp_product_product_type', array('product_id' => $id));
            $this->db->delete('tmp_product_capacity', array('product_id' => $id));
            $this->db->delete('tmp_product_weigh', array('product_id' => $id));
            $this->db->delete('tmp_product_action', array('product_id' => $id));
            $this->db->delete('tmp_product_skin_type', array('product_id' => $id));
            $this->db->delete('tmp_product_hair_type', array('product_id' => $id));
            $this->db->delete('tmp_product_pill_number', array('product_id' => $id));
            $this->db->delete('tmp_product_product_gift', array('product_id' => $id));
            $this->db->delete('tmp_product_tag', array('product_id' => $id));
        }
        redirect(base_url('admin/product/index/' . $page_no) . '?messager=success');
    }
    function delete_more($id)
    {
        $item   = $this->m_product->get_productID($id);
        if(!empty($item)){
            $folder = $item->folder;
            if (!($this->general->Checkpermission("delete_product")))
                redirect(base_url("admin/not-permission"));
            if ($this->db->delete('product', array(
                'id' => $id
            ))) {
                if (file_exists('./uploads/product/' . $folder . '/' . $item->photo)) {
                    @unlink('./uploads/product/' . $folder . '/' . $item->photo);
                }
                if (file_exists('./uploads/product/' . $folder . '/' . $item->thumb)) {
                    @unlink('./uploads/product/' . $folder . '/' . $item->thumb);
                }
                $product_photos = $this->global_function->get_array_object(array(
                    'product_id' => $id
                ), 'id, product_id, photo, thumb', 'product_photo');
                if (!empty($product_photos)) {
                    foreach ($product_photos as $product_photo) {
                        if (file_exists('uploads/product-photo/' . $product_photo->photo)) {
                            unlink('uploads/product-photo/' . $product_photo->photo);
                        }
                        if (file_exists('uploads/product-photo/' . $product_photo->thumb)) {
                            unlink('uploads/product-photo/' . $product_photo->thumb);
                        }
                        $where = array(
                            'id' => $product_photo->id
                        );
                        $this->db->delete('product_photo', $where);
                    }
                }

                $this->db->delete('tmp_product_category', array('product_id' => $id));
                $this->db->delete('tmp_product_product_type', array('product_id' => $id));
                $this->db->delete('tmp_product_capacity', array('product_id' => $id));
                $this->db->delete('tmp_product_weigh', array('product_id' => $id));
                $this->db->delete('tmp_product_action', array('product_id' => $id));
                $this->db->delete('tmp_product_skin_type', array('product_id' => $id));
                $this->db->delete('tmp_product_hair_type', array('product_id' => $id));
                $this->db->delete('tmp_product_pill_number', array('product_id' => $id));
                $this->db->delete('tmp_product_product_gift', array('product_id' => $id));
                $this->db->delete('tmp_product_tag', array('product_id' => $id));
            }
            return true;
        }
        
        
    }
    function delete_img($product_id = 0, $id_img = 0)
    {
        $product_photo = $this->global_function->get_tableWhere(array(
            'product_id' => $product_id,
            'status' => 1
        ), 'product_photo');
        if (file_exists('uploads/product-photo/' . $product_photo->photo)) {
            unlink('uploads/product-photo/' . $product_photo->photo);
        }
        if (file_exists('uploads/product-photo/' . $product_photo->thumb)) {
            unlink('uploads/product-photo/' . $product_photo->thumb);
        }
        $where = array(
            'id' => $id_img
        );
        $this->db->delete('product_photo', $where);
        redirect(base_url('admin/product/edit/' . $product_id) . '?messager=success');
    }
    function imageValidation()
    {
        if (($_FILES['photo']['name'] == '')) {
            $this->form_validation->set_message('imageValidation', 'Bạn chưa chọn hình đại diện');
            return FALSE;
        }
        if (!$this->imageCheck($_FILES['photo']['type'])) {
            $this->form_validation->set_message('imageValidation', 'Định dạng file không đúng. Hãy chọn một hình ảnh.');
            return FALSE;
        }
    }
    private function imageCheck($extension)
    {
        $allowedTypes = array(
            'image/gif',
            'image/jpg',
            'image/png',
            'image/bmp',
            'image/jpeg'
        );
        return in_array($extension, $allowedTypes);
    }
    private function set_upload_options()
    {
        $config                  = array();
        $config['upload_path']   = './uploads/product-photo/';
        $config['allowed_types'] = 'gif|jpg|png|bmp|GIF|JPG|PNG|BMP';
        $config['max_size']      = '15000';
        $config['encrypt_name']  = TRUE;
        $config['remove_spaces'] = TRUE;
        $config['overwrite']     = FALSE;
        return $config;
    }

    function slugValidation()
    {
        $slug = $this->input->post('slug');
        $count = $this->m_product->check_slug($slug);
        if($count > 0){
            $this->form_validation->set_message('slugValidation', 'Đường dẫn đã tồn tại trong hệ thống');
            return FALSE;
        }else return TRUE;
    }
}

