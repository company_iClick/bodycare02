<?php

class Product extends MX_Controller {

    function __construct() {

        parent::__construct();

        $this->load->database();
        $this->load->model(array("product/a_product", "category/a_category", "product_type/a_product_type", "skin_type/a_skin_type", "hair_type/a_hair_type", "brand/a_brand", "origin/a_origin", "action/a_action", "capacity/a_capacity", "weigh/a_weigh","pill_number/a_pill_number","comment/a_comment"));
        $this->load->helper(array('cms_price_helper', 'percen_calculate_helper'));
    }


    function product_filter(){
        
        $page_co = 40;
        $data = array();
        if($this->input->post() != ''){
            $mod = $this->input->post('mod');
            $mod_id = $this->input->post('mod_id');
            $sort = $this->input->post('sort');
            $page_no = $this->input->post('page');
            $start = ($page_no - 1) * $page_co;

            $input_brand = $this->input->post('input_brand');
            $data['layout'] = $this->input->post('layout');

            $input_product_type = '';
            if($this->input->post('input_product_type') != '')
                $input_product_type = (is_array($this->input->post('input_product_type'))) ? $this->input->post('input_product_type') : explode(',', $this->input->post('input_product_type'));

            $input_skin_type = '';
            if($this->input->post('input_skin_type') != '')
                $input_skin_type = (is_array($this->input->post('input_skin_type'))) ? $this->input->post('input_skin_type') : explode(',', $this->input->post('input_skin_type'));

            $input_hair_type = '';
            if($this->input->post('input_hair_type') != '')
                $input_hair_type = (is_array($this->input->post('input_hair_type'))) ? $this->input->post('input_hair_type') : explode(',', $this->input->post('input_hair_type'));

            $input_action = '';
            if($this->input->post('input_action') != '')
                $input_action = (is_array($this->input->post('input_action'))) ? $this->input->post('input_action') : explode(',', $this->input->post('input_action'));

            $input_origin = '';
            if($this->input->post('input_origin') != '')
                $input_origin = (is_array($this->input->post('input_origin'))) ? $this->input->post('input_origin') : explode(',', $this->input->post('input_origin'));

            $input_capacity = '';
            if($this->input->post('input_capacity') != '')
                $input_capacity = (is_array($this->input->post('input_capacity'))) ? $this->input->post('input_capacity') : explode(',', $this->input->post('input_capacity'));

            $input_weigh = '';
            if($this->input->post('input_weigh') != '')
                $input_weigh = (is_array($this->input->post('input_weigh'))) ? $this->input->post('input_weigh') : explode(',', $this->input->post('input_weigh'));

            $input_pill_number = '';
            if($this->input->post('input_pill_number') != '')
                $input_pill_number = (is_array($this->input->post('input_pill_number'))) ? $this->input->post('input_pill_number') : explode(',', $this->input->post('input_pill_number'));

            $input_price = ($this->input->post('input_price') != '') ? $this->input->post('input_price') : '';
            $arr_price = array();
            if($input_price != ''){
                $arr_price = $this->a_product->get_list_price($input_price);
            } 

            switch ($mod) {
                case 'category':
                    $url = 'product-filter';
                    $list_cate_id = $this->a_category->get_categroy_tree_id($mod_id);
                    $count = $this->a_product->count_product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    
                
                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                            break;
                    }

                    break;

                case 'promotion':
                    $count = $this->a_product->count_product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                    switch ($sort) {
                    
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;
                        }

                     break;

                case 'male':

                    $count = $this->a_product->count_product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('male' => 1));

                    switch ($sort) {

                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('male' => 1), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('male' => 1), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('male' => 1));
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('male' => 1), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('male' => 1));
                            break;
                        }

                    break;

                case 'female':
                    $count = $this->a_product->count_product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('female' => 1));

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('female' => 1), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('female' => 1), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('female' => 1));
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('female' => 1), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('female' => 1));
                            break;
                        }

                    break;

                case 'search':
                    $cat = $this->input->post('cate_id_search');
                    $keyword = $this->input->post('keyword');
                    $list_cate_id = array();
                    if($cat != 0)$list_cate_id = $this->a_category->get_categroy_tree_id($cat);

                    $count = $this->a_product->count_product_filter_param_search($keyword, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param_search($keyword,  $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale_search($keyword, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                            break;
                        }

                    break;
                
                default:
                    $count = $this->a_product->count_product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                    switch ($sort) {
                        
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                            break;
                    }

                    break;
            }

            $data['paging'] = $this->global_function->paging_ajax($page_co, $count, $page_no);
            $data['list_product'] = $list_product;
        }

        $this->load->view('public/product_filter', $data);
    }

    function product_filter_select(){        
                
        $data['price_select'] = $this->input->post('price_select');
        $data['brand_select'] = $this->input->post('brand_select');
        $data['product_type_select'] = $this->input->post('product_type_select');
        $data['skin_type_select'] = $this->input->post('skin_type_select');
        $data['hair_type_select'] = $this->input->post('hair_type_select');
        $data['action_select'] = $this->input->post('action_select');
        $data['origin_select'] = $this->input->post('origin_select');
        $data['capacity_select'] = $this->input->post('capacity_select');
        $data['weigh_select'] = $this->input->post('weigh_select');
        $data['pill_number_select'] = $this->input->post('pill_number_select');   

        $this->load->view('public/product_filter_select', $data);
    }

    function category($slug = '', $page_no = 1) { 
        $category = $this->global_function->get_row_object(array('slug' => $slug), 'id, parent_id, name, slug, title, keywords, description, banner, summary, level', 'category');
        
        $data['title_bar'] = $category->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $category->name .'</a>';

        $data['category'] = $category;
        $data['big_summary'] = $category->summary;
        $data['list_article'] = $this->global_function->get_array_object(array('category_id' => $category->id, 'type' => 3), 'id, name, slug, summary, photo, folder, created_at', 'article', 9);

        //get all child id in category  
        $data['mod_id'] = $category->id;  
        $list_cate_id = $this->a_category->get_categroy_tree_id($category->id);
        $data['child'] = $this->a_category->get_category($category->id);

        if($category->level == 2)
            $data['child'] = $this->global_function->get_array_object(array('id !=' => $category->id, 'parent_id' => $category->parent_id), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                if($ch->level == 2) $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
 
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_price($list_cate_id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_category($list_cate_id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_product_type_in_category($list_cate_id);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_category($list_cate_id);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list hair type
        $data['hair_type'] = $this->a_hair_type->get_hair_type_in_category($list_cate_id);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_hair_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_origin_in_category($list_cate_id);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_action_in_category($list_cate_id);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_capacity_in_category($list_cate_id);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_weigh_in_category($list_cate_id);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $row->count_product = $this->a_weigh->count_product($row->id, $list_cate_id);
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_category($list_cate_id);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $row->count_product = $this->a_pill_number->count_product($row->id, $list_cate_id);
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_category($list_cate_id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_category($list_cate_id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }


        $title = (!empty($category->title)) ? $category->title : $category->name;

        $this->template->write('mod', 'category');
        $this->template->write('title', $title);
        $this->template->write('keywords', $category->keywords);
        $this->template->write('description', $category->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function brand($slug = '', $page_no = 1) { 
        
        $data['title_bar'] = 'Thương hiệu';
        $data['breadcrumb'] = '<a href="'. current_url() .'">Thương hiệu</a>';

        $title = 'Thương hiệu';
        $data['list_brand'] = $this->global_function->get_array_object(array(), 'id, name, slug, photo', 'brand');
        $data['product_type'] = $this->global_function->get_array_object(array(), 'id, name, slug', 'product_type');
        $data['categories'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug', 'category');

        $this->template->write('mod', 'brand');
        $this->template->write('title', $title);
        $this->template->write_view('content', 'public/brand', $data, TRUE);
        $this->template->render();
    }

    function brand_character_(){
        $product_type_id = $this->input->post('product_type_id');
        $char = $this->input->post('char');
        $data = array();
        if($product_type_id != '' && $char != ''){
           $data['list_brand'] = $this->a_brand->get_brand_in_product_type_width_character($product_type_id, $char); 
        }
        elseif($product_type_id != '' && $char == ''){
           $data['list_brand'] = $this->a_brand->get_brand_in_product_type($product_type_id); 
        } 
        elseif($product_type_id == '' && $char != ''){
           $data['list_brand'] = $this->a_brand->get_brand_width_character($char); 
        }else{
            $data['list_brand'] = $this->global_function->get_array_object(array(), 'id, name, slug, photo', 'brand');
        }
        $this->load->view('public/brand_character', $data);
    }

    function brand_character(){
        $category_id = $this->input->post('category_id');
        if($category_id != ''){
            $category = $this->global_function->get_row_object(array('id' => $category_id), 'id, name, slug', 'category');
            $list_cate_id = $this->a_category->get_categroy_tree_id($category_id);
        }
        $char = $this->input->post('char');
        $data = array();
        if($category_id != '' && $char != ''){
            $data['category'] = $category;
            $data['list_brand'] = $this->a_brand->get_brand_in_category_width_character($list_cate_id, $char); 
        }
        elseif($category_id != '' && $char == ''){
            $data['category'] = $category;
            $data['list_brand'] = $this->a_brand->get_brand_in_category($list_cate_id); 
        } 
        elseif($category_id == '' && $char != ''){
           $data['list_brand'] = $this->a_brand->get_brand_width_character($char); 
        }else{
            $data['list_brand'] = $this->global_function->get_array_object(array(), 'id, name, slug, photo', 'brand');
        }

        $this->load->view('public/brand_character', $data);
    }

    function brand_detail($slug = '') { 
        $brand = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, photo, summary', 'brand');
        
        $data['title_bar'] = $brand->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $brand->name .'</a>';

        $id = $brand->id;
        $data['brand'] = $brand;

        $categories = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, title_show', 'category');

        if(!empty($categories)){
            foreach ($categories as $row) {
                $cate_id = $row->id;
                $list_cate_id = $this->a_category->get_categroy_tree_id($cate_id);
                $row->product = $this->a_product->get_product_in_category_brand($list_cate_id, $id);
            }
        }
        $data['categories'] = $categories;

        $data['category'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, title_show', 'category');
        if(!empty($data['category'])){
            foreach ($data['category'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
        

        $data['count_brand'] = 0;
        $data['list_brand'] = $this->a_brand->get_all_item();
        
        $title = (!empty($brand->title)) ? $brand->title : $brand->name;

        $this->template->write('mod', 'brand_detail');
        $this->template->write('title', $title);
        $this->template->write('keywords', $brand->keywords);
        $this->template->write('description', $brand->description);
        $this->template->write_view('content', 'public/brand_detail', $data, TRUE);
        $this->template->render();
    }

    function brand_detail_category($slug = '', $slug_category = '') { 
        $brand = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, photo, summary', 'brand');
        
        $data['title_bar'] = $brand->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $brand->name .'</a>';

        $id = $brand->id;
        $data['brand'] = $brand;
        $data['category_brand'] = $this->global_function->get_row_object(array('slug' => $slug_category), 'id, name, slug, title_show', 'category');
        if(empty($data['category_brand'])) redirect(site_url());
     
        $cate_id = $data['category_brand']->id;
        $list_cate_id = $this->a_category->get_categroy_tree_id($cate_id);
        $data['category_brand']->product = $this->a_product->get_product_in_category_brand($list_cate_id, $id);
        $categories = $this->global_function->get_array_object(array('parent_id' => 0, 'id !=' => $data['category_brand']->id), 'id, name, slug, title_show', 'category');

        if(!empty($categories)){
            foreach ($categories as $row) {
                $cate_id = $row->id;
                $list_cate_id = $this->a_category->get_categroy_tree_id($cate_id);
                $row->product = $this->a_product->get_product_in_category_brand($list_cate_id, $id);
            }
        }
        $data['categories'] = $categories;

        $data['category'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, title_show', 'category');
        $data['list_brand'] = $this->a_brand->get_all_item();

        $title = (!empty($brand->title)) ? $brand->title : $brand->name;

        $this->template->write('mod', 'brand_detail');
        $this->template->write('title', $title);
        $this->template->write('keywords', $brand->keywords);
        $this->template->write('description', $brand->description);
        $this->template->write_view('content', 'public/brand_detail', $data, TRUE);
        $this->template->render();
    }

    function product_type($slug = '', $page_no = 1) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'product_type');

        $id = $detail->id;
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_product_type_price($id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_product_type($id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list skin type
        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_product_type($id);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list hair type
        $data['hair_type'] = $this->a_hair_type->get_hair_type_in_product_type($id);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_hair_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_origin_in_product_type($id);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_action_in_product_type($id);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_capacity_in_product_type($id);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_weigh_in_product_type($id);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_product_type($id);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $data['other_product_type'] = $this->a_product_type->get_all_item(array('product_type.id !=' => $id));
        $data['count_other_product_type'] = 0;
        if(!empty($data['other_product_type'])){
            foreach ($data['other_product_type'] as $row) {
                $data['count_other_product_type'] += $row->count_product;
            }
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_product_type($id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_product_type($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_product_type($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_product_type_sale($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_product_type($id, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_product_type($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }


        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'product_type');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function skin_type($slug = '', $page_no = 1) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'skin_type');

        $id = $detail->id;
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_skin_type_price($id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_skin_type($id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_product_type_in_skin_type($id);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['hair_type'] = $this->a_skin_type->get_skin_type_in_hair_type($id);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_origin_in_skin_type($id);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_action_in_skin_type($id);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_capacity_in_skin_type($id);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_weigh_in_skin_type($id);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_skin_type($id);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $data['other_skin_type'] = $this->a_skin_type->get_all_item(array('skin_type.id !=' => $id));
        $data['count_other_skin_type'] = 0;
        if(!empty($data['other_skin_type'])){
            foreach ($data['other_skin_type'] as $row) {
                $data['count_other_skin_type'] += $row->count_product;
            }
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_skin_type($id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_skin_type($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_skin_type($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_skin_type_sale($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_skin_type($id, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_skin_type($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'skin_type');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function hair_type($slug = '', $page_no = 1) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'hair_type');

        $id = $detail->id;
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_hair_type_price($id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_hair_type($id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_product_type_in_hair_type($id);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_hair_type($id);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_origin_in_hair_type($id);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_action_in_hair_type($id);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_capacity_in_hair_type($id);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_weigh_in_hair_type($id);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_hair_type($id);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $data['other_hair_type'] = $this->a_hair_type->get_all_item(array('hair_type.id !=' => $id));
        $data['count_other_hair_type'] = 0;
        if(!empty($data['other_hair_type'])){
            foreach ($data['other_hair_type'] as $row) {
                $data['count_other_hair_type'] += $row->count_product;
            }
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_hair_type($id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_hair_type($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_hair_type($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_hair_type_sale($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_hair_type($id, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_hair_type($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'hair_type');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function action($slug = '', $page_no = 1) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'action');

        $id = $detail->id;
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_action_price($id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_action($id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_product_type_in_action($id);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['hair_type'] = $this->a_action->get_action_in_hair_type($id);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_origin_in_action($id);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin_type
        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_action($id);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_capacity_in_action($id);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_weigh_in_action($id);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_action($id);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $data['other_action'] = $this->a_action->get_all_item(array('action.id !=' => $id));
        $data['count_other_action'] = 0;
        if(!empty($data['other_action'])){
            foreach ($data['other_action'] as $row) {
                $data['count_other_action'] += $row->count_product;
            }
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_action($id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_action($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_action($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_action_sale($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_action($id, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_action($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function origin($slug = '', $page_no = 1) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'origin');

        $id = $detail->id;
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_origin_price($id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_origin($id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_product_type_in_origin($id);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['hair_type'] = $this->a_origin->get_origin_in_hair_type($id);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin_type
        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_origin($id);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_capacity_in_origin($id);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_weigh_in_origin($id);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_origin($id);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $data['other_origin'] = $this->a_origin->get_all_item(array('origin.id !=' => $id));
        $data['count_other_origin'] = 0;
        if(!empty($data['other_origin'])){
            foreach ($data['other_origin'] as $row) {
                $data['count_other_origin'] += $row->count_product;
            }
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_origin($id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_origin($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_origin($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_origin_sale($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_origin($id, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_origin($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'origin');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function capacity($slug = '', $page_no = 1) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'capacity');

        $id = $detail->id;
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_capacity_price($id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_capacity($id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_product_type_in_capacity($id);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['hair_type'] = $this->a_capacity->get_capacity_in_hair_type($id);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_hair_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_origin_in_capacity($id);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin_type
        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_capacity($id);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list action
        $data['action'] = $this->a_action->get_action_in_capacity($id);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_weigh_in_capacity($id);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_capacity($id);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $data['other_capacity'] = $this->a_capacity->get_all_item(array('capacity.id !=' => $id));
        $data['count_other_capacity'] = 0;
        if(!empty($data['other_capacity'])){
            foreach ($data['other_capacity'] as $row) {
                $data['count_other_capacity'] += $row->count_product;
            }
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_capacity($id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_capacity($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_capacity($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_capacity_sale($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_capacity($id, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_capacity($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'capacity');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function weigh($slug = '', $page_no = 1) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'weigh');

        $id = $detail->id;
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_weigh_price($id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_weigh($id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_product_type_in_weigh($id);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['hair_type'] = $this->a_weigh->get_weigh_in_hair_type($id);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_origin_in_weigh($id);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin_type
        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_weigh($id);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list action
        $data['action'] = $this->a_action->get_action_in_weigh($id);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list capacity
        $data['capacity'] = $this->a_capacity->get_capacity_in_weigh($id);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_weigh($id);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $data['other_weigh'] = $this->a_weigh->get_all_item(array('weigh.id !=' => $id));
        $data['count_other_weigh'] = 0;
        if(!empty($data['other_weigh'])){
            foreach ($data['other_weigh'] as $row) {
                $data['count_other_weigh'] += $row->count_product;
            }
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_weigh($id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_weigh($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_weigh($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_weigh_sale($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_weigh($id, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_weigh($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'weigh');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function pill_number($slug = '', $page_no = 1) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'pill_number');

        $id = $detail->id;
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_pill_number_price($id, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_brand_in_pill_number($id);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_product_type_in_pill_number($id);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['hair_type'] = $this->a_pill_number->get_pill_number_in_hair_type($id);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_hair_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_origin_in_pill_number($id);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin_type
        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_pill_number($id);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
        
        //list action
        $data['action'] = $this->a_action->get_action_in_pill_number($id);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
             $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list capacity
        $data['capacity'] = $this->a_capacity->get_capacity_in_pill_number($id);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_weigh_in_pill_number($id);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $data['other_pill_number'] = $this->a_pill_number->get_all_item(array('pill_number.id !=' => $id));
        $data['count_other_pill_number'] = 0;
        if(!empty($data['other_pill_number'])){
            foreach ($data['other_pill_number'] as $row) {
                $data['count_other_pill_number'] += $row->count_product;
            }
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_in_pill_number($id);
        switch ($sort) {
            case 'price-asc':
                $data['list_product'] = $this->a_product->get_product_in_pill_number($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $data['list_product'] = $this->a_product->get_product_in_pill_number($id, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_in_pill_number_sale($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                break;

            case 'qua-tang':
                $data['list_product'] = $this->a_product->get_product_in_pill_number($id, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_in_pill_number($id, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($slug), site_url($slug . '/price-asc'), site_url($slug . '/price-desc'), site_url($slug . '/khuyen-mai'), site_url($slug . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($slug . '/' . $page_no), site_url($slug . '/price-asc/' . $page_no), site_url($slug . '/price-desc/' . $page_no), site_url($slug . '/khuyen-mai/' . $page_no), site_url($slug . '/qua-tang/' . $page_no))
            );
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'pill_number');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function promotion($page_no = 1) { 
        
        $data['breadcrumb'] = '<a href="'. current_url() .'">Khuyến mãi</a>';

        $data['mod_id'] = 0;

        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_about_price($from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_all_item();
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_all_item(array('parent_id' => 0));
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['skin_type'] = $this->a_skin_type->get_all_item();
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list hair type
        $data['hair_type'] = $this->a_hair_type->get_all_item();
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_hair_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_all_item();
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_all_item();
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list capacity
        $data['capacity'] = $this->a_capacity->get_all_item();
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_all_item();
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_all_item();
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $url = 'khuyen-mai';
        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_where();
        switch ($sort) {
            case 'price-asc':
                $order_by = array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'ASC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array(), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $order_by = array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array(), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/price-desc/", $page_no);
                break;

            case 'qua-tang':
                $order_by = array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'gift_type', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array(), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/qua-tang/", $page_no);
                break;
            
            default:
                $order_by = array(array('field' => 'promotion', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array(), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Có quà tặng'),
            'link' => array(site_url($url), site_url($url . '/price-asc'), site_url($url . '/price-desc'), site_url($url . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Có quà tặng'),
                'link' => array(site_url($url . '/' . $page_no), site_url($url . '/price-asc/' . $page_no), site_url($url . '/price-desc/' . $page_no), site_url($url . '/qua-tang/' . $page_no))
            );
        }


        $data['title_bar'] = 'Khuyến mãi';
        $title = 'Khuyến mãi';

        $this->template->write('mod', 'promotion');
        $this->template->write('title', $title);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function male($page_no = 1) { 
        
        $data['breadcrumb'] = '<a href="'. current_url() .'">Dành cho nam</a>';

        $data['mod_id'] = 0;

        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_about_price($from, $to, array('male' => 1));
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_all_item(array('product.male' => 1));
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_all_item(array('parent_id' => 0, 'product.male' => 1));
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['skin_type'] = $this->a_skin_type->get_all_item(array('product.male' => 1));
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list hair type
        $data['hair_type'] = $this->a_hair_type->get_all_item(array('product.male' => 1));
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_hair_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_all_item(array('product.male' => 1));
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_all_item(array('product.male' => 1));
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list capacity
        $data['capacity'] = $this->a_capacity->get_all_item(array('product.male' => 1));
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_all_item(array('product.male' => 1));
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_all_item(array('product.male' => 1));
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $url = 'danh-cho-nam';
        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_where(array('male' => 1));
        switch ($sort) {
            case 'price-asc':
                $order_by = array(array('field' => 'price', 'sort' => 'ASC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array('male' => 1), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $order_by = array(array('field' => 'price', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array('male' => 1), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_sale_where($page_co, $start, array('male' => 1));
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/qua-tang/", $page_no);
                break;

            case 'qua-tang':
                $order_by = array(array('field' => 'gift_type', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array('male' => 1), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array('male' => 1));
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($url), site_url($url . '/price-asc'), site_url($url . '/price-desc'), site_url($url . '/khuyen-mai'), site_url($url . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($url . '/' . $page_no), site_url($url . '/price-asc/' . $page_no), site_url($url . '/price-desc/' . $page_no), site_url($url . '/khuyen-mai/' . $page_no), site_url($url . '/qua-tang/' . $page_no))
            );
        }


        $data['title_bar'] = 'Dành cho nam';
        $title = 'Dành cho nam';

        $this->template->write('mod', 'male');
        $this->template->write('title', $title);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function female($page_no = 1) { 
        
        $data['breadcrumb'] = '<a href="'. current_url() .'">Dành cho nữ</a>';

        $data['mod_id'] = 0;

        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_in_about_price($from, $to, array('female' => 1));
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_all_item(array('product.female' => 1));
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_all_item(array('parent_id' => 0, 'product.female' => 1));
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['skin_type'] = $this->a_skin_type->get_all_item(array('product.female' => 1));
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list hair type
        $data['hair_type'] = $this->a_hair_type->get_all_item(array('product.female' => 1));
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_hair_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_all_item(array('product.female' => 1));
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_all_item(array('product.female' => 1));
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list capacity
        $data['capacity'] = $this->a_capacity->get_all_item(array('product.female' => 1));
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_all_item(array('product.female' => 1));
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_all_item(array('product.female' => 1));
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list product
        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $url = 'danh-cho-nu';
        $sort = $data['sort'] = $this->uri->segment(2);
        $count = $this->a_product->count_product_where(array('female' => 1));
        switch ($sort) {
            case 'price-asc':
                $order_by = array(array('field' => 'price', 'sort' => 'ASC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array('female' => 1), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $order_by = array(array('field' => 'price', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array('female' => 1), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_sale_where($page_co, $start, array('female' => 1));
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/qua-tang/", $page_no);
                break;

            case 'qua-tang':
                $order_by = array(array('field' => 'gift_type', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array('female' => 1), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->get_product_where($page_co, $start, array('female' => 1));
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($url), site_url($url . '/price-asc'), site_url($url . '/price-desc'), site_url($url . '/khuyen-mai'), site_url($url . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($url . '/' . $page_no), site_url($url . '/price-asc/' . $page_no), site_url($url . '/price-desc/' . $page_no), site_url($url . '/khuyen-mai/' . $page_no), site_url($url . '/qua-tang/' . $page_no))
            );
        }


        $data['title_bar'] = 'Dành cho nữ';
        $title = 'Dành cho nữ';

        $this->template->write('mod', 'female');
        $this->template->write('title', $title);
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }

    function detail($slug = ''){
        $detail = $data['detail'] = $this->global_function->get_row_object(array('slug' => $slug), '*', 'product');
        $id = $detail->id;

        $views = $detail->views + 1;
        $this->db->where('id', $id);
        $this->db->update('product', array('views' => $views));

        $category = $this->a_product->get_category_info($id);
        $category_parent = $this->a_category->get_parent_category($category->parent_id);
        $list_cate_id = $this->a_category->get_categroy_tree_id($category->id);

        $data['breadcrumb'] = '<a href="'. site_url($category->slug) .'">'. $category->name .'</a>';

        $data['brand'] = $this->global_function->get_row_object(array('id' => $detail->brand_id), 'name, slug, summary', 'brand');
        $data['origin'] = $this->global_function->get_row_object(array('id' => $detail->origin_id), 'name, slug', 'origin');

        $data['product_type'] = $this->a_product_type->get_product_type_in_detail($id);

        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_detail($id);
        $data['hair_type'] = $this->a_hair_type->get_hair_type_in_detail($id);
        $data['action'] = $this->a_action->get_action_in_detail($id);
        $data['capacity'] = $this->a_capacity->get_capacity_in_detail($id);
        $data['weigh'] = $this->a_weigh->get_weigh_in_detail($id);
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_detail($id);

        $data['product_photos'] = $this->global_function->get_array_object(array('product_id' => $id), 'id, product_id, photo, thumb', 'product_photo');

        $data['other_product'] = $this->a_product->get_product_in_category($list_cate_id, 20, 0);
        $data['care_product'] = $this->a_product-> get_product_in_category_where(array('product.id !=' => $id), $list_cate_id, 3, 0, array('field' => 'product.id', 'sort' => 'RANDOM'));

        $data['company'] = $this->global_function->get_row_object(array('id' => 1), 'phone, email', 'company');

        $data['category_frm'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name', 'category');
        $data['skin_type_frm'] = $this->global_function->get_array_object(array(), 'id, name', 'skin_type');

        $list_comment = $this->a_comment->all_comment_product($id);
        $array_comment = array();
        if(!empty($list_comment)){
            foreach ($list_comment as $row) {
                $id = $row['id'];
                $parent_id = $row['parent_id'];
                if($parent_id == 0) $array_comment[$id]['parent'] = $row;
                else $array_comment[$parent_id]['child'][] = $row;
                
            }
        }
        $data['array_comment'] = $array_comment;

        $data['session_user'] = ($this->session->userdata("user_log")) ? $this->session->userdata("user") : '';
        //echo '<pre>'; print_r($array_comment); echo '</pre>';

        $this->template->write('mod', 'product_detail');
        $title = (!empty($detail->title)) ? $detail->title : $detail->name;
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/detail', $data, TRUE);
        $this->template->render();
    }
    

    function detail_abc() {
        
        $data['title'] = 'ssss';
        $this->template->write('mod', 'detail');
        $this->template->write('title', "Chi tiet");
        $this->template->write('keywords', 'Chi tiet');
        $this->template->write('description', 'Chi tiet');
        $this->template->write_view('content', 'public/detail_abc', $data, TRUE);
        $this->template->render();
    }

    function cart1() {
        
            $data['title'] = 'ssss';
        $this->template->write('mod', 'detail');
        $this->template->write('title', "Chi tiet");
        $this->template->write('keywords', 'Chi tiet');
        $this->template->write('description', 'Chi tiet');
        $this->template->write_view('content', 'public/cart1', $data, TRUE);
        $this->template->render();
    }
    function cart2() {
        
        $data['title'] = 'ssss';
        $this->template->write('mod', 'detail');
        $this->template->write('title', "Chi tiet");
        $this->template->write('keywords', 'Chi tiet');
        $this->template->write('description', 'Chi tiet');
        $this->template->write_view('content', 'public/detail_cart', $data, TRUE);
        $this->template->render();
    }

    function s_search(){
        $data = $this->input->post();
        $this->session->set_userdata('keyword', $data['keyword']);

        $keyword = $this->my_lib->changeTitle($data['keyword']);
        redirect(site_url('search/cat/' . $data['s_cate']. '/q/' . $keyword));
    }
    
    function search($cat = 0, $keyword = '', $page_no = 1) {
        $data['breadcrumb'] = '<a href="'. current_url() .'">Tìm kiếm</a>';
        $data['mod_id'] = 0;
        $list_cate_id = array();
        if($cat != 0)$list_cate_id = $this->a_category->get_categroy_tree_id($cat);

        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_search_in_about_price($list_cate_id, $keyword, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_all_item_search($list_cate_id, $keyword);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['product_type'] = $this->a_product_type->get_all_item_search($list_cate_id, $keyword);
        $data['count_product_type'] = 0;
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $data['count_product_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list skin type
        $data['skin_type'] = $this->a_skin_type->get_all_item_search($list_cate_id, $keyword);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $data['count_skin_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list hair type
        $data['hair_type'] = $this->a_hair_type->get_all_item_search($list_cate_id, $keyword);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $data['count_hair_type'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list origin
        $data['origin'] = $this->a_origin->get_all_item_search($list_cate_id, $keyword);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_all_item_search($list_cate_id, $keyword);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $data['count_action'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list capacity
        $data['capacity'] = $this->a_capacity->get_all_item_search($list_cate_id, $keyword);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $data['count_capacity'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_all_item_search($list_cate_id, $keyword);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $data['count_weigh'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_all_item_search($list_cate_id, $keyword);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $data['count_pill_number'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        $page_co = 40;
        $start = ($page_no - 1) * $page_co;

        $url = "search/cat/" . $cat . "/q/" . $keyword;
        $sort = $data['sort'] = $this->uri->segment(6);
        $count = $this->a_product->count_search($list_cate_id, $keyword);
        switch ($sort) {
            case 'price-asc':
                $order_by = array(array('field' => 'price', 'sort' => 'ASC'));
                $data['list_product'] = $this->a_product->search($list_cate_id, $keyword, $page_co, $start, array(), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/price-asc/", $page_no);
                break;

            case 'price-desc':
                $order_by = array(array('field' => 'price', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->search($list_cate_id, $keyword, $page_co, $start, array(), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/price-desc/", $page_no);
                break;

            case 'khuyen-mai':
                $data['list_product'] = $this->a_product->get_product_search_sale_where($list_cate_id, $keyword, $page_co, $start, array());
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/qua-tang/", $page_no);
                break;

            case 'qua-tang':
                $order_by = array(array('field' => 'gift_type', 'sort' => 'DESC'));
                $data['list_product'] = $this->a_product->search($list_cate_id, $keyword, $page_co, $start, array(), $order_by);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/qua-tang/", $page_no);
                break;
            
            default:
                $data['list_product'] = $this->a_product->search($list_cate_id, $keyword, $page_co, $start);
                $data['paging'] = $this->global_function->paging($page_co, $count, $url."/", $page_no);
                break;
        }

        $data['page_no'] = $page_no;
        $data['link_select'] = array(
            'sort' => array('','price-asc', 'price-desc', 'khuyen-mai', 'qua-tang'), 
            'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
            'link' => array(site_url($url), site_url($url . '/price-asc'), site_url($url . '/price-desc'), site_url($url . '/khuyen-mai'), site_url($url . '/qua-tang'))  
        );
        if($page_no > 1){
            $data['link_select'] = array(
                'sort' => array('', 'price-asc', 'price-desc', 'qua-tang'), 
                'name' => array('Tất cả', 'Giá từ thấp đến cao', 'Giá từ cao đến thấp', 'Khuyến mãi', 'Có quà tặng'),
                'link' => array(site_url($url . '/' . $page_no), site_url($url . '/price-asc/' . $page_no), site_url($url . '/price-desc/' . $page_no), site_url($url . '/khuyen-mai/' . $page_no), site_url($url . '/qua-tang/' . $page_no))
            );
        }


        $data['keyword'] = $keyword;
        $data['cat'] = $cat;
        $data['title_bar'] = 'Kết quả tìm kiếm';
        
        $this->template->write('mod', 'search');
        $this->template->write('title', 'Tìm kiếm sản phẩm');
        $this->template->write_view('content', 'public/category', $data, TRUE);
        $this->template->render();
    }
    
    /* -------------------GIO HANG ----------------- */
    
    function cart() {   
        if(count($this->cart->contents()) == 0) redirect(site_url());           
        $data['title_bar'] = 'Giỏ hàng';
        $data['breadcrumb'] = '<a href="'. current_url() .'">Giỏ hàng</a>';

        $this->template->write('mod', 'cart');
        $this->template->write('title', 'Giỏ hàng');
        $this->template->write_view('content', 'public/cart', $data, TRUE);
        $this->template->render();
    }
    
    function checkout() {
        if(count($this->cart->contents()) == 0) redirect(site_url());   

        $data['breadcrumb'] = '<a href="'. current_url() .'">Hoàn tất đơn hàng</a>';

        $session_user = $this->session->userdata("user");
        $session_login = $data['session_login'] = $this->session->userdata("user_log");
        $member_id = ($session_login) ? $session_user->id : 0;

        if(!($session_login)) $this->session->set_userdata('url_redirect', current_url());

        $data['company'] = $this->global_function->get_row_object(array('id' => 1), 'phone, email', 'company');
        
        $data['full_name'] = ($session_login) ? $session_user->full_name : $this->input->post('full_name');
        $data['email'] = ($session_login) ? $session_user->email : $this->input->post('email');        
        $data['address'] = ($session_login) ? $session_user->address : $this->input->post('address');
        $data['phone'] = ($session_login) ? $session_user->phone : $this->input->post('phone');
        $data['note'] = $this->input->post('note');
        $data['province_id'] = ($session_login) ? $session_user->province_id : $this->input->post('province_id');
        $data['district_id'] = ($session_login) ? $session_user->district_id : $this->input->post('district_id');

        $data['province'] = $this->global_function->get_province();
        $data['district'] = $this->global_function->get_district($data['province_id']);
        
        $this->template->write('mod', 'checkout');
        $this->template->write('title', 'Hoàn tất đơn hàng');
        $this->template->write_view('content', 'public/checkout', $data, TRUE);
        $this->template->render();
    }
    
    function checkout_success($order_code){
        $order = $this->global_function->get_row_object(array('code' => $order_code), '*', 'tpl_order');
        if(empty($order)) redirect(site_url());
        $data['breadcrumb'] = '<a href="'. current_url() .'">Đặt hàng thành công</a>';
        $data['company'] = $this->global_function->get_row_object(array('id' => 1), 'phone, email', 'company');
        $data['order_code'] = $order_code;
        
        $this->template->write('mod', 'checkout_success');
        $this->template->write('title', 'Đặt hàng thành công');
        $this->template->write_view('content', 'public/checkout_success', $data, TRUE);
        $this->template->render();
    }

    function load_district(){
        $province_id = $this->input->post('province_id');
        $district = $this->global_function->get_district($province_id);
        $html = '<option value="">Chon quận huyện</option>';
        if(!empty($district)){            
            foreach ($district as $row) {
                $html .= '<option value="'. $row->id .'">'. $row->type . ' ' . $row->name .'</option>';
            }
        }
        echo $html;
    }

    public function add_to_cart(){
        $id = $this->input->post('id');
        $product = $this->global_function->get_row_object(array('id' => $id), 'id, name, slug, price, price_sale, thumb, folder', 'product');
        if(empty($product)) redirect(site_url());
        
        $name = $product->slug;
        $link = site_url($product->slug);
        if($product->price > 0 && $product->price_sale > 0){
            $price = $product->price_sale;
        }
        if($product->price > 0 && $product->price_sale == 0){
            $price = $product->price;
        }
        if($product->price == 0){
            $price = $product->price;
        }
        $qty    = ($this->input->post('qty') != '') ? $this->input->post('qty') : 1;
        $images = base_url(_upload_product . $product->folder . '/' . $product->thumb);

        $flag = false;
        foreach ($this->cart->contents() as $row) {
            if ($row['id'] == $id) {
                $rid = $row['rowid'];
                $qty = $row['qty'] + $qty;
                $flag = true;
                break;
            }
        }

        if (!$flag) {
            $data = array(
                "id" => $id,
                "name" => $name ,
                "qty" => $qty,
                "price" => $price,
                "link" => $link,
                "images" => $images
            );
            $this->cart->insert($data);
            echo "ok";
        } else {
            $data = array(
                'rowid' => $rid,
                'qty' => $qty
            );
            $this->cart->update($data);
            echo "ok";
        }
    }

    function addcart() {
        $id = $this->input->post('id');

        $gia = "123";
        $sl = $this->input->post('number');
        $fag = false;
        foreach ($this->cart->contents() as $row) {
            if ($row['id'] == $id) {

                $rid = $row['rowid'];
                $qty = $row['qty'] + $sl;
                $fag = true;
                break;
            }
        }
        if (!$fag) {
            //echo "b";die;
            $data = array(
                "id" => $id,
                "name" => "sp1",
                "qty" => $sl,
                "price" => $gia,
            );
            $this->cart->insert($data);
            echo "insert";
        } else {
            $data = array(
                'rowid' => $rid,
                'qty' => $qty
            );
            $this->cart->update($data);
            echo "up";
        }
    }

    //====================== Update====================
    function update_cart() {
        $rowid = $this->input->post('rowid');
        $qty = $this->input->post('qty');
        $data = array(
            'rowid' => $rowid,
            'qty' => $qty
        );
        $this->cart->update($data);
    }

    //==================== Remove  ======================
    function remove_cart() {
        $rowid = $this->input->post('rowid');
        $data = array(
            'rowid' => $rowid,
            'qty' => 0
        );
        $this->cart->update($data);
    }

    //============ Remove All============
    function remove_all_cart() {
        $this->cart->destroy();
        redirect(site_url('gio-hang'));
    }

    public function _validate_phone() {
        $str = trim($this->input->post('phone'));
        if (!is_numeric($str)){
                $this->form_validation->set_message('_validate_phone', 'Số di động không đúng định dạng.');
                return false;
        } 
        $str_check = str_split(''.$str.'');
        if ($str_check[0]==0 || ($str_check[1]==1 && strlen($str)==11) || ($str_check[1]==9 && strlen($str)==10)){
                $this->form_validation->set_message('_validate_phone', '');
                return true;
        } else{
                $this->form_validation->set_message('_validate_phone', 'Số di động không đúng định dạng.');
                return false;
        }
    }

    public function _validate_province() {
        $province = trim($this->input->post('province_id'));
        if ($province == ''){
            $this->form_validation->set_message('_validate_province', 'Bạn chưa chọn Tỉnh/Thành.');
            return false;
        }   
    }

    public function _validate_district() {
        $district = trim($this->input->post('district_id'));
        if ($district == ''){
            $this->form_validation->set_message('_validate_district', 'Bạn chưa chọn Quận/Huyện.');
            return false;
        }   
    }

    function order_checkout(){

        if($this->input->post() != ''){
            $session_user = $this->session->userdata("user");
            $session_login = $this->session->userdata("user_log");
            $member_id = ($session_login) ? $session_user->id : 0;
            $total = $this->cart->total();
            if(count($this->cart->contents()) > 0){
                foreach ($this->cart->contents() as $row) {
                    $id = $row['id'];
                    $product = $this->global_function->get_row_object(array('id' => $id), 'name', 'product');
                    $photo = $row['images'];
                    $price = $row['price'];
                    
                    $array[] = array(
                        'id' => $id,
                        'product' => $product->name,
                        'images' => $row['images'],
                        'link' => $row['link'],
                        'price' => $price,
                        'qty' => $row['qty'],
                        'subtotal' => $row['subtotal'],
                    ); 

               }
                $order_info = json_encode($array);
            }  

            $sql['full_name'] = strip_tags($this->input->post('full_name'));
            $sql['email'] = $this->input->post('email');
            $sql['address'] = strip_tags($this->input->post('address'));
            $sql['phone'] = $this->input->post('phone');
            $sql['note'] = strip_tags($this->input->post('note'));
            $sql['order_status_id'] = 1;
            $sql['member_id'] = $member_id;
            $sql['province_id'] = $this->input->post('province_id');
            $sql['district_id'] = $this->input->post('district_id');  
            $sql['total'] = $total;  
            $sql['order_info'] = $order_info;
            $sql['code'] = random_string('numeric', 10);
            $sql['ip_address'] = $this->session->userdata('ip_address');
            $sql['user_agent'] = $this->session->userdata('user_agent');
            $sql['website'] = base_url();
            
            $this->db->insert("tpl_order", $sql);

            $district = $this->global_function->get_row_object(array('id' => $sql['district_id']), 'name', 'district');

            $district_name = !(empty($district)) ? $district->name : '';

            $province = $this->global_function->get_row_object(array('id' => $sql['province_id']), 'name', 'province');

            $province_name = !(empty($province)) ? $province->name : '';
          
            $body = '<table>';
            $body .= '<tr><th colspan="2">&nbsp;</th></tr><tr><th colspan="2">Đơn hàng '. $sql['code'] .' từ website '.site_url().'</th></tr><tr><th colspan="2">&nbsp;</th></tr><tr><th>Họ tên :</th><td>'.$sql['full_name'].'</td></tr><tr><th>Địa chỉ :</th><td>'.$sql['address'].'</td></tr><tr><th>Quận huyện :</th><td>'.$district_name.'</td></tr><tr><tr><th>Tỉnh thành :</th><td>'.$province_name.'</td></tr><tr><th>Điện thoại :</th><td>'.$sql['phone'].'</td></tr><tr><th>Email :</th><td>'.$sql['email'].'</td></tr><tr><th>Ghi chú :</th><td>'.$sql['note'].'</td></tr>';
            $body .= '</table>';

            $body .= '<table cellpadding="6" cellspacing="1" style="width:100%; margin:10px 0; padding:10px 0" border="0"><tr><th colspan="2" style="width:42%;border-bottom:1px solid #ccc;line-height:30px">Sản phẩm</th><th style="text-align:center;border-bottom:1px solid #ccc;">Số lượng</th><th style="text-align:center;;border-bottom:1px solid #ccc;">Đơn giá</th><th style="text-align:center;border-bottom:1px solid #ccc;">Thành tiền</th></tr>';

                foreach ($this->cart->contents() as $row){
                    $id = $row['id'];
                    $product = $this->global_function->get_row_object(array('id' => $id), 'name', 'product');
                    $photo = $row['images'];
                    $price = $row['price'];
                    $link = $row['link'];
                    $subtotal = $row['subtotal'];

                    $body .='<tr><td width="12%" style="text-align:center;border-bottom:1px solid #ccc;padding:10px 0; border-left:1px solid #ccc">';


                    $body .= '<p><strong><img src='. $photo .' width="60" /></p><br>';

                    $body .= '</td><td style="text-align:center;border-bottom:1px solid #ccc;padding:10px 0; font-size:13px; text-transform: uppercase;"><a href="'. $link .'">'.$product->name.'</a></td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;">'.$row['qty'].'</td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;">'.number_format($price,0,',','.').' VNĐ'.'</td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;border-right:1px solid #ccc">'.number_format($subtotal,0,',','.').' VNĐ'.'</td></tr>';
                }
                $body .= '<tr><td colspan="5" class="right" style="padding:10px 0;font-size:14px;"><strong>Tổng tiền: </strong>'.number_format($this->cart->total(),0,',','.').' VNĐ'.'</td></tr>';
            $body .='</table>';

          
            $this->cart->destroy();

            $data['mailto'] = 'dangthinh@iclick.vn';
            $data['mailcc'] = 'hientt@iclick.vn';
            $data['code'] = $sql['code'];
            $data['title'] = 'Đơn hàng '. $sql['code'] .' từ website ' . site_url();
            $data['content'] = $body;

            echo json_encode($data);
        }   
    }


    function call_back() {
        $product_id = $this->input->post('id');
        $phone = $this->input->post('phone');
        $link = $this->input->post('link');

        $data['product_id'] = $product_id;
        $data['phone'] = $phone;
        $data['link'] = $link;
        $data['weight'] = 1;
        $data['status'] = 1;

        $body = '<table>';
        $body .= '<tr><th colspan="2">Yêu cầu gọi lại từ số điện thoại '. $data['phone'] .' của website ' . site_url().'</th></tr><th colspan="2">&nbsp;</th></tr><tr><tr><th>Link sản phẩm :</th><td><a href="'.$data['link'].'">'.$data['link'].'</a></td></tr><tr><th>Click để gọi lại :</th><td><a href="tel:'.$data['phone'].'">'.$data['phone'].'</a></td></tr>';
        $body .= '</table>';

        $result = array();
        $result['mailto'] = 'dangthinh@iclick.vn';
        $result['mailcc'] = 'hientt@iclick.vn';
        $result['title'] = 'Yêu cầu gọi lại từ số điện thoại '. $data['phone'] .' của website ' . site_url();
        $result['content'] = $body;

        if ($this->db->insert('call_back', $data)) {
            $result['message'] = true;
        } else
            $result['message'] = false;

        echo json_encode($result);
    }

}
