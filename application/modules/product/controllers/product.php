<?php

class Product extends MX_Controller {

    function __construct() {

        parent::__construct();

        $this->load->database();
        $this->load->model(array("product/a_product", "category/a_category", "product_type/a_product_type", "skin_type/a_skin_type", "hair_type/a_hair_type", "brand/a_brand", "origin/a_origin", "action/a_action", "capacity/a_capacity", "weigh/a_weigh","pill_number/a_pill_number","comment/a_comment"));
        $this->load->helper(array('cms_price_helper', 'percen_calculate_helper', 'cms_price_v1_helper', 'percen_calculate_v1_helper'));
        $this->load->helper('string');
        $this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
    }


    function product_filter(){

        $page_co = 40;
        $data = array();
        if($this->input->post()){
            $mod = $data['mod'] = $this->input->post('mod');
            $mod_id = $data['mod_id'] = $this->input->post('mod_id');
            $sort = $this->input->post('sort');
            $page_no = $this->input->post('page');
            $cat = $this->input->post('cate_id_search');
            $start = ($page_no - 1) * $page_co;

            $input_brand = $this->input->post('input_brand');
            $data['layout'] = $this->input->post('layout');
            $data['filter_parameter'] = $this->input->post('url_push');

            $input_product_type = '';
            if($this->input->post('input_product_type') != '')
                $input_product_type = (is_array($this->input->post('input_product_type'))) ? $this->input->post('input_product_type') : explode(',', $this->input->post('input_product_type'));

            $input_skin_type = '';
            if($this->input->post('input_skin_type') != '')
                $input_skin_type = (is_array($this->input->post('input_skin_type'))) ? $this->input->post('input_skin_type') : explode(',', $this->input->post('input_skin_type'));

            $input_hair_type = '';
            if($this->input->post('input_hair_type') != '')
                $input_hair_type = (is_array($this->input->post('input_hair_type'))) ? $this->input->post('input_hair_type') : explode(',', $this->input->post('input_hair_type'));

            $input_action = '';
            if($this->input->post('input_action') != '')
                $input_action = (is_array($this->input->post('input_action'))) ? $this->input->post('input_action') : explode(',', $this->input->post('input_action'));

            $input_origin = '';
            if($this->input->post('input_origin') != '')
                $input_origin = (is_array($this->input->post('input_origin'))) ? $this->input->post('input_origin') : explode(',', $this->input->post('input_origin'));

            $input_capacity = '';
            if($this->input->post('input_capacity') != '')
                $input_capacity = (is_array($this->input->post('input_capacity'))) ? $this->input->post('input_capacity') : explode(',', $this->input->post('input_capacity'));

            $input_weigh = '';
            if($this->input->post('input_weigh') != '')
                $input_weigh = (is_array($this->input->post('input_weigh'))) ? $this->input->post('input_weigh') : explode(',', $this->input->post('input_weigh'));

            $input_pill_number = '';
            if($this->input->post('input_pill_number') != '')
                $input_pill_number = (is_array($this->input->post('input_pill_number'))) ? $this->input->post('input_pill_number') : explode(',', $this->input->post('input_pill_number'));

            $input_price = ($this->input->post('input_price') != '') ? $this->input->post('input_price') : '';
            $arr_price = array();
            if($input_price != ''){
                $arr_price = $this->a_product->get_list_price($input_price);
            } 

            $view_template = 'product_filter';
            switch ($mod) {
                case 'category':
                    $url = 'product-filter';
                    
                    $category = $this->global_function->get_row_object(array('id' => $mod_id), 'id, parent_id, name, slug, level', 'category');

                   
                    $data['category_child'] = $this->global_function->get_array_object(array('parent_id' => $category->id), 'id, name, slug, level', 'category');                  

                    if(!empty($data['category_child'])){
                        $view_template = 'product_filter_category';
                        $item_page = ($category->level == 0) ? 24: 16;
                        $page_no_ct = 1;
                        $start_ct = ($page_no_ct - 1) * $item_page;
                        foreach ($data['category_child'] as $ct) {
                            $ct_id = $ct->id;
                            $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);

                            $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                            switch ($sort) {
                                case 'price-asc':
                                    $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                                    break;

                                case 'price-desc':
                                    $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                                    break;

                                case 'khuyen-mai':
                                    $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct);
                                    break;

                                case 'qua-tang':
                                    $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                    break;
                                
                                default:
                                    $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct);
                                    break;
                            }

                            $ct->product =  $list_product;

                            $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                        }
                   
                    }else{
                        $list_cate_id = $this->a_category->get_categroy_tree_id($mod_id);
                        $count = $this->a_product->count_product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price); 

                        switch ($sort) {
                            case 'price-asc':
                                $list_product = $this->a_product->product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                                break;

                            case 'price-desc':
                                $list_product = $this->a_product->product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                                break;

                            case 'khuyen-mai':
                                $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                                break;

                            case 'qua-tang':
                                $list_product = $this->a_product->product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                break;
                            
                            default:
                                $list_product = $this->a_product->product_filter_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                                break;
                        }

                        $data['paging'] = $this->global_function->paging_ajax($page_co, $count, $page_no);
                        $data['list_product'] = $list_product;
                    }
                    break;

                case 'promotion':
                    $view_template = 'product_filter_list';
                    $data['list_product_category'] = array();
                    $list_category = $this->a_category->get_category_in_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                    if(!empty($list_category)){
                        $array_node = '';
                        foreach ($list_category as $row) {
                            $array_node[] = $this->a_category->get_node_category_id($row->id);
                        }
                        $array_node = array_unique($array_node);
                        $array_node = array_values($array_node);

                        if(!empty($array_node)){
                            $mod_filter = 'category';
                            switch ($mod) {
                                case 'origin':
                                    $input_origin = $mod_id;
                                    break;

                                case 'product_type':
                                    $input_product_type = $mod_id;
                                    break;

                                case 'skin_type':
                                    $input_skin_type = $mod_id;
                                    break;

                                case 'hair_type':
                                    $input_hair_type = $mod_id;
                                    break;

                                case 'action':
                                    $input_action = $mod_id;
                                    break;

                                case 'capacity':
                                    $input_capacity = $mod_id;
                                    break;

                                case 'weigh':
                                    $input_weigh = $mod_id;
                                    break;

                                case 'pill_number':
                                    $input_pill_number = $mod_id;
                                    break;
                            }

                            $item_page = 8;
                            $array_cate = array();
                            foreach ($array_node as $ct) {
                                $element = explode(',', $ct);
                                $ct_id = $element[0];
                                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                
                                $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                                switch ($sort) {
                                    case 'price-asc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'ASC')));
                                        break;

                                    case 'price-desc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'DESC')));
                                        break;

                                    case 'khuyen-mai':
                                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC')));
                                        break;

                                    case 'qua-tang':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'gift_type', 'sort' => 'DESC')));
                                        break;
                                    
                                    default:
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC')));
                                        break;
                                }

                                $array_cate[$ct]['product'] = $list_product;
                                $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                            }

                        }

                        $data['list_product_category'] = $array_cate;
                    }

                    break;

                case 'male':

                    $view_template = 'product_filter_list';
                    $data['list_product_category'] = array();
                    $list_category = $this->a_category->get_category_in_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('male' => 1));

                    if(!empty($list_category)){
                        $array_node = '';
                        foreach ($list_category as $row) {
                            $array_node[] = $this->a_category->get_node_category_id($row->id);
                        }
                        $array_node = array_unique($array_node);
                        $array_node = array_values($array_node);

                        if(!empty($array_node)){
                            $mod_filter = 'category';
                            switch ($mod) {
                                case 'origin':
                                    $input_origin = $mod_id;
                                    break;

                                case 'product_type':
                                    $input_product_type = $mod_id;
                                    break;

                                case 'skin_type':
                                    $input_skin_type = $mod_id;
                                    break;

                                case 'hair_type':
                                    $input_hair_type = $mod_id;
                                    break;

                                case 'action':
                                    $input_action = $mod_id;
                                    break;

                                case 'capacity':
                                    $input_capacity = $mod_id;
                                    break;

                                case 'weigh':
                                    $input_weigh = $mod_id;
                                    break;

                                case 'pill_number':
                                    $input_pill_number = $mod_id;
                                    break;
                            }

                            $item_page = 8;
                            $array_cate = array();
                            foreach ($array_node as $ct) {
                                $element = explode(',', $ct);
                                $ct_id = $element[0];
                                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                
                                $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('male' => 1));

                                switch ($sort) {
                                    case 'price-asc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('male' => 1), array(array('field' => 'price', 'sort' => 'ASC')));
                                        break;

                                    case 'price-desc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('male' => 1), array(array('field' => 'price', 'sort' => 'DESC')));
                                        break;

                                    case 'khuyen-mai':
                                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('male' => 1));
                                        break;

                                    case 'qua-tang':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('male' => 1), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                        break;
                                    
                                    default:
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('male' => 1));
                                        break;
                                }

                                $array_cate[$ct]['product'] = $list_product;
                                $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                            }

                        }

                        $data['list_product_category'] = $array_cate;
                    }

                    break;

                case 'female':
                    $view_template = 'product_filter_list';
                    $data['list_product_category'] = array();
                    $list_category = $this->a_category->get_category_in_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('female' => 1));

                    if(!empty($list_category)){
                        $array_node = '';
                        foreach ($list_category as $row) {
                            $array_node[] = $this->a_category->get_node_category_id($row->id);
                        }
                        $array_node = array_unique($array_node);
                        $array_node = array_values($array_node);

                        if(!empty($array_node)){
                            $mod_filter = 'category';
                            switch ($mod) {
                                case 'origin':
                                    $input_origin = $mod_id;
                                    break;

                                case 'product_type':
                                    $input_product_type = $mod_id;
                                    break;

                                case 'skin_type':
                                    $input_skin_type = $mod_id;
                                    break;

                                case 'hair_type':
                                    $input_hair_type = $mod_id;
                                    break;

                                case 'action':
                                    $input_action = $mod_id;
                                    break;

                                case 'capacity':
                                    $input_capacity = $mod_id;
                                    break;

                                case 'weigh':
                                    $input_weigh = $mod_id;
                                    break;

                                case 'pill_number':
                                    $input_pill_number = $mod_id;
                                    break;
                            }

                            $item_page = 8;
                            $array_cate = array();
                            foreach ($array_node as $ct) {
                                $element = explode(',', $ct);
                                $ct_id = $element[0];
                                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                
                                $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('female' => 1));

                                switch ($sort) {
                                    case 'price-asc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('female' => 1), array(array('field' => 'price', 'sort' => 'ASC')));
                                        break;

                                    case 'price-desc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('female' => 1), array(array('field' => 'price', 'sort' => 'DESC')));
                                        break;

                                    case 'khuyen-mai':
                                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('female' => 1));
                                        break;

                                    case 'qua-tang':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('female' => 1), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                        break;
                                    
                                    default:
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array('female' => 1));
                                        break;
                                }

                                $array_cate[$ct]['product'] = $list_product;
                                $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                            }

                        }

                        $data['list_product_category'] = $array_cate;
                    }
                    break;

                case 'search':
                    
                    $keyword = $this->input->post('keyword');
                    $list_cate_id = array();
                    if($cat != 0)$list_cate_id = $this->a_category->get_categroy_tree_id($cat);

                    $view_template = 'product_filter_list';
                    $data['list_product_category'] = array();
                    $list_category = $this->a_category->get_category_in_param_search($keyword, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                    if(!empty($list_category)){
                        $array_node = '';
                        foreach ($list_category as $row) {
                            $array_node[] = $this->a_category->get_node_category_id($row->id);
                        }
                        $array_node = array_unique($array_node);
                        $array_node = array_values($array_node);

                        if(!empty($array_node)){
                            $mod_filter = 'category';

                            $item_page = 8;
                            $array_cate = array();
                            foreach ($array_node as $ct) {
                                $element = explode(',', $ct);
                                $ct_id = $element[0];
                                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                
                                $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param_search($keyword, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                                switch ($sort) {
                                    case 'price-asc':
                                        $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                                        break;

                                    case 'price-desc':
                                        $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                                        break;

                                    case 'khuyen-mai':
                                        $list_product = $this->a_product->product_filter_param_sale_search($keyword, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0);
                                        break;

                                    case 'qua-tang':
                                        $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                        break;
                                    
                                    default:
                                        $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0);
                                        break;
                                }

                                $array_cate[$ct]['product'] = $list_product;
                                $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                            }

                        }

                        $data['list_product_category'] = $array_cate;
                    }
                    break;

                case 'tags':
                    $view_template = 'product_filter_list';
                    $data['list_product_category'] = array();
                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'tags');
                    $list_category = $this->a_category->get_category_in_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                    if(!empty($list_category)){
                        $array_node = '';
                        foreach ($list_category as $row) {
                            $array_node[] = $this->a_category->get_node_category_id($row->id);
                        }
                        $array_node = array_unique($array_node);
                        $array_node = array_values($array_node);

                        if(!empty($array_node)){
                            $mod_filter = 'category';
                            $item_page = 8;
                            $array_cate = array();
                            foreach ($array_node as $ct) {
                                $element = explode(',', $ct);
                                $ct_id = $element[0];
                                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                
                                $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);

                                switch ($sort) {
                                    case 'price-asc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')), $mod_id);
                                        break;

                                    case 'price-desc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')), $mod_id);
                                        break;

                                    case 'khuyen-mai':
                                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), null, $mod_id);
                                        break;

                                    case 'qua-tang':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')), $mod_id);
                                        break;
                                    
                                    default:
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), null, $mod_id);
                                        break;
                                }

                                $array_cate[$ct]['product'] = $list_product;
                                $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                            }

                        }

                        $data['list_product_category'] = $array_cate;
                    }
                    break;

                case 'product_type_category':
                case 'skin_type_category':
                case 'hair_type_category':
                case 'origin_category':
                case 'action_category':
                case 'capacity_category':
                case 'pill_number_category':
                case 'weigh_category':
                    $mod_filter = 'category';
                    switch ($mod) {
                        case 'product_type_category':
                            $input_product_type = $mod_id;
                            $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'product_type');
                            break;

                        case 'skin_type_category':
                            $input_skin_type = $mod_id;
                            $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'skin_type');
                            break;

                        case 'hair_type_category':
                            $input_hair_type = $mod_id;
                            $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'hair_type');
                            break;

                        case 'origin_category':
                            $input_origin = $mod_id;
                            $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'origin');
                            break;

                        case 'action_category':
                            $input_action = $mod_id;
                            $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'action');
                            break;

                        case 'capacity_category':
                            $input_pill_number = $mod_id;
                            $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'capacity');
                            break;

                        case 'pill_number_category':
                            $input_pill_number = $mod_id;
                            $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'pill_number');
                            break;

                        case 'weigh_category':
                            $input_weigh = $mod_id;
                            $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'weigh');
                            break;

                    }
                    
                    $category = $this->global_function->get_row_object(array('id' => $cat), 'id, parent_id, name, slug, level', 'category');
                   
                    $data['category_child'] = $this->global_function->get_array_object(array('parent_id' => $category->id), 'id, name, slug, level', 'category');                  

                    if(!empty($data['category_child'])){
                        $view_template = 'product_filter_category';
                        $item_page = ($category->level == 0) ? 24: 16;
                        $page_no_ct = 1;
                        $start_ct = ($page_no_ct - 1) * $item_page;
                        foreach ($data['category_child'] as $ct) {
                            $ct_id = $ct->id;
                            $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);

                            $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                            switch ($sort) {
                                case 'price-asc':
                                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                                    break;

                                case 'price-desc':
                                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                                    break;

                                case 'khuyen-mai':
                                    $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct);
                                    break;

                                case 'qua-tang':
                                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                    break;
                                
                                default:
                                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct);
                                    break;
                            }

                            $ct->product =  $list_product;

                            $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                        }
                   
                    }else{
                        $list_cate_id = $this->a_category->get_categroy_tree_id($cat);
                        $count = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price); 

                        switch ($sort) {
                            case 'price-asc':
                                $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                                break;

                            case 'price-desc':
                                $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                                break;

                            case 'khuyen-mai':
                                $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                                break;

                            case 'qua-tang':
                                $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                break;
                            
                            default:
                                $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                                break;
                        }

                        $data['paging'] = $this->global_function->paging_ajax($page_co, $count, $page_no);
                        $data['list_product'] = $list_product;
                    }
                    break;

                case 'tag_category':
                    $mod_filter = 'category';
                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'tags');
                    $category = $this->global_function->get_row_object(array('id' => $cat), 'id, parent_id, name, slug, level', 'category');
                   
                    $data['category_child'] = $this->global_function->get_array_object(array('parent_id' => $category->id), 'id, name, slug, level', 'category');                  

                    if(!empty($data['category_child'])){
                        $view_template = 'product_filter_category';
                        $item_page = ($category->level == 0) ? 24: 16;
                        $page_no_ct = 1;
                        $start_ct = ($page_no_ct - 1) * $item_page;
                        foreach ($data['category_child'] as $ct) {
                            $ct_id = $ct->id;
                            $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);

                            $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);

                            switch ($sort) {
                                case 'price-asc':
                                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'price', 'sort' => 'ASC')), $mod_id);
                                    break;

                                case 'price-desc':
                                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'price', 'sort' => 'DESC')), $mod_id);
                                    break;

                                case 'khuyen-mai':
                                    $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), null, $mod_id);
                                    break;

                                case 'qua-tang':
                                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')), $mod_id);
                                    break;
                                
                                default:
                                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, $start_ct, array(), null, $mod_id);
                                    break;
                            }

                            $ct->product =  $list_product;

                            $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                        }
                   
                    }else{
                        $list_cate_id = $this->a_category->get_categroy_tree_id($cat);
                        $count = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id); 

                        switch ($sort) {
                            case 'price-asc':
                                $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                                break;

                            case 'price-desc':
                                $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                                break;

                            case 'khuyen-mai':
                                $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), null, $mod_id);
                                break;

                            case 'qua-tang':
                                $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                break;
                            
                            default:
                                $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), null, $mod_id);
                                break;
                        }

                        $data['paging'] = $this->global_function->paging_ajax($page_co, $count, $page_no);
                        $data['list_product'] = $list_product;
                    }
                    break;


                default:
                    $view_template = 'product_filter_list';
                    $data['list_product_category'] = array();
                    $list_category = $this->a_category->get_category_in_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                    if(!empty($list_category)){
                        $array_node = '';
                        foreach ($list_category as $row) {
                            $array_node[] = $this->a_category->get_node_category_id($row->id);
                        }
                        $array_node = array_unique($array_node);
                        $array_node = array_values($array_node);

                        if(!empty($array_node)){
                            $mod_filter = 'category';
                            switch ($mod) {
                                case 'origin':
                                    $input_origin = $mod_id;
                                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'origin');
                                    break;

                                case 'product_type':
                                    $input_product_type = $mod_id;
                                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'product_type');
                                    break;

                                case 'skin_type':
                                    $input_skin_type = $mod_id;
                                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'skin_type');
                                    break;

                                case 'hair_type':
                                    $input_hair_type = $mod_id;
                                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'hair_type');
                                    break;

                                case 'action':
                                    $input_action = $mod_id;
                                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'action');
                                    break;

                                case 'capacity':
                                    $input_capacity = $mod_id;
                                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'capacity');
                                    break;

                                case 'weigh':
                                    $input_weigh = $mod_id;
                                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'weigh');
                                    break;

                                case 'pill_number':
                                    $input_pill_number = $mod_id;
                                    $data['detail'] = $this->global_function->get_row_object(array('id' => $mod_id), 'id, name, slug', 'pill_number');
                                    break;
                            }

                            $item_page = 8;
                            $array_cate = array();
                            foreach ($array_node as $ct) {
                                $element = explode(',', $ct);
                                $ct_id = $element[0];
                                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                
                                $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);

                                switch ($sort) {
                                    case 'price-asc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                                        break;

                                    case 'price-desc':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                                        break;

                                    case 'khuyen-mai':
                                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0);
                                        break;

                                    case 'qua-tang':
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                                        break;
                                    
                                    default:
                                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $item_page, 0);
                                        break;
                                }

                                $array_cate[$ct]['product'] = $list_product;
                                $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                            }

                        }

                        $data['list_product_category'] = $array_cate;
                    }

                    break;
            }

            
            $this->load->view('public/' . $view_template, $data);
        }

        
    }

    function product_filter_select(){        
                
        $data['price_select'] = $this->input->post('price_select');
        $data['brand_select'] = $this->input->post('brand_select');
        $data['product_type_select'] = $this->input->post('product_type_select');
        $data['skin_type_select'] = $this->input->post('skin_type_select');
        $data['hair_type_select'] = $this->input->post('hair_type_select');
        $data['action_select'] = $this->input->post('action_select');
        $data['origin_select'] = $this->input->post('origin_select');
        $data['capacity_select'] = $this->input->post('capacity_select');
        $data['weigh_select'] = $this->input->post('weigh_select');
        $data['pill_number_select'] = $this->input->post('pill_number_select');   

        $this->load->view('public/product_filter_select', $data);
    }

    function product_filter_element(){

        $data = array();
        if($this->input->post()){
            
            $mod = $this->input->post('mod');
            $mod_id = $this->input->post('mod_id');
            $cat = $this->input->post('cate_id_search');
            $sort = $this->input->post('sort');

            $input_brand = $this->input->post('input_brand');
            $data['input_brand'] = $input_brand;
            $data['layout'] = $this->input->post('layout');

            $input_product_type = '';
            if($this->input->post('input_product_type') != '')
                $input_product_type = (is_array($this->input->post('input_product_type'))) ? $this->input->post('input_product_type') : explode(',', $this->input->post('input_product_type'));
                $data['input_product_type'] = (is_array($this->input->post('input_product_type'))) ? $this->input->post('input_product_type') : $this->input->post('input_product_type');


            $input_skin_type = '';
            if($this->input->post('input_skin_type') != '')
                $input_skin_type = (is_array($this->input->post('input_skin_type'))) ? $this->input->post('input_skin_type') : explode(',', $this->input->post('input_skin_type'));
                $data['input_skin_type'] = (is_array($this->input->post('input_skin_type'))) ? $this->input->post('input_skin_type') : $this->input->post('input_skin_type');


            $input_hair_type = '';
            if($this->input->post('input_hair_type') != '')
                $input_hair_type = (is_array($this->input->post('input_hair_type'))) ? $this->input->post('input_hair_type') : explode(',', $this->input->post('input_hair_type'));
                $data['input_hair_type'] = (is_array($this->input->post('input_hair_type'))) ? $this->input->post('input_hair_type') : $this->input->post('input_hair_type');

            $input_action = '';
            if($this->input->post('input_action') != '')
                $input_action = (is_array($this->input->post('input_action'))) ? $this->input->post('input_action') : explode(',', $this->input->post('input_action'));
                $data['input_action'] = (is_array($this->input->post('input_action'))) ? $this->input->post('input_action') : $this->input->post('input_action');

            $input_origin = '';
            if($this->input->post('input_origin') != '')
                $input_origin = (is_array($this->input->post('input_origin'))) ? $this->input->post('input_origin') : explode(',', $this->input->post('input_origin'));
                $data['input_origin'] = (is_array($this->input->post('input_origin'))) ? $this->input->post('input_origin') : $this->input->post('input_origin');

            $input_capacity = '';
            if($this->input->post('input_capacity') != '')
                $input_capacity = (is_array($this->input->post('input_capacity'))) ? $this->input->post('input_capacity') : explode(',', $this->input->post('input_capacity'));
                $data['input_capacity'] = (is_array($this->input->post('input_capacity'))) ? $this->input->post('input_capacity') : $this->input->post('input_capacity');

            $input_weigh = '';
            if($this->input->post('input_weigh') != '')
                $input_weigh = (is_array($this->input->post('input_weigh'))) ? $this->input->post('input_weigh') : explode(',', $this->input->post('input_weigh'));
                $data['input_weigh'] = (is_array($this->input->post('input_weigh'))) ? $this->input->post('input_weigh') : $this->input->post('input_weigh');

            $input_pill_number = '';
            if($this->input->post('input_pill_number') != '')
                $input_pill_number = (is_array($this->input->post('input_pill_number'))) ? $this->input->post('input_pill_number') : explode(',', $this->input->post('input_pill_number'));
                $data['input_pill_number'] = $input_pill_number;
                $data['input_pill_number'] = (is_array($this->input->post('input_pill_number'))) ? $this->input->post('input_pill_number') : $this->input->post('input_pill_number');

            $input_price = ($this->input->post('input_price') != '') ? $this->input->post('input_price') : '';
            $arr_price = array();
            if($input_price != ''){
                $arr_price = $this->a_product->get_list_price($input_price);
            } 
            $data['input_price'] = $input_price;

            switch ($mod) {
                case 'category':
                    $list_cate_id = $this->a_category->get_categroy_tree_id($mod_id);
                    
                    //list price search
                    $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
                    if(!empty($price_search)){
                        foreach ($price_search as $row) {
                            $from = $row->p_from;
                            $to = $row->p_to;
                            $row->count_product = $this->a_product->count_product_price_param($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $from, $to);
                        }
                    }
                    $data['price_search'] = $price_search;

                    //list brand
                    $data['count_brand'] = 0;
                    $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    if(!empty($data['brand'])){
                        foreach ($data['brand'] as $row) {
                            $data['count_brand'] += $row->count_product;
                        }
                    }

                    //list product type
                    $data['count_product_type'] = 0;
                    $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    if(!empty($data['product_type'])){
                        $array_id = array();
                        foreach ($data['product_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list skin_type
                    $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_skin_type'] = 0;
                    if(!empty($data['skin_type'])){
                        $array_id = array();
                        foreach ($data['skin_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list hair_type
                    $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_hair_type'] = 0;
                    if(!empty($data['hair_type'])){
                        $array_id = array();
                        foreach ($data['hair_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list origin
                    $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_origin'] = 0;
                    if(!empty($data['origin'])){
                        $array_id = array();
                        foreach ($data['origin'] as $row) {
                            $data['count_origin'] += $row->count_product;
                            $array_id[] = $row->id;
                        }
                        $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                    }

                    //list action
                    $data['action'] = $this->a_action->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_action'] = 0;
                    if(!empty($data['action'])){
                        $array_id = array();
                        foreach ($data['action'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }
                    
                    //list capacity
                    $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_capacity'] = 0;
                    if(!empty($data['capacity'])){
                        $array_id = array();
                        foreach ($data['capacity'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list weigh
                    $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_weigh'] = 0;
                    if(!empty($data['weigh'])){
                        $array_id = array();
                        foreach ($data['weigh'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list pill_number
                    $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_pill_number'] = 0;
                    if(!empty($data['pill_number'])){
                        $array_id = array();
                        foreach ($data['pill_number'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    break;

                case 'male':
                    
                    //list price search
                    $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
                    if(!empty($price_search)){
                        foreach ($price_search as $row) {
                            $from = $row->p_from;
                            $to = $row->p_to;
                            $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $from, $to, array('product.male' => 1));
                        }
                    }
                    $data['price_search'] = $price_search;

                    //list brand
                    $data['count_brand'] = 0;
                    $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    if(!empty($data['brand'])){
                        foreach ($data['brand'] as $row) {
                            $data['count_brand'] += $row->count_product;
                        }
                    }

                    //list product type
                    $data['count_product_type'] = 0;
                    $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    if(!empty($data['product_type'])){
                        $array_id = array();
                        foreach ($data['product_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    }

                    //list skin_type
                    $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    $data['count_skin_type'] = 0;
                    if(!empty($data['skin_type'])){
                        $array_id = array();
                        foreach ($data['skin_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    }

                    //list hair_type
                    $data['hair_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    $data['count_hair_type'] = 0;
                    if(!empty($data['hair_type'])){
                        $array_id = array();
                        foreach ($data['hair_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    }

                    //list origin
                    $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    $data['count_origin'] = 0;
                    if(!empty($data['origin'])){
                        $array_id = array();
                        foreach ($data['origin'] as $row) {
                            $data['count_origin'] += $row->count_product;
                            $array_id[] = $row->id;
                        }
                        $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                    }

                    //list action
                    $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    $data['count_action'] = 0;
                    if(!empty($data['action'])){
                        $array_id = array();
                        foreach ($data['action'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    }
                    
                    //list capacity
                    $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    $data['count_capacity'] = 0;
                    if(!empty($data['capacity'])){
                        $array_id = array();
                        foreach ($data['capacity'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    }

                    //list weigh
                    $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    $data['count_weigh'] = 0;
                    if(!empty($data['weigh'])){
                        $array_id = array();
                        foreach ($data['weigh'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    }

                    //list pill_number
                    $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    $data['count_pill_number'] = 0;
                    if(!empty($data['pill_number'])){
                        $array_id = array();
                        foreach ($data['pill_number'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.male' => 1));
                    }

                    break;

                case 'female':
                    
                    //list price search
                    $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
                    if(!empty($price_search)){
                        foreach ($price_search as $row) {
                            $from = $row->p_from;
                            $to = $row->p_to;
                            $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $from, $to, array('product.female' => 1));
                        }
                    }
                    $data['price_search'] = $price_search;

                    //list brand
                    $data['count_brand'] = 0;
                    $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    if(!empty($data['brand'])){
                        foreach ($data['brand'] as $row) {
                            $data['count_brand'] += $row->count_product;
                        }
                    }

                    //list product type
                    $data['count_product_type'] = 0;
                    $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    if(!empty($data['product_type'])){
                        $array_id = array();
                        foreach ($data['product_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    }

                    //list skin type
                    $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    $data['count_skin_type'] = 0;
                    if(!empty($data['skin_type'])){
                        $array_id = array();
                        foreach ($data['skin_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    }

                    //list hair_type
                    $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    $data['count_hair_type'] = 0;
                    if(!empty($data['hair_type'])){
                        $array_id = array();
                        foreach ($data['hair_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    }

                    //list origin
                    $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    $data['count_origin'] = 0;
                    if(!empty($data['origin'])){
                        $array_id = array();
                        foreach ($data['origin'] as $row) {
                            $data['count_origin'] += $row->count_product;
                            $array_id[] = $row->id;
                        }
                        $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                    }

                    //list action
                    $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    $data['count_action'] = 0;
                    if(!empty($data['action'])){
                        $array_id = array();
                        foreach ($data['action'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    }
                    
                    //list capacity
                    $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    $data['count_capacity'] = 0;
                    if(!empty($data['capacity'])){
                        $array_id = array();
                        foreach ($data['capacity'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    }

                    //list weigh
                    $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    $data['count_weigh'] = 0;
                    if(!empty($data['weigh'])){
                        $array_id = array();
                        foreach ($data['weigh'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    }

                    //list pill_number
                    $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    $data['count_pill_number'] = 0;
                    if(!empty($data['pill_number'])){
                        $array_id = array();
                        foreach ($data['pill_number'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array('product.female' => 1));
                    }

                    break;

                case 'search':
                    $cat = $this->input->post('cate_id_search');
                    $keyword = $this->input->post('keyword');
                    $list_cate_id = array();
                    if($cat != 0)$list_cate_id = $this->a_category->get_categroy_tree_id($cat);

                    //list price search
                    $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
                    if(!empty($price_search)){
                        foreach ($price_search as $row) {
                            $from = $row->p_from;
                            $to = $row->p_to;
                            $row->count_product = $this->a_product->count_product_price_param_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $from, $to);
                        }
                    }
                    $data['price_search'] = $price_search;

                    //list brand
                    $data['count_brand'] = 0;
                        $data['brand'] = $this->a_brand->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        if(!empty($data['brand'])){
                            foreach ($data['brand'] as $row) {
                                $data['count_brand'] += $row->count_product;
                            }
                        }

                    //list product type
                    $data['count_product_type'] = 0;
                    $data['product_type'] = $this->a_product_type->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    if(!empty($data['product_type'])){
                        $array_id = array();
                        foreach ($data['product_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'product_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list skin type
               
                    $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_skin_type'] = 0;
                    if(!empty($data['skin_type'])){
                        $array_id = array();
                        foreach ($data['skin_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'skin_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list hair type
           
                    $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_hair_type'] = 0;
                    if(!empty($data['hair_type'])){
                        $array_id = array();
                        foreach ($data['hair_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'hair_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }
                
            

                    //list origin
              
                    $data['origin'] = $this->a_origin->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_origin'] = 0;
                    if(!empty($data['origin'])){
                        $array_id = array();
                        foreach ($data['origin'] as $row) {
                            $data['count_origin'] += $row->count_product;
                            $array_id[] = $row->id;
                        }
                        $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                    }
                

                    //list action

                    $data['action'] = $this->a_action->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_action'] = 0;
                    if(!empty($data['action'])){
                        $array_id = array();
                        foreach ($data['action'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_action'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'action', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }
               
                    
                    
                    //list capacity
                
                    $data['capacity'] = $this->a_capacity->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_capacity'] = 0;
                    if(!empty($data['capacity'])){
                        $array_id = array();
                        foreach ($data['capacity'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'capacity', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }
                

                    //list weigh
                    
                    $data['weigh'] = $this->a_weigh->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_weigh'] = 0;
                    if(!empty($data['weigh'])){
                        $array_id = array();
                        foreach ($data['weigh'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'weigh', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }
              

                    //list pill_number
                    
                    $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_pill_number'] = 0;
                    if(!empty($data['pill_number'])){
                        $array_id = array();
                        foreach ($data['pill_number'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'pill_number', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }
                    
                

                    break;

                case 'product_type_category':
                case 'skin_type_category':
                case 'hair_type_category':
                case 'origin_category':
                case 'action_category':
                case 'capacity_category':
                case 'pill_number_category':
                case 'weigh_category':
                    $mod_filter = 'category';
                    switch ($mod) {
                        case 'product_type_category':
                            $input_product_type = $mod_id;
                            break;

                        case 'skin_type_category':
                            $input_skin_type = $mod_id;
                            break;

                        case 'hair_type_category':
                            $input_hair_type = $mod_id;
                            break;

                        case 'origin_category':
                            $input_origin = $mod_id;
                            break;

                        case 'action_category':
                            $input_action = $mod_id;
                            break;

                        case 'pill_number_category':
                            $input_pill_number = $mod_id;
                            break;

                        case 'weigh_category':
                            $input_weigh = $mod_id;
                            break;

                    }
                    
                    $list_cate_id = $this->a_category->get_categroy_tree_id($cat);
                    
                    //list price search
                    $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
                    if(!empty($price_search)){
                        foreach ($price_search as $row) {
                            $from = $row->p_from;
                            $to = $row->p_to;
                            $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $from, $to);
                        }
                    }
                    $data['price_search'] = $price_search;

                    //list brand
                    $data['count_brand'] = 0;
                    $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    if(!empty($data['brand'])){
                        foreach ($data['brand'] as $row) {
                            $data['count_brand'] += $row->count_product;
                        }
                    }

                    //list product type
                    $data['count_product_type'] = 0;
                    $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    if(!empty($data['product_type'])){
                        $array_id = array();
                        foreach ($data['product_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list skin_type
                    $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_skin_type'] = 0;
                    if(!empty($data['skin_type'])){
                        $array_id = array();
                        foreach ($data['skin_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list hair_type
                    $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_hair_type'] = 0;
                    if(!empty($data['hair_type'])){
                        $array_id = array();
                        foreach ($data['hair_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list origin
                    $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_origin'] = 0;
                    if(!empty($data['origin'])){
                        $array_id = array();
                        foreach ($data['origin'] as $row) {
                            $data['count_origin'] += $row->count_product;
                            $array_id[] = $row->id;
                        }
                        $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                    }

                    //list action
                    $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_action'] = 0;
                    if(!empty($data['action'])){
                        $array_id = array();
                        foreach ($data['action'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }
                    
                    //list capacity
                    $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_capacity'] = 0;
                    if(!empty($data['capacity'])){
                        $array_id = array();
                        foreach ($data['capacity'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list weigh
                    $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_weigh'] = 0;
                    if(!empty($data['weigh'])){
                        $array_id = array();
                        foreach ($data['weigh'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    //list pill_number
                    $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    $data['count_pill_number'] = 0;
                    if(!empty($data['pill_number'])){
                        $array_id = array();
                        foreach ($data['pill_number'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                    }

                    break;

                case 'tag_category':
                    $mod_filter = 'category';
                    $list_cate_id = $this->a_category->get_categroy_tree_id($cat);
                    
                    //list price search
                    $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
                    if(!empty($price_search)){
                        foreach ($price_search as $row) {
                            $from = $row->p_from;
                            $to = $row->p_to;
                            $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $from, $to, array(), $mod_id);
                        }
                    }
                    $data['price_search'] = $price_search;

                    //list brand
                    $data['count_brand'] = 0;
                    $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    if(!empty($data['brand'])){
                        foreach ($data['brand'] as $row) {
                            $data['count_brand'] += $row->count_product;
                        }
                    }

                    //list product type
                    $data['count_product_type'] = 0;
                    $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    if(!empty($data['product_type'])){
                        $array_id = array();
                        foreach ($data['product_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);
                    }

                    //list skin_type
                    $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    $data['count_skin_type'] = 0;
                    if(!empty($data['skin_type'])){
                        $array_id = array();
                        foreach ($data['skin_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);
                    }

                    //list hair_type
                    $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    $data['count_hair_type'] = 0;
                    if(!empty($data['hair_type'])){
                        $array_id = array();
                        foreach ($data['hair_type'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);
                    }

                    //list origin
                    $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    $data['count_origin'] = 0;
                    if(!empty($data['origin'])){
                        $array_id = array();
                        foreach ($data['origin'] as $row) {
                            $data['count_origin'] += $row->count_product;
                            $array_id[] = $row->id;
                        }
                        $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                    }

                    //list action
                    $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    $data['count_action'] = 0;
                    if(!empty($data['action'])){
                        $array_id = array();
                        foreach ($data['action'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);
                    }
                    
                    //list capacity
                    $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    $data['count_capacity'] = 0;
                    if(!empty($data['capacity'])){
                        $array_id = array();
                        foreach ($data['capacity'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);
                    }

                    //list weigh
                    $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    $data['count_weigh'] = 0;
                    if(!empty($data['weigh'])){
                        $array_id = array();
                        foreach ($data['weigh'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);
                    }

                    //list pill_number
                    $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), null, $mod_id);
                    $data['count_pill_number'] = 0;
                    if(!empty($data['pill_number'])){
                        $array_id = array();
                        foreach ($data['pill_number'] as $row) {
                            $array_id[] = $row->id;
                        }
                        $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, array(), $mod_id);
                    }

                    break;    

                default:
                    
                    //list price search
                    $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
                    if(!empty($price_search)){
                        foreach ($price_search as $row) {
                            $from = $row->p_from;
                            $to = $row->p_to;
                            $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $from, $to);
                        }
                    }
                    $data['price_search'] = $price_search;

                    //list brand
                    if($mod != 'brand'){
                        $data['count_brand'] = 0;
                        $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        if(!empty($data['brand'])){
                            foreach ($data['brand'] as $row) {
                                $data['count_brand'] += $row->count_product;
                            }
                        }
                    }

                    //list product type
                    if($mod != 'product_type'){
                        $data['count_product_type'] = 0;
                        $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        if(!empty($data['product_type'])){
                            $array_id = array();
                            foreach ($data['product_type'] as $row) {
                                $array_id[] = $row->id;
                            }
                            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                            $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        }
                    }

                    //list skin type
                    if($mod != 'skin_type'){
                        $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        $data['count_skin_type'] = 0;
                        if(!empty($data['skin_type'])){
                            $array_id = array();
                            foreach ($data['skin_type'] as $row) {
                                $array_id[] = $row->id;
                            }
                            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                            $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        }
                    }

                    //list hair type
                    if($mod != 'hair_type'){
                        $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        $data['count_hair_type'] = 0;
                        if(!empty($data['hair_type'])){
                            $array_id = array();
                            foreach ($data['hair_type'] as $row) {
                                $array_id[] = $row->id;
                            }
                            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                            $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        }
                    }

                    //list origin
                    if($mod != 'origin'){
                        $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        $data['count_origin'] = 0;
                        if(!empty($data['origin'])){
                            $array_id = array();
                            foreach ($data['origin'] as $row) {
                                $data['count_origin'] += $row->count_product;
                                $array_id[] = $row->id;
                            }
                            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                        }
                    }

                    //list action
                    if($mod != 'action'){
                        $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        $data['count_action'] = 0;
                        if(!empty($data['action'])){
                            $array_id = array();
                            foreach ($data['action'] as $row) {
                                $array_id[] = $row->id;
                            }
                            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                            $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        }
                    }
                    
                    
                    //list capacity
                    if($mod != 'capacity'){
                        $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        $data['count_capacity'] = 0;
                        if(!empty($data['capacity'])){
                            $array_id = array();
                            foreach ($data['capacity'] as $row) {
                                $array_id[] = $row->id;
                            }
                            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                            $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        }
                    }

                    //list weigh
                    if($mod != 'weigh'){
                        $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        $data['count_weigh'] = 0;
                        if(!empty($data['weigh'])){
                            $array_id = array();
                            foreach ($data['weigh'] as $row) {
                                $array_id[] = $row->id;
                            }
                            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                            $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        }
                    }

                    //list pill_number
                    if($mod != 'pill_number'){
                        $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        $data['count_pill_number'] = 0;
                        if(!empty($data['pill_number'])){
                            $array_id = array();
                            foreach ($data['pill_number'] as $row) {
                                $array_id[] = $row->id;
                            }
                            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                            $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price);
                        }
                    }

                    break;
            }

            
            $this->load->view('public/product_filter_element', $data);
        }

        
    }

    function category($slug = '') {
        $category = $this->global_function->get_row_object(array('slug' => $slug), 'id, parent_id, name, slug, title, keywords, description, banner, link_banner, summary, level', 'category');
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $data['title_bar'] = $category->name;
        $data['banner'] = $category->banner;
        $data['link_banner'] = (!empty($category->link_banner)) ? $category->link_banner : current_url();

        $data['breadcrumb'] = '';
        if($category->parent_id != 0){
            $cate_parent = $this->a_category->get_parent_category($category->parent_id);
            if(count($cate_parent) > 1) $cate_parent = array_reverse($cate_parent);
             
            foreach ($cate_parent as $row) {
                $name = $row->name;
                $link = site_url($row->slug);
                $data['breadcrumb'] .= '<a href="'. $link .'" class="fist_menu_cate_a">'. $name .'</a>';
            }

            if(empty($category->banner)){
                $data['banner'] = $cate_parent[0]->banner;
            }
            $data['link_banner'] = (!empty($cate_parent[0]->link_banner)) ? $cate_parent[0]->link_banner : current_url();
        }

        $data['breadcrumb'] .= '<a href="'. current_url() .'">'. $category->name .'</a>';

        $data['category'] = $category;
        $data['big_summary'] = $category->summary;

        if ( ! $list_article = $this->cache->get('list_article_category_' . $slug ))
        {
            $list_article = $this->global_function->get_array_object(array('category_id' => $category->id, 'type' => 3), 'id, name, slug, summary, photo, folder, created_at', 'article', 9);
            $this->cache->save('list_article_category_' . $slug, $list_article, 300);
               
        }
        $data['list_article'] = $list_article;
       
        //get all child id in category  
        $mod = 'category';
        $data['mod_id'] = $category->id;  
        $list_cate_id = $this->a_category->get_categroy_tree_id($category->id);
        $data['child'] = $this->a_category->get_category($category->id);

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['origin_select'] = $this->global_function->get_array_object_in($origin_param, 'origin');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }

        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['count_product_type'] = 0;
        $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }

        //list skin_type
        $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }

        //list hair_type
        $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }

        //list origin
        $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }

        $data['filter_parameter'] = $_SERVER['QUERY_STRING'];

        //get sort param
        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

 
        $category_child  = $this->global_function->get_array_object(array('parent_id' => $category->id), 'id, name, slug, level', 'category');

        $data['list_cate'] = $category_child;

        if($category->level == 2)
            $data['child'] = $this->global_function->get_array_object(array('parent_id' => $category->parent_id), 'id, name, slug, level', 'category');
        

        if(!empty($category_child)){
             $data['title_cate'] = $data['title_bar'];
        }else{
            $category_parent = $this->global_function->get_row_object(array('id' => $category->parent_id), 'id, name', 'category');
            $data['title_cate'] = (!empty($category_parent)) ? $category_parent->name : 'Danh mục sản phẩm';
            $data['child'] = $this->global_function->get_array_object(array('parent_id' => $category->parent_id), 'id, name, slug, level', 'category');
        }
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }


        if(!empty($category_child)){
            $item_page = ($category->level == 0) ? 24: 16;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['pagination'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['pagination'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['pagination'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($category->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }

        $title = (!empty($category->title)) ? $category->title : $category->name;
        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'category');
        $this->template->write('title', $title);
        $this->template->write('keywords', $category->keywords);
        $this->template->write('description', $category->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function category_page(){
        if($this->input->post() != ''){
            $level = $this->input->post('level');
            $mod = $this->input->post('mod');
            $mod_id = $this->input->post('mod_id');
            $sort = $this->input->post('sort');
            $keyword = $this->input->post('keyword');

            $input_brand = $this->input->post('input_brand');
            $data['layout'] = $this->input->post('layout');

            if($mod != 'product_type_category'){
                $input_product_type = '';
                if($this->input->post('input_product_type') != '')
                $input_product_type = (is_array($this->input->post('input_product_type'))) ? $this->input->post('input_product_type') : explode(',', $this->input->post('input_product_type'));
            }else $input_product_type = $mod_id;
            
            if($mod != 'product_type_category'){
                $input_skin_type = '';
                if($this->input->post('input_skin_type') != '')
                    $input_skin_type = (is_array($this->input->post('input_skin_type'))) ? $this->input->post('input_skin_type') : explode(',', $this->input->post('input_skin_type'));
            }else $input_skin_type = $mod_id;

            if($mod != 'hair_type_category'){
                $input_hair_type = '';
                if($this->input->post('input_hair_type') != '')
                    $input_hair_type = (is_array($this->input->post('input_hair_type'))) ? $this->input->post('input_hair_type') : explode(',', $this->input->post('input_hair_type'));
            }else $input_hair_type = $mod_id;

            if($mod != 'action_category'){
                $input_action = '';
                if($this->input->post('input_action') != '')
                    $input_action = (is_array($this->input->post('input_action'))) ? $this->input->post('input_action') : explode(',', $this->input->post('input_action'));
            }else $input_action = $mod_id;
           
            if($mod != 'origin_category'){
                $input_origin = '';
                if($this->input->post('input_origin') != '')
                    $input_origin = (is_array($this->input->post('input_origin'))) ? $this->input->post('input_origin') : explode(',', $this->input->post('input_origin'));
            }else $input_origin = $mod_id; 

            if($mod != 'capacity_category'){
                $input_capacity = '';
                if($this->input->post('input_capacity') != '')
                    $input_capacity = (is_array($this->input->post('input_capacity'))) ? $this->input->post('input_capacity') : explode(',', $this->input->post('input_capacity'));
            }else $input_capacity = $mod_id; 

            if($mod != 'weigh_category'){
                $input_weigh = '';
                if($this->input->post('input_weigh') != '')
                    $input_weigh = (is_array($this->input->post('input_weigh'))) ? $this->input->post('input_weigh') : explode(',', $this->input->post('input_weigh'));
            }else $input_weigh = $mod_id;

            if($mod != 'pill_number_category'){
                $input_pill_number = '';
                if($this->input->post('input_pill_number') != '')
                    $input_pill_number = (is_array($this->input->post('input_pill_number'))) ? $this->input->post('input_pill_number') : explode(',', $this->input->post('input_pill_number'));
            }else $input_pill_number = $mod_id;

            $input_price = ($this->input->post('input_price') != '') ? $this->input->post('input_price') : '';
            $arr_price = array();
            if($input_price != ''){
                $arr_price = $this->a_product->get_list_price($input_price);
            }

            $data['ct_id'] = $cate_id = $this->input->post('cate_id');
            $page_no = $this->input->post('page_no');
            $page_co = $this->input->post('page_co');
            $total_ct = $this->input->post('total_ct');
            $start = ($page_no - 1) * $page_co;
            $list_cate_id = $this->a_category->get_categroy_tree_id($cate_id);
            $mod_filter = 'category';

            switch ($mod) {
                case 'promotion':
                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'ASC')));
                    break;

                case 'male':
                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('male' => 1));
                    break;

                case 'female':
                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array('female' => 1));
                    break;

                case 'tags':
                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start, array(), null, $mod_id);
                    break;

                case 'search':
                    $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                    break;
                
                default:
                    $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $input_brand, $input_origin, $input_product_type, $input_skin_type, $input_hair_type, $input_action, $input_capacity, $input_weigh, $input_pill_number, $arr_price, $page_co, $start);
                    break;
            }

            $data['list_product'] = $list_product;

            $data['pagination'] = ($level == 2) ? $this->global_function->paging_ajax_category($cate_id, $page_co, $total_ct, $page_no, array('class' => 'paging_ajax_category_child')) : $this->global_function->paging_ajax_category($cate_id, $page_co, $total_ct, $page_no);

            $this->load->view('public/category_page', $data);
        }
    }

    function brand($slug = '', $page_no = 1) { 
        
        $data['title_bar'] = 'Thương hiệu';
        $data['breadcrumb'] = '<a href="'. current_url() .'">Thương hiệu</a>';

        $title = 'Thương hiệu';
        $data['list_brand'] = $this->global_function->get_array_object(array(), 'id, name, slug, photo', 'brand');
        $data['product_type'] = $this->global_function->get_array_object(array(), 'id, name, slug', 'product_type');
        $data['categories'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug', 'category');

        $this->template->write('mod', 'brand');
        $this->template->write('title', $title);
        $this->template->write_view('content', 'public/brand', $data, TRUE);
        $this->template->render();
    }

    function brand_character_(){
        $product_type_id = $this->input->post('product_type_id');
        $char = $this->input->post('char');
        $data = array();
        if($product_type_id != '' && $char != ''){
           $data['list_brand'] = $this->a_brand->get_brand_in_product_type_width_character($product_type_id, $char); 
        }
        elseif($product_type_id != '' && $char == ''){
           $data['list_brand'] = $this->a_brand->get_brand_in_product_type($product_type_id); 
        } 
        elseif($product_type_id == '' && $char != ''){
           $data['list_brand'] = $this->a_brand->get_brand_width_character($char); 
        }else{
            $data['list_brand'] = $this->global_function->get_array_object(array(), 'id, name, slug, photo', 'brand');
        }
        $this->load->view('public/brand_character', $data);
    }

    function brand_character(){
        $category_id = $this->input->post('category_id');
        if($category_id != ''){
            $category = $this->global_function->get_row_object(array('id' => $category_id), 'id, name, slug', 'category');
            $list_cate_id = $this->a_category->get_categroy_tree_id($category_id);
        }
        $char = $this->input->post('char');
        $data = array();
        if($category_id != '' && $char != ''){
            $data['category'] = $category;
            $data['list_brand'] = $this->a_brand->get_brand_in_category_width_character($list_cate_id, $char); 
        }
        elseif($category_id != '' && $char == ''){
            $data['category'] = $category;
            $data['list_brand'] = $this->a_brand->get_brand_in_category($list_cate_id); 
        } 
        elseif($category_id == '' && $char != ''){
           $data['list_brand'] = $this->a_brand->get_brand_width_character($char); 
        }else{
            $data['list_brand'] = $this->global_function->get_array_object(array(), 'id, name, slug, photo', 'brand');
        }

        $this->load->view('public/brand_character', $data);
    }

    function brand_detail($slug = '') { 
        $brand = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, photo, summary', 'brand');
        
        $data['title_bar'] = $brand->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $brand->name .'</a>';

        $id = $brand->id;
        $data['brand'] = $brand;

        $categories = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, title_show', 'category');

        if(!empty($categories)){
            foreach ($categories as $row) {
                $cate_id = $row->id;
                $list_cate_id = $this->a_category->get_categroy_tree_id($cate_id);
                $row->product = $this->a_product->get_product_in_category_brand($list_cate_id, $id);
            }
        }
        $data['categories'] = $categories;

        $data['category'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, title_show', 'category');
        if(!empty($data['category'])){
            foreach ($data['category'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
        

        $data['count_brand'] = 0;
        $data['list_brand'] = $this->a_brand->get_all_item();
        
        $title = (!empty($brand->title)) ? $brand->title : $brand->name;

        $this->template->write('mod', 'brand_detail');
        $this->template->write('title', $title);
        $this->template->write('keywords', $brand->keywords);
        $this->template->write('description', $brand->description);
        $this->template->write_view('content', 'public/brand_detail', $data, TRUE);
        $this->template->render();
    }

    function brand_detail_category($slug = '', $slug_category = '') { 
        $brand = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, photo, summary', 'brand');
        
        $data['title_bar'] = $brand->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $brand->name .'</a>';

        $id = $brand->id;
        $data['brand'] = $brand;
        $data['category_brand'] = $this->global_function->get_row_object(array('slug' => $slug_category), 'id, name, slug, title_show', 'category');
        if(empty($data['category_brand'])) redirect(site_url());
     
        $cate_id = $data['category_brand']->id;
        $list_cate_id = $this->a_category->get_categroy_tree_id($cate_id);
        $data['category_brand']->product = $this->a_product->get_product_in_category_brand($list_cate_id, $id);
        $categories = $this->global_function->get_array_object(array('parent_id' => 0, 'id !=' => $data['category_brand']->id), 'id, name, slug, title_show', 'category');

        if(!empty($categories)){
            foreach ($categories as $row) {
                $cate_id = $row->id;
                $list_cate_id = $this->a_category->get_categroy_tree_id($cate_id);
                $row->product = $this->a_product->get_product_in_category_brand($list_cate_id, $id);
            }
        }
        $data['categories'] = $categories;

        $data['category'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, title_show', 'category');
        if(!empty($data['category'])){
            foreach ($data['category'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
        $data['list_brand'] = $this->a_brand->get_all_item();

        $title = (!empty($brand->title)) ? $brand->title : $brand->name;

        $this->template->write('mod', 'brand_detail');
        $this->template->write('title', $title);
        $this->template->write('keywords', $brand->keywords);
        $this->template->write('description', $brand->description);
        $this->template->write_view('content', 'public/brand_detail', $data, TRUE);
        $this->template->render();
    }

    function product_type($slug = '') { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'product_type');
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $id = $detail->id;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 24;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }


        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'product_type');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function product_type_category($slug = '', $cate_slug) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'product_type');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $id = $detail->id;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');

        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');


        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = $data['title_bar'];

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function skin_type($slug = '') { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'skin_type');

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $data['mod'] = $mod = $this->router->fetch_method();
        $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        $mod = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product

        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $action_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 24;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $id, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $id, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $id, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $id, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $id, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $id, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'skin_type');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function skin_type_category($slug = '', $cate_slug) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'skin_type');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');

        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = $detail->name . ' - ' . $detail_cate->name;

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function hair_type($slug = '') { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'hair_type');

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $action_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 24;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $id, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $id, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $id, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $id, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $id, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $id, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'hair_type');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function hair_type_category($slug = '', $cate_slug) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'hair_type');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');

        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = $detail->name . ' - ' . $detail_cate->name;

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function action($slug = '') { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'action');

        $id = $detail->id;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);


        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 24;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                    //echo '<pre>'; print_r($list_cate_id_ct); echo '</pre>';die;
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $id, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $id, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $id, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $id, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $id, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $id, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }
        
        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function action_category($slug = '', $cate_slug) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'action');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');

        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function origin($slug = '') { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'origin');

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = $data['page_no'] = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 24;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $id, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $id, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $id, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $id, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $id, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $id, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }
    
        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'origin');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function origin_category($slug = '', $cate_slug) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'origin');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;
        
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');

        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = $data['title_bar'];

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function capacity($slug = '') { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'capacity');

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 24;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $id, $weigh_param, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $id, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $id, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $id, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $id, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $id, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'capacity');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function capacity_category($slug = '', $cate_slug) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'capacity');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');

        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = $data['title_bar'];

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function weigh($slug = '') { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'weigh');

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 24;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $id, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $id, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $id, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $id, $pill_number_param, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $id, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $id, $pill_number_param, $arr_price, $item_page, 0);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'weigh');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function weigh_category($slug = '', $cate_slug) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'weigh');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;

       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');

        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = $data['title_bar'];

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function pill_number($slug = '') { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'pill_number');

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 24;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $id, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $id, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $id, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $id, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $id, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $id, $arr_price, $item_page, 0);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'pill_number');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function pill_number_category($slug = '', $cate_slug) { 
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'pill_number');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;

       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');

        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = $data['title_bar'];

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }

    function promotion() { 
        
        $data['breadcrumb'] = '<a href="'. current_url() .'">Khuyến mãi</a>';
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = 0;
        
        $data['title_bar'] = 'Khuyến mãi';
        $data['breadcrumb'] = '<a href="'. current_url() .'">Khuyến mãi</a>';

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 8;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC'), array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $title = 'Khuyến mãi';

        $this->template->write('mod', 'promotion');
        $this->template->write('title', $title);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function male() { 
        
        $mod = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = 0;

        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        
        $data['title_bar'] = 'Dành cho nam';
        $data['breadcrumb'] = '<a href="'. current_url() .'">Dành cho nam</a>';

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to, array('product.male' => 1));
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['count_product_type'] = 0;
        $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        }

        //list skin_type
        $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        }

        //list hair_type
        $data['hair_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        }

        //list origin
        $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.male' => 1));
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array("male" => 1));

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 8;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('male' => 1));

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('male' => 1), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('male' => 1), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('male' => 1), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('male' => 1), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('male' => 1), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $title = 'Dành cho nam';

        $this->template->write('mod', 'male');
        $this->template->write('title', $title);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function female() { 
        
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = 0;

        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        
        $data['title_bar'] = 'Dành cho nữ';
        $data['breadcrumb'] = '<a href="'. current_url() .'">Dành cho nữ</a>';

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }


        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to, array('product.female' => 1));
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
        $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        if(!empty($data['brand'])){
            foreach ($data['brand'] as $row) {
                $data['count_brand'] += $row->count_product;
            }
        }

        //list product type
        $data['count_product_type'] = 0;
        $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        }

        //list skin_type
        $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        }

        //list hair_type
        $data['hair_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        }

        //list origin
        $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }

        //list action
        $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        }
        
        //list capacity
        $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        }

        //list weigh
        $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        }

        //list pill_number
        $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('product.female' => 1));
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array("female" => 1));

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 8;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array('female' => 1));

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('female' => 1), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('female' => 1), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('female' => 1), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('female' => 1), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array('female' => 1), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $title = 'Dành cho nữ';

        $this->template->write('mod', 'female');
        $this->template->write('title', $title);
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }

    function tags($slug = '') { 
        $this->load->model(array("tags/a_tags", "article/a_article"));
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'tags');

        if(empty($detail)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod= $data['mod'] = $this->router->fetch_method();
        $data['mod_id'] = $mod_id = $id;
        
        $data['title_bar'] = $detail->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }
       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list skin type
        if($mod != 'skin_type'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list hair type
        if($mod != 'hair_type'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list origin
        if($mod != 'origin'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list weigh
        if($mod != 'weigh'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        //list pill_number
        if($mod != 'pill_number'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param($mod, $id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 8;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $id);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')), $id);
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')), $id);
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, $id);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')), $id);
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param($mod, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), null, $id);
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }

        $list_category_tag = $this->a_tags->get_category_in_tag($detail->id);

        if(!empty($list_category_tag)){
            foreach ($list_category_tag as $row) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($row->id);
                $row->list_product = $this->a_product->get_product_in_category_none_limit($list_ch_id);
            }
        }

        $data['list_category_tag'] = $list_category_tag;
        $data['other_tags'] = $this->a_tags->get_all_item(array('tags.id !=' => $id));
        $data['count_other_tags'] = 0;
        if(!empty($data['other_tags'])){
            foreach ($data['other_tags'] as $row) {
                $data['count_other_tags'] += $row->count_product;
            }
        }

        $data['list_article'] = $this->a_article->get_article_in_tags($id);

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $this->template->write('mod', 'tags');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/tags', $data, TRUE);
        $this->template->render();
    }

    function tag_category($slug = '', $cate_slug = ''){
        $detail = $this->global_function->get_row_object(array('slug' => $slug), 'id, name, slug, title, keywords, description, summary', 'tags');

        $detail_cate = $this->global_function->get_row_object(array('slug' => $cate_slug), 'id, name, slug', 'category');

        if(empty($detail) || empty($detail_cate)) redirect(site_url());

        $id = $detail->id;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $mod = $data['mod'] = $this->router->fetch_method();
        $mod_id = $data['mod_id'] = $id;
        
        $data['title_bar'] = $detail->name . ' - ' . $detail_cate->name;
        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $detail->name .'</a>';

        $data['category'] = $detail;
        $data['big_summary'] = $detail->summary;

        $mod_filter = 'category';
        $list_cate_id = $this->a_category->get_categroy_tree_id($detail_cate->id);
        $data['category_id'] = $detail_cate->id;
        
        //get all child id in category    
        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }
       
        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        if($mod != 'brand_category'){
            $brand_param = '';
            if($this->input->get('brand') != ''){
                $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $brand_param = $id;
        

        //get product_type param
        if($mod != 'product_type_category'){
            $product_type_param = '';
            if($this->input->get('product_type') != ''){
                $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
                $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
            }
        }else $product_type_param = $id;
        

        //get skin_type param
        if($mod != 'skin_type_category'){
            $skin_type_param = '';
            if($this->input->get('skin_type') != ''){
                $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
                $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
            }
        }else $skin_type_param = $id;
        

        //get hair_type param
        if($mod != 'hair_type_category'){
            $hair_type_param = '';
            if($this->input->get('hair_type') != ''){
                $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
                $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
            }
        }else $hair_type_param = $id;
        

        //get origin param
        if($mod != 'origin_category'){
            $origin_param = '';
            if($this->input->get('origin') != ''){
                $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
                $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
            }
        }else $origin_param = $id;
        
        //get origin param
        if($mod != 'action_category'){
            $action_param = '';
            if($this->input->get('function') != ''){
                $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
                $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
            }
        }else $action_param = $id;
        
        
        //get capacity param
        if($mod != 'capacity_category'){
            $capacity_param = '';
            if($this->input->get('capacity') != ''){
                $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
                $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
            }
        }else $capacity_param = $id;
        

        //get weigh param
        if($mod != 'weigh_category'){
            $weigh_param = '';
            if($this->input->get('weigh') != ''){
                $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
                $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
            }
        }else $weigh_param = $id;
        

        //get pill_number param
        if($mod != 'pill_number_category'){
            $pill_number_param = '';
            if($this->input->get('pill_number') != ''){
                $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
                $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
            }
        }else $pill_number_param = $id;


       
        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to, array(), $mod_id);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        if($mod != 'brand_category'){
            $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), null, $mod_id);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }
        }

        //list product type
        if($mod != 'product_type_category'){
            $data['count_product_type'] = 0;
            $data['product_type'] = $this->a_product_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), null, $mod_id);
            if(!empty($data['product_type'])){
                $array_id = array();
                foreach ($data['product_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'product_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);
            }
        }

        //list skin type
        if($mod != 'skin_type_category'){
            $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), null, $mod_id);
            $data['count_skin_type'] = 0;
            if(!empty($data['skin_type'])){
                $array_id = array();
                foreach ($data['skin_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'skin_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);
            }
        }

        //list hair type
        if($mod != 'hair_type_category'){
            $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $arr_price, array(), null, $mod_id);
            $data['count_hair_type'] = 0;
            if(!empty($data['hair_type'])){
                $array_id = array();
                foreach ($data['hair_type'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id($array_id, 'hair_type', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);
            }
        }

        //list origin
        if($mod != 'origin_category'){
            $data['origin'] = $this->a_origin->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), null, $mod_id);
            $data['count_origin'] = 0;
            if(!empty($data['origin'])){
                $array_id = array();
                foreach ($data['origin'] as $row) {
                    $data['count_origin'] += $row->count_product;
                    $array_id[] = $row->id;
                }
                $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            }
        }

        //list action
        if($mod != 'action_category'){
            $data['action'] = $this->a_action->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), null, $mod_id);
            $data['count_action'] = 0;
            if(!empty($data['action'])){
                $array_id = array();
                foreach ($data['action'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_action'] = $this->a_product->count_product_filter_param_list_id($array_id, 'action', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);
            }
        }
        
        
        //list capacity
        if($mod != 'capacity_category'){
            $data['capacity'] = $this->a_capacity->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), null, $mod_id);
            $data['count_capacity'] = 0;
            if(!empty($data['capacity'])){
                $array_id = array();
                foreach ($data['capacity'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id($array_id, 'capacity', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);
            }
        }

        //list weigh
        if($mod != 'weigh_category'){
            $data['weigh'] = $this->a_weigh->get_item_in_param_filter($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), null, $mod_id);
            $data['count_weigh'] = 0;
            if(!empty($data['weigh'])){
                $array_id = array();
                foreach ($data['weigh'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id($array_id, 'weigh', $mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);
            }
        }

        //list pill_number
        if($mod != 'pill_number_category'){
            $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter($mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), null, $mod_id);
            $data['count_pill_number'] = 0;
            if(!empty($data['pill_number'])){
                $array_id = array();
                foreach ($data['pill_number'] as $row) {
                    $array_id[] = $row->id;
                }
                $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
                $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id($array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);
            }
        }

        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

        $category_child = $this->global_function->get_array_object(array('parent_id' => $detail_cate->id), 'id, name, slug, level', 'category');

        if(!empty($category_child)){
            $item_page = 24;
            foreach ($category_child as $ct) {
                $ct_id = $ct->id;
                $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
                $ct->total_ct = $total_ct = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);

                switch ($sort) {
                    case 'price-asc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')), $mod_id);
                        break;

                    case 'price-desc':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')), $mod_id);
                        break;

                    case 'khuyen-mai':
                        $list_product = $this->a_product->product_filter_param_sale($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), null, $mod_id);
                        break;

                    case 'qua-tang':
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')), $mod_id);
                        break;
                    
                    default:
                        $list_product = $this->a_product->product_filter_param($mod_filter, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), null, $mod_id);
                        break;
                }

                $ct->product = $list_product;
                $ct->pagination = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
            }

            $data['category_child'] = $category_child;
        }else{
            //list product
            $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
            $page_co = 40;
            $start = ($page_no - 1) * $page_co;

            $count = $data['total_ct'] = $this->a_product->count_product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, array(), $mod_id);
            switch ($sort) {
                case 'price-asc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'ASC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-asc/", $page_no);
                    break;

                case 'price-desc':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'price', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/price-desc/", $page_no);
                    break;

                case 'khuyen-mai':
                    $data['list_product'] = $this->a_product->get_product_in_category_sale($list_cate_id, $page_co, $start);
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/khuyen-mai/", $page_no);
                    break;

                case 'qua-tang':
                    $data['list_product'] = $this->a_product->get_product_in_category_where(array(), $list_cate_id, $page_co, $start, array('field' => 'gift_type', 'sort' => 'DESC'));
                    $data['paging'] = $this->global_function->paging($page_co, $count, $slug."/qua-tang/", $page_no);
                    break;
                
                default:
                
                    $data['list_product'] = $this->a_product->product_filter_param($mod_filter, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $page_co, $start, array(), null, $mod_id);
                    $data['pagination'] = $this->global_function->paging_ajax_category($detail_cate->id, $page_co, $count, $page_no, array('class' => 'paging_ajax_category_child'));
                 
            }
            $data['page_no'] = $page_no;
        }
        

        $title = (!empty($detail->title)) ? $detail->title : $detail->name;

        $view_template = (!empty($data['category_child'])) ? 'category_parent' : 'category'; 

        $this->template->write('mod', 'action');
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/' . $view_template, $data, TRUE);
        $this->template->render();
    }


    function detail($slug = ''){
        $this->load->model(array("tags/a_tags"));
        $detail = $this->global_function->get_row_object(array('slug' => $slug), '*', 'product');
        $data['detail'] = $detail;
        $id = $detail->id;

        $views = $detail->views + 1;
        $this->db->where('id', $id);
        $this->db->update('product', array('views' => $views));

        $category = $this->a_product->get_category_info($id);
        $list_cate_id = $this->a_category->get_categroy_tree_id($category->id);

        $data['breadcrumb'] = '';
        if($category->parent_id != 0){
            $cate_parent = $this->a_category->get_parent_category($category->parent_id);
            if(count($cate_parent) > 1) $cate_parent = array_reverse($cate_parent);
             
            foreach ($cate_parent as $row) {
                $name = $row->name;
                $link = site_url($row->slug);
                $data['breadcrumb'] .= '<a href="'. $link .'" class="fist_menu_cate_a">'. $name .'</a>';
            }
        }

        $data['breadcrumb'] .= '<a href="'. site_url($category->slug) .'">'. $category->name .'</a>';

        $data['brand'] = $this->global_function->get_row_object(array('id' => $detail->brand_id), 'name, slug, summary', 'brand');
        $data['origin'] = $this->global_function->get_row_object(array('id' => $detail->origin_id), 'name, slug', 'origin');

        $data['product_type'] = $this->a_product_type->get_product_type_in_detail($id);

        $data['skin_type'] = $this->a_skin_type->get_skin_type_in_detail($id);
        $data['hair_type'] = $this->a_hair_type->get_hair_type_in_detail($id);
        $data['action'] = $this->a_action->get_action_in_detail($id);
        $data['capacity'] = $this->a_capacity->get_capacity_in_detail($id);
        $data['weigh'] = $this->a_weigh->get_weigh_in_detail($id);
        $data['pill_number'] = $this->a_pill_number->get_pill_number_in_detail($id);

        $data['product_photos'] = $this->global_function->get_array_object(array('product_id' => $id), 'id, product_id, photo, thumb', 'product_photo');

        if ( ! $other_product = $this->cache->get('other_product_' . $slug ))
        {
            $other_product = $this->a_product->get_product_in_category($list_cate_id, 20, 0);
            $this->cache->save('other_product_' . $slug, $other_product, 300);
               
        }
        $data['other_product'] = $other_product;

        $data['care_product'] = $this->a_product-> get_product_in_category_where(array('product.id !=' => $id), $list_cate_id, 3, 0, array('field' => 'product.id', 'sort' => 'RANDOM'));

        $data['company'] = $this->global_function->get_row_object(array('id' => 1), 'phone, email, gift_summary', 'company');

        $data['category_frm'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name', 'category');
        $data['skin_type_frm'] = $this->global_function->get_array_object(array(), 'id, name', 'skin_type');

        $data['tags'] = $this->a_tags->show_list_tags_in_product($id);

        $list_comment = $this->a_comment->all_comment_product($id);
        $array_comment = array();
        if(!empty($list_comment)){
            foreach ($list_comment as $row) {
                $id = $row['id'];
                $parent_id = $row['parent_id'];
                if($parent_id == 0) $array_comment[$id]['parent'] = $row;
                else $array_comment[$parent_id]['child'][] = $row;
                
            }
        }
        $data['array_comment'] = $array_comment;

        $data['session_user'] = $session_user = ($this->session->userdata("user_log")) ? $this->session->userdata("user") : '';

        $session_login = $data['session_login'] = $this->session->userdata("user_log");
        
        $data['province'] = $this->global_function->get_province();
        // $data['district'] = $this->global_function->get_district($data['province_id']);
        $data['province_id'] = ($session_login) ? $session_user->province_id : $this->input->post('province_id');
        $data['district_id'] = ($session_login) ? $session_user->district_id : $this->input->post('district_id');

        $this->template->write('mod', 'product_detail');
        $title = (!empty($detail->title)) ? $detail->title : $detail->name;
        $data['share_face'] = 
        '<meta property="og:type" content="website" />
        <meta property="og:url" content="'.current_url().'"/>
        <meta property="og:title" content="'.$title.'"/>
        <meta property="og:description" content="'.$detail->description.'" />
        <meta property="og:image" content="'.base_url()._upload_product.$detail->folder.'/'.$detail->photo.'"/>';
        $this->template->write('title', $title);
        $this->template->write('keywords', $detail->keywords);
        $this->template->write('description', $detail->description);
        $this->template->write_view('content', 'public/detail', $data, TRUE);
        $this->template->render();
    }
    
    function s_search(){
        $data = $this->input->post();
        $this->session->set_userdata('keyword', $data['keyword']);

        $keyword = $this->my_lib->changeTitle($data['keyword']);
        redirect(site_url('search/cat/' . $data['s_cate']. '/q/' . $keyword));
    }
    
    function search($cat = 0, $keyword = '') {
        $data['cat'] = $cat;
        $data['keyword'] = $keyword;
        $data['breadcrumb'] = '<a href="'. current_url() .'">Tìm kiếm</a>';
        $mod = $data['mod'] = $this->router->fetch_method();
        $data['mod_id'] = $mod_id = 0;
        $data['sort_mobile'] = 1;
        $data['filter_mobile'] = 1;
        $list_cate_id = array();
        if($cat != 0)$list_cate_id = $this->a_category->get_categroy_tree_id($cat);

        $data['child'] = $this->global_function->get_array_object(array('parent_id' => 0), 'id, name, slug, level', 'category');
        if(!empty($data['child'])){
            foreach ($data['child'] as $ch) {
                $list_ch_id = $this->a_category->get_categroy_tree_id($ch->id);
                $ch->count_product = $this->a_category->count_product($list_ch_id);
            }
        }

        //get price param
        $arr_price = array();
        if($this->input->get('price') != ''){
            $price_param = (is_numeric($this->input->get('price'))) ? $this->input->get('price') : explode(',', $this->input->get('price'));
            if($price_param != ''){
                $arr_price = $this->a_product->get_list_price($price_param);
            } 
            $data['price_select'] = $this->global_function->get_array_object_in($price_param, 'price_search');
        }

        //get brand param
        $brand_param = '';
        if($this->input->get('brand') != ''){
            $brand_param = (is_numeric($this->input->get('brand'))) ? $this->input->get('brand') : explode(',', $this->input->get('brand'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }

        //get product_type param
        $product_type_param = '';
        if($this->input->get('product_type') != ''){
            $product_type_param = (is_numeric($this->input->get('product_type'))) ? $this->input->get('product_type') : explode(',', $this->input->get('product_type'));
            $data['product_type_select'] = $this->global_function->get_array_object_in($product_type_param, 'product_type');
        }

        //get skin_type param
        $skin_type_param = '';
        if($this->input->get('skin_type') != ''){
            $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? $this->input->get('skin_type') : explode(',', $this->input->get('skin_type'));
            $data['skin_type_select'] = $this->global_function->get_array_object_in($skin_type_param, 'skin_type');
        }

        //get hair_type param
        $hair_type_param = '';
        if($this->input->get('hair_type') != ''){
            $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? $this->input->get('hair_type') : explode(',', $this->input->get('hair_type'));
            $data['hair_type_select'] = $this->global_function->get_array_object_in($hair_type_param, 'hair_type');
        }

        //get origin param
        $origin_param = '';
        if($this->input->get('origin') != ''){
            $origin_param = (is_numeric($this->input->get('origin'))) ? $this->input->get('origin') : explode(',', $this->input->get('origin'));
            $data['brand_select'] = $this->global_function->get_array_object_in($brand_param, 'brand');
        }
        //get origin param
        $action_param = '';
        if($this->input->get('function') != ''){
            $action_param = (is_numeric($this->input->get('function'))) ? $this->input->get('function') : explode(',', $this->input->get('function'));
            $data['action_select'] = $this->global_function->get_array_object_in($action_param, 'action');
        }

        //get capacity param
        $capacity_param = '';
        if($this->input->get('capacity') != ''){
            $capacity_param = (is_numeric($this->input->get('capacity'))) ? $this->input->get('capacity') : explode(',', $this->input->get('capacity'));
            $data['capacity_select'] = $this->global_function->get_array_object_in($capacity_param, 'capacity');
        }

        //get weigh param
        $weigh_param = '';
        if($this->input->get('weigh') != ''){
            $weigh_param = (is_numeric($this->input->get('weigh'))) ? $this->input->get('weigh') : explode(',', $this->input->get('weigh'));
            $data['weigh_select'] = $this->global_function->get_array_object_in($weigh_param, 'weigh');
        }

        //get pill_number param
        $pill_number_param = '';
        if($this->input->get('pill_number') != ''){
            $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? $this->input->get('pill_number') : explode(',', $this->input->get('pill_number'));
            $data['pill_number_select'] = $this->global_function->get_array_object_in($pill_number_param, 'pill_number');
        }

        //list price search
        $price_search = $this->global_function->get_array_object(array(), 'id, name, slug, p_from, p_to', 'price_search');
        if(!empty($price_search)){
            foreach ($price_search as $row) {
                $from = $row->p_from;
                $to = $row->p_to;
                $row->count_product = $this->a_product->count_product_price_param_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $from, $to);
            }
        }
        $data['price_search'] = $price_search;

        //list brand
        $data['count_brand'] = 0;
            $data['brand'] = $this->a_brand->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
            if(!empty($data['brand'])){
                foreach ($data['brand'] as $row) {
                    $data['count_brand'] += $row->count_product;
                }
            }

        //list product type
        $data['count_product_type'] = 0;
        $data['product_type'] = $this->a_product_type->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        if(!empty($data['product_type'])){
            $array_id = array();
            foreach ($data['product_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_product_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_product_type'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'product_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }

        //list skin type
   
        $data['skin_type'] = $this->a_skin_type->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_skin_type'] = 0;
        if(!empty($data['skin_type'])){
            $array_id = array();
            foreach ($data['skin_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_skin_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_skin_type'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'skin_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }

        //list hair type

        $data['hair_type'] = $this->a_hair_type->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_hair_type'] = 0;
        if(!empty($data['hair_type'])){
            $array_id = array();
            foreach ($data['hair_type'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_hair_type_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_hair_type'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'hair_type', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }
    


        //list origin
  
        $data['origin'] = $this->a_origin->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_origin'] = 0;
        if(!empty($data['origin'])){
            $array_id = array();
            foreach ($data['origin'] as $row) {
                $data['count_origin'] += $row->count_product;
                $array_id[] = $row->id;
            }
            $data['all_origin_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
        }
    

        //list action

        $data['action'] = $this->a_action->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_action'] = 0;
        if(!empty($data['action'])){
            $array_id = array();
            foreach ($data['action'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_action_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_action'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'action', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }
   
        
        
        //list capacity
    
        $data['capacity'] = $this->a_capacity->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_capacity'] = 0;
        if(!empty($data['capacity'])){
            $array_id = array();
            foreach ($data['capacity'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_capacity_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_capacity'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'capacity', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }
    

        //list weigh
        
        $data['weigh'] = $this->a_weigh->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_weigh'] = 0;
        if(!empty($data['weigh'])){
            $array_id = array();
            foreach ($data['weigh'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_weigh_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_weigh'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'weigh', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }
  

        //list pill_number
        
        $data['pill_number'] = $this->a_pill_number->get_item_in_param_filter_search($keyword, $list_cate_id, $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        $data['count_pill_number'] = 0;
        if(!empty($data['pill_number'])){
            $array_id = array();
            foreach ($data['pill_number'] as $row) {
                $array_id[] = $row->id;
            }
            $data['all_pill_number_id'] = (!empty($array_id)) ? implode(',', $array_id) : '';
            $data['count_pill_number'] = $this->a_product->count_product_filter_param_list_id_search($keyword, $list_cate_id, $array_id, 'pill_number', $mod, $mod_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);
        }


        $sort = $data['sort'] = $this->input->get('sort');
        $layout = $data['layout'] = ($this->input->get('layout')) ? $this->input->get('layout') : 'grid';

        //list product
        $page_no = ($this->input->get('page') != '') ? $this->input->get('page') : 1;
        $data['list_product_category'] = array();
        $list_category = $this->a_category->get_category_in_param_search($keyword, $list_cate_id, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

        if(!empty($list_category)){
            $array_node = '';
            foreach ($list_category as $row) {
                $array_node[] = $this->a_category->get_node_category_id($row->id);
            }
            $array_node = array_unique($array_node);
            $array_node = array_values($array_node);

            if(!empty($array_node)){
                $mod = 'category';
                $item_page = 8;
                $array_cate = array();
                foreach ($array_node as $ct) {
                    $element = explode(',', $ct);
                    $ct_id = $element[0];
                    $list_cate_id_ct = $this->a_category->get_categroy_tree_id($ct_id);
    
                    $array_cate[$ct]['total_ct'] = $total_ct = $this->a_product->count_product_filter_param_search($keyword, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price);

                    switch ($sort) {
                        case 'price-asc':
                            $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'ASC')));
                            break;

                        case 'price-desc':
                            $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'price', 'sort' => 'DESC')));
                            break;

                        case 'khuyen-mai':
                            $list_product = $this->a_product->product_filter_param_sale_search($keyword, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0);
                            break;

                        case 'qua-tang':
                            $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id_ct, $brand_param, $origin_param, $id, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'gift_type', 'sort' => 'DESC')));
                            break;
                        
                        default:
                            $list_product = $this->a_product->product_filter_param_search($keyword, $list_cate_id_ct, $brand_param, $origin_param, $product_type_param, $skin_type_param, $hair_type_param, $action_param, $capacity_param, $weigh_param, $pill_number_param, $arr_price, $item_page, 0, array(), array(array('field' => 'promotion', 'sort' => 'DESC')));
                            break;
                    }

                    $array_cate[$ct]['product'] = $list_product;
                    $array_cate[$ct]['pagination'] = $this->global_function->paging_ajax_category($ct_id, $item_page, $total_ct);
                }

            }

            $data['list_product_category'] = $array_cate;
        }
        
        $data['keyword'] = $keyword;
        $data['cat'] = $cat;
        $data['title_bar'] = 'Kết quả tìm kiếm';
        
        $this->template->write('mod', 'search');
        $this->template->write('title', 'Tìm kiếm sản phẩm');
        $this->template->write_view('content', 'public/list', $data, TRUE);
        $this->template->render();
    }
    
    /* -------------------GIO HANG ----------------- */
    
    function cart() {   
        if(count($this->cart->contents()) == 0) redirect(site_url());           
        $data['title_bar'] = 'Giỏ hàng';
        $data['breadcrumb'] = '<a href="'. current_url() .'">Giỏ hàng</a>';

        $data['param'] = '';
        if($this->input->get('province_id') != '') $data['param'] = $data['param'] . '?province_id=' . $this->input->get('province_id');

        if($this->input->get('province_id') != '' && $this->input->get('district_id') != '') $data['param'] = $data['param'] . '&district_id=' . $this->input->get('district_id');

        $this->template->write('mod', 'cart');
        $this->template->write('title', 'Giỏ hàng');
        $this->template->write_view('content', 'public/cart', $data, TRUE);
        $this->template->render();
    }
    
    function checkout()
    {
        $this->load->library(array(
            'nganluong',
        ));
        if ($this->input->post('ok') != '') {
            ob_start();
            $out           = fopen('php://output', 'w');
            $session_user  = $this->session->userdata("user");
            $session_login = $this->session->userdata("user_log");
            $member_id     = ($session_login) ? $session_user->id : 0;
            $total         = $this->cart->total();
            $order_code = random_string('numeric', 10);
            if (count($this->cart->contents()) > 0) {
                foreach ($this->cart->contents() as $row) {
                    $id      = $row['id'];
                    $product = $this->global_function->get_row_object(array(
                        'id' => $id
                    ), 'name', 'product');
                    $photo   = $row['images'];
                    $price   = $row['price'];
                    $array[] = array(
                        'id' => $id,
                        'product' => $product->name,
                        'images' => $row['images'],
                        'link' => $row['link'],
                        'price' => $price,
                        'qty' => $row['qty'],
                        'subtotal' => $row['subtotal']
                    );
                }
                $order_info = json_encode($array);
            }

            if ($this->input->post('pay') == 'nganluong') {
                $dataPayment['full_name']       = strip_tags($this->input->post('full_name'));
                $dataPayment['email']           = $this->input->post('email');
                $dataPayment['address']         = strip_tags($this->input->post('address'));
                $dataPayment['phone']           = $this->input->post('phone');
                $dataPayment['note']            = strip_tags($this->input->post('note'));
                $dataPayment['order_status_id'] = 1;
                $dataPayment['member_id']       = $member_id;
                $dataPayment['pay']     = $this->input->post('pay');
                $dataPayment['province_id']     = $this->input->post('province_id');
                $dataPayment['district_id']     = $this->input->post('district_id');
                $dataPayment['total']           = $total;
                $dataPayment['order_info']      = $order_info;
                $dataPayment['code']            = $order_code;
                $dataPayment['ip_address']      = $this->input->ip_address();
                $dataPayment['user_agent']      = $this->input->user_agent();
                $dataPayment['website']         = base_url();
                $this->session->set_userdata('dataPayment', $dataPayment);

                $return_url= site_url('thanh-toan-ngan-luong');
                // Link nut hủy đơn hàng
                $cancel_url= site_url('hoan-tat-don-hang');  
                //Giá của cả giỏ hàng 
                $txh_name = strip_tags($this->input->post('full_name'));  
                $txt_email = $this->input->post('email');    
                $txt_phone = $this->input->post('phone');  
                $price = $total;     
                //Thông tin giao dịch
                $transaction_info="Thong tin giao dich";
                $currency= "vnd";
                $quantity=1;
                $tax=0;
                $discount=0;
                $fee_cal=0;
                $fee_shipping=0;
                $order_description="Thong tin don hang: ".$order_code;
                $buyer_info = $txh_name."*|*".$txt_email."*|*".$txt_phone;
                $affiliate_code="";

                $url = $this->nganluong->orderProcessUrl($return_url, $cancel_url, $transaction_info, $order_code, $price, $currency, $quantity, $tax, $discount , $fee_cal, $fee_shipping, $order_description, $buyer_info , $affiliate_code);
                // echo $url;die;
                
                // echo '<meta http-equiv="refresh" content="0; url='.$url.'" >';
                redirect($url);

            }

            $sql['full_name']       = strip_tags($this->input->post('full_name'));
            $sql['email']           = $this->input->post('email');
            $sql['address']         = strip_tags($this->input->post('address'));
            $sql['phone']           = $this->input->post('phone');
            $sql['note']            = strip_tags($this->input->post('note'));
            $sql['order_status_id'] = 1;
            $sql['member_id']       = $member_id;
            $sql['pay']     = $this->input->post('pay');
            $sql['province_id']     = $this->input->post('province_id');
            $sql['district_id']     = $this->input->post('district_id');
            $sql['total']           = $total;
            $sql['order_info']      = $order_info;
            $sql['code']            = $order_code;
            $sql['ip_address']      = $this->input->ip_address();
            $sql['user_agent']      = $this->input->user_agent();
            $sql['website']         = base_url();
            $this->db->insert("tpl_order", $sql);
            $district      = $this->global_function->get_row_object(array(
                'id' => $sql['district_id']
            ), 'name', 'district');
            $district_name = !(empty($district)) ? $district->name : '';
            $province      = $this->global_function->get_row_object(array(
                'id' => $sql['province_id']
            ), 'name', 'province');
            $province_name = !(empty($province)) ? $province->name : '';
            $body          = '<table>';
            $body .= '<tr><th colspan="2">Đơn hàng ' . $sql['code'] . ' từ website ' . site_url() . '</th></tr><tr><th>Họ tên :</th><td>' . $sql['full_name'] . '</td></tr><tr><th>Địa chỉ :</th><td>' . $sql['address'] . '</td></tr><tr><th>Quận huyện :</th><td>' . $district_name . '</td></tr><tr><tr><th>Tỉnh thành :</th><td>' . $province_name . '</td></tr><tr><th>Điện thoại :</th><td>' . $sql['phone'] . '</td></tr><tr><th>Email :</th><td>' . $sql['email'] . '</td></tr><tr><th>Ghi chú :</th><td>' . $sql['note'] . '</td></tr>';
            $body .= '</table>';
            $body .= '<table cellpadding="6" cellspacing="1" style="width:100%; margin:10px 0; padding:10px 0" border="0"><tr><th colspan="2" style="width:42%;border-bottom:1px solid #ccc;line-height:30px">Sản phẩm</th><th style="text-align:center;border-bottom:1px solid #ccc;">Số lượng</th><th style="text-align:center;;border-bottom:1px solid #ccc;">Đơn giá</th><th style="text-align:center;border-bottom:1px solid #ccc;">Thành tiền</th></tr>';
            foreach ($this->cart->contents() as $row) {
                $id       = $row['id'];
                $product  = $this->global_function->get_row_object(array(
                    'id' => $id
                ), 'name', 'product');
                $photo    = $row['images'];
                $price    = $row['price'];
                $link     = $row['link'];
                $subtotal = $row['subtotal'];
                $body .= '<tr><td width="12%" style="text-align:center;border-bottom:1px solid #ccc;padding:10px 0; border-left:1px solid #ccc">';
                $body .= '<p><strong><img src=' . $photo . ' width="60" /></p><br>';
                $body .= '</td><td style="text-align:center;border-bottom:1px solid #ccc;padding:10px 0; font-size:13px; text-transform: uppercase;"><a href="' . $link . '">' . $product->name . '</a></td>';
                $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;">' . $row['qty'] . '</td>';
                $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;">' . number_format($price, 0, ',', '.') . ' VNĐ' . '</td>';
                $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;border-right:1px solid #ccc">' . number_format($subtotal, 0, ',', '.') . ' VNĐ' . '</td></tr>';
            }
            $body .= '<tr><td colspan="5" class="right" style="padding:10px 0;font-size:14px;"><strong>Tổng tiền: </strong>' . number_format($this->cart->total(), 0, ',', '.') . ' VNĐ' . '</td></tr>';
            $body .= '</table>';
            $this->cart->destroy();
            $data_order           = array();
            $data_order['mailto'] = 'dangthinh@iclick.vn';
            $data_order['mailcc'] = 'hientt@iclick.vn';
            $data_order['code']   = $sql['code'];
            $data_order['title']  = 'Đơn hàng ' . $sql['code'] . ' từ website ' . site_url();
            $params         = array(
                'mailto' => $data_order['mailto'],
                'mailcc' => $data_order['mailcc'],
                'title' => $data_order['title'],
                'content' => $body
            );
            $query          = http_build_query($params);
            $ch             = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_STDERR, $out);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_exec($ch);
            fclose($out);
            $debug = ob_get_clean();
            curl_close($ch);

            redirect(site_url('don-hang/' . $data_order['code']));
        }
        if (count($this->cart->contents()) == 0)
            redirect(site_url());
        $data['breadcrumb'] = '<a href="' . current_url() . '">Hoàn tất đơn hàng</a>';
        $session_user       = $this->session->userdata("user");
        $session_login      = $data['session_login'] = $this->session->userdata("user_log");
        $member_id          = ($session_login) ? $session_user->id : 0;
        if (!($session_login))
            $this->session->set_userdata('url_redirect', current_url());
        $data['company']     = $this->global_function->get_row_object(array(
            'id' => 1
        ), 'phone, email', 'company');
        $data['full_name']   = ($session_login) ? $session_user->full_name : $this->input->post('full_name');
        $data['email']       = ($session_login) ? $session_user->email : $this->input->post('email');
        $data['address']     = ($session_login) ? $session_user->address : $this->input->post('address');
        $data['phone']       = ($session_login) ? $session_user->phone : $this->input->post('phone');
        $data['note']        = $this->input->post('note');
        $data['province_id'] = $this->input->get('province_id');
        $data['district_id'] = $this->input->get('district_id');
        if ($session_login) {
            $data['province_id'] = ($this->input->get('province_id') != '') ? $this->input->get('province_id') : $session_user->province_id;
            $data['district_id'] = ($this->input->get('district_id') != '') ? $this->input->get('district_id') : $session_user->district_id;
        }
        $data['province'] = $this->global_function->get_province();
        $data['district'] = $this->global_function->get_district($data['province_id']);
        $this->template->write('mod', 'checkout');
        $this->template->write('title', 'Hoàn tất đơn hàng');
        $this->template->write_view('content', 'public/checkout', $data, TRUE);
        $this->template->render();
    }

    function nganluongProcess()
    {
        $this->load->library(array(
            'nganluong',
        ));
        if ($this->input->get('payment_id') != '') {
            // Lấy các tham số để chuyển sang Ngânlượng thanh toán:

            $transaction_info =$this->input->get('transaction_info');
            $order_code =$this->input->get('order_code');
            $price =$this->input->get('price');
            $payment_id =$this->input->get('payment_id');
            $payment_type =$this->input->get('payment_type');
            $error_text =$this->input->get('error_text');
            $secure_code =$this->input->get('secure_code');
            //Khai báo đối tượng của lớp NL_Checkout
            $checkpay = $this->nganluong->verifyPayment($transaction_info, $order_code, $price, $payment_id, $payment_type, $error_text, $secure_code);
            
            if ($checkpay) {    
                $sql = $this->session->userdata('dataPayment');

                $this->db->insert("tpl_order", $sql);
                $district      = $this->global_function->get_row_object(array(
                    'id' => $sql['district_id']
                ), 'name', 'district');
                $district_name = !(empty($district)) ? $district->name : '';
                $province      = $this->global_function->get_row_object(array(
                    'id' => $sql['province_id']
                ), 'name', 'province');
                $province_name = !(empty($province)) ? $province->name : '';
                $body          = '<table>';
                $body .= '<tr><th colspan="2">Đơn hàng ' . $sql['code'] . ' từ website ' . site_url() . '</th></tr><tr><th>Họ tên :</th><td>' . $sql['full_name'] . '</td></tr><tr><th>Địa chỉ :</th><td>' . $sql['address'] . '</td></tr><tr><th>Quận huyện :</th><td>' . $district_name . '</td></tr><tr><tr><th>Tỉnh thành :</th><td>' . $province_name . '</td></tr><tr><th>Điện thoại :</th><td>' . $sql['phone'] . '</td></tr><tr><th>Email :</th><td>' . $sql['email'] . '</td></tr><tr><th>Ghi chú :</th><td>' . $sql['note'] . '</td></tr>';
                $body .= '</table>';
                $body .= '<table cellpadding="6" cellspacing="1" style="width:100%; margin:10px 0; padding:10px 0" border="0"><tr><th colspan="2" style="width:42%;border-bottom:1px solid #ccc;line-height:30px">Sản phẩm</th><th style="text-align:center;border-bottom:1px solid #ccc;">Số lượng</th><th style="text-align:center;;border-bottom:1px solid #ccc;">Đơn giá</th><th style="text-align:center;border-bottom:1px solid #ccc;">Thành tiền</th></tr>';
                foreach ($this->cart->contents() as $row) {
                    $id       = $row['id'];
                    $product  = $this->global_function->get_row_object(array(
                        'id' => $id
                    ), 'name', 'product');
                    $photo    = $row['images'];
                    $price    = $row['price'];
                    $link     = $row['link'];
                    $subtotal = $row['subtotal'];
                    $body .= '<tr><td width="12%" style="text-align:center;border-bottom:1px solid #ccc;padding:10px 0; border-left:1px solid #ccc">';
                    $body .= '<p><strong><img src=' . $photo . ' width="60" /></p><br>';
                    $body .= '</td><td style="text-align:center;border-bottom:1px solid #ccc;padding:10px 0; font-size:13px; text-transform: uppercase;"><a href="' . $link . '">' . $product->name . '</a></td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;">' . $row['qty'] . '</td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;">' . number_format($price, 0, ',', '.') . ' VNĐ' . '</td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;border-right:1px solid #ccc">' . number_format($subtotal, 0, ',', '.') . ' VNĐ' . '</td></tr>';
                }
                $body .= '<tr><td colspan="5" class="right" style="padding:10px 0;font-size:14px;"><strong>Tổng tiền: </strong>' . number_format($this->cart->total(), 0, ',', '.') . ' VNĐ' . '</td></tr>';
                $body .= '</table>';
                $this->cart->destroy();
                $data_order           = array();
                $data_order['mailto'] = 'dangthinh@iclick.vn';
                $data_order['mailcc'] = 'hientt@iclick.vn';
                $data_order['code']   = $sql['code'];
                $data_order['title']  = 'Đơn hàng ' . $sql['code'] . ' từ website ' . site_url();
                $params         = array(
                    'mailto' => $data_order['mailto'],
                    'mailcc' => $data_order['mailcc'],
                    'title' => $data_order['title'],
                    'content' => $body
                );
                $query          = http_build_query($params);
                $ch             = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://vinalo.com/loadvnh158/consendm/mailvnlH9536');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
                curl_setopt($ch, CURLOPT_VERBOSE, true);
                curl_setopt($ch, CURLOPT_STDERR, $out);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                curl_exec($ch);
                fclose($out);
                $debug = ob_get_clean();
                curl_close($ch);

                redirect(site_url('don-hang/' . $data_order['code']));
            }else{
                echo "payment failed";
            }
            
        }
    }

    function checkout_success($order_code)
    {
        $order = $this->global_function->get_row_object(array(
            'code' => $order_code
        ), '*', 'tpl_order');
        if (empty($order))
            redirect(site_url());
        $data['breadcrumb'] = '<a href="' . current_url() . '">Đặt hàng thành công</a>';
        $data['company']    = $this->global_function->get_row_object(array(
            'id' => 1
        ), 'phone, email', 'company');
        $data['order'] = $order;
        $data['order_code'] = $order_code;
        $data['list_product'] = json_decode($order->order_info);  

        $this->template->write('mod', 'checkout_success');
        $this->template->write('title', 'Đặt hàng thành công');
        $this->template->write_view('content', 'public/checkout_success', $data, TRUE);
        $this->template->render();
    }

    function load_district(){
        $province_id = $this->input->post('province_id');
        $district = $this->global_function->get_district($province_id);
        $html = '<option value="">Chon quận huyện</option>';
        if(!empty($district)){            
            foreach ($district as $row) {
                $html .= '<option value="'. $row->id .'">'. $row->type . ' ' . $row->name .'</option>';
            }
        }
        echo $html;
    }

    public function add_to_cart(){
        $id = $this->input->post('id');
        $product = $this->global_function->get_row_object(array('id' => $id), 'id, name, slug, price, price_sale, thumb, folder', 'product');
        if(empty($product)) redirect(site_url());
        
        $name = $product->slug;
        $link = site_url($product->slug);
        if($product->price > 0 && $product->price_sale > 0){
            $price = $product->price_sale;
        }
        if($product->price > 0 && $product->price_sale == 0){
            $price = $product->price;
        }
        if($product->price == 0){
            $price = $product->price;
        }
        $qty    = ($this->input->post('qty') != '') ? $this->input->post('qty') : 1;
        $images = base_url(_upload_product . $product->folder . '/' . $product->thumb);

        $flag = false;
        foreach ($this->cart->contents() as $row) {
            if ($row['id'] == $id) {
                $rid = $row['rowid'];
                $qty = $row['qty'] + $qty;
                $flag = true;
                break;
            }
        }

        if (!$flag) {
            $data = array(
                "id" => $id,
                "name" => $name ,
                "qty" => $qty,
                "price" => $price,
                "link" => $link,
                "images" => $images
            );
            $this->cart->insert($data);
            echo "ok";
        } else {
            $data = array(
                'rowid' => $rid,
                'qty' => $qty
            );
            $this->cart->update($data);
            echo "ok";
        }
    }

    function addcart() {
        $id = $this->input->post('id');

        $gia = "123";
        $sl = $this->input->post('number');
        $fag = false;
        foreach ($this->cart->contents() as $row) {
            if ($row['id'] == $id) {

                $rid = $row['rowid'];
                $qty = $row['qty'] + $sl;
                $fag = true;
                break;
            }
        }
        if (!$fag) {
            //echo "b";die;
            $data = array(
                "id" => $id,
                "name" => "sp1",
                "qty" => $sl,
                "price" => $gia,
            );
            $this->cart->insert($data);
            echo "insert";
        } else {
            $data = array(
                'rowid' => $rid,
                'qty' => $qty
            );
            $this->cart->update($data);
            echo "up";
        }
    }

    //====================== Update====================
    function update_cart() {
        $rowid = $this->input->post('rowid');
        $qty = $this->input->post('qty');
        $data = array(
            'rowid' => $rowid,
            'qty' => $qty
        );
        $this->cart->update($data);
    }

    //==================== Remove  ======================
    function remove_cart() {
        $rowid = $this->input->post('rowid');
        $data = array(
            'rowid' => $rowid,
            'qty' => 0
        );
        $this->cart->update($data);
    }

    //============ Remove All============
    function remove_all_cart() {
        $this->cart->destroy();
        redirect(site_url('gio-hang'));
    }

    public function _validate_phone() {
        $str = trim($this->input->post('phone'));
        if (!is_numeric($str)){
                $this->form_validation->set_message('_validate_phone', 'Số di động không đúng định dạng.');
                return false;
        } 
        $str_check = str_split(''.$str.'');
        if ($str_check[0]==0 || ($str_check[1]==1 && strlen($str)==11) || ($str_check[1]==9 && strlen($str)==10)){
                $this->form_validation->set_message('_validate_phone', '');
                return true;
        } else{
                $this->form_validation->set_message('_validate_phone', 'Số di động không đúng định dạng.');
                return false;
        }
    }

    public function _validate_province() {
        $province = trim($this->input->post('province_id'));
        if ($province == ''){
            $this->form_validation->set_message('_validate_province', 'Bạn chưa chọn Tỉnh/Thành.');
            return false;
        }   
    }

    public function _validate_district() {
        $district = trim($this->input->post('district_id'));
        if ($district == ''){
            $this->form_validation->set_message('_validate_district', 'Bạn chưa chọn Quận/Huyện.');
            return false;
        }   
    }

    function order_checkout(){

        if($this->input->post() != ''){
            $session_user = $this->session->userdata("user");
            $session_login = $this->session->userdata("user_log");
            $member_id = ($session_login) ? $session_user->id : 0;
            $total = $this->cart->total();
            if(count($this->cart->contents()) > 0){
                foreach ($this->cart->contents() as $row) {
                    $id = $row['id'];
                    $product = $this->global_function->get_row_object(array('id' => $id), 'name', 'product');
                    $photo = $row['images'];
                    $price = $row['price'];
                    
                    $array[] = array(
                        'id' => $id,
                        'product' => $product->name,
                        'images' => $row['images'],
                        'link' => $row['link'],
                        'price' => $price,
                        'qty' => $row['qty'],
                        'subtotal' => $row['subtotal'],
                    ); 

               }
                $order_info = json_encode($array);
            }  

            $sql['full_name'] = strip_tags($this->input->post('full_name'));
            $sql['email'] = $this->input->post('email');
            $sql['address'] = strip_tags($this->input->post('address'));
            $sql['phone'] = $this->input->post('phone');
            $sql['note'] = strip_tags($this->input->post('note'));
            $sql['order_status_id'] = 1;
            $sql['member_id'] = $member_id;
            $sql['province_id'] = $this->input->post('province_id');
            $sql['district_id'] = $this->input->post('district_id');  
            $sql['total'] = $total;  
            $sql['order_info'] = $order_info;
            $sql['code'] = random_string('numeric', 10);
            $sql['ip_address'] = $this->input->ip_address();
            $sql['user_agent'] = $this->input->user_agent();
            $sql['website'] = base_url();
            
            $this->db->insert("tpl_order", $sql);

            $district = $this->global_function->get_row_object(array('id' => $sql['district_id']), 'name', 'district');

            $district_name = !(empty($district)) ? $district->name : '';

            $province = $this->global_function->get_row_object(array('id' => $sql['province_id']), 'name', 'province');

            $province_name = !(empty($province)) ? $province->name : '';
          
            $body = '<table>';
            $body .= '<tr><th colspan="2">Đơn hàng '. $sql['code'] .' từ website '.site_url().'</th></tr><tr><th>Họ tên :</th><td>'.$sql['full_name'].'</td></tr><tr><th>Địa chỉ :</th><td>'.$sql['address'].'</td></tr><tr><th>Quận huyện :</th><td>'.$district_name.'</td></tr><tr><tr><th>Tỉnh thành :</th><td>'.$province_name.'</td></tr><tr><th>Điện thoại :</th><td>'.$sql['phone'].'</td></tr><tr><th>Email :</th><td>'.$sql['email'].'</td></tr><tr><th>Ghi chú :</th><td>'.$sql['note'].'</td></tr>';
            $body .= '</table>';

            $body .= '<table cellpadding="6" cellspacing="1" style="width:100%; margin:10px 0; padding:10px 0" border="0"><tr><th colspan="2" style="width:42%;border-bottom:1px solid #ccc;line-height:30px">Sản phẩm</th><th style="text-align:center;border-bottom:1px solid #ccc;">Số lượng</th><th style="text-align:center;;border-bottom:1px solid #ccc;">Đơn giá</th><th style="text-align:center;border-bottom:1px solid #ccc;">Thành tiền</th></tr>';

                foreach ($this->cart->contents() as $row){
                    $id = $row['id'];
                    $product = $this->global_function->get_row_object(array('id' => $id), 'name', 'product');
                    $photo = $row['images'];
                    $price = $row['price'];
                    $link = $row['link'];
                    $subtotal = $row['subtotal'];

                    $body .='<tr><td width="12%" style="text-align:center;border-bottom:1px solid #ccc;padding:10px 0; border-left:1px solid #ccc">';


                    $body .= '<p><strong><img src='. $photo .' width="60" /></p><br>';

                    $body .= '</td><td style="text-align:center;border-bottom:1px solid #ccc;padding:10px 0; font-size:13px; text-transform: uppercase;"><a href="'. $link .'">'.$product->name.'</a></td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;">'.$row['qty'].'</td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;">'.number_format($price,0,',','.').' VNĐ'.'</td>';
                    $body .= '<td style="text-align:center;border-bottom:1px solid #ccc;border-right:1px solid #ccc">'.number_format($subtotal,0,',','.').' VNĐ'.'</td></tr>';
                }
                $body .= '<tr><td colspan="5" class="right" style="padding:10px 0;font-size:14px;"><strong>Tổng tiền: </strong>'.number_format($this->cart->total(),0,',','.').' VNĐ'.'</td></tr>';
            $body .='</table>';

          
            $this->cart->destroy();

            $data = array();
            $data['mailto'] = 'dangthinh@iclick.vn';
            $data['mailcc'] = 'hientt@iclick.vn';
            $data['code'] = $sql['code'];
            $data['title'] = 'Đơn hàng '. $sql['code'] .' từ website ' . site_url();
            $data['content'] = $body;

            echo json_encode($data);
        }   
    }


    function call_back() {
        $product_id = $this->input->post('id');
        $phone = $this->input->post('phone');
        $link = $this->input->post('link');

        $data['product_id'] = $product_id;
        $data['phone'] = $phone;
        $data['link'] = $link;
        $data['weight'] = 1;
        $data['status'] = 1;

        $body = '<table>';
        $body .= '<tr><th colspan="2">Yêu cầu gọi lại từ số điện thoại '. $data['phone'] .' của website ' . site_url().'</th></tr><th colspan="2">&nbsp;</th></tr><tr><tr><th>Link sản phẩm :</th><td><a href="'.$data['link'].'">'.$data['link'].'</a></td></tr><tr><th>Click để gọi lại :</th><td><a href="tel:'.$data['phone'].'">'.$data['phone'].'</a></td></tr>';
        $body .= '</table>';

        $result = array();
        $result['mailto'] = 'dangthinh@iclick.vn';
        $result['mailcc'] = 'hientt@iclick.vn';
        $result['title'] = 'Yêu cầu gọi lại từ số điện thoại '. $data['phone'] .' của website ' . site_url();
        $result['content'] = $body;

        if ($this->db->insert('call_back', $data)) {
            $result['message'] = true;
        } else
            $result['message'] = false;

        echo json_encode($result);
    }

}
