<?php
    $this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager'), 'id' => $this->input->get('id')));
?>

<!-- Alternative Content Box Start -->

<div class="contentcontainer">

    <div class="headings altheading"><h2>Danh sách</h2></div>

    <div class="contentbox">
        
        <?php if(!empty($website)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Nguồn</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($website as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?website='.$cate->id; ?>" <?php if($this->input->get('website') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>    
        
        <?php if(!empty($category)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Danh mục</b></label>
            <select name="check_category" id="check_category" style="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($category as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?cat='.$cate->id; ?>" <?php if($this->input->get('cat') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($brand)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Thương hiệu</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($brand as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?brand='.$cate->id; ?>" <?php if($this->input->get('brand') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($product_type)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Loại sản phẩm</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($product_type as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?product_type='.$cate->id; ?>" <?php if($this->input->get('product_type') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($skin_type)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Loại da</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($skin_type as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?skin_type='.$cate->id; ?>" <?php if($this->input->get('skin_type') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($hair_type)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Loại tóc</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($hair_type as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?hair_type='.$cate->id; ?>" <?php if($this->input->get('hair_type') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($origin)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Xuất xứ</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($origin as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?origin='.$cate->id; ?>" <?php if($this->input->get('origin') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($action)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Chức năng</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($action as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?action='.$cate->id; ?>" <?php if($this->input->get('action') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($capacity)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Dung tích</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($capacity as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?capacity='.$cate->id; ?>" <?php if($this->input->get('capacity') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($weigh)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Dung tích</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($weigh as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?weigh='.$cate->id; ?>" <?php if($this->input->get('weigh') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php if(!empty($pill_number)){ ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Số lượng viên</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"> Tất cả</option>
                <?php foreach($pill_number as $cate){ ?>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?pill_number='.$cate->id; ?>" <?php if($this->input->get('pill_number') == $cate->id) echo 'selected';  ?>><?php echo $cate->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <?php /* ?>
        <div style="float: left;margin-right: 10px;max-width: 250px">
            <label><b>Trạng thái</b></label>
            <select tyle="width: 100%" onchange="window.open(this.value,'_self');">
                <option value="<?php echo site_url('admin/product') ?>"<?php if(empty($this->input->get('status'))) echo 'selected';  ?>> Tất cả</option>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?status=1'; ?>" <?php if(!empty($this->input->get('status')) && $this->input->get('status') == 1) echo 'selected';  ?>>Hiển thị</option>
                <option class="check_item bold" value="<?php echo site_url('admin/product').'?status=0'; ?>" <?php if(!empty($this->input->get('status')) && $this->input->get('status') == 0) echo 'selected';  ?>>Ẩn</option>
            </select>
        </div>
        <?php */ ?>
        <div style="clear: both;height: 10px"></div>
        <div>
            <label><b>Tìm kiếm</b></label>
            <form method="post" action="" name="frm_search" enctype="multipart/form-data">
                <input type="text" name="keyword" class="inputbox" value="<?php echo $this->input->post('keyword'); ?>" placeholder="Từ khóa...">
                <div class="clear" style="height: 10px"></div>
                <input type="button" name="ok" value="OK" class="btn">
            </form>
        </div>
        <div style="clear: both;height: 20px"></div>
        
        <form method="post" action="" enctype="multipart/form-data">
        
            <div class="extrabottom">
                <ul>
                    <li>
                        <img src="theme_admin/img/icons/add.png" alt="Add" /> 
                        <a style="text-decoration: none;" href="admin/product/add">Thêm mới</a>
                    </li>

                    <li> <img src="theme_admin/img/icons/icon_approve.png" alt="Approve" />
                        <input class="a_button_act a_show" name="show" style="cursor: pointer" type="submit"	value="Hiện danh sách đã chọn" />
                    </li>

                    <li> <img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" />
                        <input class="a_button_act a_hide" name="hide" style="cursor: pointer" type="submit"	value="Ẩn danh sách đã chọn" />
                    </li>

                    <li> <img src="theme_admin/img/icons/icon_delete.png" alt="Delete" />
                        <p style="display:none">
                            <input class="a_button_act a_delete" name="delete" style="cursor: pointer" type="submit"	value="Delete" />
                        </p>

                        <a style=" cursor:pointer" class="delete" onclick="Delete()">Delete</a> 
                    </li>
                </ul>            

            </div>
            <div style="clear:both; height: 10px"></div>    
            <table width="100%">

                <thead>
                    <tr>
                        <th width="3%">STT</th>
                        <th width="15%">Name</th>
                        <th>Bình luận</th>                          
                        <th>Hình ảnh</th> 
                        <th>Nổi bật</th> 
                        <th>Hot sale</th>                                        
                        <th>Mới</th>  
                        <th>Bán chạy</th>
                        <th>Khuyến mãi</th>
                        <th>Nên mua</th>
                        <th>Menu</th>
                        <th>Cao cấp</th> 
                        <th>Khuyến mãi hôm nay</th>
                        <th>Thứ tự</th>                    
                        <th width="8%">Action</th>
                        <th>Link</th> 
                        <th><input name="" type="checkbox" value="" id="checkboxall" /></th> 
                    </tr>

                </thead>

                <tbody>

                <?php
                $x = 0;
                ?>
                    <?php foreach ($item as $a) { 
                        $imgUrl = base_url(_upload_product . $a->folder . '/' . $a->photo);
                        $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=80&h=80&zc=1');
                    ?>
                        <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                            <td style="text-align:center"><?php echo $x + 1; ?></td>
                            <td><?php echo $a->name ?></td>
                            <td><a href="<?php echo site_url('admin/comment/product/'.$a->id); ?>" target="_blank"><span style="color:red"><?php echo $a->count_comment ?></span> BL mới</a></td>
                                                        
                            <td style="text-align:center"><img src="<?php echo $imgUrl; ?>" width="80" /></td>

                             <td style="text-align:center">
                                <?php if ($a->hot == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('hot', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('hot', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                             <td style="text-align:center">
                                <?php if ($a->hot_sale == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('hot_sale', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('hot_sale', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>
                               
                            <td style="text-align:center">
                                <?php if ($a->new == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('new', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('new', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <td style="text-align:center">
                                <?php if ($a->sale == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('sale', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('sale', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <td style="text-align:center">
                                <?php if ($a->promotion == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('promotion', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('promotion', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <td style="text-align:center">
                                <?php if ($a->should_purchase == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('should_purchase', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('should_purchase', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <td style="text-align:center">
                                <?php if ($a->menu_vertical == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('menu_vertical', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('menu_vertical', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <td style="text-align:center">
                                <?php if ($a->supper == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('supper', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('supper', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>

                            <td style="text-align:center">
                                <?php if ($a->new_sale == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('new_sale', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('new_sale', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>
                            </td>
                       
                            <td style="text-align:center"><?php echo $a->weight ?></td>
                            <td style="text-align:center">

                                <a href="admin/product/edit/<?php echo $a->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit"/></a>

                                <?php if ($a->status == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('status', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('status', 'product', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>

                                    <a onclick="if(!confirm('Xác nhận xóa?'))return false;" href="admin/product/delete/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Xóa"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete"/></a>
                            </td>
                            <td style="text-align:center">
                                <a href="<?php echo site_url($a->slug); ?>" target="_blank" style="text-decoration: underline;">View</a>
                            </td>
                            <td style="text-align:center">
                                <input type="checkbox" value="check_item[<?php echo $a->id ?>]" name="checkall[<?php echo $a->id ?>]" class="checkall"/>
                            </td>
                            

                        </tr>
                    <?php
                    $x++;
                    }
                    ?>

                </tbody>

            </table>

            <p style="color:#FF0000; font-weight:bold; text-align:center"><?php echo "Bạn chỉ được phép xóa các thư mục rỗng và không có dữ liệu" ?></p>
            <div style="clear: both;"></div>
            <p style="display:none">
                <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>
            </p>
        </form>

        <div style="clear: both"></div>
        <?php if($this->input->post('keyword') == '') echo $link ?>

    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[name=ok]').click(function(){
            var keyword = $('input[name=keyword]').val();
            if(keyword == ''){
                alert('Bạn chưa nhập từ khóa');
                $('input[name=keyword]').focus();
                return false;
            }

            document.frm_search.submit();
        })
    })
</script>


