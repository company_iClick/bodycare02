<style type="text/css">
    .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
    }
    /* IE 6 doesn't support max-height
    * we use height instead, but this forces the menu to always be this tall
    */
    * html .ui-autocomplete {
    height: 100px;
    }
    #files > div{padding: 0 0 20px}
    #files p{margin: 15px 0;}
    #files button{margin-top: 10px;}

    .progress {
        height: 20px;
        margin-top: 20px;
        margin-bottom: 20px;
        overflow: hidden;
        background-color: #f5f5f5;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
        box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
    }
    .progress-bar {
        float: left;
        width: 0;
        height: 100%;
        font-size: 12px;
        line-height: 20px;
        color: #fff;
        text-align: center;
        background-color: #428bca;
        -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
        box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
        -webkit-transition: width .6s ease;
        -o-transition: width .6s ease;
        transition: width .6s ease;
    }
    .progress-bar-success {
        background-color: #5cb85c;
    }
    .show{display: block;}
    .hide{display: none}
    .selectImage{margin-top: 10px;}
    .selectImage span, .selectImage input{vertical-align: middle;}
    .item_image a{color: #007eff}
    .div_img {
        float: left;
        width: 100%;
        padding-bottom: 10px
    }
    #add {
        float: right;
        width: 100%;
        text-align: right;
        cursor: pointer
    }
    .show_img_frame{float: left;margin-right: 10px;height: 165px}
    .input_img_frame{margin-bottom: 10px}
    .input_img_frame span, .input_img_frame input{vertical-align: middle;}
    .show_img {
        width: 130px;
        position: relative;
        float: left;
        border: solid 1px #F00;
        margin-bottom: 5px;line-height: 0
    }
    .delete_img {
        position: absolute;
        right: -5px;
        top: -5px
    }

    .bold {
        font-weight: bold
    }
</style>
<link rel="stylesheet" href="theme_admin/multyupload/css/jquery.fileupload.css">

<script>
    $(document).ready(function() {
        $('input[type=radio][name=gift_type]').change(function() {

            if ($(this).val() == '1') {
                $('#gift_type_one').css({'display', 'block'});
                $('#gift_type_two').css({'display', 'none'});
            }
            if ($(this).val() == '2') {
                $('#gift_type_one').css({'display', 'none'});
                $('#gift_type_two').css({'display', 'block'});
            }
        });
    });

  $(function() {

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }

        var list_product = <?php echo $list_product; ?>
        $( "#gift_list" )
          // don't navigate away from the field on tab when selecting an item
          .on( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
              event.preventDefault();
            }
          })
          .autocomplete({
            minLength: 0,
            source: function( request, response ) {
              // delegate back to autocomplete, but extract the last term
              response( $.ui.autocomplete.filter(
                list_product, extractLast( request.term ) ) );
            },
            focus: function() {
              // prevent value inserted on focus
              return false;
            },
            select: function( event, ui ) {
              var terms = split( this.value );
              var html = '<div style="padding:5px 7px;border:1px solid #ccc;position:relative;margin-bottom:10px">'+ ui.item.label +'</li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_gift"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="product_gift[]" value="'+ ui.item.id  +'"></div>';
              $('#append_input_gift').append(html);
              // remove the current input
              terms.pop();
              // add the selected item
              terms.push( ui.item.value );
              // add placeholder to get the comma-and-space at the end
              terms.push( "" );
              this.value = terms.join( ", " );
              

              return false;
            }
        });

        var tags = <?php echo $tags?>;

        $( "#tags" )
          // don't navigate away from the field on tab when selecting an item
          .on( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
              event.preventDefault();
            }
          })
          .autocomplete({
            minLength: 0,
            source: function( request, response ) {
              // delegate back to autocomplete, but extract the last term
              response( $.ui.autocomplete.filter(
                tags, extractLast( request.term ) ) );
            },
            focus: function() {
              // prevent value inserted on focus
              return false;
            },
            select: function( event, ui ) {
              var terms = split( this.value );
              var html = '<div style="float:left;margin-right:10px;padding:5px 7px;border:1px solid #ccc;position:relative">'+ ui.item.label +'</li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_tag"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="tags[]" value="'+ ui.item.id  +'"></div>';
              $('#append_input_tag').append(html);
              // remove the current input
              terms.pop();
              // add the selected item
              terms.push( ui.item.value );
              // add placeholder to get the comma-and-space at the end
              terms.push( "" );
              this.value = terms.join( ", " );
              

              return false;
            }
        });

  } );


    $(document).on('click', '.delete_tag', function() {
        if(!confirm('Bạn muốn xóa tag này?')) return false;

        $(this).parent().remove();
    });

    $(document).on('click', '.delete_gift', function() {
        if(!confirm('Bạn muốn xóa quà tặng này?')) return false;

        $(this).parent().remove();

        

    });

    


</script>

<?php
    $this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
    $category_id = (isset($item)) ? $item->category_id : $this->input->post('category_id');
    $brand_id = (isset($item)) ? $item->brand_id : $this->input->post('brand_id');
    $origin_id = (isset($item)) ? $item->origin_id : $this->input->post('origin_id');
    $website_id = (isset($item)) ? $item->website_id : $this->input->post('website_id');
    $name = (isset($item)) ? $item->name : $this->input->post('name');
    $slug = (isset($item)) ? $item->slug : $this->input->post('slug');
    $price = (isset($item)) ? $item->price : $this->input->post('price');
    $price_sale = (isset($item)) ? $item->price_sale : $this->input->post('price_sale');
    $model = (isset($item)) ? $item->model : $this->input->post('model');
    $specifications = (isset($item)) ? $item->specifications : $this->input->post('specifications');
    $youtube_code = (isset($item)) ? $item->youtube_code : $this->input->post('youtube_code');
    $summary = (isset($item)) ? $item->summary : $this->input->post('summary');
    $content = (isset($item)) ? $item->content : $this->input->post('content');
    $guide = (isset($item)) ? $item->guide : $this->input->post('guide');
    $element = (isset($item)) ? $item->element : $this->input->post('element');
    $gift_summary = (isset($item)) ? $item->gift_summary : $this->input->post('gift_summary');
    $photo = (isset($item)) ? $item->photo : NULL;
    $title = (isset($item)) ? $item->title : $this->input->post('title');
    $keywords = (isset($item)) ? $item->keywords : $this->input->post('keywords');
    $description = (isset($item)) ? $item->description : $this->input->post('description');
    $weight = (isset($item)) ? $item->weight : 1;
    $gift_type = (isset($item)) ? $item->gift_type : 1;

    $array_product_category = isset($array_product_category) ? $array_product_category : $this->input->post('category');
    $array_product_product_type = isset($array_product_product_type) ? $array_product_product_type : $this->input->post('product_type');
    $array_product_skin_type = isset($array_product_skin_type) ? $array_product_skin_type : $this->input->post('skin_type');
    $array_product_hair_type = isset($array_product_hair_type) ? $array_product_hair_type : $this->input->post('hair_type');
    $array_product_action = isset($array_product_action) ? $array_product_action : $this->input->post('action');
    $array_product_capacity = isset($array_product_capacity) ? $array_product_capacity : $this->input->post('capacity');
    $array_product_weigh = isset($array_product_weigh) ? $array_product_weigh : $this->input->post('weigh');
    $array_product_pill_number = isset($array_product_pill_number) ? $array_product_pill_number : $this->input->post('pill_number');

    $title_bar  = (isset($item)) ? 'Cập nhật' : 'Thêm mới';
?>

<form name="them" method="post" id="them" action=""	enctype="multipart/form-data">

    <div class="contentcontainer med left">

        <div class="headings altheading"><h2><?php echo $title_bar; ?></h2></div>

        <div class="contentbox">
            <p>
                <label for="textfield"><strong>Tên</strong></label>
                <input type="text" id="textfield" class="inputbox" name="name" value="<?php echo $name; ?>" OnkeyUp="add_alias_vn(this)" />
            </p>
            <?php echo form_error('name'); ?>
            <p>
                <label for="textfield"><strong>Đường dẫn</strong></label>
                <input type="text" class="inputbox" name="slug" value="<?php echo $slug; ?>" />
                <?php if(isset($item)){?><br /><span class="smltxt">Đường dẫn hiện tại: (<i style="color: #f00"><?php echo $slug; ?>)</i></span><?php } ?>
            </p>
            <?php echo form_error('slug'); ?>
            <p>
                <label for="textfield"><strong>Giá</strong></label>
                <input type="text" id="textfield" class="inputbox" name="price" value="<?php echo $price; ?>" />
                <br />
                <span class="smltxt">(VNĐ)</span>
            </p>
            <p>
                <label for="textfield"><strong>Giá khuyến mãi</strong></label>
                <input type="text" id="textfield" class="inputbox" name="price_sale" value="<?php echo $price_sale; ?>" />
                <br />
                <span class="smltxt">(VNĐ)</span>
            </p>
            <p>
                <label for="textfield"><strong>Mã sản phẩm</strong></label>
                <input type="text" id="textfield" class="inputbox" name="model" value="<?php echo $model; ?>" />
            </p>
            <p>
                <label for="textfield"><strong>Dung lượng</strong></label>
                <input type="text" id="textfield" class="inputbox" name="specifications" value="<?php echo $specifications; ?>" />
            </p>
            <p>
                <label for="textfield"><strong>Clip đánh giá(mã nhúng)</strong></label>
                <input type="text" id="textfield" class="inputbox" name="youtube_code" value="<?php echo $youtube_code; ?>" />
                <br /><span class="smltxt">https://www.youtube.com/watch?v=<i style="color: #f00">Fw1t0iD09Us</i></span>
            </p>
            <p>
                <label for="textfield"><strong>Mô tả</strong></label>
                <textarea name="summary" class="textarea inputbox" rows="5"><?php echo $summary; ?></textarea>
            </p>

            <p>
                <label for="textfield"><strong>Chi tiết sản phẩm</strong></label>
                <textarea name="content" class="textarea inputbox"><?php echo $content; ?></textarea>
            </p>

            <p>
                <label for="textfield"><strong>Hướng dẫn sử dụng</strong></label>
                <textarea name="guide" class="textarea inputbox"><?php echo $guide; ?></textarea>
            </p>

            <p>
                <label for="textfield"><strong>Thành phần</strong></label>
                <textarea name="element" class="textarea inputbox"><?php echo $element; ?></textarea>
            </p>   

            <div class="ui-widget">
              <label for="tags"><b>Chọn quà tặng:</b> </label>
              <div style="padding-bottom: 5px">
                <input type="radio" name="gift_type" value="1"<?php if($gift_type == 1 || $gift_type == '') echo ' checked'; ?>> Sản phẩm
                <input type="radio" name="gift_type" value="2"<?php if($gift_type == 2) echo ' checked'; ?>> Nội dung
              </div>

              <div id="gift_type_one"<?php if($gift_type == 2 || $gift_type == 0) echo ' style="display: none"'; ?>>
                  <textarea type="text" id="gift_list" rows="3" class="inputbox" placeholder="Gõ tên sản phẩm..." style="width:325px"></textarea>

                    <div id="append_input_gift" style="padding:10px 0">
                        <label for="tags">Sản phẩm đã chọn: </label>
                        <?php if(isset($list_product_gift) && !empty($list_product_gift)){
                            foreach ($list_product_gift as $key => $value) {
                                $id = $value->id;
                                $name = $value->name;
                        ?>
                        <div style="padding:5px 7px;border:1px solid #ccc;position:relative;margin-bottom: 10px"><?php echo $name; ?></li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_gift"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="product_gift[]" value="<?php echo $id; ?>"></div>
                        <?php }} ?>
                    </div>  
              </div>
              <div id="gift_type_two"<?php if($gift_type == 1 || $gift_type == 0) echo ' style="display: none"'; ?>>
                    <p>
                        <label for="textfield"><strong>Nội dung quà tặng</strong></label>
                        <textarea name="gift_summary" class="gift_summary" rows="4"><?php echo $gift_summary; ?></textarea>
                    </p>
              </div>
               
                <div style="clear:both; height:10px"></div>
            </div>
            
            <?php if(isset($item)){ ?>
            <p>
            
            <label for="smallbox"><strong>Hình đại diện: </strong></label>
     
            <div style="clear:both; height:10px"></div>
            <div class="show_img_frame">
                <div class="input_img_frame">
                    <input type="radio" name="select_img_main" value="<?php echo $photo; ?>" checked><span>Chọn làm hình đại diện</span>
                </div>
                <?php if (isset($item)) {?>
                <div class="show_img">                                       
                    <img width="130" src="uploads/product/<?php echo $item->folder . '/' . $item->thumb; ?>"/> 
                </div>
                <?php } ?>
            </div>
             <?php 

            if(!empty($product_photo)){
                foreach ($product_photo as $row) { ?>
                <div class="show_img_frame">
                    <div class="input_img_frame">
                        <input type="radio" name="select_img_main" value="<?php echo $row->photo; ?>"><span>Chọn làm hình đại diện</span>
                    </div>
                    <div class="show_img">
                        <img alt="Color" src="uploads/product-photo/<?php echo $row->thumb ?>" width="130">
                        <a href="admin/product/delete_img/<?php echo $item->id . "/" . $row->id ?>"
                           class="delete_img" title="Xóa" onclick="if(!confirm('Xác nhận xóa?'))return false;"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete"/></a>
                        
                    </div>
                </div>
        <?php }} ?>
            </p>
            <?php }?>            

        </div><!-- end contentbox -->



    </div>

    <div class="contentcontainer sml right">

        <div class="headings altheading">

            <h2 class="left">Sơ đồ</h2>

        </div>       

        <div class="contentbox">
            <div class="list">
                <p>
                    <label for="smallbox"><strong>Thứ Tự: </strong></label>
                    <input type="text" name="weight"  value="<?php echo $weight; ?>" style="width: 30px; text-align: center" class="inputbox">
                </p>

                <p>
                    <input type="radio" value="1" name="status" <?php if(isset($item) && $item->status==1){?> checked <?php }else echo 'checked';?>>Hiển thị
                    <input type="radio" value="0" name="status" <?php if(isset($item) && $item->status==0){?> checked <?php }?>>Ẩn
                </p>
            </div>
            <?php if(!empty($website)){ ?>
            <div class="list">
                <label for="textfield"><strong>Nguồn website</strong></label>
                <select name="website_id">
                    <option value="0">Chọn</option>
                    <?php foreach($website as $val){
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <option value="<?php echo $id; ?>"<?php if($website_id == $id) echo ' selected'; ?>><?php echo $name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php } ?>

            <?php if(!empty($category)){ ?>
            <div class="list">
                <label for="textfield"><strong>Danh mục</strong></label>
                <div style="max-height: 500px;overflow: auto;">
                    <?php foreach ($category as $val) {
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <div><input type="checkbox" name="category[]" value="<?php echo $id; ?>"<?php if(!empty($array_product_category) && in_array($id, $array_product_category)) echo ' checked'; ?>><?php echo $name; ?></div>
                    <?php } ?>
                </div>
                <?php echo form_error('category[]'); ?>               
            </div>
            <?php } ?>

            <?php if(!empty($product_type)){ ?>
            <div class="list">
                <label for="textfield"><strong>Loại sản phẩm</strong></label>
                <div style="max-height: 250px;overflow: auto;">
                    <?php foreach ($product_type as $val) { 
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <div><input type="checkbox" name="product_type[]" value="<?php echo $id; ?>"<?php if(!empty($array_product_product_type) && in_array($id, $array_product_product_type)) echo ' checked'; ?>><?php echo $name; ?></div>
                    <?php } ?>
                </div>           
            </div>
            <?php } ?>

            <?php if(!empty($skin_type)){ ?>
            <div class="list">
                <label for="textfield"><strong>Loại da</strong></label>
                <div style="max-height: 250px;overflow: auto;">
                    <?php foreach ($skin_type as $val) { 
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <div><input type="checkbox" name="skin_type[]" value="<?php echo $id; ?>"<?php if(!empty($array_product_skin_type) && in_array($id, $array_product_skin_type)) echo ' checked'; ?>><?php echo $name; ?></div>
                    <?php } ?>
                </div>           
            </div>
            <?php } ?>

            <?php if(!empty($hair_type)){ ?>
            <div class="list">
                <label for="textfield"><strong>Loại tóc</strong></label>
                <div style="max-height: 250px;overflow: auto;">
                    <?php foreach ($hair_type as $val) { 
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <div><input type="checkbox" name="hair_type[]" value="<?php echo $id; ?>"<?php if(!empty($array_product_hair_type) && in_array($id, $array_product_hair_type)) echo ' checked'; ?>><?php echo $name; ?></div>
                    <?php } ?>
                </div>           
            </div>
            <?php } ?>

            <?php if(!empty($action)){ ?>
            <div class="list">
                <label for="textfield"><strong>Chức năng</strong></label>
                <div style="max-height: 250px;overflow: auto;">
                    <?php foreach ($action as $val) { 
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <div><input type="checkbox" name="action[]" value="<?php echo $id; ?>"<?php if(!empty($array_product_action) && in_array($id, $array_product_action)) echo ' checked'; ?>><?php echo $name; ?></div>
                    <?php } ?>
                </div>           
            </div>
            <?php } ?>

            <?php if(!empty($capacity)){ ?>
            <div class="list">
                <label for="textfield"><strong>Dung tích</strong></label>
                <div style="max-height: 250px;overflow: auto;">
                    <?php foreach ($capacity as $val) { 
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <div><input type="checkbox" name="capacity[]" value="<?php echo $id; ?>"<?php if(!empty($array_product_capacity) && in_array($id, $array_product_capacity)) echo ' checked'; ?>><?php echo $name; ?></div>
                    <?php } ?>
                </div>           
            </div>
            <?php } ?>

            <?php if(!empty($weigh)){ ?>
            <div class="list">
                <label for="textfield"><strong>Trọng lượng</strong></label>
                <div style="max-height: 250px;overflow: auto;">
                    <?php foreach ($weigh as $val) { 
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <div><input type="checkbox" name="weigh[]" value="<?php echo $id; ?>"<?php if(!empty($array_product_weigh) && in_array($id, $array_product_weigh)) echo ' checked'; ?>><?php echo $name; ?></div>
                    <?php } ?>
                </div>           
            </div>
            <?php } ?>

            <?php if(!empty($pill_number)){ ?>
            <div class="list">
                <label for="textfield"><strong>Số lượng viên</strong></label>
                <div style="max-height: 250px;overflow: auto;">
                    <?php foreach ($pill_number as $val) { 
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <div><input type="checkbox" name="pill_number[]" value="<?php echo $id; ?>"<?php if(!empty($array_product_pill_number) && in_array($id, $array_product_pill_number)) echo ' checked'; ?>><?php echo $name; ?></div>
                    <?php } ?>
                </div>           
            </div>
            <?php } ?>

            <?php if(!empty($origin)){ ?>
            <div class="list">
                <label for="textfield"><strong>Xuất xứ</strong></label>
                <select name="origin_id">
                    <option value="0">Chọn xuất xứ</option>
                    <?php foreach($origin as $val){
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <option value="<?php echo $id; ?>"<?php if($origin_id == $id) echo ' selected'; ?>><?php echo $name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php } ?>
            
            <?php if(!empty($brand)){ ?>
            <div class="list">
                <label for="textfield"><strong>Thương hiệu</strong></label>
                <select name="brand_id">
                    <option value="">Chọn thương hiệu</option>
                    <?php foreach($brand as $val){
                        $id = $val->id;
                        $name = $val->name;
                    ?>
                    <option value="<?php echo $id; ?>"<?php if($brand_id == $id) echo ' selected'; ?>><?php echo $name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php } ?>
            <?php echo form_error('brand_id'); ?>

            <div class="list">
                <label for="textfield"><strong>Sản phẩm dành cho</strong></label>
                <input type="radio" value="1" name="male" <?php if(isset($item) && $item->male==1){?> checked <?php } ?>>Nam
                <input type="radio" value="1" name="female" <?php if(isset($item) && $item->female==1){?> checked <?php } ?>>Nữ
            </div>
            
            <div class="list">
                <label for="textfield"><strong>SEO</strong></label>

                <p>
                    <label for="textfield"><strong>Title</strong></label>
                    <input type="text" id="textfield" class="inputbox" name="title" value="<?php echo $title; ?>" />
                </p>
                
                <p>
                    <label for="textfield"><strong>Keywords</strong></label>
                    <textarea name="keywords" class="textarea" style="width:325px;border-radius: 4px;padding:10px" rows="4"><?php echo $keywords; ?></textarea>
                </p>
                
                <p>
                    <label for="textfield"><strong>Description</strong></label>
                    <textarea name="description" class="textarea" style="width:325px;border-radius: 4px;padding:10px" rows="4"><?php echo $description; ?></textarea>
                </p>
                
            </div>
          
            <hr />
            <div class="ui-widget">
              <label for="tags"><b>Chọn Tags:</b> </label>
              <input type="text" id="tags" class="inputbox" placeholder="Gõ tên Tag..." style="width:325px">
            </div>
            <div id="append_input_tag" style="padding:10px 0;max-height: 200px;overflow: auto;">
                <label for="tags">Tag đã chọn: </label>
                <?php if(isset($list_product_tag) && !empty($list_product_tag)){
                    foreach ($list_product_tag as $key => $value) {
                        $id = $value->id;
                        $name = $value->name;
                ?>
                <div style="float:left;margin-right:10px;padding:5px 7px;border:1px solid #ccc;position:relative"><?php echo $name; ?></li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_tag"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="tags[]" value="<?php echo $id; ?>"></div>
                <?php }} ?>
            </div>   
            <div style="clear:both; height:10px"></div>
            <div style="clear: both"></div>
            <input type="submit" value="<?php echo $title_bar; ?>" name="ok" class="btn" />
            <a class="btn" style="color: #fff" href="admin/product">Thoát</a>
        </div>

    </div>
    <input type="hidden" name="img_select_upload" value="">
    <input type="hidden" name="img_other_upload" value="">
</form>
<div style="clear: both"></div>
<div style=" padding: 0 15px;">
    <label for="smallbox"><strong>Thêm hình ảnh</strong></label>
    <div id="multyformupload">
    <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Chọn file...</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="files[]" multiple>
    </span>
    <br>
    <br>
    <!-- The global progress bar -->
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div id="files" class="files"></div>
    <br>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $("#add").click(function() {
            var img = '<div class="div_img"><input type="file" name="product_photo[]" style="margin-top:5px" /></div>';
            $("#other_img").append(img);
        });
    });
</script>

<script type="text/javascript">
    
    CKEDITOR.replace('summary', {height: 200, width: "98%", resize_enabled: true,
        filebrowserBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Images',
    });

    CKEDITOR.replace('content', {height: 300, width: "98%", resize_enabled: true,
        filebrowserBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Images',
    });
    
    CKEDITOR.replace('guide', {height: 300, width: "98%", resize_enabled: true,
        filebrowserBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Images',
    });

    CKEDITOR.replace('element', {height: 300, width: "98%", resize_enabled: true,
        filebrowserBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Images',
    });

    CKEDITOR.replace('gift_summary', {height: 200, width: "98%", resize_enabled: true,
        filebrowserBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Images',
    });

    function up() {

        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();

    }

</script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="theme_admin/multyupload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>

<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="theme_admin/multyupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="theme_admin/multyupload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="theme_admin/multyupload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="theme_admin/multyupload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="theme_admin/multyupload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="theme_admin/multyupload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="theme_admin/multyupload/jquery.fileupload-validate.js"></script>
<script>
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'multyupload/',
        uploadButton = $('<button/>')
            .addClass('btn btn-primary')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });

    
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 999000,
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 80,
        previewCrop: false
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files').addClass('item_image').css({'float':'left', 'width': '180px'});
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
            // select_image();
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            var inputSelectImage = '<div style="clear:both"></div><div class="selectImage"><span>Ảnh đại diện</span> <input type="radio" name="select_image" value="'+ file.name +'"></div><div class="selectImage"><span>Ảnh con</span> <input type="checkbox" name="select_image_other" value="'+ file.name +'"></div>';
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .append(inputSelectImage)
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
            select_image();
            //$('input[name=img_select_upload]').val(file.name);
            //$('.item_image').append(inputSelectImage)
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function select_image(){
    $('input:radio[name="select_image"]').change(
    function(){
        if ($(this).is(':checked')) {
            $('input[name=img_select_upload]').val($(this).val());
        }
    });

    var img_other_array = [];
    $('input[name="select_image_other"]').change(
    function(){
        if ($(this).is(':checked')) {
            img_other_array.push($(this).val());
            $('input[name=img_other_upload]').val(img_other_array);
        }else{
            var index = img_other_array.indexOf($(this).val());
            if (index > -1) {
                img_other_array.splice(index, 1);
                $('input[name=img_other_upload]').val(img_other_array);
            }
        }
    });
}

$(document).ready(function(){
    $('input[name=ok]').click(function(){
        var length = $('input[name=select_img_main]').length;
        var img_select_upload = $('input[name=img_select_upload]').val();
        if(length == 0 && img_select_upload == ''){
            alert('Bạn chưa chọn hình đại diện');
            return false;
        }else{
            document.main_form.submit();
        }
    })
})

</script>