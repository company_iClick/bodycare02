<div class="wrapper_full wrapper_full_border_1">
    <div class="wrapper_full_1">
        <div class="wrapper_full_4">

            <div class="all_thuonghieu clearfix">
                <p>Tât cả thương hiệu </p>
                <select class="select_th" name="category_brand" onchange="load_category_brand()">
                    <option value="">Danh mục sản phẩm</option>
                    <?php if(!empty($categories)){
                        foreach ($categories as $row) {
                            echo '<option value="'. $row->id .'">'. $row->name .'</option>';
                        }  
                    }?>
                
                </select>
                <div class="wp_text_all">
                    <?php foreach (range('A', 'Z') as $char) { 
                        $count_product = $this->a_brand->count_brand_width_character($char);
                        if($count_product > 0){
                    ?>
                    <a href="javascript:void(0)" data-char="<?php echo $char; ?>" class="brand_char"><?php echo $char; ?></a>
                    <?php }} ?>
                </div>

            </div>
            
            <?php if(!empty($list_brand)){?>  
            <div id="list_all_brand">
                <div class="list_all_thuonghieu">
                    <?php 
                        $i = 0;
                        foreach($list_brand as $row){
                        $name = $row->name;
                        $link = site_url($row->slug);
                        $photo = base_url(_upload_brand . $row->photo);
                        $class = (( $i + 1 ) % 6 == 0) ? ' child_thuonghieu1' : '';
                        $class2 = (( $i + 1 ) % 2 == 0) ? ' child_thuonghieu1_2' : '';
                        $class3 = (( $i + 1 ) % 3 == 0) ? ' child_thuonghieu1_3' : '';
                        $class4 = (( $i + 1 ) % 4 == 0) ? ' child_thuonghieu1_4' : '';
                        $class5 = (( $i + 1 ) % 5 == 0) ? ' child_thuonghieu1_5' : '';
                        echo sprintf('<div class="child_thuonghieu%s%s%s%s%s">
                                        <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                                        <a href="%s" title="%s" class="name_thuonghieu">%s</a>
                                    </div>', $class2,$class3,$class4,$class5, $class, $link, $name, $photo, $name, $link, $name, $name);
                        $i++;
                    }?>
                </div>
                <div class="ajax_loadding"></div>
            </div>
            <?php } ?>
        </div>
    </div>

</div>
<?php $this->load->view(BLOCK . 'policy'); ?>