<?php
    if(!empty($list_product)){
        foreach($list_product as $pr){
        $id = $pr->id;
        $name = $pr->name;
        $pr_name = $this->my_lib->cut_string($pr->name, 55);
        $link = site_url($pr->slug);
        $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
        $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
        $summary = $pr->summary;
        $class_layout_list = ($layout == 'list') ? ' box_product_v2_list' : '';
        $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
        $price = cms_price_v1($pr->price_sale, $pr->price);

        echo sprintf('<div class="box_product_v2%s">
                        <div class="box_product_v2_img">
                            <a href="%s" title="%s">
                                <img src="%s" alt="%s">
                            </a>
                        </div>
                        <div class="box_product_v2_name_element">
                            <div class="box_product_v2_name">
                                <a href="%s" title="%s">%s</a>
                                <a href="%s" title="%s" class="name_layout_list">%s</a>
                            </div>
                            <div class="box_product_v2_sum">%s</div>
                            <div class="box_product_v2_price clearfix">
                                <div class="box_product_v2_price_num">%s</div>
                                <div class="box_product_v2_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                            </div>
                        </div>%s%s
                    </div>', $class_layout_list, $link, $name, $photo, $name, $link, $name, $name, $link, $name, $name, $summary, $price, $id, $gift_type, $percent);
    }
}?>
<div class="clear"></div>
<?php echo $paging; ?>
<div class="ajax_loadding"></div>