<div class="wrapper_full">
    <div class="wrapper_full_1">
        <form class="form_pay" id="frm_order" method="post">
            <div class="left_pay">
                <div class="title_tt_1">Thông tin người mua</div>
                <div class="div_form_pay">

                    <?php if(!$session_login): ?><p class="tk_p_login">Bạn đã có tài khoản ? <a href="<?php echo site_url('dang-nhap'); ?>">Đăng nhập</a></p><?php endif; ?>
                    <div class="wp_input_div">
                        <p>Họ và tên(<span class="red">*</span>)</p>
                        <div class="input_frm">
                            <input type="text" name="full_name" value="<?php echo $full_name; ?>" class="input_pay" required>
                            <div id="errorOrderName"></div>
                        </div>
                    </div>

                    <div class="wp_input_div">
                        <p>Số điện thoại(<span class="red">*</span>)</p>
                        <div class="input_frm">
                            <input type="tel" name="phone" value="<?php echo $phone; ?>" class="input_pay" required pattern="0([0-9]{9,10})">
                             <div id="errorOrderPhone"></div>
                        </div>
                    </div>
                    <div class="wp_input_div">
                        <p>Email</p>
                        <div class="input_frm">
                            <input type="email" name="email" <?php echo $email; ?> class="input_pay">
                             <div id="errorOrderEmail"></div>
                        </div>
                    </div>

                    <div class="wp_input_div">
                        <p>Địa chỉ giao hàng(<span class="red">*</span>)</p>
                        <div class="input_frm">
                            <input type="text" name="address" value="<?php echo $address; ?>" class="input_pay" required>
                             <div id="errorOrderAdress"></div>
                        </div>
                    </div>

                    <div class="wp_input_div">
                        <p>Phương thức thanh toán(<span class="red">*</span>)</p>
                        <div class="wp_distric">
                            <div id="checkoutType">
                                <div class="checkoutTypeSelect">
                                    <input type="radio" name="pay" value="Thanh toán khi nhận hàng (COD)" data-type="1" checked><span>Thanh toán khi nhận hàng (COD)</span>
                                    <img src="themes/images/cod.jpg" alt="Ngân lượng">
                                </div>
                                <div class="checkoutTypeSelect">
                                    <input type="radio" name="pay" value="nganluong" data-type="2"><span>Thanh toán trực tuyến</span>
                                    <img src="themes/images/safe-pay-3.png" alt="Ngân lượng">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="wp_input_div">
                        <p></p>
                        <div class="wp_distric">
                            <div>
                                <select name="province_id" onchange="load_district()">
                                  <option value="">Chon tỉnh / thành</option>
                                  <?php if(!empty($province)){
                                    foreach ($province as $row) {
                                        $id = $row->id;
                                        $name = $row->name;
                                        $selected = ($province_id == $id) ? ' selected' :  '';
                                        echo '<option value="'. $id .'"'. $selected .'>'. $name .'</option>';
                                    }}?>
                                </select>
                            </div>
                            
                            <div>
                                <select name="district_id">
                                  <option value="">Chon quận huyện</option>
                                  <?php if(!empty($district)){
                                    foreach ($district as $row) {
                                        $id = $row->id;
                                        $name = $row->name;
                                        echo $selected = ($district_id == $id) ? ' selected' :  '';
                                        echo '<option value="'. $id .'"'. $selected .'>'. $name .'</option>';
                                    }}?>
                                </select>
                             </div>
                        </div>
                    </div>
                    <div class="wp_input_div">
                        <p>Ghi chú</p>
                        <textarea name="note"><?php echo $note; ?></textarea>
                    </div>

                </div>

            </div>
            <div class="right_pay">

                <div class="title_tt_1">Đơn hàng</div>

                <div class="div_form_pay1">
                    <?php 
                        $total = number_format($this->cart->total()) . ' VNĐ';
                        foreach ($this->cart->contents() as $row) { 
                        $rowid = $row['rowid'];
                        $id = $row['id'];
                        $product = $this->global_function->get_row_object(array('id' => $id), 'name, brand_id', 'product');
                       
                        $brand = $this->global_function->get_row_object(array('id' => $product->brand_id), 'name, slug', 'brand');
                        $price = number_format($row['price']) . ' VNĐ';
                        $subtotal = number_format($row['subtotal']) . ' VNĐ';
                        $qty = ($row['qty'] < 10) ? 0 . $row['qty'] : $row['qty'];
                        $link = $row['link'];
                        $images = $row['images'];
                    ?>
                    <div class="child_cart_detail">
                        <a href="<?php echo $link; ?>" class="img_a_cart" target="_blank"><img src="<?php echo $images; ?>"></a>
                        <span class="name_cart_span1">
                            <?php if(!empty($brand)){ ?>
                            <a href="<?php echo site_url($brand->slug) ?>" target="_blank"><?php echo $brand->name ?></a>
                            <?php } ?>
                            <p><a href="<?php echo $link; ?>" target="_blank"><?php echo $product->name; ?></a></p>
                            <b><?php echo $price; ?></b>
                        </span>
                        <span class="name_cart_span2">
                            <p>SL</p>
                            <b><?php echo $qty; ?></b>
                        </span>
                    </div>
                    <?php } ?>

                    <div class="child_cart_detail1">
                        <b>Tạm tính:</b>
                        <p><?php echo $total; ?></p>
                    </div>
                    <?php /* ?>
                    <div class="child_cart_detail1">
                        <b>Mã giảm giá :</b>
                        <span class="wp_input_dt">
                           <input type="" name="" class="input_12">
                           <input type="button" name="" class="input_13" value="Sử dụng">
                       </span>
                    </div>
                    <?php */ ?>

                    <div class="child_cart_detail2">
                        <div class="child_cart_detail3">
                            <b>Thành tiền :</b>
                            <p><?php echo $total; ?></p>
                        </div>

                        <div class="child_cart_detail3">
                            <i>(Đã bao gồm VAT)</i>
                        </div>

                        <div class="child_cart_detail4">
                            <input type="submit" name="ok" id="btn_order" class="input_14" value="Hoàn tất">
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>