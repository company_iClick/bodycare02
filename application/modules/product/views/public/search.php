<div class="container">
    <div class="row">
        <div class="col-xs-12">     
            <div class="col-xs-12"><?php echo $breadcrumbs; ?></div> 
            <div id="wrap_content" class="clearfix">
                <div class="col-xs-12"><div class="title"><h1>Tìm kiếm sản phẩm</h1></div></div>
            <?php
            if(!empty($list_products)){
                foreach ($list_products as $product){ 
                    $name = $product->name;
                    $link = site_url($product->slug);
                    $imgUrl = base_url(_upload_product . $product->folder . '/' . $product->thumb);
                    $price = ($product->price > 0) ? number_format($product->price, 0, ',', '.') . ' vnđ' : 'Liên hệ';

                    $imgHtml = sprintf('<div class="box_product_img"><a href="%s"><img src="%s" alt="%s" /></a></div>', $link, $imgUrl, $name);       
                    $priceHtml = sprintf('<span class="price"><label>Giá:</label> %s</span>', $price);
                    $nameHtml = sprintf('<h3><a href="%s" title="%s">%s</a></h3><a href="%s" class="view_detail">Chi tiết</a>', $link, $name, $name, $link);
                ?>
                <div class="col-xs-12 col-sm-2 col-md-3">
                
                    <div class="box_product clearfix">
                        <?php if($product->new == 1){ ?>
                        <div class="lable_new">Mới</div>
                        <?php } ?>
                        <?php echo $imgHtml; ?>
                        <div class="box_price_cart clearfix">
                            <?php echo $priceHtml; ?>
                            <button class="btn_cart">
                                <img src="<?php echo _images; ?>icon_cart.png" alt="" />
                            </button>
                        </div>
                        <?php echo $nameHtml; ?>
                    </div>
          
                </div>
            <?php  }}else echo '<div class="col-xs-12"><h4 class="error">Không tìm thấy kết quả nào.</h4></div>'; ?>
            <div class="clear"></div>
            <div class="col-xs-12"><?php echo $paging; ?></div>
            </div>
            
        </div>

        
        
        
    </div>
</div>

