<div class="wrapper_full">
    <div class="wrapper_full_1">
        
        <?php if(!empty($list_product)): ?>

            <div class="right_account_user3 mgrb_20">



                <p class="title_tt_3 title_tt_3_mb">Danh sách sản phẩm</p>

                <table class="tablle_donhang_history history_order_detail_tpl">

                    <thead>

                        <tr>

                            <th>STT</th>

                            <th>Sản phẩm</th>

                            <th>Hình ảnh</th>

                            <th>Số lượng</th>

                            <th>Đơn giá</th>

                            <th>Thành tiền</th>

                        </tr>

                      </thead>

                      <tbody>

                      <?php 



                        $i = 0;

                        foreach($list_product as $row){

                            $name = $row->product;

                            $images = $row->images;

                            $link = $row->link;

                            $qty = $row->qty;

                            $price = number_format($row->price, 0, ',', '.') . 'đ';

                            $subtotal = number_format($row->subtotal, 0, ',', '.') . 'đ';

                        ?>

                        <tr>

                            <td><?php echo $i + 1; ?></td>

                            <td><a href="<?php echo $link; ?>" target="_blank" class="text_cart_history"><?php echo $name; ?></a></td>

                            <td><a href="<?php echo $link; ?>" target="_blank" class="text_cart_history"><img src="<?php echo $images; ?>" alt="<?php echo $name; ?>" style="width: 80px"></a></td>

                            <td>

                                <p class="text_cart_history"><?php echo $qty; ?></p>

                            </td>

                            <td>

                                <p class="text_cart_history"><?php echo $price; ?></p>

                            </td>

                            <td>

                                <p class="text_cart_history"><?php echo $subtotal; ?></p>

                            </td>

                        </tr>

                      <?php $i++;} ?>

                    </tbody>

                </table>



                <div class="history_order_detail_mobile">

                    <?php foreach($list_product as $row){

                            $name = $row->product;

                            $images = $row->images;

                            $link = $row->link;

                            $qty = $row->qty;

                            $price = number_format($row->price, 0, ',', '.') . 'đ';

                            $subtotal = number_format($row->subtotal, 0, ',', '.') . 'đ'; ?>

                    <div class="child_cart_detail">

                        <a href="<?php echo $link; ?>" class="img_a_cart" target="_blank"><img src="<?php echo $images; ?>"></a>

                        <span class="name_cart_span1">

                            <p><a href="<?php echo $link; ?>" target="_blank"><?php echo $name; ?></a></p>

                            <b><?php echo $subtotal; ?></b>

                        </span>

                        <span class="name_cart_span2">

                            <p>SL</p>

                            <b><?php echo $qty; ?></b>

                        </span>

                    </div>

                    <?php } ?>

                </div>

            </div>

            <div class="order_detail_success">Tổng tiền: <span><?php echo number_format($order->total, 0, ',', '.') ?></span></div>
			<div class="box_checkout_success">
	            <p class="note-msg">
	                <?php echo 'Bạn đã đặt hàng với mã số đơn hàng <span class="yellow">'.$order_code.'</span> thành công ! Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất. Để được tư vấn trực tiếp vui lòng liên hệ '. $company->phone; ?>   
	            </p>
	        </div>
            <?php endif; ?>
    </div>
</div>