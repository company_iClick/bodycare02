<div class="wrapper_full wrapper_full_border_1">
    <div class="wrapper_full_1">
        <div class="wrapper_full_4">
            <?php $this->load->view(BLOCK . 'left_one'); ?>
            <div class="right_list_product">
                <div class="banner_pr_list1">
                    <div class="left_thuonghieu_img_1">
                        <img src="<?php echo base_url(_upload_brand . $brand->photo); ?>" alt="<?php echo $brand->name; ?>">
                    </div>
                    <div class="left_thuonghieu_img_2">
                        <h1 class="h1_1">THƯƠNG HIỆU <?php echo $brand->name; ?></h1>
                        <?php echo $brand->summary; ?>
                    </div>
                </div>
                <div class="cate_arrangement_pr_list clearfix">
                    <div class="cate_arrangement_pr_list_text" style="width: 100%;float: left">
                        <p style="float: left;width: 100%;padding-bottom: 10px">Sản phẩm hiệu <?php echo $brand->name; ?></p>
                        <b style="float:left"></b>
                    </div>
                </div>
                
                <div class="list_pr_ds">
                    <?php if(isset($category_brand)){
                        $title_show = (!empty($category_brand->title_show)) ? $category_brand->title_show : $category_brand->name;
                        $list_product = $category_brand->product;
                    ?>
                    <div class="list_thuonghieu_cate clearfix">
                        <div class="title_name_cate_2">
                            <p><?php echo $title_show; ?></p>
                            <i class="line_1"></i>
                        </div>
                        <div class="slider_cate_thuonghieu">
                            <?php 
                            foreach($list_product as $pr){
                                $id = $pr->id;
                                $name = $pr->name;
                                $pr_name = $this->my_lib->cut_string($pr->name, 55);
                                $link = site_url($pr->slug);
                                $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                                $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                                $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                                $price = cms_price_v1($pr->price_sale, $pr->price);

                                echo sprintf('<div class="box_product_v2">
                                                <div class="box_product_v2_img">
                                                    <a href="%s" title="%s">
                                                        <img src="%s" alt="%s">
                                                    </a>
                                                </div>
                                                
                                                <div class="box_product_v2_name">
                                                    <a href="%s" title="%s">%s</a>
                                                </div>
                                                <div class="box_product_v2_price clearfix">
                                                    <div class="box_product_v2_price_num">%s</div>
                                                    <div class="box_product_v2_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                                </div>%s%s
                                            </div>', $link, $name, $photo, $name, $link, $name, $name, $price, $id, $percent, $gift_type);
                            }?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if(!empty($categories)){ ?>
                        <?php foreach($categories as $row){ 
                            $title_show = (!empty($row->title_show)) ? $row->title_show : $row->name;
                            $list_product = $row->product;
                            if(!empty($list_product)){
                        ?>
                        <div class="list_thuonghieu_cate">
                            <div class="title_name_cate_2">
                                <p><?php echo $title_show; ?></p>
                                <i class="line_1"></i>
                             
                            </div>
                            <div class="slider_cate_thuonghieu">
                                <?php 
                            foreach($list_product as $pr){
                                $id = $pr->id;
                                $name = $pr->name;
                                $pr_name = $this->my_lib->cut_string($pr->name, 55);
                                $link = site_url($pr->slug);
                                $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                                $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                                $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                                $price = cms_price_v1($pr->price_sale, $pr->price);

                                echo sprintf('<div class="box_product_v2">
                                                <div class="box_product_v2_img">
                                                    <a href="%s" title="%s">
                                                        <img src="%s" alt="%s">
                                                    </a>
                                                </div>
                                                
                                                <div class="box_product_v2_name">
                                                    <a href="%s" title="%s">%s</a>
                                                </div>
                                                <div class="box_product_v2_price clearfix">
                                                    <div class="box_product_v2_price_num">%s</div>
                                                    <div class="box_product_v2_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                                </div>%s%s
                                            </div>', $link, $name, $photo, $name, $link, $name, $name, $price, $id, $percent, $gift_type);
                            }?>
                            </div>
                        </div>
                    <?php }}} ?>
            
                </div>
                
                
                
            </div>
        </div>
    </div>
</div>