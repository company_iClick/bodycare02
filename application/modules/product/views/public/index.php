<div class="clear h1"></div>

<div class="col_full fleft box-content">

    <div class="col-left-home fleft">

        <h1 class="title font-roboto text-primary relative ">

                    <span class="text fleft bold">

                        <?php echo $detail->name ?>

                    </span>

            <span class="line col_full m_auto absolute"></span>

        </h1>

        <div class="clear h1"></div>

        <div class="relative col_full fleft">

           <?php echo $detail->content?>

        </div>

    </div>

    <?php echo modules::run('home/home/block_right_employer', array("type" => "users")); ?>

</div>