<?php if(isset($price_search) && !empty($price_search)){?>
<div class="name_cate_left_1">
    <p>Giá</p>
    <b></b>
</div>

<ul class="ul_list_recieve_text_1">
        <?php foreach($price_search as $ps){
            $id = $ps->id;
            $name = $ps->name;
            $count_product = $ps->count_product;
            $id_input = 'input_price_' . $id;
        ?>
        <li>
            <input type="checkbox" name="input_price[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>" onchange="product_filter()"<?php if(!empty($input_price) && in_array($id, $input_price)) echo ' checked'; ?>>
            <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
            <b>(<?php echo $count_product; ?>)</b>
        </li>
        <?php } ?>
</ul>
<?php } ?>

<?php if(isset($brand) && $count_brand > 0){?>
<div class="list_th_scroll">
    <div class="name_cate_left_1 click_show_left">
        <p>Thuơng hiệu</p>
        <b></b>
        <i></i>
    </div>

    <div class="list_th_scroll_1 mCustomScrollbar">
        <ul class="list_th_scroll_ul ">
           
                <?php foreach($brand as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_brand_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_brand[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>" onchange="product_filter()"<?php if(!empty($input_brand) && in_array($id, $input_brand)) echo ' checked'; ?>>
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            
        </ul>
    </div>
</div>
<?php } ?>

<?php if(isset($product_type) && $count_product_type > 0){?>
<div class="name_cate_left_1">
    <p>Loại sản phẩm</p>
    <b></b>
</div>
<ul class="ul_list_recieve_text_1">
    <li>
        <input type="checkbox" name="input_product_type" id="input_product_type" class="css-checkbox-hotel" value="<?php echo $all_product_type_id; ?>" data-name="Tất cả" onchange="product_filter_all_product_type()"<?php if(!empty($input_product_type) && $all_product_type_id == $input_product_type) echo ' checked'; ?>>
        <label for="input_product_type" class="css-label_hotel radGroup4">Tất cả</label>
        <b>(<?php echo $count_product_type; ?>)</b>
    </li>
    <?php foreach($product_type as $st){
        $id = $st->id;
        $name = $st->name;
        $count_product = $st->count_product;
        $id_input = 'input_product_type_' . $id;
        if($count_product > 0){
    ?>
    <li>
        <input type="checkbox" name="input_product_type[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>" onchange="product_filter()"<?php if(!empty($input_product_type) && is_array($input_product_type) && in_array($id, $input_product_type)) echo ' checked'; ?>>
        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
        <b>(<?php echo $count_product; ?>)</b>
    </li>
    <?php }} ?>
</ul>

<?php }?>

<?php if(isset($skin_type) && $count_skin_type > 0){?>
<div class="name_cate_left_1">
    <p>Loại da</p>
    <b></b>
</div>
<ul class="ul_list_recieve_text_1">
    <li>
        <input type="checkbox" name="input_skin_type" id="input_skin_type" class="css-checkbox-hotel" value="<?php echo $all_skin_type_id; ?>" data-name="Tất cả" onchange="product_filter_all_skin_type()"<?php if(!empty($input_skin_type) && $all_skin_type_id == $input_skin_type) echo ' checked'; ?>>
        <label for="input_skin_type" class="css-label_hotel radGroup4">Tất cả</label>
        <b>(<?php echo $count_skin_type; ?>)</b>
    </li>
    <?php foreach($skin_type as $st){
        $id = $st->id;
        $name = $st->name;
        $count_product = $st->count_product;
        $id_input = 'input_skin_type_' . $id;
        if($count_product > 0){
    ?>
    <li>
        <input type="checkbox" name="input_skin_type[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>" onchange="product_filter()"<?php if(!empty($input_skin_type) && is_array($input_skin_type) && in_array($id, $input_skin_type)) echo ' checked'; ?>>
        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
        <b>(<?php echo $count_product; ?>)</b>
    </li>
    <?php }} ?>
</ul>

<?php }?>

<?php if(isset($hair_type) && $count_hair_type > 0){?>
<div class="name_cate_left_1">
    <p>Loại tóc</p>
    <b></b>
</div>
<ul class="ul_list_recieve_text_1">
    <li>
        <input type="checkbox" name="input_hair_type" id="input_hair_type" class="css-checkbox-hotel" value="<?php echo $all_hair_type_id; ?>" data-name="Tất cả" onchange="product_filter_all_hair_type()"<?php if(!empty($input_hair_type) && $all_hair_type_id == $input_hair_type) echo ' checked'; ?>>
        <label for="input_hair_type" class="css-label_hotel radGroup4">Tất cả</label>
        <b>(<?php echo $count_hair_type; ?>)</b>
    </li>
    <?php foreach($hair_type as $st){
        $id = $st->id;
        $name = $st->name;
        $count_product = $st->count_product;
        $id_input = 'input_hair_type_' . $id;
        if($count_product > 0){
    ?>
    <li>
        <input type="checkbox" name="input_hair_type[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(!empty($input_hair_type) && is_array($input_hair_type) && in_array($id, $input_hair_type)) echo ' checked'; ?>>
        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
        <b>(<?php echo $count_product; ?>)</b>
    </li>
    <?php }} ?>
</ul>          
<?php }?>

<?php if(isset($origin) && $count_origin > 0){?>
<div class="name_cate_left_1">
    <p>Xuất xứ</p>
    <b></b>
</div>
<ul class="ul_list_recieve_text_1">
    <li>
        <input type="checkbox" name="input_origin" id="input_origin" class="css-checkbox-hotel" value="<?php echo $all_origin_id; ?>" data-name="Tất cả" onchange="product_filter_all_origin()"<?php if(!empty($input_origin) && $all_origin_id == $input_origin) echo ' checked'; ?>>
        <label for="input_origin" class="css-label_hotel radGroup4">Tất cả</label>
        <b>(<?php echo $count_origin; ?>)</b>
    </li>
    <?php foreach($origin as $st){
        $id = $st->id;
        $name = $st->name;
        $count_product = $st->count_product;
        $id_input = 'input_origin_' . $id;
        if($count_product > 0){
    ?>
    <li>
        <input type="checkbox" name="input_origin[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(!empty($input_origin) && is_array($input_origin) && in_array($id, $input_origin)) echo ' checked'; ?>>
        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
        <b>(<?php echo $count_product; ?>)</b>
    </li>
    <?php }} ?>
</ul>         
<?php }?>

<?php if(isset($action) && $count_action > 0){?>
<div class="name_cate_left_1">
    <p>Chức năng</p>
    <b></b>
</div>
<ul class="ul_list_recieve_text_1">
    <li>
        <input type="checkbox" name="input_action" id="input_action" class="css-checkbox-hotel" value="<?php echo $all_action_id; ?>" data-name="Tất cả" onchange="product_filter_all_action()"<?php if(!empty($input_action) && $all_action_id == $input_action) echo ' checked'; ?>>
        <label for="input_action" class="css-label_hotel radGroup4">Tất cả</label>
        <b>(<?php echo $count_action; ?>)</b>
    </li>
    <?php foreach($action as $st){
        $id = $st->id;
        $name = $st->name;
        $count_product = $st->count_product;
        $id_input = 'input_action_' . $id;
        if($count_product > 0){
    ?>
    <li>
        <input type="checkbox" name="input_action[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(!empty($input_action) && is_array($input_action) && in_array($id, $input_action)) echo ' checked'; ?>>
        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
        <b>(<?php echo $count_product; ?>)</b>
    </li>
    <?php }} ?>
</ul>
<?php }?>

<?php if(isset($capacity) && $count_capacity > 0){?>
<div class="name_cate_left_1 click_show_left ">
    <p>Dung tích</p>
    <b></b>
    <i></i>
</div>
 <div class="list_th_scroll_1 mCustomScrollbar">
    <ul class="list_th_scroll_ul">
        <li>
            <input type="checkbox" name="input_capacity" id="input_capacity" class="css-checkbox-hotel" value="<?php echo $all_capacity_id; ?>" data-name="Tất cả" onchange="product_filter_all_capacity()"<?php if(!empty($input_capacity) && $all_capacity_id == $input_capacity) echo ' checked'; ?>>
            <label for="input_capacity" class="css-label_hotel radGroup4">Tất cả</label>
            <b>(<?php echo $count_capacity; ?>)</b>
        </li>
        <?php foreach($capacity as $st){
            $id = $st->id;
            $name = $st->name;
            $count_product = $st->count_product;
            $id_input = 'input_capacity_' . $id;
            if($count_product > 0){
        ?>
        <li>
            <input type="checkbox" name="input_capacity[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(!empty($input_capacity) && is_array($input_capacity) && in_array($id, $input_capacity)) echo ' checked'; ?>>
            <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
            <b>(<?php echo $count_product; ?>)</b>
        </li>
        <?php }} ?>
    </ul>

</div>
<?php }?>
<?php if(isset($weigh) && $count_weigh > 0){?>
<div>
<div class="name_cate_left_1 click_show_left">
    <p>Trọng lượng</p>
    <b></b>
      <i></i>
</div>
<div class="list_th_scroll_1 mCustomScrollbar">
    <ul class="list_th_scroll_ul">
        <li>
            <input type="checkbox" name="input_weigh" id="input_weigh" class="css-checkbox-hotel" value="<?php echo $all_weigh_id; ?>" data-name="Tất cả" onchange="product_filter_all_weigh()"<?php if(!empty($input_weigh) && $all_weigh_id == $input_weigh) echo ' checked'; ?>>
            <label for="input_weigh" class="css-label_hotel radGroup4">Tất cả</label>
            <b>(<?php echo $count_weigh; ?>)</b>
        </li>
        <?php foreach($weigh as $st){
            $id = $st->id;
            $name = $st->name;
            $count_product = $st->count_product;
            $id_input = 'input_weigh_' . $id;
            if($count_product > 0){
        ?>
        <li>
            <input type="checkbox" name="input_weigh[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(!empty($input_weigh) && is_array($input_weigh) && in_array($id, $input_weigh)) echo ' checked'; ?>>
            <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
            <b>(<?php echo $count_product; ?>)</b>
        </li>
        <?php }} ?>
    </ul>

</div>
</div>
<?php }?>

<?php if(isset($pill_number) && !empty($pill_number)){?>
<div class="name_cate_left_1">
    <p>Số lượng viên</p>
    <b></b>
</div>
<ul class="ul_list_recieve_text_1">
    <li>
        <input type="checkbox" name="input_pill_number" id="input_pill_number" class="css-checkbox-hotel" value="<?php echo $all_pill_number_id; ?>" data-name="Tất cả" onchange="product_filter_all_pill_number()"<?php if(!empty($input_pill_number) && $all_pill_number_id == $input_pill_number) echo ' checked'; ?>>
        <label for="input_pill_number" class="css-label_hotel radGroup4">Tất cả</label>
        <b>(<?php echo $count_pill_number; ?>)</b>
    </li>
    <?php foreach($pill_number as $st){
        $id = $st->id;
        $name = $st->name;
        $count_product = $st->count_product;
        $id_input = 'input_pill_number_' . $id;
        if($count_product > 0){
    ?>
    <li>
        <input type="checkbox" name="input_pill_number[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(!empty($input_pill_number) && is_array($input_pill_number) && in_array($id, $input_pill_number)) echo ' checked'; ?>>
        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
        <b>(<?php echo $count_product; ?>)</b>
    </li>
    <?php }} ?>
</ul>
<?php }?>