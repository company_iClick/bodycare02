<?php 
    $photo = base_url(_upload_product . $detail->folder . '/' . $detail->photo);
    $thumb = base_url(_upload_product . $detail->folder . '/' . $detail->thumb);
    $percent = percen_calculate_v1($detail->price_sale, $detail->price, array('class' => 'price_percent_detail'));
?>
<div class="wrapper_full">
    <div class="wrapper_full_1">
        <div class="left_detail_pr">

            <div class="img_detail_name clearfix">
                <div class="img_detail_name_1">
                  
                    <div class="zoom-small-image">
                        <?php echo $percent; ?>
                        <a href='<?php echo $photo; ?>' class = 'cloud-zoom' id='zoom1' rel="adjustX:10, adjustY:-4" onclick="return false">   
                            <img src="<?php echo $photo; ?>" alt="<?php echo $detail->name; ?>" />
                        </a>
                    </div>
             
                    <div id="carousel" class="slider_carousel">
                        <div class="slide">
                            <a href="<?php echo $photo; ?>" class="cloud-zoom-gallery active" title="Red" rel="useZoom: 'zoom1', smallImage: '<?php echo $photo; ?>'" onclick="return false">
                                <img src="<?php echo $thumb; ?>">
                            </a> 
                        </div>
                        <?php if(!empty($product_photos)){
                            foreach($product_photos as $p){
                                $p_photo = base_url(_upload_product_photo . $p->photo);
                                $p_thumb = base_url(_upload_product_photo . $p->thumb);
                        ?>
                         <div class="slide">
                                <a href='<?php echo $p_photo; ?>' class='cloud-zoom-gallery' title='Red' rel="useZoom: 'zoom1', smallImage: '<?php echo $p_photo; ?>'" onclick="return false">
                                    <img src="<?php echo $p_thumb; ?>">
                                </a>
                            
                        </div>
                        <?php }} ?>
                    </div>
                    <?php if(!empty($detail->youtube_code)){ ?>
                    <div class="wp_danhgia">
                        <a class="various fancybox.iframe" href="http://www.youtube.com/embed/<?php echo $detail->youtube_code; ?>?autoplay=1">
                            <p>Xem Clip đánh giá sản phẩm này</p><span class="icon-xemvideo"></span></a>
                    </div>
                    <?php } ?>
                </div>
                <div class="img_detail_name_2">
                    <h1 class="h2_name"><?php echo $detail->name; ?></h1>
                    <div class="brand_name_detail clearfix">
                        <div class="brand_name_detail_elm">Thương hiệu: <a href="<?php echo site_url($brand->slug); ?>" title="<?php echo $brand->name; ?>"><?php echo $brand->name; ?></a></div>
                        <?php if(!empty($origin)){?>
                        <div class="brand_name_detail_elm">Xuất xứ: <a href="<?php echo site_url($origin->slug); ?>" title="<?php echo $origin->name; ?>"><?php echo $origin->name; ?></a></div>
                        <?php } ?>
                        <?php if(!empty($skin_type)){?>
                        <div class="brand_name_detail_elm">Loại da: 
                            <?php 
                                $i=0;
                                foreach($skin_type as $row){
                                    $link = site_url($row->slug);
                                    echo '<a href="'. $link .'">'. $row->name .'</a>';
                                    if(count($skin_type) - 1 > $i) echo ', ';
                                    $i++;    
                                }?>
                        </div>
                        <?php } ?>
                        <?php if(!empty($hair_type)){?>
                        <div class="brand_name_detail_elm">Loại tóc: 
                            <?php 
                                $i=0;
                                foreach($hair_type as $row){
                                    $link = site_url($row->slug);
                                    echo '<a href="'. $link .'">'. $row->name .'</a>';
                                    if(count($hair_type) - 1 > $i) echo ', ';
                                    $i++;    
                                }?>
                        </div>
                        <?php } ?>
                        <?php if(!empty($capacity)){?>
                        <div class="brand_name_detail_elm">Dung tích:
                            <?php 
                                $i=0;
                                foreach($capacity as $row){
                                    $link = site_url($row->slug);
                                    echo '<a href="'. $link .'">'. $row->name .'</a>';
                                    if(count($capacity) - 1 > $i) echo ', ';
                                    $i++;    
                                }?>
                        </div>
                        <?php } ?>
                        <?php if(!empty($weigh)){?>
                        <div class="brand_name_detail_elm">Trọng lượng:
                            <?php 
                                $i=0;
                                foreach($weigh as $row){
                                    $link = site_url($row->slug);
                                    echo '<a href="'. $link .'">'. $row->name .'</a>';
                                    if(count($weigh) - 1 > $i) echo ', ';
                                    $i++;    
                                }
                            ?>
                        </div>
                        <?php } ?>
                        <?php if(!empty($product_type)){?>
                        <div class="brand_name_detail_elm">Loại sản phẩm:
                            <?php 
                                $i=0;
                                foreach($product_type as $row){
                                    $link = site_url($row->slug);
                                    echo '<a href="'. $link .'">'. $row->name .'</a>';
                                    if(count($product_type) - 1 > $i) echo ', ';
                                    $i++;    
                                }
                            ?>
                        </div>
                        <?php } ?>
                        <?php 
                            if($detail->male == 1 && $detail->female == 1){
                                $intended_for = 'Nam, nữ';

                            }else if($detail->male == 1 && $detail->female == 0){
                                $intended_for = '<a href="'. site_url('danh-cho-nam') .'">Nam</a>';
                            }else if($detail->male == 0 && $detail->female == 1){
                                $intended_for = '<a href="'. site_url('danh-cho-nu') .'">Nữ</a>';
                            }else $intended_for = '';
                        ?>
                        <?php if($intended_for != ''){?>
                        <div class="brand_name_detail_elm">Dành cho:
                            <?php echo $intended_for; ?>
                        </div>
                        <?php } ?>
                        <?php if(!empty($action)){?>
                        <div class="brand_name_detail_elm">Chức năng:
                            <?php 
                                $i=0;
                                foreach($action as $row){
                                    $link = site_url($row->slug);
                                    echo '<a href="'. $link .'">'. $row->name .'</a>';
                                    if(count($action) - 1 > $i) echo ', ';
                                    $i++;    
                                }
                            ?>
                        </div>
                        <?php } ?>
                        <?php if(!empty($pill_number)){?>
                        <div class="brand_name_detail_elm">Số lượng viên:
                            <?php 
                                $i=0;
                                foreach($pill_number as $row){
                                    $link = site_url($row->slug);
                                    echo '<a href="'. $link .'">'. $row->name .'</a>';
                                    if(count($pill_number) - 1 > $i) echo ', ';
                                    $i++;    
                                }
                            ?>
                        </div>
                        <?php } ?>
                        <?php if(!empty($detail->sespecifications)){?>
                        <div class="brand_name_detail_elm">Dung lượng:
                            <?php echo $detail->sespecifications; ?>
                        </div>
                        <?php } ?>
                    </div>
                    <?php echo $detail->summary; ?>


                    <div class="price_wp_xx">

                        <div class="price_wp_xx_left">
                        <?php if($detail->price > 0 && $detail->price_sale > 0){
                        echo sprintf('<div class="price_old_detail clearfix"><b>Giá thị trường:</b><p>%s</p></div>
                                        <div class="price_news_detail clearfix"><b>Giá</b><p>%s</p></div>
                                        <div class="saving_detail clearfix"> <b>Tiết kiệm:</b><p>%s (%s)</p> </div>', number_format($detail->price) . ' VNĐ', number_format($detail->price_sale) . ' VNĐ', number_format($detail->price - $detail->price_sale, 0, ',', '.'), round(100 - (($detail->price_sale / $detail->price) * 100)). '%');
                        } ?>

                        <?php if($detail->price > 0 && $detail->price_sale == 0){
                        echo sprintf('<div class="price_news_detail clearfix" style="padding-top:10px"><b>Giá</b><p>%s</p></div>', number_format($detail->price) . ' VNĐ');
                        } ?>

                        <?php if($detail->price == 0){
                        echo sprintf('<div class="price_old_detail clearfix"><b>Giá</b><p>%s</p></div>', 'Liên hệ');
                        } ?> 

                        <div class="wp_text_number clearfix">
                                <p class="text_number_1">Số lượng:</p>
                                <div class="number_cart1">
                                    <span class="icon-soluongtru img_click_minus"></span>
                                    <input type="text" name="qty" class="number_cart_input" value="1">
                                    <span class="icon-soluongcong img_click_plus"></span>
                                </div>

                            </div>                          
                                                     
                        </div>
                        <div class="price_wp_xx_right">
                            
                            <div class="wp_quatang">
                                <div class="wp_quatang1"><i class="icon-nhieuquatang"></i><span>Quà tặng kèm</span></div>
                                <div class="gift_summary"><?php if(!empty($detail->gift_summary)) echo $detail->gift_summary; else echo $company->gift_summary; ?></div>
                            </div>
                            
                        </div>

                        <div class="clear"></div>
                        <div class="price_wp_xx_cart clearfix">
                            
                            <div class="card_by">
                                <a href="javascript:void(0)" class="card_by1" onclick="add_to_cart_pr_detail(<?php echo $detail->id; ?>)">
                                    <i class="icon-muahang"></i><span>mua ngay</span>
                                </a>
                            </div>
                            <div class="yeucau_call">
                                <form id="form_call" method="post">
                                    <input type="text" class="input_Call" name="callback_phone" placeholder="Yêu cầu gọi lại">
                                    <input type="submit" class="submit_Call" id="submit_callback" value="Gửi" data-id="<?php echo $detail->id; ?>">
                                    <div class="clear"></div>
                                    <div id="error_callback"></div>
                                </form>
                            </div>
                        </div>
                        <div class="note_logistic clearfix">
                            <div class="logo_logistic">
                                <span class="icon-logistics"></span>
                            </div>
                            <div class="note_logistic_info">Sản phẩm được giao và thu tiền tận nơi trong vòng 24 giờ đến các <span>quận/huyện</span> tại TP Hồ Chí Minh, đối với đơn hàng ở tỉnh tùy thuộc vào khoảng cách địa lý, <a href="http://bodycare.vn">Bodycare.vn</a> luôn ưu tiên giao hàng nhanh nhất có thể. Các trường hợp cần giao gấp hãy liên hệ trực tiếp ĐT: <a href="tel:090.77.999.88">090.77.999.88</a> để được giao hàng ngay. <a href="<?php echo site_url('huong-dan-mua-hang'); ?>" target="_blank">Chi tiết</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="container">
                <ul class="faq">
                    <li class="q rotate1" data-id="a"><img src="themes/images/row_2_blue.png" class="rotate"> &nbsp; Chi tiết sản phẩm</li>
                    <li class="a" id="a" style="display: block;">
                        <div class="faq_content">
                            <?php echo $detail->content; ?>
                            <?php if(!empty($tags)){?>
                            <div class="tags-node">
                                <label>Tags: </label>
                                <div class="list-tag">
                                <?php $i=0; foreach($tags as $t){
                                    $name = $t->name;
                                    $link= site_url($t->slug);
                                    $join = ($i < count($tags) - 1) ? ',' : '';
                                    echo sprintf('<span><a href="%s" title="%s">%s</a>%s</span>', $link, $name, $name, $join)
                                ?>
                                <?php $i++;} ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </li>

                    <li class="q rotate1" data-id="a1"><img src="themes/images/row_2_blue.png" class="rotate">&nbsp; Hướng dẫn sử dụng</li>
                    <li class="a" id="a1" style="display: block;">
                        <div class="faq_content"><?php echo $detail->guide; ?></div>
                    </li>

                    <li class="q rotate1" data-id="a2"><img src="themes/images/row_2_blue.png" class="rotate">&nbsp; Thành phần</li>
                    <li class="a" id="a2" style="display: block;">
                        <div class="faq_content"><?php echo $detail->element; ?></div>
                    </li>
                    
                    <li class="q rotate1" data-id="a3"><img src="themes/images/row_2_blue.png" class="rotate"> &nbsp; Thương hiệu <?php echo $brand->name; ?></li>
                    <li class="a" id="a3" style="display: block;">
                        <div class="faq_content"><?php echo $brand->summary; ?></div>
                    </li>


                </ul>
            </div>


            <div class="container_cart">
                <a href="javascript:void(0)" onclick="add_to_cart_pr_detail(<?php echo $detail->id; ?>)">mua ngay</a>
            </div>

            <div class="comment_danhgia_v1">
                <i class="icon-hoidap"></i><span>Hỏi, đáp về sản phẩm</span>
            </div>

            <div class="list_comment">
                <form id="form_send_comment" method="post">
                    <div class="form_send_comment_box">
                        <input type="text" name="comment_name" value="<?php echo (!empty($session_user)) ? $session_user->full_name : ''; ?>" placeholder="Họ tên(bắt buộc)">
                        <div id="errorCommentName"></div>
                    </div>
                    <div class="form_send_comment_box">
                        <textarea name="comment_content" placeholder="Viết bình luận"></textarea>
                        <div id="errorCommentContent"></div>
                    </div>
                    <div id="mgs_comment" class="mgs_comment"></div>
                    <input type="button" class="submit_send_comment" value="Gửi" onclick="send_comment(<?php echo $detail->id; ?>)">
                </form>

                <?php if(!empty($array_comment)): ?>
                <div class="list_comment1">
                    <?php foreach($array_comment as $row){ 
                        $parent = $row['parent'];
                        $child = (isset($row['child'])) ? array_reverse($row['child']) : '';
                        $comment_id = $row['parent']['id'];
                    ?>
                    <div class="child_comment1">
                        <div class="avatar_comment"><img src="themes/images/img_ac.png"></div>
                        <span class="name_faq">
                            <b><?php echo $parent['name']; ?></b>
                            <p><?php echo $parent['content']; ?> <b class="click_comment_b" onclick="toogle_reaply(<?php echo $comment_id; ?>)"><i class="icon-traloi"></i>&nbsp; Trả lời </b> </p>
                             <form class="form_send_comment1 css_none" id="form_send_comment_<?php echo $comment_id; ?>">
                                <div class="form_send_comment_box">
                                    <input type="text" name="comment_name" placeholder="Họ tên(bắt buộc)" value="<?php echo (!empty($session_user)) ? $session_user->full_name : ''; ?>">
                                    <div id="errorCommentName<?php echo $comment_id; ?>"></div>
                                </div>
                                <div class="form_send_comment_box">
                                    <textarea name="comment_content" id="comment_content_<?php echo $comment_id; ?>" placeholder="Viết bình luận"></textarea>
                                    <div id="errorCommentContent<?php echo $comment_id; ?>"></div>
                                </div>
                                <div class="form_send_comment_box">
                                    <div class="mgs_comment" id="mgs_comment_<?php echo $comment_id; ?>"></div>
                                    <input type="button" value="Gửi" class="submit_send_comment" onclick="answer_comment(<?php echo $comment_id; ?>, <?php echo $detail->id; ?>)">
                                </div>
                             </form>
                        </span>
                        
                        <?php if(!empty($child)): 
                            foreach ($child as $rc) :
                                $name = $rc['name'];
                                $content = $rc['content'];  
                        ?>

                        <div class="child_comment1_1">
                            <i class="arrow_comment1"></i>
                            <div class="avatar_comment"><img src="themes/images/thumb_admin.png"></div>
                            <span class="name_faq">
                                <b><?php echo $name; ?></b>
                                <p><?php echo $content; ?></p>
                            </span>
                        </div>
                        <?php endforeach;endif; ?>
                    </div>
                    <?php } ?>
                </div>
                <?php endif; ?>

            </div>

            
            <?php if(!empty($other_product)){?>
            <div class="box_other_product">
                 <div class="comment_danhgia_v1">
                    <i class="icon-cungloại"></i><span>Sản phẩm cùng loại</span>
                </div>
            
                <div class="list_product_home_cate clearfix">
                    <?php 
                    foreach($other_product as $pr){
                        $id = $pr->id;
                        $name = $pr->name;
                        $pr_name = $this->my_lib->cut_string($pr->name, 55);
                        $link = site_url($pr->slug);
                        $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                        $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                        $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                        $price = cms_price_v1($pr->price_sale, $pr->price);

                        echo sprintf('<div class="box_product_v2">
                                        <div class="box_product_v2_img">
                                            <a href="%s" title="%s">
                                                <img src="%s" alt="%s">
                                            </a>
                                        </div>
                                        
                                        <div class="box_product_v2_name">
                                            <a href="%s" title="%s">%s</a>
                                        </div>
                                        <div class="box_product_v2_price clearfix">
                                            <div class="box_product_v2_price_num">%s</div>
                                            <div class="box_product_v2_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                        </div>%s%s
                                    </div>', $link, $name, $photo, $name, $link, $name, $name, $price, $id, $percent, $gift_type);
                    }
                    ?>
                </div>
            </div>

        </div>
        <?php } ?>

        <div class="right_detail_pr">
            <div class="box_support_policy">
                <div class="box_support_policy_header clearfix">
                    <div class="bsp_phone_icon">
                        <i class="icon-hotline2"></i>
                    </div>
                    <div class="bsp_phone_title">
                        <p>Hỗ trợ tư vấn</p>
                        <div class="bsp_phone_hotline">Hotline: <a href="tel:<?php echo $company->phone; ?>"><?php echo $company->phone; ?></a></div>
                    </div>
                </div>
                <div class="box_support_policy_midle">
                    <div class="box_support_policy_midle_title">Tùy chọn giao hàng</div>
                    <div class="select_location clearfix">
                        <label>Tỉnh/Thành</label>
                        <select name="province_id" onchange="load_district()">
                            <option value="">Chon tỉnh / thành</option>
                            <?php if(!empty($province)){
                            foreach ($province as $row) {
                                $id = $row->id;
                                $name = $row->name;
                                $selected = ($province_id == $id) ? ' selected' :  '';
                                echo '<option value="'. $id .'"'. $selected .'>'. $name .'</option>';
                            }}?>
                        </select>
                    </div>
                    <div class="select_location clearfix">
                        <label>Quận/Huyện</label>
                        <select name="district_id">
                            <option value="">Chon quận huyện</option>
                        </select>
                    </div>
                </div>
                <div class="box_support_policy_footer">
                    <ul>
                        <li class="clearfix">
                            <div class="ship_icon"><span class="icon-delivery"></span></div>
                            <div class="policy_content">
                                <div class="policy_content_title"><span>Giao hàng tiêu chuẩn: Miễn phí</span></div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="ship_icon"><span class="icon-delivery-truck"></span></div>
                            <div class="policy_content">
                                <div class="policy_content_title"><span>Giao hàng nhanh: 10.000 VND</span></div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="ship_icon"><span class="icon-money"></span></div>
                            <div class="policy_content">
                                <div class="policy_content_title"><span>Thanh toán khi nhận hàng</span></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="box_tuvan_detail">

                <div class="name_scrool">

                    <h4 class="h4_1"><?php echo $detail->name; ?></h4>
                    <?php if($detail->price != 0 && $detail->price_sale != 0){ ?>
                    <div class="price_show_1 clearfix">
                        <p class="price_show_1_1"><?php echo number_format($detail->price_sale) . ' VNĐ'; ?></p>
                        <p class="price_show_1_2"><?php echo number_format($detail->price) . ' VNĐ'; ?></p>
                    </div>
                    <?php } ?>
                    <?php if($detail->price != 0 && $detail->price_sale == 0){ ?>
                    <div class="price_show_1 clearfix">
                        <p class="price_show_1_1"><?php echo number_format($detail->price) . ' VNĐ'; ?></p>
                    </div>
                    <?php } ?>
                    <?php if($detail->price == 0){ ?>
                    <div class="price_show_1 clearfix">
                        <p class="price_show_1_1">Liên hệ</p>
                    </div>
                    <?php } ?>
                    <div class="wp_text_number clearfix">
                        <p class="text_number_1">Số lượng:</p>
                        <div class="number_cart1 number_cart2">
                            <span class="icon-soluongtru img_click_minus"></span>
                            <input type="text" name="qty" class="number_cart_input" value="1">
                            <span class="icon-soluongcong img_click_plus"></span>
                        </div>

                    </div>
                    <?php if($detail->gift_type != 0) {?>
                    <div class="wp_quatang wp_text_number1">
                        <a href="" class="wp_quatang1 wp_quatang2">
                            <p> <b>Quà tặng kèm</b></p>
                        </a>
                        <?php if(!empty($detail->gift_summary)){ ?><div class="gift_summary"><?php echo $detail->gift_summary ?></div><?php } ?>
                    </div>
                    <?php } ?>

                    <div class="card_by2 clearfix<?php if($detail->gift_type == 0) echo ' wp_text_number1'; ?>">
                        <a href="javascript:void(0)" class="card_by1" onclick="add_to_cart_pr_detail(<?php echo $detail->id; ?>)">
                            <p><b>mua ngay</b></p>
                        </a>
                    </div>
                   
                    

                </div>

                <div class="box_support_policy box_support_policy_fixed">
                    <div class="box_support_policy_header clearfix">
                        <div class="bsp_phone_icon">
                            <i class="icon-hotline2"></i>
                        </div>
                        <div class="bsp_phone_title">
                            <p>Hỗ trợ tư vấn</p>
                            <div class="bsp_phone_hotline">Hotline: <a href="tel:<?php echo $company->phone; ?>"><?php echo $company->phone; ?></a></div>
                        </div>
                    </div>
                    <?php /* ?>
                    <div class="box_support_policy_midle">
                        <div class="box_support_policy_midle_title">Tùy chọn giao hàng</div>
                        <div class="select_location clearfix">
                            <label>Tỉnh/Thành</label>
                            <select name="province_id" onchange="load_district()">
                                <option value="">Chon tỉnh / thành</option>
                                <?php if(!empty($province)){
                                foreach ($province as $row) {
                                    $id = $row->id;
                                    $name = $row->name;
                                    $selected = ($province_id == $id) ? ' selected' :  '';
                                    echo '<option value="'. $id .'"'. $selected .'>'. $name .'</option>';
                                }}?>
                            </select>
                        </div>
                        <div class="select_location clearfix">
                            <label>Quận/Huyện</label>
                            <select name="district_id">
                                <option value="">Chon quận huyện</option>
                            </select>
                        </div>
                    </div>
                    <div class="box_support_policy_footer">
                        <ul>
                            <li class="clearfix">
                                <div class="ship_icon"><span class="icon-delivery"></span></div>
                                <div class="policy_content">
                                    <div class="policy_content_title"><span>Giao hàng tiêu chuẩn: Miễn phí</span></div>
                                </div>
                            </li>
                            <li class="clearfix">
                                <div class="ship_icon"><span class="icon-delivery-truck"></span></div>
                                <div class="policy_content">
                                    <div class="policy_content_title"><span>Giao hàng nhanh: 10.000 VND</span></div>
                                </div>
                            </li>
                            <li class="clearfix">
                                <div class="ship_icon"><span class="icon-money"></span></div>
                                <div class="policy_content">
                                    <div class="policy_content_title"><span>Thanh toán khi nhận hàng</span></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <?php */ ?>
                </div>


            </div>



            <div class="box_prduct_care">
                <div class="title_cate_ft1">
                    <p>có thể bạn quan tâm</p>
                    <b></b>
                </div>
                
                <div class="box_prduct_care_element">
                <?php 
                foreach($other_product as $pr){
                    $id = $pr->id;
                    $name = $pr->name;
                    $pr_name = $this->my_lib->cut_string($pr->name, 75);
                    $link = site_url($pr->slug);
                    $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                    $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                    $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                    $price = cms_price_v1($pr->price_sale, $pr->price);

                    echo sprintf('<div class="box_product_v3">
                                    <div class="box_product_v3_img">
                                        <a href="%s" title="%s">
                                            <img src="%s" alt="%s">
                                        </a>
                                    </div>
                                    
                                    <div class="box_product_v3_name">
                                        <a href="%s" title="%s">%s</a>
                                    </div>
                                    <div class="box_product_v3_price clearfix">
                                        <div class="box_product_v3_price_num">%s</div>
                                        <div class="box_product_v3_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                    </div>%s%s
                                </div>', $link, $name, $photo, $name, $link, $name, $name, $price, $id, $percent, $gift_type);
                }
                ?>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="fixed_cart_phone">
    <div class="fixed_phone">
        <a href="tel:<?php echo $company->phone; ?>">
            <i class="icon-sodienthoai"></i>
            <span><?php echo $company->phone; ?></span>
        </a>
    </div>
    <div class="fixed_cart">
        <a href="javascript:void(0)" onclick="add_to_cart(<?php echo $detail->id; ?>)">
            <i class="icon-muahang"></i>
            <span>Mua ngay</span>
        </a>
    </div>
</div>