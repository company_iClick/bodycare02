<?php if(!empty($list_product_category)){ 
    foreach($list_product_category as $key => $value){
        $element = explode(',', $key);
        $ct_id = $element[0];
        $name = $element[1];
        $link = (!empty($filter_parameter)) ? site_url('tags/' . $element[2]) . '?' . $filter_parameter : site_url('tags/' . $element[2]);

        switch ($mod) {

            case 'category':
                $link = (!empty($filter_parameter)) ? site_url($element[2]) . '?' . $filter_parameter : site_url($element[2]);
                break;

            case 'tags':
                $link = (!empty($filter_parameter)) ? site_url('tags/' . $detail->slug . '/' . $element[2]) . '?' . $filter_parameter : site_url('tags/' . $detail->slug . '/' . $element[2]);
                break;

            case 'promotion':
                $link = (!empty($filter_parameter)) ? site_url('khuyen-mai/' . $element[2]) . '?' . $filter_parameter : site_url('khuyen-mai/' . $element[2]);
                break;

            case 'male':
                $link = (!empty($filter_parameter)) ? site_url('danh-cho-nam/'. $element[2]) . '?' . $filter_parameter : site_url('danh-cho-nam/' . $element[2]);
                break;

            case 'female':
                $link = (!empty($filter_parameter)) ? site_url('danh-cho-nu/' . $element[2]) . '?' . $filter_parameter : site_url('danh-cho-nu/' . $element[2]);
                break;

            case 'search':
                $link = 'javascript:void(0)';
                break;
            
            default:
                $link = (!empty($filter_parameter)) ? site_url($detail->slug . '/' . $element[2]) . '?' . $filter_parameter : site_url($detail->slug . '/' . $element[2]);
                break;
        }

        $pagination = $value['pagination'];
        $total_ct = $value['total_ct'];
        $list_product = $value['product'];

        if(!empty($list_product)){
?>

<div class="list_pr_ds">   
    <div class="cate_arrangement_pr_list clearfix">
        <div class="cate_arrangement_pr_list_text">
            <p><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?></a></p>
            <b></b>
        </div>
        <?php if($mod != 'search'){ ?>
        <a href="<?php echo $link; ?>" class="cate_view_all">Xem tất cả</a>
        <?php } ?>
    </div>
    <div class="clear"></div>
    <div id="load_product_list_<?php echo $ct_id; ?>" class="load_product_list clearfix">
        <?php 
        foreach($list_product as $pr){
            $id = $pr->id;
            $name = $pr->name;
            $pr_name = $this->my_lib->cut_string($pr->name, 55);
            $link = site_url($pr->slug);
            $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
            $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
            $summary = $pr->summary;
            $class_layout_list = ($layout == 'list') ? ' box_product_v2_list' : '';
            $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
            $price = cms_price_v1($pr->price_sale, $pr->price);

            echo sprintf('<div class="box_product_v2%s">
                            <div class="box_product_v2_img">
                                <a href="%s" title="%s">
                                    <img src="%s" alt="%s">
                                </a>
                            </div>
                            <div class="box_product_v2_name_element">
                                <div class="box_product_v2_name">
                                    <a href="%s" title="%s">%s</a>
                                    <a href="%s" title="%s" class="name_layout_list">%s</a>
                                </div>
                                <div class="box_product_v2_sum">%s</div>
                                <div class="box_product_v2_price clearfix">
                                    <div class="box_product_v2_price_num">%s</div>
                                    <div class="box_product_v2_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                </div>
                            </div>%s%s
                        </div>', $class_layout_list, $link, $name, $photo, $name, $link, $name, $name, $link, $name, $name, $summary, $price, $id, $percent, $gift_type);
        }?>
        <div class="ajax_loadding"></div>
        <div class="clear"></div>
        <div id="page_cate_<?php echo $ct_id; ?>"><?php echo $pagination; ?></div>
    </div>
    <input type="hidden" name="total_ct_<?php echo $ct_id; ?>" value="<?php echo $total_ct; ?>">
    
    <input type="hidden" name="method_id_<?php echo $ct_id; ?>" value="<?php echo $mod_id; ?>">
</div>
<?php }}} ?>