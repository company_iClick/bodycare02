<div class="wrapper_full wrapper_full_border_1">
    <div class="wrapper_full_1">
        <div class="wrapper_full_4">
            <?php $this->load->view(BLOCK . 'left'); ?>
            
            <div class="right_list_product">
                
                <?php if(isset($category) && !empty($banner)){?>
                <div class="banner_pr_list">
                    <a href="<?php echo $link_banner; ?>"><img src="<?php echo base_url(_upload_category . $banner) ?>"></a>
                </div>
                <?php } ?>

                <?php $this->load->view(BLOCK . 'list_cate'); ?>
                
                <div class="cate_arrangement_pr_list cate_arrangement_pr_list_parent clearfix">
                    <div class="cate_arrangement_pr_list_text">
                        <p><?php echo $title_bar; ?></p>
                        <b></b>
                    </div>
                    <div class="cate_arrangement_pr_list_select">
                        <p>Sắp xếp theo</p>
                        <select class="selec_arrangementt" name="sort" onchange="product_filter()">
                            <option value="">Tất cả</option>
                            <option value="price-asc"<?php if($sort == "price-asc") echo " selected"; ?>>Giá từ thấp đến cao</option>
                            <option value="price-desc"<?php if($sort == "price-desc") echo " selected"; ?>>Giá từ cao đến thấp</option>
                            <option value="khuyen-mai"<?php if($sort == "khuyen-mai") echo " selected"; ?>>Khuyến mãi</option>
                            <option value="qua-tang"<?php if($sort == "qua-tang") echo " selected"; ?>>Có quà tặng</option>
                        </select>
                        <span class="arrangementt_icon square_arrangementt<?php if($layout == '' || $layout == 'grid') echo ' active_arrangementt'; ?>">
                            <i></i>
                        </span>
                        <span class="arrangementt_icon long_arrangementt<?php if($layout == 'list') echo ' active_arrangementt'; ?>">
                            <i></i>
                        </span>
                    </div>
                </div>
                
                <div class="clear"></div>
                <!-- product filter select -->
                <div id="product_filter_select">
                    <?php if(!empty($price_select) || !empty($brand_select) || !empty($product_type_select) || !empty($skin_type_select) || !empty($hair_type_select) || !empty($origin_select) || !empty($action_select) || !empty($capacity_select) || !empty($weigh_select) || !empty($pill_number_select)){ ?>

                    <label>Lọc theo:</label>
                    <?php if(!empty($price_select)){ 
                        foreach($price_select as $row){
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Giá:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('price', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($brand_select)){ 
                        foreach($brand_select as $row){
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Thương hiệu:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('brand', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($product_type_select)){ 
                        foreach($product_type_select as $row){
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Loại sản phẩm:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('product_type', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($skin_type_select)){ 
                        foreach($skin_type_select as $row){
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Loại da:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('skin_type', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($hair_type_select)){ 
                        foreach($hair_type_select as $row){ 
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Loại tóc:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('hair_type', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($origin_select)){ 
                        foreach($origin_select as $row){ 
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Xuất xứ:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('origin', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($action_select)){ 
                        foreach($action_select as $row){
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Chức năng:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('action', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($capacity_select)){ 
                        foreach($capacity_select as $row){   
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Dung tích:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('capacity', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($weigh_select)){ 
                        foreach($weigh_select as $row){
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Trọng lượng:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('weigh', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <?php if(!empty($pill_number_select)){ 
                        foreach($pill_number_select as $row){
                            $id = $row->id;
                            $name = $row->name;
                        ?>
                        <div class="item_select">
                            <label>Số lượng viên:</label>
                            <span><?php echo $name; ?></span>
                            <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('pill_number', <?php echo $id; ?>)"></i></div>
                        </div>
                    <?php }} ?>

                    <div class="item_select">
                        <label>Xóa tất cả</label>
                        <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_all()"></i></div>
                    </div>

                <?php } ?>
                </div>
    
                <div class="clear"></div>
                <div id="load_product_list" class="clearfix">
                    <?php if(!empty($category_child)){ 
                        foreach($category_child as $row){
                            $ct_id = $row->id;
                            $name = $row->name;
                            $ct_link = (!empty($filter_parameter)) ? site_url($row->slug) . '?' . $filter_parameter : site_url($row->slug);
                            if(isset($mod) && ($mod == 'product_type_category' || $mod == 'skin_type_category'  || $mod == 'hair_type_category' || $mod == 'origin_category' || $mod == 'action_category' || $mod == 'pill_number_category' || $mod == 'weigh_category' || $mod == 'capacity_category')){
                                $link = (!empty($filter_parameter)) ? site_url($this->uri->segment(1) . '/' . $row->slug) . '?' . $filter_parameter : site_url($this->uri->segment(1) . '/' . $row->slug);
                            }
                            $pagination = $row->pagination;
                            $total_ct = $row->total_ct;
                            $list_product = $row->product;

                            if(!empty($list_product)){
                    ?>
                    
                    <div class="list_pr_ds">   
                        <div class="cate_arrangement_pr_list clearfix">
                            <div class="cate_arrangement_pr_list_text">
                                <p><a href="<?php echo $ct_link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?></a></p>
                                <b></b>
                            </div>
                            <a href="<?php echo $ct_link; ?>" class="cate_view_all">Xem tất cả<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="clear"></div>
                        <div id="load_product_list_<?php echo $ct_id; ?>" class="load_product_list clearfix">
                            <?php 
                            foreach($list_product as $pr){
                                $id = $pr->id;
                                $name = $pr->name;
                                $pr_name = $this->my_lib->cut_string($pr->name, 55);
                                $link = site_url($pr->slug);
                                $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                                $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                                $summary = $pr->summary;
                                $class_layout_list = ($layout == 'list') ? ' box_product_v2_list' : '';
                                $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                                $price = cms_price_v1($pr->price_sale, $pr->price);

                                echo sprintf('<div class="box_product_v2%s">
                                                <div class="box_product_v2_img">
                                                    <a href="%s" title="%s">
                                                        <img src="%s" alt="%s">
                                                    </a>
                                                </div>
                                                <div class="box_product_v2_name_element">
                                                    <div class="box_product_v2_name">
                                                        <a href="%s" title="%s">%s</a>
                                                        <a href="%s" title="%s" class="name_layout_list">%s</a>
                                                    </div>
                                                    <div class="box_product_v2_sum">%s</div>
                                                    <div class="box_product_v2_price clearfix">
                                                        <div class="box_product_v2_price_num">%s</div>
                                                        <div class="box_product_v2_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                                    </div>
                                                </div>%s%s
                                            </div>', $class_layout_list, $link, $name, $photo, $name, $link, $name, $name, $link, $name, $name, $summary, $price, $id, $gift_type, $percent);
                            }?>
                            <div class="ajax_loadding"></div>
                            <div class="clear"></div>
                            <div id="page_cate_<?php echo $ct_id; ?>"><?php echo $pagination; ?></div>
                        </div>
                        <div class="btn_view_more"><a href="<?php echo $ct_link; ?>">Xem thêm</a></div>
                        <input type="hidden" name="total_ct_<?php echo $ct_id; ?>" value="<?php echo $total_ct; ?>">
                    </div>
                    <?php }}} ?>
                </div>

                <div class="clear"></div>
                <?php if(isset($big_summary) && !empty($big_summary)){ ?>
                <div class="banner_pr_list1 box_summary_cate"><?php echo $big_summary; ?></div>
                <div class="clear"></div>
                <?php } ?>
                <?php $this->load->view(BLOCK . 'search_cate_mb'); ?>
                <?php if(isset($list_article) && !empty($list_article)){?>
                <div class="cate_news_1">
                    <div class="title_name_cate_3 color_5">
                        <p>Kiến thức làm đẹp</a></p>
                        <i class="line_1"></i>
                    </div>
                    <?php 
                        $i = 0; 
                        foreach ($list_article as $row) {
                        $a_name = $row->name;
                        $a_link = site_url($row->slug);
                        $summary = $this->my_lib->cut_string($row->summary, 100);
                        $imgUrl = base_url(_upload_article . $row->folder . '/' . $row->photo);
                        $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=270&h=170&zc=1');
                        $class = (($i + 1) % 3 == 0) ? ' last' : '';
                        echo sprintf('<div class="child_news1%s">
                                        <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                                        <span class="summary_news">
                                            <a href="%s" title="%s">%s</a>
                                            <p>%s</p>
                                             <a href="%s" class="fa fa-long-arrow-right" aria-hidden="true"></a>
                                        </span>
                                    </div>', $class, $a_link, $a_name, $imgUrl, $a_name, $a_link, $a_name, $a_name, $summary, $a_link);
                        $i++;
                    } ?>
                </div>
            </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>