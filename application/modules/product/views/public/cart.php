<div class="wrapper_full">
    <div class="wrapper_full_1">
        <div class="wrapper_full_1_mb">
    <?php 
        $total = number_format($this->cart->total()) . ' VNĐ';
        if(count($this->cart->contents()) > 0){ 
    ?>

        <table class="table_cart">
            <tbody>
                <tr>
                    <td class="name_pr_cart_table">Sản phẩm</td>
                    <td class="price_pr_cart_table">Giá</td>
                    <td class="number_pr_cart_table">Số lượng</td>
                    <td class="total_pr_cart_table">Thành tiền</td>
                    <td class="delete_pr_cart_table">Xóa</td>
                </tr>
                
                <?php foreach ($this->cart->contents() as $row) { 
                    $rowid = $row['rowid'];
                    $id = $row['id'];
                    $product = $this->global_function->get_row_object(array('id' => $id), 'name, brand_id', 'product');
                   
                    $brand = $this->global_function->get_row_object(array('id' => $product->brand_id), 'name, slug', 'brand');
                    $price = number_format($row['price']) . ' VNĐ';
                    $subtotal = number_format($row['subtotal']) . ' VNĐ';
                    $qty = $row['qty'];
                    $link = $row['link'];
                    $images = $row['images'];
                ?>
                <tr>
                    <td class="name_pr_cart_table">
                        <a href="<?php echo $link; ?>" class="img_a_cart_1" target="_blank"><img src="<?php echo $images; ?>"></a>
                        <span>
                            <?php if(!empty($brand)){ ?><a href="<?php echo site_url($brand->slug) ?>" target="_blank"><?php echo $brand->name ?></a><?php } ?>
                            <p><a href="<?php echo $link; ?>" target="_blank"><?php echo $product->name; ?></a></p>
                        </span>

                    </td>
                    <td class="price_pr_cart_table">
                        <p class="price_pr_cart_table1"><?php echo $price; ?></p>

                    </td>
                    <td class="number_pr_cart_table">
                        <div class="number_cart1">
                            <span class="icon-soluongtru img_click_minus_one" data-rowid="<?php echo $rowid; ?>"></span>
                            <input type="text" class="number_cart_input" id="qty_<?php echo $rowid; ?>" value="<?php echo $qty; ?>">
                            <span class="icon-soluongcong img_click_plus_one" data-rowid="<?php echo $rowid; ?>"></span>
                        </div>
                    </td>
                    <td class="total_pr_cart_table">
                        <p class="price_pr_cart_table1"><?php echo $subtotal; ?></p>
                    </td>
                    <td class="delete_pr_cart_table"><i onclick="if (!confirm('Bạn muốn xóa sản phẩm này?')) return false; delete_cart('<?php echo $rowid;?>')"></i></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

        <div class="content_cart_mobile clearfix">
            <?php foreach ($this->cart->contents() as $row) { 
                $rowid = $row['rowid'];
                $id = $row['id'];
                $product = $this->global_function->get_row_object(array('id' => $id), 'name, brand_id', 'product');
               
                $brand = $this->global_function->get_row_object(array('id' => $product->brand_id), 'name, slug', 'brand');
                $price = number_format($row['price']) . 'đ';
                $subtotal = number_format($row['subtotal']) . 'đ';
                $qty = $row['qty'];
                $link = $row['link'];
                $images = $row['images'];
            ?>
            <div class="item_cart clearfix">
                <div class="thumb_donhang">
                    <a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo $images; ?>" alt="<?php echo $product->name; ?>"></a>
                </div>
                <div class="info_donhang">
                    <div class="title_sanpham_donhang">
                        <div><strong><?php if(!empty($brand)){ ?><a href="<?php echo site_url($brand->slug) ?>" target="_blank"><?php echo $brand->name ?></a><?php } ?></strong></div>
                        <a href="<?php echo $link; ?>" target="_blank"><?php echo $product->name; ?></a>
                    </div>
                    <div class="price_cart_info"><span class="giamoi"><?php echo $subtotal; ?></span></div>
                    <div class="number_pr_cart_table">
                        <div class="number_cart1">
                            <span class="icon-soluongtru img_click_minus_one" data-rowid="<?php echo $rowid; ?>"></span>
                            <input type="text" class="number_cart_input" id="qty_<?php echo $rowid; ?>" value="<?php echo $qty; ?>">
                            <span class="icon-soluongcong img_click_plus_one" data-rowid="<?php echo $rowid; ?>"></span>
                        </div>
                    </div>
                </div>

                <a href="javascript:void(0)" class="btn_del_cart_item material-ripple"><i class="fa fa-trash" aria-hidden="true" onclick="if (!confirm('Bạn muốn xóa sản phẩm này?')) return false; delete_cart('<?php echo $rowid;?>')"></i></a>
            </div>
            <?php } ?>
            <div class="totail_11">
                <p class="totail_1_1">Tổng tiền</p>

                <p class="totail_1_2"><?php echo $total; ?></p>
            </div>
            <div class="thanhtoan_muasam">
                <a href="<?php echo site_url(); ?>" class="a_next_by">Tiếp tục mua sắm</a>
                <a href="<?php echo site_url('hoan-tat-don-hang'); ?>" class="a_thanhtoan">Hoàn tất đơn hàng</a>
            </div>
        </div>
        
        <table class="text_tt_cart">
            <tr>
                <td>
                    <div class="text_tt_cart_left">

                        <ul>
                            <li>Nếu bạn đồng ý mua danh sách sản phẩm ở trên thì vui lòng bấm nút "<a href="">HOÀN TẤT ĐƠN HÀNG</a>"</li>
                            <li>Để tiếp tục mua thêm sản phẩm khác, bạn bấm nút "<a href="">TIẾP TỤC MUA</a>"</li>
                            <li>Để bỏ từng sản phẩm trong danh sách, bấm vào biểu tượng xóa phía bên trái hình sản phẩm</li>
                            <li>Để thêm số lượng cho một sản phẩm cần mua, bạn chọn số lượng</li>
                        </ul>

                    </div>
                </td>
                <td>
                    <div class="totail_11">
                        <p class="totail_1_1">Tổng tiền</p>

                        <p class="totail_1_2"><?php echo $total; ?></p>
                    </div>
                    <div class="thanhtoan_muasam">
                        <a href="<?php echo site_url(); ?>" class="a_next_by">Tiếp tục mua sắm</a>
                        <a href="<?php echo site_url('hoan-tat-don-hang') . $param; ?>" class="a_thanhtoan">Hoàn tất đơn hàng</a>
                    </div>

                </td>
            </tr>

        </table>
        <?php } ?>


    </div>


</div>
</div>
</div>