<?php if(!empty($price_select) || !empty($brand_select) || !empty($product_type_select) || !empty($skin_type_select) || !empty($hair_type_select) || !empty($origin_select) || !empty($action_select) || !empty($capacity_select) || !empty($weigh_select) || !empty($pill_number_select)){ ?>

	<label>Lọc theo:</label>
	<?php if(!empty($price_select)){ 
		foreach($price_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Giá:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('price', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($brand_select)){ 
		foreach($brand_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Thương hiệu:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('brand', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($product_type_select)){ 
		foreach($product_type_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Loại sản phẩm:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('product_type', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($skin_type_select)){ 
		foreach($skin_type_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Loại da:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('skin_type', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($hair_type_select)){ 
		foreach($hair_type_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Loại tóc:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('hair_type', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($origin_select)){ 
		foreach($origin_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Xuất xứ:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('origin', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($action_select)){ 
		foreach($action_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Chức năng:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('action', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($capacity_select)){ 
		foreach($capacity_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Dung tích:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('capacity', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($weigh_select)){ 
		foreach($weigh_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Trọng lượng:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('weigh', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<?php if(!empty($pill_number_select)){ 
		foreach($pill_number_select as $row){
			$array_value = json_decode($row);
			$id = $array_value->id;
			$name = $array_value->name;
		?>
		<div class="item_select">
			<label>Số lượng viên:</label>
		    <span><?php echo $name; ?></span>
		    <div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_select('pill_number', <?php echo $id; ?>)"></i></div>
		</div>
	<?php }} ?>

	<div class="item_select">
		<label>Xóa tất cả</label>
		<div class="item_select_close"><i class="fa fa-times" aria-hidden="true" onclick="remove_fiter_all()"></i></div>
	</div>

<?php } ?>