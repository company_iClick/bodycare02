
<div class="wrapper_full wrapper_full_border_1">
    <div class="wrapper_full_1">
        <div class="wrapper_full_4">
            <?php $this->load->view(BLOCK . 'left_one'); ?>
            <div class="right_list_product">
                <div class="banner_pr_list1">
                    <div class="left_thuonghieu_img_1">
                        <img src="<?php echo base_url(_upload_brand . $brand->photo); ?>" alt="<?php echo $brand->name; ?>">
                    </div>
                    <div class="left_thuonghieu_img_2">
                        <h1 class="h1_1">THƯƠNG HIỆU <?php echo $brand->name; ?></h1>
                        <?php echo $brand->summary; ?>
                    </div>
                </div>
                <div class="cate_arrangement_pr_list">
                    <div class="cate_arrangement_pr_list_text" style="width: 100%;float: left">
                        <p style="float: left;width: 100%;padding-bottom: 10px">Sản phẩm hiệu <?php echo $brand->name; ?></p>
                        <b style="float:left"></b>
                    </div>
                </div>
                <?php if(isset($category_brand)){
                    $title_show = (!empty($category_brand->title_show)) ? $category_brand->title_show : $category_brand->name;
                    $product = $category_brand->product;
                ?>
                <div class="list_pr_ds">
                    <div class="list_thuonghieu_cate clearfix">
                        <div class="title_name_cate_2">
                            <p><?php echo $title_show; ?></p>
                            <i class="line_1"></i>
                        </div>
                  <div class="slider_cate_thuonghieu">
                        <?php 
                        $i = 0;
                        foreach($product as $pr){ 
                            $id = $pr->id;
                            $name = $pr->name;
                            $pr_name = $this->my_lib->cut_string($pr->name, 80);
                            $link = site_url($pr->slug);
                            $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                            $percent = percen_calculate($pr->price_sale, $pr->price, array('class' => 'percent_pr'));
                            $price = cms_price($pr->price_sale, $pr->price); 
                            $class = (($i + 1) % 4 == 0) ? ' child_cate_produc_2_last' : '';
                            $br_name = $pr->br_name;
                            $br_slug = site_url($pr->br_slug);
                            echo sprintf('<div class="child_cate_produc_2%s">
                                            %s
                                            <a href="%s" title="%s" class="img_pr_home">
                                                <img src="%s" alt="%s">
                                            </a>
                                            <a href="%s" class="name_link_pr">%s</a>
                                            <p class="pr_name"><a style="color:#5e5e5e" href="%s" title="%s">%s</a></p>
                                            <div class="price_cart">
                                                %s
                                                <div class="wp_cart">
                                                    <p class="icon_cart" onclick="add_to_cart(%u)">Mua ngay</p>
                                                </div>
                                            </div>
                                        </div>', $class, $percent, $link, $name, $photo, $name, $br_slug, $br_name, $link, $name, $pr_name, $price, $id);
                            $i++;
                        }?>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <?php if(!empty($categories)){ ?>
                <div class="list_pr_ds">
                    <?php foreach($categories as $row){ 
                        $title_show = (!empty($row->title_show)) ? $row->title_show : $row->name;
                        $product = $row->product;
                        if(!empty($product)){
                    ?>
                    <div class="list_thuonghieu_cate">
                        <div class="title_name_cate_2">
                            <p><?php echo $title_show; ?></p>
                            <i class="line_1"></i>
                         
                        </div>
 <div class="slider_cate_thuonghieu">
                        <?php 
                        $i = 0;
                        foreach($product as $pr){ 
                            $id = $pr->id;
                            $name = $pr->name;
                            $pr_name = $this->my_lib->cut_string($pr->name, 80);
                            $link = site_url($pr->slug);
                            $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                            $percent = percen_calculate($pr->price_sale, $pr->price, array('class' => 'percent_pr'));
                            $price = cms_price($pr->price_sale, $pr->price); 
                            $class = (($i + 1) % 4 == 0) ? ' child_cate_produc_2_last' : '';
                            $br_name = $pr->br_name;
                            $br_slug = site_url($pr->br_slug);
                            echo sprintf('<div class="child_cate_produc_2%s">
                                            %s
                                            <a href="%s" title="%s" class="img_pr_home">
                                                <img src="%s" alt="%s">
                                            </a>
                                            <a href="%s" class="name_link_pr">%s</a>
                                            <p class="pr_name"><a style="color:#5e5e5e" href="%s" title="%s">%s</a></p>
                                            <div class="price_cart">
                                                %s
                                                <div class="wp_cart">
                                                    <p class="icon_cart" onclick="add_to_cart(%u)">Mua ngay</p>
                                                </div>
                                            </div>
                                        </div>', $class, $percent, $link, $name, $photo, $name, $br_slug, $br_name, $link, $name, $pr_name, $price, $id);
                            $i++;
                        }?>
                    </div>

                    <?php }} ?>
                     </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.slider_cate_thuonghieu').bxSlider({
        auto: true,
        slideWidth: 200,
        minSlides: 4,
        maxSlides: 10,
        moveSlides: 4,
          startSlide: 4,
        slideMargin: 21,
        pager: false, controls: true

    });
});
</script>