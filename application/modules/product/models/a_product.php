<?php
class A_product extends CI_Model
{
    protected $_table = 'product';
    protected $_cate = 'category';
    protected $_tmp_pc = 'tmp_product_category';
    protected $_tmp_ppt = 'tmp_product_product_type';
    protected $_tmp_pht = 'tmp_product_hair_type';
    protected $_tmp_pst = 'tmp_product_skin_type';
    protected $_tmp_pa = 'tmp_product_action';
    protected $_tmp_pca = 'tmp_product_capacity';
    protected $_tmp_pw = 'tmp_product_weigh';
    protected $_tmp_ppn = 'tmp_product_pill_number';
    protected $_tmp_ptg = 'tmp_product_tag';
    protected $_br = 'brand';

    protected $_pt = 'product_type';
    protected $_a = 'action';
    protected $_cp = 'capacity';
    protected $_st = 'skin_type';
    protected $_ht = 'hair_type';
    protected $_ppn = 'pill_number';
    protected $_w = 'weigh';
    protected $_tgs = 'tags';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function count_all_item($filter_id, $list_id, $tpl, $tpl_filter, $where = array()){
        $this->db->select("$this->_table.id");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1));

        switch ($tpl) {
            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $list_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $list_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $list_id); 
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $list_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $list_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $list_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $list_id);
                break;
        }
       

        switch ($tpl_filter) {
            case 'product_type':
                $this->db->where("$this->_tmp_ppt.product_type_id", $filter_id);
                break;

            case 'action':
                $this->db->where("$this->_tmp_pa.action_id", $filter_id);
                break;

            case 'capacity':
                $this->db->where("$this->_tmp_pca.capacity_id", $filter_id); 
                break;

            case 'skin_type':
                $this->db->where("$this->_tmp_pst.skin_type_id", $filter_id);
                break;

            case 'hair_type':
                $this->db->where("$this->_tmp_pht.hair_type_id", $filter_id);
                break;

            case 'pill_number':
                $this->db->where("$this->_tmp_ppn.pill_number_id", $filter_id);
                break;

            case 'weigh':
                $this->db->where("$this->_tmp_pw.weigh_id", $filter_id);
                break;

            case 'origin':
                $this->db->where("$this->_table.origin_id", $filter_id);
                break;

            case 'tags':
                $this->db->where("$this->_tmp_ptg.tag_id", $filter_id);
                break;
        }

        $this->db->group_by("$this->_table.id");

        $this->db->from($this->_table);

        switch ($tpl) {
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                $this->db->join("$this->_pt", "$this->_pt.id = $this->_tmp_ppt.product_type_id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                $this->db->join("$this->_a", "$this->_a.id = $this->_tmp_pa.action_id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id"); 
                $this->db->join("$this->_cp", "$this->_cp.id = $this->_tmp_pca.capacity_id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id"); 
                $this->db->join("$this->_st", "$this->_st.id = $this->_tmp_pst.skin_type_id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id"); 
                $this->db->join("$this->_ht", "$this->_ht.id = $this->_tmp_pht.hair_type_id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");  
                $this->db->join("$this->_ppn", "$this->_ppn.id = $this->_tmp_ppn.pill_number_id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");  
                $this->db->join("$this->_w", "$this->_w.id = $this->_tmp_pw.weigh_id"); 
                break;
        }

        switch ($tpl_filter) {
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                $this->db->join("$this->_pt", "$this->_pt.id = $this->_tmp_ppt.product_type_id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                $this->db->join("$this->_a", "$this->_a.id = $this->_tmp_pa.action_id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id"); 
                $this->db->join("$this->_cp", "$this->_cp.id = $this->_tmp_pca.capacity_id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id"); 
                $this->db->join("$this->_st", "$this->_st.id = $this->_tmp_pst.skin_type_id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id"); 
                $this->db->join("$this->_ht", "$this->_ht.id = $this->_tmp_pht.hair_type_id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");  
                $this->db->join("$this->_ppn", "$this->_ppn.id = $this->_tmp_ppn.pill_number_id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");  
                $this->db->join("$this->_w", "$this->_w.id = $this->_tmp_pw.weigh_id"); 
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");  
                $this->db->join("$this->_tgs", "$this->_tgs.id = $this->_tmp_ptg.tag_id"); 
                break;
        }
        
        return $this->db->get()->num_rows();
    }


    function count_all_item_category($list_id, $list_cate_id, $tpl, $where = array()){
        $this->db->select("$this->_table.id");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1));

        switch ($tpl) {
            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $list_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $list_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $list_id); 
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $list_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $list_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $list_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $list_id);
                break;
        }

        $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");

        $this->db->from($this->_table);

        switch ($tpl) {
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                $this->db->join("$this->_pt", "$this->_pt.id = $this->_tmp_ppt.product_type_id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                $this->db->join("$this->_a", "$this->_a.id = $this->_tmp_pa.action_id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id"); 
                $this->db->join("$this->_cp", "$this->_cp.id = $this->_tmp_pca.capacity_id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id"); 
                $this->db->join("$this->_st", "$this->_st.id = $this->_tmp_pst.skin_type_id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id"); 
                $this->db->join("$this->_ht", "$this->_ht.id = $this->_tmp_pht.hair_type_id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");  
                $this->db->join("$this->_ppn", "$this->_ppn.id = $this->_tmp_ppn.pill_number_id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");  
                $this->db->join("$this->_w", "$this->_w.id = $this->_tmp_pw.weigh_id"); 
                break;
        }

        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");

        return $this->db->get()->num_rows();
    }

    function count_all_item_other($list_id, $tpl, $where = array()){
        $this->db->select("$this->_table.id");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1));

        switch ($tpl) {
            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $list_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $list_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $list_id); 
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $list_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $list_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $list_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $list_id);
                break;
        }

        $this->db->group_by("$this->_table.id");

        $this->db->from($this->_table);

        switch ($tpl) {
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                $this->db->join("$this->_pt", "$this->_pt.id = $this->_tmp_ppt.product_type_id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                $this->db->join("$this->_a", "$this->_a.id = $this->_tmp_pa.action_id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id"); 
                $this->db->join("$this->_cp", "$this->_cp.id = $this->_tmp_pca.capacity_id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id"); 
                $this->db->join("$this->_st", "$this->_st.id = $this->_tmp_pst.skin_type_id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id"); 
                $this->db->join("$this->_ht", "$this->_ht.id = $this->_tmp_pht.hair_type_id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");  
                $this->db->join("$this->_ppn", "$this->_ppn.id = $this->_tmp_ppn.pill_number_id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");  
                $this->db->join("$this->_w", "$this->_w.id = $this->_tmp_pw.weigh_id"); 
                break;
        }

        return $this->db->get()->num_rows();
    }

    function count_all_item_search($list_id, $list_cate_id, $keyword, $tpl, $where = array()){
        $this->db->select("$this->_table.id");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1));
        $this->db->like("$this->_table.slug", $keyword);

        switch ($tpl) {
            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $list_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $list_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $list_id); 
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $list_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $list_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $list_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $list_id);
                break;
        }

        $this->db->group_by("$this->_table.id");

        $this->db->from($this->_table);

        switch ($tpl) {
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                $this->db->join("$this->_pt", "$this->_pt.id = $this->_tmp_ppt.product_type_id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                $this->db->join("$this->_a", "$this->_a.id = $this->_tmp_pa.action_id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id"); 
                $this->db->join("$this->_cp", "$this->_cp.id = $this->_tmp_pca.capacity_id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id"); 
                $this->db->join("$this->_st", "$this->_st.id = $this->_tmp_pst.skin_type_id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id"); 
                $this->db->join("$this->_ht", "$this->_ht.id = $this->_tmp_pht.hair_type_id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");  
                $this->db->join("$this->_ppn", "$this->_ppn.id = $this->_tmp_ppn.pill_number_id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");  
                $this->db->join("$this->_w", "$this->_w.id = $this->_tmp_pw.weigh_id"); 
                break;
        }
        if (!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");

        return $this->db->get()->num_rows();
    }


    function get_category_info($detail_id){
        return $this->db->select("$this->_cate.id, $this->_cate.parent_id, $this->_cate.name, $this->_cate.slug")
                        ->where("$this->_tmp_pc.product_id", $detail_id)
                        ->from("$this->_cate")
                        ->join("$this->_tmp_pc", "$this->_tmp_pc.category_id = $this->_cate.id")
                        ->get()
                        ->row();
    }
  
    function search($list_cate_id, $keyword, $limit, $offset, $where = array(), $options = null)
    {
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        if (!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->like("$this->_table.slug", $keyword);
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        } 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        if (!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_search_sale_where($list_cate_id, $keyword, $limit, $offset, $where = array()){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        if (!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->like("$this->_table.slug", $keyword);
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        if (!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_search($list_cate_id, $keyword)
    {
        if (!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->like("$this->_table.slug", $keyword);
        $this->db->from($this->_table);
        if (!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_sale_index(){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug")
                    ->where(array("$this->_table.status" => 1, "$this->_table.sale" => 1, "$this->_br.status" => 1))
                    ->order_by("$this->_table.weight", "ASC") 
                    ->order_by("$this->_table.id", "DESC")
                    ->group_by("$this->_table.id")
                    ->from($this->_table)
                    ->join("$this->_br", "$this->_br.id = $this->_table.brand_id")
                    ->get()
                    ->result();
    }

    function get_product_new_sale_index($limit = 1){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_br.name as br_name, $this->_br.slug as br_slug")
                    ->where(array("$this->_table.status" => 1, "$this->_table.new_sale" => 1, "$this->_br.status" => 1))
                    ->order_by("$this->_table.weight", "ASC") 
                    ->order_by("$this->_table.id", "DESC")
                    ->group_by("$this->_table.id")
                    ->limit($limit)
                    ->from($this->_table)
                    ->join("$this->_br", "$this->_br.id = $this->_table.brand_id")
                    ->get()
                    ->result();
    }

    function get_product_hot_in_category($list_cate_id, $limit = 1){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_br.name as br_name, $this->_br.slug as br_slug")
                    ->where_in("$this->_tmp_pc.category_id", $list_cate_id)
                    ->where(array("$this->_table.status" => 1, "$this->_table.hot" => 1, "$this->_br.status" => 1))
                    ->order_by("$this->_table.id", "RANDOM")
                    ->order_by("$this->_table.weight", "ASC") 
                    ->order_by("$this->_table.id", "DESC")
                    ->group_by("$this->_table.id")
                    ->limit($limit)
                    ->from($this->_table)
                    ->join("$this->_br", "$this->_br.id = $this->_table.brand_id")
                    ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id")
                    ->get()
                    ->result();
    }

    function get_product_in_category($list_cate_id, $limit, $offset){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug")
                    ->where_in("$this->_tmp_pc.category_id", $list_cate_id)
                    ->where(array("$this->_table.status" => 1, "$this->_br.status" => 1))
                    ->order_by("$this->_table.weight", "ASC") 
                    ->order_by("$this->_table.id", "DESC")
                    ->group_by("$this->_table.id")
                    ->limit($limit, $offset)
                    ->from($this->_table)
                    ->join("$this->_br", "$this->_br.id = $this->_table.brand_id")
                    ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id")
                    ->get()
                    ->result();
    }

    function get_product_in_category_none_limit($list_cate_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug")
                    ->where_in("$this->_tmp_pc.category_id", $list_cate_id)
                    ->where(array("$this->_table.status" => 1, "$this->_br.status" => 1))
                    ->order_by("$this->_table.weight", "ASC") 
                    ->order_by("$this->_table.id", "DESC")
                    ->group_by("$this->_table.id")
                    ->from($this->_table)
                    ->join("$this->_br", "$this->_br.id = $this->_table.brand_id")
                    ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id")
                    ->get()
                    ->result();
    }

    function count_product_in_category($list_cate_id){        
        return $this->db->select("count($this->_table.id)")
                    ->where_in("$this->_tmp_pc.category_id", $list_cate_id)
                    ->where(array("$this->_table.status" => 1, "$this->_br.status" => 1))
                    ->order_by("$this->_table.weight", "ASC") 
                    ->order_by("$this->_table.id", "DESC")
                    ->from($this->_table)
                    ->join("$this->_br", "$this->_br.id = $this->_table.brand_id")
                    ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id")
                    ->group_by("$this->_table.id")->get()->num_rows();
    }

    function get_product_in_category_where($where = array(), $list_cate_id, $limit, $offset, $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        if(!empty($where)) $this->db->where($where);
        $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null) $this->db->order_by($options['field'], $options['sort']);
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_category_sale($list_cate_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_price($list_cate_id, $p_from, $p_to, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_sale_where($limit, $offset, $where = array()){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_price($list_cate_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_category_brand($list_cate_id, $brand_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug")
                    ->where_in("$this->_tmp_pc.category_id", $list_cate_id)
                    ->where(array("$this->_table.status" => 1, "$this->_table.brand_id" => $brand_id, "$this->_br.status" => 1))
                    ->order_by("$this->_table.weight", "ASC") 
                    ->order_by("$this->_table.id", "DESC")
                    ->group_by("$this->_table.id")
                    ->from($this->_table)
                    ->join("$this->_br", "$this->_br.id = $this->_table.brand_id")
                    ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id")
                    ->get()
                    ->result();
    }

    function get_product_where($limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        } 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_where($where = array()){
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function count_product_in_about_price($p_from, $p_to, $where = array()){
        $this->db->select("count($this->_table.id)");
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->where($where);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function count_product_search_in_about_price($list_cate_id, $keyword, $p_from, $p_to, $where = array()){
        $this->db->select("count($this->_table.id)");
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        if (!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->like("$this->_table.slug", $keyword);
        $this->db->where($where);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        if (!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_product_type($product_type_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_tmp_ppt.product_type_id", $product_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_product_type_sale($product_type_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where("$this->_tmp_ppt.product_type_id", $product_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_product_type($product_type_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_tmp_ppt.product_type_id", $product_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_product_type_price($product_type_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_tmp_ppt.product_type_id", $product_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_hair_type($hair_type_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pht.hair_type_id", $hair_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_hair_type_sale($hair_type_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where("$this->_tmp_pht.hair_type_id", $hair_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_hair_type($hair_type_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pht.hair_type_id", $hair_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_hair_type_price($hair_type_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_tmp_pht.hair_type_id", $hair_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_skin_type($skin_type_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pst.skin_type_id", $skin_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_skin_type_sale($skin_type_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where("$this->_tmp_pst.skin_type_id", $skin_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_skin_type($skin_type_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pst.skin_type_id", $skin_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_skin_type_price($skin_type_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_tmp_pst.skin_type_id", $skin_type_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_action($action_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pa.action_id", $action_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_action_sale($action_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where("$this->_tmp_pa.action_id", $action_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_action($action_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pa.action_id", $action_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_action_price($action_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_tmp_pa.action_id", $action_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_capacity($capacity_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pca.capacity_id", $capacity_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_capacity_sale($capacity_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where("$this->_tmp_pca.capacity_id", $capacity_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_capacity($capacity_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pca.capacity_id", $capacity_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_capacity_price($capacity_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_tmp_pca.capacity_id", $capacity_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_weigh($weigh_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pw.weigh_id", $weigh_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_weigh_sale($weigh_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where("$this->_tmp_pw.weigh_id", $weigh_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_weigh($weigh_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_tmp_pw.weigh_id", $weigh_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_weigh_price($weigh_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_tmp_pw.weigh_id", $weigh_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_pill_number($pill_number_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_tmp_ppn.pill_number_id", $pill_number_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function get_product_in_pill_number_sale($pill_number_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where("$this->_tmp_ppn.pill_number_id", $pill_number_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_pill_number($pill_number_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_tmp_ppn.pill_number_id", $pill_number_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_pill_number_price($pill_number_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_tmp_ppn.pill_number_id", $pill_number_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_product_in_origin($origin_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_table.origin_id", $origin_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        return $this->db->get()->result();
    }

    function get_product_in_origin_sale($origin_id, $limit, $offset){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");
        $this->db->where("$this->_table.origin_id", $origin_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        return $this->db->get()->result();
    }

    function count_product_in_origin($origin_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_table.origin_id", $origin_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_origin_price($origin_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_table.origin_id", $origin_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        return $this->db->count_all_results();
    }

    function product_filter_param($param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $limit, $offset, $where = array(), $options = null, $option_tag = null){

        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);

        switch ($param_type) {

            case 'category':
                $this->db->where_in("$this->_tmp_pc.category_id", $param_id);
                break;
                
            case 'brand':
                $this->db->where_in("$this->_table.brand_id", $param_id);
                break;

            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $param_id);
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $param_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $param_id);
                break;

            case 'origin':
                $this->db->where_in("$this->_table.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $param_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $param_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $param_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $param_id);
                break;

            case 'tags':
                $this->db->where_in("$this->_tmp_ptg.tag_id", $param_id);
                break;
            
        }

        if(!empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if($option_tag != null){
            $this->db->where_in("$this->_tmp_ptg.tag_id", $option_tag);
            $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
        } 

        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to"; 
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        
        switch ($param_type) {

            case 'category':
                $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
                break;
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
                break;
        }

        if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function product_filter_param_sale($param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $limit, $offset, $where = array(), $options = null){
        
        $this->db->select("count($this->_table.id)");
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");

        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        switch ($param_type) {

            case 'category':
                $this->db->where_in("$this->_tmp_pc.category_id", $param_id);
                break;
            case 'brand':
                $this->db->where_in("$this->_table.brand_id", $param_id);
                break;

            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $param_id);
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $param_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $param_id);
                break;

            case 'origin':
                $this->db->where_in("$this->_table.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $param_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $param_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $param_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $param_id);
                break;

            case 'tags':
                $this->db->where_in("$this->_tmp_ptg.tag_id", $param_id);
                break;
            
        }

        if(!empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to"; 
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }

        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        
        switch ($param_type) {

            case 'category':
                $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
                break;
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
                break;
        }

        if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_filter_param($param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $where = array(), $option_tag = null){

        $this->db->select("count($this->_table.id)");
        $this->db->where($where);

        switch ($param_type) {

            case 'category':
                $this->db->where_in("$this->_tmp_pc.category_id", $param_id);
                break;
            case 'brand':
                $this->db->where_in("$this->_table.brand_id", $param_id);
                break;

            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $param_id);
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $param_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $param_id);
                break;

            case 'origin':
                $this->db->where_in("$this->_table.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $param_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $param_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $param_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $param_id);
                break;

            case 'tags':
                $this->db->where_in("$this->_tmp_ptg.tag_id", $param_id);
                break;
            
        }

        if(!empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if($option_tag != null){
            $this->db->where_in("$this->_tmp_ptg.tag_id", $option_tag);
            $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
        }
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to"; 
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));      
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        
        switch ($param_type) {

            case 'category':
                $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
                break;
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
                break;
        }

        if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }


    function product_filter_param_search($keyword, $list_cate_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        
        $this->db->like("$this->_table.slug", $keyword);
        $this->db->where($where);

        if(!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);

        if(!empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to"; 
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        
        if(!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function product_filter_param_sale_search($keyword, $list_cate_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $limit, $offset, $where = array(), $options = null){
        
        $this->db->select("count($this->_table.id)");
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug, ($this->_table.price / $this->_table.price_sale) as percent");

        $this->db->like("$this->_table.slug", $keyword);
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if (!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);

        if(!empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to"; 
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }

        $this->db->order_by("percent", "DESC"); 
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        
        if(!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");

        if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_filter_param_search($keyword, $list_cate_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $where = array()){

        $this->db->select("count($this->_table.id)");
        $this->db->like("$this->_table.slug", $keyword);
        $this->db->where($where);

        if(!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);

        if(!empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to";  
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));      
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
 
        if(!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function get_list_price($arr_id){
        return $this->db->select('p_from, p_to')
                ->where_in('id', $arr_id)
                ->from('price_search')
                ->get()
                ->result();
    }

    function get_product_in_tags($tag_id, $limit, $offset, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_table.sale, $this->_table.gift_type, $this->_table.thumb, $this->_table.price, $this->_table.price_sale, $this->_table.folder, $this->_table.summary, $this->_br.name as br_name, $this->_br.slug as br_slug");
        $this->db->where($where);
        $this->db->where("$this->_tmp_ptg.tag_id", $tag_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
        return $this->db->get()->result();
    }

    function count_product_in_tags($tag_id, $where = array()){  
        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->where("$this->_tmp_ptg.tag_id", $tag_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC");
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->num_rows();
    }

    function count_product_in_tags_price($tag_id, $p_from, $p_to){
        $this->db->select("count($this->_table.id)");
        $this->db->where("$this->_tmp_ptg.tag_id", $tag_id);
        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function count_product_price_param($param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $p_from, $p_to, $where = array(), $option_tag = null){
        $this->db->select("count($this->_table.id)");
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);

        switch ($param_type) {

            case 'category':
                $this->db->where_in("$this->_tmp_pc.category_id", $param_id);
                break;
            case 'brand':
                $this->db->where_in("$this->_table.brand_id", $param_id);
                break;

            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $param_id);
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $param_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $param_id);
                break;

            case 'origin':
                $this->db->where_in("$this->_table.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $param_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $param_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $param_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $param_id);
                break;

            case 'tags':
                $this->db->where_in("$this->_tmp_ptg.tag_id", $param_id);
                break;
            
        }

        if(!empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;

                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to"; 
                
                
                $i++;
            }
            $this->db->where("($price_where)");
        } 

        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));
        $this->db->where($where);
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->group_by("$this->_table.id");
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");

        if($option_tag != null){
            $this->db->where_in("$this->_tmp_ptg.tag_id", $option_tag);
            $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
        }
        
        switch ($param_type) {

            case 'category':
                $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
                break;
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
                break;
        }

        if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");


        return $this->db->get()->num_rows();
    }

    function count_product_filter_param_list_id($list_id, $tpl, $param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $where = array(), $option_tag = null){

        $this->db->select("count($this->_table.id)");
        $this->db->where($where);

        switch ($tpl) {
            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $list_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $list_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $list_id); 
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $list_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $list_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $list_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $list_id);
                break;
        }

        switch ($param_type) {

            case 'category':
                $this->db->where_in("$this->_tmp_pc.category_id", $param_id);
                break;
            case 'brand':
                $this->db->where_in("$this->_table.brand_id", $param_id);
                break;

            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $param_id);
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $param_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $param_id);
                break;

            case 'origin':
                $this->db->where_in("$this->_table.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $param_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $param_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $param_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $param_id);
                break;

            case 'tags':
                $this->db->where_in("$this->_tmp_ptg.tag_id", $param_id);
                break;
            
        }

        if($tpl != 'brand' && !empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if($tpl != 'origin' && !empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if($tpl != 'product_type' && !empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if($tpl != 'skin_type' && !empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if($tpl != 'hair_type' && !empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if($tpl != 'action' && !empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if($tpl != 'capacity' && !empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if($tpl != 'weigh' && !empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if($tpl != 'pill_number' && !empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to"; 
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));        
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");

        if($option_tag != null){
            $this->db->where_in("$this->_tmp_ptg.tag_id", $option_tag);
            $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
        }
        
        switch ($tpl) {
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                $this->db->join("$this->_pt", "$this->_pt.id = $this->_tmp_ppt.product_type_id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                $this->db->join("$this->_a", "$this->_a.id = $this->_tmp_pa.action_id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id"); 
                $this->db->join("$this->_cp", "$this->_cp.id = $this->_tmp_pca.capacity_id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id"); 
                $this->db->join("$this->_st", "$this->_st.id = $this->_tmp_pst.skin_type_id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id"); 
                $this->db->join("$this->_ht", "$this->_ht.id = $this->_tmp_pht.hair_type_id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");  
                $this->db->join("$this->_ppn", "$this->_ppn.id = $this->_tmp_ppn.pill_number_id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");  
                $this->db->join("$this->_w", "$this->_w.id = $this->_tmp_pw.weigh_id"); 
                break;
        }
        
        switch ($param_type) {

            case 'category':
                $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
                break;
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
                break;
        }

        if($tpl != 'product_type' && !empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if($tpl != 'skin_type' && !empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if($tpl != 'hair_type' && !empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if($tpl != 'action' && !empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if($tpl != 'capacity' && !empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if($tpl != 'weigh' && !empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if($tpl != 'pill_number' && !empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function count_product_price_param_search($keyword, $list_cate_id, $param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $p_from, $p_to, $where = array()){
        $this->db->select("count($this->_table.id)");
        $this->db->where('price >=', $p_from);
        if($p_to != 0)$this->db->where('price <', $p_to);

        $this->db->like("$this->_table.slug", $keyword);
        $this->db->where($where);

        if(!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);

        if(!empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to";  
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));        
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
 
        if(!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

    function count_product_filter_param_list_id_search($keyword, $list_cate_id, $list_id, $tpl, $param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $where = array()){

        $this->db->select("count($this->_table.id)");
        $this->db->where($where);
        $this->db->like("$this->_table.slug", $keyword);

        switch ($tpl) {
            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $list_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $list_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $list_id); 
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $list_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $list_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $list_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $list_id);
                break;
        }

        switch ($param_type) {

            case 'category':
                $this->db->where_in("$this->_tmp_pc.category_id", $param_id);
                break;
            case 'brand':
                $this->db->where_in("$this->_table.brand_id", $param_id);
                break;

            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $param_id);
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $param_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $param_id);
                break;

            case 'origin':
                $this->db->where_in("$this->_table.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $param_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $param_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $param_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $param_id);
                break;

            case 'tags':
                $this->db->where_in("$this->_tmp_ptg.tag_id", $param_id);
                break;
            
        }

        if(!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        if($tpl != 'brand' && !empty($brand_value)) $this->db->where_in("$this->_table.brand_id", $brand_value);
        if($tpl != 'origin' && !empty($origin_value)) $this->db->where_in("$this->_table.origin_id", $origin_value);
        if($tpl != 'product_type' && !empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if($tpl != 'skin_type' && !empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if($tpl != 'hair_type' && !empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if($tpl != 'action' && !empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if($tpl != 'capacity' && !empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if($tpl != 'weigh' && !empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if($tpl != 'pill_number' && !empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to";  
                
                $i++;
            }
            $this->db->where("($price_where)");
        }  

        $this->db->where(array("$this->_table.status" => 1, "$this->_br.status" => 1));        
        $this->db->from($this->_table);
        $this->db->join("$this->_br", "$this->_br.id = $this->_table.brand_id");
        
        switch ($tpl) {
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                $this->db->join("$this->_pt", "$this->_pt.id = $this->_tmp_ppt.product_type_id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                $this->db->join("$this->_a", "$this->_a.id = $this->_tmp_pa.action_id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id"); 
                $this->db->join("$this->_cp", "$this->_cp.id = $this->_tmp_pca.capacity_id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id"); 
                $this->db->join("$this->_st", "$this->_st.id = $this->_tmp_pst.skin_type_id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id"); 
                $this->db->join("$this->_ht", "$this->_ht.id = $this->_tmp_pht.hair_type_id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");  
                $this->db->join("$this->_ppn", "$this->_ppn.id = $this->_tmp_ppn.pill_number_id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");  
                $this->db->join("$this->_w", "$this->_w.id = $this->_tmp_pw.weigh_id"); 
                break;
        }
        
        switch ($param_type) {

            case 'category':
                $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
                break;
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_table.id");
                break;
        }

        if(!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_table.id");
        if($tpl != 'product_type' && !empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_table.id");
        if($tpl != 'skin_type' && !empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_table.id");
        if($tpl != 'hair_type' && !empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_table.id");
        if($tpl != 'action' && !empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_table.id");
        if($tpl != 'capacity' && !empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_table.id");
        if($tpl != 'weigh' && !empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_table.id");
        if($tpl != 'pill_numbe' && !empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_table.id");
        $this->db->group_by("$this->_table.id");return $this->db->get()->num_rows();
    }

}