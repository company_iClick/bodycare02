<?php

class M_weigh extends CI_Model
{
    protected $_table = 'weigh';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_weigh($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function get_weighID($id)
    {
        $this->db->where('id', $id);
        $this->db->from($this->_table);
        return $this->db->get()->row();
    }    
    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    function count_product($weigh_id)
    {
         $this->db->select('product.id');
         $this->db->where('tmp_product_weigh.weigh_id', $weigh_id);
         $this->db->group_by('product.id');
         $this->db->from('product');
         $this->db->join('tmp_product_weigh', 'tmp_product_weigh.product_id=product.id');
         return $this->db->count_all_results();
    }
}

