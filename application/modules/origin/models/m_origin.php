<?php
class M_origin extends CI_Model
{
    protected $_table = 'origin';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_origin($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function get_originID($id)
    {
        $this->db->where('id', $id);
        $this->db->from($this->_table);
        return $this->db->get()->row();
    }    
    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }
}

