<?php

class Origin extends MX_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->model(array("category/a_category", "origin/a_origin"));
	}

	function index()
	{
		$data['title_bar'] = 'Xuất xứ';

        $data['info'] = $info = $this->global_function->get_tableWhere(array("id"=>1),"company","title, descriptions, logo, phone");

        $data['breadcrumb'] = '<a href="'. current_url() .'">'. $data['title_bar'] .'</a>';

      	$list_origin = $this->a_origin->get_all_item_category();
      	$array_cate_node = array();
      	if(!empty($list_origin)){
      		
      		foreach ($list_origin as $row) {
      			$array_cate[$row->parent_id][] = $row;
      		}
      		
      		if(!empty($array_cate)){
      			
      			foreach ($array_cate as $key => $value) {
      				$array_node = $this->a_category->get_parent_category_array($key);
      				if(!empty($array_node)){
                  $array_node_res = $array_node[count($array_node) - 1];            
                  $array_cate_node[$array_node_res['id'] . ',' . $array_node_res['name'] . ',' . $array_node_res['slug']][] = $value;
              }
      			}
      		}      		
      	}

      	$data['array_cate_node'] = $array_cate_node;

        $data['share_face'] = 
		'<meta property="og:type" content="website" />
		<meta property="og:url" content="'.current_url().'"/>
		<meta property="og:title" content="'.$info->title.'"/>
		<meta property="og:description" content="'.$info->descriptions.'" />
		<meta property="og:image" content="'.base_url()._upload_default.$info->logo.'"/>
		<meta property="og:image:type" content="image/png">';

        $this->template->write('mod','origin');
        $this->template->write('title',$data['title_bar']);
        $this->template->write_view('content','public/index', $data, TRUE);
        $this->template->render();
	}

 
}
?>