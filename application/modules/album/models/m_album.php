<?php



class M_album extends CI_Model {



    function __construct() {

        parent::__construct();

        $this->load->database();

    }

    function show_list_album($where = array(), $limit, $offset) {

        $this->db->where($where);

        $this->db->order_by('id', "ASC");

        $this->db->order_by('weight', "ASC");

        $this->db->limit($limit, $offset);

        $this->db->from('album');

        return $this->db->get()->result();

        $this->db->free_result();

    }



    // get id

    function get_albumID($id) {

        $this->db->where('album.id', $id);

        $this->db->from('album');

        return $this->db->get()->row();

    }

    // get tmp

    function show_list_tmp_table_where($where=array()){

        $this->db->where($where);

        $this->db->from('album');

        $this->db->join("tmp_users_post_album","tmp_users_post_album.album_id=album.id");

        return $this->db->get()->result();

        $this->db->free_result();

    }

}

