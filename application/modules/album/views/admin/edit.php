<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
?>
<div class="contentcontainer med left">

    <div class="headings altheading"><h2>Edit</h2></div>

    <div class="contentbox">
        <form name="them" method="post" id="them" action=""	enctype="multipart/form-data">
            <!-- -----------------Danh muc theo ngon ngu------------------------------- -->
                <p>
                    <label for="textfield"><strong>Tên Danh Mục</strong></label>
                    <input type="text" id="textfield"	class="inputbox" name="name" value="<?php echo $item->name ?>" />
                </p>
                <?php echo form_error('name'); ?>

            <!-- ---------------------------------------------------------------------- -->

            <!-- -------------------Set vi tri----------------------------------------- -->
            <p>
                <label for="smallbox"><strong>Thứ Tự: </strong></label>
                <input type="text" name="weight"  value="<?php echo $item->weight ?>" style="width: 30px; text-align: center" class="inputbox">
                <br>
                <span class="smltxt">(Số thứ tự càng lớn, độ ưu tiên càng cao)</span>
            </p>
            <p>
                <input type="radio" value="1" name="status" <?php if ($item->status == 1) { ?>checked<?php } ?>>Hiển thị bài
                <input type="radio" value="0" name="status" <?php if ($item->status == 0) { ?>checked<?php } ?>>Ẩn bài
            </p>
            <!-- ---------------------------------------------------------------------- -->

            <input type="submit" value="Thêm mới" name="ok" class="btn" />


        </form>

    </div><!-- end contentbox -->

</div>