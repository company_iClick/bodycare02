<?php
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->load->model(array(
            "order/m_order",
            "general",
            "global_function"
        ));
        $this->template->set_template('admin');
    }
    function index($page_no = 1)
    {
        if (!($this->general->Checkpermission("view"))) {
            redirect(base_url("admin/not-permission"));
        } else {
            if (isset($_POST['show']) && $this->input->post('checkall') != "") {
                $array = array_keys($this->input->post('checkall'));
                foreach ($array as $a) {
                    $this->show_more($a);
                }
                redirect(base_url('admin/order/index/' . $page_no) . '?messager=success');
            }
            if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
                $array = array_keys($this->input->post('checkall'));
                foreach ($array as $a) {
                    $this->hide_more($a);
                }
                redirect(base_url('admin/order/index/' . $page_no) . '?messager=success');
            }
            if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
                $array = array_keys($this->input->post('checkall'));
                foreach ($array as $a) {
                    $this->delete_more($a);
                }
                redirect(base_url('admin/order/index/' . $page_no) . '?messager=success');
            }
            $page_co         = 30;
            $start           = ($page_no - 1) * $page_co;
            $count           = $this->general->count_table_where(array(), "tpl_order");
            $data['page_no'] = $page_no;
            $data['list']    = $this->m_order->show_list_order_where(array('vip' => 0), $page_co, $start);
            $data['keyword'] = $this->input->post('keyword');
            if ($this->input->post('keyword') != '') {
                $keyword      = $this->input->post('keyword');
                $data['list'] = $this->m_order->show_list_order_search($keyword, $page_co, $start);
                $count        = $this->m_order->count_list_order_search($keyword);
            }
            $data['link'] = $this->general->paging($page_co, $count, 'admin/order/index/', $page_no);
            $this->template->write('mod', "order");
            $this->template->write_view('content', 'admin/index', $data, TRUE);
            $this->template->render();
        }
    }

    public function date($date){
        $data['list']=$this->m_order->show_list_order_date($date, array('vip' => 0));
        $data['date'] = date('d/m/Y', strtotime($date));
        $this->template->write('mod', "order");
        $this->template->write_view('content', 'admin/date', $data, TRUE);
        $this->template->render();
    }

    function edit($id)
    {
        if (!($this->general->Checkpermission("view"))) {
            redirect(base_url("admin/not-permission"));
        } else {
            $data['title']    = "MyProfile";
            $data['item']     = $this->general->get_tableID($id, "tpl_order");
            $data['user']     = $this->global_function->get_row_object(array(
                'id' => $data['item']->member_id
            ), '*', 'users');
            $data['products'] = json_decode($data['item']->order_info);
            $data['id']       = $id;
            $data['status']   = $this->general->get_list_table("order_status");

            $data['address'] = $data['item']->address;

            $district = $this->global_function->get_row_object(array('id' => $data['item']->district_id), 'name', 'district');
            if(!empty($district)) $data['address'] .= ' - ' . $district->name;

            $province = $this->global_function->get_row_object(array('id' => $data['item']->province_id), 'name', 'province');
            if(!empty($province)) $data['address'] .= ' - ' . $province->name;
            
            if (isset($_REQUEST['ok'])) {
                $sql = array(
                    "order_status_id" => $this->input->post("status")
                );

                if(!empty($data['user'])){
                    $sql['score'] = $this->input->post("score");

                    $user_score = $data['user']->score;

                    $new_score = $user_score + $sql['score'];
                    $this->db->where("id", $data['user']->id);
                    $this->db->update("users", array('score' => $new_score));
                }

                $this->db->where("id", $id);
                $this->db->update("tpl_order", $sql);



                redirect(base_url('admin/order/edit/' . $id) . '?messager=success');
            }
            $this->template->write('mod', "order");
            $this->template->write_view('content', 'admin/view', $data, TRUE);
            $this->template->render();
        }
    }
    function delete($id, $page_no)
    {
        if (!($this->general->Checkpermission("delete"))) {
            redirect(base_url("admin/not-permission"));
        } else {
            $this->db->delete('tpl_order', array(
                'id' => $id
            ));
            redirect(base_url('admin/order/index/' . $page_no) . '?messager=success');
        }
    }
    function hide($id, $page_no = 1)
    {
        if (!($this->general->Checkpermission("edit"))) {
            redirect(base_url("admin/not-permission"));
        } else {
            $this->general->update_tableID($id, array(
                'status' => 0
            ), "tpl_order");
            redirect(base_url('admin/order/index/' . $page_no) . '?messager=success');
        }
    }
    function show($id, $page_no = 1)
    {
        if (!($this->general->Checkpermission("edit"))) {
            redirect(base_url("admin/not-permission"));
        } else {
            $this->general->update_tableID($id, array(
                'status' => 1
            ), "tpl_order");
            redirect(base_url('admin/order/index/' . $page_no) . '?messager=success');
        }
    }
    function delete_more($id)
    {
        if (!($this->general->Checkpermission("delete"))) {
            redirect(base_url("admin/not-permission"));
        } else {
            $this->db->delete('tpl_order', array(
                'id' => $id
            ));
            return true;
        }
    }
    function hide_more($id)
    {
        if (!($this->general->Checkpermission("edit"))) {
            redirect(base_url("admin/not-permission"));
        } else {
            $this->general->update_tableID($id, array(
                'status' => 0
            ), "tpl_order");
            return true;
        }
    }
    function show_more($id)
    {
        if (!($this->general->Checkpermission("edit"))) {
            redirect(base_url("admin/not-permission"));
        } else {
            $this->general->update_tableID($id, array(
                'status' => 1
            ), "tpl_order");
            return true;
        }
    }
}
