<?php

class M_order extends CI_Model {

    protected $_table = 'tpl_order';
    
    function show_list_order_where($where = array(), $limit, $offset) {

        $this->db->select($this->_table.".*, order_status.name as order_status_name");

        $this->db->where($where);

        $this->db->order_by($this->_table.'.id', "DESC");

        $this->db->limit($limit, $offset);

        $this->db->from($this->_table);
        
        $this->db->join('order_status','order_status.id='.$this->_table.'.order_status_id');

        return $this->db->get()->result();

    }

    function show_list_order_all_date($where = array()) {

        $this->db->select($this->_table.".*, order_status.name as order_status_name");
        $this->db->where("DATE(`created_at`) >= CURDATE()");
        $this->db->where($where);
        $this->db->order_by($this->_table.'.id', "DESC");
        $this->db->from($this->_table);      
        $this->db->join('order_status','order_status.id='.$this->_table.'.order_status_id');
        return $this->db->get()->result();

    }

    function show_list_order_date($date, $where = array()) {

        $this->db->select($this->_table.".*, order_status.name as order_status_name");
        $this->db->where("DATE(`created_at`) = '$date'");
        $this->db->where($where);
        $this->db->order_by($this->_table.'.id', "DESC");
        $this->db->from($this->_table);      
        $this->db->join('order_status','order_status.id='.$this->_table.'.order_status_id');
        return $this->db->get()->result();

    }

    function show_order_today($where = array()) {
        $this->db->select($this->_table.".*, order_status.name as order_status_name");
        $this->db->where("DATE(`created_at`) >= CURDATE()");
        $this->db->where($where);
        $this->db->order_by($this->_table.'.id', "DESC");
        $this->db->from($this->_table);      
        $this->db->join('order_status','order_status.id='.$this->_table.'.order_status_id');
        return $this->db->get()->result();

    }

    
    function show_list_order_search($keyword, $limit, $offset) {

        $this->db->select($this->_table.".*, order_status.name as order_status_name");

        $this->db->like('full_name', $keyword);
        $this->db->or_like('email', $keyword);
        $this->db->or_like('phone', $keyword);
        $this->db->order_by($this->_table.'.id', "DESC");

        $this->db->limit($limit, $offset);

        $this->db->from($this->_table);
        
        $this->db->join('order_status','order_status.id='.$this->_table.'.order_status_id');

        return $this->db->get()->result();

    }
    
    function count_list_order_search($keyword) {
        $this->db->select($this->_table.".*, order_status.name as order_status_name");
        $this->db->like('full_name', $keyword);
        $this->db->or_like('email', $keyword);
        $this->db->or_like('phone', $keyword);
        $this->db->order_by($this->_table.'.id', "DESC");
        $this->db->from($this->_table);      
        $this->db->join('order_status','order_status.id='.$this->_table.'.order_status_id');
        return $this->db->count_all_results();
    }



    // Tim kiem

    function count_list_user_where_ajax($param) {

        $this->db->select("*");

        if(isset($param['key']) && $param['key']!='null'){

            $this->db->or_like("full_name",$param['key']);

            $this->db->or_like("email",$param['key']);

        }

        if(isset($param['status']) && $param['status']!='null'){

            $this->db->where("status",$param['status']);

        }

        $this->db->where("type",$param['type']);

        $this->db->order_by('order.id', "DESC");

        $this->db->from('order');

        return $this->db->get()->num_rows();

    }

    function show_list_user_where_ajax($param, $limit, $offset) {

        $this->db->select("*");

        if(isset($param['key']) && $param['key']!='null'){

            $this->db->or_like("full_name",$param['key']);

            $this->db->or_like("email",$param['key']);

        }

        if(isset($param['status']) && $param['status']!='null'){

            $this->db->where("status",$param['status']);

        }

        $this->db->order_by('order.id', "DESC");

        $this->db->limit($limit, $offset);

        $this->db->from('order');

        return $this->db->get()->result();

    }



}

