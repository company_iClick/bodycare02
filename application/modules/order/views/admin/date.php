<?php

$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager'), 'id' => $this->input->get('id'), 'type' => $this->input->get('type')));

?>

<!-- Alternative Content Box Start -->

<div class="contentcontainer">

    <div class="headings altheading"><h2>Danh sách đơn hàng ngày <?php echo $date; ?></h2></div>

    <div class="contentbox">

        <div id="show_jax">

            <div class="extrabottom">


                <div style="clear: both; height: 10px"></div>

            <table width="100%">

                <thead>

                    <tr>

                        <th width="3%">STT</th>
                        <th>IP</th>
                        <th>Thông tin thiết bị</th>
                        <th>Mã đơn hàng</th>
                        <th>Số điện thoại</th>
                        <th width="7%">Email</th>
                        <th width="20%">Ghi chú</th>
                        <th>Thời gian mua</th>
                        <th>Phương thức thanh toán</th>
                        <th>Điểm tích lũy</th>
                        <th>Tình trạng</th>
                        <th width="9%">Action</th>

                        <th width="2%"><input name="" type="checkbox" value="" id="checkboxall" /></th>

                    </tr>

                </thead>

                <div style="clear: both; height: 10px"></div>

                <tbody id="load">

                    <?php

                    $x = 0;

                    foreach ($list as $i) {

                        ?>

                        <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?> id="app-<?php echo $i->id?>">

                            <td style="text-align:center"><?php echo $x + 1 ?></td>
                            <td><?php echo $i->ip_address?></td>
                            <td><?php echo $i->user_agent?></td>
                            <td>

                                <a data="" title="Permition" href="admin/order/edit/<?php echo $i->id ?>"  style="cursor: pointer">

                                    <b><?php echo $i->code ?></b>

                                </a>

                            </td>

                            <td><?php echo $i->phone?></td>
                            <td><?php echo $i->email?></td>
                            <td><?php echo $i->note?></td>
                            <td><?php echo date('H:i d/m/Y', strtotime($i->created_at));?></td>
                            <td><?php echo $i->pay?></td>
                            <td style="text-align:center;"><?php echo $i->score?></td>
                            <td><?php echo $i->order_status_name?></td>

                            <td style="text-align:center;">

                                <a href="admin/order/edit/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_edit.png" alt="Edit" /></a>

                                <?php if ($i->status == 1) { ?>

                                    <a href="admin/order/hide/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_approve.png" alt="Approve" /></a>

                                <?php } else { ?>

                                    <a href="admin/order/show/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" /></a>

                                <?php } ?>



                                <a data="" title="Delete" onclick="Alert('admin/order/delete/<?php echo $i->id ?>/1')"  style="cursor: pointer"><img src="theme_admin/img/icons/icon_square_close.png" alt="Delete" /></a>



                            </td>

                            <td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $i->id ?>]" name="checkall[<?php echo $i->id ?>]" class="checkall"  /></td>

                        </tr>            

                        <?php $x++;  }?>


                </tbody>

            </table>
                

            <div style="clear: both;"></div>

            <p style="display:none">

                <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>

            </p>

     

        <div style="clear: both"></div>

    </div>

    </div>

</div>

<div id="div_che"></div>

