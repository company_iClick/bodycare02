<div class="contentcontainer">

    <div class="headings altheading"><h2>Thông tin khách hàng</h2></div>

    <div class="contentbox">
        <div style="float: left;width: 49%;overflow: auto;">
        <table style="width: 100%">

            <tr><th colspan="2" style="text-align: center; font-weight: bold">THÔNG TIN KHÁCH HÀNG</th></tr>
            
            <tr width="30%">
                <td>IP: </td>
                <td><?php echo $item->ip_address ?></td>
            </tr>
            
            <tr width="30%">
                <td>Thông tin thiết bị: </td>
                <td><?php echo $item->user_agent ?></td>
            </tr>

            <tr>
                <td width="30%">Họ & Tên: </td>
                <td><?php echo $item->full_name ?></td>
            </tr>
          
            <tr>
                <td>Email: </td>
                <td><?php echo $item->email ?></td>
            </tr>

            <tr>
                <td>Địa chỉ: </td>
                <td><?php echo $address; ?></td>
            </tr>

            <tr>
                <td>Điện thoại: </td>
                <td><?php echo $item->phone ?></td>
            </tr>

            <tr>
                <td>Thời gian đặt hàng: </td>
                <td><?php echo date('H:i d/m/Y', strtotime($item->created_at)) ?></td>
            </tr>
            <tr>
                <td>Mã đơn hàng: </td>
                <td><?php echo $item->code; ?></td>
            </tr>
            <tr>
                <td>Phương thức thanh toán: </td>
                <td><?php echo $item->pay; ?></td>
            </tr>
            <tr>
                <td>Ghi chú: </td>
                <td><?php echo $item->note ?></td>
            </tr>
            
        </table>
        </div>
        
        <div style="float: right;width: 49%;overflow: auto;">
        <table style="width: 100%">

            <tr><th colspan="2" style="text-align: center; font-weight: bold">THÔNG TIN THÀNH VIÊN</th></tr>
            <?php if(!empty($user)){ ?>
            <tr>
                <td width="30%">Họ & Tên: </td>
                <td><?php echo $user->full_name ?></td>
            </tr>
          
            <tr>
                <td>Email: </td>
                <td><?php echo $user->email ?></td>
            </tr>

            <tr>
                <td>Địa chỉ: </td>
                <td><?php echo $user->address ?></td>
            </tr>

            <tr>
                <td>Điện thoại: </td>
                <td><?php echo $user->phone ?></td>
            </tr>
            <?php }else{ ?>
            <tr>
                <td colspan="2">Không phải thành viên đặt hàng</td>
            </tr>
            <?php } ?>
            
        </table>
        </div>

        <div style="clear: both; height: 20px"></div>
        
        <h3 style="float: left;margin:10px 0">Thông tin sản phẩm</h3>
        <table style="width: 100%" id="t-order">
            <thead>
            
            <tr><th>Hình sản phẩm</th>
            <th>Tên sản phẩm</th>
            <th>Đơn giá</th>
            <th>Số lượng</th>
            <th>Thành tiền</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product){ 
                $name = $product->product;
                $link = $product->link;
                $imgProduct = $product->images;
                $price = number_format($product->price, 0, ',', '.') . ' vnđ';
                $qty = $product->qty;
                $total_price= number_format($product->subtotal, 0, ',', '.') . ' vnđ';
            ?>
            <tr class="alt">
                <td align="center"><a target="_blank" href="<?php echo $link; ?>"><img src="<?php echo $imgProduct; ?>" width="100"></a></td>
                <td align="center"><a target="_blank" href="<?php echo $link; ?>"><?php echo $name; ?></a></td>
                <td align="center"><?php echo $price; ?></td>
                <td align="center"><?php echo $qty; ?></td>
                <td align="center"><?php echo $total_price; ?></td>
            </tr>
            <?php } ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td class="red align-right">Tổng tiền: </td>
                <td><?php echo number_format($item->total, 0, ',', '.') . ' vnđ'; ?></td>
            </tr>
     
            </tbody>
        </table>
        
        <div style="clear: both; height:10px;"></div>
        <form action="" method="post">
            <?php if(!empty($user)): ?>
            <label>Cộng điểm tích lũy</label>
            <input type="text" name="score" class="inputbox" value="<?php echo $item->score; ?>"><br><br>
            <?php endif; ?>
            <ul id="list-status">
            <?php foreach($status as $st){?>
                <li style="white-space: nowrap"><input type="radio" name="status" <?php if($st->id==$item->order_status_id){?> checked <?php }?> value="<?php echo $st->id?>"><?php echo $st->name?></li>
            <?php }?>
            </ul>
            <div style="clear: both; height: 10px"></div>
            <input type="submit" class="btn" value="Cập nhật" name="ok"/>
            <a href="admin/order/index" class="btn" style="color: #fff; text-decoration: none">Quay lại</a>
        </form>
    </div>

</div>
<style>
    .col-one-order {
        float: left
    }

    .col-one-order p {
        margin: 0px;
        padding: 5px; white-space: nowrap;
    }
    #t-order tr td{ text-align: center}
    #list-status li{ display: inline-block}
</style>
