<?php

$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager'), 'id' => $this->input->get('id'), 'type' => $this->input->get('type')));

?>

<!-- Alternative Content Box Start -->

<div class="contentcontainer">

    <div class="headings altheading"><h2>Danh sách đơn hàng</h2></div>

    <div class="contentbox">
        
        <form name="frm_search_order" method="post" action="" style="margin-bottom: 20px">
            <input type="text" class="inputbox" value="<?php echo $keyword; ?>" placeholder="Nhập số điện thoại, email..." name="keyword">
            <input type="submit" class="btn" value="Tìm kiếm" style="float: none">
        </form>
        <div id="show_jax">

            <div class="extrabottom">

                <ul>

                    <li> <img src="theme_admin/img/icons/icon_approve.png" alt="Approve" />

                        <input class="a_button_act a_show" name="show" style="cursor: pointer" type="submit"	value="Hiện danh sách đã chọn" />

                    </li>

                    <li> <img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" />

                        <input class="a_button_act a_hide" name="hide" style="cursor: pointer" type="submit"	value="Ẩn danh sách đã chọn" />

                    </li>

                    <li> <img src="theme_admin/img/icons/icon_delete.png" alt="Delete" />

                        <p style="display:none">

                            <input class="a_button_act a_delete" name="delete" style="cursor: pointer" type="submit"	value="Delete" />

                        </p>

                        <a style=" cursor:pointer" class="delete" onclick="Delete()">Delete</a>

                    </li>

                </ul>

                <?php echo $link ?>

                <div style="clear: both; height: 10px"></div>

            <table width="100%">

                <thead>

                    <tr>

                        <th width="3%">STT</th>
                        <th>IP</th>
                        <th>Thông tin thiết bị</th>
                        <th>Mã đơn hàng</th>
                        <th>Số điện thoại</th>
                        <th width="7%">Email</th>
                        <th width="20%">Ghi chú</th>
                        <th>Thời gian mua</th>
                        <th>Phương thức thanh toán</th>
                        <th>Điểm tích lũy</th>
                        <th>Tình trạng</th>
                        <th width="9%">Action</th>

                        <th width="2%"><input name="" type="checkbox" value="" id="checkboxall" /></th>

                    </tr>

                </thead>

                <div style="clear: both; height: 10px"></div>

                <tbody id="load">

                    <?php

                    $x = 0;

                    foreach ($list as $i) {

                        ?>

                        <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?> id="app-<?php echo $i->id?>">

                            <td style="text-align:center"><?php echo $x + 1 ?></td>
                            <td><?php echo $i->ip_address?></td>
                            <td><?php echo $i->user_agent?></td>
                            <td>

                                <a data="" title="Permition" href="admin/order/edit/<?php echo $i->id ?>"  style="cursor: pointer">

                                    <b><?php echo $i->code ?></b>

                                </a>

                            </td>

                            <td><?php echo $i->phone?></td>
                            <td><?php echo $i->email?></td>
                            <td><?php echo $i->note?></td>
                            <td><?php echo date('H:i d/m/Y', strtotime($i->created_at));?></td>
                            <td><?php echo $i->pay?></td>
                            <td style="text-align:center;"><?php echo $i->score?></td>
                            <td><?php echo $i->order_status_name?></td>

                            <td style="text-align:center;">

                                <a href="admin/order/edit/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_edit.png" alt="Edit" /></a>

                                <?php if ($i->status == 1) { ?>

                                    <a href="admin/order/hide/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_approve.png" alt="Approve" /></a>

                                <?php } else { ?>

                                    <a href="admin/order/show/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" /></a>

                                <?php } ?>



                                <a data="" title="Delete" onclick="Alert('admin/order/delete/<?php echo $i->id ?>/<?php echo $page_no ?>')"  style="cursor: pointer"><img src="theme_admin/img/icons/icon_square_close.png" alt="Delete" /></a>



                            </td>

                            <td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $i->id ?>]" name="checkall[<?php echo $i->id ?>]" class="checkall"  /></td>

                        </tr>            

                        <?php $x++;  }?>

                <tr><td colspan="10"><?php echo $link ?></td></tr>

                </tbody>

            </table>
                

            <div style="clear: both;"></div>

            <p style="display:none">

                <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>

            </p>

     

        <div style="clear: both"></div>

    </div>

    </div>

</div>

<div id="div_che"></div>

<script type="text/javascript">

    $(document).ready(function () {

        $(".action-app").live("click",function(){

            var id=$(this).attr("data-id");

            var data_type=$(this).attr("data-type");

            var type=$(this).parent().attr("data");

            var tr=$(this).parent().parent().attr("id");

            $(this).html("<img src='theme_admin/img/icons/loading.gif'>");

            $.post("admin/order/action_employee",{id:id,data_type:data_type,type:type},function(data){

                $("#"+tr).find("."+type).find("span").html(" ");

                if(data=='1'){

                    if(data_type==0){

                        $("#"+tr).find("."+type).find("span").removeClass("approve");

                        $("#"+tr).find("."+type).find("span").addClass("un-approve");

                        $("#"+tr).find("."+type).find("span").attr("data-type","1");

                    }else{

                        $("#"+tr).find("."+type).find("span").removeClass("un-approve");

                        $("#"+tr).find("."+type).find("span").addClass("approve");

                        $("#"+tr).find("."+type).find("span").attr("data-type","0");

                    }

                }

            });

        });

        $("#btn-search").click(function(){

            var status=$("#status-id").val();

            if(status==''){

                alert("Bạn vui lòng chọn thuộc tính tìm kiếm");

                return false;

            }else{

                if(status=='') status="null";

                $.ajax({

                    url: "admin/order/order_ajax/<?php echo $type?>/"+null+"/"+status,

                    type: "GET",

                    cache: false,

                    beforeSend: function (data) {

                        $("#show_jax").append("<p id='loading-page'></p>");

                        $("#show_jax").addClass("bgwhite");

                    },

                    success: function (data) {

                        $("#show_jax").html(data);

                        $("#show_jax").removeClass("bgwhite");



                    }

                }); //end ajax

            }

        });

    });



function Key(e){

    var key=$("#key").val();

    $.ajax({

        url: "admin/order/order_key_ajax",

        type: "POST",

        data:{key:key,type:<?php echo $type?>},

        beforeSend: function (data) {

            $("#show_jax").append("<p id='loading-page'></p>");

            $("#show_jax").addClass("bgwhite");

        },

        success: function (data) {

            $("#show_jax").html(data);

            $("#show_jax").removeClass("bgwhite");



        }

    }); //end ajax

    return false

}

</script>