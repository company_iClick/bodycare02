<?php

class M_call_back extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // show list call_back
    function show_list_call_back($lang = 'vn') {
        $this->db->select("*");
        $this->db->order_by('call_back.weight', "ASC");
		$this->db->order_by('call_back.id', "DESC");
        $this->db->from('call_back');
        return $this->db->get()->result();
    }
    // show list call_back where
    function show_list_call_back_where($where=array()) {
        $this->db->select("*");
        $this->db->where($where);
        $this->db->order_by('call_back.weight', "ASC");
		$this->db->order_by('call_back.id', "DESC");
        $this->db->from('call_back');
        return $this->db->get()->result();
    }
    
}
