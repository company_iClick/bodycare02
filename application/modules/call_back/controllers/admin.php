<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model(array("call_back/m_call_back","product/m_product", "general"));
        $this->template->set_template('admin');        // Set template 
        $this->template->write('mod', "call_back"); // set mod
    }

    function index($page_no = 1) {
        if (!($this->general->Checkpermission("view_call_back")))
            redirect(base_url("admin/not-permission"));
        // tool all
        if (isset($_POST['show']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(base_url('admin/call_back') . '?messager=success');
        }
        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                //--------change parent------
                $this->hide_more($a);
            }
            redirect(base_url('admin/call_back') . '?messager=success');
        }
        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                //--------change parent------
                $this->delete_more($a);
            }
            redirect(base_url('admin/call_back') . '?messager=success');
        }
        //end toll
        $page_co = 20;
        $start = ($page_no - 1) * $page_co;
        $count = $this->general->count_tableWhere(array("id !=" => 0), "call_back");
        $data['page_no'] = $page_no;
        $data['list'] = $this->m_call_back->show_list_call_back_where(array("id !=" => 0), $page_co, $start);
        $data['link'] = $this->general->paging($page_co, $count, 'admin/call_back' . "/", $page_no);
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();
    }

    function date($date) {
        if (!($this->general->Checkpermission("view_call_back")))
            redirect(base_url("admin/not-permission"));
        $data['list']=$this->global_function->show_list_table_where("DATE(`created_at`) = '$date'", 'call_back', '*', 'id', 'DESC');
        $data['date'] = date('d/m/Y', strtotime($date));
        $this->template->write_view('content', 'admin/date', $data, TRUE);
        $this->template->render();
    }
 

//=========================================== 
    function hide($id) {
        if (!($this->general->Checkpermission("edit_call_back")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 0), "call_back");
        redirect(base_url('admin/call_back') . '?messager=success');
    }

//============================================\
    function hide_more($id) {
        if (!($this->general->Checkpermission("edit_call_back")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 0), "call_back");
        return true;
    }

//============================================\
    function show_more($id) {
        if (!($this->general->Checkpermission("edit_call_back")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 1), "call_back");
        return true;
    }

//============================================\
    function show($id) {
        if (!($this->general->Checkpermission("edit_call_back")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 1), "call_back");
        redirect(base_url('admin/call_back') . '?messager=success');
    }

// ============================================
    function delete($id) {
        if (!($this->general->Checkpermission("delete_call_back")))
            redirect(base_url("admin/not-permission"));
            $this->db->delete('call_back', array('id' => $id));
            redirect(base_url('admin/call_back') . '?messager=success');
    }

    function delete_more($id) {
        if (!($this->general->Checkpermission("delete_call_back")))
            redirect(base_url("admin/not-permission"));
            $this->db->delete('call_back', array('id' => $id));
            return true;

    }

}
