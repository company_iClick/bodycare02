<?php

class M_term extends CI_Model
{
    protected $_table = 'term';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_term($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('id', "ASC");
        $this->db->order_by('weight', "ASC");
        $this->db->limit($limit, $offset);
        $this->db->from('term');
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function get_termID($id)
    {
        $this->db->where('term.id', $id);
        $this->db->from('term');
        return $this->db->get()->row();
    }
    function show_list_tmp_table_where($where = array())
    {
        $this->db->where($where);
        $this->db->from('term');
        $this->db->join("tmp_users_post_term", "tmp_users_post_term.term_id=term.id");
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function menu_admin($parentid = 0, $space = "", $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }
        $this->db->where('parent_id', $parentid);
        $this->db->order_by("weight", "asc");
        $this->db->order_by("id", "desc");
        $result    = $this->db->get($this->_table)->result();
        $trees_obj = array();
        if (!empty($result)) {
            foreach ($result as $rs) {
                $trees[] = array(
                    'id' => $rs->id,
                    'parent_id' => $rs->parent_id,
                    'weight' => $rs->weight,
                    'status' => $rs->status,
                    'name' => $space . $rs->name,
                    'level' => $rs->level,
                    'count_article' => $this->count_article($rs->id)
                );
                $trees   = $this->menu_admin($rs->id, $space . '|---', $trees);
            }
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $tree        = (object) $tree;
                $trees_obj[] = $tree;
            }
        }
        return $trees_obj;
    }

    function get_categroy_tree_id($parentid = 0, $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }
        $trees[] = $parentid;
        $this->db->select('id');
        $this->db->where('parent_id', $parentid);
        $this->db->order_by("weight", "asc");
        $this->db->order_by("id", "desc");
        $result    = $this->db->get($this->_table)->result();
        $trees_obj = array();
        if (!empty($result)) {
            foreach ($result as $rs) {
                $trees[] = $rs->id;
                $trees   = $this->get_categroy_tree_id($rs->id, $trees);
            }
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $tree        =  $tree;
                $trees_obj[] = $tree;
            }
        }
        return $trees_obj;
    }
    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }
    function count_article($term_id)
    {
         $this->db->where('tmp_term_article.term_id', $term_id);
         $this->db->from('article');
         $this->db->join('tmp_term_article', 'tmp_term_article.article_id=article.id');
         return $this->db->count_all_results();
    }
}

