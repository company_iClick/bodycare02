<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>

<?php

$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager'), 'id' => $this->input->get('id'), 'type' => $this->input->get('type')));

?>

<!-- Alternative Content Box Start -->

<div class="contentcontainer">

    <div class="headings altheading"><h2>Danh sách</h2></div>

    <div class="contentbox">

        <form method="post" action=""	enctype="multipart/form-data">

            <div class="extrabottom">

                <ul>

                    <li>

                        <img src="theme_admin/img/icons/add.png" alt="Add" /> 

                        <a style="text-decoration: none;" href="admin/term/add/<?php echo $type?>">Thêm mới</a>

                    </li>

                    <li> <img src="theme_admin/img/icons/icon_approve.png" alt="Approve" />

                        <input class="a_button_act a_show" name="show" style="cursor: pointer" type="submit"	value="Hiện danh sách đã chọn" />

                    </li>

                    <li> <img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" />

                        <input class="a_button_act a_hide" name="hide" style="cursor: pointer" type="submit"	value="Ẩn danh sách đã chọn" />

                    </li>

                    <li> <img src="theme_admin/img/icons/icon_delete.png" alt="Delete" />
                        <p style="display:none">
                           <input class="a_button_act a_delete" name="delete" style="cursor: pointer" type="submit"	value="Delete" />
                        </p>
                        <a style=" cursor:pointer" class="delete" onclick="Delete()">Delete</a> 
                    </li>
                </ul>            

            </div>

            <table width="100%">
                <thead>
                    <tr>
                        <th width="5%">ID</th>
                        <th width="30%">Name</th>
                        <th>Thứ tự</th>
                        <th>Action</th>
                        <th><input name="" type="checkbox" value="" id="checkboxall" /></th>
                    </tr>

                </thead>

                <tbody>

                    <?php

                    $x = 0;

                    ?>

                    <?php foreach ($item as $a) { ?>

                        <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                            <td style="text-align:center"><?php echo $a->id ?></td>
                            <td><b><?php echo $a->name ?></b></td>
                            <td style="text-align:center"><?php echo $a->weight ?></td>                            
                            <td style="text-align:center">
                                <a href="admin/term/edit/<?php echo $type."/".$a->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit"/></a>

                                <?php if ($a->status == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value_article('status', 'term', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0, <?php echo $type; ?>)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value_article('status', 'term', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1, <?php echo $type; ?>)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php } ?>

                                <?php if(($type !=2 &&$type !=5)){?>

                                <a href="admin/term/delete/<?php echo $type."/". $a->id ?>/<?php echo $page_no ?>" title="Xóa"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete"/></a>

                                <?php }else{?>

                                    <a  title="Xóa" style="opacity: 0.3"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete"/></a>

                                <?php }?>

                            </td>

                            <td style="text-align:center">
                            

                                <input type="checkbox" value="check_item[<?php echo $a->id ?>]" name="checkall[<?php echo $a->id ?>]" class="checkall"/>
                            </td>

                        </tr>

                        <?php foreach($this->global_function->show_list_table_where(array("type"=>$type,"status"=>1,"parent_id"=>$a->id),"term") as $c){?>

                            <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>

                                <td style="text-align:center"><?php echo $c->id ?></td>

                                <td>|--<?php echo $c->name ?></td>

                                <td style="text-align:center"><?php echo $c->weight ?></td>

                            <?php if($type==3){?>

                                <td style="text-align:center">

                                </td>
								<td style="text-align:center">

                                </td>
								<?php }?>

                                <td style="text-align:center">

                                    <a href="admin/term/edit/<?php echo $type."/".$c->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit"/></a>



                                    <?php if ($c->status == 1) { ?>

                                        <a href="admin/term/hide/<?php echo $type."/". $c->id ?>/<?php echo $page_no ?>" title="Hiện"><img

                                                src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>

                                    <?php } else { ?>

                                        <a href="admin/term/show/<?php echo $type."/".$c->id ?>/<?php echo $page_no ?>" title="Ẩn"><img

                                                src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>

                                    <?php } ?>



                                    <a href="admin/term/delete/<?php echo $type."/". $c->id ?>/<?php echo $page_no ?>" title="Xóa"><img

                                            src="theme_admin/img/icons/icon_delete.png" alt="Delete"/></a>

                                </td>

                                <td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $c->id ?>]" name="checkall[<?php echo $c->id ?>]"

                                        class="checkall"/></td>

                            </tr>

                            <?php }?>

                        <?php

                        $x++;

                    }

                        ?>

                    </tbody>

                </table>

                <p style="color:#FF0000; font-weight:bold; text-align:center"><?php echo "Bạn chỉ được phép xóa các thư mục rỗng và không có bài viết" ?></p>

                <div style="clear: both;"></div>

                <p style="display:none">

                    <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>

                </p>

        </form>

            <div style="clear: both"></div>

            <?php echo $link ?>

    </div>

</div>



