<?php
class Admin extends CI_Controller
{
    protected $_table = 'term';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array(
            "url"
        ));
        $this->load->model(array(
            "term/m_term",
            "general"
        ));
        $this->template->set_template('admin');
    }
    function index($type = 1, $page_no = 1)
    {
        if (!($this->general->Checkpermission("view_term")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST['show']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
        }
        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->hide_more($a);
            }
            redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
        }
        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->delete_more($a);
            }
            redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
        }
        $page_co         = 20;
        $start           = ($page_no - 1) * $page_co;
        $count           = $this->general->count_table_where(array(
            'type' => $type,
            "parent_id" => 0
        ), 'term');
        $data['page_no'] = $page_no;
        $data['type']    = $type;
        $data['item']    = $this->m_term->show_list_term(array(
            'type' => $type,
            "parent_id" => 0
        ), $page_co, $start);
        $data['link']    = $this->general->paging($page_co, $count, 'admin/term/', $page_no);
        $this->template->write('mod', "term_" . $type);
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();
    }
    function add($type = 1)
    {
        if (!($this->general->Checkpermission("add_term")))
            redirect(base_url("admin/not-permission"));
        $data               = array();
        $data['breadcrumb'] = '<li>>></li><li><a href="back/term">Danh mục bài viết</a></li><li>>></li><li class="current">Thêm mới</li>';
        if (isset($_POST['ok'])) {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required|max_length[100]');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'weight' => $this->input->post('weight'),
                    'parent_id' => $this->input->post('parent_id'),
                    'name' => $this->input->post('name'),
                    'slug' => ($this->input->post('slug') != '') ? $this->input->post('slug') : $this->my_lib->changeTitle($this->input->post('name')),
                    'summary' => $this->input->post('summary'),
                    'status' => $this->input->post('status'),
                    'title' => $this->input->post('title'),
                    'keywords' => $this->input->post('keywords'),
                    'description' => $this->input->post('description'),
                    'user_id' => $this->session->userdata('admin_login')->id,
                    'type' => $type
                );
                $this->db->insert('term', $sql);
                redirect(base_url('admin/term/index/' . $type) . '?messager=success');
            }
        }
        $data['type'] = $type;
        $this->template->write('mod', "term_" . $type);
        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }
    function edit($type = 1, $id)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST['ok'])) {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required|max_length[100]');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'weight' => $this->input->post('weight'),
                    'parent_id' => $this->input->post('parent_id'),
                    'name' => $this->input->post('name'),
                    'slug' => ($this->input->post('slug') != '') ? $this->input->post('slug') : $this->my_lib->changeTitle($this->input->post('name')),
                    'summary' => $this->input->post('summary'),
                    'status' => $this->input->post('status'),
                    'title' => $this->input->post('title'),
                    'keywords' => $this->input->post('keywords'),
                    'description' => $this->input->post('description'),
                    'modified_in' => date('Y-m-d H:i:s'),
                    'user_edit_id' => $this->session->userdata('admin_login')->id
                );
                $this->db->where('id', $id);
                $this->db->update('term', $sql);
                redirect(base_url('admin/term/edit/' . $type . "/" . $id) . '?messager=success');
            }
        }
        $data['type'] = $type;
        $this->template->write('mod', "term_" . $type);
        $data['item'] = $this->m_term->get_termID($id);
        $this->template->write_view('content', 'admin/edit', $data, TRUE);
        $this->template->render();
    }
    function hide($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "term");
        redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function hide_menu($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'menu' => 0
        ), "term");
        redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function hide_hot($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'hot' => 0
        ), "term");
        redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function hide_more($id)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "term");
        return true;
    }
    function show_more($id)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "term");
        return true;
    }
    function show($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "term");
        redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function show_menu($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'menu' => 1
        ), "term");
        redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function show_hot($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_term")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'hot' => 1
        ), "term");
        redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function delete($type, $id, $page_no)
    {
        if (!($this->general->Checkpermission("delete_term")))
            redirect(base_url("admin/not-permission"));

        $count_child = $this->m_term->count_where(array(
            'parent_id' => $id
        ), 'term');
        if ($count_child > 0)
            redirect(base_url('admin/term/index/' . $type . '/' . $page_no) . '?messager=delete_error');

        $count_article = $this->m_term->count_article($id);
        if ($count_article > 0)
            redirect(base_url('admin/term/index/' . $type . '/' . $page_no) . '?messager=delete_error');

        $this->db->delete('term', array(
            'id' => $id
        ));
        redirect(base_url('admin/term/index/' . $type . "/" . $page_no) . '?messager=success');
    }
    function delete_more($id)
    {
        if (!($this->general->Checkpermission("delete_term")))
            redirect(base_url("admin/not-permission"));

        $count_child = $this->m_term->count_where(array(
            'parent_id' => $id
        ), 'term');
        $count_article = $this->m_term->count_article($id);
        if ($count_child == 0 || $count_article == 0)
            $this->db->delete($this->_table, array('id' => $id));
        return true;
    }
}