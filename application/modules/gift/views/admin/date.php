
<?php
$this->load->view('back/inc/messager', array('type_messager'=>$this->input->get('messager') ) );
?>
<!-- Alternative Content Box Start -->
<div class="contentcontainer">
    <div class="headings altheading">
        <h2>Đơn hàng ngày <?php echo $date; ?></h2>
    </div>
    <div class="contentbox">
        <div style="clear:both; height:10px"></div>
        <form method="post" action=""	enctype="multipart/form-data">
            <table width="100%" id="order-admin">
                <thead>
                <tr>
                    <th width="10%">IP</th>
                    <th  width="15%">Thông tin thiết bị</th>
                    <th >Mã booking</th>
                    <th>Thời gian mua</th>
                    <th>Ghi chú</th>
                    <th>Tình trạng</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="show_list_ajax">
                <?php
                $x=1;
                foreach($list as $i){
                    $status=$this->general->get_tableWhere(array("id"=>$i->status),"order_status");
                    $store=$this->m_store->show_detail_store_id($i->change_store_id,"vn");
                    switch($i->status){
                        case 1:$style="waiting";break;
                        case 2:$style="confirmed";break;
                        case 3:$style="sucessfull";break;
                        case 4:$style="cancel";break;
                        case 5:$style="shipped";break;
                        case 6:$style="changed";break;
                    }
                    ?>
                    <tr class="<?php echo $style?>">
                        <td><?php echo $i->ip_address?></td>
                        <td><?php echo $i->user_agent?></td>
                        <td><a  href="admin/order/view/<?php echo $i->id?>"><?php echo $i->code_booking?></a></td>
                        <td><?php echo date("H:i d-m-Y",strtotime($i->date_create))?></td>
                        <td  style="text-align: left; width: 200px"><?php echo wordwrap($i->note,100,"<br>")?></td>
                        <td class="name"><?php echo isset($status->name_vn)?$status->name_vn:"";?></td>
                        <td style="text-align:center">
                            <a href="admin/order/view/<?php  echo $i->id ?>" title="Chỉnh sửa">
                                <img src="theme_admin/img/icons/icon_edit.png" alt="Edit" />
                            </a>
                            <a data="" title="Xóa" onclick="Alert('admin/order/delete/<?php echo $i->id ?>')"  class="delete" ><img src="theme_admin/img/icons/icon_delete.png" alt="Delete" /></a>

                        </td>
                    </tr>
                    <?php $x++;}?>
                </tbody>
            </table>

            <div style="clear: both;"></div>
        </form>
    </div>
</div>
 
