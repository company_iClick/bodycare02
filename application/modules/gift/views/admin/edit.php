<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
?>
<div class="contentcontainer">
    <div class="headings altheading"><h2 style="font-size: 18px">Cập nhật thông tin quà tặng</h2></div>
    <div class="contentbox">
        <form method="post" id="frm_update_gift" action="" enctype="multipart/form-data">
            <div style="clear:both; height:10px"></div>
            <div id="append_input">
                <input type="hidden" name="member_id" value="<?php echo $user->id; ?>">
                <input type="hidden" name="g_id" value="<?php echo $item->id; ?>">
                <label for="tags">Tên sản phẩm: </label>
                <input type="text" class="inputbox" style="width:400px" name="g_name" value="<?php echo $item->name; ?>">
                <?php echo form_error('name'); ?>

                <label for="tags">Số lượng: </label>
                <input type="text" class="inputbox" style="width:50px" name="g_qty" value="<?php echo $item->qty; ?>">
                <div>
                    <label for="tags"><b>Trừ điểm tích lũy:</b></label>
                    <input type="text" name="g_score" class="inputbox" value="<?php echo $item->score; ?>" style="width:100px">
                </div>
                <div>
                    <label for="tags"><b>Ghi chú:</b></label>
                    <textarea name="g_note" class="inputbox" rows="3"><?php echo $item->note; ?></textarea>
                </div>
                <div style="margin-top:10px;background:blue;border:none !important" class="btn" id="update_gift" onclick="update_gift()">Cập nhật</div>
            </div>  
            
        </form>
    </div>
</div>

