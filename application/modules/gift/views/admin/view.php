<script language="javascript">
    function printDiv(divID) {
        //Get the HTML of div
        var divElements = document.getElementById(divID).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        document.body.innerHTML = oldPage;


    }
</script>
<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
?>
<!-- Alternative Content Box Start -->
<style>
    .col-one-order {
        float: left
    }

    .col-one-order p {
        margin: 0px;
        padding: 5px; white-space: nowrap;
    }
    #t-order tr td{ text-align: center}
    #list-status li{ display: inline-block}
</style>

<div class="contentcontainer" id="contentcontainer">
    <div class="headings altheading">
        <h2>Chi tiết <a  onclick='printDiv("contentbox")' ><img src="<?php echo base_url()?>themes/images/layout/print.png"   style="float: right; cursor: pointer"></a></h2>
    </div>
    <div  id="contentbox" class="contentbox">
        <p style="color: #f00;"><b>Thời gian đặt hàng:</b> <?php echo date("H:i d-m-Y",strtotime($detail->date_create))?></p>
        <p style="color: #000"><b>Địa chỉ IP:</b> <?php echo $detail->ip_address?></p>
        <p style="color: #000"><b>Thông tin thiết bị:</b> <?php echo $detail->user_agent?></p>
        <?php       
            $buyer = $this->general->get_tableWhere(array("id" => $detail->buyer_id), "user_buy");
            $recipient_id = $this->general->get_tableWhere(array("buyer_id" => $detail->buyer_id), "user_buy");
        ?>
        <table width="100%">
            <tr>
                <td>  <div class="col-one-order">
                        <p><strong>THÔNG TIN NGƯỜI MUA HÀNG</strong></p>

                        <?php if ($detail->type_account == 1) { 
                            $user = $this->general->get_tableWhere(array("id" => $detail->buyer_id), "users");
                        ?>
                        <p><strong>Thành viên VIP: </strong><a href="<?php echo site_url('admin/users/edit/' . $user->id); ?>" target="_blank"><?php echo $user->full_name . ' - ' . $user->username . ' - ' . $user->email?></a></p>
                        <?php } ?>

                        <p><strong>Họ & tên:</strong><?php echo isset($buyer->full_name) ? $buyer->full_name : "" ?></p>

                        <p><strong>Email: </strong><?php echo isset($buyer->email) ? $buyer->email : "" ?></p>

                        <p><strong>Điện thoại di động :</strong><?php echo isset($buyer->cell_phone) ? $buyer->cell_phone : "" ?></p>

                        <p><strong>Địa chỉ:</strong> <?php echo isset($buyer->address) ? $buyer->address : "" ?>

                        </p>

                        <p><strong>Ghi chú:</strong> <?php echo isset($detail->note) ? $detail->note : "" ?>

                        </p>

                    </div>
                </td>

            </tr>
        </table>


            <div style="clear: both"></div>
            <hr>
        <div style="clear: both"></div>
        <hr>
        <table style="width: 100%" id="t-order">
            <thead>
            <th>Hình sản phẩm</th>
            <th>Tên sản phẩm</th>
            <th>Đơn giá</th>
            <th>Số lượng</th>
            <th>Giảm giá</th>
            <th>Thành tiền</th>
            </thead>
            <tbody>
            <?php
            $point=0;
            $x=0;
            $tong=$this->m_order->sum_total($detail->id);
            foreach($list_item as $list){
                if($list->point==1)
                {
                    $point=$point+($list->total);
                }
				$link = site_url($this->global_function->changeTitle($list->p_name));
				$imgUrl = base_url() . $list->picture;
                $item = $this->global_function->get_tableWhere(array('id' => $list->id_item), 'item', 'value, price');
           
                $price = $list->price;
                $sale = $list->sale;
                $sub_total = $list->total;
           
            ?>
            <tr  <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
            	<td><a target="_blank" href="<?php echo $link; ?>"><img src="<?php echo $imgUrl; ?>" width="100" /></a></td>
                <td><a target="_blank" href="<?php echo $link; ?>"><?php echo $list->p_name?></a></td>
                <td><?php echo number_format($price,0,",",".")?> vnđ</td>
                <td><?php echo $list->quantity?></td>
                <td><?php echo number_format($sale,0,",",".")?> vnđ</td>
                <td><?php echo number_format($sub_total,0,",",".")?> vnđ</td>
            </tr>
            <?php $x++;}?>
            <tr>
                <td></td>
                <td></td>
                <td class="red align-right"><?php echo "Tổng tiền"?>: </td>
                <td><?php echo number_format($detail->total_older,0,",",".")?> vnđ</td>
            </tr>
            <!--<tr>
                <td></td>
                <td></td>
                <td class="red align-right"><?php echo "Giảm giá"?>: </td>
                <td><?php echo number_format($detail->discount_order,0,",",".")?> vnđ</td>
            </tr>
            <?php if($detail->coupon_code != ''){ ?>
            <tr>
                <td></td>
                <td></td>
                <td class="red align-right"><?php echo "Mã giảm giá"?>: </td>
                <td><?php echo $detail->coupon_code; ?> vnđ</td>
            </tr>-->
            <?php } ?>
            <!--<tr>
                <td></td>
                <td></td>
                <td class="red align-right"><?php echo "Điểm thưởng"?>: </td>
                <td>
                    <?php
                    if($point>=D_MONEY){
                        echo  round($point/D_MONEY,2);
                    }else{ echo "0";}
                    ?>
                </td>
            </tr>-->
            </tbody>
        </table>
        <div style="clear: both"></div>
        <hr>
        <form action="" method="post">
        <ul id="list-status">
        <?php foreach($status as $st){?>
            <li style="white-space: nowrap"><input type="radio" name="status" <?php if($st->id==$detail->status){?> checked <?php }?> value="<?php echo $st->id?>"><?php echo $st->name_vn?></li>
        <?php }?>
        </ul>
        <div style="clear: both; height: 10px"></div>
        <input type="submit" class="btn" value="Cập nhật" name="ok"/>
        <input type="button" class="btn" value="Quay lại" onclick="window.location.href='admin/order'"/>
        </form>
    </div>
</div>
