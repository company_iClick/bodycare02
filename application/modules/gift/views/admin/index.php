<script>
    $(document).ready(function(){
        $('#start_datetimepicker').datetimepicker({
            format:'Y/m/d H:i:00',
            lang:'vi',
            timepicker:false,
        });
        $('#end_datetimepicker').datetimepicker({
            format:'Y/m/d H:i:00',
            lang:'vi',
            timepicker:false,
        });
    });
</script>
<?php
$this->load->view('back/inc/messager', array('type_messager'=>$this->input->get('messager') ) );
?>
<!-- Alternative Content Box Start -->
<div class="contentcontainer">
    <div class="headings altheading">
        <h2>Đơn hàng thành viên VIP <?php if(isset($user)) echo $user->full_name; ?></h2>
    </div>
    <div class="contentbox">

        <div style="clear:both; height:10px"></div>
        <form method="post" action="" enctype="multipart/form-data">
            <div class="extrabottom">
                <ul>
                    <li> <img src="theme_admin/img/icons/add.png" alt="Add" /> <a style="text-decoration: none;" href="admin/order_vip/add/<?php echo $user->id ?>">Thêm đơn hàng mới</a> </li>
                    <li>
                </ul>
                <div style="float:right"> </div>
            </div>
            <table width="100%" id="order-admin">
                <thead>
                <tr>
                    <th >Mã đơn hàng</th>
                    <th>Thời gian</th>
                    <th>Ghi chú</th>
                    <th>Tình trạng</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="show_list_ajax">
                <?php
                $x=1;
                foreach($list as $i){

                    ?>
                    <tr>
                        <td><a href="admin/order_vip/edit/<?php echo $user->id ?>/<?php echo $i->id?>"><?php echo $i->order_code?></a></td>
                        <td><?php echo date("H:i d-m-Y",strtotime($i->date_create))?></td>
                        <td style="text-align: left; width: 200px"><?php echo $i->note?></td>
                        <td><?php echo ($i->status == 1) ? 'Đã giao' : 'Chưa giao'; ?></td>

                        <td style="text-align:center">
                            <a href="admin/order_vip/edit/<?php echo $user->id ?>/<?php echo $i->id ?>" title="Chỉnh sửa">
                                <img src="theme_admin/img/icons/icon_edit.png" alt="Edit" />
                            </a>
                            <a data="" title="Xóa" onclick="Alert('admin/order_vip/delete/<?php echo $user->id ?>/<?php echo $i->id ?>')"  class="delete" ><img src="theme_admin/img/icons/icon_delete.png" alt="Delete" /></a>

                        </td>
                    </tr>
                    <?php $x++;}?>
                    <tr><td colspan="5"><?php echo $link ?></td></tr>
                </tbody>
            </table>

            <div style="clear: both;"></div>
        </form>
    </div>
</div>