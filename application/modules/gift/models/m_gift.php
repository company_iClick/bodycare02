<?php

class M_gift extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //================ check itemdetail is db====================
    function show_list_gift($limit, $offset) {
        $this->db->select('*');
        $this->db->from('gift');
        $this->db->limit($limit, $offset);
        $this->db->order_by("id","DESC");
        return $this->db->get()->result();
    }
    function show_list_gift_where($where, $limit, $offset) {
        $this->db->select('*');
        $this->db->from('gift');
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by("id","DESC");
        return $this->db->get()->result();
    }
    function count_list_gift() {
        $this->db->select('*');
        $this->db->from('gift');
        $this->db->where($where);
        return $this->db->get()->num_rows();
    }
    function count_list_gift_where($where) {
        $this->db->select('*');
        $this->db->from('gift');
        return $this->db->get()->num_rows();
    }
    function Detail($id) {
        $this->db->select('*');
        $this->db->where("id",$id);
        $this->db->from('gift');
        return $this->db->get()->row();
    }

    

}
