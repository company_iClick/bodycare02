<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->helper(array('url', 'text', 'form', 'file', 'string'));
        $this->load->library(array('form_validation', 'ftp'));
        $this->load->database();
        $this->load->model(array('m_gift', 'general'));
        $this->load->model('global_function');

        $this->template->set_template('admin');

    }

    public function add($user_id){
        $user = $this->global_function->get_tableWhere(array('id' => $user_id, 'status' => 1), "users", "*");
        if(empty($user)) redirect(base_url('admin'));
        $data['user'] = $user;
        $this->load->view('admin/add', $data);
    }
    
    public function add_gift(){
        header('Content-Type: text/html; charset=utf-8');
        if($this->input->post() != ''){
            $data = $this->input->post();
            $data['status'] = 1;
            $data['user_id'] = $this->session->userdata('admin_login')->id;

            $user = $this->general->get_tableID($data['member_id'], "users");
            if($this->db->insert('gift', $data)){
                $score = $user->score - $data['score'];
                $this->db->where('id', $data['member_id']);
                $this->db->update('users', array('score' => $score));
                echo 1;
            }else echo 0;
        }
        
    }

    public function edit($user_id, $id){
        header('Content-Type: text/html; charset=utf-8');
        $user = $this->global_function->get_tableWhere(array('id' => $user_id, 'status' => 1), "users", "*");
        $item = $this->global_function->get_tableWhere(array('id' => $id), "gift", "*");
        if(empty($user) || empty($item)) redirect(base_url('admin'));
        $data['user'] = $user;
        $data['item'] = $item;

        $this->load->view('admin/edit', $data);        
    }

    public function edit_gift(){
        header('Content-Type: text/html; charset=utf-8');
        $data_post = $this->input->post();
        $user = $this->global_function->get_tableWhere(array('id' => $data_post['member_id'], 'status' => 1), "users", "*");
        $item = $this->global_function->get_tableWhere(array('id' => $data_post['g_id']), "gift", "*");

        $sql['name'] = $data_post['g_name'];
        $sql['qty'] = $data_post['g_qty'];
        $sql['score'] = $data_post['g_score'];
        $sql['note'] = $data_post['g_note'];
        $sql['user_edit_id'] = $this->session->userdata('admin_login')->id;

        $this->db->where(array('id' => $data_post['g_id'], 'member_id' => $data_post['member_id']));
        if($this->db->update('gift', $sql)){
            $score = $user->score - $sql['score'] + $item->score;                
            $this->db->where('id', $data_post['member_id']);
            $this->db->update('users', array('score' => $score));
            echo 1;
        }else echo 0;
    }

   
    function delete($user_id, $id, $type = 1){
        $user = $this->global_function->get_tableWhere(array('id' => $user_id, 'status' => 1), "users", "*");
        $item = $this->global_function->get_tableWhere(array('id' => $id), "gift", "*");
        if(empty($user) || empty($item)) redirect(base_url('admin'));
        $this->db->where(array("id"=>$id, "user_id" => $user_id));
        if($this->db->delete("gift")){
            $score = $user->score - $item->score;
            $this->db->where('id', $user_id);
            $this->db->update('users', array('score' => $score));
            if($type == 1){
                redirect(base_url('admin/users/edit/' . $user_id . '?messager=success'));
            }else redirect(base_url('admin/users/view/' . $user_id . '?messager=success'));
            
        }     
    }

}