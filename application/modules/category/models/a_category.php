<?php

class A_category extends CI_Model {

	protected $_table = 'category';
    protected $_tmp_pc = 'tmp_product_category';
    protected $_tmp_ppt = 'tmp_product_product_type';
    protected $_tmp_pht = 'tmp_product_hair_type';
    protected $_tmp_pst = 'tmp_product_skin_type';
    protected $_tmp_pa = 'tmp_product_action';
    protected $_tmp_pca = 'tmp_product_capacity';
    protected $_tmp_pw = 'tmp_product_weigh';
    protected $_tmp_ppn = 'tmp_product_pill_number';
    protected $_tmp_ptg = 'tmp_product_tag';
    protected $_pr = 'product';
    protected $_br = 'brand';
    protected $_pt = 'product_type';
    protected $_a = 'action';
    protected $_cp = 'capacity';
    protected $_st = 'skin_type';
    protected $_ht = 'hair_type';
    protected $_ppn = 'pill_number';
    protected $_w = 'weigh';
    protected $_tgs = 'tags';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	function get_category($parentid = 0, $space = "", $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }

        $this->db->where('parent_id', $parentid);
        $this->db->order_by("weight", "asc");
        $this->db->order_by("id", "desc");
        $result    = $this->db->get($this->_table)->result();
        $trees_obj = array();
        if (!empty($result)) {
            foreach ($result as $rs) {
                $list_cate_id = $this->get_categroy_tree_id($rs->id);
                $trees[] = array(
                    'id' => $rs->id,
                    'parent_id' => $rs->parent_id,
                    'weight' => $rs->weight,
                    'hot' => $rs->hot,
                    'status' => $rs->status,
                    'name' => $space . $rs->name,
                    'slug' => $rs->slug,
                    'level' => $rs->level,
                    'count_product' => $this->count_product($list_cate_id)
                    );
                $trees   = $this->get_category($rs->id, $space  , $trees);
            }
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
				$tree        = (object) $tree;
                $trees_obj[] = $tree;
            }
        }
        return $trees_obj;
    }

    function get_parent_category($parentid = 0, $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }

        $this->db->where('id', $parentid);
        $result    = $this->db->get($this->_table)->row();
        $trees_obj = array();
       
        if (!empty($result)) {
            $trees[] = array(
                    'id' => $result->id,
                    'parent_id' => $result->parent_id,
                    'status' => $result->status,
                    'name' => $result->name,
                    'slug' => $result->slug,
                    'banner' => $result->banner,
                    'link_banner' => $result->link_banner,
                );
            $trees   = $this->get_parent_category($result->parent_id, $trees);    
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $tree        = (object) $tree;
                $trees_obj[] = $tree;
            }
        }
        return $trees_obj;
     
    }

    function get_parent_category_array($parentid = 0, $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }
        $this->db->select('id, parent_id, name, slug');
        $this->db->where('id', $parentid);
        $result    = $this->db->get($this->_table)->row();
        $trees_obj = array();
       
        if (!empty($result)) {
            $trees[] = array(
                    'id' => $result->id,
                    'parent_id' => $result->parent_id,
                    'name' => $result->name,
                    'slug' => $result->slug
                );
            $trees   = $this->get_parent_category_array($result->parent_id, $trees);    
        }
    
        return $trees;
     
    }

    function recurse($categories, $space = "", $parent = 0, $level = 0)
    {
        if($level < 3){
            $ret = '<ul>';
            foreach($categories as $index => $category)
            {
                if($category['parent_id'] == $parent)
                {
                    $ret .= '<li><a href="#"><p class="Tier' . $level . '">' . $space . $category['name'] . '</p></a>';
                    $ret .= $this->recurse($categories, $space . '|---', $category['id'], $level+1);
                    $ret .= '</li>';
                }
            }
            return $ret . '</ul>';
        }
    }

    function recurse_one($categories, $parent = 0, $level = 0)
    {
        if($level < 3){
            $trees = array();
            $trees_obj = array();
            foreach($categories as $index => $category)
            {
                if($category['parent_id'] == $parent)
                {
                    $trees[] = array(
                        'id' => $category['id'],
                        'parent_id' => $category['parent_id'],
                        'name' => $category['name'],
                        'slug' => $category['slug'],
                        'color' => $category['color'],
                        'font_class' => $category['font_class'],
                        'banner_menu' => $category['banner_menu'],
                        'child' => $this->recurse_one($categories, $category['id'], $level+1)
                    );                  
                }
            }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $tree        = (object) $tree;
                $trees_obj[] = $tree;
            }
        }
        return $trees_obj;
        }
    }

    function get_categroy_tree_id($parentid = 0, $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }
        $trees[] = $parentid;
        $this->db->select('id');
        $this->db->where('parent_id', $parentid);
        $result    = $this->db->get($this->_table)->result();
        $trees_obj = array();
        if (!empty($result)) {
            foreach ($result as $rs) {
                $trees[] = $rs->id;
                $trees   = $this->get_categroy_tree_id($rs->id, $trees);
            }
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $trees_obj[] = $tree;
            }
        }
        $trees_obj = array_unique($trees_obj);
        return $trees_obj;
    }

    function count_product($list_cate_id)
    {  
        return $this->db->select("count($this->_pr.id)")->where_in("$this->_tmp_pc.category_id", $list_cate_id)
            ->where("$this->_pr.status", 1)
            ->from("$this->_pr")
            ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id=$this->_pr.id")
            ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function get_category_same($where){
        return $this->db->select("id, parent_id, name, slug")
                ->distinct("parent_id")
                ->distinct("level")
                ->where($where)
                ->where("status", 1)
                ->from("$this->_table")
                ->get()
                ->result();
    }

    function get_category_hot(){
        $this->db->select('a.id, a.parent_id, a.name, a.slug, a.hot');
        $this->db->where('a.hot', 1);
        $this->db->from("$this->_table as a");
        $this->db->join("$this->_table as b", "a.id = b.parent_id", "RIGHT OUTER");
        return $this->db->get()->result_array();
    }

    function get_node_category_id($parentid = 0, $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }
        $this->db->select('id, parent_id, name, slug');
        $this->db->where('id', $parentid);
        $result    = $this->db->get($this->_table)->row();
        $trees_obj = array();
       
        if (!empty($result)) {
            $trees = $result->id . ',' . $result->name . ',' . $result->slug;
            $trees   = $this->get_node_category_id($result->parent_id, $trees);    
        }
    
        return $trees;
     
    }

    function get_category_in_param($param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $where = array()){
        $this->db->select("$this->_table.id, $this->_table.parent_id, $this->_table.name, $this->_table.slug");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1));
        
        switch ($param_type) {
                
            case 'product_type':
                $this->db->where(array("$this->_tmp_ppt.product_type_id" => $param_id, "$this->_pt.status" => 1));
                break;

            case 'skin_type':
                $this->db->where(array("$this->_tmp_pst.skin_type_id" => $param_id, "$this->_st.status" => 1));
                break;

            case 'hair_type':
                $this->db->where(array("$this->_tmp_pht.hair_type_id" => $param_id, "$this->_ht.status" => 1));
                break;

            case 'origin':
                $this->db->where_in("$this->_pr.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where(array("$this->_tmp_pa.action_id" => $param_id, "$this->_a.status" => 1));
                break;

            case 'capacity':
                $this->db->where(array("$this->_tmp_pca.capacity_id" => $param_id, "$this->_cp.status" => 1));
                break;

            case 'weigh':
                $this->db->where(array("$this->_tmp_pw.weigh_id" => $param_id, "$this->_w.status" => 1));
                break;

            case 'pill_number':
                $this->db->where(array("$this->_tmp_ppn.pill_number_id" => $param_id, "$this->_ppn.status" => 1));
                break;

            case 'tags':
                $this->db->where(array("$this->_tmp_ptg.tag_id" => $param_id, "$this->_tgs.status" => 1));
                break;            
        }

        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.category_id = $this->_table.id");
        $this->db->join("$this->_pr", "$this->_pr.id = $this->_tmp_pc.product_id");

        switch ($param_type) {
                
            case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_pr.id");
                $this->db->join("$this->_pt", "$this->_pt.id = $this->_tmp_ppt.product_type_id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id");
                $this->db->join("$this->_st", "$this->_st.id = $this->_tmp_pst.skin_type_id");
                break;

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id");
                $this->db->join("$this->_ht", "$this->_ht.id = $this->_tmp_pht.hair_type_id");
                break;

            case 'origin':
                $this->db->where("$this->_pr.origin_id", $param_id);
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id");
                $this->db->join("$this->_a", "$this->_a.id = $this->_tmp_pa.action_id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id");
                $this->db->join("$this->_cp", "$this->_cp.id = $this->_tmp_pca.capacity_id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id");
                $this->db->join("$this->_w", "$this->_w.id = $this->_tmp_pw.weigh_id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id");
                $this->db->join("$this->_ppn", "$this->_ppn.id = $this->_tmp_ppn.pill_number_id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_pr.id");
                $this->db->join("$this->_tgs", "$this->_tgs.id = $this->_tmp_ptg.tag_id");
                break;            
        }

        if(!empty($brand_value)) $this->db->where_in("$this->_pr.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_pr.origin_id", $origin_value);
        if(!empty($product_type_value)){
            $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        } 
        if(!empty($skin_type_value)){
            $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        } 
        if(!empty($hair_type_value)){
            $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        } 
        if(!empty($action_value)){
            $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        } 
        if(!empty($capacity_value)){
            $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        } 
        if(!empty($weigh_value)){
            $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        } 
        if(!empty($pill_number_value)){
            $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        } 
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to"; 
                
                $i++;
            }
            $this->db->where("($price_where)");
        }


        return $this->db->get()->result();
    }

    function get_category_in_param_search($keyword, $list_cate_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $where = array()){
        $this->db->select("$this->_table.id, $this->_table.parent_id, $this->_table.name, $this->_table.slug");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1));
        $this->db->like("$this->_pr.slug", $keyword);
        
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.category_id = $this->_table.id");
        $this->db->join("$this->_pr", "$this->_pr.id = $this->_tmp_pc.product_id");

        if(!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        if(!empty($brand_value)) $this->db->where_in("$this->_pr.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_pr.origin_id", $origin_value);
        if(!empty($product_type_value)){
            $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        } 
        if(!empty($skin_type_value)){
            $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        } 
        if(!empty($hair_type_value)){
            $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        } 
        if(!empty($action_value)){
            $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        } 
        if(!empty($capacity_value)){
            $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        } 
        if(!empty($weigh_value)){
            $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        } 
        if(!empty($pill_number_value)){
            $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id");
            $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        } 
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to";  
                
                $i++;
            }
            $this->db->where("($price_where)");
        }


        return $this->db->get()->result();
    }

}