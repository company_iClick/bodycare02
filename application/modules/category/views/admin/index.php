<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager'), 'id' => $this->input->get('id'), 'type' => $this->input->get('type')));
?>

<!-- Alternative Content Box Start -->

<div class="contentcontainer">
    <div class="headings altheading"><h2>Danh sách</h2></div>
    <div class="contentbox">
        <form method="post" action=""	enctype="multipart/form-data">
            <div class="extrabottom">
                <ul>
                    <li>
                        <img src="theme_admin/img/icons/add.png" alt="Add" /> 
                        <a style="text-decoration: none;" href="admin/category/add">Thêm mới</a>
                    </li>

                    <li> <img src="theme_admin/img/icons/icon_approve.png" alt="Approve" />
                        <input class="a_button_act a_show" name="show" style="cursor: pointer" type="submit"	value="Hiện danh sách đã chọn" />
                    </li>
                    <li> <img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" />

                        <input class="a_button_act a_hide" name="hide" style="cursor: pointer" type="submit"	value="Ẩn danh sách đã chọn" />

                    </li>

                    <li> <img src="theme_admin/img/icons/icon_delete.png" alt="Delete" />

                        <p style="display:none">

                            <input class="a_button_act a_delete" name="delete" style="cursor: pointer" type="submit"	value="Delete" />

                        </p>

                        <a style=" cursor:pointer" class="delete" onclick="Delete()">Delete</a> 

                    </li>

                </ul>            

            </div>

            <table width="100%">

                <thead>

                    <tr>

                        <th width="5%">STT</th>
                        <th width="30%">Tên</th>
                        <th>Đường dẫn</th>
                        <th>Thứ tự</th>
                        <th>Nổi bật</th>
                        <th>Layout(2 cột)</th>
                        <th>Action</th>
                        <th><input name="" type="checkbox" value="" id="checkboxall" /></th>
                    </tr>

                </thead>

                <tbody>

                    <?php
                    $x = 0;
                    ?>

                    <?php if (!empty($item)) {
                        foreach ($item as $a) {
                            ?>

                            <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                                <td style="text-align:center"><?php echo $x + 1; ?></td>
                                <td><b><?php echo $a->name ?></b></td>
                                <td style="text-align:center">
                                    <input type="text" name="slug" id="slug_<?php echo $a->id; ?>" value="<?php echo $a->slug ?>" style="width: 200px;padding:5px">
                                    <i class="fa fa-refresh" aria-hidden="true" style="color: #f00;cursor: pointer;" onclick="change_slug(<?php echo $a->id; ?>)"></i>
                                </td>
                                <td style="text-align:center">
                                    <input type="number" name="weight" id="weight_<?php echo $a->id; ?>" value="<?php echo $a->weight ?>" style="width: 35px;padding:3px">
                                    <i class="fa fa-refresh" aria-hidden="true" style="color: #f00;cursor: pointer;" onclick="change_weight(<?php echo $a->id; ?>)"></i>
                                </td>
                                <td style="text-align:center">
                                <?php if($a->level == 0) {
                                    if ($a->hot == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('hot', 'category', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('hot', 'category', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php }} ?>
                                </td>
                                <td style="text-align:center">
                                <?php if($a->level == 0) {
                                    if ($a->layout == 1) { ?>
                                    <a href="javascript:void(0)" onclick="set_value('layout', 'category', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 0)" title="Hiện"><img
                                            src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                <?php } else { ?>
                                    <a href="javascript:void(0)" onclick="set_value('layout', 'category', <?php echo $a->id; ?>, <?php echo $page_no; ?>, 1)" title="Ẩn"><img
                                            src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                                <?php }} ?>
                                </td>
                                <td style="text-align:center">
                                    <a href="admin/category/edit/<?php echo $a->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit"/></a>

                                    <?php if ($a->status == 1) { ?>

                                        <a href="admin/category/hide/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Hiện"><img src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                    <?php } else { ?>

                                        <a href="admin/category/show/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Ẩn"><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>

                                    <?php } ?>

                                    <a onclick="if (!confirm('Xác nhận xóa')) return false;" href="admin/category/delete/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Xóa"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete"  /></a>
                                </td>

                                <td style="text-align:center">
                                    <input type="checkbox" value="check_item[<?php echo $a->id ?>]" name="checkall[<?php echo $a->id ?>]" class="checkall"/>

                                </td>
                            </tr>

                            <?php
                            $x++;
                        }
                    }
                    ?>

                </tbody>

            </table>

            <p style="color:#FF0000; font-weight:bold; text-align:center;margin-top: 5px;"><?php echo "Bạn chỉ được phép xóa các thư mục rỗng và không có dữ liệu" ?></p>

            <div style="clear: both;"></div>

            <p style="display:none">
                <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>
            </p>

        </form>
        <div style="clear: both"></div>
    </div>

</div>
<script type="text/javascript">
function change_slug(id) {
        var slug = $('#slug_' + id).val();
        $.ajax({
            type: 'POST',
            url: 'admin/category/change_slug',
            data: {'id': id, 'slug': slug},
            success: function(result){
                alert(result);
            }
        })
    }

    function change_weight(id) {
        var weight = $('#weight_' + id).val();
        $.ajax({
            type: 'POST',
            url: 'admin/category/change_weight',
            data: {'id': id, 'weight': weight},
            success: function(result){
                alert(result);
            }
        })
    }
</script>


