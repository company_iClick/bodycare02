<?php
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array(
            "url"
        ));
        $this->load->model(array(
            "comment/m_comment",
            "general"
        ));
        $this->template->set_template('admin');
        $this->template->write('mod', "comment");
    }

    function index($page_no = 1){
        if (!($this->general->Checkpermission("comment")))
            redirect(base_url("admin/not-permission"));

        $page_co         = 20;
        $start           = ($page_no - 1) * $page_co;
        $count           = $this->m_comment->count_comment_where(array(
            'parent_id' => 0
        ), 'comment');
        $data['page_no'] = $page_no;
        $data['item']    = $this->m_comment->show_list_comment(array('parent_id' => 0), $page_co, $start);
        if(!empty($data['item'])){
            foreach ($data['item'] as $row) {
                $product_id = $row->product_id;
                $list_id = $this->m_comment->get_all_tree_id($product_id);

                $row->product_name = $this->global_function->get_row_object(array('id' => $product_id), 'name', 'product')->name;

                $row->count_comment = $this->m_comment->count_comment_all_id($list_id, array('read' => 0));
            }       
        }

        $data['link']    = $this->general->paging($page_co, $count, 'admin/comment/' , $page_no);
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();

    }
  
    function product($product_id, $page_no = 1)
    {
        if (!($this->general->Checkpermission("view_comment")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST['show']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(base_url('admin/comment/' . $product_id . '/' . $page_no) . '?messager=success');
        }
        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->hide_more($a);
            }
            redirect(base_url('admin/comment/' . $product_id . '/' . $page_no) . '?messager=success');
        }
        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->delete_more($a);
            }
            redirect(base_url('admin/comment/' . $product_id . '/' . $page_no) . '?messager=success');
        }

        $sql['read']  = 1;
        $this->db->where('product_id', $product_id);
        $this->db->update('comment', $sql);

        $page_co         = 20;
        $start           = ($page_no - 1) * $page_co;
        $count           = $this->general->count_table_where(array(
            'id !=' => 0,
            'product_id' => $product_id
        ), 'comment');
        $data['page_no'] = $page_no;
        $data['item']    = $this->m_comment->menu_admin($product_id);

        $data['product_id'] = $product_id;
        $data['link']    = $this->general->paging($page_co, $count, 'admin/comment/' . $product_id . '/', $page_no);
        $this->template->write_view('content', 'admin/product', $data, TRUE);
        $this->template->render();
    }
    function add($product_id)
    {
        if (!($this->general->Checkpermission("add_comment")))
            redirect(base_url("admin/not-permission"));
        $data               = array();
        $data['breadcrumb'] = '<li>>></li><li><a href="back/comment">Thêm bình luận</a></li><li>>></li><li class="current">Thêm mới</li>';
        if (isset($_POST['ok'])) {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required|max_length[100]');
            $this->form_validation->set_rules('support_staff_id', 'Nhân viên hỗ trợ', 'required');
            $this->form_validation->set_rules('content', 'Nội dung câu hỏi', 'trim|required');
            $this->form_validation->set_rules('answer', 'Nội dung trả lời', 'trim|required');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'product_id' => $product_id,
                    'name' => $this->input->post('name'),
                    'content' => $this->input->post('content'),
                    'status' => $this->input->post('status'),
                    'weight' => $this->input->post('weight'),
                    'read' => 1,
                    'user_id' => $this->session->userdata('admin_login')->id
                );
                if($this->db->insert('comment', $sql)){
                    $id = $this->db->insert_id();

                    if($this->input->post('answer')){
                        $sql_answer = array(
                            'admin' => 1,
                            'parent_id' => $id,
                            'product_id' => $product_id,
                            'support_staff_id' => $this->input->post('support_staff_id'),
                            'content' => $this->input->post('answer'),
                            'read' => 1,
                            'weight' => 1,
                            'status' => 1,
                            'user_id' => $this->session->userdata('admin_login')->id,
                        );
                        $this->db->insert('comment', $sql_answer);
                    }
                }
            
                redirect(base_url('admin/comment/product/' . $product_id) . '?messager=success');
            }
        }

        $data['product'] = $this->global_function->get_row_object(array('id' => $product_id), 'id, name,slug,created_at', 'product');
        $data['support_staff'] = $this->global_function->get_array_object(array(), 'id, name', 'support_staff');

        $this->template->write('mod', "comment");
        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }
    function edit($product_id, $id)
    {
        if (!($this->general->Checkpermission("add_comment")))
            redirect(base_url("admin/not-permission"));
        $data               = array();
        $data['breadcrumb'] = '<li>>></li><li><a href="back/comment">Thêm bình luận</a></li><li>>></li><li class="current">Thêm mới</li>';

        $data['item'] = $this->m_comment->get_commentID($id);

        $comment_parent = $this->global_function->get_row_object(array('id' => $data['item']->parent_id), 'id, name', 'comment');
        $parent_id = (!empty($comment_parent )) ? $comment_parent->id : $id;

        $this->db->where('id', $id);
        $this->db->update('comment', array('read' => 1));

        if (isset($_POST['ok'])) {

            $this->form_validation->set_rules('support_staff_id', 'Nhân viên hỗ trợ', 'required');
            $this->form_validation->set_rules('content', 'Nội dung câu hỏi', 'trim|required');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'product_id' => $product_id,
                    'name' => $this->input->post('name'),
                    'content' => $this->input->post('content'),
                    'status' => $this->input->post('status'),
                    'weight' => $this->input->post('weight'),
                    'read' => 1,
                    'user_edit_id' => $this->session->userdata('admin_login')->id
                );
                
                $this->db->where('id', $id);

                if($this->db->update('comment', $sql)){
                    if($this->input->post('answer')){
                        
                        $sql_answer = array(
                            'admin' => 1,
                            'parent_id' => $parent_id,
                            'product_id' => $product_id,
                            'content' => $this->input->post('answer'),
                            'support_staff_id' => $this->input->post('support_staff_id'),
                            'read' => 1,
                            'weight' => 1,
                            'status' => 1,
                            'user_id' => $this->session->userdata('admin_login')->id,
                        );
                        $this->db->insert('comment', $sql_answer);
                    }
                }
            
                redirect(base_url('admin/comment/product/' . $product_id) . '?messager=success');
            }
        }

        $data['product'] = $this->global_function->get_row_object(array('id' => $product_id), 'id, name,slug,created_at', 'product');
        $data['list_comment'] = $this->m_comment->menu_admin($product_id);
        $data['support_staff'] = $this->global_function->get_array_object(array(), 'id, name', 'support_staff');
        

        $this->template->write('mod', "comment");
        $this->template->write_view('content', 'admin/edit', $data, TRUE);
        $this->template->render();
    }
    function view($id)
    {
        if (!($this->general->Checkpermission("edit_comment")))
            redirect(base_url("admin/not-permission"));
        $data['item'] = $this->m_comment->get_commentID($id);
        $sql['read']  = 1;
        $this->db->where('id', $id);
        $this->db->update('comment', $sql);
        $this->template->write_view('content', 'admin/view', $data, TRUE);
        $this->template->render();
    }
    function hide($product_id, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_comment")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "comment");
        redirect(base_url('admin/comment/product/' . $product_id . '/' . $page_no) . '?messager=success');
    }
    function hide_more($id)
    {
        if (!($this->general->Checkpermission("edit_comment")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "comment");
        return true;
    }
    function show_more($id)
    {
        if (!($this->general->Checkpermission("edit_comment")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "comment");
        return true;
    }
    function show($product_id, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_comment")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "comment");
        redirect(base_url('admin/comment/product/' . $product_id . '/' . $page_no) . '?messager=success');
    }
    function delete($product_id, $id, $page_no)
    {
        if (!($this->general->Checkpermission("delete_comment")))
            redirect(base_url("admin/not-permission"));
        $this->db->delete('comment', array(
            'id' => $id
        ));
        redirect(base_url('admin/comment/product/' . $product_id . '/' . $page_no) . '?messager=success');
    }
    function delete_more($id)
    {
        if (!($this->general->Checkpermission("delete_comment")))
            redirect(base_url("admin/not-permission"));
        $this->db->delete('comment', array(
            'id' => $id
        ));
        return true;
    }
}

