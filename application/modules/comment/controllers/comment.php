<?php

class Comment extends MX_Controller {
    
    protected $_table = 'comment';
    
    function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->model(array("comment/a_comment"));
        $this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
    }
    
    public function list_comment(){
        $product_id = $this->input->get('product_id');
        
        $data['list_comment'] = $this->a_comment->get_list_comment($product_id);
        $this->load->view('public/list_comment', $data);
    }


    public function send_comment(){
        $sql['product_id'] = $this->input->post('product_id');
        $sql['member_id'] = ($this->session->userdata("user_log")) ? $this->session->userdata("user")->id : 0;
        $sql['parent_id'] = 0;
        $sql['name'] = strip_tags($this->input->post('name'));
        $sql['content'] = strip_tags($this->input->post('content'));
        $sql['weight'] = 1;
        $sql['status'] = 0;
        
        if($this->db->insert($this->_table, $sql)){
            echo 'Bình luận của bạn đã gửi thành công. BodyCare.vn sẽ giải đáp thắc mắc của bạn trong thời gian nhanh nhất. Xin chân thành cám ơn việc góp ý của bạn tới BodyCare.vn';
        }else echo 'Đã xảy ra lỗi. Vui lòng thử lại';
    }

    public function answer_comment(){
        $sql['product_id'] = $this->input->post('product_id');
        $sql['member_id'] = ($this->session->userdata("user_log")) ? $this->session->userdata("user")->id : 0;
        $sql['parent_id'] = $this->input->post('comment_id');;
        $sql['name'] = strip_tags($this->input->post('name'));
        $sql['content'] = strip_tags($this->input->post('content'));
        $sql['weight'] = 1;
        $sql['status'] = 0;
        
        if($this->db->insert($this->_table, $sql)){
            echo 'Tin trả lời của bạn đã được gửi và chờ duyệt.';
        }else echo 'Đã xảy ra lỗi. Vui lòng thử lại';
    }

}

