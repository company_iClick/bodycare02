<?php

class M_comment extends CI_Model {

    protected $_table = 'comment';
    function __construct() {
        parent::__construct();
        $this->load->database();

    }

    function show_list_comment($where = array(), $limit, $offset) {
        $this->db->select("comment.*, product.name as product_name");
        $this->db->where($where);
        $this->db->group_by("product_id");
        $this->db->limit($limit, $offset);
        $this->db->order_by("read", "asc");
        $this->db->order_by("id", "desc");
        $this->db->from('comment');
        $this->db->join('product', 'product.id = comment.product_id');
        return $this->db->get()->result();

    }

    function menu_admin($product_id, $parentid = 0, $space = "", $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }
        $this->db->where('product_id', $product_id);
        $this->db->where('parent_id', $parentid);
        $this->db->order_by("weight", "asc");
        $this->db->order_by("id", "desc");
        $result    = $this->db->get($this->_table)->result();
        $trees_obj = array();
        if (!empty($result)) {
            foreach ($result as $rs) {
                $support_staff = $this->global_function->get_row_object(array('id' => $rs->support_staff_id), 'id, name', 'support_staff');
                $name = (!empty($support_staff)) ? $support_staff->name : $rs->name;
                $trees[] = array(
                    'id' => $rs->id,
                    'parent_id' => $rs->parent_id,
                    'content' => $rs->content,
                    'weight' => $rs->weight,
                    'status' => $rs->status,
                    'name' => $space . $name,
                    );
                $trees   = $this->menu_admin($product_id, $rs->id, $space . '|---', $trees);
            }
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $tree        = (object) $tree;
                $trees_obj[] = $tree;
            }
        }
        return $trees_obj;
    }


     function count_comment_read($where = array()) {
        $this->db->select('id');
        $this->db->where($where);
        $this->db->where('read', 0);
        $this->db->from('comment');
        return $this->db->count_all_results();
     }
    // get id
    function get_commentID($id)
    {
        $this->db->where('id', $id);
        $this->db->from($this->_table);
        return $this->db->get()->row();
    } 

    function count_comment_where($where = array()) {
        $this->db->where($where);
        $this->db->group_by("product_id");
        $this->db->from('comment');
        return $this->db->get()->num_rows();


     }

     function get_all_tree_id($parentid = 0, $trees = array())
     {
        if (!$trees) {
            $trees = array();
        }
        $trees[] = $parentid;
        $this->db->select('id');
        $this->db->where('parent_id', $parentid);
        $result    = $this->db->get($this->_table)->result();
        $trees_obj = array();
        if (!empty($result)) {
            foreach ($result as $rs) {
                $trees[] = $rs->id;
                $trees   = $this->get_all_tree_id($rs->id, $trees);
            }
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $trees_obj[] = $tree;
            }
        }
        $trees_obj = array_unique($trees_obj);
        return $trees_obj;
    }

    function count_comment_all_id($list_id, $where = array()) {
        $this->db->select('id');
        $this->db->where_in('product_id', $list_id);
        $this->db->where($where);
        $this->db->from('comment');
        return $this->db->get()->num_rows();


     }

}

