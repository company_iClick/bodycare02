<?php if(!empty($list_comment)){ ?>
<ul class="media-list">
<?php foreach($list_comment as $comment){ 
    $content = $comment->content;
    $username = $comment->username;
    $created_at = date('H:i d-m-Y',strtotime($comment->created_at));
?>
    <li class="media">

        <div class="media-body">

            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object img-circle " src="<?php echo _images; ?>/user_avatar.png" />
                </a>
                <div class="media-body" >
                    <?php echo $content; ?>
                    <br />
                    <small class="text-muted"><?php echo $username; ?> | <?php echo $created_at; ?></small>
                    <hr />
                </div>
            </div>

        </div>
    </li>
<?php } ?>
</ul>
<?php }else{ ?>
<h5>Không có bình luận nào.</h5>
<?php } ?>
