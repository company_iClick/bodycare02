<?php 
	$send_money = (!empty($detail->send_money)) ? '<li class="guitien">Gửi tiền:<span>' . $detail->send_money . '</span></li>' : NULL;
	$withdrawal = (!empty($detail->withdrawal)) ? '<li class="ruttien">Rút tiền:<span> ' . $detail->withdrawal . '</span></li>' : NULL;
	$interface = (!empty($detail->interface)) ? '<li class="giaodien">Giao diện:<span> ' . $detail->interface . '</span></li>' : NULL;
	$evalution = (!empty($detail->evalution)) ? '<li class="khuyenmai">Khuyến mãi:<span> ' . $detail->evalution . '</span></li>' : NULL;
	$support = (!empty($detail->support)) ? '<li class="hotro">Hỗ trợ:<span> ' . $detail->support . '</span></li>' : NULL;
	$odds = (!empty($detail->odds)) ? '<li class="odds">Odds:<span> ' . $detail->odds . '</span></li>' : NULL;
	$evalutionHtml = $send_money . $withdrawal . $interface . $evalution . $support . $odds;
	
	$rating_image = (!empty($detail->rate_image)) ? '<img src="' . base_url() . 'uploads/avatar/' . $detail->rate_image . '" alt="star" />'  : NULL;
?>
<div class="main_container">
    <div class="main-col">
        <?php echo $breadcrumb; ?>
        <div class="nhacai type-post status-publish format-standard has-post-thumbnail base-box blog-post p-single bp-horizontal-share single-nhacai">

            <div class="entry-content">

                
                <div class="info-nhacai-left">
                    <a href="<?php echo $detail->link; ?>" title="<?php echo $detail->name; ?>" target="_blank">
                        <img src="<?php echo base_url('uploads/avatar/' . $detail->image) ?>" class="nhacai-img disappear appear">
                    </a>
                    <div class="clear"></div>
                    <a class="nhacai-link-visit" href="<?php echo $detail->link; ?>" target="_blank">Xem site</a>
                </div>
                <div class="clear"></div>
                <div class="info-nhacai-right">

                    <h1 class="nhacai-title"><?php echo $detail->name; ?></h1>
                    <div class="nhacai-chucnang">
                        <ul>
                            <?php echo $evalutionHtml; ?>
                        </ul>
                    </div>
                    <div class="nhacai-thuong">Khuyến mãi: <span><?php echo number_format($detail->promotion, 0, ',', '.'); ?></span> vnđ</div>

                </div>
                
                <div class="one_fourth last">

                    <div class="nhacai-title">Đánh Giá</div>

                    <div class="point">
                        <div class="point-first"><?php echo $detail->mark; ?></div>
                        <div class="point-gach">/</div>
                        <div class="point-second">10</div>
                    </div>

                    <div class="rating">
                        <?php echo $rating_image; ?>
                    </div>
                    <div class="total">
                        Tổng số: <i class="fa-icon-user"></i> <?php echo number_format($detail->rate, 0, ',', '.'); ?> </div>

                </div>
            </div>
            <div class="clear"></div>
			
            <?php if(!empty($link_article)){ ?>
            <div class="wrap-box-clgt">
                <div class="nhacai-button">
                <?php $i=0; foreach($link_article as $link){ 
					$name = $link->name;
					$link = $link->links;
					$linkClass = NULL;
					if($i == 1) $linkClass = ' guitien';
					if($i == 2) $linkClass = ' ruttien';
				?>
                    <div class="nhacai-link-huongdan<?php echo $linkClass; ?>">
                        <a href="<?php echo $link; ?>" target="_blank"><?php echo $name; ?></a>
                    </div>
                    
                <?php $i++;} ?>    
                </div>
            </div>
            <div class="clear"></div>
            <?php } ?>

            <div class="entry-content">
             	  <?php echo $detail->content; ?> 
            </div>


        </div>
    </div>
    <div class="clear"></div>
</div>

<?php $this->load->view('front/block/right'); ?>