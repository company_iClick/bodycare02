<div class="news-box bookies-news-box">
    <header class="nb-header">
        <h1 class="nb-title" style=";">
            <span>Danh sách nhà cái cá cược bóng đá uy tín nhất tại Việt Nam</span>
        </h1>
    </header>
</div>
<?php if(!empty($betting)){ ?>
<div class="widget bookies-box table-responsive">		
    <section class="section">
    <table class='tbl-danhsachnhacai'>
        <thead>
            <tr class="tbl-header">
                <th class="nRank"><span>RANK</span></th>
                <th class="nName"><span>NHÀ CÁI</span></th>
                <th class="nSale"><span>KHUYẾN MÃI</span></th>
                <th class="nFunc"><span>ĐÁNH GIÁ</span></th>
                <th class="nGrade"><span>ĐIỂM</span></th>
                <th class="nRate"><span>RATING</span></th>
                <th class="nInfo"><span>THÔNG TIN</span></th>
            </tr>
        </thead>
        <tbody>
        <?php $i=0; foreach($betting as $bet){ 
			$sort = $i + 1;
			if($i+1 == 1) $sort = '<img alt="nha cai bong da top 1" src="' . base_url() . 'themes/images/rank1.png" />';
			if($i+1 == 2) $sort = '<img alt="nha cai bong da top 2" src="' . base_url() . 'themes/images/rank2.png" />';
			if($i+1 == 3) $sort = '<img alt="nha cai bong da top 3" src="' . base_url() . 'themes/images/rank3.png" />';
			
			$name = $bet->name;
			$link_to = $bet->link;
			$photo = base_url() . 'uploads/avatar/' . $bet->photo;
			$photoHtml = sprintf('<a href="%s" target="_blank">
                	<img alt="%s" src="%s" />
                </a>', $link_to, $name, $photo);
			
			$percent = $bet->percent . '%';
			$promotion = number_format($bet->promotion, 0, ',', '.');
			$promotionHtml = sprintf('<span class="nSalePercent">Thưởng <em>%s</em> lên đến</span>
                    <span class="nSaleMoney"><em>%s</em> vnđ</span>', $percent, $promotion);
					
			$send_money = (!empty($bet->send_money)) ? 'Gửi tiền:<span> ' . $bet->send_money . '</span><br>' : NULL;
			$withdrawal = (!empty($bet->withdrawal)) ? 'Rút tiền:<span> ' . $bet->withdrawal . '</span><br>' : NULL;
			$interface = (!empty($bet->interface)) ? 'Giao diện:<span> ' . $bet->interface . '</span><br>' : NULL;
			$evalution = (!empty($bet->evalution)) ? 'Khuyến mãi:<span> ' . $bet->evalution . '</span><br>' : NULL;
			$support = (!empty($bet->support)) ? 'Hỗ trợ:<span> ' . $bet->support . '</span><br>' : NULL;
			$odds = (!empty($bet->odds)) ? 'Odds:<span> ' . $bet->odds . '</span><br>' : NULL;
			$evalutionHtml = $send_money . $withdrawal . $interface . $evalution . $support . $odds;
			
			$mark = $bet->mark;
			$rating_image = (!empty($bet->rate_image)) ? '<img src="' . base_url() . 'uploads/avatar/' . $bet->rate_image . '" alt="star" />'  : NULL;

			$rate = number_format($bet->rate, 0, ',', '.');
			$rateHtml = sprintf('%s<span>Lượt đánh giá: <strong>%s</strong></span>', $rating_image, $rate);
			
			$link_detail = site_url('nha-cai-uy-tin/' . $bet->slug);
			$visit_site_html = sprintf('<a target="_blank" href="%s" class="nWebsite">Visit Site</a>
                    <a href="%s" class="nDetail">Đánh giá chi tiết</a>', $link_to, $link_detail);		
			
		?>
            <tr>
                <td class="nOrder">
                	<?php echo $sort; ?>
                </td>
                <td>
               <?php echo $photoHtml; ?>
                </td>
                <td class="nSaleMoney">
                    <?php echo $promotionHtml; ?>
                </td>
                <td class="nFunc">
                    <?php echo $evalutionHtml; ?>
                </td>
                <td class="nDiem">
                    <div class="rank-meter">
                    <span class="clock clock-5"></span>
                    <span class="rank-meter-text"><?php echo $mark; ?></span>
                </div>
                </td>
                <td class="nRating">
                    <?php echo $rateHtml; ?>
                </td>
                <td>
                    <?php echo $visit_site_html; ?>
                </td>
            </tr>
        <?php $i++;} ?> 
       	
       </tbody>
    </table>
    </section>

</div>
<?php } ?>