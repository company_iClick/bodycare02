<?php



class m_betting extends CI_Model {



    function __construct() {

        parent::__construct();

        $this->load->database();

    }

    function show_list_betting($where = array(), $limit, $offset) {

        $this->db->where($where);

        $this->db->order_by('weight', "ASC");
		
		$this->db->order_by('id', "DESC");
		
        $this->db->limit($limit, $offset);

        $this->db->from('betting');

        return $this->db->get()->result();

        $this->db->free_result();

    }

    // get id

    function get_bettingID($id) {

        $this->db->where('betting.id', $id);

        $this->db->from('betting');

        return $this->db->get()->row();

    }

    // get tmp



}

