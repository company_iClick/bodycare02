<?php

class Betting extends MX_Controller

{

    function __construct()

    {
        parent::__construct();

        $this->load->database();

        $this->load->helper(array("url"));

        $this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
	

    }
	
	protected $_table = 'betting';

    function index(){

        $data['betting']=$this->global_function->show_list_table_where(array("status"=>1),$this->_table,"id,name,slug,promotion,percent,evalution,mark,send_money,withdrawal,interface,support,odds,rate,rate_image,photo,link","weight","ASC");

        $data['active']='betting';

        $this->template->write('mod', 'betting');

        $this->template->write('title', 'Danh sách những nhà cái cá cược bóng đá uy tín nhất Việt Nam');

        $this->template->write_view('content', 'public/index', $data, TRUE);

        $this->template->render();

    }

  

    function detail($slug='')

    {

        $data['detail']=$this->global_function->get_tableWhere(array("status"=>1,"slug"=>$slug),$this->_table,"*");

        if(!isset($data['detail']->id)) redirect(site_url());
		
		$data['link_article'] = $this->global_function->show_list_table_where(array("status"=>1),"link_article","*","weight","ASC");

        $data['breadcrumb'] = '<div class="category-title">
            <div class="mom_breadcrumb breadcrumb breadcrumbs">
                <div class="breadcrumbs-plus">';
		
		$data['breadcrumb'] .= ' <span><a itemprop="url" href="'.site_url().'" class="home"><span itemprop="title">Trang chủ</span></a></span>';
		
		$data['breadcrumb'] .= ' <span><i class="sep fa-icon-double-angle-right"></i><a itemprop="url" href="'.site_url('nha-cai-uy-tin').'" class="home"><span itemprop="title">Nhà cái uy tín</span></a></span>';
	       
	   $data['breadcrumb'] .=  '<span class="separator"><i class="sep fa-icon-double-angle-right"></i></span>'.$data['detail']->name.'';
	   $data['breadcrumb'] .= '</div></div></div>';

        $this->template->write('mod', 'detail');

        $this->template->write('title', $data['detail']->name);

        $this->template->write_view('content', 'public/detail', $data, TRUE);

        $this->template->render();

    }

}