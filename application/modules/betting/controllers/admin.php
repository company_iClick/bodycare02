<?php



/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */



class Admin extends CI_Controller {



    function __construct() {

        parent::__construct();

        $this->load->database();

        $this->load->helper(array("url"));

        $this->load->model(array("betting/m_betting", "general", "m_session",));

        $this->template->set_template('admin');        // Set template 



    }



    function index($page_no = 1) {

        //echo $page_no;exit;

        if (!($this->general->Checkpermission("view_betting")))

            redirect(site_url("admin/not-permission"));

        // tool all

        if (isset($_POST['show']) && $this->input->post('checkall') != "") {

            $array = array_keys($this->input->post('checkall'));

            foreach ($array as $a) {

                $this->show_more($a);

            }

            redirect(site_url('admin/betting/index/'.$type."/".$page_no) . '?messager=success');

        }

        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {

            $array = array_keys($this->input->post('checkall'));

            foreach ($array as $a) {

                //--------change parent------

                $this->hide_more($a);

            }

            redirect(site_url('admin/betting/index/'.$type."/".$page_no) . '?messager=success');

        }

        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {

            $array = array_keys($this->input->post('checkall'));

            foreach ($array as $a) {

                //--------change parent------

                $this->delete_more($a);

            }

            redirect(site_url('admin/betting/index/'.$type."/".$page_no) . '?messager=success');

        }

        //end toll

        $page_co = 20;

        $start = ($page_no - 1) * $page_co;

        $count = $this->general->count_table_where(array(), 'betting');

        //echo $count;

        $data['page_no'] = $page_no;

 

        $data['item'] = $this->m_betting->show_list_betting(array(), $page_co, $start);

        //var_dump($data['item']);exit();

        $data['link'] = $this->general->paging($page_co, $count, 'admin/betting/index/', $page_no);

        $this->template->write('mod', "betting"); // set mod

        $this->template->write_view('content', 'admin/index', $data, TRUE);

        $this->template->render();

    }

    function add($type=1) {

        if (!($this->general->Checkpermission("add_betting")))

            redirect(site_url("admin/not-permission"));

        $data = array();

        $data['breadcrumb'] = '<li>>></li><li><a href="back/betting">Loại Bài Viết</a></li><li>>></li><li class="current">Thêm mới</li>';

        if (isset($_POST['ok'])) {

            $this->form_validation->set_rules('name', 'Tên', 'trim|required');

            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');

            if ($this->form_validation->run() == TRUE) {

                if (($_FILES['photo']['name'] != '')) {

                        $photo = $this->global_function->upload_img("photo", "avatar", 150, 150);

                }else{

                    $photo="NULL";

                }
				
				if (($_FILES['rate_image']['name'] != '')) {

                        $rate_image = $this->global_function->upload_img("rate_image", "avatar", 150, 150);

                }else{

                    $rate_image="NULL";

                }
				
				if (($_FILES['image']['name'] != '')) {

                        $image = $this->global_function->upload_img("image", "avatar", 300, 250);

                }else{

                    $image="NULL";

                }


                $sql = array(

                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'slug' => $this->global_function->unicode($this->input->post('name')),
					'promotion' => $this->input->post('promotion'),
					'percent' => $this->input->post('percent'),
					'evalution' => $this->input->post('evalution'),
					'send_money' => $this->input->post('send_money'),
					'withdrawal' => $this->input->post('withdrawal'),
					'interface' => $this->input->post('interface'),
					'support' => $this->input->post('support'),
					'odds' => $this->input->post('odds'),
					'mark' => $this->input->post('mark'),
					'rate' => $this->input->post('rate'),
					'link' => $this->input->post('link'),
					'content' => $this->input->post('content'),
                    'status' => $this->input->post('status'),
                    'photo' => $photo,
					'rate_image' => $rate_image,
					'image' => $image,
                );

                $this->db->insert('betting', $sql);

               
                redirect(site_url('admin/betting/index/'.$type) . '?messager=success');

            }

        }


        $this->template->write('mod', "betting"); // set mod

        $this->template->write_view('content', 'admin/add', $data, TRUE);

        $this->template->render();

    }



    function edit($id) {

        if (!($this->general->Checkpermission("edit_betting")))

            redirect(site_url("admin/not-permission"));

        if (isset($_POST['ok'])) {

            $this->form_validation->set_rules('name', 'Tên', 'trim|required');

            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');

            if ($this->form_validation->run() == TRUE) {


                if (($_FILES['photo']['name'] != '')) {

                        $photo = $this->global_function->upload_img("photo", "avatar", 150, 150);

                }else{

                    $photo = $this->input->post("old_photo");

                }
				
				if (($_FILES['rate_image']['name'] != '')) {

                    $rate_image = $this->global_function->upload_img("rate_image", "avatar", 150, 150);

                }else{

                    $rate_image = $this->input->post("old_rate_image");

                }
				
				if (($_FILES['image']['name'] != '')) {

                    $image = $this->global_function->upload_img("image", "avatar", 300, 250);

                }else{

                    $image = $this->input->post("old_image");

                }


                $sql = array(

                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'slug' => $this->global_function->unicode($this->input->post('name')),
					'promotion' => $this->input->post('promotion'),
					'percent' => $this->input->post('percent'),
					'evalution' => $this->input->post('evalution'),
					'send_money' => $this->input->post('send_money'),
					'withdrawal' => $this->input->post('withdrawal'),
					'interface' => $this->input->post('interface'),
					'support' => $this->input->post('support'),
					'odds' => $this->input->post('odds'),
					'mark' => $this->input->post('mark'),
					'rate' => $this->input->post('rate'),
					'link' => $this->input->post('link'),
					'content' => $this->input->post('content'),
                    'status' => $this->input->post('status'),
                    'photo' => $photo,
					'rate_image' => $rate_image,
					'image' => $image,
                );
				
			

                $this->db->where('id', $id);

                $this->db->update('betting', $sql);

              

                redirect(site_url('admin/betting/edit/'.$id) . '?messager=success');

            }

        }



        $this->template->write('mod', "betting"); // set mod

        $data['item'] = $this->m_betting->get_bettingID($id);

      

        $this->template->write_view('content', 'admin/edit', $data, TRUE);

        $this->template->render();

    }



//=========================================== 

    function hide($id,$page_no) {

        //exit($id);

        if (!($this->general->Checkpermission("edit_betting")))

            redirect(site_url("admin/not-permission"));

        $this->general->update_tableID($id, array('status' => 0), "betting");

        redirect(site_url('admin/betting/index/' . $page_no) . '?messager=success');

    }
	
	function hide_hot($id,$page_no) {

        //exit($id);

        if (!($this->general->Checkpermission("edit_betting")))

            redirect(site_url("admin/not-permission"));

        $this->general->update_tableID($id, array('hot' => 0), "betting");

        redirect(site_url('admin/betting/index/' . $page_no) . '?messager=success');

    }



//============================================\

    function hide_more($id) {

        //echo $id; exit($id);

        if (!($this->general->Checkpermission("edit_betting")))

            redirect(site_url("admin/not-permission"));

        $this->general->update_tableID($id, array('status' => 0), "betting");

        return true;

    }



//============================================\

    function show_more($id) {

        //	exit($id);

        if (!($this->general->Checkpermission("edit_betting")))

            redirect(site_url("admin/not-permission"));

        $this->general->update_tableID($id, array('status' => 1), "betting");

        return true;

    }



//============================================\

    function show($id,$page_no) {

        if (!($this->general->Checkpermission("edit_betting")))

            redirect(site_url("admin/not-permission"));

        $this->general->update_tableID($id, array('status' => 1), "betting");

        redirect(site_url('admin/betting/index/' . $page_no) . '?messager=success');

    }
	
	function show_hot($id,$page_no) {

        if (!($this->general->Checkpermission("edit_betting")))

            redirect(site_url("admin/not-permission"));

        $this->general->update_tableID($id, array('hot' => 1), "betting");

        redirect(site_url('admin/betting/index/' . $page_no) . '?messager=success');

    }



// ============================================

    function delete($id,$page_no) {

        //exit($id);

        if (!($this->general->Checkpermission("delete_betting")))

            redirect(site_url("admin/not-permission"));

            $this->db->delete('betting', array('id' => $id));

            redirect(site_url('admin/betting/index/'.$page_no) . '?messager=success');



    }



    function delete_more($id) {

        if (!($this->general->Checkpermission("delete_betting")))

            redirect(site_url("admin/not-permission"));

            $this->db->delete('betting', array('id' => $id));

            return true;

    }

}

