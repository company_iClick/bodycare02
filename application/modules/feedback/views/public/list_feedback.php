<?php if(!empty($array_comment)): ?>
    <div class="list_commnet1">
        <?php foreach($array_comment as $row){ 
            $parent = $row['parent'];
            $child = (isset($row['child'])) ? array_reverse($row['child']) : '';
            $comment_id = $row['parent']['id'];
            $avatar = ($this->session->userdata('user_log')) ? base_url('themes/images/img_ac.png') : base_url('themes/images/thumb_admin.png');
        ?>
        <div class="child_comment1">
            <div class="avatar_comment"><img src="<?php echo $avatar; ?>"></div>
            <span class="name_faq">
                <b><?php echo $parent['name']; ?></b>
                <p><?php echo $parent['content']; ?> <b class="click_comment_b" onclick="toogle_reaply(<?php echo $comment_id; ?>)"><i class="icon-traloi"></i>&nbsp; Trả lời </b> </p>
                 <form class="form_send_comment1 css_none" id="form_send_comment_<?php echo $comment_id; ?>">
                    <div class="form_send_comment_box">
                        <textarea name="comment_content" id="comment_content_<?php echo $comment_id; ?>" placeholder="Viết bình luận"></textarea>
                        <div id="errorCommentContent<?php echo $comment_id; ?>"></div>
                    </div>
                    <div class="form_send_comment_box">
                        <input type="button" value="Gửi" class="submit_send_comment" onclick="answer_feedback(<?php echo $comment_id; ?>)">
                    </div>
                 </form>
            </span>
            
            <?php if(!empty($child)): 
                foreach ($child as $rc) :
                    $name = $rc['name'];
                    $content = $rc['content'];  
                    $avatar = ($this->session->userdata('user_log')) ? base_url('themes/images/img_ac.png') : base_url('themes/images/thumb_admin.png');
            ?>

            <div class="child_comment1_1">
                <i class="arrow_comment1"></i>
                <div class="avatar_comment"><img src="<?php echo $avatar; ?>"></div>
                <span class="name_faq">
                    <b><?php echo $name; ?></b>
                    <p><?php echo $content; ?></p>
                </span>
            </div>
            <?php endforeach;endif; ?>
        </div>
        <?php } ?>
    </div>
    <?php endif; ?>