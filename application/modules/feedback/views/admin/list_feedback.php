<?php if(!empty($feedbacks)){ ?>
<label for="tags"><h3>Danh sách phản hồi:</h3></label>
<ul>
    <?php foreach($feedbacks as $feedback){ 
        $id = $feedback->id;
        $created_at = date('H:i d/m/Y', strtotime($feedback->created_at));
        $content = $feedback->content;
        $child_feedback = $feedback->child_feedback;
    ?>
    <li style="padding:0 5px 10px;padding-top: 10px;border-bottom: 1px dotted #ccc;position: relative;">
        <a href="javascript:void(0)" onclick="delete_feedback(<?php echo $id; ?>)" style="position: absolute;top: 10px;right:0;color: #f00">X</a>
        <b><?php echo $user->email; ?></b>
        <div style="font-size:11px;margin-bottom: 5px"><i><?php echo $created_at; ?></i></div>
        
        <p style="padding-bottom: 5px"><?php echo $content; ?></p>
        <a href="javascript:void(0)" onclick="load_answer(<?php echo $user->id; ?>,<?php echo $id; ?>)" style="font-size:11px;color: blue; text-decoration: underline;">Trả lời</a>
        <div id="load_answer_<?php echo $id; ?>"></div>
        <?php if(!empty($child_feedback)){?>
        	<ul style="padding-left: 20px">
        		<?php foreach($child_feedback as $c): 
        			$feedback_name = ($c->member_id == 0) ? 'Admin' : $user->email;
        			$id = $c->id;
			        $created_at = date('H:i d/m/Y', strtotime($c->created_at));
			        $content = $c->content;
        		?>
        		<li style="padding-top: 10px;border-bottom: 1px solid #ccc;position: relative;">
                    <a href="javascript:void(0)" onclick="delete_feedback(<?php echo $id; ?>)" style="position: absolute;top: 10px;right:0;color: #f00">X</a>
        			<b><?php echo $feedback_name; ?></b>
			        <div style="font-size:11px;margin-bottom: 5px"><i><?php echo $created_at; ?></i></div>
			        
			        <p style="padding-bottom: 5px"><?php echo $content; ?></p>
        		</li>
    			<?php endforeach; ?>
        	</ul>
        <?php } ?>
    </li>
    <?php } ?>
</ul>
<?php } ?>