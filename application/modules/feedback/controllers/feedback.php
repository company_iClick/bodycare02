<?php

class Feedback extends MX_Controller {
    
    protected $_table = 'feedback';
    
    function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->model(array("feedback/a_feedback"));
        $this->load->driver('cache', array('adapter' => 'file', 'backup' => 'file'));
    }
    
    public function list_feedback(){       
        $session_user = $this->session->userdata("user"); 
        $list_feedback = $this->a_feedback->all_feedback_product($session_user->id);
        $array_feedback = array();
        if(!empty($list_feedback)){
            foreach ($list_feedback as $row) {
                $id = $row['id'];
                $parent_id = $row['parent_id'];
                if($parent_id == 0) $array_feedback[$id]['parent'] = $row;
                else $array_feedback[$parent_id]['child'][] = $row;
                
            }
        }
        $data['array_comment'] = $array_feedback;
        $this->load->view('public/list_feedback', $data);
    }


    public function send_feedback(){
        if(!$this->session->userdata("user_log")) redirect(site_url());

        $sql['member_id'] = $this->session->userdata("user")->id;
        $sql['parent_id'] = 0;
        $sql['content'] = strip_tags($this->input->post('content'));
        $sql['weight'] = 1;
        $sql['status'] = 1;

        $user = $this->global_function->get_row_object(array('id' => $sql['member_id']), '*', 'users');
        $user_name = (!empty($user->full_name)) ? $user->full_name : $user->email;

        $result = array();

        $body = '<div style="width:700px;margin:auto"><table>';
        $body .= '
            <tr>
                <th colspan="2">Phản hồi mới từ website '. site_url() .'</th>
            </tr>
            <tr>
                <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
                <th>Thành viên :</th><td>'.$user_name.'</td>
            </tr>
            <tr>
                <th>Nội dung :</th><td>'.$sql['content'].'</td>
            </tr>';
        $body .= '</table>';

        $result = array();
        $result['mailto'] = 'dangthinh@iclick.vn';
        $result['mailcc'] = 'hientt@iclick.vn';
        $result['title'] = 'Phản hồi mới từ website ' . site_url();
        $result['content'] = $body;
        
        if($this->db->insert($this->_table, $sql)){
            $result['message'] = 'Bạn đã gửi phản hồi thành công. Bodycare.vn sẽ liên hệ trả lời bạn trong thời gian sớm nhất.';
        }else $result['message'] = 'Đã xảy ra lỗi. Vui lòng thử lại';

        echo json_encode($result);
    }

    public function answer_feedback(){
        if(!$this->session->userdata("user_log")) redirect(site_url());

        $sql['member_id'] = ($this->session->userdata("user_log")) ? $this->session->userdata("user")->id : 0;
        $sql['parent_id'] = $this->input->post('comment_id');;
        $sql['content'] = strip_tags($this->input->post('content'));
        $sql['weight'] = 1;
        $sql['status'] = 1;

        $this->db->insert($this->_table, $sql);

        $user = $this->global_function->get_row_object(array('id' => $sql['member_id']), '*', 'users');

        $user_name = (!empty($user->full_name)) ? $user->full_name : $user->email;

        $feedbacks = $this->global_function->show_list_table_where(array('member_id' => $sql['member_id'], 'parent_id' => 0, 'status' => 1), 'feedback', "*", "id", "DESC");

        if(!empty($feedbacks)){
            foreach ($feedbacks as $key => $value) {
                $value->child_feedback = $this->global_function->show_list_table_where(array('parent_id' => $value->id, 'status' => 1), 'feedback', "*", "id", "ASC");
            }
            
        }

        $body = '<div style="width:700px;margin:auto"><table>';
        $body .= '
            <tr>
                <th colspan="2">Trả lời phản hồi từ website '. site_url() .'</th>
            </tr>
            <tr>
                <th colspan="2">&nbsp;</th>
            </tr>
            <tr>
                <th>Thành viên :</th><td>'.$user_name.'</td>
            </tr>
            <tr>
                <th>Nội dung :</th><td>'.$sql['content'].'</td>
            </tr>';
        $body .= '</table>';

        $body .= '<div style="width:700px;margin:auto;margin-top:10px">Xem toàn bộ nội dung trò truyện:';
        $body .= '<ul>';
        foreach ($feedbacks as $key => $value) {
            $child_feedback = $value->child_feedback;
            $body .= '<li><b>'. $user_name .': </b>'.$value->content;
            if(!empty($child_feedback)){
                $body .= '<ul>';
                foreach ($child_feedback as $key => $value) {
                    $feedback_name = ($value->member_id == 0) ? 'Admin' : $user_name;
                    $body .= '<li><b>'.$feedback_name.': </b>'.$value->content.'</li>';
                }
                $body .= '</ul>';
            }
            $body .= '</li>';
        }
        $body .= '</ul></div>';

        $result = array();
        $result['mailto'] = 'dangthinh@iclick.vn';
        $result['mailcc'] = 'hientt@iclick.vn';
        $result['title'] = 'Trả lời phản hồi từ website ' . site_url();
        $result['content'] = $body;

        echo json_encode($result);
    }

}

