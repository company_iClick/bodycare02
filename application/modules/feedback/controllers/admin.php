<?php
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array(
            "url"
        ));
        $this->load->model(array(
            "feedback/m_feedback",
            "general"
        ));
        $this->template->set_template('admin');
        $this->template->write('mod', "feedback");
    }

    function index($page_no = 1){
        if (!($this->general->Checkpermission("feedback")))
            redirect(base_url("admin/not-permission"));

        $page_co         = 20;
        $start           = ($page_no - 1) * $page_co;
        $count           = $this->m_feedback->count_feedback_where(array(
            'parent_id' => 0
        ), 'feedback');
        $data['page_no'] = $page_no;
        $data['item']    = $this->m_feedback->show_list_feedback(array('parent_id' => 0), $page_co, $start);
        if(!empty($data['item'])){
            foreach ($data['item'] as $row) {
                $product_id = $row->product_id;
                $list_id = $this->m_feedback->get_all_tree_id($product_id);

                $row->product_name = $this->global_function->get_row_object(array('id' => $product_id), 'name', 'product')->name;

                $row->count_feedback = $this->m_feedback->count_feedback_all_id($list_id, array('read' => 0));
            }       
        }

        $data['link']    = $this->general->paging($page_co, $count, 'admin/feedback/' , $page_no);
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();

    }
  
    function product($product_id, $page_no = 1)
    {
        if (!($this->general->Checkpermission("view_feedback")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST['show']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(base_url('admin/feedback/' . $product_id . '/' . $page_no) . '?messager=success');
        }
        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->hide_more($a);
            }
            redirect(base_url('admin/feedback/' . $product_id . '/' . $page_no) . '?messager=success');
        }
        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->delete_more($a);
            }
            redirect(base_url('admin/feedback/' . $product_id . '/' . $page_no) . '?messager=success');
        }

        $sql['read']  = 1;
        $this->db->where('product_id', $product_id);
        $this->db->update('feedback', $sql);

        $page_co         = 20;
        $start           = ($page_no - 1) * $page_co;
        $count           = $this->general->count_table_where(array(
            'id !=' => 0,
            'product_id' => $product_id
        ), 'feedback');
        $data['page_no'] = $page_no;
        $data['item']    = $this->m_feedback->menu_admin($product_id);

        $data['product_id'] = $product_id;
        $data['link']    = $this->general->paging($page_co, $count, 'admin/feedback/' . $product_id . '/', $page_no);
        $this->template->write_view('content', 'admin/product', $data, TRUE);
        $this->template->render();
    }
    function add($product_id)
    {
        if (!($this->general->Checkpermission("add_feedback")))
            redirect(base_url("admin/not-permission"));
        $data               = array();
        $data['breadcrumb'] = '<li>>></li><li><a href="back/feedback">Thêm bình luận</a></li><li>>></li><li class="current">Thêm mới</li>';
        if (isset($_POST['ok'])) {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required|max_length[100]');
            $this->form_validation->set_rules('support_staff_id', 'Nhân viên hỗ trợ', 'required');
            $this->form_validation->set_rules('content', 'Nội dung câu hỏi', 'trim|required');
            $this->form_validation->set_rules('answer', 'Nội dung trả lời', 'trim|required');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'product_id' => $product_id,
                    'name' => $this->input->post('name'),
                    'content' => $this->input->post('content'),
                    'status' => $this->input->post('status'),
                    'weight' => $this->input->post('weight'),
                    'read' => 1,
                    'user_id' => $this->session->userdata('admin_login')->id
                );
                if($this->db->insert('feedback', $sql)){
                    $id = $this->db->insert_id();

                    if($this->input->post('answer')){
                        $sql_answer = array(
                            'admin' => 1,
                            'parent_id' => $id,
                            'product_id' => $product_id,
                            'support_staff_id' => $this->input->post('support_staff_id'),
                            'content' => $this->input->post('answer'),
                            'read' => 1,
                            'weight' => 1,
                            'status' => 1,
                            'user_id' => $this->session->userdata('admin_login')->id,
                        );
                        $this->db->insert('feedback', $sql_answer);
                    }
                }
            
                redirect(base_url('admin/feedback/product/' . $product_id) . '?messager=success');
            }
        }

        $data['product'] = $this->global_function->get_row_object(array('id' => $product_id), 'id, name,slug,created_at', 'product');
        $data['support_staff'] = $this->global_function->get_array_object(array(), 'id, name', 'support_staff');

        $this->template->write('mod', "feedback");
        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }
    function edit($product_id, $id)
    {
        if (!($this->general->Checkpermission("add_feedback")))
            redirect(base_url("admin/not-permission"));
        $data               = array();
        $data['breadcrumb'] = '<li>>></li><li><a href="back/feedback">Thêm bình luận</a></li><li>>></li><li class="current">Thêm mới</li>';

        $data['item'] = $this->m_feedback->get_feedbackID($id);

        $feedback_parent = $this->global_function->get_row_object(array('id' => $data['item']->parent_id), 'id, name', 'feedback');
        $parent_id = (!empty($feedback_parent )) ? $feedback_parent->id : $id;

        $this->db->where('id', $id);
        $this->db->update('feedback', array('read' => 1));

        if (isset($_POST['ok'])) {

            $this->form_validation->set_rules('support_staff_id', 'Nhân viên hỗ trợ', 'required');
            $this->form_validation->set_rules('content', 'Nội dung câu hỏi', 'trim|required');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'product_id' => $product_id,
                    'name' => $this->input->post('name'),
                    'content' => $this->input->post('content'),
                    'status' => $this->input->post('status'),
                    'weight' => $this->input->post('weight'),
                    'read' => 1,
                    'user_edit_id' => $this->session->userdata('admin_login')->id
                );
                
                $this->db->where('id', $id);

                if($this->db->update('feedback', $sql)){
                    if($this->input->post('answer')){
                        
                        $sql_answer = array(
                            'admin' => 1,
                            'parent_id' => $parent_id,
                            'product_id' => $product_id,
                            'content' => $this->input->post('answer'),
                            'support_staff_id' => $this->input->post('support_staff_id'),
                            'read' => 1,
                            'weight' => 1,
                            'status' => 1,
                            'user_id' => $this->session->userdata('admin_login')->id,
                        );
                        $this->db->insert('feedback', $sql_answer);
                    }
                }
            
                redirect(base_url('admin/feedback/product/' . $product_id) . '?messager=success');
            }
        }

        $data['product'] = $this->global_function->get_row_object(array('id' => $product_id), 'id, name,slug,created_at', 'product');
        $data['list_feedback'] = $this->m_feedback->menu_admin($product_id);
        $data['support_staff'] = $this->global_function->get_array_object(array(), 'id, name', 'support_staff');
        

        $this->template->write('mod', "feedback");
        $this->template->write_view('content', 'admin/edit', $data, TRUE);
        $this->template->render();
    }
    function view($id)
    {
        if (!($this->general->Checkpermission("edit_feedback")))
            redirect(base_url("admin/not-permission"));
        $data['item'] = $this->m_feedback->get_feedbackID($id);
        $sql['read']  = 1;
        $this->db->where('id', $id);
        $this->db->update('feedback', $sql);
        $this->template->write_view('content', 'admin/view', $data, TRUE);
        $this->template->render();
    }
    function hide($product_id, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_feedback")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "feedback");
        redirect(base_url('admin/feedback/product/' . $product_id . '/' . $page_no) . '?messager=success');
    }
    function hide_more($id)
    {
        if (!($this->general->Checkpermission("edit_feedback")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 0
        ), "feedback");
        return true;
    }
    function show_more($id)
    {
        if (!($this->general->Checkpermission("edit_feedback")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "feedback");
        return true;
    }
    function show($product_id, $id, $page_no)
    {
        if (!($this->general->Checkpermission("edit_feedback")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            'status' => 1
        ), "feedback");
        redirect(base_url('admin/feedback/product/' . $product_id . '/' . $page_no) . '?messager=success');
    }
    function delete($product_id, $id, $page_no)
    {
        if (!($this->general->Checkpermission("delete_feedback")))
            redirect(base_url("admin/not-permission"));
        $this->db->delete('feedback', array(
            'id' => $id
        ));
        redirect(base_url('admin/feedback/product/' . $product_id . '/' . $page_no) . '?messager=success');
    }
    function delete_more($id)
    {
        if (!($this->general->Checkpermission("delete_feedback")))
            redirect(base_url("admin/not-permission"));
        $this->db->delete('feedback', array(
            'id' => $id
        ));
        return true;
    }

    function list_feedback($id){
        $data['user'] = $this->general->get_tableID($id, "users");
        $data['feedbacks'] = $this->global_function->show_list_table_where(array('member_id' => $id, 'parent_id' => 0, 'status' => 1), 'feedback', "*", "id", "DESC");
        if(!empty($data['feedbacks'])){
            foreach ($data['feedbacks'] as $key => $value) {
                $value->child_feedback = $this->global_function->show_list_table_where(array('parent_id' => $value->id, 'status' => 1), 'feedback', "*", "id", "ASC");
            }
            
        }

        $this->load->view('admin/list_feedback', $data);
    }

    function answear($feedback_id){
        $data['parent_id'] = $feedback_id;  
        $data['content'] = $this->input->post('content');
        $data['status'] = 1;
        $data['read'] = 1;
        $data['weight'] = 1;
        if($this->db->insert('feedback', $data)){
            echo 1;
        }else echo 0;
    }

    function delete_feedback($feedback_id){
        $child_feedback = $this->global_function->show_list_table_where(array('parent_id' => $feedback_id), 'feedback', "id", "id", "DESC");
        if(!empty($child_feedback)){
            foreach ($child_feedback as $key => $value) {
                $this->db->where('id', $value->id);
                $this->db->delete('feedback');
            }
        }
        $this->db->where('id', $feedback_id);
        if($this->db->delete('feedback')){
            echo 1;
        }else echo 0;
    }
}

