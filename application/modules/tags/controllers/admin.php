<?php
class Admin extends CI_Controller {

    protected $_table = 'tags';

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper(array("url"));
        $this->load->model(array("tags/m_tags","product/m_product","article/m_article","category/m_category", "general", "global_function"));
        $this->template->set_template('admin');        // Set template 
        $this->template->write('mod', "tags"); // set mod
    }

    function index($page_no = 1) {
        if (!($this->general->Checkpermission("view_tags")))
            redirect(base_url("admin/not-permission"));
        // tool all
        if (isset($_POST['show']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                $this->show_more($a);
            }
            redirect(base_url('admin/tags') . '?messager=success');
        }
        if (isset($_POST['hide']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                //--------change parent------
                $this->hide_more($a);
            }
            redirect(base_url('admin/tags') . '?messager=success');
        }
        if (isset($_POST['delete']) && $this->input->post('checkall') != "") {
            $array = array_keys($this->input->post('checkall'));
            foreach ($array as $a) {
                //--------change parent------
                $this->delete_more($a);
            }
            redirect(base_url('admin/tags') . '?messager=success');
        }
        //end toll
        $page_co = 2;
        $start = ($page_no - 1) * $page_co;
        $count = $this->general->count_tableWhere(array("id !=" => 0), "tags");
        $data['page_no'] = $page_no;
        $data['list'] = $this->m_tags->show_list_tags_where(array("id !=" => 0), $page_co, $start);
        $data['link'] = $this->general->paging($page_co, $count, 'admin/tags' . "/", $page_no);
        $this->template->write_view('content', 'admin/index', $data, TRUE);
        $this->template->render();
    }

    function add() {
        if (!($this->general->Checkpermission("add_tags")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST["ok"])) {
          
            $this->form_validation->set_rules('name', 'Tên', 'trim|required|max_length[100]');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                $sql = array(
                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'slug' => ($this->input->post('slug') != '') ? $this->input->post('slug') : $this->my_lib->changeTitle($this->input->post('name')),
                    'summary' => $this->input->post('summary'),
                    'status' => $this->input->post('status'),
                    'title' => $this->input->post('title'),
                    'keywords' => $this->input->post('keywords'),
                    'description' => $this->input->post('description'),
                    'user_id' => $this->session->userdata('admin_login')->id
                );
				
				
                if($this->db->insert("tags", $sql)){
                    $id_insert = $this->db->insert_id();
                    $postIdProduct = array_unique($this->input->post('id_product'));
                    if(!empty($postIdProduct)){
                        foreach ($postIdProduct as $key => $value) {
                            $this->db->insert('tmp_product_tag', array('tag_id' => $id_insert, 'product_id' => $value));
                        }    
                    }

                    $postIdArticle = array_unique($this->input->post('id_article'));
                    if(!empty($postIdArticle)){
                        foreach ($postIdArticle as $key => $value) {
                            $this->db->insert('tmp_article_tag', array('tag_id' => $id_insert, 'article_id' => $value));
                        }    
                    }

                    $postIdCategory = $this->input->post('id_category');
                    if(!empty($postIdCategory)){
                        foreach ($postIdCategory as $key => $value) {
                            $this->db->insert('tmp_category_tag', array('tag_id' => $id_insert, 'category_id' => $value));
                        }    
                    }
    
                    redirect(base_url('admin/tags') . '?messager=success');
                }else {
                    redirect(base_url('admin/tags') . '?messager=warning');
                }
                
            }
        }

        $data['category'] = $this->m_category->menu_admin(0);
        $data['product'] = $this->global_function->get_array_object(array('status' => 1), 'id, name', 'product', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['article'] = $this->global_function->get_array_object(array('status' => 1, 'type' => 3), 'id, name', 'article', 0, array('field' => 'name', 'sort' => 'ASC'));

        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }

    function edit($id) {
        if (!($this->general->Checkpermission("add_tags")))
            redirect(base_url("admin/not-permission"));
        if (isset($_POST["ok"])) {
            $this->form_validation->set_rules('name', 'Tên', 'trim|required|max_length[100]');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run() == TRUE) {
                   $sql = array(
                    'weight' => $this->input->post('weight'),
                    'name' => $this->input->post('name'),
                    'slug' => ($this->input->post('slug') != '') ? $this->input->post('slug') : $this->my_lib->changeTitle($this->input->post('name')),
                    'summary' => $this->input->post('summary'),
                    'status' => $this->input->post('status'),
                    'title' => $this->input->post('title'),
                    'keywords' => $this->input->post('keywords'),
                    'description' => $this->input->post('description'),
                    'modified_in' => date('Y-m-d H:i:s'),
                    'user_edit_id' => $this->session->userdata('admin_login')->id
                );

                $this->db->where('id', $id);   
                if($this->db->update('tags', $sql)){

                    $this->db->delete('tmp_product_tag', array('tag_id' => $id));
                    $postIdProduct = array_unique($this->input->post('id_product'));
                    if(!empty($postIdProduct)){
                        foreach ($postIdProduct as $key => $value) {
                            $this->db->insert('tmp_product_tag', array('tag_id' => $id, 'product_id' => $value));
                        }    
                    }

                    $this->db->delete('tmp_article_tag', array('tag_id' => $id));
                    $postIdArticle = array_unique($this->input->post('id_article'));
                    if(!empty($postIdArticle)){
                        foreach ($postIdArticle as $key => $value) {
                            $this->db->insert('tmp_article_tag', array('tag_id' => $id, 'article_id' => $value));
                        }    
                    }

                    $this->db->delete('tmp_category_tag', array('tag_id' => $id));
                    $postIdCategory = $this->input->post('id_category');
                    if(!empty($postIdCategory)){
                        foreach ($postIdCategory as $key => $value) {
                            $this->db->insert('tmp_category_tag', array('tag_id' => $id, 'category_id' => $value));
                        }    
                    }

                } 

                redirect(base_url('admin/tags/edit/'.$id) . '?messager=success');
            } else {
                redirect(base_url('admin/tags/edit/'.$id) . '?messager=warning');
            }
        }

        $data["item"] = $this->m_tags->show_detail_tags(array('id' => $id));
        $data['category'] = $this->m_category->menu_admin(0);
        $data['product'] = $this->global_function->get_array_object(array('status' => 1), 'id, name', 'product', 0, array('field' => 'name', 'sort' => 'ASC'));
        $data['article'] = $this->global_function->get_array_object(array('status' => 1, 'type' => 3), 'id, name', 'article', 0, array('field' => 'name', 'sort' => 'ASC'));

        //get tmp_product_tag
        $data["list_tmp_product_tag"] = $this->m_tags->show_list_tmp_product_tags_where($id);
        $array_product_tags = array();
        if(!empty($data["list_tmp_product_tag"])){
            foreach ($data["list_tmp_product_tag"] as $key => $value) {
                $array_product_tags[] = $value->product_id;
            }
        } 
        $data['array_product_tags'] = $array_product_tags;

        //get tmp_article_tag
        $data["list_tmp_tag_article"] = $this->m_tags->show_list_tmp_article_tags_where($id);

        $array_article_tags = array();
        if(!empty($data["list_tmp_tag_article"])){
            foreach ($data["list_tmp_tag_article"] as $key => $value) {
                $array_article_tags[] = $value->article_id;
            }
        } 
        $data['array_article_tags'] = $array_article_tags;

        $data["list_tmp_tag_category"] = $this->m_tags->show_list_tmp_category_tags_where($id);
        $array_category_tags = array();
        if(!empty($data["list_tmp_tag_category"])){
            foreach ($data["list_tmp_tag_category"] as $key => $value) {
                $array_category_tags[] = $value->category_id;
            }
        } 
        $data['array_category_tags'] = $array_category_tags;
           
        $this->template->write_view('content', 'admin/add', $data, TRUE);
        $this->template->render();
    }

    function load_product(){
        $product_name = $this->input->post('product_name');
        $id = $this->input->post('id');
        $category_id = $this->input->post('category_id');

        if($id != 0){
            $data["list_tmp_product_tag"] = $this->m_tags->show_list_tmp_product_tag_id($id);
            $array_product_tags = array();
            if(!empty($data["list_tmp_product_tag"])){
                foreach ($data["list_tmp_product_tag"] as $key => $value) {
                    $array_product_tags[] = $value->product_id;
                }
            } 
            $data['array_product_tags'] = $array_product_tags;
        }        

        $keyword = $this->my_lib->changeTitle($product_name);
        $list_category = $this->m_category->get_categroy_tree_id($category_id );
        $data['result'] = $this->m_product->show_list_product_keyword_category($list_category, $keyword);
        $this->load->view('admin/load_product', $data);
    }

    function load_article(){
        $article_name = $this->input->post('article_name');
        $id = $this->input->post('id');
        $category_id = $this->input->post('category_id');

        if($id != 0){
            $data["list_tmp_article_tag"] = $this->m_tags->show_list_tmp_article_tag_id($id);
            $array_article_tags = array();
            if(!empty($data["list_tmp_article_tag"])){
                foreach ($data["list_tmp_article_tag"] as $key => $value) {
                    $array_article_tags[] = $value->article_id;
                }
            } 
            $data['array_article_tags'] = $array_article_tags;
        }        

        $keyword = $this->my_lib->changeTitle($article_name);
        $data['result'] = $this->m_article->show_list_article_keyword_category($category_id, $keyword, 3);
        $this->load->view('admin/load_article', $data);
    }

//=========================================== 
    function hide($id) {
        if (!($this->general->Checkpermission("edit_tags")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 0), "tags");
        redirect(base_url('admin/tags') . '?messager=success');
    }

//============================================\
    function hide_more($id) {
        if (!($this->general->Checkpermission("edit_tags")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 0), "tags");
        return true;
    }

//============================================\
    function show_more($id) {
        if (!($this->general->Checkpermission("edit_tags")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 1), "tags");
        return true;
    }

//============================================\
    function show($id) {
        if (!($this->general->Checkpermission("edit_tags")))
            redirect(base_url("admin/not-permission"));
        $this->general->update_tableID($id, array('status' => 1), "tags");
        redirect(base_url('admin/tags') . '?messager=success');
    }

// ============================================
    function delete($id) {
        if (!($this->general->Checkpermission("delete_tags")))
        redirect(base_url("admin/not-permission"));

        $count_product = $this->m_tags->count_product($id);
        if ($count_product > 0)
            redirect(base_url('admin/tags/index/' . $page_no) . '?messager=delete_error');

        if($this->db->delete($this->_table, array('id' => $id))){
            $this->db->delete('tmp_product_tag', array('tag_id' => $id));
            $this->db->delete('tmp_article_tag', array('tag_id' => $id));
            $this->db->delete('tmp_category_tag', array('tag_id' => $id));
        }
        redirect(base_url('admin/tags') . '?messager=success');
    }

    function delete_more($id) {
        if (!($this->general->Checkpermission("delete_tags")))
            redirect(base_url("admin/not-permission"));
        $count_product = $this->m_tags->count_product($id);
        if ($count_product == 0){
            if($this->db->delete($this->_table, array('id' => $id))){
                $this->db->delete('tmp_product_tag', array('tag_id' => $id));
                $this->db->delete('tmp_article_tag', array('tag_id' => $id));
                $this->db->delete('tmp_category_tag', array('tag_id' => $id));
            }
        }

        return true;
    }

}
