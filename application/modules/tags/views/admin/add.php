<?php
    $this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));

    $tag_id = (isset($item)) ? $item->id : 0;
    $name = (isset($item)) ? $item->name : $this->input->post('name');
    $slug = (isset($item)) ? $item->slug : $this->input->post('slug');
    $summary = (isset($item)) ? $item->summary : $this->input->post('summary');
    $title = (isset($item)) ? $item->title : $this->input->post('title');
    $keywords = (isset($item)) ? $item->keywords : $this->input->post('keywords');
    $description = (isset($item)) ? $item->description : $this->input->post('description');
    $weight = (isset($item)) ? $item->weight : 1;

    $title_bar  = (isset($item)) ? 'Cập nhật' : 'Thêm mới';

?>
<form name="them" method="post" id="them" action=""	enctype="multipart/form-data">
<div class="contentcontainer med left">

    <div class="headings altheading"><h2><?php echo $title_bar; ?></h2></div>

    <div class="contentbox">
   
            <p>
                <label for="textfield"><strong>Tên</strong></label>
                <input type="text" id="textfield" class="inputbox" name="name" value="<?php echo $name; ?>" OnkeyUp="add_alias_vn(this)" />
            </p>
            <?php echo form_error('name'); ?>
            
            <p>
                <label for="textfield"><strong>Đường dẫn</strong></label>
                <input type="text" class="inputbox" name="slug" value="<?php echo $slug; ?>" />
                <?php if(isset($item)){?><br /><span class="smltxt">Đường dẫn hiện tại: (<i style="color: #f00"><?php echo $slug; ?>)</i></span><?php } ?>
            </p>

            <p>
                <label for="textfield"><strong>Mô tả</strong></label>
                <textarea name="summary" class="textarea inputbox" rows="5"><?php echo $summary; ?></textarea>
            </p>
    
            <p class="div_lang">
                <label for="textfield"><strong>Title</strong></label>
                <input type="text" id="textfield" class="inputbox title_vn" name="title" value="<?php echo $title; ?>" />
            </p>
            <p class="div_lang">
                <label for="textfield"><strong>Meta Description</strong></label>
                <textarea class="textarea inputbox"  id="description" name="description" rows="5"><?php echo $description; ?></textarea>
            </p>
            <p class="div_lang" >
                <label for="textfield"><strong>Meta keywords</strong></label>
                <textarea class="textarea inputbox"  id="keywords" name="keywords" rows="5"><?php echo $keywords; ?></textarea>
            </p> 
                
            <p>
                <label for="smallbox"><strong>Thứ Tự: </strong></label>
                <input type="text" name="weight"  value="<?php echo $weight; ?>" style="width: 30px; text-align: center" class="inputbox">
            </p>

            <p>
                <input type="radio" value="1" name="status" <?php if(isset($item) && $item->status==1){?> checked <?php }else echo 'checked';?>>Hiển thị
                <input type="radio" value="0" name="status" <?php if(isset($item) && $item->status==0){?> checked <?php }?>>Ẩn
            </p>

            <input type="submit" value="<?php echo $title_bar; ?>" name="ok" class="btn" />
            <a class="btn" style="color: #fff" href="admin/tags">Thoát</a>  

    </div><!-- end contentbox -->
    

</div>

<div class="contentcontainer sml right">
	<div class="headings altheading">
        <h2 class="left">Thuộc tính</h2>
    </div>
    <div class="contentbox">
        <div class="list">
            <?php if(!empty($category)){?>
            <p>
                <select class="inputbox" id="category_product" onchange="load_product_cate(this)">
                    <option value="0">Chọn danh mục</option>
                    <?php foreach($category as $row){
                        $id = $row->id;
                        $name = $row->name;
                    ?>
                    <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                    <?php } ?>
                </select>
            </p>
            <?php } ?>
        </div>
        <div class="list">
            <p>
                <label for="other_item"><b>Chọn sản phẩm:</b></label>
    			<?php if(!empty($product)){ ?> 
                <input type="text" class="inputbox" placeholder="Nhập tên sản phẩm" id="product_name" data-id="<?php echo $tag_id; ?>" onkeyup="load_product(this)">
                <div style="max-height:300px;overflow: auto;position: relative;" id="load_product">
                 <?php foreach($product as $value){ 
                    $id = $value->id;
                    $name = $value->name;
                ?>  
                    <div><input type="checkbox" name="product[]" id="input_product_<?php echo $id; ?>" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>"<?php if(isset($item) && !empty($array_product_tags) && in_array($id, $array_product_tags)) echo ' checked'; ?>> <?php echo $name; ?></div>
                 <?php } ?>
                 </div>
                 <?php } ?>   
            </p>
            <div id="append_input_product" style="padding:10px 0">
                <label for="tags"><b>Sản phẩm đã chọn:</b> </label>
                <?php if(!empty($list_tmp_product_tag)){
                    foreach ($list_tmp_product_tag as $key => $value) {
                        $id = $value->product_id;
                        $name = $value->name;
                ?>
                <div id="product_<?php echo $id; ?>" style="float:left;margin-right:10px;margin-bottom:5px;padding:5px 7px;border:1px solid #ccc;position:relative"><?php echo $name; ?></li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_product" data-id="<?php echo $id; ?>"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="id_product[]" value="<?php echo $id; ?>"></div>
                <?php }} ?>
            </div>
        </div>

        <div style="clear: both;height: 20px"></div>
        <hr>
        <div class="list">
            <label for="other_item"><b>Kiến thức làm đẹp :</b></label>
            <?php if(!empty($category)){?>
            <p>
            <select class="inputbox" id="category_article" onchange="load_article_cate(this)">
                <option value="0">Chọn danh mục</option>
                <?php foreach($category as $row){
                    $id = $row->id;
                    $name = $row->name;
                ?>
                <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                <?php } ?>
            </select>
            </p>
            <?php } ?>
            <p>
                <label for="other_item"><b>Chọn bài viết :</b></label>
                <?php if(!empty($article)){ ?> 
                <input type="text" class="inputbox" placeholder="Nhập tên bài viết" id="article_name" data-id="<?php echo $tag_id; ?>" onkeyup="load_article(this)">
                <div style="max-height:300px;overflow: auto;position: relative;" id="load_article">
                <?php foreach($article as $value){ 
                    $id = $value->id;
                    $name = $value->name;
                ?>  
                <div><input type="checkbox" name="article[]" id="input_article_<?php echo $id; ?>" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>"<?php if(isset($item) && !empty($array_article_tags) && in_array($id, $array_article_tags)) echo ' checked'; ?>> <?php echo $name; ?></div>
                 <?php } ?>
                 </div>
                 <?php } ?>   
            </p>
            <div id="append_input_article" style="padding:10px 0">
                <label for="tags"><b>Tin tức đã chọn:</b> </label>
                <?php if(!empty($list_tmp_tag_article)){
                    foreach ($list_tmp_tag_article as $key => $value) {
                        $id = $value->article_id;
                        $name = $value->name;
                ?>
                <div id="article_<?php echo $id; ?>" style="float:left;margin-right:10px;margin-bottom:5px;padding:5px 7px;border:1px solid #ccc;position:relative"><?php echo $name; ?></li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_article" data-id="<?php echo $id; ?>"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="id_article[]" value="<?php echo $id; ?>"></div>
                <?php }} ?>
             </div>
        </div>
        <div class="contentbox">
            <p>
                <label for="other_item"><b>Chọn danh mục :</b></label>
                <?php if(!empty($category)){?>
                <div style="max-height:350px;overflow: auto;position: relative;" id="load_category">
                <?php foreach($category as $value){ 
                    $id = $value->id;
                    $name = $value->name;
                ?>  
                    <div><input type="checkbox" name="id_category[]" value="<?php echo $id; ?>"<?php if(!empty($array_category_tags) && in_array($id, $array_category_tags)) echo ' checked'; ?>> <?php echo $name; ?></div>
                <?php } ?>
                </div>
                <?php } ?>
            </p>
        </div>
    </div>
</div>
</form>


<script type="text/javascript" charset="utf-8" async defer>

    $(document).ready(function(){
        select_product();
        delete_product();
        select_article();
        delete_article();
    })

    function select_product() {
        $('input[name="product[]"]').change(function() {
            var id = $(this).val();
            var name = $(this).data('name');
            if($(this).is(':checked')){
                var html = '<div id="product_'+ id +'" style="float:left;margin-right:10px;margin-bottom:5px;padding:5px 7px;border:1px solid #ccc;position:relative">'+ name +'</li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_product" data-id="'+ id +'"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="id_product[]" value="'+ id +'"></div>';
                $('#append_input_product').append(html);
            }else{
                $('#append_input_product').find($('#product_' + id)).remove();
            }
            
            
        });
    }

    function delete_product(){
        $(document).on('click', '.delete_product', function() {
            // if(!confirm('Bạn muốn xóa mục này?')) return false;
            var id = $(this).data('id');
            $('#input_product_' + id).prop('checked', false);
            $(this).parent().remove();
        });
    }

    function load_product(e){
        var category_id = $('#category_product').val();
        var product_name = $(e).val();
        var id = $(e).data('id');

        $.ajax({
            url: "admin/tags/load_product",
            type: "POST",
            data:{'product_name':product_name, 'id': id, 'category_id': category_id},
            beforeSend: function (data) {
                $("#load_product").append("<p id='loading-page'></p>");
                $("#load_product").addClass("bgwhite");
            },
            success: function (data) {
                $("#load_product").html(data);
                $("#load_product").removeClass("bgwhite");
                select_product();
                delete_product();
            }
        }); 

        return false
    }

    function load_product_cate(e){
        var category_id = $(e).val();
        var product_name = $('#product_name').val();
        var id = $('#product_name').data('id');

        $.ajax({
            url: "admin/tags/load_product",
            type: "POST",
            data:{'product_name':product_name, 'id': id, 'category_id': category_id},
            beforeSend: function (data) {
                $("#load_product").append("<p id='loading-page'></p>");
                $("#load_product").addClass("bgwhite");
            },
            success: function (data) {
                $("#load_product").html(data);
                $("#load_product").removeClass("bgwhite");
                select_product();
                delete_product();
            }
        }); 

        return false
    }

    function load_article(e){
        var category_id = $('#category_article').val();
        var article_name = $(e).val();
        var id = $(e).data('id');

        $.ajax({
            url: "admin/tags/load_article",
            type: "POST",
            data:{'article_name':article_name, 'id': id, 'category_id': category_id},
            beforeSend: function (data) {
                $("#load_article").append("<p id='loading-page'></p>");
                $("#load_article").addClass("bgwhite");
            },
            success: function (data) {
                $("#load_article").html(data);
                $("#load_article").removeClass("bgwhite");
                select_article();
                delete_article();
            }
        }); 

        return false
    }

    function load_article_cate(e){
        var category_id = $(e).val();
        var article_name = $('#article_name').val();
        var id = $('#article_name').data('id');

        $.ajax({
            url: "admin/tags/load_article",
            type: "POST",
            data:{'article_name':article_name, 'id': id, 'category_id': category_id},
            beforeSend: function (data) {
                $("#load_article").append("<p id='loading-page'></p>");
                $("#load_article").addClass("bgwhite");
            },
            success: function (data) {
                $("#load_article").html(data);
                $("#load_article").removeClass("bgwhite");
                select_article();
                delete_article();
            }
        }); 

        return false
    }

    function select_article() {
        $('input[name="article[]"]').change(function() {
            var id = $(this).val();
            var name = $(this).data('name');
            if($(this).is(':checked')){
                var html = '<div id="article_'+ id +'" style="float:left;margin-right:10px;margin-bottom:5px;padding:5px 7px;border:1px solid #ccc;position:relative">'+ name +'</li><span style="position:absolute;top:-8px;right:-5px;color:#f00;cursor: pointer;line-height: 0;" class="delete_article" data-id="'+ id +'"><i class="fa fa-times" aria-hidden="true"></i></span><input type="hidden" name="id_article[]" value="'+ id +'"></div>';
                $('#append_input_article').append(html);
            }else{
                $('#append_input_article').find($('#article_' + id)).remove();
            }
            
            
        });
    }

    function delete_article(){
        $(document).on('click', '.delete_article', function() {
            // if(!confirm('Bạn muốn xóa mục này?')) return false;
            var id = $(this).data('id');
            $('#input_article_' + id).prop('checked', false);
            $(this).parent().remove();
        });
    }

	function locdau(str){
		str= str.toLowerCase();
		str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
		str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
		str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
		str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
		str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
		str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
		str= str.replace(/đ/g,"d");
		str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|\;|\||\{|\}|~/g,"-");
		str= str.replace(/^\-+|\-+$/g,"-");
		str= str.replace(/\\/g,"-");
		str= str.replace(/-+-/g,"-");
		return str;
	}


	function add_alias_vn(obj){
		var str = $(obj).val();
		var als = locdau(str);
		$(".nam_vn").val($(".name_vn").val());
		$(".link_vn").val(als);
		//$(".title_vn").val($(".name_vn").val());
	}

    CKEDITOR.replace('summary', {height: 300, width: "98%", resize_enabled: true,
        filebrowserBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Images',
    });

</script>
