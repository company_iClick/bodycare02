<?php

class M_tags extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // show list tags
    function show_list_tags($lang = 'vn') {
        $this->db->select("*");
        $this->db->order_by('tags.weight', "ASC");
		$this->db->order_by('tags.id', "DESC");
        $this->db->from('tags');
        return $this->db->get()->result();
    }
    // show list tags where
    function show_list_tags_where($where=array(), $limit, $offset) {
        $this->db->select("*");
        $this->db->where($where);
        $this->db->order_by('tags.weight', "ASC");
		$this->db->order_by('tags.id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from('tags');
        return $this->db->get()->result();
    }
	
	function show_detail_tags($where=array()) {
        $this->db->select("*");
		$this->db->where($where);
        $this->db->from('tags');
        return $this->db->get()->row();
    }

    function count_item_in_tags($id_item, $tag_id) {
        //$list_article = explode(',', $list_article);
        $new_where =  "FIND_IN_SET('".$id_item."', id_item)";
        $this->db->select("id");
        $this->db->where($new_where);
        $this->db->where('id', $tag_id);
        $this->db->from('tags');
        return $this->db->get()->num_rows();
    }

    function show_list_tmp_product_tag_id($tag_id) {
        $this->db->where('tag_id', $tag_id);
        $this->db->from('tmp_product_tag');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_tag_id($tag_id) {
        $this->db->where('tag_id', $tag_id);
        $this->db->from('tmp_article_tag');
        return $this->db->get()->result();
    }

    function show_list_tag_array() {
        $this->db->select("id,name as label, name as value");
        $this->db->where('status', 1);
        $this->db->order_by('name', "ASC");
        $this->db->from('tags');
        return $this->db->get()->result_array();
    }

    function show_list_tags_in_product($product_id) {
        $this->db->select("id, name");
        $this->db->where('status', 1);
        $this->db->where('tmp_product_tag.product_id', $product_id);
        $this->db->order_by('tags.name', "ASC");
        $this->db->order_by('tags.id', "DESC");
        $this->db->from('tags');
        $this->db->join('tmp_product_tag', 'tmp_product_tag.tag_id = tags.id');
        return $this->db->get()->result();
    }


    function show_list_tags_in_article($article_id) {
        $this->db->select("id, name");
        $this->db->where('status', 1);
        $this->db->where('tmp_article_tag.article_id', $article_id);
        $this->db->order_by('tags.name', "ASC");
        $this->db->order_by('tags.id', "DESC");
        $this->db->from('tags');
        $this->db->join('tmp_article_tag', 'tmp_article_tag.tag_id = tags.id');
        return $this->db->get()->result();
    }

    function count_product($tag_id)
    {
         $this->db->select('product.id');
         $this->db->where('tmp_product_tag.tag_id', $tag_id);
         $this->db->group_by('product.id');
         $this->db->from('product');
         $this->db->join('tmp_product_tag', 'tmp_product_tag.product_id=product.id');
         return $this->db->count_all_results();
    }

    function show_list_tmp_product_tags_where($tag_id) {
        $this->db->select("tmp_product_tag.product_id, product.name");
        $this->db->where('tag_id', $tag_id);
        $this->db->from('tmp_product_tag');
        $this->db->join('product', 'product.id = tmp_product_tag.product_id');
        return $this->db->get()->result();
    }

    function show_list_tmp_article_tags_where($tag_id) {
        $this->db->select("tmp_article_tag.article_id, article.name");
        $this->db->where('tag_id', $tag_id);
        $this->db->from('tmp_article_tag');
        $this->db->join('article', 'article.id = tmp_article_tag.article_id');
        return $this->db->get()->result();
    }

    function show_list_tmp_category_tags_where($tag_id) {
        $this->db->select("*");
        $this->db->where('tag_id', $tag_id);
        $this->db->from('tmp_category_tag');
        return $this->db->get()->result();
    }
    
}
