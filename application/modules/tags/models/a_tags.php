<?php

class A_tags extends CI_Model {

    protected $_table = 'tags';
    protected $_tmp_ptg = 'tmp_product_tag';
    protected $_tmp_pc = 'tmp_product_category';
    protected $_pr = 'product';
    protected $_cate = 'category';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
    function get_all_item($where = array()){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.tag_id = $this->_table.id");
        $this->db->join("$this->_pr", "$this->_pr.id = $this->_tmp_ptg.product_id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->result();
    }
	
    function show_list_tags_in_product($product_id) {
        $this->db->select("id, name, slug");
        $this->db->where('status', 1);
        $this->db->where('tmp_product_tag.product_id', $product_id);
        $this->db->order_by('tags.name', "ASC");
        $this->db->order_by('tags.id', "DESC");
        $this->db->from('tags');
        $this->db->join('tmp_product_tag', 'tmp_product_tag.tag_id = tags.id');
        return $this->db->get()->result();
    }

    function show_list_tags_in_article($article_id) {
        $this->db->select("id, name, slug");
        $this->db->where('status', 1);
        $this->db->where('tmp_article_tag.article_id', $article_id);
        $this->db->order_by('tags.name', "ASC");
        $this->db->order_by('tags.id', "DESC");
        $this->db->from('tags');
        $this->db->join('tmp_article_tag', 'tmp_article_tag.tag_id = tags.id');
        return $this->db->get()->result();
    }
    
    function show_list_items_in_tags($id) {
        $this->db->select("item.id,itemdetail.item_name,item.value,item.sale,item.new,item.price,itemdetail.item_link,images.name as photo,images.folder,images.thumb");

        $this->db->where('tmp_item_tag.tag_id', $id);
        $this->db->where(array("item_status" => 1, "itemdetail.active" => 1));
        $this->db->order_by('item.item_weight', "ASC");
        $this->db->order_by('item.id', "DESC");
        $this->db->group_by('item.id');
        $this->db->from('item');
        $this->db->join('tmp_item_tag', 'tmp_item_tag.item_id = item.id');
        $this->db->join('itemdetail', 'itemdetail.item_id=item.id');

        $this->db->join('itemimages', 'item.id=itemimages.item_id');
        $this->db->join('images', 'itemimages.image_id=images.id');
        return $this->db->get()->result();
    }

    function show_list_article_in_tags($id, $limit, $offset)
    {
        $this->db->select("articledetail.article_name,articledetail.article_link,articledetail.article_summary,article.id,article.date_create,article.date_modify,article.article_status,article.folder,articleterm.term_id,article.photo,article.thumb");
        $this->db->where('tmp_article_tag.tag_id', $id);
        $this->db->where(array("article.article_status"=>'1',"articledetail.active"=>1));
        $this->db->from('article');
        $this->db->limit($limit, $offset);
        $this->db->order_by("article.article_weight","ASC");
        $this->db->order_by("article.id","DESC");
        $this->db->join('tmp_article_tag', 'tmp_article_tag.article_id = .article.id');
        $this->db->join('articledetail','articledetail.article_id=article.id');
        $this->db->join('articleterm','articleterm.article_id=article.id');
                
        return $this->db->get()->result();
    }

    function count_list_article_in_tags($id)
    {
        $this->db->select("article.id");
        $this->db->where('tmp_article_tag.tag_id', $id);
        $this->db->where(array("article.article_status"=>'1',"articledetail.active"=>1));
        $this->db->from('article');
        $this->db->join('tmp_article_tag', 'tmp_article_tag.article_id = .article.id');
        $this->db->join('articledetail','articledetail.article_id=article.id');
        $this->db->join('articleterm','articleterm.article_id=article.id');
                
        return $this->db->count_all_results();
    }

    function get_all_item_category(){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_cate.id as cate_id, $this->_cate.name as cate_name, $this->_cate.slug as cate_slug, $this->_cate.parent_id")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1, "$this->_cate.status" => 1))
                ->order_by("$this->_cate.weight", "ASC")
                ->order_by("$this->_cate.id", "DESC")
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ptg", "$this->_tmp_ptg.tag_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ptg.product_id")
                ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_pr.id")
                ->join("$this->_cate", "$this->_cate.id = $this->_tmp_pc.category_id")
                ->get()
                ->result();
    }

    function get_category_in_tag($tag_id){
        $this->db->select("category.id,category.name, category.slug");
        $this->db->where(array("category.status" => 1, "tmp_category_tag.tag_id" => $tag_id));
        $this->db->order_by('category.name', "ASC");
        $this->db->order_by('category.id', "DESC");
        $this->db->from('category');
        $this->db->join('tmp_category_tag', 'tmp_category_tag.category_id = category.id');
        return $this->db->get()->result();
    }

    function count_category_in_tag($tag_id, $category_id){
        $this->db->select("category.id");
        $this->db->where(array("category.category_status" => 1, "category.id" => $category_id, "tmp_category_tag.tag_id" => $tag_id));
        $this->db->from('category');
        $this->db->join('tmp_category_tag', 'tmp_category_tag.category_id = category.id');
        return $this->db->get()->num_rows();
    }

}
