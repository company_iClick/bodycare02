<div class="wrapper_full">
    <div class="wrapper_full_1">

        <div class="left_account_user">

            <div class="title_loc_search">Tài khoản</div>
            <ul>
                <li><a href="<?php echo site_url('thong-tin-tai-khoan'); ?>">Thông tin tài khoản</a></li>
                <li><a href="<?php echo site_url('doi-mat-khau'); ?>" class="active_menu_a">Đổi mật khẩu</a></li>
                <li><a href="<?php echo site_url('dia-chi-giao-hang'); ?>">Địa chỉ giao hàng</a></li>
                <li><a href="<?php echo site_url('lich-su-don-hang'); ?>">Lịch sử đơn hàng</a></li>
            </ul>
        </div>

        <div class="right_account_user">

            <div class="right_account_user1">
                <?php if(!empty($message)): ?>
                <div id="message_change_pass"><div class="txt_success"><?php echo $message; ?></div></div>
                <?php endif; ?>
                <h1 class="h1_2">Đổi mật khẩu</h1>
                <form method="post" action="">
                    <div class="update_div_input">                        
                        <p>Mật khẩu hiện tại :</p>
                        <div class="input_update_frm">
                            <input type="password" name="old_password" class="input_update" value="<?php echo $old_password; ?>">
                            <?php echo form_error('old_password'); ?>
                        </div>
                    </div>

                    <div class="update_div_input">                        
                        <p>Mật khẩu mới :</p>
                        <div class="input_update_frm">
                            <input type="password" name="password" class="input_update" value="<?php echo $password; ?>">
                            <?php echo form_error('password'); ?>
                        </div>
                    </div>

                    <div class="update_div_input">                        
                        <p>Xác nhận mật khẩu mới :</p>
                        <div class="input_update_frm">
                            <input type="password" name="re_password" class="input_update" value="<?php echo $re_password; ?>">
                        </div>
                    </div>

                    <div class="update_div_input">
                        <p></p>
                        <input type="submit" class="submit_update" name="" value="Cập nhập">
                    </div>

                </form>
            </div>

        </div>


    </div>
</div>