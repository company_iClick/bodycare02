<div class="clear h1"></div>
<div class="col_full fleft box-content">
<div class="col-left-home fleft">
    <div class="clear h1"></div>
    <div class="relative col_full fleft">
        <div class="col3 fleft i-users">
            <div class="i-pic-u fleft i-employee"></div>
            <?php if($url=='dang-nhap'){?>
            <a href="<?php echo site_url('dang-nhap-nguoi-tim-viec')?>" class="btn-u bold">
                Đăng nhập Người tìm việc
            </a>
            <a href="<?php echo $this->global_function->get_row_link(2)?>" class="icon-guide fleft col_full bold text-center" target="_blank">
                Hướng dẫn Người tìm việc đăng nhập
            </a>
            <?php }else{?>
                <a href="<?php echo site_url('dang-ky-nguoi-tim-viec')?>" class="btn-u bold">
                    Đăng ký Người tìm việc
                </a>
                <a href="<?php echo $this->global_function->get_row_link(3)?>" class="icon-guide fleft col_full bold text-center" target="_blank">
                    Hướng dẫn Người tìm việc đăng ký
                </a>
            <?php }?>
        </div>
        <div class="col3 fleft i-users mleft2">
            <div class="i-pic-u fleft i-employer"></div>
            <?php if($url=='dang-nhap'){?>
            <a href="<?php echo site_url('dang-nhap-nha-tuyen-dung')?>" class="btn-u bold">
                Đăng nhập Nhà tuyển dụng
            </a>
            <a href="<?php echo $this->global_function->get_row_link(4)?>" class="icon-guide fleft col_full bold text-center" target="_blank">
                Hướng dẫn Nhà tuyển dụng đăng nhập
            </a>
            <?php }else{?>
                <a href="<?php echo site_url('dang-ky-nha-tuyen-dung')?>" class="btn-u bold">
                    Đăng ký Nhà tuyển dụng
                </a>
                <a href="<?php echo $this->global_function->get_row_link(5)?>" class="icon-guide fleft col_full bold text-center" target="_blank">
                    Hướng dẫn Nhà tuyển dụng đăng ký
                </a>
            <?php }?>
        </div>
    </div>
</div>
<?php echo modules::run('home/home/block_right_employer', array("type" => "users")); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        PageSearch();
    })
</script>