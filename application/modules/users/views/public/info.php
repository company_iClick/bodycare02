<div class="wrapper_full">
    <div class="wrapper_full_1">

        <div class="left_account_user">

            <div class="title_loc_search">tài khoản</div>
            <ul>
                <li><a href="<?php echo site_url('thong-tin-tai-khoan'); ?>" class="active_menu_a">Thông tin tài khoản</a></li>
                <li><a href="<?php echo site_url('doi-mat-khau'); ?>">Đổi mật khẩu</a></li>
                <li><a href="<?php echo site_url('dia-chi-giao-hang'); ?>">Địa chỉ giao hàng</a></li>
                <li><a href="<?php echo site_url('lich-su-don-hang'); ?>">Lịch sử đơn hàng</a></li>
            </ul>
        </div>

        <div class="right_account_user">

            <div class="right_account_user1">
                <?php if(!empty($message)): ?>
                <div id="message_change_pass"><div class="txt_success"><?php echo $message; ?></div></div>
                <?php endif; ?>
                <h1 class="h1_2">Thông tin tài khoản</h1>
                <form method="post" action="">
                    <div class="update_div_input">                        
                        <p>Email :</p>
                        <div class="input_update_frm">
                            <i onclick="enable_edit('email')"></i>
                            <input type="email" class="input_update" value="<?php echo $email; ?>" readonly="true" name="email">
                            <?php echo form_error('email'); ?>
                        </div>
                    </div>

                    <div class="update_div_input">
                        <p>Họ Tên :</p>
                        <div class="input_update_frm">
                            <i onclick="enable_edit('full_name')"></i>
                            <input type="text" class="input_update" value="<?php echo $full_name; ?>" readonly="true" name="full_name">
                            <?php echo form_error('full_name'); ?>
                        </div>
                    </div>

                    <div class="update_div_input"> 
                        <p>Địa chỉ :</p>
                        <div class="input_update_frm">
                            <i onclick="enable_edit('address')"></i>
                            <input type="text" class="input_update" value="<?php echo $address; ?>" readonly="true" name="address">
                            <?php echo form_error('address'); ?>
                        </div>
                    </div>
                    <div class="update_div_input">      
                        <p>Điên thoại ( <b>*</b> )</p>
                        <div class="input_update_frm">
                            <i onclick="enable_edit('phone')"></i>
                            <input type="text" class="input_update" value="<?php echo $phone; ?>" readonly="true" name="phone">
                            <?php echo form_error('phone'); ?>
                        </div>
                    </div>
                    <div class="update_div_input">
                        <p></p>
                        <input type="submit" class="submit_update" name="" value="Cập nhập">
                    </div>

                </form>
            </div>

        </div>


    </div>
</div>
