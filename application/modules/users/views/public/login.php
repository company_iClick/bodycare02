<div class="wrapper_full">
    <div class="wrapper_full_1 wrapper_full_5">
        <div class="register_user clearfix">
            <form class="register_form_user" method="post" action="">
                <div class="title_user clearfix">
                    <h1>bodycare.vn</h1>
                    <p>Đăng ký và mua sắm</p>
                </div>

                <div class="wp_input_user_login">
                    <a href="javascript:void(0)" class="login_face" onclick="fbLogin()">Login facebook<i></i></a>
                    <a href="<?php echo $loginGoogleUrl; ?>" class="login_gmail">Login google +<i></i></a>
                    <input type="text" name="email" value="<?php echo $email; ?>" class="input_user" placeholder="Email" required>
                    <input type="password" name="password" value="<?php echo $password; ?>" class="input_user" placeholder="Password" required>
                </div>
                <div class="wp_input_user2 clearfix">
                    <div class="forgot_pass">Quên mật khẩu? Nhấn vào <a href="<?php echo site_url('quen-mat-khau'); ?>">đây</a></div>
                    <a href="<?php echo site_url('dang-ky'); ?>" class="a_dangky">
                        <i class="icon-dangky"></i>
                        <span>Đăng ký</span>
                    </a>
                    <div class="btn_login">
                        <span>Đăng nhập</span>
                        <input type="submit" value="">
                        <i class="icon-dangnhap"></i>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>