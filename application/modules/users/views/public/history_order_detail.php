<div class="wrapper_full">
    <div class="wrapper_full_1">
        <div class="left_account_user">
            <div class="title_loc_search">Tài khoản</div>
            <ul>
                <li><a href="<?php echo site_url('thong-tin-tai-khoan'); ?>">Thông tin tài khoản</a></li>
                <li><a href="<?php echo site_url('doi-mat-khau'); ?>">Đổi mật khẩu</a></li>
                <li><a href="<?php echo site_url('dia-chi-giao-hang'); ?>">Địa chỉ giao hàng</a></li>
                <li><a href="<?php echo site_url('lich-su-don-hang'); ?>" class="active_menu_a">Lịch sử đơn hàng</a></li>
            </ul>
        </div>

        <div class="right_account_user_1">

            <div class="right_account_user2 mgrb_20">
                <div><a class="w3-left w3-btn" href="<?php echo site_url('lich-su-don-hang'); ?>">❮ Quay lại</a></div>

                <p class="title_tt_2">Thông tin thành viên</p>
                <span class="span_tt_1">
                      <p>Email :</p>
                      <b><?php echo $user->email ?></b>
                  </span>
                <?php if(!empty($user->full_name)): ?>
                <span class="span_tt_1">
                      <p>Họ tên :</p>
                      <b><?php echo $user->full_name ?></b>
                  </span>
                <?php endif; ?>
                <?php if(!empty($user->phone)): ?>
                <span class="span_tt_1">
                      <p>Điện thoại :</p>
                      <b><?php echo $user->phone ?></b>
                </span>
                <?php endif; ?>
                <?php if(!empty($user->address)): ?>
                <span class="span_tt_1">
                    <p>Địa chỉ :</p>
                    <b><?php echo $user->address ?></b>
                </span>
                <?php endif; ?>
                <?php if(!empty($district_name)): ?>
                <span class="span_tt_1">
                    <p>Quận huyện :</p>
                    <b><?php echo $district_name ?></b>
                </span>
                <?php endif; ?>
                <?php if(!empty($province_name)): ?>
                <span class="span_tt_1">
                    <p>Tỉnh thành :</p>
                    <b><?php echo $province_name ?></b>
                </span>
                <?php endif; ?>
                <span class="span_tt_1">
                      <p>Điểm tích lũy :</p>
                      <b><?php echo $user->score ?></b>
                  </span>
            </div>

            <div class="right_account_user2 mgrb_20">
                <p class="title_tt_2">Thông tin đơn hàng</p>
                <span class="span_tt_1">
                      <p>Mã đơn hàng :</p>
                      <b><?php echo $order_detail->code ?></b>
                  </span>
                <span class="span_tt_1">
                      <p>Cộng điểm tích lũy :</p>
                      <b><?php echo $order_detail->score ?></b>
                </span>
                <span class="span_tt_1">
                      <p>Thời gian đặt hàng :</p>
                      <b><?php echo date('H:i d-m-Y', strtotime($order_detail->created_at)); ?></b>
                </span>
                <span class="span_tt_1">
                      <p>Địa chỉ giao hàng :</p>
                      <b><?php echo $order_detail->address; ?></b>
                      <?php if(! empty($district_order_name)){?>
                      <b>- <?php echo $district_order_name; ?></b>
                      <?php } ?>
                      <?php if(! empty($province_order_name)){?>
                      <b>- <?php echo $province_order_name; ?></b>
                      <?php } ?>
                </span>
                <span class="span_tt_1">
                    <p>Tổng tiền :</p>
                    <b><?php echo number_format($order_detail->total, 0, ',', '.') . 'đ'; ?></b>
                </span>
                <span class="span_tt_1">
                    <p>Tình trạng :</p>
                    <b><?php echo $order_detail->order_status; ?></b>
                </span>
            </div>
            
            <?php if(!empty($list_product)): ?>
            <div class="right_account_user3 mgrb_20">

                <p class="title_tt_3 title_tt_3_mb">Danh sách sản phẩm</p>
                <table class="tablle_donhang_history history_order_detail_tpl">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Sản phẩm</th>
                            <th>Hình ảnh</th>
                            <th>Số lượng</th>
                            <th>Đơn giá</th>
                            <th>Thành tiền</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 

                        $i = 0;
                        foreach($list_product as $row){
                            $name = $row->product;
                            $images = $row->images;
                            $link = $row->link;
                            $qty = $row->qty;
                            $price = number_format($row->price, 0, ',', '.') . 'đ';
                            $subtotal = number_format($row->subtotal, 0, ',', '.') . 'đ';
                        ?>
                        <tr>
                            <td><?php echo $i + 1; ?></td>
                            <td><a href="<?php echo $link; ?>" target="_blank" class="text_cart_history"><?php echo $name; ?></a></td>
                            <td><a href="<?php echo $link; ?>" target="_blank" class="text_cart_history"><img src="<?php echo $images; ?>" alt="<?php echo $name; ?>" style="width: 80px"></a></td>
                            <td>
                                <p class="text_cart_history"><?php echo $qty; ?></p>
                            </td>
                            <td>
                                <p class="text_cart_history"><?php echo $price; ?></p>
                            </td>
                            <td>
                                <p class="text_cart_history"><?php echo $subtotal; ?></p>
                            </td>
                        </tr>
                      <?php $i++;} ?>
                    </tbody>
                </table>

                <div class="history_order_detail_mobile">
                    <?php foreach($list_product as $row){
                            $name = $row->product;
                            $images = $row->images;
                            $link = $row->link;
                            $qty = $row->qty;
                            $price = number_format($row->price, 0, ',', '.') . 'đ';
                            $subtotal = number_format($row->subtotal, 0, ',', '.') . 'đ'; ?>
                    <div class="child_cart_detail">
                        <a href="<?php echo $link; ?>" class="img_a_cart" target="_blank"><img src="<?php echo $images; ?>"></a>
                        <span class="name_cart_span1">
                            <p><a href="<?php echo $link; ?>" target="_blank"><?php echo $name; ?></a></p>
                            <b><?php echo $subtotal; ?></b>
                        </span>
                        <span class="name_cart_span2">
                            <p>SL</p>
                            <b><?php echo $qty; ?></b>
                        </span>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php $this->load->view(BLOCK . 'policy'); ?>