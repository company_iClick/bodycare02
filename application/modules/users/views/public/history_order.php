<div class="wrapper_full">
    <div class="wrapper_full_1">
        <div class="left_account_user">
            <div class="title_loc_search">Tài khoản</div>
            <ul>
                <li><a href="<?php echo site_url('thong-tin-tai-khoan'); ?>">Thông tin tài khoản</a></li>
                <li><a href="<?php echo site_url('doi-mat-khau'); ?>">Đổi mật khẩu</a></li>
                <li><a href="<?php echo site_url('dia-chi-giao-hang'); ?>">Địa chỉ giao hàng</a></li>
                <li><a href="<?php echo site_url('lich-su-don-hang'); ?>" class="active_menu_a">Lịch sử đơn hàng</a></li>
            </ul>
        </div>

        <div class="right_account_user_1">

            <div class="right_account_user2">

                <p class="title_tt_2">Thông tin thành viên</p>
                <span class="span_tt_1">
                      <p>Email :</p>
                      <b><?php echo $user->email ?></b>
                  </span>
                <?php if(!empty($user->full_name)): ?>
                <span class="span_tt_1">
                      <p>Họ tên :</p>
                      <b><?php echo $user->full_name ?></b>
                  </span>
                <?php endif; ?>
                <?php if(!empty($user->phone)): ?>
                <span class="span_tt_1">
                      <p>Điện thoại :</p>
                      <b><?php echo $user->phone ?></b>
                </span>
                <?php endif; ?>
                <?php if(!empty($user->address)): ?>
                <span class="span_tt_1">
                    <p>Địa chỉ :</p>
                    <b><?php echo $user->address ?></b>
                </span>
                <?php endif; ?>
                <?php if(!empty($district_name)): ?>
                <span class="span_tt_1">
                    <p>Quận huyện :</p>
                    <b><?php echo $district_name ?></b>
                </span>
                <?php endif; ?>
                <?php if(!empty($province_name)): ?>
                <span class="span_tt_1">
                    <p>Tỉnh thành :</p>
                    <b><?php echo $province_name ?></b>
                </span>
                <?php endif; ?>
                <span class="span_tt_1">
                      <p>Điểm tích lũy :</p>
                      <b><?php echo $user->score ?></b>
              </span>
            </div>

    
            <div class="<?php if(empty($list_order)) echo 'right_account_user2 mgrt_20';else echo 'right_account_user3' ?>">

                <?php if(!empty($list_order)){ ?>
                <p class="title_tt_3">Danh sách đơn hàng</p>
                <table class="tablle_donhang_history history_order_tpl">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mã đơn hàng</th>
                            <th>Tổng tiền</th>
                            <th>Thời gian</th>
                            <th>Cộng điểm tích lũy</th>
                            <th>Trạng thái</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                          $i = 0;
                          foreach($list_order as $row){
                            $code = $row->code;
                            $total = number_format($row->total, 0, ',', '.') . 'đ';
                            $created_at = date('H:i - d/m/Y', strtotime($row->created_at));
                            $score = ($row->score > 0) ?  '+' . $row->score : $row->score;
                            $order_status = $row->order_status;
                            $link = site_url('chi-tiet-don-hang/' . $row->id);
                            echo sprintf('<tr>
                                            <td>%s</td>
                                            <td>
                                                <p class="code_cart">%s</p>
                                            </td>
                                            <td>
                                                <p class="text_cart_history">%s</p>
                                            </td>
                                            <td>
                                                <p class="text_cart_history">%s</p>
                                            </td>
                                            <td>
                                                <p class="text_cart_history">%s</p>
                                            </td>
                                            <td>
                                                <p class="text_cart_history">%s</p>
                                            </td>
                                            <td><a href="%s" class="view_cart">Xem</a></td>
                                        </tr>', $i + 1, $code, $total, $created_at, $score, $order_status, $link);
                            $i++;}
                          ?>
                    </tbody>
                </table>
                <?php }else{?>
                <div class="txt_success">Bạn chưa có đơn hàng nào.</div>
                <?php } ?>
            </div>
            
            <?php if(!empty($list_gift)){ ?>
            <div class="right_account_user3">
                <p class="title_tt_3">danh sách Quà tặng</p>
                <table class="tablle_donhang_history history_gift_tpl">
                    <tbody>
                        <tr>
                            <td>STT</td>
                            <td>Tên sản phẩm</td>
                            <td>Số lượng</td>
                            <td>Thời gian</td>
                            <td>Trừ điểm tích lũy</td>
                        </tr>
                        <?php 
                            $i = 0;
                            foreach($list_gift as $row){ 
                                $name = $row->name;
                                $qty = $row->qty;
                                $created_at = date('H:i - d/m/Y', strtotime($row->created_at));
                                $score = ($row->score > 0) ?  '-' . $row->score : $row->score;
                            ?>
                        <tr>
                            <td><?php echo $i + 1; ?></td>
                            <td><a href="" class="text_cart_history"><?php echo $name; ?></a></td>
                            <td><a href="" class="text_cart_history"><?php echo $qty; ?></a></td>
                            <td>
                                <p class="text_cart_history"><?php echo $created_at; ?></p>
                            </td>
                            <td>
                                <p class="text_cart_history"><?php echo $score; ?></p>
                            </td>
                        </tr>
                        <?php $i++;} ?>
                    </tbody>
                </table>
            </div>
            <?php } ?>


            <div class="list_commnet_history">
                <div class="title_send_comment">Gửi phản hồi</div>
                <div class="list_commnet">
                    <form id="form_send_comment">
                        <textarea name="comment_content" class="" placeholder="Viết bình luận"></textarea>
                        <div id="errorCommentContent"></div>
                        <div id="mgs_comment" class="mgs_comment"></div>
                        <input type="button" class="submit_send_comment" value="Gửi" onclick="send_feedback()">
                    </form>
                    
                    <div id="list_feedback" class="clearfix">
                        <?php if(!empty($array_comment)): ?>
                        <div class="list_commnet1">
                            <?php foreach($array_comment as $row){
                                $parent = $row['parent'];
                                $child = (isset($row['child'])) ? array_reverse($row['child']) : '';
                                $comment_id = $row['parent']['id'];
                                $avatar = ($this->session->userdata('user')->id == $row['parent']['member_id']) ? base_url('themes/images/img_ac.png') : base_url('themes/images/thumb_admin.png');
                            ?>
                            <div class="child_comment1">
                                <div class="avatar_comment"><img src="<?php echo $avatar; ?>"></div>
                                <span class="name_faq">
                                    <b><?php echo $parent['name']; ?></b>
                                    <p><?php echo $parent['content']; ?> <b class="click_comment_b" onclick="toogle_reaply(<?php echo $comment_id; ?>)"><i class="icon-traloi"></i>&nbsp; Trả lời </b> </p>
                                     <form class="form_send_comment1 css_none" id="form_send_comment_<?php echo $comment_id; ?>">
                                        <div class="form_send_comment_box">
                                            <textarea name="comment_content" id="comment_content_<?php echo $comment_id; ?>" placeholder="Viết bình luận"></textarea>
                                            <div id="errorCommentContent<?php echo $comment_id; ?>"></div>
                                        </div>
                                        <div class="form_send_comment_box">
                                            <input type="button" value="Gửi" class="submit_send_comment" onclick="answer_feedback(<?php echo $comment_id; ?>)">
                                        </div>
                                     </form>
                                </span>
                                
                                <?php if(!empty($child)): 
                                    foreach ($child as $rc) :
                                        $name = $rc['name'];
                                        $content = $rc['content'];  
                                        $avatar = ($this->session->userdata('user')->id == $rc['member_id']) ? base_url('themes/images/img_ac.png') : base_url('themes/images/thumb_admin.png');
                                ?>

                                <div class="child_comment1_1">
                                    <i class="arrow_comment1"></i>
                                    <div class="avatar_comment"><img src="<?php echo $avatar; ?>"></div>
                                    <span class="name_faq">
                                        <b><?php echo $name; ?></b>
                                        <p><?php echo $content; ?></p>
                                    </span>
                                </div>
                                <?php endforeach;endif; ?>
                            </div>
                            <?php } ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            


        </div>

    </div>
</div>

<?php $this->load->view(BLOCK . 'policy'); ?>