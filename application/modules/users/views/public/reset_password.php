<div class="wrapper_full">
    <div class="wrapper_full_1 wrapper_full_5">
        <div class="register_user clearfix">
            <form class="register_form_user" method="post">
                <div class="title_user clearfix">
                    <h1>Bodycare.vn</h1>
                    <p>Thay đổi mật khẩu</p>
                </div>
            
                <div class="wp_input_user_login">
                    <?php if(!empty($message)): ?>
                    <div id="message_change_pass"><div class="txt_success"><?php echo $message; ?></div></div>
                    <?php endif; ?>
                    <input type="password" name="password" class="input_user" placeholder="Mật khẩu mới">
                    <?php echo form_error('password'); ?>
                    <input type="password" name="re_password" class="input_user" placeholder="Xác nhận mật khẩu mới">
                </div>
                <div class="wp_input_user2">
                    <input type="submit" class="submit_update" value="Gửi">
                </div>
            </form>
        </div>
    </div>
</div>
