<div class="wrapper_full">
    <div class="wrapper_full_1">
        <div class="register_user clearfix">
            <form class="register_form_user" method="post" action="">
                <div class="title_user clearfix">
                    <h1>bodycare.vn</h1>
                    <p>Đăng ký và mua sắm</p>
                </div>
                <div class="wp_input_user">
                    <input type="email" name="email" value="<?php echo $email; ?>" class="input_user" placeholder="Email" required>
                    <?php echo form_error('email'); ?>
                    <input type="password" name="password" class="input_user" placeholder="Mật khẩu">
                     <?php echo form_error('password'); ?>
                    <input type="password" name="re_password" class="input_user" placeholder="Xác nhận mật khẩu" required>
                    <input type="text" name="full_name" value="<?php echo $full_name; ?>" class="input_user" placeholder="Họ & tên">
                    <input type="text" name="address" value="<?php echo $address; ?>" class="input_user" placeholder="Địa chỉ">
                    <input type="text" name="phone" value="<?php echo $phone; ?>" class="input_user" placeholder="Số điện thoại" pattern="0([0-9]{9,10})">
                    <?php echo form_error('phone'); ?>
                </div>
                <div class="wp_input_user1 clearfix">
                    <div class="btn_register">
                        <i class="icon-dangky"></i>
                        <span>Đăng ký</span>
                        <input type="submit" value="">
                    </div>
                    <div class="btn_login btn_login_p">
                        <a href="<?php echo site_url('dang-nhap'); ?>">
                            <span>Đăng nhập</span>
                            <i class="icon-dangnhap"></i>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>