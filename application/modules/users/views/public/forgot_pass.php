<div class="clear h1"></div>
<div class="col_full fleft box-content">
    <div class="col-left-home fleft">
        <div class="clear h1"></div>
        <form action="" method="post">
            <div class="relative col_full fleft">
                <h1 class="title font-roboto text-primary relative ">
                    <span class="text fleft bold">
                        <?php echo "Lấy lại mật khẩu" ?>
                    </span>
                    <span class="line col_full m_auto absolute"></span>

                    <div class="pull-right">
                        <p class="fleft hotline"> ĐT hỗ trợ: <?php echo $info->phone?></p>
                    </div>
                </h1>

                <div class="col_full fleft">
                    <a  class="icon-guide fleft col_full gray" target="_blank">
                        Mời bạn nhập địa chỉ email đã đăng ký trên tài khoản Tuyendungvieclam.vn
                    </a>
                    <a  class="icon-guide fleft col_full gray" target="_blank">
                        Tuyển dụng việc làm sẽ gửi tới bạn hướng dẫn để tạo mật khẩu mới, vui lòng kiểm tra email.
                    </a>
                    <div class="clear h1"></div>
                    <div class="group-field">
                        <label class="label-form">
                            Email
                        </label>
                        <input type="text" id="email" name="email" class="text" value="<?php echo set_value('email'); ?>">
                        <div class="col_full red bold"><?php echo form_error("email") ?></div>
                    </div>
                    <div class="group-field">
                        <input  class="btn-submit col_full fleft " id="btn-submit-register" type="submit" name="ok" value="Gửi">
                        <div class="clear h1"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php echo modules::run('home/home/block_right_employer', array("type" => "users")); ?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        PageSearch();
    })
</script>