<div class="wrapper_full">
    <div class="wrapper_full_1">

        <div class="left_account_user">

            <div class="title_loc_search">tài khoản</div>
            <ul>
                <li><a href="<?php echo site_url('thong-tin-tai-khoan'); ?>">Thông tin tài khoản</a></li>
                <li><a href="<?php echo site_url('doi-mat-khau'); ?>">Đổi mật khẩu</a></li>
                <li><a href="<?php echo site_url('dia-chi-giao-hang'); ?>" class="active_menu_a">Địa chỉ giao hàng</a></li>
                <li><a href="<?php echo site_url('lich-su-don-hang'); ?>">Lịch sử đơn hàng</a></li>
            </ul>
        </div>

        <div class="right_account_user">

            <div class="right_account_user1">
                <?php if(!empty($message)): ?>
                <div id="message_change_pass"><div class="txt_success"><?php echo $message; ?></div></div>
                <?php endif; ?>
                <h1 class="h1_2">Địa chỉ giao hàng</h1>
                <form method="post" action="">
                    <div class="update_div_input">                        
                        <div class="wp_distric" style="float: left;">
                            <div>
                                <select name="province_id" onchange="load_district()">
                                  <option value="">Chon tỉnh / thành</option>
                                  <?php if(!empty($province)){
                                    foreach ($province as $row) {
                                        $id = $row->id;
                                        $name = $row->name;
                                        $selected = ($province_id == $id) ? ' selected' :  '';
                                        echo '<option value="'. $id .'"'. $selected .'>'. $name .'</option>';
                                    }}?>
                                </select>
                                <?php echo form_error('province_id'); ?>
                            </div>
                            
                            <div>
                                <select name="district_id">
                                  <option value="">Chon quận huyện</option>
                                  <?php if(!empty($district)){
                                    foreach ($district as $row) {
                                        $id = $row->id;
                                        $name = $row->name;
                                        $selected = ($district_id == $id) ? ' selected' :  '';
                                        echo '<option value="'. $id .'"'. $selected .'>'. $name .'</option>';
                                    }}?>
                                </select>
                                <?php echo form_error('district_id'); ?>
                             </div>
                        </div>
                    </div>

                    <div class="update_div_input">
                        <input type="submit" class="submit_update" name="" value="Cập nhập">
                    </div> 

                </form>
            </div>

        </div>


    </div>
</div>