<style>
    .t-info{ float: left; width: 100%}
    .t-info td{ border: solid 1px #ccc; border-collapse: collapse}
</style>
<?php
$info = $this->general->get_tableWhere(array("user_id"=>$id), "users_employer");
$location = $this->general->get_tableWhere(array("id"=>isset($info->location_id)?$info->location_id:0), "location");
?>
<div class="contentcontainer">
    <div class="headings altheading"><h2>Thông tin user</h2></div>
    <div class="contentbox">
        <table  class="t-info">
            <tr><td colspan="2" style="text-align: center; font-weight: bold">THÔNG TIN THÀNH VIÊN</td></tr>
            <tr>
                <td width="20%">Họ & Tên: </td>
                <td><?php echo $user->full_name ?></td>
            </tr>
            <tr>
                <td>Email: </td>
                <td><?php echo $user->email ?></td>
            </tr>
            <tr><td colspan="2" style="text-align: center; font-weight: bold">THÔNG TIN CÔNG TY</td></tr>
            <tr>
                <td>Tên công ty: </td>
                <td><?php echo $info->company ?></td>
            </tr>
            <tr>
                <td>Địa chỉ: </td>
                <td><?php echo $info->address ?></td>
            </tr>
            <tr>
                <td>Sơ lượt: </td>
                <td><?php echo $info->summary ?></td>
            </tr>
            <tr>
                <td>Tỉnh/Tp: </td>
                <td><?php echo $location->name ?></td>
            </tr>
            <tr>
                <td>Website: </td>
                <td><?php echo $info->web ?></td>
            </tr>
            <tr>
                <td>Logo: </td>
                <td>
                    <?php
                    $logo=base_url()."uploads/logo_employer/".$info->logo;
                    if (file_exists($logo)) {?>
                    <img src="<?php echo base_url()?>uploads/logo_employer/<?php echo $info->logo; ?>" width="105px" />
                    <?php }?>
                </td>
            </tr>
            <tr><td colspan="2" style="text-align: center; font-weight: bold">THÔNG TIN NGƯỜI LIÊN HỆ</td></tr>
            <tr>
                <td>Tên người liên hệ: </td>
                <td><?php echo $info->user_contact ?></td>
            </tr>
            <tr>
                <td>Chức vụ: </td>
                <td><?php echo $info->office ?></td>
            </tr>
            <tr>
                <td>Địa chỉ: </td>
                <td><?php echo $info->address_contact ?></td>
            </tr>
            <tr>
                <td>Điện thoại: </td>
                <td><?php echo $info->phone_contact ?></td>
            </tr>
        </table>
        <div style="clear: both; height:10px;"></div>
        <a href="admin/users/index/<?php echo $type."/".$page_no?>" class="btn" style="color: #fff; text-decoration: none">Quay lại</a>
    </div>


</div>
