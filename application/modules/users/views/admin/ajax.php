<form method="post" action=""   enctype="multipart/form-data">

    <div class="extrabottom">

        <ul>

            <li> <img src="theme_admin/img/icons/icon_approve.png" alt="Approve" />

                <input class="a_button_act a_show" name="show" style="cursor: pointer" type="submit"    value="Hiện danh sách đã chọn" />

            </li>

            <li> <img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" />

                <input class="a_button_act a_hide" name="hide" style="cursor: pointer" type="submit"    value="Ẩn danh sách đã chọn" />

            </li>

            <li> <img src="theme_admin/img/icons/icon_delete.png" alt="Delete" />

                <p style="display:none">

                    <input class="a_button_act a_delete" name="delete" style="cursor: pointer" type="submit"    value="Delete" />

                </p>

                <a style=" cursor:pointer" class="delete" onclick="Delete()">Delete</a>

            </li>

        </ul>



        <div style="clear: both; height: 10px"></div>

    <table width="100%">

        <thead>

            <tr>

                <th width="5%">ID</th>

                <th>Họ tên</th>
                <th>Email</th>
                <th>Oauth</th>
                <th>Điểm tích lũy</th>
                <th>Thời gian đăng ký</th>
                <th>VIP</th>
                <th width="9%">Action</th>

                <th><input name="" type="checkbox" value="" id="checkboxall" /></th>

            </tr>

        </thead>

        <div style="clear: both; height: 10px"></div>

        <tbody id="load">

            <?php

            $x = 0;

            foreach ($list as $i) {

                ?>

                <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?> id="app-<?php echo $i->id?>">

                    <td style="text-align:center"><?php echo $x + 1 ?></td>

                    <td>

                        <a data="" title="Permition" href="admin/users/edit/<?php echo $i->id."/1" ?>"  style="cursor: pointer">

                            <b><?php echo $i->full_name ?></b>

                        </a>

                    </td>

                    <td><?php echo $i->email?></td>
                    <td><?php echo $i->oauth?></td>
                    <td><?php echo $i->score?></td>
                    <td><?php echo date('H:i d/m/Y', strtotime($i->created_at));?></td>
                    <td style="text-align:center">
                        <?php if ($i->vip == 1) { ?>
                            <a href="javascript:void(0)" onclick="set_value('vip', 'users', <?php echo $i->id; ?>, 1, 0)" title="Hiện"><img
                                    src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                        <?php } else { ?>
                            <i href="javascript:void(0)" onclick="set_value('vip', 'users', <?php echo $i->id; ?>, 1, 1)" title="Ẩn"><img
                                    src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                        <?php } ?>
                    </td>
                    <td style="text-align:center">
                        <a href="admin/users/edit/<?php echo $i->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit"/></a>

                        <?php if ($i->status == 1) { ?>
                            <a href="javascript:void(0)" onclick="set_value('status', 'users', <?php echo $i->id; ?>, 1, 0)" title="Hiện"><img
                                    src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                        <?php } else { ?>
                            <i href="javascript:void(0)" onclick="set_value('status', 'users', <?php echo $i->id; ?>, 1, 1)" title="Ẩn"><img
                                    src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>
                        <?php } ?>

                         <a onclick="if (!confirm('Xác nhận xóa?'))return false;" href="admin/users/delete/<?php echo $i->id ?>/1" title="Xóa"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete"/></a>
                    </td>
                    <td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $i->id ?>]" name="checkall[<?php echo $i->id ?>]" class="checkall"  /></td>

                </tr>            

                <?php $x++;  }?>



        </tbody>

    </table>

    <div style="clear: both;"></div>

    <p style="display:none">

        <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>

    </p>

</form>