<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
?>
<style>
    .t-info{ float: left; width: 46%;padding: 0 2%;}
    .t-info td{ border: solid 1px #ccc; border-collapse: collapse}
</style>

<div class="contentcontainer">
    <div class="headings altheading"><h2>Thêm mới thành viên</h2></div>
    <div class="contentbox">
        <form name="them" method="post" id="them" action="" enctype="multipart/form-data">

            <div class="t-info">
            	<p class="div_lang" >
                    <label for="smallbox"><strong>Email: </strong></label>
                    <input name="email" type="text" id="textfield"	class="inputbox" value="<?php echo $this->input->post('email') ?>" >
                    <?php echo form_error('email'); ?>
                </p>
            </div>

            <div class="t-info">
                <p class="div_lang" >
                    <label for="smallbox"><strong>Mật khẩu: (Không nhập mặc định là "bodycare")</strong></label>
                    <input name="password" type="text" id="textfield"  class="inputbox" value="<?php echo $this->input->post('password') ?>" >
                </p>
            </div>

            <div class="t-info">
                <p class="div_lang" >
                    <label for="smallbox"><strong>Họ tên: </strong></label>
                    <input name="full_name" type="text" id="textfield"	class="inputbox" value="<?php echo $this->input->post('full_name') ?>" >
                </p>
            </div>

            <div class="t-info">
                <p class="div_lang" >
                    <label for="smallbox"><strong>Điện thoại: <span style="color: red">*</span></strong></label>
                    <input name="phone" type="text" id="textfield"	class="inputbox" value="<?php echo $this->input->post('phone') ?>" >
                </p>
            </div>

            <div class="t-info">
                <p class="div_lang" >
                    <label for="smallbox"><strong>Địa chỉ: </strong></label>
                    <input name="address" type="text" id="textfield" class="inputbox" value="<?php echo $this->input->post('address') ?>" >
                </p>
            </div>

            <div class="t-info">
                <p class="div_lang" >
                    <label for="smallbox"><strong>Điểm tích lũy: </strong></label>
                    <input name="score" type="text" id="textfield" class="inputbox" value="<?php echo $this->input->post('score') ?>" >
                </p>
            </div>

            <div class="t-info">
                <p class="div_lang" >
                    <label for="smallbox"><strong>Ghi chú: </strong></label>
                    <textarea class="inputbox" name="note" id="note" rows="5"><?php echo $this->input->post('note') ?></textarea>
                </p>
            </div>
            <div style="clear: both"></div>
            <div class="t-info">
                <p>
                    <label for="smallbox"><strong>Loại thành viên: </strong></label>
                    <input type="radio" value="1" name="vip">Vip
                    <input type="radio" value="0" name="vip">Bình thường
                </p>
                <p>
                    <label for="smallbox"><strong>Thứ Tự: </strong></label>
                    <input type="text" name="weight"  value="1" style="width: 30px; text-align: center" class="inputbox">
                </p>
                
                <p>
                    <input type="radio" value="1" name="status" checked>Hiển thị
                    <input type="radio" value="0" name="status">Ẩn
                </p>
            </div>
            <div style="clear: both"></div>
            <div class="t-info">
                <input type="submit" value="Thêm mới" name="ok" class="btn" />
            </div>
        </form>
    </div><!-- end contentbox -->
</div>