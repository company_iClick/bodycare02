<?php



class A_user extends CI_Model {



    function __construct() {

        parent::__construct();

        $this->load->database();

    }



    function get_users_where($where = array(),$table,$select,$p=0) {

        $this->db->select($select);

        $this->db->where($where);

        $this->db->order_by("users.id","DESC");

        $this->db->group_by("users.id");

        $this->db->from("users");

        if($p>0) $this->db->limit($p);

        $this->db->join($table,$table.".user_id=users.id");

        return $this->db->get()->result();

    }

    function get_users_whereID($where = array(),$table,$select) {

        $this->db->select(trim($select));

        $this->db->where($where);

        $this->db->order_by("users.id");

        $this->db->group_by("users.id");

        $this->db->from("users");

        $this->db->join($table,$table.".user_id=users.id");

        return $this->db->get()->row();

    }

    function list_users_where($where = array()) {

        $this->db->select("users.user_name,users.id");

        $this->db->where($where);

        $this->db->from("users");

        $this->db->order_by("users.id","DESC");

        $this->db->limit(10);

        return $this->db->get()->result();

    }



    function get_avatar($id) {



        $this->db->select('avatar');

        $this->db->where('status', 1);

        $this->db->where('id', $id);

        $this->db->from("users");

        return $this->db->get()->row();

    }







    function get_user($id) {

        $this->db->select('users.id,users.views');

        $this->db->where('users.status', 1);

        $this->db->where('users.id', $id);

        $this->db->from('users');

        return $this->db->get()->row();

    }

    function get_user_where($id) {

        $this->db->select('*');

        $this->db->where('users.status', 1);

        $this->db->where('users.id', $id);

        $this->db->from('users');

        return $this->db->get()->row();

    }





	/////////////////////////////////////////////////////

	function check_login_gmail($fid)

	{

		$this->db->select('users.user_name');

		$this->db->where("users.fid", $fid);

		$this->db->where("users.flag_login", 2);

		return $this->db->get('users')->num_rows();

	}



}

