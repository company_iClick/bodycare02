<?php

class M_newsletter extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // show list newsletter
    function show_list_newsletter() {
        $this->db->select("*");
        $this->db->order_by('newsletter.weight', "ASC");
		$this->db->order_by('newsletter.id', "DESC");
        $this->db->from('newsletter');
        return $this->db->get()->result();
    }
    // show list newsletter where
    function show_list_newsletter_where($where=array()) {
        $this->db->select("*");
        $this->db->where($where);
        $this->db->order_by('newsletter.weight', "ASC");
		$this->db->order_by('newsletter.id', "DESC");
        $this->db->from('newsletter');
        return $this->db->get()->result();
    }
    
}
