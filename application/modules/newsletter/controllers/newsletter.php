<?php

class Newsletter extends MX_Controller {

    protected $_table = 'newsletter';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    function index(){
        $email = $this->input->post('email');
        $count = $this->global_function->count_tableWhere(array('email' => $email, 'status' => 1), $this->_table);
        if($count > 0){
            echo '<div class="show_error_white">Email đã được đăng ký</div>';
            return false;
        }

        if($this->db->insert($this->_table, array('email' => $email, 'status' => 1, 'weight' => 1))){
            echo '<div class="show_error_white" style="font-style: normal;">Bạn đắ đăng ký thành công</div>';
        }
    }
}
?>