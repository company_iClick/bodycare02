<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager'), 'id' => $this->input->get('id'), 'type' => $this->input->get('type')));
?>

<!-- Alternative Content Box Start -->

<div class="contentcontainer">
    <div class="headings altheading"><h2>Danh sách</h2></div>
    <div class="contentbox">
        <form method="post" hair_type=""	enctype="multipart/form-data">
            <div class="extrabottom">
                <ul>
                    <li>
                        <img src="theme_admin/img/icons/add.png" alt="Add" /> 
                        <a style="text-decoration: none;" href="admin/hair_type/add">Thêm mới</a>
                    </li>

                    <li> <img src="theme_admin/img/icons/icon_approve.png" alt="Approve" />
                        <input class="a_button_act a_show" name="show" style="cursor: pointer" type="submit"	value="Hiện danh sách đã chọn" />
                    </li>
                    <li> <img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" />

                        <input class="a_button_act a_hide" name="hide" style="cursor: pointer" type="submit"	value="Ẩn danh sách đã chọn" />

                    </li>

                    <li> <img src="theme_admin/img/icons/icon_delete.png" alt="Delete" />

                        <p style="display:none">

                            <input class="a_button_act a_delete" name="delete" style="cursor: pointer" type="submit"	value="Delete" />

                        </p>

                        <a style=" cursor:pointer" class="delete" onclick="Delete()">Delete</a> 

                    </li>

                </ul>            

            </div>

            <table width="100%">
                <thead>
                    <tr>
                        <th width="5%">STT</th>
                        <th width="30%">Tên</th>
                        <th>Thứ tự</th>
                        <th>Action</th>
                        <th><input name="" type="checkbox" value="" id="checkboxall" /></th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    $x = 0;
                    ?>

                    <?php if (!empty($item)) {
                        foreach ($item as $a) {
                            ?>

                            <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                                <td style="text-align:center"><?php echo $x + 1; ?></td>
                                <td><b><?php echo $a->name ?></b></td>
                                <td style="text-align:center"><?php echo $a->weight ?></td>
                                <td style="text-align:center">
                                    <a href="admin/hair_type/edit/<?php echo $a->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit"/></a>

                                    <?php if ($a->status == 1) { ?>

                                        <a href="admin/hair_type/hide/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Hiện"><img src="theme_admin/img/icons/icon_approve.png" alt="Approve"/></a>
                                    <?php } else { ?>

                                        <a href="admin/hair_type/show/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Ẩn"><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove"/></a>

                                    <?php } ?>

                                    <a onclick="if (!confirm('Xác nhận xóa')) return false;" href="admin/hair_type/delete/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Xóa"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete"  /></a>
                                </td>

                                <td style="text-align:center">
                                    <input type="checkbox" value="check_item[<?php echo $a->id ?>]" name="checkall[<?php echo $a->id ?>]" class="checkall"/>

                                </td>
                            </tr>

                            <?php
                            $x++;
                        }
                    }
                    ?>

                </tbody>

            </table>

            <p style="color:#FF0000; font-weight:bold; text-align:center;margin-top: 5px;"><?php echo "Bạn chỉ được phép xóa các thư mục rỗng và không có dữ liệu" ?></p>

            <div style="clear: both;"></div>

            <p style="display:none">
                <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>
            </p>

        </form>
        <div style="clear: both"></div>
        <?php echo $link;  ?>
    </div>

</div>