<?php
class M_hair_type extends CI_Model
{
    protected $_table = 'hair_type';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_hair_type($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function get_hair_typeID($id)
    {
        $this->db->where('id', $id);
        $this->db->from($this->_table);
        return $this->db->get()->row();
    }    
    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }
    function count_product($hair_type_id)
    {
         $this->db->select('product.id');
         $this->db->where('tmp_product_hair_type.hair_type_id', $hair_type_id);
         $this->db->group_by('product.id');
         $this->db->from('product');
         $this->db->join('tmp_product_hair_type', 'tmp_product_hair_type.product_id=product.id');
         return $this->db->count_all_results();
    }
}

