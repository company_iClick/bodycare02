<?php

$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager')));
$name = (isset($item)) ? $item->name : $this->input->post('name');
$slug = (isset($item)) ? $item->slug : $this->input->post('slug');
$summary = (isset($item)) ? $item->summary : $this->input->post('summary');
$title = (isset($item)) ? $item->title : $this->input->post('title');
$keywords = (isset($item)) ? $item->keywords : $this->input->post('keywords');
$description = (isset($item)) ? $item->description : $this->input->post('description');
$weight = (isset($item)) ? $item->weight : 1;
$button = (isset($item)) ? 'Cập nhật' : 'Thêm mới';


?>
<form name="them" method="post" id="them" action="" enctype="multipart/form-data">
<div class="contentcontainer med left">
    <div class="headings altheading"><h2><?php echo $button; ?></h2></div>
    <div class="contentbox">
            <p>
                <label for="textfield"><strong>Tên</strong></label>
                <input type="text" class="inputbox" name="name" value="<?php echo $name; ?>" OnkeyUp="add_alias_vn(this)" />
            </p>
            <?php echo form_error('name'); ?>

            <p>
                <label for="textfield"><strong>Đường dẫn</strong></label>
                <input type="text" class="inputbox" name="slug" value="<?php echo $slug; ?>" />
            </p>
            <?php echo form_error('slug'); ?>

            <p>
                <label for="smallbox"><strong>Hình ảnh</strong></label>
                <?php if (isset($item) && file_exists('uploads/brand/'.$item->thumb)) {?>
                    <img src="uploads/brand/<?php echo $item->thumb; ?>" width="100">
                <?php }?>
                <div id="local">
                    <input id="uploader" type="file" name="photo">
                    <img alt="Loading" src="theme_admin/img/loading.gif">
                    Uploading...
                </div>

            </p>
            <?php echo form_error('photo'); ?>

            <p>
                <label for="textfield"><strong>Mô tả</strong></label>
                <textarea name="summary" class="textarea" rows="4"><?php echo $summary; ?></textarea>
            </p>

            <p>
                <label for="smallbox"><strong>Thứ Tự: </strong></label>
                <input type="text" name="weight"  value="<?php echo $weight; ?>" style="width: 30px; text-align: center" class="inputbox">
                <br>
            </p>

            <p>
                <input type="radio" value="1" name="status"  checked>Hiển thị
                <input type="radio" value="0" name="status" >Ẩn
            </p>

            <input type="submit" value="<?php echo $button; ?>" name="ok" class="btn" />
            <a class="btn" style="color: #fff" href="admin/brand">Thoát</a>
        
    </div><!-- end contentbox -->
</div>
<div class="contentcontainer sml right">

    <div class="headings altheading">
        <h2 class="left">Sơ đồ</h2>
    </div>
    <div class="contentbox">
        <p>
            <label for="textfield"><strong>Title</strong></label>
            <input type="text" class="inputbox" name="title" value="<?php echo $title; ?>" />
        </p>

        <p>
            <label for="textfield"><strong>Keywords</strong></label>
            <textarea name="keywords" class="textarea" style="width:340px;border-radius: 4px;padding: 5px;" rows="4"><?php echo $keywords; ?></textarea>
        </p>

        <p>
            <label for="textfield"><strong>Description</strong></label>
            <textarea name="description" class="textarea" style="width:340px;border-radius: 4px;padding: 5px;" rows="4"><?php echo $description; ?></textarea>
        </p>
    </div>
</div>
</form>


<script type="text/javascript">

    CKEDITOR.replace('summary', {height: 350, width: "98%", resize_enabled: true,
        filebrowserBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserImageBrowseUrl: '<?php echo base_url() ?>editor/find/ckfinder.html',
        filebrowserUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '<?php echo base_url() ?>editor/find/core/connector/php/connector.php?command=QuickUpload&type=Images',
    });

</script>