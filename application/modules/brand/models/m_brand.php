<?php

class M_brand extends CI_Model
{
    protected $_table = 'brand';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_brand($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from('brand');
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function show_list_tmp_brand_category_id($brand_id) {
        $this->db->where('brand_id', $brand_id);
        $this->db->from('tmp_brand_category');
        return $this->db->get()->result();
    }
    function get_brandID($id)
    {
        $this->db->where('brand.id', $id);
        $this->db->from('brand');
        return $this->db->get()->row();
    }    
    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }
    function check_slug($slug){
        return $this->db->where('slug', $slug)
                        ->where('id !=', $this->uri->segment(4))
                        ->where('status', 1)
                        ->from($this->_table)
                        ->count_all_results();
    }

    function show_list_brand_keyword($keyword)
    {
        $this->db->like('slug', $keyword);
        $this->db->order_by('brand.weight', "ASC");
        $this->db->order_by('brand.id', "DESC");
        $this->db->from('brand');
        return $this->db->get()->result();
        
    }
}

