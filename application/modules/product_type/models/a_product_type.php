<?php

class A_product_type extends CI_Model {

	protected $_table = 'product_type';
	protected $_tmp_ppt = 'tmp_product_product_type';
	protected $_tmp_pc = 'tmp_product_category';
    protected $_tmp_pht = 'tmp_product_hair_type';
    protected $_tmp_pst = 'tmp_product_skin_type';
    protected $_tmp_pa = 'tmp_product_action';
    protected $_tmp_pca = 'tmp_product_capacity';
    protected $_tmp_pw = 'tmp_product_weigh';
    protected $_tmp_ppn = 'tmp_product_pill_number';
    protected $_tmp_ptg = 'tmp_product_tag';
	protected $_pr = 'product';
    protected $_cate = 'category';

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function get_all_item($where = array()){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1));
        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id");
        $this->db->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->result();
    }

    function get_all_item_search($list_cate_id, $keyword, $where = array()){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product");
        $this->db->where($where);
        $this->db->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1));
        $this->db->like("$this->_pr.slug", $keyword);
        $this->db->order_by("$this->_table.weight", "ASC") ;
        $this->db->order_by("$this->_table.id", "DESC");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id");
        $this->db->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id");
        if(!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id=$this->_pr.id");
        $this->db->group_by("$this->_table.id");
        return $this->db->get()->result();
    }

    function count_all_item_category($list_id, $list_cate_id, $where = array()){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where($where)
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1, 'parent_id' => 0))
                ->where_in("$this->_tmp_ppt.product_type_id", $list_id)
                ->where_in("$this->_tmp_pc.category_id", $list_cate_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_pr.id")
                ->get()
                ->num_rows();
    }

    function get_product_type_in_category($list_cate_id){
    	return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
    			->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1, 'parent_id' => 0))
    			->where_in("$this->_tmp_pc.category_id", $list_cate_id)
    			->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_pr.id")
                ->get()
                ->result();
    }

    function get_product_type_in_detail($product_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug")
                ->where(array("$this->_table.status" => 1, 'parent_id' => 0))
                ->where_in("$this->_tmp_ppt.product_id", $product_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->get()
                ->result();
    }

    function count_product_pt($product_type_id, $where = array())
    {
        return $this->db->select("count($this->_pr.id)")->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->where($where)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id=$this->_pr.id")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function count_product($product_type_id, $list_cate_id)
    {
        return $this->db->select("count($this->_pr.id)")->where_in("$this->_tmp_pc.category_id", $list_cate_id)
                    ->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id=$this->_pr.id")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function count_product_search($product_type_id, $list_cate_id, $keyword, $where = array()){
        if(!empty($list_cate_id)) $this->db->select("count($this->_pr.id)")->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        $this->db->where($where);
        $this->db->where("$this->_tmp_ppt.product_type_id", $product_type_id);
        $this->db->where("$this->_pr.status", 1);
        $this->db->like("$this->_pr.slug", $keyword);
        $this->db->from("$this->_pr");
        if(!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id=$this->_pr.id");
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id");
        return $this->db->group_by("$this->_pr.id")->get()->num_rows();
    }

    function get_product_type_in_hair_type($hair_type_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1))
                ->where("$this->_tmp_pht.hair_type_id", $hair_type_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id")
                ->get()
                ->result();
    }

    function count_product_hair_type($product_type_id, $hair_type_id)
    {
        return $this->db->select("count($this->_pr.id)")->where("$this->_tmp_pht.hair_type_id", $hair_type_id)
                    ->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id")
                    ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function get_product_type_in_skin_type($skin_type_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1))
                ->where("$this->_tmp_pst.skin_type_id", $skin_type_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id")
                ->get()
                ->result();
    }

    function count_product_skin_type($product_type_id, $skin_type_id)
    {
        return $this->db->select("count($this->_pr.id)")->where("$this->_tmp_pst.skin_type_id", $skin_type_id)
                    ->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id")
                    ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function get_product_type_in_action($action_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1))
                ->where("$this->_tmp_pa.action_id", $action_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id")
                ->get()
                ->result();
    }

    function count_product_action($product_type_id, $action_id)
    {
        return $this->db->select("count($this->_pr.id)")->where("$this->_tmp_pa.action_id", $action_id)
                    ->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id")
                    ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function get_product_type_in_capacity($capacity_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1))
                ->where("$this->_tmp_pca.capacity_id", $capacity_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id")
                ->get()
                ->result();
    }

    function count_product_capacity($product_type_id, $capacity_id)
    {
        return $this->db->select("count($this->_pr.id)")->where("$this->_tmp_pca.capacity_id", $capacity_id)
                    ->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id")
                    ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function get_product_type_in_weigh($weigh_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1))
                ->where("$this->_tmp_pw.weigh_id", $weigh_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id")
                ->get()
                ->result();
    }

    function count_product_weigh($product_type_id, $weigh_id)
    {
        return $this->db->select("count($this->_pr.id)")->where("$this->_tmp_pw.weigh_id", $weigh_id)
                    ->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id")
                    ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function get_product_type_in_pill_number($pill_number_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1))
                ->where("$this->_tmp_ppn.pill_number_id", $pill_number_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id")
                ->get()
                ->result();
    }

    function count_product_pill_number($product_type_id, $pill_number_id)
    {
        return $this->db->select("count($this->_pr.id)")->where("$this->_tmp_ppn.pill_number_id", $pill_number_id)
                    ->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id")
                    ->group_by("$this->_pr.id")->get()->num_rows();
    }

    function get_product_type_in_origin($origin_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1))
                ->where("$this->_pr.origin_id", $origin_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->get()
                ->result();
    }

    function count_product_origin($product_type_id, $origin_id)
    {
        return $this->db->where("$this->_pr.origin_id", $origin_id)
                    ->where("$this->_tmp_ppt.product_type_id", $product_type_id)
                    ->where("$this->_pr.status", 1)
                    ->from("$this->_pr")
                    ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id=$this->_pr.id")
                    ->count_all_results();
    }

    function get_product_type_in_tags($tag_id){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1))
                ->where("$this->_tmp_ptg.tag_id", $tag_id)
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC") 
                ->group_by("$this->_table.id") 
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_pr.id")
                ->get()
                ->result();
    }

    function get_item_in_param_filter($param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $where = array(), $options = null, $option_tag = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product");
        $this->db->where($where);

        switch ($param_type) {

            case 'category':
                $this->db->where_in("$this->_tmp_pc.category_id", $param_id);
                break;
            case 'brand':
                $this->db->where_in("$this->_pr.brand_id", $param_id);
                break;

            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $param_id);
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $param_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $param_id);
                break;

            case 'origin':
                $this->db->where_in("$this->_pr.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $param_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $param_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $param_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $param_id);
                break;

            case 'tags':
                $this->db->where_in("$this->_tmp_ptg.tag_id", $param_id);
                break;
            
        }

        if(!empty($brand_value)) $this->db->where_in("$this->_pr.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_pr.origin_id", $origin_value);
        // if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to";  
                
                $i++;
            }
            $this->db->where("($price_where)");
        }

        $this->db->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }

        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC"); 
        $this->db->group_by("$this->_table.id");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id");
        $this->db->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id");

        if($option_tag != null){
            $this->db->where_in("$this->_tmp_ptg.tag_id", $option_tag);
            $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_pr.id");
        }

        switch ($param_type) {

            case 'category':
                $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_pr.id");
                break;
                
            /*case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_pr.id");
                break;*/

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_pr.id");
                break;
        }

        /*if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_pr.id");*/
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id");

        return $this->db->get()->result();
    }

    function get_item_in_param_filter_search($keyword, $list_cate_id, $param_type, $param_id, $brand_value, $origin_value, $product_type_value, $skin_type_value, $hair_type_value, $action_value, $capacity_value, $weigh_value, $pill_number_value, $price_value, $where = array(), $options = null){
        $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, count($this->_pr.id) as count_product");
        $this->db->where($where);

        $this->db->like("$this->_pr.slug", $keyword);

        switch ($param_type) {

            case 'category':
                $this->db->where_in("$this->_tmp_pc.category_id", $param_id);
                break;
            case 'brand':
                $this->db->where_in("$this->_pr.brand_id", $param_id);
                break;

            case 'product_type':
                $this->db->where_in("$this->_tmp_ppt.product_type_id", $param_id);
                break;

            case 'skin_type':
                $this->db->where_in("$this->_tmp_pst.skin_type_id", $param_id);
                break;

            case 'hair_type':
                $this->db->where_in("$this->_tmp_pht.hair_type_id", $param_id);
                break;

            case 'origin':
                $this->db->where_in("$this->_pr.origin_id", $param_id);
                break;

            case 'action':
                $this->db->where_in("$this->_tmp_pa.action_id", $param_id);
                break;

            case 'capacity':
                $this->db->where_in("$this->_tmp_pca.capacity_id", $param_id);
                break;

            case 'weigh':
                $this->db->where_in("$this->_tmp_pw.weigh_id", $param_id);
                break;

            case 'pill_number':
                $this->db->where_in("$this->_tmp_ppn.pill_number_id", $param_id);
                break;

            case 'tags':
                $this->db->where_in("$this->_tmp_ptg.tag_id", $param_id);
                break;
            
        }

        if(!empty($list_cate_id)) $this->db->where_in("$this->_tmp_pc.category_id", $list_cate_id);
        if(!empty($brand_value)) $this->db->where_in("$this->_pr.brand_id", $brand_value);
        if(!empty($origin_value)) $this->db->where_in("$this->_pr.origin_id", $origin_value);
        // if(!empty($product_type_value)) $this->db->where_in("$this->_tmp_ppt.product_type_id", $product_type_value);
        if(!empty($skin_type_value)) $this->db->where_in("$this->_tmp_pst.skin_type_id", $skin_type_value);
        if(!empty($hair_type_value)) $this->db->where_in("$this->_tmp_pht.hair_type_id", $hair_type_value);
        if(!empty($action_value)) $this->db->where_in("$this->_tmp_pa.action_id", $action_value);
        if(!empty($capacity_value)) $this->db->where_in("$this->_tmp_pca.capacity_id", $capacity_value);
        if(!empty($weigh_value)) $this->db->where_in("$this->_tmp_pw.weigh_id", $weigh_value);
        if(!empty($pill_number_value)) $this->db->where_in("$this->_tmp_ppn.pill_number_id", $pill_number_value);
        if(!empty($price_value)){
            $i = 0;
            foreach ($price_value as $row) {
                $p_from = $row->p_from;
                $p_to = ($row->p_to > 0) ? $row->p_to - 1 : $row->p_from * 100 ;
                
                if($i == 0 ){
                    $price_where = ($p_from == 0) ? "price < $p_to" : "price BETWEEN $p_from AND $p_to"; ; 
                }else $price_where .= " OR price BETWEEN $p_from AND $p_to";  
                
                $i++;
            }
            $this->db->where("($price_where)");
        }

        $this->db->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1));
        if($options != null){
            foreach ($options as $row) {
                $this->db->order_by($row['field'], $row['sort']);
            }
        }

        $this->db->order_by("$this->_table.weight", "ASC"); 
        $this->db->order_by("$this->_table.id", "DESC"); 
        $this->db->group_by("$this->_table.id");
        $this->db->from($this->_table);
        $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id");
        $this->db->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id");

        switch ($param_type) {

            case 'category':
                $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_pr.id");
                break;
                
            /*case 'product_type':
                $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_pr.id");
                break;*/

            case 'hair_type':
                $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id");
                break;

            case 'skin_type':
                $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id");
                break;

            case 'action':
                $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id");
                break;

            case 'capacity':
                $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id");
                break;

            case 'weigh':
                $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id");
                break;

            case 'pill_number':
                $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id");
                break;

            case 'tags':
                $this->db->join("$this->_tmp_ptg", "$this->_tmp_ptg.product_id = $this->_pr.id");
                break;
        }

        if(!empty($list_cate_id)) $this->db->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_pr.id");
        /*if(!empty($product_type_value)) $this->db->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_id = $this->_pr.id");*/
        if(!empty($skin_type_value)) $this->db->join("$this->_tmp_pst", "$this->_tmp_pst.product_id = $this->_pr.id");
        if(!empty($hair_type_value)) $this->db->join("$this->_tmp_pht", "$this->_tmp_pht.product_id = $this->_pr.id");
        if(!empty($action_value)) $this->db->join("$this->_tmp_pa", "$this->_tmp_pa.product_id = $this->_pr.id");
        if(!empty($capacity_value)) $this->db->join("$this->_tmp_pca", "$this->_tmp_pca.product_id = $this->_pr.id");
        if(!empty($weigh_value)) $this->db->join("$this->_tmp_pw", "$this->_tmp_pw.product_id = $this->_pr.id");
        if(!empty($pill_number_value)) $this->db->join("$this->_tmp_ppn", "$this->_tmp_ppn.product_id = $this->_pr.id");

        return $this->db->get()->result();
    }

    function get_all_item_category(){
        return $this->db->select("$this->_table.id, $this->_table.name, $this->_table.slug, $this->_cate.id as cate_id, $this->_cate.name as cate_name, $this->_cate.slug as cate_slug, $this->_cate.parent_id")
                ->where(array("$this->_table.status" => 1, "$this->_pr.status" => 1, "$this->_cate.status" => 1))
                ->order_by("$this->_cate.weight", "ASC")
                ->order_by("$this->_cate.id", "DESC")
                ->order_by("$this->_table.weight", "ASC") 
                ->order_by("$this->_table.id", "DESC")
                ->group_by("$this->_table.id")
                ->from($this->_table)
                ->join("$this->_tmp_ppt", "$this->_tmp_ppt.product_type_id = $this->_table.id")
                ->join("$this->_pr", "$this->_pr.id = $this->_tmp_ppt.product_id")
                ->join("$this->_tmp_pc", "$this->_tmp_pc.product_id = $this->_pr.id")
                ->join("$this->_cate", "$this->_cate.id = $this->_tmp_pc.category_id")
                ->get()
                ->result();
    }
    
}