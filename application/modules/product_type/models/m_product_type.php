<?php
class M_product_type extends CI_Model
{
    protected $_table = 'product_type';
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function show_list_product_type($where = array(), $limit, $offset)
    {
        $this->db->where($where);
        $this->db->order_by('weight', "ASC");
        $this->db->order_by('id', "DESC");
        $this->db->limit($limit, $offset);
        $this->db->from($this->_table);
        return $this->db->get()->result();
        $this->db->free_result();
    }
    function get_product_typeID($id)
    {
        $this->db->where('id', $id);
        $this->db->from($this->_table);
        return $this->db->get()->row();
    }    

    function menu_admin($parentid = 0, $space = "", $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }
        $this->db->where('parent_id', $parentid);
        $this->db->order_by("weight", "asc");
        $this->db->order_by("id", "desc");
        $result    = $this->db->get($this->_table)->result();
        $trees_obj = array();
        if (!empty($result)) {
            foreach ($result as $rs) {
                $trees[] = array(
                    'id' => $rs->id,
                    'parent_id' => $rs->parent_id,
                    'weight' => $rs->weight,
                    'status' => $rs->status,
                    'name' => $space . $rs->name,
                    'level' => $rs->level,
                    'count_product' => $this->count_product($rs->id)
                    );
                $trees   = $this->menu_admin($rs->id, $space . '|---', $trees);
            }
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $tree        = (object) $tree;
                $trees_obj[] = $tree;
            }
        }
        return $trees_obj;
    }

    function get_product_type_tree_id($parentid = 0, $trees = array())
    {
        if (!$trees) {
            $trees = array();
        }
        $this->db->select('id');
        $this->db->where('parent_id', $parentid);
        $this->db->order_by("weight", "asc");
        $this->db->order_by("id", "desc");
        $result    = $this->db->get($this->_table)->result();
        $trees_obj = array();
        if (!empty($result)) {
            foreach ($result as $rs) {
                $trees[] = $rs->id;
                $trees   = $this->get_categroy_tree_id($rs->id, $trees);
            }
        }
        if (!empty($trees)) {
            foreach ($trees as $tree) {
                $tree        =  $tree;
                $trees_obj[] = $tree;
            }
        }
        return $trees_obj;
    }

    function count_where($where = array(), $table)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }
    function count_product($product_type_id)
    {
         $this->db->select('product.id');
         $this->db->where('tmp_product_product_type.product_type_id', $product_type_id);
         $this->db->group_by('product.id');
         $this->db->from('product');
         $this->db->join('tmp_product_product_type', 'tmp_product_product_type.product_id=product.id');
         return $this->db->count_all_results();
    }
}

