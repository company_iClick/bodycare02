<?php if(!empty($array_cate_node)){ ?> 
<div class="wrapper_full wrapper_full_border_1">
    <div class="wrapper_full_1">
    	<?php foreach($array_cate_node as $key => $value){
    		$cate_info = explode(',', $key);
    		$name = $cate_info[1];
    		$list_item = $value;
    	?> 
    	<div class="list_pr_ds"> 
    		
            <div class="cate_arrangement_pr_list">
                <div class="cate_arrangement_pr_list_text">
                    <p><?php echo $name; ?></p>
                    <b></b>
                </div>
            </div>
            <div class="clear"></div>
            <div class="box_list_item clearfix">
            	<?php if(!empty($list_item)){?>
				<ul>
					<?php foreach ($list_item as $row) {
						foreach ($row as $val) {
							$item_name = $val->name;
							$item_link = site_url($val->slug);
							echo sprintf('<li><a href="%s" title="%s">%s</a></li>', $item_link, $item_name, $item_name);
						}	
					} ?>
				</ul>
            	<?php } ?>
            </div>

        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>