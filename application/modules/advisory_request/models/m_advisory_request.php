<?php

class M_advisory_request extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // show list advisory_request
    function show_list_advisory_request($lang = 'vn') {
        $this->db->select("*");
        $this->db->order_by('advisory_request.weight', "ASC");
		$this->db->order_by('advisory_request.id', "DESC");
        $this->db->from('advisory_request');
        return $this->db->get()->result();
    }
    // show list advisory_request where
    function show_list_advisory_request_where($where=array()) {
        $this->db->select("*");
        $this->db->where($where);
        $this->db->order_by('advisory_request.weight', "ASC");
		$this->db->order_by('advisory_request.id', "DESC");
        $this->db->from('advisory_request');
        return $this->db->get()->result();
    }
    
}
