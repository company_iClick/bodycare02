<?php



class M_group_contact extends CI_Model {



    function __construct() {

        parent::__construct();

        $this->load->database();

    }

    function show_list_group_contact($where = array(), $limit, $offset) {

        $this->db->where($where);

        $this->db->order_by('id', "ASC");

        $this->db->order_by('weight', "ASC");

        $this->db->limit($limit, $offset);

        $this->db->from('group_contact');

        return $this->db->get()->result();

        $this->db->free_result();

    }



    // get id

    function get_group_contactID($id) {

        $this->db->where('group_contact.id', $id);

        $this->db->from('group_contact');

        return $this->db->get()->row();

    }

}

