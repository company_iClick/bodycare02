<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class index extends CI_Controller 

{

		protected $meta_keywords ='Bếp từ, bếp điện, lò nướng điện';

		protected $meta_description = 'Bếp từ, bếp điện, lò nướng điện';

function __construct()

{

	parent::__construct();

		$this->load->database();

		$this->load->library(array('session','cart'));

		$this->load->helper(array("url"));

		$this->load->view("front/inc/lang/block");

		$this->load->model(array("global_function","a_article","a_general","a_item", "counter"));

}

//====Trang chủ=====================================================================================	

	function url()

	{

		redirect(base_url('vn/trang-chu'));

	}

	function index($lang='vn')

	{

		/*$query ="SELECT id FROM category where top_category = 0

					 ORDER BY RAND()";

					 $result = $this->db->query($query);

			 		foreach($result->result_array() as $row){

						$id = $row['id'];

		}*/

		$data['id'] = 81;

		$this->load->helper('text');

		if($this->uri->segment(1) == ""){$lang='vn';}else{$lang=$this->uri->segment(1);}

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		

		$data['mod']='home';

		$data['title'] = $l->lang_trangchu[$lang];

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		//==========================================================

			//$data['row_main'] = $this->get_one_default();

			//$data['rs_all']	  = $this->get_all_video();

		//===========================================================

		//$data['sp_project']=$this->a_general->show_list_album_where(array("album.hot"=>'1'),$lang,100,0);

		$id_tt = 2;

		$data['get_one_tt'] = $this->a_article->get_one_article($id_tt,$lang);

		//print_r($data['get_one_tt']);exit;

		

		//get one article post

		$data['rs_one'] = $this->news_last_post($id_tt, $lang);

		

		$data['tintuc']=$this->a_article->get_value_article($id_tt,$lang, $data['get_one_tt']['id_article']);

		if($this->input->post("ajax") == 1){

			$this->load->view("front/page/home_ajax",$data);

		}else{		

			$this->template->write('title','Design');

			$this->template->write('description',$this->meta_description);

			$this->template->write('keywords',$this->meta_keywords);

			$this->template->write_view('content','front/page/home', $data, true);

			$this->template->render();

		}

	}

	/************************************************************************/

	function news_last_post($id, $lang)

	{

		$this->db->select("articledetail.name,articledetail.summary,articledetail.name_link,article.id,article.date_create,article.date_modify,article.status,articleterm.id_term,images.thumb");

		$this->db->where("country.name",$lang);

		$this->db->where("article.status",1);

		$this->db->where("termdetail.id_term",$id);

	    $this->db->group_by('articledetail.name');

		$this->db->order_by('article.id',"DESC");

		$this->db->order_by('article.weight',"ASC");

		$this->db->limit(1);

		$this->db->from('article');

		$this->db->join('articledetail','articledetail.id_article=article.id');

		$this->db->join('articleterm','articleterm.id_article=article.id');

		$this->db->join('articleimages','articleimages.id_article=article.id');

		$this->db->join('images','articleimages.id_image=images.id');

		$this->db->join('term','articleterm.id_term=term.id');

		$this->db->join('termdetail','termdetail.id_term=term.id');

		$this->db->join('country','articledetail.id_country=country.id');

		$query = $this->db->get();

		if($query->num_rows() >0 )

		{

			$result = $query->row_array();

			$query->free_result();

		}else{

			$result = FALSE;

		}

		return $result;

	}

	function launch()

	{

		//exit('aaa');

		$this->load->helper('text');

		$lang='vn';

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		

		$data['mod']='home';

		$data['title'] = 'Launch';

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		

		$this->template->write('title',$data['title']);

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/launch', $data, true);

		$this->template->render();

	}

	function launch_load()

	{

		//exit($id);

		$data['id']= $this->a_item->show_list_item_tieubieu_page_id($lang='vn');

		//print_r($data['id']->id);exit;

		$data['detail']=$this->a_item->show_detail_item_id($data['id']->id,$lang);

		//print_r($data['detail']); exit();

		$this->load->view('front/page/v_launch_load', $data);

	}

	function launch_load_01($id)

	{

		$lang = 'vn';

		$data['id']=$id;

		$data['detail']=$this->a_item->show_detail_item_id($id,$lang);

		//print_r($data['detail']);exit;

		$this->load->view('front/page/v_launch_load_01', $data);

	}

	function innovate($id, $lang='vn')

	{

		if($this->uri->segment(2) != '') $page_no=$this->uri->segment(2);else $page_no=1;

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		$detail=$this->a_article->show_detail_term_id($id,$lang);

		//print_r($detail); exit();

		$data['title'] =$detail->name;

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		$data['breadcrumb']=$detail ->name;

		$data['show_title']=$detail ->name;

		$data['mod']=$detail ->name;

		$data['page_nom']=1;

		$page_co=6;

		$start=($page_no-1)*$page_co;

		$data['id']=$id;

		//echo $page_co;exit($page_co);

		$count=$this->counter->count_cat($data['id'],$lang);

		$data['article']=$this->a_article->show_list_article_all_($data['id'],$page_co,$start,$lang);

		

		$data['link']=$this->paging($page_co,$count,$this->uri->segment(1).'/',$page_no);

		

		

		$this->template->write('title',$data['title']);

		$this->template->write('description',$data['description']);

		$this->template->write('keywords',$data['keywords']);

		$this->template->write_view('content','front/page/innovate', $data, true);

		$this->template->render();

	}

	function innovate_detail($id_article, $lang='vn')

	{

		//exit($id_article);

		$id = $id_article;

		//exit($id);

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['mod']='news';



		$data['page_nom']=1;

		$detail=$this->a_article->show_detail_article_id($id, $lang);

		//echo $detail->id_term; exit;

		//print_r($detail);exit;

		if(empty($detail))

		{

			redirect(base_url());

		}

		/****************Cap nhat view********************/

		if ($this->session->userdata('article'.$id) != $id)

			{

				$this->session->set_userdata('article'.$id, $id);

				$this->db->where('id', $id);

				$this->db->set('view', 'view+1', FALSE);

				

				$this->db->update('article');						

			}		

		/**************End cap nhat view***************************/

		$data['title'] = $detail->name ;

		$data['keywords']= $detail->keywords_seo;

		$data['description'] = $detail->description_seo;

		//exit($data['description']);

		$data['breadcrumb']=$detail->name;

		$data['show_title']=$detail->name;

		

		$data['detail']=$detail;

		$data['article_more'] = $this->a_article->show_list_article_same($lang='vn',$detail->id_term,$detail->id,6);

		//phan trang

		if($this->uri->segment(3) != '') $page_no=$this->uri->segment(3);else $page_no=1;

		$data['page_nom']=1;

		$page_co=4;

		$start=($page_no-1)*$page_co;

		$count_innovate=$this->counter->count_cat(30,$lang);

		$data['innovate']=$this->a_article->show_list_article_all_(30,$page_co,$start,$lang);

		$count_prepare=$this->counter->count_cat(33,$lang);

		$data['prepare']=$this->a_article->show_list_article_all_(33,$page_co,$start,$lang);

		$data['link_innovate']=$this->paging($page_co,$count_innovate,$this->uri->segment(1).'/',$page_no);

		$data['link_prepare']=$this->paging($page_co,$count_prepare,$this->uri->segment(1).'/',$page_no);

		

		$this->template->write('title',$data['title']);

		$this->template->write('description',$data['keywords']);

		$this->template->write('keywords',$data['description']);

		$this->template->write_view('content','front/page/innovate_detail', $data, true);

		$this->template->render();

	}

	function prepare($id, $lang='vn')

	{

		if($this->uri->segment(2) != '') $page_no=$this->uri->segment(2);else $page_no=1;

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		$detail=$this->a_article->show_detail_term_id($id,$lang);

		//print_r($detail); exit();

		$data['title'] =$detail->name;

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		$data['breadcrumb']=$detail ->name;

		$data['show_title']=$detail ->name;

		$data['mod']=$detail ->name;

		$data['page_nom']=1;

		$page_co=4;

		$start=($page_no-1)*$page_co;

		$data['id']=$id;

		//echo $page_co;exit($page_co);

		$count=$this->counter->count_cat($data['id'],$lang);

		$data['article']=$this->a_article->show_list_article_all_($data['id'],$page_co,$start,$lang);

		

		$data['link']=$this->paging($page_co,$count,$this->uri->segment(1).'/',$page_no);

		

		

		$this->template->write('title',$data['title']);

		$this->template->write('description',$data['description']);

		$this->template->write('keywords',$data['keywords']);

		$this->template->write_view('content','front/page/prepare', $data, true);

		$this->template->render();

	}

	function prepare_detail($id_article, $lang='vn')

	{

		//exit($id_article);

		$id = $id_article;

		//exit($id);

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['mod']='news';



		$data['page_nom']=1;

		$detail=$this->a_article->show_detail_article_id($id, $lang);

		if(empty($detail))

		{

			redirect(base_url());

		}

		/****************Cap nhat view********************/

		if ($this->session->userdata('article'.$id) != $id)

			{

				$this->session->set_userdata('article'.$id, $id);

				$this->db->where('id', $id);

				$this->db->set('view', 'view+1', FALSE);

				

				$this->db->update('article');						

			}		

		/**************End cap nhat view***************************/

		$data['title'] = $detail->name ;

		$data['keywords']= $detail->keywords_seo;

		$data['description'] = $detail->description_seo;

		//exit($data['description']);

		$data['breadcrumb']=$detail->name;

		$data['show_title']=$detail->name;

		

		$data['detail']=$detail;

		$data['article_more'] = $this->a_article->show_list_article_same($lang='vn',$detail->id_term,$detail->id,6);

		//phan trang mind

		if($this->uri->segment(3) != '') $page_no=$this->uri->segment(3);else $page_no=1;

		$data['page_nom']=1;

		$page_co=4;

		$start=($page_no-1)*$page_co;

		$count_innovate=$this->counter->count_cat(30,$lang);

		$data['innovate']=$this->a_article->show_list_article_all_(30,$page_co,$start,$lang);

		$count_prepare=$this->counter->count_cat(33,$lang);

		$data['prepare']=$this->a_article->show_list_article_all_(33,$page_co,$start,$lang);

		$data['link_innovate']=$this->paging($page_co,$count_innovate,$this->uri->segment(1).'/',$page_no);

		$data['link_prepare']=$this->paging($page_co,$count_prepare,$this->uri->segment(1).'/',$page_no);

		//end

		$this->template->write('title',$data['title']);

		$this->template->write('description',$data['keywords']);

		$this->template->write('keywords',$data['description']);

		$this->template->write_view('content','front/page/prepare_detail', $data, true);

		$this->template->render();

	}

	function troopers()

	{

		$this->load->helper('text');

		$lang='vn';

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		

		$data['mod']='home';

		$data['title'] = 'Troopers';

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		

		$data['about']=$this->a_article->show_detail_article_id(1,$lang);

		$this->template->write('title',$data['title']);

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/troopers', $data, true);

		$this->template->render();

	}

	function journey()

	{

		$this->load->helper('text');

		$lang='vn';

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		

		$data['mod']='home';

		$data['title'] = 'journey';

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		

		$this->template->write('title',$data['title']);

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/journey', $data, true);

		$this->template->render();

	}

	function quote()

	{

		$this->load->helper('text');

		$lang='vn';

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		//exit($lang);

		

		$data['mod']='home';

		$data['title'] = 'Quote';

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		

		$this->template->write('title',$data['title']);

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/quote', $data, true);

		$this->template->render();

	}

	function category($id)

	{

		$lang = $this->uri->segment(1);

		//exit($id);

		$data['id'] = $id;

		$this->load->helper('text');

		if($this->uri->segment(1) == ""){$lang='vn';}else{$lang=$this->uri->segment(1);}

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		

		$detail=$this->a_item->show_detail_category_id($id, $lang);

		$data['detail']= $detail;

		$data['title'] = $detail->name ;

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		$data['breadcrumb']=$detail->name ;

		$data['show_title']=$detail->name ;

			$this->template->write('title',$data['title']);

			$this->template->write('description',$this->meta_description);

			$this->template->write('keywords',$this->meta_keywords);

			$this->template->write_view('content','front/page/home', $data, true);

			$this->template->render();

	}

	function home_ajax($lang='vn')

	{

		

		$data['id'] = $this->input->post('id');

		//exit($lang);

		$this->load->helper('text');

		//if($this->uri->segment(1) == ""){$lang='vn';}else{$lang=$this->uri->segment(1);}

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		//exit($lang);

		

		$data['mod']='home';

		$data['title'] = $l->lang_trangchu[$lang];

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;



		if($this->input->post("ajax") == 1){

			$this->load->view("front/page/home_ajax",$data);

		}

	}

	/************************************************************************/

	function get_all_video()

	{

		$this->db->select('*');

		$this->db->where('status',1);

		$this->db->order_by('id DESC');

		$query = $this->db->get('video');

		

		if($query->num_rows() > 0 )

		{

			foreach($query->result_array() as $row):

				$return[] = $row;

			endforeach;

			$result = $return;

			$query->free_result();

		}else{

			$result = FALSE;

		}

		return $result;

	}

	/************************************************************************/

	function get_one_default()

	{

		$this->db->select('*');

		$this->db->where('status',1);

		$this->db->order_by('id DESC');

		$this->db->limit(1);

		$query = $this->db->get('video');

		if( $query->num_rows() > 0 )

		{

			$row = $query->row_array(); 

			$query->free_result();

			return $row;

		}else{

			return FALSE;

		}

	}

	/************************************************************************/

	/************************************************************************/

	function get_video()

	{

		$video_id = (int)$this->input->post('video_id');

		$this->db->select('*');

		$this->db->where('id',$video_id);

		$query = $this->db->get('video');

		if($query->num_rows() >0 )

		{

			$row 	  = $query->row_array();

			$title 	  = $row['title'];

			$content  = "";

			

			

			$data = $title.'||'.$content; 

			echo $data;

		}else

		{

			$status = 1;

			$msg    = 'Data not found!';

			$data   = $status.'||'.$msg;

			echo $data;exit;

		}

	}

	

	//================================================================

function About($lang='vn')

	{

		//exit('aaa');

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['mod']='about';

		$data['title'] = $l->lang_about[$lang];

		$data['description'] = $this->meta_description;

		//exit($data['description']);

		$data['keywords']=$this->meta_keywords;

		

		$data['about']=$this->a_article->show_detail_article_id(1,$lang);

		//print_r($data['about']);exit;

		

		$this->template->write('title','Giới thiệu');

		$this->template->write('description',$data['description']);

		$this->template->write('keywords',$data['keywords']);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

//================================================================

function cat_About($lang='vn', $id)

	{

		//exit($lang);

		$id=$this->uri->segment(3);

		//exit($id);

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['title'] = $l->lang_about[$lang];

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		$data['about']=$this->a_article->show_detail_article_name_link($id,$lang);

		//print_r($data['about']);exit;

		$this->template->write('title',$data['title']);

		$this->template->write('description',$data['description']);

		$this->template->write('keywords',$data['keywords']);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

	function cat_home($lang='vn', $id)

	{

		//exit($lang);

		$id=$this->uri->segment(3);

		//exit($id);

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['title'] = 'Giới thiệu';

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		$data['about']=$this->a_article->show_detail_article_name_link($id,$lang);

		//print_r($data['about']);exit;

		$this->template->write('title','Giới thiệu');

		$this->template->write('description',$data['description']);

		$this->template->write('keywords',$data['keywords']);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

function repair($lang='vn')

	{

		

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['mod']='repair';

		//$data['title'] = $l->lang_suachua[$lang];

		$data['title'] = "chuyên sửa laptop các loại hcm";

		/*$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;*/

		$data['description'] = "Trung tâm sửa chữa laptop ,máy tính bảng ,iphone uy tín lớn nhất tại tp.hcm";

		$data['keywords']= "Sửa chữa laptop, Sửa chữa máy tính bảng, Sửa chữa smartphone";

		$data['content']='about';

		$data['about']=$this->a_article->show_detail_article_id(2,$lang);

		$this->load->view("front/index",$data);	

	}

	function service($lang='vn')

	{

		//exit('aa');

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['mod']='about';

		$data['title'] = $l->lang_about[$lang];

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		

		$data['about']=$this->a_article->show_detail_article_id(2,$lang);

		//print_r($data['about']);exit;

		$this->template->write('title','Dịch vụ');

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

		//================================================================

	function khuyenmai($lang='vn')

	{

		$lang='vn';

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;

		$data['title'] = $l->lang_hotro[$lang];

		//$data['description'] = $l->lang_hotro[$lang];

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

	

		$data['about']=$this->a_article->show_detail_article_id(20,$lang);

		print_r($data['about']);exit;

		$this->template->write('title','Khuyến mãi');

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

//================================================================

//================================================================

	function support($lang='vn')

	{

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['mod']='support';

		$data['title'] = $l->lang_hotro[$lang];

		//$data['description'] = $l->lang_hotro[$lang];

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		$data['keywords']=$l->lang_suachua[$lang];

		$data['content']='about';

		$data['about']=$this->a_article->show_detail_article_id(3,$lang);

		$this->load->view("front/index",$data);	

	}

//================================================================

	function Recruitment($lang='vn')

	{

		//exit('aaa');

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['mod']='recruitment';

		$data['title'] = $l->lang_recruitment[$lang];

		$data['description'] = '';

		$data['keywords']='';

		$data['content']='about';

		$data['about']=$this->a_article->show_detail_article_id(50,$lang);

		$this->template->write('title',$data['title']);

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

	function Promotion($lang='vn')

	{

		//exit($lang);

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['title'] =$l->lang_khuyenmai[$lang];

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		$data['content']='about';

		$data['about']=$this->a_article->show_detail_article_id(51,$lang);

		//var_dump($data['about']);exit;

		$this->template->write('title',$data['title']);

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

	function Distributor($lang='vn')

	{

		//exit($lang);

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['title'] =$l->lang_donggop[$lang];

		$data['description'] = $l->lang_donggop[$lang];

		$data['keywords']=$l->lang_donggop[$lang];

		$data['content']='about';

		$data['about']=$this->a_article->show_detail_article_id(54,$lang);

		//var_dump($data['about']);exit;

		$this->template->write('title',$data['title']);

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

	function Warranty($lang='vn')

	{

		//exit($lang);

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['title'] =$l->lang_baohanhchinhhang[$lang];

		$data['description'] = $l->lang_baohanhchinhhang[$lang];

		$data['keywords']=$l->lang_baohanhchinhhang[$lang];

		$data['content']='about';

		$data['about']=$this->a_article->show_detail_article_id(55,$lang);

		//var_dump($data['about']);exit;

		$this->template->write('title',$data['title']);

		$this->template->write('description',$this->meta_description);

		$this->template->write('keywords',$this->meta_keywords);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

	function ajax_local()

	{

		$data['id'] = $this->input->post('id');

		$this->load->view('front/page/local/ajax_local', $data);

	}

	function ajax_hn()

	{

		$this->load->view('front/page/local/ajax_hn');

	}

	function Dichvu()

	{

		$data['mod']='dichvu';

		$data['title'] = 'Dịch vụ | TOAN DECAL';

		$data['description'] = '';

		$data['keywords']='';

		$data['content']='about';

		$data['about']=$this->a_article->show_detail_article_id(2);

		$this->load->view("front/index",$data);	

	}

	function cat_about_detail()

	{

		

		$id=$this->uri->segment(2);

		$lang = 'vn';

		$data['lang']='';

		$l= new lang();

		

		if($lang=='')

			$data['lang']='en';

		else

			$data['lang']=$lang;

			

		$data['l']=$l;



		$data['mod']='about';

		$data['title'] = $l->lang_about[$lang];

		$data['description'] = $this->meta_description;

		$data['keywords']=$this->meta_keywords;

		$data['about']=$this->a_article->show_detail_article_name_link($id,$lang);

		$this->template->write('title',$data['about']->name);

		$this->template->write('description',$data['about']->name);

		$this->template->write('keywords',$data['about']->name);

		$this->template->write_view('content','front/page/about', $data, true);

		$this->template->render();

	}

	function load_ajax()

	{

		$this->load->view('front/page/js/js_popup_info');

	}

	function load_ajax_dn()

	{

		$this->load->view('front/page/js/js_popup_dn_info');

	}

	  public function paging($page,$total,$url,$id=1)

	{



		$previous_btn = true;

		$next_btn = true;

		$first_btn = true;

		$last_btn = true;

		//kiem tra





		$count=$total;

		$tongtrang=ceil($total/$page);

		$num="";



		if($count!=0)

		{

			if ($id >= 7) {

				$start_loop = $id - 4;

				if ($tongtrang > $id + 4)

				$end_loop = $id + 4;

				else if ($id <= $tongtrang && $id > $tongtrang - 6) {

					$start_loop = $tongtrang - 6;

					$end_loop = $tongtrang;

				} else {

					$end_loop = $tongtrang;

				}

			} else {

				$start_loop = 1;

				if ($tongtrang > 7)

				$end_loop = 7;

				else

				$end_loop = $tongtrang;

			}

		}





		// FOR ENABLING THE FIRST BUTTON

		if ($first_btn && $id > 1) {

			$dau = "<li  class=''><a href='".site_url($url.'1')."'>Đầu</a></li>";

		} else if ($first_btn) {

			$dau= "<li  class='text'></li>";

		}



		// FOR ENABLING THE PREVIOUS BUTTON

		if ($previous_btn && $id > 1) {

			$tam=$id-1;

			$lui = "<li class=''><a href='".site_url($url. $tam)."'>Lùi</a></li>";

		} else if ($previous_btn) {

			$lui = "<li class='text'></li>";

		}





		if ($next_btn && $id < $tongtrang) {

			$tam2=$id+1;

			$toi = "<li class=''><a href='".site_url($url. $tam2)."'> Tới </a></li>";

		} else if ($next_btn) {

			$toi = "<li class='text'></li>";

		}



		// TO ENABLE THE END BUTTON

		if ($last_btn && $id < $tongtrang) {

			$cuoi= "<li  class=''><a href='".site_url($url.$tongtrang)."'> Cuối </a></li>";

		} else if ($last_btn) {

			$cuoi = "<li class='text'></li>";

		}

		if($count>0)

		{

			for($i=$start_loop;$i<=$end_loop;$i++)

			{

				if($i==$id)

				$num.="<li class='page'><a href='#' title='' onclick='return false'>$i</a></li>";

				else

				$num.="<li><a href='".site_url($url . $i)."' title=''>$i</a></li>";

			}

		}

		if($count>0&&$tongtrang>1)

		$link="

		<ul class='pagination'>

            

			".$num."

			

		</ul>

			";

		else

		$link='';



		return $link;



	}

	

}

