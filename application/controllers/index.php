<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class home extends CI_Controller {



    function __construct() {

        parent::__construct();

        $this->load->database();

        $this->load->model(array("p_model", 'global_function', 'a_general', 'm_session'));

        //$this->load->model("iclick");

        $this->load->library(array('session', 'cart'));

        $this->load->helper(array("url", "text"));

        $this->load->view("front/inc/lang/block");

        //	$this->load->view('public/function');

    }



//====Trang chủ=====================================================================================	

    public function index() { 

        $data['mod'] = "NotFound";

        $data['title'] = "NotFound";

        $this->template->write('title', $data['title']);

        $this->template->add_css('themes/css/default/account-setting.css');

        $this->template->add_css('themes/css/default/stype.css');

        $this->template->write('mod', $data['mod']);

        $this->template->write_view('content', 'front/inc/not_found', $data, TRUE);

        $this->template->render();

    }



}

