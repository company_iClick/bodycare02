<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
	
	protected $meta_title ;
	protected $meta_keywords ;
	protected $meta_description ;
	protected $project="Terrace resort" ;
	
	function __construct()
	{
		parent::__construct();
		// Your own constructor code
		$this->load->helper(array('url','text','form','file'));
		$this->load->library(array('session','form_validation'));
		$this->load->database();
		$this->load->model(array('m_admin','m_article','counter','general'));
		$this->load->model('global_function');
		$this->load->model('m_session');
		date_default_timezone_set('Asia/Saigon');
		
	}
	function admin()
	{
		parent::Controller();

	}
	
	function index()
	{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		$data['name_project']=$this->project;
		$data['mod']='term_list';
		$data['breadcrumb']='<li>>></li><li class="current">Loại Bài Viết</li>';
		
		if(isset($_POST['show'])){
			$array=array_keys($this->input->post('checkall'));
			foreach ($array as $a){
				if($this->checkper($a,'show')){
					$this->show_more($a);
				}
			}
			redirect( site_url('back/term/').'?messager=success' );
		}
		if(isset($_POST['hide'])){
			$array=array_keys($this->input->post('checkall'));
			foreach ($array as $a){
				//--------change parent------
				if($this->checkper($a,'hide')){
				$this->hide_more($a);
				}
			}
			redirect( site_url('back/term/').'?messager=success' );
		}
		if(isset($_POST['delete'])){
			$array=array_keys($this->input->post('checkall'));
			foreach ($array as $a){
				//--------change parent------
				if($this->checkper($a,'del')){
				$this->delete_more($a);
				}
			}
			redirect( site_url('back/term/').'?messager=success' );
		}
		$data['term']=$this->m_article->show_list_term_root();
		$data['right']='back/term/list';
		$this->load->view('back/index',$data);
	
	}
	//============================================
	function add(){
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		$data=array(); 
		$data['name_project']=$this->project;
		$data['mod']='term_add';
		$data['breadcrumb']='<li>>></li><li><a href="back/term">Loại Bài Viết</a></li><li>>></li><li class="current">Thêm mới</li>';
		
		
		if(isset($_POST['ok']))
		{
			$this->form_validation->set_rules('top_items', 'Thư mục gốc', 'is_natural_no_zero');
			foreach($this->m_article->show_list_lang() as $lang){
				$this->form_validation->set_rules('name_'.$lang->name, 'Tên danh mục ('.$lang->name.') ', 'trim|required|max_length[500]');
			}
			$this->form_validation->set_error_delimiters('<label class="error">', '</label>');
			
			if ($this->form_validation->run() == TRUE){
				$sql = array(
	            'weight'=>$this->input->post('weight'),
	            'date_modify'=>  date('Y-m-d H:i:s'),
	            'date_create'=>  date('Y-m-d H:i:s'),
							'top_term'=>$this->input->post('top_items'),
						'per'=>'a:5:{i:0;s:4:"hide";i:1;s:4:"show";i:2;s:3:"del";i:3;s:4:"edit";i:4;s:3:"add";}',
							'author'=>  $this->m_session->userdata('admin_login')->user_id,
						'edit'=>  $this->m_session->userdata('admin_login')->user_id
				);
				$this->db->insert('term' , $sql);
        $id_category=$this->db->insert_id();
			
				foreach($this->m_article->show_list_lang() as $lang){
          $sql = array(
          	'id_term'=>$id_category,
          	'id_country'=>$lang->id,
            'name'=>$this->input->post('name_'.$lang->name),
          	'name_link'=>$this->global_function->unicode($this->input->post('name_'.$lang->name)),
					);
					$this->db->insert('termdetail' , $sql);
         }
				redirect( site_url('back/term/').'?messager=success' );
			}
		}
		if(isset($_POST['ok_con']))
		{
			$this->form_validation->set_rules('top_items', 'Thư mục gốc', 'is_natural_no_zero');
			foreach($this->m_article->show_list_lang() as $lang){
				$this->form_validation->set_rules('name_'.$lang->name, 'Tên danh mục ('.$lang->name.') ', 'trim|required|max_length[500]');
			}
			$this->form_validation->set_error_delimiters('<label class="error">', '</label>');
			if ($this->form_validation->run() == TRUE){
				$sql = array(
            'weight'=>$this->input->post('weight'),
            'date_modify'=>  date('Y-m-d H:i:s'),
            'date_create'=>  date('Y-m-d H:i:s'),
						'top_term'=>$this->input->post('top_items'),
					'per'=>'a:5:{i:0;s:4:"hide";i:1;s:4:"show";i:2;s:3:"del";i:3;s:4:"edit";i:4;s:3:"add";}',
					'author'=>  $this->m_session->userdata('admin_login')->user_id,
						'edit'=>  $this->m_session->userdata('admin_login')->user_id
				);
				$this->db->insert('term' , $sql);
	      $id_category=$this->db->insert_id();
				
				foreach($this->m_article->show_list_lang() as $lang){
          $sql = array(
          	'id_term'=>$id_category,
          	'id_country'=>$lang->id,
            'name'=>$this->input->post('name_'.$lang->name),
          	'name_link'=>$this->global_function->unicode($this->input->post('name_'.$lang->name)),
					);
					$this->db->insert('termdetail' , $sql);
          }
				redirect( site_url('back/term/add').'?messager=success' );
			}
		}
		$data['right']='back/term/add';
		$this->load->view('back/index',$data);
	}
	//============================================
	function edit($id=''){
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		if(!$this->checkper($id,'edit')){
			redirect( site_url('back/term').'?messager=warning' );
		}
		$data=array(); $data['name_project']=$this->project;
		$data['mod']='term_list';
		
		if(isset($_POST['ok']))
		{
			$this->form_validation->set_rules('top_items', 'Thư mục gốc', 'is_natural_no_zero');
			foreach($this->m_article->show_list_lang() as $lang){
				$this->form_validation->set_rules('name_'.$lang->name, 'Tên danh mục ('.$lang->name.') ', 'trim|required|max_length[500]');
			}
			$this->form_validation->set_error_delimiters('<label class="error">', '</label>');
			if ($this->form_validation->run() == TRUE){
				$sql = array(
          'weight'=>$this->input->post('weight'),
          'date_modify'=>  date('Y-m-d H:i:s'),
					'top_term'=>$this->input->post('top_items'),
				'edit'=>  $this->m_session->userdata('admin_login')->user_id
				);
				$this->db->where('id',$id);
				$this->db->update('term' , $sql);
				
				foreach($this->m_article->show_list_lang() as $lang){
          	$sql = array(
          		'name'=>$this->input->post('name_'.$lang->name),
          		'name_link'=>$this->global_function->unicode($this->input->post('name_'.$lang->name)),
						);
						$this->db->where('id_term',$id);
						$this->db->where('id_country',$lang->id);
						$this->db->update('termdetail' , $sql);
          }
					redirect( site_url('back/term/edit/'.$id).'?messager=success' );
				}
			}
			$data['breadcrumb']='<li>>></li><li><a href="back/term">Loại Bài Viết</a></li><li>>></li><li class="current">Cập nhật</li>';
			$data['id']=$id;
			$data['right']='back/user/edit';
			$this->load->view('back/index',$data);
	}
	//============================================
	function change_pass(){
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		if(!$this->checkper($this->m_session->userdata('admin_login')->user_id,'edit')){
			redirect( site_url('back/term').'?messager=warning' );
		}
		$data=array(); $data['name_project']=$this->project;
		$data['mod']='user';
		
		if(isset($_POST['ok']))
		{
			
			$this->form_validation->set_rules('old_pass', 'Mật khẩu', 'trim|required|callback_checklogin|max_length[24]|min_length[6]');
			$this->form_validation->set_rules('new_pass', 'Mật khẩu mới', 'trim|required|max_length[24]|min_length[6]');
			$this->form_validation->set_rules('re_new_pass', 'Mật khẩu nhập lại', 'trim|required|max_length[24]|min_length[6]|callback_checknewpass');
			$this->form_validation->set_message('checklogin', 'Sai mật khẩu');
			$this->form_validation->set_message('checknewpass', 'Nhập lại không đúng');
			$this->form_validation->set_error_delimiters('<label class="error">', '</label>');
			if ($this->form_validation->run() == TRUE){
				$sql = array(
          'user_password'=>MD5($this->input->post('new_pass')),
          'user_modify'=>  date('Y-m-d H:i:s'),
					
				);
				$this->db->where('user_id',$this->m_session->userdata('admin_login')->user_id);
				if($this->db->update('tbl_user' , $sql))
				{
					if($this->m_session->userdata('admin_login') )
					{
						$this->m_session->destroy();
					}
					redirect( site_url('admin/') );
				}else{
						
					redirect( site_url('admin/change_pass').'?messager=error' );
				}
				}
			}
			$data['breadcrumb']='<li>>></li><li class="current">Đổi pass</li>';
			$data['right']='back/user/changepass';
			$this->load->view('back/index',$data);
	}
	//============================================\
	function delete($id)
	{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		if(!$this->checkper($id,'del')){
			redirect( site_url('back/term').'?messager=warning' );
		}
		
		$where=array('id_term'=>$id);
		$this->db->delete('termdetail',$where);
		
		$where=array('id_term'=>$id);
		$this->db->delete('articleterm',$where);
		
		$where=array('id'=>$id);
		$this->db->delete('term',$where);
	
		redirect( site_url('back/term').'?messager=success' );
	}
	//============================================\
	function delete_more($id)
	{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		$where=array('id_term'=>$id);
		$this->db->delete('termdetail',$where);
		
		$where=array('id_term'=>$id);
		$this->db->delete('articleterm',$where);
		
		$where=array('id'=>$id);
		$this->db->delete('term',$where);
		
		return true;
	}
	//============================================\
	function hide($id)
	{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		if(!$this->checkper($id,'hide')){
			redirect( site_url('back/term').'?messager=warning' );
		}
		$sql = array('status'=>0);
		$this->db->where('id',$id);
		$this->db->update('term' , $sql);
	
		redirect( site_url('back/term').'?messager=success' );
	}
	//============================================\
	function hide_more($id)
	{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		$sql = array('status'=>0);
		$this->db->where('id',$id);
		$this->db->update('term' , $sql);
	
		return true;
	}
//============================================\
	function show_more($id)
	{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		$sql = array('status'=>1);
		$this->db->where('id',$id);
		$this->db->update('term' , $sql);
	
		return true;
	}
	//============================================\
	function show($id)
	{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		if(!$this->checkper($id,'show')){
			redirect( site_url('back/term').'?messager=warning' );
		}
		$sql = array('status'=>1);
		$this->db->where('id',$id);
		$this->db->update('term' , $sql);
	
		redirect( site_url('back/term').'?messager=success' );
	}
	//===============================
		function checklogin()
		{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		if($this->m_admin->get_admin(0,$this->m_session->userdata('admin_login')->user_loginname,$_POST['old_pass'])==1)
			return true;
		else
			return false;
	}
	//===============================
		function checknewpass()
		{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		if($_POST['new_pass']==$_POST['re_new_pass'])
			return true;
		else
			return false;
		}
		//============================================
		public function checkper($id,$per){
		$a=$this->m_session->userdata('admin_login')->per;
		
				return true;
		
	}
	//============================================\
	function info($id)
	{
		if(!$this->checkuser()) redirect( site_url('admin/thongke/').'?messager=warning' );
		redirect( site_url('back/term/').'?messager=information&type=term&id='.$id );
	}
	//============================================
	public function checkuser(){
		if(!$this->m_session->userdata('admin_login')) {
			redirect( site_url('admin/login'));	
		}
		return true;
	}
	//============================================
	function print_arr($array){
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}

}

