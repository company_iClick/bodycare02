<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Administrator extends CI_Controller
{
    protected $meta_title;
    protected $meta_keywords;
    protected $meta_description;
    protected $project = "Terrace resort";
    function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'general',
            "global_function"
        ));
        $this->load->helper('url');
        $this->template->set_template('admin');
    }
    public function index()
    {
        if (!$this->session->userdata('admin_login')) {
            redirect(site_url('admin/login'));
        } else {
            redirect(site_url('admin/thongke'));
        }
    }
    function login()
    {
        $this->load->library('form_validation');
        if ($this->input->post()) {
            $this->form_validation->set_rules('user', 'Username', 'required|trim');
            $this->form_validation->set_rules('pass', 'Password', 'required|trim|callback_checklogin');
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            $this->form_validation->set_message('checklogin', 'Sai username hoặc password');
            if ($this->form_validation->run() == true) {
                $user   = strtolower($this->input->post('user'));
                $pass   = $this->input->post('pass');
                $record = $this->general->login($user, $pass);
                if ($record->num_rows() == 1) {
                    $login = $record->row();
                    $this->session->set_userdata('admin_login', $login);
                    $this->session->set_userdata('active_log', TRUE);
                    redirect(site_url('admin/'));
                } else {
                    redirect(site_url('admin/login'));
                }
            }
        }
        $this->load->view('back/login');
    }
    function checklogin()
    {
        if ($this->general->get_admin(0, $_POST['user'], $_POST['pass']) == 1)
            return true;
        else
            return false;
    }
    public function logout()
    {
        if ($this->session->userdata('admin_login')) {
            $this->session->unset_userdata('admin_login');
            $this->session->unset_userdata('active_log');
        }
        redirect(site_url('admin/'));
    }
    function NotPermission()
    {
        $data['mod']        = '';
        $data['meta_title'] = 'NotPemission';
        $this->template->write('title', $data['meta_title']);
        $this->template->add_css('themes/css/default/register.css');
        $this->template->write('mod', $data['mod']);
        $this->template->write_view('content', 'back/not_permission', $data, TRUE);
        $this->template->render();
    }

    function set_value(){
        $type = $this->input->get('type');
        $table = $this->input->get('table');
        $id = $this->input->get('id');
        $page = $this->input->get('page');
        $value = $this->input->get('value');
        if (!($this->general->Checkpermission("edit_".$table)))
            redirect(site_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            $type => $value
        ), $table);
        //redirect(site_url('admin/' . $table . '/index/' . $page) . '?messager=success');
    }

    function set_value_article(){
        $type = $this->input->get('type');
        $table = $this->input->get('table');
        $id = $this->input->get('id');
        $page = $this->input->get('page');
        $value = $this->input->get('value');
        $type_article = $this->input->get('type_article');
        if (!($this->general->Checkpermission("edit_".$table)))
            redirect(site_url("admin/not-permission"));
        $this->general->update_tableID($id, array(
            $type => $value
        ), $table);
        //redirect(site_url('admin/' . $table . '/index/' .$type_article . '/' . $page) . '?messager=success');
    }

    function thongke() {
        $this->template->set_template('admin');        // Set template 
        $data['mod'] = '';
        $data['meta_title'] = 'Thống kê';
        $data['right'] = 'back/thongke';
        $this->template->write('mod', $data['mod']);
        $this->template->write_view('content', 'back/thongke', $data, TRUE);
        $this->template->render();
    }
}

