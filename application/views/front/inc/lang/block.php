<?php
class lang
{
	// ---------------- Menu -----------------------------//
	public $lang_home= array('vn'=>'Trang chủ' , 'en'=>'Home');
    public $lang_about=array('vn'=>'Giới thiệu' , 'en'=>'About us');
    public $lang_store=array('vn'=>'Hệ thống cửa hàng' , 'en'=>'Stores');
    public $lang_product=array('vn'=>'Sản phẩm' , 'en'=>'Product');
    public $lang_sale=array('vn'=>'Khuyến mãi' , 'en'=>'Sale');
    public $lang_news=array('vn'=>'Tin tức' , 'en'=>'News');
    public $lang_contact=array('vn'=>'Liên hệ' , 'en'=>'Contact us');
    public $lang_support_one=array('vn'=>'Tồng đài CSKH' , 'en'=>'Telephone Operator');
    public $lang_cop_satra=array('vn'=>'Công ty thành viên Satra' , 'en'=>'Satra Cooperation Company');
    public $lang_more_service=array('vn'=>'Dịch vụ cộng thêm' , 'en'=>'Service');
    public $lang_ques=array('vn'=>'Hỏi đáp' , 'en'=>'Questions');
    public $lang_recruitment=array('vn'=>'Tuyển dụng' , 'en'=>'Recruitment');
    public $lang_total_visited=array('vn'=>'Lượt truy cập' , 'en'=>'Total Visited');
    public $lang_note_code=array('vn'=>'Khách hàng thân thiết xem điểm tích lũy tại đây' , 'en'=>'Loyalty points accumulated in this view');
    public $lang_share=array('vn'=>'Chia sẻ' , 'en'=>'Share');
    public $lang_delicious_dishes_every_day=array('vn'=>'Món ngon mỗi ngày' , 'en'=>'Delicious dishes every day');
    // --------------------- url menu
    public $lang_url_home= array('vn'=>'trang-chu' , 'en'=>'home');
    public $lang_url_about= array('vn'=>'gioi-thieu' , 'en'=>'about-us');
    public $lang_url_store= array('vn'=>'cua-hang' , 'en'=>'store');
    public $lang_url_product= array('vn'=>'san-pham' , 'en'=>'product');
    public $lang_url_sale= array('vn'=>'khuyen-mai' , 'en'=>'sale');
    public $lang_url_news= array('vn'=>'tin-tuc' , 'en'=>'news');
    public $lang_url_contact= array('vn'=>'lien-he' , 'en'=>'contact-us');
    public $lang_url_ques=array('vn'=>'hoi-dap' , 'en'=>'questions');
    public $lang_url_recruitment=array('vn'=>'tuyen-dung' , 'en'=>'recruitment');
    public $lang_url_more_service=array('vn'=>'dich-vu-cong-them' , 'en'=>'more-service');
    public $lang_url_search=array('vn'=>'tim-kiem' , 'en'=>'search');
    public $lang_url_check_point=array('vn'=>'xem-diem-tich-luy' , 'en'=>'check-point');
    public $lang_url_delicious_dishes_every_day=array('vn'=>'mon-ngon-moi-ngay' , 'en'=>'delicious-dishes-every-day');
    // --------------------- Users ------------------------------------
    public $lang_login=array('vn'=>'Đăng nhập' , 'en'=>'Login');
    public $lang_login_mod=array('vn'=>'Thành viên satra' , 'en'=>'Moderator');
    public $lang_register=array('vn'=>'Đăng ký' , 'en'=>'Register');
    public $lang_logout=array('vn'=>'Thoát' , 'en'=>'Logout');
    public $lang_login_info=array('vn'=>'Thông tin đăng nhập' , 'en'=>'Login Info');
    public $lang_old_password=array('vn'=>'Mật khẩu cũ' , 'en'=>'Old password');
    public $lang_new_password=array('vn'=>'Mật khẩu mới' , 'en'=>'New password');
    public $lang_password=array('vn'=>'Mật khẩu' , 'en'=>'Info');
    public $lang_re_password=array('vn'=>'Xác nhận mật khẩu' , 'en'=>'Confirm Password');
    public $lang_error_login=array("vn"=>"Thông tin không chính xác","en"=>"Emal or Password not correct");
    public $lang_success_login=array("vn"=>"Bạn đã đăng nhập thành công !","en"=>"Login Successfull!");
    public $lang_register_info=array('vn'=>'Thông tin đăng ký' , 'en'=>'Register Info');
    public $lang_birthday=array('vn'=>'Ngày sinh' , 'en'=>'Birth day');
    public $lang_sex=array('vn'=>'Giới tính' , 'en'=>'Gender');
    public $lang_men=array('vn'=>'Nam' , 'en'=>'Men');
    public $lang_women=array('vn'=>'Nữ' , 'en'=>'Women');
    public $lang_only_number=array("vn"=>"Chỉ được nhập số","en"=>"Only number");
    public $lang_account_info=array("vn"=>"Thông tin tài khoản","en"=>"Account Info");
    public $lang_order_info=array("vn"=>"Thông tin đơn hàng","en"=>"Order Info");
    public $lang_point=array("vn"=>"Điểm tích lũy","en"=>"Point");
    public $lang_update=array("vn"=>"Cập nhật","en"=>"Update");
    public $lang_change_pass=array("vn"=>"Đổi mật khẩu","en"=>"Change password");
    public $lang_update_success=array("vn"=>"Cập nhật thành công","en"=>"Cập nhật thành công");
    public $lang_address=array('vn'=>'Địa chỉ' , 'en'=>'Address');
    public $lang_phone=array('vn'=>'Điện thoại' , 'en'=>'Phone');
    public $lang_full_name=array('vn'=>'Họ & Tên' , 'en'=>'Full Name');
    public $lang_nationality=array('vn'=>'Quốc tịch' , 'en'=>'Nationality');
    public $lang_landline=array('vn'=>'Điện thoại bàn (nếu có)' , 'en'=>'Landline');
    public $lang_cellphone=array('vn'=>'Điện thoại di động' , 'en'=>'Cellphone');
    public $lang_city=array('vn'=>'Tỉnh/Tp' , 'en'=>'City');
    public $lang_agent=array('vn'=>'Quận/Huyện' , 'en'=>'County');
    public $lang_date_buy=array('vn'=>'Ngày mua' , 'en'=>'Date Buy');
    public $lang_status_order=array('vn'=>'Tình trạng đơn hàng' , 'en'=>'Status Order');
    public $lang_code_booking=array('vn'=>'Mã đơn hàng' , 'en'=>'Code Booking');
    public $lang_change=array('vn'=>'Thay đổi' , 'en'=>'Change');
    public $lang_cancel=array('vn'=>'Hủy' , 'en'=>'Cancel');
    public $lang_restore=array('vn'=>'Phục hồi' , 'en'=>'Restore');
    public $lang_title_contact=array('vn'=>'Tiêu đề' , 'en'=>'Title');
    public $lang_error_captcha=array('vn'=>'Captcha không chính xác' , 'en'=>'Captcha false !');
    public $lang_change_store=array('vn'=>'Chuyển cửa hàng' , 'en'=>'Change Store');
    public $lang_send=array('vn'=>'Gửi thông tin' , 'en'=>'Send Info');
    public $lang_store_process=array('vn'=>'Cửa hàng xử lý' , 'en'=>'Store processing');
    public $lang_store_change=array('vn'=>'Cửa hàng đã chuyển' , 'en'=>'Store changed');
    public $lang_from_change=array('vn'=>'Chuyển từ' , 'en'=>'From');
    public $lang_tax_code=array('vn'=>'Mã số thuê' , 'en'=>'Tax Code');
    public $lang_code_member=array('vn'=>'Mã khách hàng' , 'en'=>'Code member');
    public $lang_register_address=array('vn'=>'Nơi đăng ký' , 'en'=>'Register Address');
    public $lang_number_card=array('vn'=>'Số thẻ KHTT' , 'en'=>'Number Card');
    public $lang_type_card=array('vn'=>'Loại thẻ KHTT' , 'en'=>'Type Card');
    public $lang_point_begin=array('vn'=>'Điểm bắt đầu' , 'en'=>'Begin Point');
    public $lang_point_used=array('vn'=>'Điểm đã dùng' , 'en'=>'Used Point');
    public $lang_point_no_used=array('vn'=>'Điểm chưa dùng' , 'en'=>'Not used Point');
    public $lang_point_end_date=array('vn'=>'Điểm hết hạn' , 'en'=>'EndDate Point');
    public $lang_false_code=array('vn'=>'"Hiện tại hệ thống đang cập nhật dữ liệu, quý khách hàng vui lòng trở lại sau. Chân thành cám ơn!"' , 'en'=>'"Hiện tại hệ thống đang cập nhật dữ liệu, quý khách hàng vui lòng trở lại sau. Chân thành cám ơn!"');
    public $lang_not_membership_card=array('vn'=>'Bạn chưa được cấp thẻ khách hàng' , 'en'=>"You haven't membership card");
    public $lang_active_account=array('vn'=>'Kích hoạt tài khoản' , 'en'=>"Active Account");
    public $lang_actived_account=array('vn'=>'Tài khoản của bạn đã được kích hoạt' , 'en'=>"Your account is actived");
    public $lang_actived_false=array('vn'=>'Link kích hoạt này không còn hoạt động' , 'en'=>"Link not activity");
    public $lang_title_register=array('vn'=>'Bạn vửa đăng ký thành viên tại website của chúng tôi ! Vui lòng click vào link bên dưới để kích hoạt tài khoản' , 'en'=>"You just register at our website! Please click on the link below to activate your account");
    public $lang_forgot_pass=array('vn'=>'Quên mật khẩu ?' , 'en'=>'Forgot Pass ?');
    public $lang_send_forgot_pass=array('vn'=>'Yêu cầu của bạn đã được gửi thành công !Vui lòng kiểm tra email để xác nhận thông tin' , 'en'=>'Your request has been sent successfully! Please check your email to confirm information');
    public $lang_title_forgot=array('vn'=>'Bạn vừa yêu cầu khôi phục lại mật khẩu. Thông tin mới của bạn là:' , 'en'=>'You just require to restore password. New information is:');
    public $lang_error_recipient=array('vn'=>'Bạn chưa cập nhật đầy đủ thông tin người nhận hàng' , 'en'=>'You have not fully updated information consignee');
    //--------------- url
    public $lang_url_login=array('vn'=>'dang-nhap' , 'en'=>'login');
    public $lang_url_register=array('vn'=>'dang-ky' , 'en'=>'register');
    public $lang_url_logout=array('vn'=>'thoat' , 'en'=>'Logout');
    public $lang_url_cart=array('vn'=>'gio-hang' , 'en'=>'cart');
    public $lang_url_check_info=array('vn'=>'thong-tin-mua-hang' , 'en'=>'checkout-info');
    public $lang_url_checkout=array('vn'=>'thanh-toan' , 'en'=>'checkout');
    public $lang_url_account_info=array("vn"=>"thong-tin-tai-khoan","en"=>"account-info");
    public $lang_url_point=array("vn"=>"diem-tich-luy","en"=>"my-point");
    public $lang_url_order_info=array("vn"=>"don-hang-cua-ban","en"=>"my-order");
    public $lang_url_change_my_pass=array("vn"=>"doi-mat-khau-cua-ban","en"=>"change-my-password");
    public $lang_url_recipient=array('vn'=>'thong-tin-nguoi-nhan-hang' , 'en'=>'recipient-info');
    public $lang_url_recipient_ajax=array('vn'=>'thong-tin-nguoi-nhan-hang-ajax' , 'en'=>'recipient-info-ajax');
    public $lang_url_detail_order=array('vn'=>'chi-tiet-don-hang' , 'en'=>'detail-order');
    public $lang_url_order_store=array('vn'=>'don-hang-cua-hang' , 'en'=>'order-store');
    public $lang_url_detail_order_store=array('vn'=>'chi-tiet-don-hang-cua-hang' , 'en'=>'detail-order-store');
    public $lang_url_change_store=array('vn'=>'chuyen-cua-hang' , 'en'=>'change-store');
    public $lang_url_forgot_pass=array('vn'=>'quen-mat-khau' , 'en'=>'forgot-pass');



    //--------------------------- Search---------------------------
    public $lang_text_search=array('vn'=>'Nhập nội dung tìm kiếm' , 'en'=>'Please fill text key');
    public $lang_btn_search=array('vn'=>'Tìm kiếm sản phẩm' , 'en'=>'Search');
    public $lang_search=array('vn'=>'Tìm kiếm' , 'en'=>'Search');
    //--------------------------- Local news---------------------------
    public $lang_local_news=array('vn'=>'Tin nội bộ' , 'en'=>'Local News');
    public $lang_unread_news=array('vn'=>'Tin chưa đọc' , 'en'=>'Unread');
    public $lang_read_news=array('vn'=>'Tin đã đọc' , 'en'=>'Read');
    public $lang_list_news=array('vn'=>'Danh sách tin' , 'en'=>'List News');
    public $lang_document=array('vn'=>'Tài liệu' , 'en'=>'Documents');
    public $lang_title=array('vn'=>'Tên' , 'en'=>'Title');
    public $lang_user_post=array('vn'=>'Người đăng' , 'en'=>'Author');
    public $lang_date_post=array('vn'=>'Ngày đăng' , 'en'=>'Date');
    public $lang_content=array('vn'=>'Nội dung' , 'en'=>'Content');
    public $lang_comment=array('vn'=>'Bình luận' , 'en'=>'Comment');
    public $lang_list_comment=array('vn'=>'Danh sách bình luận' , 'en'=>'List Comments');
    //-----------url
    public $lang_url_local_news=array('vn'=>'tin-noi-bo' , 'en'=>'local-news');
    public $lang_url_unread_news=array('vn'=>'tin-chua-doc' , 'en'=>'unread');
    public $lang_url_read_news=array('vn'=>'tin-da-doc' , 'en'=>'read');
    public $lang_url_list_news=array('vn'=>'danh-sach-tin' , 'en'=>'list-news');
    public $lang_url_document=array('vn'=>'tai-lieu' , 'en'=>'document');
    public $lang_url_detail_notice=array('vn'=>'chi-tiet-thong-bao' , 'en'=>'notice-detail');

    //---------------------------- Detail product------------------
    public $lang_code_product=array('vn'=>'Mã sản phẩm' , 'en'=>'No');
    public $lang_detail_product=array('vn'=>'Chi tiết sản phẩm' , 'en'=>'Product Detail');
    public $lang_price=array('vn'=>'Giá' , 'en'=>'Price');
    public $lang_value=array('vn'=>'Giá gốc' , 'en'=>'Origin Price');
    public $lang_origin=array('vn'=>'Xuất xứ' , 'en'=>'Origin');
    public $lang_month=array('vn'=>'Tháng' , 'en'=>'month');
    public $lang_warranty=array('vn'=>'Thời gian bản hành' , 'en'=>'Warranty');
    public $lang_note_ship=array('vn'=>'Satrafoods giao hàng miễn phí trong bán kính 5km tại TPHCM với hóa đơn trên 200,000đ.' , 'en'=>'Satrafoods free delivery within a radius of 5 km in the city with over 200,000 VND bill.');
    public $lang_chose_numbers=array("vn"=>"Chọn số lượng","en"=>"Choose numbers");
    public $lang_add_cart=array('vn'=>'Thêm vào giỏ hàng' , 'en'=>'Add Cart');
    public $lang_info_detail=array('vn'=>'Thông tin chi tiết' , 'en'=>'Info Detail');
    public $lang_chose_location=array('vn'=>'Chọn khu vực' , 'en'=>'Choose Location');
    public $lang_chose_city=array('vn'=>'Chọn tỉnh/Tp' , 'en'=>'Choose City');
    public $lang_chose_agent=array('vn'=>'Chọn quận' , 'en'=>'Choose Agent');
    public $lang_chose_store=array('vn'=>'Chọn cửa hàng' , 'en'=>'Choose Store');
    public $lang_sort_location=array('vn'=>'Xắp xếp theo khu vực' , 'en'=>'Sort Location');
    public $lang_view_more=array('vn'=>'Xem thêm' , 'en'=>'View more');
    public $lang_view_detail=array('vn'=>'Xem chi tiết' , 'en'=>'View detail');
    public $lang_updating=array('vn'=>'Dữ liệu đang cập nhật' , 'en'=>'Data updating........');
    public $lang_other_news=array('vn'=>'Tin tức khác' , 'en'=>'Other News');
    public $lang_other_delicious=array('vn'=>'Tham khảo các món ngon khác cùng Satrafoods' , 'en'=>'Consult other delicacies along Satrafoods');
    public $lang_gif=array('vn'=>'Quà tặng kèm' , 'en'=>'Gif');
    public $lang_cart=array('vn'=>'Giỏ hàng' , 'en'=>'Cart');
    public $lang_quantity=array('vn'=>'Số lượng' , 'en'=>'Quantity');
    public $lang_totla_price=array('vn'=>'Thành tiền' , 'en'=>'Total price');
    public $lang_total_money=array('vn'=>'Tổng tiền' , 'en'=>'Total money');
    public $lang_delete=array('vn'=>'Xóa' , 'en'=>'Delete');
    public $lang_continues_buy=array('vn'=>'Tiếp tục mua hàng' , 'en'=>'Continues');
    public $lang_checkout=array('vn'=>'Thanh toán' , 'en'=>'Checkout');
    public $lang_empty_cart=array('vn'=>'Bạn chưa chọn sản phẩm nào' , 'en'=>'Your cart is empty');
    public $lang_user_buy=array('vn'=>'Thông tin người đặt hàng' , 'en'=>'Buyer Info');
    public $lang_recipient=array('vn'=>'Thông tin người nhận hàng' , 'en'=>'Recipient Info');
    public $lang_change_recipient_info=array('vn'=>'Đổi thông tin người nhận hàng' , 'en'=>'Change recipient Info');
    public $lang_note=array('vn'=>'Ghi chú' , 'en'=>'Note');
    public $lang_register_success=array('vn'=>'Bạn đã đăng ký thành công' , 'en'=>'Successfull');
    public $lang_copy_info=array('vn'=>'Thông tin người nhận hàng giống thông tin người đặt hàng' , 'en'=>'Information consignee same ordering information');
    public $lang_store_name=array('vn'=>'Tên cửa hàng' , 'en'=>'Store');
    public $lang_info_store=array('vn'=>'Thông tin siêu thị' , 'en'=>'Info Store');
    public $lang_note_checkout=array('vn'=>'<p class="bold">Thanh toán và giao nhận</p><p>Phương thức thanh toán : Giao hàng nhận tiền</p><p>Thời gian nhận hàng: Satrafoods sẽ giao hàng trong vòng 24h</p>' , 'en'=>'<p class="bold">Checkout and Ship</p>
<p>Type payment : Cash</p><p>Time : Satrafoods will ship in 24h</p>' , 'en'=>'<p class="bold">Checkout and Ship</p>');
    public $lang_note_success=array('vn'=>'Bạn đã đặt hàng thành công ! Chúng tôi sẽ liên hệ với bạn trong 24h tới' , 'en'=>'Bạn đã đặt hàng thành công ! Chúng tôi sẽ liên hệ với bạn trong 24h tới');
    public $lang_email_valiable=array('vn'=>'Email đã tồn tại' , 'en'=>'Email was exist');
    public $lang_email_false=array('vn'=>'Email không chính xác' , 'en'=>'Email was false');
    public $lang_email_add=array('vn'=>'Email đã được lưu thành công !' , 'en'=>'Email was saved successfull !');
    public $lang_email_sale=array('vn'=>'Nhận email khuyến mãi !' , 'en'=>'Email sale !');
    public $lang_send_ques=array('vn'=>'Gửi câu hỏi' , 'en'=>'Send Question');
    public $lang_supplier=array('vn'=>'Nhà cung cấp' , 'en'=>'Supplier');
    public $lang_manufacturers=array('vn'=>'Nhà sản xuất' , 'en'=>'Manufacturers');
    public $lang_packing=array('vn'=>'Qui cách đóng gói' , 'en'=>'Packing');
    public $lang_material=array('vn'=>'Chất liệu' , 'en'=>'Material');
    public $lang_taste=array('vn'=>'Hương vị' , 'en'=>'Taste');
    public $lang_subjects=array('vn'=>'Đối tượng' , 'en'=>'Subjects');
    public $lang_yes_no=array('vn'=>'Có' , 'en'=>'Yes');
    public $lang_note_point=array('vn'=>'Nhập Mã KHTT hoặc số CMND' , 'en'=>'Enter Code MP or ID number');
}
?>