<?php

$CI =& get_instance();
$rtr =& load_class('Router', 'core');
$moduleName = $rtr->fetch_module();
$className = $rtr->fetch_class();
$methodName = $rtr->fetch_method();

$uri_one = $this->uri->segment(1);

switch ($methodName) {

    case 'about':
        $article = $this->global_function->get_array_object(array('type' => 4), 'id, name, slug', 'article');
        break;

    case 'about_detail':
        $article = $this->global_function->get_array_object(array('type' => 4), 'id, name, slug', 'article');
        break;

    case 'report':
        $article = $this->global_function->get_array_object(array('type' => 5), 'id, name, slug', 'term');
        break;
    
    case 'report_term':
        $article = $this->global_function->get_array_object(array('type' => 5), 'id, name, slug', 'term');
        break;
    
    case 'report_detail':
        $article = $this->global_function->get_array_object(array('type' => 5), 'id, name, slug', 'term');
        break;
        
    case 'recruitment':
        $array = array(
            0 => array(
                'name' => 'Vị trí đang tuyển',
                'slug' => 'tuyen-dung',
            ),
            1 => array(
                'name' => 'Quy trình tuyển dụng',
                'slug' => $this->my_lib->changeTitle('Quy trình tuyển dụng'),
            ),
            2 => array(
                'name' => 'Văn hóa doanh nghiệp',
                'slug' => $this->my_lib->changeTitle('Văn hóa doanh nghiệp'),
            )
        );
        $article = json_encode($array);
        $article = json_decode($article);

        break;
    
    case 'recruitment_detail':
        $array = array(
            0 => array(
                'name' => 'Vị trí đang tuyển',
                'slug' => 'tuyen-dung',
            ),
            1 => array(
                'name' => 'Quy trình tuyển dụng',
                'slug' => $this->my_lib->changeTitle('Quy trình tuyển dụng'),
            ),
            2 => array(
                'name' => 'Văn hóa doanh nghiệp',
                'slug' => $this->my_lib->changeTitle('Văn hóa doanh nghiệp'),
            )
        );
        $article = json_encode($array);
        $article = json_decode($article);

        break;
    
    case 'page_detail':
        $array = array(
            0 => array(
                'name' => 'Vị trí đang tuyển',
                'slug' => 'tuyen-dung',
            ),
            1 => array(
                'name' => 'Quy trình tuyển dụng',
                'slug' => $this->my_lib->changeTitle('Quy trình tuyển dụng'),
            ),
            2 => array(
                'name' => 'Văn hóa doanh nghiệp',
                'slug' => $this->my_lib->changeTitle('Văn hóa doanh nghiệp'),
            )
        );
        $article = json_encode($array);
        $article = json_decode($article);

        break;

    default:
        break;
}


?>

<div class="list_article">
    <?php if(!empty($article)){ ?>
        <ul>
        <?php 
            $liClass = NULL; 
            $i=0;
            foreach($article as $key => $value){
                $name = $value->name;
                $link = site_url($value->slug);

                switch ($methodName) {
                    case 'about':
                        $liClass = ($i == 0) ? ' class="active"' : NULL;
                        break;

                    case 'about_detail':
                        $liClass = ($uri_one == $value->slug) ? ' class="active"' : NULL;
                        break;
                    
                    case 'report':
                        $liClass = ($i == 0) ? ' class="active"' : NULL;
                        break;

                    case 'report_term':
                        $liClass = ($uri_one == $value->slug) ? ' class="active"' : NULL;
                        break;
                    
                    case 'report_detail':
                        $liClass = (isset($term_id) && $term_id == $value->id) ? ' class="active"' : NULL;
                         break;
                    
                    case 'recruitment':
                        $liClass = ($i == 0) ? ' class="active"' : NULL;
                        break;
                    
                    case 'recruitment_detail':
                        $liClass = ($i == 0) ? ' class="active"' : NULL;
                        break;
                    
                    case 'page_detail':
                        $liClass = ($uri_one == $value->slug) ? ' class="active"' : NULL;
                        break;
                    
                    default:
                    break;

                }
            
        ?>
            <li<?php echo $liClass; ?>><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo $name; ?></a></li>
        <?php $i++;} ?>
        </ul>
    <?php } ?>
</div>

