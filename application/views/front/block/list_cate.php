<?php
if(!empty($list_cate)){ 
?>
<div class="box_cate_add">
	<div class="cate_home_mb"><p><?php echo $title_cate; ?></p></div>
	<div class="box_cate_news">
		<ul>
			<?php
				foreach($list_cate as $row){
				$liClass = ($row->slug == $this->uri->segment(1)) ? ' class="active"' : '';
                $listName = $row->name;
                $listLink = site_url($row->slug);
                $listHtml = sprintf('<li%s><a href="%s" title="%s">%s</a></li>', $liClass, $listLink, $listName, $listName);
                echo $listHtml;
                
            }
        ?> 
		</ul>
	</div>
</div>
<?php } ?>