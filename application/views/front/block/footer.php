<?php    
    $customer_support = $this->global_function->get_array_object(array('type' => 4), 'id, name, slug', 'article'); 
?>
<div class="clear"></div>
<footer>
    <div id="footer">
        <div class="footer">
            <div class="left_footer">
                <div class="logo_ft">
                    <a href=""><img src="<?php echo base_url() . _images; ?>logo_1.png"> </a>
                </div>
                <div class="text_add_company">
                    <div class="text_add_ft">
                        <span class="icon_ft icon-diachi">
                    </span>
                        <span class="text_right_ft ">
                        Địa chỉ: <?php echo $info->address; ?>
                    </span>
                    </div>
                    <div class="text_add_ft">
                        <span class="icon_ft icon-hotline2">
                    </span>
                        <span class="text_right_ft">
                          <p>Tel: <?php echo $info->tel; ?></p>
                          <p>Hotline: <b><?php echo $info->phone; ?></b></p>
                    </span>
                    </div>
                    <div class="text_add_ft">
                        <span class="icon_ft icon-email">
                    </span>
                        <span class="text_right_ft">
                       <p>Website: <?php echo $info->website; ?></p>
                        <p>Email: <?php echo $info->email; ?></p>
                    </span>
                    </div>

                </div>

            </div>
            <div class="right_footer">
                <div class="email_letter_social">
                    <form id="email_letter">
                        <input type="email" id="input_email_letter" name="email_letter" placeholder="Email nhận thông tin khuyến mãi">
                        <input type="button" class="submit_letter" id="btn_email_letter" value="Gửi">
                        <div class="clear"></div>
                        <div id="errorNewsletter"></div>
                    </form>
                    <div class="social">
                        <p>kết nối với chúng tôi</p>
                        <div class="footer_mangxh">
                            <a style="" class="secoial_icon icon_google" target="_blank" href="<?php echo $info->google; ?>"></a>
                            <a class="secoial_icon icon_fb" target="_blank" href="<?php echo $info->facebook; ?>"></a>
                            <a class="secoial_icon icon_twitter" target="_blank" href="<?php echo $info->twitter; ?>"></a>
                            <a class="secoial_icon icon_youtube" target="_blank" href="<?php echo $info->youtube; ?>"></a>
                        </div>
                    </div>
                </div>

                <div class="menu_cate_footer">

                    <div class="cate_menu_khachhang">
                        <div class="title_cate_ft">
                            <p>HỖ TRỢ KHÁCH HÀNG</p>
                            <b></b>
                        </div>
                        <?php if(!empty($customer_support)){?>
                        <ul class="menu_footer_ul_kh">
                            <?php foreach($customer_support as $cs){
                                $name = $cs->name;
                                $link = site_url($cs->slug);
                                echo sprintf('<li><a href="%s" title="%s">%s</a></li>', $link, $name, $name);
                            }?>
                        </ul>
                        <?php } ?>


                    </div>

                    <div class="cate_menu_product_ft">
                        <div class="title_cate_ft">
                            <p>sản phẩm</p>
                            <b></b>
                        </div>
                        <?php if(!empty($category)){?>
                        <ul class="menu_footer_ul_sp">
                            <?php 
                                $i = 0;
                                foreach($category as $cate){ 
                                $name = $cate->name;
                                $link = site_url($cate->slug);
                                $class = (($i + 1) % 2 == 0) ? ' class="li_right"' : '';
                                echo sprintf('<li><a href="%s" title="%s">%s</a></li>', $link, $name, $name);
                                $i++;
                            }?>
                        </ul>
                        <?php } ?>

                    </div>

                    <div class="cate_menu_account_ft">
                        <div class="title_cate_ft">
                            <p>tài khoản</p>
                            <b></b>
                        </div>
                        <ul class="menu_footer_ul_kh">
                            <li><a href="<?php echo site_url('thong-tin-tai-khoan'); ?>">Tài khoản</a> </li>
                            <li><a href="<?php echo site_url('lich-su-don-hang'); ?>">Lịch sử đơn hàng</a> </li>
                            <li><a href="">Yêu thích</a> </li>
                            <li><a href="">Thư thông báo</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer1">
        <div class="footer">
            <p>Copyright © 2017 bodycare.vn - All rights reserved - Designed by <a href="http://iclick.vn/" target="_blank" rel="nofollow">iClick.vn</a> </p>
        </div>
    </div>
    <div class="clear"></div>
</footer>