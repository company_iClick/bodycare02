<?php 
    $CI=&get_instance();
    $rtr =& load_class('Router', 'core');
    $moduleName = $rtr->fetch_module();
    $className = $rtr->fetch_class();
    $methodName = $rtr->fetch_method();
    $CI->load->model('category/a_category');
    $list_category = $this->global_function->get_array(array(), 'id, parent_id, name, slug, color, font_class, level, banner_menu', 'category');
    $category_mega = $CI->a_category->recurse_one($list_category);
    if(!empty($category_mega)){
?>
<div class="wrapper_full_mb">
    <div class="wrapper_full_mb_1">
        <div class="wrapper_full_mb1">
            <div class="cate_home_mb"><p>Danh mục sản phẩm</p></div>
            <ul>
                <?php $i=1; foreach($category_mega as $c){
                    $name = $c->name;
                    $link = site_url($c->slug);
                    $font_class = (!empty($c->font_class)) ? 'icon-' . $c->font_class : 'icon-chamsoccothe';
                    $color = (!empty($c->color)) ? $c->color : '#666';
                    $child = $c->child;
                    $banner_menu = (!empty($c->banner_menu)) ? base_url(_upload_category . $c->banner_menu) : '';
                ?>
                <li><a href="<?php echo $link; ?>"><p class="icon_fonts_menu <?php echo $font_class; ?>" style="color:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>"></p><b><?php echo $name; ?></b></a>
                    <?php if(!empty($child)) { ?>

                    <i class="icon_arow_1 icon_arow_click icon_arow_click_<?=$i?>"></i>
                    <?php } ?>


<script type="text/javascript">
    
        $(document).ready(function(){
 $('.icon_arow_click_<?=$i?>').click(function(){


      $('.ul_menu_mb_child_<?=$i?>').toggle('slow');
  });

        })


</script>

                    <?php if(!empty($child)){ ?>
                    <ul class="ul_menu_mb_child ul_menu_mb_child_<?=$i?>">
                         <?php 
                            foreach($child as $ch){ 
                            $ch_name = $ch->name;
                            $ch_link = site_url($ch->slug);
                            $child_one = $ch->child;
                        ?>
                        <li><a href="<?php echo $ch_link; ?>"><?php echo $ch_name; ?></a>
                            <?php if(!empty($child_one)){?>
                            <ul class="ul_menu_mb_child1">
                                <?php 
                                    foreach($child_one as $ch_one){
                                    $ch_one_name = $ch_one->name;
                                    $ch_one_link = site_url($ch_one->slug); 
                                    echo sprintf(' <li><a href="%s" title="%s">%s</a></li>', $ch_one_link, $ch_one_name, $ch_one_name);
                                    }
                                ?>
                            </ul>
                            <?php } ?>
                        </li>
                        <?php $i++; } ?>
                    </ul>
                    <?php } ?>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<?php } ?>