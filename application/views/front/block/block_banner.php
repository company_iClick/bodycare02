<div id="block_banner">
	<div class="block_banner">
		<div id="wrapper_tab" class="tab1"> 
			<div class="tab_title clearfix">
				<ul>
					<li><a href="javascipt:;" class="tab1 tab_link">Khám phá ngay</a></li>
					<li><a href="javascipt:;" class="tab2 tab_link">Thương hiệu chính hãng</a></li>
					<li><a href="javascipt:;" class="tab3 tab_link">Mẹo hay - Làm đẹp</a></li>
					<li><a href="<?php echo site_url(); ?>" class="tab4 tab_link tab_redirect"><span><i class="icon-khuyen-mai"></i></span>Khuyến mãi hôm nay</a></li>
				</ul>
	
			</div>
			<div class="tab_content">
				<div class="tab1 tab_body">
					<?php if(!empty($discover_banner)){ ?>
					<ul class="discover_banner clearfix">
						<li>
							<a href="<?php echo $discover_banner[0]->link; ?>" target="_blank">
								<img src="<?php echo base_url() . $discover_banner[0]->name; ?>" alt="<?php echo $discover_banner[0]->title; ?>">
							</a>
						</li>
						<li>
							<?php if(isset($discover_banner[1])){ ?>
							<a href="<?php echo $discover_banner[1]->link; ?>" target="_blank">
								<img src="<?php echo base_url() . $discover_banner[1]->name; ?>" alt="<?php echo $discover_banner[1]->title; ?>">
							</a>
							<?php } ?>

							<?php if(isset($discover_banner[2])){ ?>
							<a href="<?php echo $discover_banner[2]->link; ?>" target="_blank">
								<img src="<?php echo base_url() . $discover_banner[2]->name; ?>" alt="<?php echo $discover_banner[2]->title; ?>">
							</a>
							<?php } ?>
						</li>
						<li>
							<?php if(isset($discover_banner[3])){ ?>
							<a href="<?php echo $discover_banner[3]->link; ?>" target="_blank">
								<img src="<?php echo base_url() . $discover_banner[3]->name; ?>" alt="<?php echo $discover_banner[3]->title; ?>">
							</a>
							<?php } ?>

							<?php if(isset($discover_banner[4])){ ?>
							<a href="<?php echo $discover_banner[4]->link; ?>" target="_blank">
								<img src="<?php echo base_url() . $discover_banner[4]->name; ?>" alt="<?php echo $discover_banner[4]->title; ?>">
							</a>
							<?php } ?>
						</li>
					</ul>
					<?php if(count($discover_banner) > 5){ ?>
					<ul class="discover_banner clearfix">
						<li>
							<div>
								<a href="<?php echo $discover_banner[5]->link; ?>" target="_blank">
									<img src="<?php echo base_url() . $discover_banner[5]->name; ?>" alt="<?php echo $discover_banner[5]->title; ?>">
								</a>
							</div>
							<div>
								<?php if(isset($discover_banner[6])){ ?>
								<a href="<?php echo $discover_banner[6]->link; ?>" target="_blank">
									<img src="<?php echo base_url() . $discover_banner[6]->name; ?>" alt="<?php echo $discover_banner[6]->title; ?>">
								</a>	
								<?php } ?>
							</div>
						</li>
						<li>
							<div>
								<?php if(isset($discover_banner[7])){ ?>
								<a href="<?php echo $discover_banner[7]->link; ?>" target="_blank">
									<img src="<?php echo base_url() . $discover_banner[7]->name; ?>" alt="<?php echo $discover_banner[7]->title; ?>">
								</a>
								<?php } ?>
							</div>
							<div>
								<?php if(isset($discover_banner[8])){ ?>
								<a href="<?php echo $discover_banner[8]->link; ?>" target="_blank">
									<img src="<?php echo base_url() . $discover_banner[8]->name; ?>" alt="<?php echo $discover_banner[8]->title; ?>">
								</a>
								<?php } ?>
							</div>
							
						</li>
						<li>
							<div>
								<?php if(isset($discover_banner[9])){ ?>
								<a href="<?php echo $discover_banner[9]->link; ?>" target="_blank">
									<img src="<?php echo base_url() . $discover_banner[9]->name; ?>" alt="<?php echo $discover_banner[9]->title; ?>">
								</a>
								<?php } ?>
							</div>
							<div>
								<?php if(isset($discover_banner[10])){ ?>
								<a href="<?php echo $discover_banner[10]->link; ?>" target="_blank">
									<img src="<?php echo base_url() . $discover_banner[10]->name; ?>" alt="<?php echo $discover_banner[10]->title; ?>">
								</a>
								<?php } ?>
							</div>
							
						</li>
					</ul>
					<?php } ?>
					<?php } ?>
				</div>
				<div class="tab2 tab_body">
					<div class="wrapper_full_bg_wrapper">
					    <div class="wrapper_full_1 clearfix">
					        <div class="lert_trademark">
					            <?php if(!empty($banner_brand)){
					                $banner = $banner_brand[0];
					                $link = $banner->link;
					                $img = base_url() . $banner->name;
					                $name = $banner->title;
					            ?>
					            <a href="<?php echo $link ?>" target="_blank"><img src="<?php echo $img; ?>" alt="<?php echo $name; ?>"></a>
					            <?php } ?>
					        </div>
					        <div class="right_trademark">
					            <div class="title_thuonghieu_mb_home">THƯƠNG HIỆU</div>
					            <?php if(!empty($brand)){
					                $i = 0;
					                foreach($brand as $br){
					                $name = $br->name;
					                $link = site_url($br->slug);
					                $photo = base_url() . _upload_brand . $br->photo;
					                $class = (($i + 1) % 4 == 0) ? ' last_a' : '';
					                $class2 = (($i + 1) % 2 == 0) ? ' trademark_a_2' : '';
					                $class3 = (($i + 1) % 3 == 0) ? ' trademark_a_3' : '';
					                echo sprintf('<a href="%s" title="%s" class="trademark_a%s%s%s"><img src="%s" alt="%s"></a>', $link, $name,$class2,$class3, $class, $photo, $name);
					                $i++;
					            }}?>
					        </div>
					    </div>
					</div>
				</div>
				<div class="tab3 tab_body">
					<?php if(!empty($good_tips)){ ?>
					<div id="good_tips" class="clearfix">
						<?php
			            $i = 0; 
			            foreach($good_tips as $a){
			            $name = $a->name;
			            $a_name = $this->my_lib->cut_string($name, 80);;
			            $link = site_url($a->slug);
			            $summary = $this->my_lib->cut_string($a->summary, 150);
			            $imgUrl = base_url(_upload_article . $a->folder . '/' . $a->photo);
			            $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=480&h=336&zc=1');
			            $date = date('d', strtotime($a->created_at));
			            $month_year = date('m/Y', strtotime($a->created_at));
			            $class = (($i + 1) % 2 == 0) ? ' child_news_home_2' : '';
			            echo sprintf('<div class="child_news_home">
			                            <div class="time_news">
			                                <p>%u</p>
			                                <p>%s</p>
			                            </div>
			                            <div class="img_news_home">
			                                <a href="%s" title="%s"><img src="%s" alt="%s"></a>
			                            </div>
			                            <div class="sum_name_news_home">
			                                <h3><a href="%s" title="%s">%s</a></h3>
			                                <p>%s</p>
			                            </div>
			                        </div>', $date, $month_year, $link, $name, $imgUrl, $name, $link, $name, $a_name, $summary);
			            $i++;
			        }?> 
					</div>
					<?php } ?>
				</div>
				<div class="tab4 tab_body">
					<?php if(!empty($new_sale)){ ?>
					<div id="today_sale_pr" class="clearfix">
					<?php 
                        foreach($new_sale as $pr){
                            $id = $pr->id;
                            $name = $pr->name;
                            $pr_name = $this->my_lib->cut_string($pr->name, 65);
                            $link = site_url($pr->slug);
                            $gift_type = ($pr->gift_type > 0) ? '<p class="pr_gift"><i class="icon-freegift"></i></p>' : '';
                            $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                            $percent = percen_calculate_v1($pr->price_sale, $pr->price, array('class' => 'price_percent'));
                            $price = cms_price_v1($pr->price_sale, $pr->price);

                            echo sprintf('<div class="box_product_v4">
                                            <div class="box_product_v4_img">
                                                <a href="%s" title="%s">
                                                    <img src="%s" alt="%s">
                                                </a>
                                            </div>
                                            
                                            <div class="box_product_v4_name">
                                                <a href="%s" title="%s">%s</a>
                                            </div>
                                            <div class="box_product_v4_price clearfix">
                                                <div class="box_product_v4_price_num">%s</div>
                                                <div class="box_product_v4_price_percent"><button class="btn_cart" onclick="add_to_cart(%s)"><span>Mua ngay</span><i class="icon-muahang"></i></button></div>
                                            </div>%s%s
                                        </div>', $link, $name, $photo, $name, $link, $name, $pr_name, $price, $id, $percent, $gift_type);
                        }
                        ?>	
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>