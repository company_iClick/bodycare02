<?php 
    $last_news = $this->global_function->get_array_object(array('type' => 3), 'id, name, slug, photo, folder, created_at', 'article', 5);
    $video = $this->global_function->get_array_object(array(), 'id, name, slug, photo', 'video', 5);

    $param = array(
        "select" => "images.name,images.link,images.title",
        "where" => array("images.status" => 1, "imagealbum.album_id" => 6),
        "order" => "images.id",
        "asc" => "DESC",
        "limit" => 10
    );

    $banner = $this->a_banner->show_list_image_album_where($param);
?>
<?php if(!empty($last_news)){ ?>
<div class="title_loc_search">Tin mới</div>
<ul class="ul_hot_news_1">
    <?php 
        foreach ($last_news as $row) {
            $name = $row->name;
            $link = site_url($row->slug);
            $imgUrl = base_url(_upload_article . $row->folder . '/' . $row->photo);
            $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=90&h=68&zc=1');
            echo sprintf('<li>
                            <a href="%s" title="%s" class="ing_news_hot"><img src="%s" alt="%s"></a>
                            <a href="%s" title="%s" class="name_news_hot">%s</a>

                        </li>', $link, $name, $imgUrl, $name, $link, $name, $name);
        }
    ?>
</ul>
<?php }?>

<?php if(!empty($banner)){
    foreach ($banner as $row) {
        $link = $row->link;
        $img = base_url() . $row->name;
        $name = $row->title;
        echo sprintf('<a href="%s" class="banner_news_left" target="_blank"><img src="%s", %s></a>', $link, $img, $name);
    }
}?>   

<?php if(!empty($video)){ ?>
<div class="title_loc_search">Video</div>
<ul class="ul_hot_news_1">
    <?php 
        foreach ($video as $row) {
            $name = $row->name;
            $link = site_url('video/' . $row->slug);
            $imgUrl = base_url(_upload_video . $row->photo);
            $imgUrl = base_url('timthumb.php?src='. $imgUrl .'&w=90&h=68&zc=1');
            echo sprintf('<li>
                            <a href="%s" title="%s" class="ing_news_hot"><img src="%s" alt="%s"><i></i></a>
                            <a href="%s" title="%s" class="name_news_hot">%s</a>

                        </li>', $link, $name, $imgUrl, $name, $link, $name, $name);
        }
    ?>
</ul>
<?php }?>
