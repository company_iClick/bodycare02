<?php 
    $CI=&get_instance();
    $rtr =& load_class('Router', 'core');
    $moduleName = $rtr->fetch_module();
    $className = $rtr->fetch_class();
    $methodName = $rtr->fetch_method();
?>
<div class="left_list_product">
    <div class="list_cate_1 background_1">

        <div class="title_loc_search">
            Bộ lọc tìm kiếm
        </div>
        <div class="name_cate_left">
            <?php if(isset($title_bar) && $methodName == 'category'){ ?><p><?php echo $title_bar; ?></p><?php }else{ ?><p>Danh mục sản phẩm</p><?php } ?>
            <b></b>
        </div>
        <?php if(isset($child) && !empty($child)){?>
        <ul class="ul_list_recieve_text">
            <?php foreach($child as $ch){
                $name = $ch->name;
                $link = site_url($ch->slug);
                $count_product = $ch->count_product;
                $class = ($ch->level == 2) ? 'c_child' : 'c_parent';
                if($count_product > 0)
                    echo sprintf('<li class="%s"><a href="%s" title="%s">%s<b>(%s)</b></a></li>', $class, $link, $name, $name, $count_product);
            }?>
        </ul>
        <?php } ?>
    </div>


    <div class="trademark_price_left">
        <div class="trademark_price_left_1">

        <?php if(isset($price_search) && !empty($price_search)){?>
            <div class="name_cate_left_1">
                <p>Giá</p>
                <b></b>
            </div>

            <ul class="ul_list_recieve_text_1">
                    <?php foreach($price_search as $ps){
                        $id = $ps->id;
                        $name = $ps->name;
                        $count_product = $ps->count_product;
                        $id_input = 'input_price_' . $id;
                    ?>
                    <li>
                        <input type="checkbox" name="input_price[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                        <b>(<?php echo $count_product; ?>)</b>
                    </li>
                    <?php } ?>
            </ul>
            <?php } ?>

            <?php if(isset($brand) && $count_brand > 0){?>
            <div class="list_th_scroll">
                <div class="name_cate_left_1 click_show_left">
                    <p>Thuơng hiệu</p>
                    <b></b>
                    <i></i>
                </div>

                <div class="list_th_scroll_1 mCustomScrollbar">
                    <ul class="list_th_scroll_ul ">
                       
                            <?php foreach($brand as $st){
                                $id = $st->id;
                                $name = $st->name;
                                $count_product = $st->count_product;
                                $id_input = 'input_brand_' . $id;
                                if($count_product > 0){
                            ?>
                            <li>
                                <input type="checkbox" name="input_brand[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                                <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                                <b>(<?php echo $count_product; ?>)</b>
                            </li>
                            <?php }} ?>
                        
                    </ul>
                </div>
            </div>
            <?php } ?>

            <?php if(isset($product_type) && $count_product_type > 0){?>
            <div class="name_cate_left_1">
                <p>Loại sản phẩm</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_product_type" id="input_product_type" class="css-checkbox-hotel">
                    <label for="input_product_type" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_product_type; ?>)</b>
                </li>
                <?php foreach($product_type as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_product_type_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_product_type" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>

            <?php }?>
            
            <?php if(isset($skin_type) && $count_skin_type > 0){?>
            <div class="name_cate_left_1">
                <p>Loại da</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_skin_type" id="input_skin_type" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                    <label for="input_skin_type" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_skin_type; ?>)</b>
                </li>
                <?php foreach($skin_type as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_skin_type_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_skin_type" id="<?php echo $id_input; ?>" class="css-checkbox-hotel">
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>
          
            <?php }?>

            <?php if(isset($hair_type) && $count_hair_type > 0){?>
            <div class="name_cate_left_1">
                <p>Loại tóc</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_hair_type" id="input_hair_type" class="css-checkbox-hotel">
                    <label for="input_hair_type" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_hair_type; ?>)</b>
                </li>
                <?php foreach($hair_type as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_hair_type_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_hair_type" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>          
            <?php }?>

            <?php if(isset($origin) && $count_origin > 0){?>
            <div class="name_cate_left_1">
                <p>Xuất xứ</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_origin" id="input_origin" class="css-checkbox-hotel">
                    <label for="input_origin" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_origin; ?>)</b>
                </li>
                <?php foreach($origin as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_origin_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_origin" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>         
            <?php }?>

            <?php if(isset($action) && $count_action > 0){?>
            <div class="name_cate_left_1">
                <p>Chức năng</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_action" id="input_action" class="css-checkbox-hotel">
                    <label for="input_action" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_action; ?>)</b>
                </li>
                <?php foreach($action as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_action_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_action" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()" value="<?php echo $id; ?>" onchange="product_filter()">
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>
            <?php }?>
            
            <?php if(isset($capacity) && $count_capacity > 0){?>
            <div class="name_cate_left_1 click_show_left ">
                <p>Dung tích</p>
                <b></b>
                <i></i>
            </div>
             <div class="list_th_scroll_1 mCustomScrollbar">
                <ul class="list_th_scroll_ul">
                    <li>
                        <input type="checkbox" name="input_capacity" id="input_capacity" class="css-checkbox-hotel">
                        <label for="input_capacity" class="css-label_hotel radGroup4">Tất cả</label>
                        <b>(<?php echo $count_capacity; ?>)</b>
                    </li>
                    <?php foreach($capacity as $st){
                        $id = $st->id;
                        $name = $st->name;
                        $count_product = $st->count_product;
                        $id_input = 'input_capacity_' . $id;
                        if($count_product > 0){
                    ?>
                    <li>
                        <input type="checkbox" name="input_capacity" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                        <b>(<?php echo $count_product; ?>)</b>
                    </li>
                    <?php }} ?>
                </ul>
            
            </div>
            <?php }?>
            <?php if(isset($weigh) && $count_weigh > 0){?>
            <div>
            <div class="name_cate_left_1 click_show_left">
                <p>Trọng lượng</p>
                <b></b>
                  <i></i>
            </div>
           <div class="list_th_scroll_1 mCustomScrollbar">
                <ul class="list_th_scroll_ul">
                    <li>
                        <input type="checkbox" name="input_weigh" id="input_weigh" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                        <label for="input_weigh" class="css-label_hotel radGroup4">Tất cả</label>
                        <b>(<?php echo $count_weigh; ?>)</b>
                    </li>
                    <?php foreach($weigh as $st){
                        $id = $st->id;
                        $name = $st->name;
                        $count_product = $st->count_product;
                        $id_input = 'input_weigh_' . $id;
                        if($count_product > 0){
                    ?>
                    <li>
                        <input type="checkbox" name="input_weigh" id="<?php echo $id_input; ?>" class="css-checkbox-hotel">
                        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                        <b>(<?php echo $count_product; ?>)</b>
                    </li>
                    <?php }} ?>
                </ul>
            
            </div>
            </div>
        <?php }?>

            <?php if(isset($pill_bumber) && !empty($pill_bumber)){?>
            <div class="name_cate_left_1">
                <p>Số lượng viên</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_pill_number" id="input_pill_number" class="css-checkbox-hotel" value="<?php echo $id; ?>" onchange="product_filter()">
                    <label for="input_pill_number" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_pill_bumber; ?>)</b>
                </li>
                <?php foreach($pill_bumber as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_pill_number_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="radiog_dark" id="<?php echo $id_input; ?>" class="css-checkbox-hotel">
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>
            <?php }?>
        </div>  
    </div>
</div>

  

    <script>
        (function($){
            $(window).on("load",function(){
                
                $("a[rel='load-content']").click(function(e){
                    e.preventDefault();
                    var url=$(this).attr("href");
                    $.get(url,function(data){
                        $(".list_th_scroll_1 .mCSB_container").append(data); //load new content inside .mCSB_container
                        //scroll-to appended content 
                        $(".list_th_scroll_1").mCustomScrollbar("scrollTo","h2:last");
                    });
                });
                
                $(".list_th_scroll_1").delegate("a[href='top']","click",function(e){
                    e.preventDefault();
                    $(".list_th_scroll_1").mCustomScrollbar("scrollTo",$(this).attr("href"));
                });
                
            });
        })(jQuery);
    </script>



<script type="text/javascript">
      $(document).ready(function() {

        $('.click_show_left').toggle(
            function() {
                 $(this).siblings('.list_th_scroll_1').slideUp("fast");  
                    $(this).children('i').addClass('a_arrow_123');
            },
            function() {
                $(this).siblings('.list_th_scroll_1').slideDown("fast");
                
                 $(this).children('i').removeClass('a_arrow_123');  
            }

           
        );

        
    });

    function product_filter(){
        
        //get price value
        var input_price_val = [];
        $('input[name="input_price[]"]:checked').each(function(i){
          input_price_val[i] = $(this).val();
        });

        //get brand value
        var input_brand_val = [];
        $('input[name="input_brand[]"]:checked').each(function(i){
          input_brand_val[i] = $(this).val();
        });
        
        alert(input_brand_val)
    }
</script>

















