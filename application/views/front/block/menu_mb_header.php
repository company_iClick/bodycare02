<?php 
    $CI=&get_instance();
    $rtr =& load_class('Router', 'core');
    $moduleName = $rtr->fetch_module();
    $className = $rtr->fetch_class();
    $methodName = $rtr->fetch_method();
    $session_login = $this->session->userdata("user_log");
?>

   <link rel="stylesheet" href="<?=base_url()?>themes/css/mobile-main.css?v=<?=time();?>">
    <script type="text/javascript">
        var LZD = window.LZD || {};
        LZD.experiments = {};
        LZD.venture = "vn"; // Possible values: my, vn, sg, ph, th, id
        LZD.lang = "vi";

        LZD.translation = LZD.translation || {};
        LZD.translation['in'] = 'trong';

        LZD.suggestApiUrl = "";

        LZD.csrfToken = {
            name: "FORM_TOKEN",
            value: "1cb6b5f7e58bce9e76e556cbaaf7adc73d6a2b88"
        };

        var JS_ERRORS_DISABLE = 1;

        if (typeof console === "undefined" || typeof console.log === "undefined") {
            console = {};
            console.log = function () {
            };
        }
    </script>


    <script type="text/javascript">
        if (!route) {
            var route = "";
        }
    </script>
    <script type="text/javascript">
       $(document).ready(function(){
        $('.container').click(function(){
        $('.container').removeAttr('style');
          });
       });
    </script>

    <script src="<?=base_url()?>themes/js/modernizr-custom.js"></script>

<div class="container" id="header_mobile">
    <div class="left-container">


        
      <div class="menu_mobi_click_add">
        <span class="mobile__menu__1__add"></span>
      </div>

        <ul class="category-list menu-list category-list--cms-menu">
           
             <li class="category-list__listItem category-list__listItem--cms-menu" style="display: none;">
                <a href="<?=site_url()?>"></a>
            </li>
            <li><a href="tel:090.77.999.88"><p class="icon_phone_mb" style="color:#b20505;border:1px solid #b20505"></p><b style="display: inline-block;margin-top: 5px">Hotline : <p>090.77.999.88</p></b></a></li>
            <?php if($session_login){ ?>
            <li><a href="<?=site_url('thong-tin-tai-khoan')?>"><p class="icon_dk_mb" style="color:#296525;border:1px solid #296525"></p><b style="display: inline-block;margin-top: 5px">Tài khoản</b></a></li>
            <li><a href="<?=site_url('dang-xuat')?>"><p class="icon_dx_mb" style="color:#296525;border:1px solid #296525"></p><b style="display: inline-block;margin-top: 5px">Thoát</b></a></li>
            <?php }else{ ?>
            <li><a href="<?=site_url('dang-ky')?>"><p class="icon_dk_mb" style="color:#296525;border:1px solid #296525"></p><b style="display: inline-block;margin-top: 5px">Đăng ký </b></a></li>

            <li><a href="<?=site_url('dang-nhap')?>"><p class="icon_dn_mb" style="color:#296525;border:1px solid #296525"></p><b style="display: inline-block;margin-top: 5px">Đăng nhập</b></a></li>
            <?php } ?>

            <li style="float: left;width: 100%;background: #e8eaee;height: 20px"></li>
           

            <li><a href="<?=site_url()?>"><p class="icon_home_mb" style="color:#296525;border:1px solid #296525"></p><b style="display: inline-block;margin-top: 5px">Trang chủ</b></a>
            </li>

            <li style="display: none;">
                <a href="<?=site_url()?>"> <p class="icon_home_mb"></p>Trang chủ</a>
            </li>
                <?php foreach($category_mega as $c){
                    $id = $c->id;
                    $name = $c->name;
                    $link = site_url($c->slug);
                    $font_class = (!empty($c->font_class)) ? 'icon-' . $c->font_class : 'icon-chamsoccothe';
                    $color = (!empty($c->color)) ? $c->color : '#666';
                    $child = $c->child;
                    $banner_menu = (!empty($c->banner_menu)) ? base_url(_upload_category . $c->banner_menu) : '';
                ?>
                <li><a href="<?php echo $link; ?>"><p class="icon_fonts_menu <?php echo $font_class; ?>" style="color:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>"></p><b><?php echo $name; ?></b></a><?php if(!empty($child)) echo '<i class="fa fa-plus toogle_cate" aria-hidden="true" data-id="'. $id .'"></i>'; ?>
                    <?php if(!empty($child)){?>
                    <ul id="cate_<?php echo $id; ?>">
                        <?php foreach ($child as $row) {
                        $c_id = $row->id;
                        $c_link = site_url($row->slug);
                        $c_name = $row->name;
                        $c_child = $row->child;
                        ?>
                        <li><a href="<?php echo $c_link; ?>"><?php echo $c_name; ?></a><?php if(!empty($c_child)) echo '<i class="fa fa-minus toogle_cc_cate" aria-hidden="true" data-id="'. $c_id .'"></i>'; ?>
                            <?php if(!empty($c_child)){?>
                            <ul id="cc_cate_<?php echo $c_id; ?>" class="cate_open">
                                <?php foreach ($c_child as $row) {
                                $cc_link = site_url($row->slug);
                                $cc_name = $row->name;
                                ?>
                                <li><a href="<?php echo $cc_link; ?>"><?php echo $cc_name; ?></a></li>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </li>
                        <?php } ?>
                    </ul>
                    <?php } ?>                   
                </li>

                <?php } ?>

        </ul>
        <div style="float: left;width: 100%;height: 200px"></div>
    </div>

    <div class="main-container">


        <header class="c-header-mobile<?php if($moduleName == 'product' && ((isset($sort_mobile) && $sort_mobile == 1) || (isset($filter_mobile) && $filter_mobile == 1))) echo ' mobile_filter_sort_mobile'; ?>">
            <?php if($moduleName != 'home'){?>
            <a href="javascript:;" onclick="goBack()" class="go_back"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
            <?php } ?>
            <span id="c-header-mobile__menu" onclick="hide_menu();" data="1" class="c-header-mobile__menu<?php if($moduleName != 'home') echo ' c-header-mobile__menu-inner' ?>">
                <span class="c-header-mobile__menu-icon"></span>
            </span>
            <span id="c-header-mobile__user-menu" class="c-header-mobile__user-menu ">
                <span class="c-header-mobile__user-menu-icon"></span>
            </span>
            <a href="<?=site_url('gio-hang')?>" id="c-header-mobile__cart" class="c-header-mobile__cart">
                <span id="c-header-mobile__cart-item" class="c-header-mobile__cart-item"><?php echo $this->cart->total_items() ?></span>
            </a>
            <a class="c-header-mobile__logo" href="">
                <span class="icon-bodycarevn"></span>
                <a class="phone___mb_1 icon-sodienthoai" href="tel:0907799988"><b>090.77.999.88</b></a>
            </a>
            <div id="filter_sort_mobile">
                <div class="filter_mobile">
                    <a href="javascript:;">
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        <span>Bộ lọc</span>
                    </a>  
                </div>
                <div class="sort_mobile">
                    <a href="javascript:;">
                        <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
                        <span>Sắp xếp</span>
                    </a>
                </div>
            </div>
        </header>
        <div id="frm_search_mobile">
            <form method="post" action="<?php echo site_url('s-search') ?>" class="clearfix">
                <input type="hidden" name="s_cate" value="0">
                <input type="text" name="keyword" value="<?php if($moduleName == 'product' && $methodName == 'search') echo $this->session->userdata('keyword'); ?>" placeholder="Tìm kiếm sản phẩm" required>
                <button class="submit" id="btn_search"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
            <?php if($moduleName == 'product' && $methodName == 'category'){ ?>
            <div class="select_search_cate"><input type="checkbox" name="s_cate_select" value="<?php echo $category->id; ?>"><span>Chỉ tìm trong <?php echo $category->name; ?></span></div>
            <?php } ?>
        </div>
        <div class="user-menu-popup" data-qa-locator="mob_user_menu_popup">
             <ul class="category-list menu-list">
                 <li class="li_left1"><a href="" onclick="return false">Chăm sóc</a></li>
                 <li class="li_left1"><a href="" onclick="return false">Làm đẹp</a></li>
                 <li class="li_left1"><a href="" onclick="return false">Trang điểm</a></li>
                 <li class="li_left1"><a href="" onclick="return false">Cho nam</a></li>
                 <li class="li_left1"><a href="" onclick="return false">Làm đẹp</a></li>
                 <li class="li_left1"><a href="" onclick="return false">Cho nữ</a></li>
                 <li class="li_left1"><a href="" onclick="return false">Thực phẩm</a></li>
                 <li class="li_left1"><a href="<?=site_url('kien-thuc-lam-dep')?>">Kiến thức</a></li>
                 <li class="li_left1"><a href="<?=site_url('thuong-hieu')?>"> Thương hiệu</a></li>
                 <li class="li_left1"><a href="<?=site_url('khuyen-mai')?>">Khuyến mãi</a></li>
            </ul>
        </div>
  </div>
</div>