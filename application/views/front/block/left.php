<?php 
    $CI=&get_instance();
    $rtr =& load_class('Router', 'core');
    $moduleName = $rtr->fetch_module();
    $className = $rtr->fetch_class();
    $methodName = $rtr->fetch_method();
    $mod_id = (isset($mod_id)) ? $mod_id : 0;
    switch ($methodName) {
        case 'search':
            $cate_id_search = $this->uri->segment(3);
            break;

        case 'product_type_category':
        case 'skin_type_category':
        case 'hair_type_category':
        case 'origin_category':
        case 'action_category':
        case 'capacity_category':
        case 'pill_number_category':
        case 'weigh_category':
        case 'tag_category':
            $cate_id_search = $category_id;
            break;
        
        default:
            $cate_id_search = '';
            break;
    }

    $keyword = ($methodName == 'search') ? $this->uri->segment(5) : '';

    switch ($methodName) {
        case 'tags':
            $route = site_url('tags/' . $this->uri->segment(2));
            break;

        case 'tag_category':
            $route = site_url('tags/' . $this->uri->segment(2) . '/' . $this->uri->segment(3));
            break;

        case 'search':
            $route = site_url('search/cat/' . $cat . '/q/' . $keyword);
            break;

        case 'product_type_category':
        case 'skin_type_category':
        case 'hair_type_category':
        case 'origin_category':
        case 'action_category':
        case 'capacity_category':
        case 'pill_number_category':
        case 'weigh_category':
            $route = site_url( $this->uri->segment(1) . '/' . $this->uri->segment(2));
            break;
        
        default:
            $route = site_url($this->uri->segment(1));
            break;
    }
   
    $page = ($this->input->get('page') != '') ? $this->input->get('page') : 1;

    $price_param = array();
    if($this->input->get('price') != ''){
        $price_param = (is_numeric($this->input->get('price'))) ? array($this->input->get('price')) : explode(',', $this->input->get('price')); 
    }

    //get brand param
    $brand_param = array();
    if($this->input->get('brand') != ''){
        $brand_param = (is_numeric($this->input->get('brand'))) ? array($this->input->get('brand')) : explode(',', $this->input->get('brand'));
    }

    //get product_type param
    $product_type_param = array();
    if($this->input->get('product_type') != ''){
        $product_type_param = (is_numeric($this->input->get('product_type'))) ? array($this->input->get('product_type')) : explode(',', $this->input->get('product_type'));
    }

    //get skin_type param
    $skin_type_param = array();
    if($this->input->get('skin_type') != ''){
        $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? array($this->input->get('skin_type')) : explode(',', $this->input->get('skin_type'));
    }

    //get hair_type param
    $hair_type_param = array();
    if($this->input->get('hair_type') != ''){
        $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? array($this->input->get('hair_type')) : explode(',', $this->input->get('hair_type'));
    }

    //get origin param
    $origin_param = array();
    if($this->input->get('origin') != ''){
        $origin_param = (is_numeric($this->input->get('origin'))) ? array($this->input->get('origin')) : explode(',', $this->input->get('origin'));
    }
    //get origin param
    $action_param = array();
    if($this->input->get('function') != ''){
        $action_param = (is_numeric($this->input->get('function'))) ? array($this->input->get('function')) : explode(',', $this->input->get('function'));
    }

    //get capacity param
    $capacity_param = array();
    if($this->input->get('capacity') != ''){
        $capacity_param = (is_numeric($this->input->get('capacity'))) ? array($this->input->get('capacity')) : explode(',', $this->input->get('capacity'));
    }

    //get weigh param
    $weigh_param = array();
    if($this->input->get('weigh') != ''){
        $weigh_param = (is_numeric($this->input->get('weigh'))) ? array($this->input->get('weigh')) : explode(',', $this->input->get('weigh'));
    }

    //get pill_number param
    $pill_number_param = array();
    if($this->input->get('pill_number') != ''){
        $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? array($this->input->get('pill_number')) : explode(',', $this->input->get('pill_number'));
    }

?>
<div class="left_list_product">
    <div class="list_cate_1 background_1">

        <div class="title_loc_search">
            Bộ lọc tìm kiếm
        </div>

        <?php if(isset($other_product_type) && $count_other_product_type > 0){?>
        <div class="name_cate_left">
            <p>Loại sản phẩm khác</p>
            <b></b>
        </div>

        <ul class="ul_list_recieve_text">
            <?php foreach($other_product_type as $row){
                $id = $row->id;
                $name = $row->name;
                $link = site_url($row->slug);
                $count_product = $row->count_product;
                if($count_product > 0){
            ?>
         
            <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?><b>(<?php echo $count_product; ?>)</b></a></li>
       
            <?php }} ?>
        </ul>
        <?php } ?>

        <?php if(isset($other_skin_type) && $count_other_skin_type > 0){?>
        <div class="name_cate_left">
            <p>Loại da khác</p>
            <b></b>
        </div>

        <ul class="ul_list_recieve_text">
            <?php foreach($other_skin_type as $row){
                $id = $row->id;
                $name = $row->name;
                $link = site_url($row->slug);
                $count_product = $row->count_product;
                if($count_product > 0){
            ?>
         
            <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?><b>(<?php echo $count_product; ?>)</b></a></li>
       
            <?php }} ?>
        </ul>
        <?php } ?>

        <?php if(isset($other_hair_type) && !empty($other_hair_type)){?>
        <div class="name_cate_left">
            <p>Loại tóc khác</p>
            <b></b>
        </div>

        <ul class="ul_list_recieve_text">
            <?php foreach($other_hair_type as $row){
                $id = $row->id;
                $name = $row->name;
                $link = site_url($row->slug);
                $count_product = $row->count_product;
                if($count_product > 0){
            ?>
         
            <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?><b>(<?php echo $count_product; ?>)</b></a></li>
       
            <?php }} ?>
        </ul>
        <?php } ?>

        <?php if(isset($other_action) && $count_other_action > 0){?>
        <div class="name_cate_left">
            <p>Chức năng khác</p>
            <b></b>
        </div>

        <ul class="ul_list_recieve_text">
            <?php foreach($other_action as $row){
                $id = $row->id;
                $name = $row->name;
                $link = site_url($row->slug);
                $count_product = $row->count_product;
                if($count_product > 0){
            ?>
         
            <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?><b>(<?php echo $count_product; ?>)</b></a></li>
       
            <?php }} ?>
        </ul>
        <?php } ?>

        <?php if(isset($other_origin) && $count_other_origin > 0){?>
        <div class="name_cate_left">
            <p>Xuất xứ khác</p>
            <b></b>
        </div>

        <ul class="ul_list_recieve_text">
            <?php foreach($other_origin as $row){
                $id = $row->id;
                $name = $row->name;
                $link = site_url($row->slug);
                $count_product = $row->count_product;
                if($count_product > 0){
            ?>
         
            <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?><b>(<?php echo $count_product; ?>)</b></a></li>
       
            <?php }} ?>
        </ul>
        <?php } ?>

        <?php if(isset($other_capacity) && $count_other_capacity > 0){?>
        <div>
            <div class="name_cate_left">
                <p>Dung tích khác</p>
                <b></b>
            </div>
            <div class="list_th_scroll_1 mCustomScrollbar">
                <ul class="list_th_scroll_ul">
                    <?php foreach($other_capacity as $row){
                        $id = $row->id;
                        $name = $row->name;
                        $link = site_url($row->slug);
                        $count_product = $row->count_product;
                        if($count_product > 0){
                    ?>
                 
                    <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?><b>(<?php echo $count_product; ?>)</b></a></li>
               
                    <?php }} ?>
                </ul>
            </div>
        </div>
        <?php } ?>

        <?php if(isset($other_weigh) && $count_other_weigh > 0){?>
        <div>
            <div class="name_cate_left">
                <p>Trọng lượng khác</p>
                <b></b>
            </div>
            <div class="list_th_scroll_1 mCustomScrollbar">
                <ul class="list_th_scroll_ul">
                    <?php foreach($other_weigh as $row){
                        $id = $row->id;
                        $name = $row->name;
                        $link = site_url($row->slug);
                        $count_product = $row->count_product;
                        if($count_product > 0){
                    ?>
                 
                    <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?><b>(<?php echo $count_product; ?>)</b></a></li>
               
                    <?php }} ?>
                </ul>
            </div>
        </div>
        <?php } ?>

        <?php if(isset($other_pill_number) && $count_other_pill_number > 0){?>
        <div class="name_cate_left">
            <p>Số lượng viên khác</p>
            <b></b>
        </div>

        <ul class="ul_list_recieve_text">
            <?php foreach($other_pill_number as $row){
                $id = $row->id;
                $name = $row->name;
                $link = site_url($row->slug);
                $count_product = $row->count_product;
                if($count_product > 0){
            ?>
         
            <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?><b>(<?php echo $count_product; ?>)</b></a></li>
       
            <?php }} ?>
        </ul>
        <?php } ?>

        <div class="name_cate_left">
            <?php if(isset($title_cate) && $methodName == 'category'){ ?><p><?php echo $title_cate; ?></p><?php }else{ ?><p>Danh mục sản phẩm</p><?php } ?>
            <b></b>
        </div>
        <?php if(isset($child) && !empty($child)){?>
        <ul class="ul_list_recieve_text">
            <?php foreach($child as $ch){
                $id = $ch->id;
                $name = $ch->name;
                $link = site_url($ch->slug);
                $count_product = $ch->count_product;
                $class = 'c_node';
                if ($ch->level == 1) $class = 'c_parent';
                if ($ch->level == 2) $class = 'c_child';
                if(isset($category) && $id == $category->id) $class .= ' current';
                if($count_product > 0)
                    echo sprintf('<li class="%s"><a href="%s" title="%s">%s<b>(%s)</b></a></li>', $class, $link, $name, $name, $count_product);
            }?>
        </ul>
        <?php } ?>
    </div>


    <div class="trademark_price_left">
        <div class="trademark_price_left_1" id="product_filter_element">

            <?php if(isset($price_search) && !empty($price_search)){?>
            <div class="name_cate_left_1">
                <p>Giá</p>
                <b></b>
            </div>

            <ul class="ul_list_recieve_text_1">
                <?php foreach($price_search as $ps){
                    $id = $ps->id;
                    $name = $ps->name;
                    $count_product = $ps->count_product;
                    $id_input = 'input_price_' . $id;
                ?>
                <li>
                    <input type="checkbox" name="input_price[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>" onchange="product_filter()"<?php if(in_array($id, $price_param)) echo ' checked'; ?>>
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php } ?>
            </ul>
            <?php } ?>

            <?php if(isset($brand) && $count_brand > 0){?>
            <div class="list_th_scroll">
                <div class="name_cate_left_1 click_show_left">
                    <p>Thuơng hiệu</p>
                    <b></b>
                    <i></i>
                </div>

                <div class="list_th_scroll_1 mCustomScrollbar">
                    <ul class="list_th_scroll_ul "> 
                        <?php foreach($brand as $st){
                            $id = $st->id;
                            $name = $st->name;
                            $count_product = $st->count_product;
                            $id_input = 'input_brand_' . $id;
                            if($count_product > 0){
                        ?>
                        <li>
                            <input type="checkbox" name="input_brand[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>" onchange="product_filter()"<?php if(in_array($id, $brand_param)) echo ' checked'; ?>>
                            <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                            <b>(<?php echo $count_product; ?>)</b>
                        </li>
                        <?php }} ?>                        
                    </ul>
                </div>
            </div>
            <?php } ?>

            <?php if(isset($product_type) && $count_product_type > 0){?>
            <div class="name_cate_left_1">
                <p>Loại sản phẩm</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_product_type" id="input_product_type" class="css-checkbox-hotel" value="<?php echo $all_product_type_id; ?>" data-name="Tất cả" onchange="product_filter_all_product_type()"<?php if($all_product_type_id == $this->input->get('product_type')) echo ' checked'; ?>>
                    <label for="input_product_type" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_product_type; ?>)</b>
                </li>
                <?php foreach($product_type as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_product_type_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_product_type[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>" onchange="product_filter()"<?php if(in_array($id, $product_type_param)) echo ' checked'; ?>>
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>

            <?php }?>
            
            <?php if(isset($skin_type) && $count_skin_type > 0){?>
            <div class="name_cate_left_1">
                <p>Loại da</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_skin_type" id="input_skin_type" class="css-checkbox-hotel" value="<?php echo $all_skin_type_id; ?>" data-name="Tất cả" onchange="product_filter_all_skin_type()"<?php if($all_skin_type_id == $this->input->get('skin_type')) echo ' checked'; ?>>
                    <label for="input_skin_type" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_skin_type; ?>)</b>
                </li>
                <?php foreach($skin_type as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_skin_type_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_skin_type[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" data-name="<?php echo $name; ?>" value="<?php echo $id; ?>" onchange="product_filter()"<?php if(in_array($id, $skin_type_param)) echo ' checked'; ?>>
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>
          
            <?php }?>

            <?php if(isset($hair_type) && $count_hair_type > 0){?>
            <div class="name_cate_left_1">
                <p>Loại tóc</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_hair_type" id="input_hair_type" class="css-checkbox-hotel" value="<?php echo $all_hair_type_id; ?>" data-name="Tất cả" onchange="product_filter_all_hair_type()"<?php if($all_hair_type_id == $this->input->get('hair_type')) echo ' checked'; ?>>
                    <label for="input_hair_type" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_hair_type; ?>)</b>
                </li>
                <?php foreach($hair_type as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_hair_type_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_hair_type[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(in_array($id, $hair_type_param)) echo ' checked'; ?>>
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>          
            <?php }?>

            <?php if(isset($origin) && $count_origin > 0){?>
            <div class="name_cate_left_1">
                <p>Xuất xứ</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_origin" id="input_origin" class="css-checkbox-hotel" value="<?php echo $all_origin_id; ?>" data-name="Tất cả" onchange="product_filter_all_origin()"<?php if($all_origin_id == $this->input->get('origin')) echo ' checked'; ?>>
                    <label for="input_origin" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_origin; ?>)</b>
                </li>
                <?php foreach($origin as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_origin_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_origin[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(in_array($id, $origin_param)) echo ' checked'; ?>>
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>         
            <?php }?>

            <?php if(isset($action) && $count_action > 0){?>
            <div class="name_cate_left_1">
                <p>Chức năng</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_action" id="input_action" class="css-checkbox-hotel" value="<?php echo $all_action_id; ?>" data-name="Tất cả" onchange="product_filter_all_action()"<?php if($all_action_id == $this->input->get('function')) echo ' checked'; ?>>
                    <label for="input_action" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_action; ?>)</b>
                </li>
                <?php foreach($action as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_action_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_action[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(in_array($id, $action_param)) echo ' checked'; ?>>
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>
            <?php }?>
            
            <?php if(isset($capacity) && $count_capacity > 0){?>
            <div class="name_cate_left_1 click_show_left ">
                <p>Dung tích</p>
                <b></b>
                <i></i>
            </div>
             <div class="list_th_scroll_1 mCustomScrollbar">
                <ul class="list_th_scroll_ul">
                    <li>
                        <input type="checkbox" name="input_capacity" id="input_capacity" class="css-checkbox-hotel" value="<?php echo $all_capacity_id; ?>" data-name="Tất cả" onchange="product_filter_all_capacity()"<?php if($all_capacity_id == $this->input->get('capacity')) echo ' checked'; ?>>
                        <label for="input_capacity" class="css-label_hotel radGroup4">Tất cả</label>
                        <b>(<?php echo $count_capacity; ?>)</b>
                    </li>
                    <?php foreach($capacity as $st){
                        $id = $st->id;
                        $name = $st->name;
                        $count_product = $st->count_product;
                        $id_input = 'input_capacity_' . $id;
                        if($count_product > 0){
                    ?>
                    <li>
                        <input type="checkbox" name="input_capacity[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(in_array($id, $capacity_param)) echo ' checked'; ?>>
                        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                        <b>(<?php echo $count_product; ?>)</b>
                    </li>
                    <?php }} ?>
                </ul>
            
            </div>
            <?php }?>
            <?php if(isset($weigh) && $count_weigh > 0){?>
            <div>
            <div class="name_cate_left_1 click_show_left">
                <p>Trọng lượng</p>
                <b></b>
                  <i></i>
            </div>
           <div class="list_th_scroll_1 mCustomScrollbar">
                <ul class="list_th_scroll_ul">
                    <li>
                        <input type="checkbox" name="input_weigh" id="input_weigh" class="css-checkbox-hotel" value="<?php echo $all_weigh_id; ?>" data-name="Tất cả" onchange="product_filter_all_weigh()"<?php if($all_weigh_id == $this->input->get('weigh')) echo ' checked'; ?>>
                        <label for="input_weigh" class="css-label_hotel radGroup4">Tất cả</label>
                        <b>(<?php echo $count_weigh; ?>)</b>
                    </li>
                    <?php foreach($weigh as $st){
                        $id = $st->id;
                        $name = $st->name;
                        $count_product = $st->count_product;
                        $id_input = 'input_weigh_' . $id;
                        if($count_product > 0){
                    ?>
                    <li>
                        <input type="checkbox" name="input_weigh[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(in_array($id, $weigh_param)) echo ' checked'; ?>>
                        <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                        <b>(<?php echo $count_product; ?>)</b>
                    </li>
                    <?php }} ?>
                </ul>
            
            </div>
            </div>
        <?php }?>

            <?php if(isset($pill_number) && $count_pill_number > 0){?>
            <div class="name_cate_left_1">
                <p>Số lượng viên</p>
                <b></b>
            </div>
            <ul class="ul_list_recieve_text_1">
                <li>
                    <input type="checkbox" name="input_pill_number" id="input_pill_number" class="css-checkbox-hotel" value="<?php echo $all_pill_number_id; ?>" data-name="Tất cả" onchange="product_filter_all_pill_number()"<?php if($all_pill_number_id == $this->input->get('pill_number')) echo ' checked'; ?>>
                    <label for="input_pill_number" class="css-label_hotel radGroup4">Tất cả</label>
                    <b>(<?php echo $count_pill_number; ?>)</b>
                </li>
                <?php foreach($pill_number as $st){
                    $id = $st->id;
                    $name = $st->name;
                    $count_product = $st->count_product;
                    $id_input = 'input_pill_number_' . $id;
                    if($count_product > 0){
                ?>
                <li>
                    <input type="checkbox" name="input_pill_number[]" id="<?php echo $id_input; ?>" class="css-checkbox-hotel" value="<?php echo $id; ?>" data-name="<?php echo $name; ?>" onchange="product_filter()"<?php if(in_array($id, $pill_number_param)) echo ' checked'; ?>>
                    <label for="<?php echo $id_input; ?>" class="css-label_hotel radGroup4"><?php echo $name; ?></label>
                    <b>(<?php echo $count_product; ?>)</b>
                </li>
                <?php }} ?>
            </ul>
            <?php }?>
        </div>  
    </div>
    <input type="hidden" name="page" value="<?php echo $page; ?>">
    <input type="hidden" name="mod" value="<?php echo $methodName; ?>">
    <input type="hidden" name="mod_id" value="<?php echo $mod_id; ?>">
    <input type="hidden" name="cate_id_search" value="<?php echo $cate_id_search; ?>">
    <input type="hidden" name="keyword" value="<?php echo $keyword; ?>">
    <input type="hidden" name="route" value="<?php echo $route; ?>">
    <input type="hidden" name="layout" value="<?php echo $layout ?>">
</div>