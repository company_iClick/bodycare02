<section id="content" class="clearfix">
    <div class="box_item" id="item_one">
        <a href="<?php echo site_url('giai-phap-va-san-pham-chieu-sang'); ?>" title="Giải pháp và sản phẩm chiếu sáng">
            <span class="text_link">Giải pháp và sản phẩm chiếu sáng</span>
        </a>
    </div>
    <div class="box_item" id="item_two">
        <a href="<?php echo site_url('hang-cong-nghe'); ?>" title="Hàng công nghệ">
            <span class="text_link">
                Hàng công nghệ
            </span>
        </a>
    </div>
    <div class="box_item" id="item_three">
        <a href="<?php echo site_url('dich-vu-cho-thue-van-phong'); ?>" title="Dịch vụ cho thuê văn phòng">
            <span class="text_link">
                Dịch vụ cho thuê văn phòng
            </span>
        </a>
    </div>
    <div class="box_item" id="item_four">
        <a href="<?php echo site_url('cac-linh-vuc-khac'); ?>" title="Các lĩnh vực khác">
            <span class="text_link">
                Các lĩnh vực khác
            </span>
        </a>
    </div>
    <div class="box_item" id="item_five">
        <a href="<?php echo site_url('gioi-thieu'); ?>" title="Giới thiệu">
            <span class="text_link">
                Giới thiệu
            </span>
        </a>
    </div>
    <div class="box_item" id="item_six">
        <a href="<?php echo site_url('bao-cao'); ?>" title="Báo cáo">
            <span class="text_link">
                Báo cáo
            </span>
        </a>
    </div>
    <div class="box_item" id="item_seven">
        <a href="<?php echo site_url('tuyen-dung'); ?>"" title="Tuyển dụng">
            <span class="text_link">
                Tuyển dụng
            </span>
        </a>
    </div>
    <div class="box_item" id="item_eight">
        <a href="<?php echo site_url('lien-he'); ?>" title="Liên hệ">
            <span class="text_link">
                Liên hệ
            </span>
        </a>
    </div>
</section>