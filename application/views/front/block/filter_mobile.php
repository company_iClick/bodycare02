<?php 
    $price_param = array();
    if($this->input->get('price') != ''){
        $price_param = (is_numeric($this->input->get('price'))) ? array($this->input->get('price')) : explode(',', $this->input->get('price')); 
    }

    //get brand param
    $brand_param = array();
    if($this->input->get('brand') != ''){
        $brand_param = (is_numeric($this->input->get('brand'))) ? array($this->input->get('brand')) : explode(',', $this->input->get('brand'));
    }

    //get product_type param
    $product_type_param = array();
    if($this->input->get('product_type') != ''){
        $product_type_param = (is_numeric($this->input->get('product_type'))) ? array($this->input->get('product_type')) : explode(',', $this->input->get('product_type'));
    }

    //get skin_type param
    $skin_type_param = array();
    if($this->input->get('skin_type') != ''){
        $skin_type_param = (is_numeric($this->input->get('skin_type'))) ? array($this->input->get('skin_type')) : explode(',', $this->input->get('skin_type'));
    }

    //get hair_type param
    $hair_type_param = array();
    if($this->input->get('hair_type') != ''){
        $hair_type_param = (is_numeric($this->input->get('hair_type'))) ? array($this->input->get('hair_type')) : explode(',', $this->input->get('hair_type'));
    }

    //get origin param
    $origin_param = array();
    if($this->input->get('origin') != ''){
        $origin_param = (is_numeric($this->input->get('origin'))) ? array($this->input->get('origin')) : explode(',', $this->input->get('origin'));
    }
    //get origin param
    $action_param = array();
    if($this->input->get('function') != ''){
        $action_param = (is_numeric($this->input->get('function'))) ? array($this->input->get('function')) : explode(',', $this->input->get('function'));
    }

    //get capacity param
    $capacity_param = array();
    if($this->input->get('capacity') != ''){
        $capacity_param = (is_numeric($this->input->get('capacity'))) ? array($this->input->get('capacity')) : explode(',', $this->input->get('capacity'));
    }

    //get weigh param
    $weigh_param = array();
    if($this->input->get('weigh') != ''){
        $weigh_param = (is_numeric($this->input->get('weigh'))) ? array($this->input->get('weigh')) : explode(',', $this->input->get('weigh'));
    }

    //get pill_number param
    $pill_number_param = array();
    if($this->input->get('pill_number') != ''){
        $pill_number_param = (is_numeric($this->input->get('pill_number'))) ? array($this->input->get('pill_number')) : explode(',', $this->input->get('pill_number'));
    }
?>
<div class="context-layout" id="filter-menu">
    <div class="nav">
        <a class="context-back" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h2>Bộ lọc</h2>
    </div>
    <div class="wrap">
        <div class="card">
            <div class="card-menu">
                <div class="list-group">
                    <?php if(isset($price_search) && !empty($price_search)){?>
                    <div class="list-group-item" data-group="price">
                        <a href="#panelPrice" aria-expanded="true" class="group_toogle">
                            <div class="text-uppercase">Giá</div>
                            <span class="filter_select" id="filter_select_price"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelPrice" class="card-menu card-sub-menu collapse in">
                            <div class="list-group">
                                <?php foreach($price_search as $ps){
                                    $id = $ps->id;
                                    $name = $ps->name;
                                    $count_product = $ps->count_product;
                                ?>
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_price" data-group="price" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $price_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                   
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($action) && $count_action > 0){?>
                    <div class="list-group-item" data-group="action">
                        <a href="#panelAction" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Chức năng</div>
                            <span class="filter_select" id="filter_select_action"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelAction" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($action as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_action" data-group="action" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $action_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($origin) && $count_origin > 0){?>
                    <div class="list-group-item" data-group="origin">
                        <a href="#panelOrigin" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Xuất xứ</div>
                            <span class="filter_select" id="filter_select_origin"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelOrigin" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($origin as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_origin" data-group="origin" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $origin_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($brand) && $count_brand > 0){?>
                    <div class="list-group-item" data-group="brand">
                        <a href="#panelBrand" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Thương hiệu</div>
                            <span class="filter_select" id="filter_select_brand"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelBrand" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($brand as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;  
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_brand" data-group="brand" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $brand_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($product_type) && $count_product_type > 0){?>
                    <div class="list-group-item" data-group="product_type">
                        <a href="#panelProductType" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Loại sản phẩm</div>
                            <span class="filter_select" id="filter_select_product_type"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelProductType" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($product_type as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_product_type" data-group="product_type" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $product_type_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($skin_type) && $count_skin_type > 0){?>
                    <div class="list-group-item" data-group="skin_type">
                        <a href="#panelSkinType" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Loại da</div>
                            <span class="filter_select" id="filter_select_skin_type"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelSkinType" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($skin_type as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_skin_type" data-group="skin_type" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $skin_type_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($hair_type) && $count_hair_type > 0){?>
                    <div class="list-group-item" data-group="hair_type">
                        <a href="#panelHairType" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Loại tóc</div>
                            <span class="filter_select" id="filter_select_hair_type"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelHairType" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($hair_type as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_hair_type" data-group="hair_type" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $hair_type_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($capacity) && $count_skin_type > 0){?>
                    <div class="list-group-item" data-group="capacity">
                        <a href="#panelCapacity" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Dung tích</div>
                            <span class="filter_select" id="filter_select_capacity"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelCapacity" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($capacity as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_capacity" data-group="capacity" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $capacity_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($weigh) && $count_weigh > 0){?>
                    <div class="list-group-item" data-group="weigh">
                        <a href="#panelWeigh" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Trọng lượng</div>
                            <span class="filter_select" id="filter_select_weigh"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelWeigh" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($weigh as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_weigh" data-group="weigh" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $weigh_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if(isset($pill_number) && $count_pill_number > 0){?>
                    <div class="list-group-item" data-group="pill_number">
                        <a href="#panelPillNumber" aria-expanded="false" class="group_toogle">
                            <div class="text-uppercase">Số lượng viên</div>
                            <span class="filter_select" id="filter_select_pill_number"></span>
                            <i class="fa fa-angle-down chevron" aria-hidden="true"></i>
                        </a>
                        <div id="panelPillNumber" class="card-menu card-sub-menu collapse">
                            <div class="list-group">
                                <?php foreach($pill_number as $st){
                                    $id = $st->id;
                                    $name = $st->name;
                                    $count_product = $st->count_product;
                                ?>                                   
                                <div class="list-group-item">
                                    <a href="#" data-value="<?php echo $id; ?>" data-label="<?php echo $name; ?>" data-id="#filter_select_pill_number" data-group="pill_number" class="select_filter_condition">
                                        <span class="text-nowrap"><?php echo $name; ?></span>
                                        <?php if(in_array($id, $pill_number_param)) echo '<i class="fa fa-check-square" aria-hidden="true"></i>';else echo '<i class="fa fa-square-o" aria-hidden="true"></i>'; ?>
                                    </a>
                                </div>   
                                <?php } ?>                                     
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky">
        <div class="left">
            <a class="btn btn-block btn-blue filter-apply" href="#">Áp dụng</a>
        </div>
        <div class="right">
            <a class="btn btn-block btn-normal filter-clear" href="#">Hủy</a>
        </div>
    </div>
</div>