<?php
$random=$this->global_function->show_list_table_where(array("status"=>1,"type"=>3,"id >"=>4),"article","name,slug","id","ASC",50);

?>
<div class="col_full fleft">
    <div  class="col12 m_auto">
        <ul  class="marquee" id="mycrawler2">
            <?php foreach($random as $rand){?>
                <li style="display: inline-block; margin-right: 10px"><a style="color: #000; font-size: 18px" href="<?php echo site_url($rand->slug)?>"><?php echo $rand->name?></a></li>

            <?php }?>
        </ul>
    </div>
</div>
<script type="text/javascript">
    marqueeInit({
        uniqueid: 'mycrawler2',
        style: {
            'padding': '2px',
            'width': '1000px',
            'height': '20px'
        },
        inc: 5, //speed - pixel increment for each iteration of this marquee's movement
        mouse: 'cursor driven', //mouseover behavior ('pause' 'cursor driven' or false)
        moveatleast: 2,
        neutral: 150,
        savedirection: true,
        random: true
    });
</script>
