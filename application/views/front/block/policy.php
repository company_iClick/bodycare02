<?php 
  $CI=&get_instance();
  $rtr =& load_class('Router', 'core');
  $moduleName = $rtr->fetch_module();
  $className = $rtr->fetch_class();
  $methodName = $rtr->fetch_method();
?>
<div class="wrapper_full<?php if($moduleName != 'home') echo ' wrapper_full_blue'; ?>">
  <div class="wrapper_full_1">
        <div class="banner_home1">
                        
            <div class="child_tichluy">
              <div class="icon_policy">
                  <i class="icon-tichluydiemthuong"></i>
              </div>
              <span>
                  <a href="">tích lũy điểm thưởng</a>
                  <p>Tích điểm khi đăng kí thành viên, mua hàng & viết đánh giá</p>
              </span>  
                
            </div>
              <div class="child_tichluy">
                <div class="icon_policy">
                    <i class="icon-freeship"></i>
                </div>
                <span>
                    <a href="">free ship toàn quốc</a>
                    <p>Áp dụng cho đơn hàng trên 500.000 vnđ</p>
                </span>    
            </div>
              <div class="child_tichluy">
                <div class="icon_policy">
                    <i class="icon-nhieuquatang"></i>
                </div>
                <span>
                    <a href="">nhiều quà tặng hấp dẫn</a>
                    <p>Xem quà tặng kèm trong chi tiết của mỗi sản phẩm</p>
                </span>         
            </div>
              <div class="child_tichluy child_tichluy_last">
                <div class="icon_policy">
                    <i class="icon-chatluongvauytin"></i>
                </div>
                <span>
                    <a href="">Chất lượng và uy tín</a>
                    <p>Đảm bảo hàng thật chính hãng</p>
                </span>  
            </div>

        </div>
  </div>
</div>
