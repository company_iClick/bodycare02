<?php 
    $CI =& get_instance();
    $rtr =& load_class('Router', 'core');
    $moduleName = $rtr->fetch_module();
    $className = $rtr->fetch_class();
    $methodName = $rtr->fetch_method();

    $uri_one = $this->uri->segment(1);

    $term = $this->global_function->get_array_object(array('type' => 3), 'id, name, slug', 'term');
?>
<div class="left">
    <div class="title_left"><h2>Tin tức hàng công nghệ</h2></div>
    <div class="list_article list_category_left">
    <?php if(!empty($term)){ ?>
        <ul>
        <?php 
            
            $i=0;
            foreach ($term as $value){ 
                switch ($methodName) {

                    case 'news':
                        $liClass = ($i == 0) ? ' class="active"' : NULL;
                        break;

                    case 'news_term':
                        $liClass = ($uri_one == $value->slug) ? ' class="active"' : NULL;
                        break;
                    
                    case 'news_detail':
                        $liClass = (isset($term_id) && $term_id == $value->id) ? ' class="active"' : NULL;
                         break;
                    
                    
                    default:
                        $liClass = NULL; 
                        break;

                }
            ?>
            <li<?php echo $liClass; ?>><a href="<?php echo site_url($value->slug); ?>" title="<?php echo $value->name; ?>"><i class="fa fa-plus" aria-hidden="true"></i> <?php echo $value->name; ?></a></li>
        <?php $i++;} ?>
        </ul>
    <?php } ?>
    </div>
</div>

    

