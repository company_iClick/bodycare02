<?php 
	$CI=&get_instance();
	$rtr =& load_class('Router', 'core');
	$moduleName = $rtr->fetch_module();
	$className = $rtr->fetch_class();
	$methodName = $rtr->fetch_method();
	
	$promotion = $this->global_function->show_list_table_where(array("status"=>1, "type"=>6),"article","id,name,picture,slug","weight","ASC", 5);
	$company = $this->global_function->get_tableWhere(array("id"=>1),"company","facebook");
	$picture = $this->global_function->show_list_table_where(array("status"=>1, "type"=>4),"article","id,name,picture,slug","weight","ASC", 12);
	
	$betting = $this->global_function->show_list_table_where(array("status"=>1,"hot"=>1),"betting","id,name,slug,promotion,percent,evalution,mark,send_money,withdrawal,interface,support,odds,rate,rate_image,photo,link","weight","ASC",10);
	//echo '<pre>'; print_r($promotion); echo '</pre>';die;
	
?>
<div class="sidebar main-sidebar">
	<?php if(!empty($promotion)&& $moduleName == 'home'){ ?>
    <div class="widget momizat-posts">
        <div class="widget-head">
            <h3 class="widget-title"><span>Khuyễn mãi nhà cái</span></h3>
        </div>
        <div class="mom-posts-widget">
		<?php foreach($promotion as $value){ 
			$a_name = $value->name; 
			$a_link = site_url($value->slug);
			$a_picture = base_url('uploads/tin-tuc/' . $value->picture);
		?>
            <div class="mpw-post">
                <div class="post-img main-sidebar-element">
                    <a href="<?php echo $a_link; ?>"><img src="<?php echo $a_picture; ?>" alt="<?php echo $a_name; ?>" width="90" height="60">
                    </a>
                </div>
                <div class="details has-feature-image">
                    <h4><a href="<?php echo $a_link; ?>"><?php echo $a_name; ?></a></h4>
                </div>
            </div>
            <?php } ?> 

            

        </div>
    </div>
    <?php } ?>
    
    <?php if($moduleName != 'home' && !empty($betting)){ ?>
    <div class="widget bookies-sidebar-box">
    	<div class="widget-head">
        	<div class="widget-title">
            	<span>Top Nhà Cái</span>
            </div>
        </div>		
        <table class="tbl-danhsachnhacai">
            <tbody>
            <?php foreach($betting as $bet){ 
				$name = $bet->name;
				$link_to = $bet->link;
				$link_detail = site_url('nha-cai-uy-tin/' . $bet->slug);
				$photo = base_url() . 'uploads/avatar/' . $bet->photo;
				$photoHtml = sprintf('<a href="%s" target="_blank">
                	<img alt="%s" src="%s" />
                </a>', $link_to, $name, $photo);
				
				$rating_image = (!empty($bet->rate_image)) ? '<img src="' . base_url() . 'uploads/avatar/' . $bet->rate_image . '" alt="star" />'  : NULL;
				$rating_html = sprintf('%s<a href="%s" class="nDetail">Đánh giá</a>', $rating_image, $link_detail);
			?>
                 <tr>
                    <td class="nImageBookies">
                        <?php echo $photoHtml; ?>
                    </td>
        
                    <td class="nRating">
                        <?php echo $rating_html ?>
                    </td>
        
                    <td class="nView">
                        <a target="_blank" href="<?php echo $link_to; ?>" class="nSignup">Xem</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

</div>
	<?php } ?>
    
    <div class="widget momizat-facebook">
        <div class="widget-head">
            <h3 class="widget-title"><span>Liên kết Facebook</span></h3>
        </div>
        <div class="fb-page" data-href="<?php echo $company->facebook; ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"></div></div>
        <!--like_box_footer-->

    </div>
    <?php if($moduleName == 'home' && !empty($picture)){ ?>
    <div class="widget momizat-posts-images">
        <div class="widget-head">
            <h3 class="widget-title"><span>Ảnh đẹp</span></h3>
        </div>
        <div class="new-in-pics-widget">
            <div class="nip-grid-w">
                <ul class="clearfix">
                <?php foreach($picture as $value){ 
					$a_name = $value->name; 
					$a_link = site_url($value->slug);
					$a_picture = base_url('uploads/tin-tuc/' . $value->picture);
				?>
                    <li>
                        <a href="<?php echo $a_link; ?>" class="simptip-position-top simptip-movable" data-tooltip="<?php echo $a_name; ?>"><img style="height:60px" src="<?php echo $a_picture; ?>" alt="<?php echo $a_name; ?>" width="90" height="60">
                        </a>
                    </li>
                <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <?php } ?>
</div>