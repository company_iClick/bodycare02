
        <link type="text/css" rel="stylesheet" href="<?=base_url()?>themes/css/jquery.mmenu.all.css?v=<?=time()?>" />
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.2.0.js"></script>
        <script type="text/javascript" src="<?=base_url()?>themes/js/jquery.mmenu.all.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $('nav#menu').mmenu({
                    extensions              : [ 'effect-slide-menu', 'shadow-page', 'shadow-panels' ],
                    keyboardNavigation      : true,
                    screenReader            : true,
                    counters                : true,
                });
            });
        </script>

     <div id="page">
            <div class="header_111">
                <a class="menu_click_1" href="#menu"><span></span></a>
             <a href="<?=site_url()?>" class="logo_mb_12"><img src="themes/images/logo_mobi.svg"></a>
            </div>
            <nav id="menu">
                <ul>
                <?php for($i=1;$i<3;$i++) {?>
                    <li><a href="#">Home</a></li>
                    <li><span>About us</span>
                        <ul>
                            <li><a href="#about/history">History</a></li>
                            <li><span>The team</span>
                                <ul>
                                    <li><a href="#about/team/management">Management</a></li>
                                    <li><a href="#about/team/sales">Sales</a></li>
                                    <li><a href="#about/team/development">Development</a></li>
                                </ul>
                            </li>
                            <li><a href="#about/address">Our address</a></li>
                        </ul>
                    </li>
                    <li><a href="#contact">Contact</a></li>
                    <?php } ?>
                </ul>
            </nav>
 </div>