<?php 
    $brand = $this->global_function->get_array_object(array(), 'id, name, slug, photo, thumb', 'brand', 24);
    $CI=&get_instance();
    $rtr =& load_class('Router', 'core');
    $moduleName = $rtr->fetch_module();
?>

<div id="menu_home">
    <div class="menu_home clearfix">
        <div class="cate_home cate_home_one">
            <p class="cate_parent_name">Tất cả sản phẩm</p>
            <?php if(!empty($category_mega)){ ?>
            <ul class="hide_ul_menu hide_ul_menu_cate">
                <?php foreach($category_mega as $c){
                    $id = $c->id;
                    $name = $c->name;
                    $link = site_url($c->slug);
                    $font_class = (!empty($c->font_class)) ? 'icon-' . $c->font_class : 'icon-chamsoccothe';
                    $color = (!empty($c->color)) ? $c->color : '#666';
                    $child = $c->child;
                    $banner_menu = (!empty($c->banner_menu)) ? base_url(_upload_category . $c->banner_menu) : '';
                ?> 
                <li class="menu_li_left">
                    <a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><p class="icon_fonts_menu <?php echo $font_class; ?>" style="color:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>"></p><b><?php echo $name; ?></b><?php if(!empty($child)) echo '<i class="icon_arow_1"></i>';  ?></a>
                    <?php if(!empty($child)){ ?>
                        <div class="sub_menu_left_12"<?php if(!empty($banner_menu)) echo ' style="background: #fff url('. $banner_menu .') no-repeat top right"'; ?>>
                            <div class="sub_menu_left_1_1">
                                <?php
                                $total = ceil(count($child)/2);
                                if($id == 24){
                                    $child_slice[0] = array_slice($child, 0, $total - 1);
                                    $child_slice[1]  = array_slice($child , $total - 1, count($child) );
                                }else{
                                    $child_slice[0] = array_slice($child, 0, $total);
                                    $child_slice[1]  = array_slice($child, $total, count($child) );
                                }
                                ?>
                                <ul class="ul_child_menu_left_1_1">
                                    <?php
                                    foreach($child_slice[0] as $ch){
                                        $ch_name = $ch->name;
                                        $ch_link = site_url($ch->slug);
                                        $child_one = $ch->child;
                                        ?>
                                        <li><a class="a_cap_1" href="<?php echo $ch_link; ?>" title="<?php echo $ch_name; ?>"><?php echo $ch_name; ?></a> </li>
                                        <?php if(!empty($child_one)){
                                            foreach($child_one as $ch_one){
                                                $ch_one_name = $ch_one->name;
                                                $ch_one_link = site_url($ch_one->slug);
                                                echo sprintf(' <li><a class="a_cap_2" href="%s" title="%s">%s</a></li>', $ch_one_link, $ch_one_name, $ch_one_name);
                                            }}
                                        ?>
                                    <?php } ?>
                                </ul>

                                <ul class="ul_child_menu_left_1_1">
                                    <?php
                                    foreach($child_slice[1] as $ch){
                                        $ch_name = $ch->name;
                                        $ch_link = site_url($ch->slug);
                                        $child_one = $ch->child;

                                        ?>
                                        <li><a class="a_cap_1" href="<?php echo $ch_link; ?>" title="<?php echo $ch_name; ?>"><?php echo $ch_name; ?></a> </li>
                                        <?php if(!empty($child_one)){
                                            foreach($child_one as $ch_one){
                                                $ch_one_name = $ch_one->name;
                                                $ch_one_link = site_url($ch_one->slug);
                                                echo sprintf(' <li><a class="a_cap_2" href="%s" title="%s">%s</a></li>', $ch_one_link, $ch_one_name, $ch_one_name);
                                            }}
                                        ?>
                                    <?php } ?>
                                </ul>
                            </div>
                            <a href="<?php echo $link ?>" target="_blank" class="sub_menu_left_view_link"></a>
                        </div>
                    <?php } ?>
                </li>
                <?php } ?>
            </ul>
            <?php } ?>
        </div>
        <div id="smoothmenu1" class="ddsmoothmenu">
            <?php if(!empty($category_mega)){ ?>
            <ul class="menu_li" id="menu1">
                <?php 
                $i = 0;
                foreach($category_mega as $c){
                    $id = $c->id;
                    $name = $c->name;
                    $link = site_url($c->slug);
                    if($i < 7){
                ?>
                <li class="li_menu"><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?></a></li>
                <?php } ?>
                <?php $i++;} ?>
            </ul>
            <a href="javascript:;" class="view_more_cate"><span class="fa fa-plus"></span></a>
            <ul id="view_more_cate">
                <?php 
                if(count($category_mega > 7)){ 
                $i = 0;
                foreach($category_mega as $c){
                    $id = $c->id;
                    $name = $c->name;
                    $link = site_url($c->slug);
                    if($i >= 7){
                ?>
                <li><a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><?php echo $name; ?></a></li>
                <?php } ?>
                <?php $i++;}} ?>
                <li><a href="<?php echo site_url('danh-cho-nam') ?>">Dành cho nam</a></li>
                <li><a href="<?php echo site_url('danh-cho-nu') ?>">Dành cho nữ</a></li>
                <li><a href="<?php echo site_url('thuong-hieu') ?>">Thương hiệu</a></li>
                <li><a href="<?php echo site_url('kien-thuc-lam-dep') ?>">Kiến thức làm đẹp</a></li>
            </ul>
            <?php } ?>
        </div>

    </div>
</div>
