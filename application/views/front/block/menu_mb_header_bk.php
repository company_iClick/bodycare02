<?php 
    $CI=&get_instance();
    $rtr =& load_class('Router', 'core');
    $moduleName = $rtr->fetch_module();
    $className = $rtr->fetch_class();
    $methodName = $rtr->fetch_method();
    $CI->load->model('category/a_category');
    $list_category = $this->global_function->get_array(array(), 'id, parent_id, name, slug, color, font_class, level, banner_menu', 'category');
    $category_mega = $CI->a_category->recurse_one($list_category);
    $session_login = $this->session->userdata("user_log");

?>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>themes/css/style_menu_mb.css?v=<?=time();?>" />
<script src="<?=base_url()?>themes/js/modernizr.custom.js"></script>
<div class="menu_header_mobi">

    <div class="container">
            <ul id="gn-menu" class="gn-menu-main">
                <li class="gn-trigger">
                    <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
                    <nav class="gn-menu-wrapper">
                        <div class="gn-scroller">
                            <ul class="gn-menu">
                                 <?php foreach($category_mega as $c){
                    $name = $c->name;
                    $link = site_url($c->slug);
                    $font_class = (!empty($c->font_class)) ? 'icon-' . $c->font_class : 'icon-chamsoccothe';
                    $color = (!empty($c->color)) ? $c->color : '#666';
                    $child = $c->child;
                    $banner_menu = (!empty($c->banner_menu)) ? base_url(_upload_category . $c->banner_menu) : '';
                ?> 
                <li>
                    <a href="<?php echo $link; ?>"><p class="icon_fonts_menu <?php echo $font_class; ?>" style="color:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>"></p><b><?php echo $name; ?></b><?php if(!empty($child)) echo '<i class="icon_arow_1"></i>'; ?></a></li>
                    <?php } ?>
                            </ul>
                        </div><!-- /gn-scroller -->
                    </nav>
                </li>
             <li class="li_logo_mb"><a href=""><img src="<?php echo base_url() . _upload_default . $info->logo_mobile; ?>"></a></li>
             <li class="li_giohang_mb"><a href="<?=site_url('gio-hang')?>" class="icon_giohang_1_a"><i class="could_num_11"><?php echo $this->cart->total_items() ?></i><i class="icon-giohang"></i></a></li>
             <li class="dangnhap_box_mb"><a href="" onclick="return false"><i class="icon_login_mobi_1"></i></a>

                        <ul style="display: none;">
                            <li><a href="">Dang nhâp</a></li>
                            <li><a href="">Quản lý đơn hàng</a></li>
                            <li><a href="">Địa chỉ giao hàng</a></li>
                            <li><a href="">Thóat</a></li>
                        </ul>
             </li>
            </ul>           
        </div>
</div>
<script src="<?=base_url()?>themes/js/classie.js"></script>
<script src="<?=base_url()?>themes/js/gnmenu.js"></script>
<script type="text/javascript">new gnMenu( document.getElementById('gn-menu'));</script>
<script type="text/javascript">
    $(window).scroll(function() {
   $('.gn-menu-wrapper').removeClass('gn-open-all');
});
</script>