<?php 
    $CI=&get_instance();
    $rtr =& load_class('Router', 'core');
    $moduleName = $rtr->fetch_module();
    $className = $rtr->fetch_class();
    $methodName = $rtr->fetch_method();
    
    $session_login = $this->session->userdata("user_log");
?>
<div id="header1">
    <div class="header1 clearfix">
      
        <div id="logo">
            <a href="" class="logo_pc"><img src="<?php echo base_url() . _upload_default . $info->logo; ?>"></a>
            <a href="" class="logo_mb"><img src="<?php echo base_url() . _upload_default . $info->logo_mobile; ?>"> </a>
        </div>
        <div class="menu_hide">
            <div class="cate_home">
                <p class="cate_parent_name">Danh mục sản phẩm</p>
            </div>
            <?php if(!empty($category_mega)){ ?>
            <ul class="hide_ul_menu">
                <?php foreach($category_mega as $c){
                    $id = $c->id;
                    $name = $c->name;
                    $link = site_url($c->slug);
                    $font_class = (!empty($c->font_class)) ? 'icon-' . $c->font_class : 'icon-chamsoccothe';
                    $color = (!empty($c->color)) ? $c->color : '#666';
                    $child = $c->child;
                    $banner_menu = (!empty($c->banner_menu)) ? base_url(_upload_category . $c->banner_menu) : '';
                ?> 
                <li class="menu_li_left">
                        <a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><p class="icon_fonts_menu <?php echo $font_class; ?>" style="color:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>"></p><b><?php echo $name; ?></b><?php if(!empty($child)) echo '<i class="icon_arow_1"></i>';  ?></a>
                        <?php if(!empty($child)){ ?>
                            <div class="sub_menu_left_12"<?php if(!empty($banner_menu)) echo ' style="background: #fff url('. $banner_menu .') no-repeat top right"'; ?>>
                                <div class="sub_menu_left_1_1">
                                    <?php
                                    $total = ceil(count($child)/2);
                                    if($id == 24){
                                        $child_slice[0] = array_slice($child, 0, $total - 1);
                                        $child_slice[1]  = array_slice($child , $total - 1, count($child) );
                                    }else{
                                        $child_slice[0] = array_slice($child, 0, $total);
                                        $child_slice[1]  = array_slice($child, $total, count($child) );
                                    }
                                    ?>
                                    <ul class="ul_child_menu_left_1_1">
                                        <?php
                                        foreach($child_slice[0] as $ch){
                                            $ch_name = $ch->name;
                                            $ch_link = site_url($ch->slug);
                                            $child_one = $ch->child;
                                            ?>
                                            <li><a class="a_cap_1" href="<?php echo $ch_link; ?>" title="<?php echo $ch_name; ?>"><?php echo $ch_name; ?></a> </li>
                                            <?php if(!empty($child_one)){
                                                foreach($child_one as $ch_one){
                                                    $ch_one_name = $ch_one->name;
                                                    $ch_one_link = site_url($ch_one->slug);
                                                    echo sprintf(' <li><a class="a_cap_2" href="%s" title="%s">%s</a></li>', $ch_one_link, $ch_one_name, $ch_one_name);
                                                }}
                                            ?>
                                        <?php } ?>
                                    </ul>

                                    <ul class="ul_child_menu_left_1_1">
                                        <?php
                                        foreach($child_slice[1] as $ch){
                                            $ch_name = $ch->name;
                                            $ch_link = site_url($ch->slug);
                                            $child_one = $ch->child;

                                            ?>
                                            <li><a class="a_cap_1" href="<?php echo $ch_link; ?>" title="<?php echo $ch_name; ?>"><?php echo $ch_name; ?></a> </li>
                                            <?php if(!empty($child_one)){
                                                foreach($child_one as $ch_one){
                                                    $ch_one_name = $ch_one->name;
                                                    $ch_one_link = site_url($ch_one->slug);
                                                    echo sprintf(' <li><a class="a_cap_2" href="%s" title="%s">%s</a></li>', $ch_one_link, $ch_one_name, $ch_one_name);
                                                }}
                                            ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <a href="<?php echo $link ?>" target="_blank" class="sub_menu_left_view_link"></a>
                            </div>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
            <?php } ?>

        </div>
        <div class="search_heder">
            <form id="form_search" name="form_search" action="<?php echo site_url('s-search') ?>" method="post">
                <select class="select_search_header" name="s_cate" id="s_cate">
                <option value="0">Tất cả</option>
                <?php if(!empty($category)){ 
                    foreach($category as $c){
                        $id = $c->id;
                        $name = $c->name;
                        $select = (isset($cat) && $cat == $id) ? ' selected' : '';
                        echo sprintf('<option value="%u"%s>%s</option>', $id, $select, $name);
                    }
                }?>
            </select>
                <input type="text" class="input_search" name="keyword" value="<?php if($moduleName == 'product' && $methodName == 'search') echo $this->session->userdata('keyword'); ?>" id="s_keyword" required>
                <button type="submit" id="btn_search" class="input_search_sm"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
        </div>
        <div class="header_element_right">
            <ul>
                <li><a href="<?php echo site_url('gio-hang'); ?>"><i class="icon-muahang"></i><span><?php echo $this->cart->total_items() ?></span></a></li>
                <li>
                    <p><i class="icon-huongdanmuahang"></i></p>
                    <span><a href="<?php echo site_url('huong-dan-mua-hang') ?>" title="Hướng dẫn mua hàng">Hướng dẫn mua hàng</a></span>
                </li>
                <li>
                    <?php if($session_login){ ?>
                    <p><i class="icon-taikhoan"></i></p>
                    <span class="account_header"><a href="<?php echo site_url('thong-tin-tai-khoan'); ?>">Tài khoản</a>
                        <ul>
                            <li> <a class="icon_account_1 icon-taikhoan" href="<?php echo site_url('thong-tin-tai-khoan'); ?>"><b>Tài khoản của bạn</b></a></li>
                            <li> <a class="icon_quanly icon-quanlydonhang" href="<?php echo site_url('lich-su-don-hang'); ?>"><b>Quản lý đơn hàng</b></a></li>
                            <li> <a class="icon_address_1 icon-diachigiaohang" href="<?php echo site_url('dia-chi-giao-hang'); ?>"><b>Địa chỉ giao hàng</b></a></li>
                            <li> <a class="icon_out icon-thoat" href="<?php echo site_url('dang-xuat'); ?>"><b>Thoát</b></a></li>
                        </ul>
                    </span>
                    <?php }else{?>
                    <p><i class="icon-dangky"></i></p>
                    <span><a href="<?php echo site_url('dang-ky') ?>" title="Đăng ký">Đăng ký</a></span>
                    <span><a href="<?php echo site_url('dang-nhap') ?>" title="Đăng nhập">Đăng nhập</a></span>
                    <?php } ?>
                </li>
                <li>
                    <p><i class="icon-hotline2"></i></p>
                    <span>Hotline:<p><a href="tel:<?php echo $info->phone; ?>"><?php echo $info->phone; ?></a></p></span>
                </li>
            </ul>
        </div>

    </div>
</div>
