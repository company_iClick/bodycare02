<?php 
	$term_5 = $this->global_function->show_list_table_where(array("status"=>1, "type"=>5, "hot"=>1, "parent_id"=>0),"term","id,name,slug","weight","ASC");
	if(!empty($term_5)){
		foreach($term_5 as $t5){
			$t5->article = $this->a_article->show_list_article_term_parent($t5->id, 10, 0, 1);
		}	
?>
<div class="clear"></div>
<div class="widget widget_text">
    <div class="textwidget">
    <?php foreach($term_5 as $t5){ 
		$t5_id = $t5->id;
		$t5_name = $t5->name;
		$t5_link = site_url($t5->slug);
		$t5_article = $t5->article;
	?>
        <script>
            jQuery(document).ready(function($) {
                var rtl = false;
                var rows = 1;
                if (rows !== '' && rows > 1) {
                    var divs = $(".sb-content-<?php echo $t5_id; ?> .sb-item");
                    for (var i = 0; i < divs.length; i += rows) {
                        divs.slice(i, i + rows).wrapAll("<div class='rows-1'></div>");
                    }
                }

                $(".sb-content-<?php echo $t5_id; ?>").owlCarousel({
                    items: 4,
                    baseClass: 'mom-carousel',
                    rtl: rtl,
                    autoplay: false,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    responsive: {
                        1000: {
                            items: 4
                        },

                        671: {
                            items: 3
                        },

                        480: {
                            items: 2
                        },

                        320: {
                            items: 1
                        }
                    }
                });
            });
        </script>
        <div class="news-box base-box scrolling-box-wrap">
            <header class="nb-header">
                <h2 class="nb-title" style=";"><a href="<?php echo $t5_link; ?>"><?php echo $t5_name; ?></a></h2>
            </header>
            <!--nb header-->
            <div class="nb-content">
                <div class="scrolling-box">
                    <div class="sb-content mom-carousel sb-content-<?php echo $t5_id; ?>">
                    <?php foreach($t5_article as $value){ 
						$a_name = $value->name; 
						$a_link = site_url($value->slug);
						$a_picture = base_url('uploads/tin-tuc/' . $value->picture);
					?>
                        <div class="sb-item post type-post status-publish format-standard">
                            <div class="sb-item-img">
                                <a href="<?php echo $a_link; ?>"><img src="<?php echo $a_picture; ?>" alt="<?php echo $a_name; ?>" width="265" height="168">
                                </a><span class="post-format-icon"></span>
                            </div>
                            <h3><a href="<?php echo $a_link; ?>"><?php echo $a_name; ?></a></h3>
                        </div>
                        <!--sb item-->
                    <?php } ?>    
                    </div>
                    <!--sb-content-->
                </div>
                <!--scrolling box-->
            </div>

        <?php } ?>
        </div>
        <!--news box-->
    </div>
<?php } ?>