<div id="banner_home">
    <div class="banner_home">
        <div id="left_conent">
            <ul class="left_menu_home">
                <?php if(!empty($category_mega)){
                    foreach($category_mega as $cate){
                        $id = $cate->id;
                        $font_class = (!empty($cate->font_class)) ? 'icon-' . $cate->font_class : 'icon-chamsoccothe';
                        $color = (!empty($cate->color)) ? $cate->color : '#666';
                        $name = $cate->name;
                        $link = site_url($cate->slug);
                        $child = $cate->child;
                        $banner_menu = (!empty($cate->banner_menu)) ? base_url(_upload_category . $cate->banner_menu) : '';
                        ?>
                        <li class="menu_li_left">
                            <a href="<?php echo $link; ?>" title="<?php echo $name; ?>"><p class="icon_fonts_menu <?php echo $font_class; ?>" style="color:<?php echo $color; ?>;border:1px solid <?php echo $color; ?>"></p><b><?php echo $name; ?></b><?php if(!empty($child)) echo '<i class="icon_arow_1"></i>'; ?></a>
                            <?php if(!empty($child)){ ?>
                                <div class="sub_menu_left_12"<?php if(!empty($banner_menu)) echo ' style="background: #fff url('. $banner_menu .') no-repeat top right"'; ?>>
                                    <div class="sub_menu_left_1_1">
                                        <?php
                                        $total = ceil(count($child)/2);
                                        if($id == 24){
                                            $child_slice[0] = array_slice($child, 0, $total - 1);
                                            $child_slice[1]  = array_slice($child , $total - 1, count($child) );
                                        }else{
                                            $child_slice[0] = array_slice($child, 0, $total);
                                            $child_slice[1]  = array_slice($child, $total, count($child) );
                                        }
                                        ?>
                                        <ul class="ul_child_menu_left_1_1">
                                            <?php
                                            foreach($child_slice[0] as $ch){
                                                $ch_name = $ch->name;
                                                $ch_link = site_url($ch->slug);
                                                $child_one = $ch->child;
                                                ?>
                                                <li><a class="a_cap_1" href="<?php echo $ch_link; ?>" title="<?php echo $ch_name; ?>"><?php echo $ch_name; ?></a> </li>
                                                <?php if(!empty($child_one)){
                                                    foreach($child_one as $ch_one){
                                                        $ch_one_name = $ch_one->name;
                                                        $ch_one_link = site_url($ch_one->slug);
                                                        echo sprintf(' <li><a class="a_cap_2" href="%s" title="%s">%s</a></li>', $ch_one_link, $ch_one_name, $ch_one_name);
                                                    }}
                                                ?>
                                            <?php } ?>
                                        </ul>

                                        <ul class="ul_child_menu_left_1_1">
                                            <?php
                                            foreach($child_slice[1] as $ch){
                                                $ch_name = $ch->name;
                                                $ch_link = site_url($ch->slug);
                                                $child_one = $ch->child;

                                                ?>
                                                <li><a class="a_cap_1" href="<?php echo $ch_link; ?>" title="<?php echo $ch_name; ?>"><?php echo $ch_name; ?></a> </li>
                                                <?php if(!empty($child_one)){
                                                    foreach($child_one as $ch_one){
                                                        $ch_one_name = $ch_one->name;
                                                        $ch_one_link = site_url($ch_one->slug);
                                                        echo sprintf(' <li><a class="a_cap_2" href="%s" title="%s">%s</a></li>', $ch_one_link, $ch_one_name, $ch_one_name);
                                                    }}
                                                ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <a href="<?php echo $link ?>" target="_blank" class="sub_menu_left_view_link"></a>
                                </div>
                            <?php } ?>
                        </li>
                    <?php }} ?>
                <?php if(count($category_mega) > 10){ ?>
                    <li class="menu_li_left dropdown">
                        <a onclick="return false" href="" style="text-align: center"><img src="<?php echo base_url() . _images; ?>more_menu.png"> </a>
                    </li>
                <?php } ?>
            </ul>

        </div>
        <div id="right_conent">
            <div class="right_conent_1">
                <?php if(!empty($banner_1)){ ?>
                    <div class="right_conent1">
                        <div class="right_conent1_1">
                            <?php foreach($banner_1 as $banner){
                                $link = $banner->link;
                                $img = base_url() . $banner->name;
                                $name = $banner->title; ?>
                                <a href="<?php echo $link ?>" target="_blank"><img src="<?php echo $img; ?>" alt="<?php echo $name; ?>"></a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

                <?php if(!empty($banner_2)){
                    $banner = $banner_2[0];
                    $link = $banner->link;
                    $img = base_url() . $banner->name;
                    $name = $banner->title;
                    ?>
                    <div class="right_conent2">
                        <a href="<?php echo $link ?>" target="_blank"><img src="<?php echo $img; ?>" alt="<?php echo $name; ?>"></a>
                    </div>
                <?php } ?>


            </div>

            <div class="right_conent_1">
                <div class="right_conent1">
                    <?php if(!empty($product_hot_sale)){?>
                        <div class="hot_sale">
                            <p>HOT SALE!</p>
                        </div>
                        <div id="product_slider_wp">
                            <div class="product_slider_wp">
                                <?php foreach($product_hot_sale as $pr){
                                    $name = $pr->name;
                                    $link = site_url($pr->slug);
                                    $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                                    echo sprintf('<div class="product_slider">
                                                <a href="%s" title="%s"><img src="%s" alt="%s"></a>
                                            </div> ', $link, $name, $photo, $name);
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <?php if(!empty($banner_3)){
                    $banner = $banner_3[0];
                    $link = $banner->link;
                    $img = base_url() . $banner->name;
                    $name = $banner->title;
                    ?>
                    <div class="right_conent2">
                        <a href="<?php echo $link ?>" target="_blank"><img src="<?php echo $img; ?>" alt="<?php echo $name; ?>"></a>
                    </div>
                <?php } ?>
            </div>

        </div>

    </div>


</div>

<?php $this->load->view(BLOCK . 'policy'); ?>