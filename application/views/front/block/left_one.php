<div class="left_list_product">
    <div class="list_cate_1 background_1">
        <div class="name_cate_left">
            <p>Danh mục sản phẩm</p>
            <b></b>
        </div>
        <?php if(isset($category) && !empty($category)){?>
        <ul class="ul_list_recieve_text">
            <?php foreach($category as $row){
                $name = $row->name;
                $link = site_url($row->slug);
                $count_product = $row->count_product;
                if($count_product > 0){
                    echo sprintf('<li><a href="%s" title="%s">%s<b>(%s)</b></a></li>', $link, $name, $name, $count_product);
                }
            }?>
        </ul>
        <?php } ?>
    </div>
    <div class="list_cate_1 background_1">
        <div class="name_cate_left">
            <p>Thương hiệu</p>
            <b></b>
        </div>

        <?php if(isset($list_brand) && !empty($list_brand)){?>
        <div class="list_th_scroll_1 mCustomScrollbar">
            <ul class="list_th_scroll_ul">
                <?php foreach($list_brand as $row){
                    $name = $row->name;
                    $link = site_url($row->slug);
                    $count_product = $row->count_product;
                    $class = ($this->uri->segment(1) == $row->slug) ? ' class="current"' : '';
                    echo sprintf('<li%s><a href="%s" title="%s">%s<b>(%s)</b></a></li>', $class, $link, $name, $name, $count_product);
                }?>
            </ul>
        </div>
        <?php } ?>
    </div>
</div>