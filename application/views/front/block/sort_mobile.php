<div class="context-layout" id="sort-menu">
    <div class="nav">
        <a class="context-back" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        <h2>Sắp xếp</h2>
    </div>
    <div class="wrap">
        <div class="card">
            <div class="card-menu">
                <div class="list-group">
                    <div class="list-group-item">
                        <a href="javascript:;" data-sort="">
                            <span class="text-nowrap">
                                Tất cả
                            </span>
                            <?php if($sort == '') echo '<i class="fa fa-dot-circle-o" aria-hidden="true"></i>';else echo '<i class="fa fa-circle-thin" aria-hidden="true"></i>'; ?>
                        </a>
                    </div>
                    <div class="list-group-item">
                        <a href="javascript:;" data-sort="price-asc">
                            <span class="text-nowrap">
                                Giá từ thấp đến cao
                            </span>
                            <?php if($sort == 'price-asc') echo '<i class="fa fa-dot-circle-o" aria-hidden="true"></i>';else echo '<i class="fa fa-circle-thin" aria-hidden="true"></i>'; ?>
                        </a>
                    </div>
                    <div class="list-group-item">
                        <a href="javascript:;" data-sort="price-desc">
                            <span class="text-nowrap">
                                Giá từ cao đến thấp
                            </span>
                            <?php if($sort == 'price-desc') echo '<i class="fa fa-dot-circle-o" aria-hidden="true"></i>';else echo '<i class="fa fa-circle-thin" aria-hidden="true"></i>'; ?>
                        </a>
                    </div>
                    <div class="list-group-item">
                        <a href="javascript:;" data-sort="khuyen-mai">
                            <span class="text-nowrap">
                                Khuyến mãi
                            </span>
                            <?php if($sort == 'khuyen-mai') echo '<i class="fa fa-dot-circle-o" aria-hidden="true"></i>';else echo '<i class="fa fa-circle-thin" aria-hidden="true"></i>'; ?>
                        </a>
                    </div>
                    <div class="list-group-item">
                        <a href="javascript:;" data-sort="qua-tang">
                            <span class="text-nowrap">
                                Có quà tặng
                            </span>
                            <?php if($sort == 'qua-tang') echo '<i class="fa fa-dot-circle-o" aria-hidden="true"></i>';else echo '<i class="fa fa-circle-thin" aria-hidden="true"></i>'; ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>