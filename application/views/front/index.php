<?php
$CI=&get_instance();
$rtr =& load_class('Router', 'core');
$CI->load->library('user_agent');
$CI->load->model('category/a_category');
$moduleName = $rtr->fetch_module();
$className = $rtr->fetch_class();
$methodName = $rtr->fetch_method();
$info = $info = $this->global_function->get_tableWhere(array("id" => 1), "company", "*");
$category = $this->global_function->get_array_object(array('parent_id' => 0), "id, name, slug", "category");

$list_category = $this->global_function->get_array(array(), 'id, parent_id, name, slug, color, font_class, level, banner_menu', 'category');
$category_mega = $CI->a_category->recurse_one($list_category);

$keywords = (isset($keywords) && !empty($keywords)) ? $keywords : $info->keywords;
$description = (isset($description) && !empty($description)) ? $description : $info->descriptions;
?>
<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo base_url() ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">       
        <meta http-equiv="description" name="description" content="<?php echo $description; ?>" />
        <meta http-equiv="keywords" name="keywords" content="<?php echo $keywords; ?>" />
        <meta http-equiv="author" content="Công Ty TNHH Thương Mại Điện Tử iCLick - iClick E-Commerce Company Limited." />
        <meta name="generator" content="IClick E-Commerce Co.Ltd" />
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="address=no" />
        <?php echo @$share_face; ?>
        <?php echo $info->code_meta ?>
        
        <title><?php echo (isset($title) && !empty($title)) ? $title : $info->title; ?></title>
        <link rel="shortcut icon" href="<?php echo base_url() . _images; ?>favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="<?= base_url() ?>themes/minify/g=css?time=<?php echo time(); ?>" type="text/css" />
        <?php if($moduleName == 'product' && $methodName == 'detail'){ ?>
        <link rel="stylesheet" href="<?php echo base_url() . _plugins; ?>cloud-zoom/cloud-zoom.css">
        <link rel="stylesheet" href="<?php echo base_url() . _plugins; ?>fancybox/source/jquery.fancybox.css?v=2.1.6" type="text/css" media="screen" />
        <?php } ?>
        
        <script type="text/javascript" src="<?php echo base_url() . _js; ?>jquery-1.11.1.min.js"></script>
    </head>

    <body> 
        <script>

            window.fbAsyncInit = function() {
              FB.init({
                appId      : '185966515231217',
                xfbml      : true,
                version    : 'v2.8'
              });
              FB.AppEvents.logPageView();
            };

            (function(d, s, id){
               var js, fjs = d.getElementsByTagName(s)[0];
               if (d.getElementById(id)) {return;}
               js = d.createElement(s); js.id = id;
               js.src = "//connect.facebook.net/en_US/sdk.js";
               fjs.parentNode.insertBefore(js, fjs);
             }(document, 'script', 'facebook-jssdk'));

    </script>

        <?php            
        
            $this->load->view(BLOCK . 'menu_mb_header', array('category_mega' => $category_mega)); 

            $this->load->view(BLOCK . 'header', array('info' => $info, 'category' => $category, 'category_mega' => $category_mega));
            $this->load->view(BLOCK . 'menu', array('category_mega' => $category_mega));
            if($moduleName == 'home') $this->load->view(BLOCK . 'block_banner');
 
            if($moduleName != 'home') $this->load->view(BLOCK . 'breadcrumb');
            echo $content;
            $this->load->view(BLOCK . 'footer', array('info' => $info, 'category' => $category));
            if($moduleName == 'product' && isset($sort_mobile) && $sort_mobile == 1){
                $this->load->view(BLOCK . 'sort_mobile');
            }
            if($moduleName == 'product' && isset($filter_mobile) && $filter_mobile == 1){
                $this->load->view(BLOCK . 'filter_mobile');
            }
        ?>       
                
        <script type="text/javascript" src="<?= base_url() ?>themes/minify/g=js?time=<?php echo time(); ?>"></script>

        <?php if($moduleName == 'product'){ ?>    
            <?php if($methodName != 'detail'){ ?>
                <script src="<?php echo base_url() . _js; ?>product_filter.js"></script>          
            <?php } ?>
            
            <?php if($methodName == 'detail'){ ?>
                <?php if (!$this->agent->is_mobile()) {?>
                <script src="<?php echo base_url() . _js; ?>jquery-migrate-1.2.1.min.js"></script>
                <script src="<?php echo base_url() . _plugins; ?>cloud-zoom/cloud-zoom.1.0.2.min.js"></script>
                <?php }?>
                <script type="text/javascript" src="<?php echo base_url() . _plugins; ?>fancybox/source/jquery.fancybox.pack.js?v=2.1.6"></script>
                  <?php if (!$this->agent->is_mobile()) {?>
                <script src="<?php echo base_url() . _js; ?>modernizr.js"></script>
                  <?php }?>
                <script src="<?php echo base_url() . _js; ?>script_1.js"></script>
        <?php }} ?>
        
        <?php if ($this->agent->is_mobile()) {?>
            <script src="<?=base_url()?>themes/js/mobile-main.js"></script>
            <script src="<?php echo base_url() . _js; ?>mobile.js?time=<?php echo time(); ?>"></script>
        <?php } ?>
       

    </body>
</html>
