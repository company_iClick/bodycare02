<?php
$this->load->view('back/inc/messager', array('type_messager' => $this->input->get('messager'), 'id' => $this->input->get('id'), 'type' => $this->input->get('type')));
?>
<!-- Alternative Content Box Start -->
<div class="contentcontainer">
    <div class="headings altheading"><h2>Danh sách khu vực</h2></div>
    <div class="contentbox">
        <form method="post" action=""	enctype="multipart/form-data">
            <div class="extrabottom">
                <ul>
                    <li>
                        <img src="theme_admin/img/icons/add.png" alt="Add" /> 
                        <a style="text-decoration: none;" href="admin/location/add">Thêm mới</a>
                    </li>
                    <li> <img src="theme_admin/img/icons/icon_approve.png" alt="Approve" />
                        <input class="a_button_act a_show" name="show" style="cursor: pointer" type="submit"	value="Hiện bài đã chọn" />
                    </li>
                    <li> <img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" />
                        <input class="a_button_act a_hide" name="hide" style="cursor: pointer" type="submit"	value="Ẩn bài đã chọn" />
                    </li>
                    <li> <img src="theme_admin/img/icons/icon_delete.png" alt="Delete" />
                        <p style="display:none">
                            <input class="a_button_act a_delete" name="delete" style="cursor: pointer" type="submit"	value="Delete" />
                        </p>
                        <a style=" cursor:pointer" class="delete" onclick="Delete()">Delete</a> 
                    </li>
                </ul>            
            </div>          
            <table width="100%">
                <thead>
                    <tr>
                        <th width="5%">ID</th>
                        <th width="30%">Name</th>
                        <th>Thứ tự</th>
                        <th>Action</th>
                                          <!--<th><input name="" type="checkbox" value="" id="checkboxall" /></th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $x = 0;
                    foreach ($item as $i) {
                        ?>
                        <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                            <td style="text-align:center"><?php echo $i->id ?></td>
                            <td><a style="color: #f00"><b><?php echo $i->name ?></b></a></td>
                            <td style="text-align:center"><?php echo $i->weight ?></td>
                            <td  style="text-align:center" >
                                <a href="admin/location/edit/<?php echo $i->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit" /></a>

                                <?php if ($i->status == 1) { ?>
                                    <a href="admin/location/hide/<?php echo $i->id ?>/<?php echo $page_no ?>" title="Hiện"><img src="theme_admin/img/icons/icon_approve.png" alt="Approve" /></a>
                                <?php } else { ?>
                                    <a href="admin/location/show/<?php echo $i->id ?>/<?php echo $page_no ?>" title="Ẩn"><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" /></a>
                                <?php } ?>

                                <a href="admin/location/delete/<?php echo $i->id ?>/<?php echo $page_no ?>" title="Xóa"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete" /></a>
                            </td>
                                                          <!--<td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $i->id ?>]" name="checkall[<?php echo $i->id ?>]" class="checkall"  /></td>-->
                        </tr>
                        <?php foreach ($this->m_location->show_list_location(array('parent_id' => $i->id), 10, 0, 0) as $a) { ?>
                            <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                                <td style="text-align:center"><?php echo $a->id ?></td>
                                <td><a><b>|&mdash;&nbsp;<?php echo $a->name ?></b></a></td>
                                <td style="text-align:center"><?php echo $i->weight ?></td>
                                <td style="text-align:center"  >
                                    <a href="admin/location/edit/<?php echo $a->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit" /></a>

                                    <?php if ($a->status == 1) { ?>
                                        <a href="admin/location/hide/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Hiện"><img src="theme_admin/img/icons/icon_approve.png" alt="Approve" /></a>
                                    <?php } else { ?>
                                        <a href="admin/location/show/<?php echo $a->id ?>/<?php echo $page_no ?>" title="Ẩn"><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" /></a>
                                    <?php } ?>

                                    <a href="admin/location/delete/<?php echo $i->id ?>/<?php echo $page_no ?>" title="Xóa"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete" /></a>
                                </td>
                                                              <!--<td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $a->id ?>]" name="checkall[<?php echo $a->id ?>]" class="checkall"  /></td>-->
                            </tr>
                            <?php
                            $x++;
                            foreach ($this->m_location->show_list_location(array('parent_id' => $a->id), 10, 0, 0) as $c) { ?>
                            <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                                <td style="text-align:center"><?php echo $c->id ?></td>
                                <td><a><i>|&mdash;&nbsp;&mdash;&nbsp;<?php echo $c->name ?></i></a></td>
                                <td style="text-align:center"><?php echo $c->weight ?></td>
                                <td style="text-align:center"  >
                                    <a href="admin/location/edit/<?php echo $c->id ?>" title="Chỉnh sửa"><img src="theme_admin/img/icons/icon_edit.png" alt="Edit" /></a>

                                    <?php if ($a->status == 1) { ?>
                                        <a href="admin/location/hide/<?php echo $c->id ?>/<?php echo $page_no ?>" title="Hiện"><img src="theme_admin/img/icons/icon_approve.png" alt="Approve" /></a>
                                    <?php } else { ?>
                                        <a href="admin/location/show/<?php echo $c->id ?>/<?php echo $page_no ?>" title="Ẩn"><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" /></a>
                                    <?php } ?>

                                    <a href="admin/location/delete/<?php echo $c->id ?>/<?php echo $page_no ?>" title="Xóa"><img src="theme_admin/img/icons/icon_delete.png" alt="Delete" /></a>
                                </td>
                            </tr>
                        <?php }}?>
                    <?php }
                    ?>
                </tbody>
            </table>
            <p style="color:#FF0000; font-weight:bold; text-align:center"><?php echo "Bạn chỉ được phép xóa các thư mục rỗng và không có bài viết" ?></p>
            <div style="clear: both;"></div>
            <p style="display:none">
                <input type="checkbox" value="check_item[0]" name="checkall[0]" class="checkall"   checked="checked"/>
            </p>
        </form>
        <div style="clear: both"></div>
        <?php echo $link ?>
    </div>
</div>
<la
