<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Đăng Nhập || Control Panel</title>
    <base href="<?=base_url()?>" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <!-- stylesheets -->
    <link href="theme_admin/styles/layout.css" rel="stylesheet" type="text/css" />
    <link href="theme_admin/styles/login.css" rel="stylesheet" type="text/css" />
				
		<!-- scripts (jquery) -->
		<link href="theme_admin/themes/red/styles.css" rel="stylesheet" type="text/css" />	

</head>
<body>
<div id="logincontainer">
	<div id="loginbox">
		<div id="loginheader">
			<img src="theme_admin/themes/blue/img/cp_logo_login.png" alt="Control Panel Login" />
		</div>
		<div id="innerlogin">
			<form action="<?=site_url('admin/login')?>" method="post" name="admin_login" enctype="application/x-www-form-urlencoded">
				<p>Nhập username:</p>
				<input type="text" class="logininput" name="user" id="user" /> <?php echo form_error('user'); ?>
				<p>Nhập password:</p>
				<input type="password" class="logininput" name="pass" id="pass" /> <?php echo form_error('pass'); ?>
			
				<input type="submit" class="loginbtn" value="Đăng Nhập" name="submit" /><br />
			<?php echo form_error('login'); ?>
			</form>
		</div>
	</div>
	<img src="theme_admin/img/login_fade.png" alt="Fade" /></div>
</body>
</html>