<?php 
    $count_comment = $this->global_function->count_tableWhere(array( 'read' => 0), 'comment');

    $count_feedback = $this->global_function->count_tableWhere(array('read' => 0, 'member_id !=' => 0), 'feedback');
?>
<div id="leftside">

    <div class="user">

        <img src="theme_admin/img/avatar.png" width="44" height="44" class="hoverimg" alt="Avatar" />

        <p>Login by:</p>

        <p class="username"><?php

            $user_name = $this->session->userdata('admin_login')->user_name;

            if (isset($user_name)) {

                echo $user_name;

            } else {

                redirect(site_url('admin/login'));

            }

            ?></p>

        <!-- <p class="userbtn"><a href="#" title="">Thông Tin</a></p>-->

        <p class="userbtn"><a href="admin/logout" title="">Logout</a></p>

    </div>

    <div class="notifications">

            <!-- <p class="help"><a href="#" title="">Xem hướng dẫn</a></p>-->

        <p class="change" style="font-size: 12px"><a href="admin/change-my-pass" title="" >Change Pass</a> | <a href="admin/change-info">Change Info</a></p>

    </div>

    <ul id="nav">
        <li >
            <a <?php if ($mod=='category' || $mod=='brand' || $mod=='product_type' || $mod=='skin_type' || $mod=='hair_type' || $mod=='origin' || $mod=='action' || $mod=='capacity' || $mod=='weigh' || $mod=='pill_number' || $mod=='tags' || $mod=='price_search' || $mod=='product') { ?>class="heading expanded"<?php } else {

            ?>class="heading collapsed"<?php } ?>>Sản phẩm</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'category') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/category') ?>" title="">Danh mục</a>
                </li>
                <li class="heading <?php if ($mod == 'brand') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/brand') ?>" title="">Thương hiệu</a>
                </li>
                <li class="heading <?php if ($mod == 'product_type') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/product_type') ?>" title="">Loại sản phẩm</a>
                </li>
                <li class="heading <?php if ($mod == 'skin_type') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/skin_type') ?>" title="">Loại da</a>
                </li>
                <li class="heading <?php if ($mod == 'hair_type') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/hair_type') ?>" title="">Loại tóc</a>
                </li>
                <li class="heading <?php if ($mod == 'origin') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/origin') ?>" title="">Xuất xứ</a>
                </li>
                <li class="heading <?php if ($mod == 'action') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/action') ?>" title="">Chức năng</a>
                </li>
                <li class="heading <?php if ($mod == 'capacity') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/capacity') ?>" title="">Dung tích</a>
                </li>
                <li class="heading <?php if ($mod == 'weigh') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/weigh') ?>" title="">Trọng lượng</a>
                </li>
                <li class="heading <?php if ($mod == 'pill_number') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/pill_number') ?>" title="">Số lượng viên</a>
                </li>
                <li class="heading <?php if ($mod == 'tags') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/tags') ?>" title="">Tags</a>
                </li>
                <li class="heading <?php if ($mod == 'price_search') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/price_search') ?>" title="">Khoảng giá tìm kiếm</a>
                </li>
                <li class="heading <?php if ($mod == 'website_get') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/website_get') ?>" title="">Nguồn website</a>
                </li>
                <li class="heading <?php if ($mod == 'product') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/product') ?>" title="">Danh sách</a>
                </li>
            </ul>
        </li>

        <li >
            <a <?php if ($mod=="comment") { ?>class="heading expanded"<?php } else {?>class="heading collapsed"<?php } ?>>Bình luận sản phẩm</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'comment') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/comment') ?>" title="">Danh sách<b style="float: right;color: #ff0">(<?php echo $count_comment ?> BL mới)</b></a>
                </li>
            </ul>

        </li>

        <li >
            <a <?php if ($mod=="support_staff") { ?>class="heading expanded"<?php } else {?>class="heading collapsed"<?php } ?>>Nhân viên hỗ trợ</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'support_staff') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/support_staff') ?>" title="">Danh sách</a>
                </li>
            </ul>

        </li>

        <li>

            <a <?php if ($mod=='call_back' || $mod=='advisory_request') { ?>class="heading expanded"<?php } else {
            ?>class="heading collapsed"<?php } ?>>Yêu cầu tư vấn</a>
            <ul class="navigation">
                <li class="heading <?php if ($mod == 'call_back') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/call_back') ?>" title="">Yêu cầu tư vấn sp</a>
                </li>
                <li class="heading <?php if ($mod == 'advisory_request') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/advisory_request') ?>" title="">Hỗ trợ tư vấn</a>
                </li>
            </ul>

        </li>

        <li >
            <a <?php if ($mod=="term_1" || $mod=='article_1') { ?>class="heading expanded"<?php } else {?>class="heading collapsed"<?php } ?>>Trang tĩnh</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'article_1') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/article/index/1') ?>" title="">Bài viết</a>
                </li>
            </ul>

        </li>
    
        
        <li >
            <a<?php if ($mod=="term_3" || $mod=='article_3') { ?> class="heading expanded"<?php } else {
            ?> class="heading collapsed"<?php } ?>>Kiến thức làm đẹp</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'term_3') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/term/index/3') ?>" title="">Danh mục</a>
                </li>
                <li class="heading <?php if ($mod == 'article_3') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/article/index/3') ?>" title="">Bài viết</a>
                </li>
            </ul>

        </li>


        <li >

            <a <?php if ($mod=="term_4" || $mod=='article_4') { ?>class="heading expanded"<?php } else {
            ?>class="heading collapsed"<?php } ?>>Hỗ trợ khách hàng</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'article_4') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/article/index/4') ?>" title="">Danh sách</a>
                </li>
            </ul>

        </li>

        <li >
            <a <?php if ($mod=='video') { ?>class="heading expanded"<?php } else {
            ?>class="heading collapsed"<?php } ?>>Video</a>
            <ul class="navigation">
                <li class="heading <?php if ($mod == 'video') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/video') ?>" title="">Danh sách</a>
                </li>
            </ul>
        </li>

        <li >
            <a <?php if ($mod=='customer_reviews') { ?>class="heading expanded"<?php } else {
            ?>class="heading collapsed"<?php } ?>>Ý kiến khách hàng</a>
            <ul class="navigation">
                <li class="heading <?php if ($mod == 'customer_reviews') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/customer_reviews') ?>" title="">Danh sách</a>
                </li>
            </ul>
        </li>

    
        <li >

            <?php

            $b=substr($mod,strpos($mod,"_")+1);

            $bb=substr($mod,0,(strpos($mod,"_")));

            ?>

            <a <?php if ($mod=='album'|| ($bb=="banner" && $this->global_function->count_tableWhere(array("id"=>$b),"album")>0)) { ?>class="heading expanded"<?php } else {

            ?>class="heading collapsed"<?php } ?>>Banner - Hình ảnh</a>

            <ul class="navigation">

                <?php foreach($this->global_function->show_list_table_where(array("id !="=>0, 'status' => 1),"album", "*", "weight", "asc") as $al){?>

                    <li class="heading <?php if ($mod == 'banner_'.$al->id) { ?> selected <?php } ?>">

                        <a href="<?php echo site_url('admin/banner/index/'.$al->id) ?>" title=""><?php echo $al->name?></a>

                    </li>

                <?php }?>

            </ul>

        </li>

       
        <li >

            <a <?php if ($mod=='order') { ?>class="heading expanded"<?php } else {

            ?>class="heading collapsed"<?php } ?>>Quản lý đơn hàng</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'order') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/order') ?>" title="">Đơn hàng</a>
                </li>

            </ul>

        </li>
  
        <li >

            <a <?php if ($mod=='users' || $mod=='users_feedback') { ?>class="heading expanded"<?php } else {

            ?>class="heading collapsed"<?php } ?>>Quản lý thành viên</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'users') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/users') ?>" title="">Thành viên</a>
                </li>
                <li class="heading <?php if ($mod == 'users_feedback') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/users/feedback') ?>" title="">Phản hồi<b style="float: right;color: #ff0">(<?php echo $count_feedback ?> BL mới)</b></a>
                </li>
            </ul>

        </li>
        
        <?php /* ?>
        <li >

         <a <?php if ($mod=='contact') { ?>class="heading expanded"<?php } else {

            ?>class="heading collapsed"<?php } ?>>Quản lý liên hệ</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'contact') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/contact') ?>" title="">Liên hệ</a>
                </li>                

            </ul>

        </li>
         <?php */ ?>

         <li >

            <a <?php if ($mod=='newsletter') { ?>class="heading expanded"<?php } else {

            ?>class="heading collapsed"<?php } ?>>Email khuyến mãi</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'newsletter') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/newsletter') ?>" title="">Danh sách</a>

                </li>
            </ul>

        </li>
        
        <li >

            <a <?php if ($mod=='company') { ?>class="heading expanded"<?php } else {

            ?>class="heading collapsed"<?php } ?>>Setting</a>

            <ul class="navigation">
                <li class="heading <?php if ($mod == 'company') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/company') ?>" title="">Company</a>

                </li>
            </ul>

        </li>

        <li>
            <a <?php if ($mod == 'users' || $mod == "moderator" || $mod == 'total-money' || $mod=='membership') { ?>class="heading expanded"<?php } else { ?>class="heading collapsed"<?php } ?>>Users</a>
            <ul class="navigation">
                <li class="heading <?php if ($mod == 'moderator') { ?> selected <?php } ?>">
                    <a href="<?php echo site_url('admin/moderator') ?>" title="">Quản lý</a>
                </li>

            </ul>

        </li>

    </ul>     

</div>

<!-- Left Dark Bar End