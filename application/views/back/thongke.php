<?php 
    $CI=&get_instance();
    $rtr =& load_class('Router', 'core');
    $CI=&get_instance();
    $CI->load->model(array(
        "order/m_order"
    ));
    $today = date('Y-m-d');


    $call_back = $this->global_function->show_list_table_where("DATE(`created_at`) >= CURDATE()", 'call_back', '*', 'id', 'DESC');

    $order = $this->m_order->show_order_today(array('vip' => 0));

    $advisory_request = $this->global_function->show_list_table_where("DATE(`created_at`) >= CURDATE()", 'advisory_request', '*', 'id', 'DESC');

    $all_order = $this->global_function->show_list_table_where(array('vip' => 0), 'tpl_order', 'created_at', 'id', 'DESC');
    if(!empty($all_order)){
        $arr_date_order = array();
        foreach ($all_order as $key => $value) {
            $arr_date_order[date('Y-m-d', strtotime($value->created_at))][] = $value;
        }
    }

    $all_call_back  = $this->global_function->show_list_table_where(array(), 'call_back', 'created_at', 'id', 'DESC');
    if(!empty($all_call_back)){
        $arr_date_call_back = array();
        foreach ($all_call_back as $key => $value) {
            $arr_date_call_back[date('Y-m-d', strtotime($value->created_at))][] = $value;
        }
    }

    $all_advisory_request  = $this->global_function->show_list_table_where(array(), 'advisory_request', 'created_at', 'id', 'DESC');
    if(!empty($all_advisory_request)){
        $arr_date_advisory_request = array();
        foreach ($all_advisory_request as $key => $value) {
            $arr_date_advisory_request[date('Y-m-d', strtotime($value->created_at))][] = $value;
        }
    }
?>



<link href="<?php echo base_url() ?>theme_admin/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>theme_admin/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src='<?php echo base_url() ?>theme_admin/fullcalendar/lib/moment.min.js'></script>
<script type="text/javascript" src='<?php echo base_url() ?>theme_admin/fullcalendar/fullcalendar.min.js'></script>
<style type="text/css">
.fc-start{text-align: center !important; font-size: 13px !important}
.fc-event {
    background-color: #3a87ad !important;color: #fff !important;
}
.fc-event a{color: #fff !important;}
.fc td.fc-today{background: green !important; color: #fff;}
</style>
<script>

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = yyyy+'-'+mm+'-'+dd;

    $(document).ready(function() {
        $('#calendar').fullCalendar({
            defaultDate: today,
            editable: true,
            eventLimit: true,
            navLinks: true,
            navLinkDayClick: function(date, jsEvent) {
                console.log('day', date.format()); // date is a moment
                console.log('coords', jsEvent.pageX, jsEvent.pageY);
            },
             eventClick: function(event) {
                if (event.url) {
                    window.open(event.url);
                    return false;
                }
            },
            <?php if(!empty($arr_date_order) || !empty($arr_date_order)){ ?>
            events: [
                <?php if(!empty($arr_date_order)){ 
                    foreach($arr_date_order as $key => $value) { 
                    $title = count($value) . ' đơn hàng';
                    $url = site_url('admin/order/date/' . $key);
                    $start = $key;
                ?>
                {
                    title: '<?php echo $title; ?>',
                    url: '<?php echo $url; ?>',
                    start: '<?php echo $start; ?>'
                },
                <?php }} ?>
                <?php if(!empty($arr_date_call_back)){
                    foreach($arr_date_call_back as $key => $value) { 
                    $title = count($value) . ' yêu cầu tư vấn';
                    $url = site_url('admin/call_back/date/' . $key);
                    $start = $key;
                ?>
                {
                    title: '<?php echo $title; ?>',
                    url: '<?php echo $url; ?>',
                    start: '<?php echo $start; ?>'
                },
                <?php }} ?>
                <?php if(!empty($arr_date_advisory_request)){
                    foreach($arr_date_advisory_request as $key => $value) { 
                    $title = count($value) . ' hỗ trợ tư vấn';
                    $url = site_url('admin/advisory_request/date/' . $key);
                    $start = $key;
                ?>
                {
                    title: '<?php echo $title; ?>',
                    url: '<?php echo $url; ?>',
                    start: '<?php echo $start; ?>'
                },
                <?php }} ?>
            ]
            <?php } ?>
        });
        
    });

</script>

<style>
    /* Short cut 
*/
#shortcut {
    text-align: center;
    padding: 0 0 20px 0;
}
#shortcut ul {
    padding:0;
    margin-left:0;
}
#shortcut ul li {
    display: inline-block;
    position:relative;
    margin: 10px 3px 0 3px;
    padding:10px;
    min-width:100px;
    text-align: center;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
#shortcut ul li:hover {
    background-color:#eee;
}
#shortcut ul li a {
    position:relative;
    display: block;
    font-weight: bold;
    white-space: nowrap;
    color: #626262;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
}

#shortcut ul li a span {
    display: block;
    margin-top: 5px;
    font-size:13px;
}

#shortcut ul li label {
    display: inline-block;
    padding: 2px 4px;
    font-size: 12px;
    font-weight: bold;
    line-height: 14px;
    color: white;
    vertical-align: baseline;
    white-space: nowrap;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);  
    text-shadow: 0 1px rgba(0, 0, 0, 0.2);
    background: #e23442;
    border: 1px solid #911f28;
    border-radius: 11px;
    background-image: -webkit-linear-gradient(top, #e8616c, #dd202f);
    background-image: -moz-linear-gradient(top, #e8616c, #dd202f);
    background-image: -o-linear-gradient(top, #e8616c, #dd202f);
    background-image: linear-gradient(to bottom, #e8616c, #dd202f);
    -webkit-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.1), 0 1px rgba(0, 0, 0, 0.12);
    box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.1), 0 1px rgba(0, 0, 0, 0.12);

    position:absolute;
    z-index: 99;
    top:3px;
    right:auto;
    left:55%;
    
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
}
    #calendar {
        max-width: 900px;
        margin: 0 auto;
    }
</style>

<div id="shortcut">
    <ul>
        <li>
            <label><?php echo count($advisory_request); ?> </label>
            <a href="<?php echo site_url('admin/advisory_request'); ?>" title="">
                <i class="fa fa-life-ring" aria-hidden="true" style="font-size: 34px"></i>
                <span>Gửi hỗ trợ tư vấn</span>
            </a>
        </li>
        <li>
            <label><?php echo count($call_back); ?> </label>
            <a href="<?php echo site_url('admin/call_back'); ?>" title="">
                <i class="fa fa-mobile" aria-hidden="true" style="font-size: 34px"></i>
                <span>Yêu cầu tư vấn</span>
            </a>
        </li>
        <li>
            <label><?php echo count($order); ?> </label>
            <a href="<?php echo site_url('admin/order'); ?>" title="">
                <i class="fa fa-bars" aria-hidden="true" style="font-size: 34px"></i>
                <span>Đơn hàng</span>
            </a>
        </li>
                                
    </ul>
</div>

<div class="contentcontainer">
    <?php if(!empty($call_back)){ ?>
    <div class="headings altheading"><h2>Số điện thoại yêu cầu gọi tư vấn hôm nay</h2></div>
    <div class="contentbox">
        <table width="100%">
            <thead>
                <tr>
                    <th width="5%">ID</th>
                    <th width="5%">STT</th>
                    <th>Số điện thoại</th>
                    <th width="40%;">Link sản phẩm</th>
                    <th>Thời gian yêu cầu</th>
                    <th>Action</th>
                    <th><input name="" type="checkbox" value="" id="checkboxall" /></th>
                </tr>
            </thead>

            <tbody>
                <?php
                $x = 0;
                foreach ($call_back as $i) {
                    ?>
                    <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                        <td style="text-align:center"><?php echo $i->id ?></td>
                        <td style="text-align:center"><?php echo $i->weight ?></td>
                
                        <td style="text-align:center;"><a><b><?php echo $i->phone ?></b></a></td>
                        <td style="text-align:center;"><a href="<?php echo $i->link ?>" target="_blank"><b><?php echo $i->link ?></b></a>
                        </td>
                        <td style="text-align:center;"><a><b><?php echo date('H:i:s d-m-Y', strtotime($i->created_at)) ?></b></a></td>
                        <td style="text-align:center;">

                            <?php if ($i->status == 1) { ?>
                                <a href="admin/call_back/hide/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_approve.png" alt="Approve" /></a>
                            <?php } else { ?>
                                <a href="admin/call_back/show/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" /></a>
                            <?php } ?>

                            <a data="" title="" onclick="Alert('admin/call_back/delete/<?php echo $i->id ?>')" ><img src="theme_admin/img/icons/icon_delete.png" alt="Delete" /></a>
                        </td>
                        <td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $i->id ?>]" name="checkall[<?php echo $i->id ?>]" class="checkall"  /></td>
                    </tr>            
                    <?php
                    $x++;
                    ?>

                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php } ?>

    <?php if(!empty($order)){ ?>
    <div class="headings altheading"><h2>Đơn hàng hôm nay</h2></div>
    <div class="contentbox">
        <table width="100%">

                <thead>

                    <tr>

                        <th width="3%">STT</th>
                        <th>IP</th>
                        <th>Thông tin thiết bị</th>
                        <th>Mã đơn hàng</th>
                        <th>Số điện thoại</th>
                        <th width="7%">Email</th>
                        <th width="20%">Ghi chú</th>
                        <th>Thời gian mua</th>
                        <th>Điểm tích lũy</th>
                        <th>Tình trạng</th>
                        <th width="9%">Action</th>

                        <th width="2%"><input name="" type="checkbox" value="" id="checkboxall" /></th>

                    </tr>

                </thead>

                <div style="clear: both; height: 10px"></div>

                <tbody id="load">

                    <?php

                    $x = 0;

                    foreach ($order as $i) {

                        ?>

                        <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?> id="app-<?php echo $i->id?>">

                            <td style="text-align:center"><?php echo $x + 1 ?></td>
                            <td><?php echo $i->ip_address?></td>
                            <td><?php echo $i->user_agent?></td>
                            <td>

                                <a data="" title="Permition" href="admin/order/edit/<?php echo $i->id ?>"  style="cursor: pointer">

                                    <b><?php echo $i->code ?></b>

                                </a>

                            </td>

                            <td><?php echo $i->phone?></td>
                            <td><?php echo $i->email?></td>
                            <td><?php echo $i->note?></td>
                            <td><?php echo date('H:i d/m/Y', strtotime($i->created_at));?></td>
                            <td style="text-align:center;"><?php echo $i->score?></td>
                            <td><?php echo $i->order_status_name?></td>

                            <td style="text-align:center;">

                                <a href="admin/order/edit/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_edit.png" alt="Edit" /></a>

                                <?php if ($i->status == 1) { ?>

                                    <a href="admin/order/hide/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_approve.png" alt="Approve" /></a>

                                <?php } else { ?>

                                    <a href="admin/order/show/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" /></a>

                                <?php } ?>



                                <a data="" title="Delete" onclick="Alert('admin/order/delete/<?php echo $i->id ?>')"  style="cursor: pointer"><img src="theme_admin/img/icons/icon_square_close.png" alt="Delete" /></a>



                            </td>

                            <td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $i->id ?>]" name="checkall[<?php echo $i->id ?>]" class="checkall"  /></td>

                        </tr>            

                        <?php $x++;  }?>

                </tbody>

            </table>
    </div>
    <?php } ?>

    <?php if(!empty($advisory_request)){ ?>
    <div class="headings altheading"><h2>Hỗ trợ tư vấn gửi hôm nay</h2></div>
    <div class="contentbox">
        <table width="100%">
            <thead>
                <tr>
                    <th width="5%">STT</th>
                    <th>Họ Tên</th>
                    <th>Số điện thoại</th>
                    <th>Email</th>
                    <th>Loại sản phẩm</th>
                    <th>Loại da</th>
                    <th>Nội dung</th>
                    <th>Thời gian yêu cầu</th>
                    <th>Action</th>
                    <th><input name="" type="checkbox" value="" id="checkboxall" /></th>
                </tr>
            </thead>

            <tbody>
                <?php
                $x = 0;
                foreach ($advisory_request as $i) {
                    ?>
                    <tr <?php if ($x % 2 == 0) { ?>class="alt"<?php } ?>>
                        <td style="text-align:center"><?php echo $x + 1; ?></td>
                        <td style="text-align:center;"><?php echo $i->name ?></td>
                        </td>
                        <td style="text-align:center;"><a><b><?php echo $i->phone ?></b></a></td>
                        </td>
                        <td style="text-align:center;"><?php echo $i->email ?></td>
                        </td>
                        <td style="text-align:center;"><?php echo $i->category ?></td>
                        </td>
                        <td style="text-align:center;"><?php echo $i->skin_type ?></td>
                        </td>
                        <td style="text-align:center;"><?php echo $i->content ?></td>
                        </td>
                        <td style="text-align:center;"><a><b><?php echo date('H:i:s d-m-Y', strtotime($i->created_at)) ?></b></a></td>
                        <td style="text-align:center;">

                            <?php if ($i->status == 1) { ?>
                                <a href="admin/advisory_request/hide/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_approve.png" alt="Approve" /></a>
                            <?php } else { ?>
                                <a href="admin/advisory_request/show/<?php echo $i->id ?>" title=""><img src="theme_admin/img/icons/icon_unapprove.png" alt="Unapprove" /></a>
                            <?php } ?>

                            <a data="" title="" onclick="Alert('admin/advisory_request/delete/<?php echo $i->id ?>')" ><img src="theme_admin/img/icons/icon_delete.png" alt="Delete" /></a>
                        </td>
                        <td style="text-align:center"><input type="checkbox" value="check_item[<?php echo $i->id ?>]" name="checkall[<?php echo $i->id ?>]" class="checkall"  /></td>
                    </tr>            
                    <?php
                    $x++;
                    ?>

                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php } ?>
</div>

<div id="calendar"></div>