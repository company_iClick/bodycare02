<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!Doctype html>

<html>

    <head>

        <?php $company = $this->general->show_company(); ?>

        <title><?= $company->name ?> || Control Panel</title>

        <base href="<?php echo site_url(); ?>" />

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <meta http-equiv="Cache-control" content="no-cache">

        <link rel="stylesheet" href="<?php echo base_url() ?>theme_admin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

        <link href="<?php echo base_url() ?>theme_admin/styles/layout.css" rel="stylesheet" type="text/css"/>

        <!-- scripts (jquery) -->

        <link href="<?php echo base_url() ?>theme_admin/themes/red/styles.css" rel="stylesheet" type="text/css"/>


        <script type='text/javascript' src='<?php echo base_url() ?>theme_admin/js/jquery-1.11.1.min.js'></script>

        <script type="text/javascript" src='<?php echo base_url() ?>theme_admin/scripts/functions.js'></script>

        <script type="text/javascript" src='<?php echo base_url() ?>theme_admin/js/generate_slug.js'></script>

       <script type="text/javascript" src="<?php echo base_url() ?>editor/ck/ckeditor.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>editor/find/ckfinder.js"></script>



        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!-- <script>
          $(function() {
            $( "#datepicker" ).datepicker({
                minDate : 0,
                dateFormat: 'yy-mm-dd'
            });
          });
          </script> -->



    </head>

    <body>

        <?php $this->view('back/inc/header'); ?>

        <?php $this->view('back/inc/topright'); ?>

        <?php $this->view('back/inc/left'); ?>

        <!-- Right Side/Main Content Start -->

        <div id="rightside" >

            <?php echo $content; ?>

            <div style="clear: both"></div>

            <?php $this->view('back/inc/footer'); ?>

        </div>



        <script type="text/javascript">

            $(document).ready(function(){

                $(".show-page .pagination li a").click(function(){

                    var data=$(".show-page").attr("data");

                    $(".pagination li").removeClass("page");

                    var link=$(this).attr("href");

                    $(this).parent().addClass("page");

                    var h=$("#show-ajax").height();

                    $.ajax({

                        url: link,

                        type: "GET",

                        cache: false,

                        beforeSend: function (data) {

                            $("#show_jax").append("<p id='loading-page'></p>");

                            $("#show_jax").addClass("bgwhite");

                        },

                        success: function (data) {

                            $("#show_jax").html(data);

                            $("#show_jax").removeClass("bgwhite");



                        }

                    }); //end ajax

                    return false;

                })

            })

            function Alert(e)

            {

                var r = confirm("Bạn có muốn xóa mục này");

                if (r == false)

                {

                    return false;

                } else

                    window.location = (e);



            }

            function Delete()

            {

                var r = confirm("Bạn có muốn xóa mục này");

                if (r == false)

                {

                    return false;

                } else {

                    $(".a_delete").trigger('click');

                }



            }

            function DeleteAjax(div) {

                var r = confirm("Bạn có muốn xóa mục này");

                if (r == false)

                {

                    return false;

                } else {

                    var id = $(div).attr("data-value");

                    var url = $(div).attr("data-url");

                    $.post(url, {id: id}, function(data) {

                        if (data == 1) {

                            window.location.reload();

                        }

                        else {

                            alert(data);

                        }

                    })

                }

            }



        </script>



        <script type="text/javascript" charset="utf-8" async defer>

            function locdau(str){
                str= str.toLowerCase();
                str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
                str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
                str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
                str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
                str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
                str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
                str= str.replace(/đ/g,"d");
                str= str.replace(/!|@|\$|%|\’|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|\;|\||\{|\}|~/g,"-");

                str= str.replace(/^\-+|\-+$/g,"-");
                str= str.replace(/\\/g,"-");
                str= str.replace(/-+-/g,"-");
                return str;
            }


            function add_alias_vn(obj){
                var str = $(obj).val();
                var als = locdau(str);
                $("input[name=slug]").val(als);
                //$("input[name=title]").val($("input[name=name]").val());
            }

            function set_value(type, table, id, page, value) {

                $.ajax({
                    type: 'GET',
                    url: 'admin/set-value',
                    data: {'type': type, 'table': table, 'id': id, 'page': page, 'value': value},
                    success:function(){
                        window.location.reload();
                    }
                })

            
            }

            function set_value_article(type, table, id, page, value, type_article) {

                $.ajax({
                    type: 'GET',
                    url: 'admin/set-value-article',
                    data: {'type': type, 'table': table, 'id': id, 'page': page, 'value': value, 'type_article': type_article},
                    success:function(){
                        window.location.reload();
                    }
                })
            }

        </script>



    </body>

</html>