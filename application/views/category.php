<div class="wrapper_full wrapper_full_border_1">
    <div class="wrapper_full_1">
        <div class="wrapper_full_4">
            <?php $this->load->view(BLOCK . 'left'); ?>
            
            <div class="right_list_product">
                <?php if(!empty($category->banner)){?>
                <div class="banner_pr_list">
                    <a href="<?php echo current_url(); ?>"><img src="<?php echo base_url(_upload_category . $category->banner) ?>"></a>
                </div>
                <?php } ?>
                <div class="cate_arrangement_pr_list">
                    <div class="cate_arrangement_pr_list_text">
                        <p>CHĂM SÓC DA MẶT</p>
                        <b></b>
                    </div>
                    <div class="cate_arrangement_pr_list_select">
                        <p>Sắp xếp theo</p>
                        <select class="selec_arrangementt" onchange="window.open(this.value,'_self');">
                            <?php                           
                            for($i = 0; $i < count($link_select['name']); $i++){ ?>
                            <option value="<?php echo $link_select['link'][$i] ?>"<?php if($sort == $link_select['sort'][$i]) echo ' selected'; ?>><?php echo $link_select['name'][$i] ?></option>
                            <?php } ?>
                        </select>
                        <span class="arrangementt_icon square_arrangementt active_arrangementt">
                    <i></i>
                    </span>
                        <span class="arrangementt_icon long_arrangementt">
                      <i></i>
                    </span>
                    </div>
                </div>

                <div class="list_pr_ds">
                    <?php 
                    $i = 0;
                    foreach($list_product as $pr){ 
                        $name = word_limiter($pr->name, 12);
                        //$name = $pr->name;
                        $link = site_url($pr->slug);
                        $photo = base_url() . _upload_product . $pr->folder . '/' . $pr->thumb;
                        $percent = percen_calculate($pr->price_sale, $pr->price, array('class' => 'percent_pr'));
                        $price = cms_price($pr->price_sale, $pr->price); 
                        $class = (($i + 1) % 4 == 0) ? ' child_cate_produc_2_last' : '';
                        $br_name = $pr->br_name;
                        $br_slug = site_url($pr->br_slug);
                        echo sprintf('<div class="child_cate_produc_2%s">
                                        %s
                                        <a href="%s" title="%s" class="img_pr_home">
                                            <img src="%s" alt="%s">
                                        </a>
                                        <a href="%s" class="name_link_pr">%s</a>
                                        <p><a style="color:#5e5e5e" href="%s" title="%s">%s</a></p>
                                        <div class="price_cart">
                                            %s
                                            
                                        </div>
                                    </div>', $class, $percent, $link, $name, $photo, $name, $br_slug, $br_name, $link, $name, $name, $price);
                    $i++;
                    }?>

                    <div class="clear"></div>
                    <?php //echo $paging; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(document).ready(function() {
        $('.long_arrangementt').click(function() {
            $('.child_cate_produc_2').addClass('child_cate_produc_23');
            $('.square_arrangementt').removeClass('active_arrangementt');
            $('.long_arrangementt').addClass('active_arrangementt');


        });
        // active_arrangementt

        $('.square_arrangementt').click(function() {
            $('.child_cate_produc_2').removeClass('child_cate_produc_23');
            $('.long_arrangementt').removeClass('active_arrangementt');
            $('.square_arrangementt').addClass('active_arrangementt');
        })
    });
</script>
