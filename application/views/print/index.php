<?php
$info = $this->global_function->get_tableWhere(array("id"=>1),"company","*");
?>
<!DOCTYPE html>
<html>
<head>
    <base href="<?php echo base_url() ?>" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php
        if (!empty($title)) {
            echo $title;
        } else {
            echo $info->name;
        }
        ?></title>
    <meta http-equiv="description" name="description" content="<?php
    if (!empty($description)) {
        echo $description;
    } else {
        echo $title;
    }
    ?> " />
    <meta http-equiv="keywords" content="<?php
    if (!empty($keywords)) {
        echo $keywords;
    } else {
        echo $title;
    }
    ?>" />
    <link href="" type="image/x-icon" />
    <link rel="shortcut icon" href="<?= base_url() ?>/icon.ico"  />
    <meta http-equiv="description" name="description" content="" />
    <meta http-equiv="keywords" content="" />
    <meta name="viewport" content="width=auto, initial-scale=1, maximum-scale=1, user-scalable=1"/>
    <meta http-equiv="author" content="Công Ty TNHH Thương Mại Điện Tử iCLick - iClick E-Commerce Company Limited." />
    <meta name="generator" content="IClick E-Commerce Co.Ltd" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>themes/css/default/reset.css" />
    <link type="text/css" href="<?php echo base_url() ?>themes/css/default/col.css" rel="stylesheet"/>
    <link type="text/css" href="<?php echo base_url() ?>themes/css/default/default.css" rel="stylesheet"/>
    <link type="text/css" href="<?php echo base_url() ?>themes/css/default/paging.css" rel="stylesheet"/>
    <script type='text/javascript' src='<?php echo base_url() ?>themes/js/default/jquery-1.8.2.min.js'></script>
</head>
<body>
<div></div>
<?php if(isset($h1) && $h1==1)  echo "<h1 style=' display: none'>".$info->name."</h1>";?>
<div id="wrapper" class="col_full fleft relative">
    <div id="header" class="col_full fleft" style="background: #00A1E0 !important;">
        <div id="item-header" class="m_auto" style="background: none">
            <a class="logo fleft" style="margin-left: 20px"><img src="<?php echo base_url() ?>themes/images/layout/logo.png"></a>
        </div>
    </div>
    <div id="main-content" class="m_auto">

        <?php echo $content?>
    </div>
</div><!-- wrapper-->
</body>
</html>
<script>
    $(document).ready(function(){
        $('.slider4').bxSlider({
            slideWidth: 145,
            minSlides: 2,
            maxSlides: 6,
            moveSlides: 1,
            slideMargin: 10,
            pager:false,
            controls:false,
            auto:true
        });
        Menusearch();
        generate_slug("#key-search","#key-search-slug")
    })
</script>